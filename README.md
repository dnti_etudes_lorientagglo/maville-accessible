# MaVille Accessible

![CI](https://github.com/cowork-hit/maville-accessible/actions/workflows/ci.yml/badge.svg)

## Préface

L'application MaVille Accessible est une application Laravel plutôt classique.
Si vous ne trouvez pas une information dans cette documentation, n'hésitez pas à consulter la
[documentation Laravel](https://laravel.com/docs/master).

L'application est structurée en deux parties distinctes :

- Le côté serveur (*backend*) développé avec PHP et le framework Laravel en suivant la
  spécification [JSON:API](https://jsonapi.org) ;
- Le côté client (*frontend*) développé avec Typescript, le bundler Vite et les frameworks Vue et Vuetify ;

## Conditions

Pour faire fonctionner MaVille Accessible, vous devez disposer de plusieurs dépendances logicielles :

- PHP 8.3
- Composer 2+
- NodeJS 20
- PNPM 8+
- PostgreSQL 15+ avec Postgis

Vous devez également disposer d'une compatibilité avec les
[dépendances du framework Laravel](https://laravel.com/docs/master/deployment#server-requirements),
utilisé pour ce projet.

Vous pouvez vous inspirer des fichiers suivants pour consulter les dépendances
utilisées dans un environnement local :

- [`docker-compose.yml`](docker-compose.yml) : Configuration Docker générale
- [`docker/services/app/Dockerfile`](docker/services/app/Dockerfile) : Configuration pour PHP
- [`docker/services/node/Dockerfile`](docker/services/node/Dockerfile) : Configuration pour Node

## Développement

Pour développer sur ma ville accessible, vous devez avoir
[`docker` et `docker compose`](https://www.docker.com/)
d'installés.
Vous pouvez alors commencer par cloner le projet :

```shell
git clone https://gitlab.adullact.net/dnti_etudes_lorientagglo/maville-accessible.git
```

Une fois le projet cloné, vous pouvez aller dans le dossier du projet et
lancer la commande suivante pour initialiser les services Docker:

```shell
make first
```

Vous devez également créer un bucket MinIO nommé `maville-accessible` sur
`http://storage.<APP_DOMAIN>.localhost` pour permettre l'upload de fichier
sur la plateforme en local (remplacez `<APP_DOMAIN>` par le nom de domaine choisi
dans votre fichier `.env`).

Le `Makefile` propose également d'autres entrées que vous pouvez lister via
la commande suivante:

```shell
make help
```

Vous pouvez également configurer vos variables d'environnement via le fichier
`.env` situé à la racine du projet.

> :warning: **La configuration docker proposée pour le développement ne doit
> pas être utilisée en production.**

## Production

Il y a de nombreuses façons de déployer l'application en production, et nous ne pouvons en recommander une en
particulier. C'est pourquoi nous vous proposons de consulter la
[documentation sur le déploiement de Laravel](https://laravel.com/docs/master/deployment)
afin de trouver le mode de déploiement le plus adapté à votre usage.

### Recommandations générales

Quelques recommandations tout de même :

- L'application tire bénéfice de l'asynchronisme de Laravel (queue, etc.), c'est pourquoi il est grandement recommandé
  d'utiliser ces mécanismes en production afin de fluidifier le traitement des requêtes par le serveur
  (le worker est lancé en production via la commande PHP `php artisan queue:work`);
- Un build Javascript est nécessaire pour faire fonctionner l'application. Pour générer ces fichiers,
  vous pouvez utiliser la commande `pnpm prod` après avoir configuré votre environnement ;
- Une fois l'application déployée, vous pouvez configurer certains services (logo, connexions via des services tiers,
  services pour la récupération de données)
  via la page "Plateforme > Configuration" (vous devez avoir le rôle "Administrateur de la plateforme"
  pour y avoir accès).
- Vous pouvez vous inspirer de la configuration Docker locale pour faire
  fonctionner l'application en production.

### Nom de la plateforme

Il est possible de définir les variables `APP_NAME` et `APP_TERRITORY` pour
définir le nom de la plateforme (normalement "Les accessibles") et le
territoire (par exemple "Lorient Agglomération").

### Fonctionnalités

Il est possible d'activer des fonctionnalités pour la plateforme via les variables d'environnement.

Chaque fonctionnalité disponible est listée et décrite dans
[l'`enum` appelé `FeatureName`](app/Features/FeatureName.php).

Pour activer une fonctionnalité, il suffit de déclarer une variable d'environnement `APP_FEAT_<NAME>`
où `<NAME>` correspond au nom de la fonctionnalité.

### Services externes

Il est également possible d'activer l'interaction avec des services externes
(parfois requises, notamment pour la commande `crawl:queue`).

Voici une liste des services externes configurables :

- `facebookLogin` : connexion via Facebook ;
- `googleLogin` : connexion via Google ;
- `franceConnectLogin` : connexion via France Connect ;
- `acceslibre` : récupération et envoi de données via acceslibre (catégories, lieux) ;
- `infolocale` : récupération de données via InfoLocale (catégories, actualités, activités, événements, structures et
  lieux) ;
- `datagouv` : envoi de fichiers sur des jeux de données data.gouv.fr ;
- `matomo` : suivi des statistiques de la plateforme via Matomo ;
- `hitineraire` : calcul d'itinéraires via l'API HiTinéraire ;

Par défaut, tous les services sont configurables sur la page de configuration
de la plateforme. Pour limiter les services affichés,
indiquer les identifiants de services à afficher dans la variable
d'environnement `APP_EXTERNAL_SERVICES` (séparés par des virgules) :

```dotenv
APP_EXTERNAL_SERVICES=facebookLogin,googleLogin
```

#### Notes sur certains services externes

##### `datagouv`

La mise en ligne des données dans un jeu de données data.gouv.fr effectue
les actions suivantes pour chaque jeu de données :

- Mise à jour du titre et de la description du jeu de données
  (si activé via `DATAGOUV_UPDATE_DATASET`) ;
- Création d'une nouvelle ressource contenant un fichier JSON avec les données ;
- Mise à jour de la ressource créée avec un titre et une description complète ;

#### Configuration des services externes

La plupart de la configuration des services externes se fait via la page
de configuration de la plateforme. Cependant, certains services proposent
des options de configuration avancées via des variables d'environnement.

##### `acceslibre`

- `ACCESLIBRE_ENDPOINT` : permet de personnaliser l'URL de l'API acceslibre
  (par défaut `https://acceslibre.beta.gouv.fr/api`), notamment lors d'une
  utilisation de l'API de recette (`https://recette.acceslibre.info/api`) ;
- `ACCESLIBRE_ZONE` : permet de filtrer les lieux à importer via le paramètre
  de requête `zone` (le comportement par défaut est de filtrer via le paramètre
  de requête `around` via le centre de la carte défini en configuration) ;

##### `datagouv`

- `DATAGOUV_ENDPOINT` : permet de personnaliser l'URL de l'API acceslibre
  (par défaut `https://www.data.gouv.fr/api/1`), notamment lors d'une
  utilisation de l'API de démo (`https://demo.data.gouv.fr/api/1/`) ;
- `DATAGOUV_UPDATE_DATASET` : active la mise à jour automatique de la
  description des jeux de données. Cela vous permet de personnaliser
  la description des jeux de données si vous le souhaitez ;

##### `hitineraire`

- `HITINERAIRE_ENDPOINT` : permet de personnaliser l'URL de l'API HiTinéraire
  (par défaut `https://routing.lorient-agglo.handimap.fr/api`), notamment lors d'une
  utilisation de l'API de beta (`https://dev.routing.lorient-agglo.handimap.fr/api`) ;

### Thème

Le thème (les couleurs) de la plateforme peuvent être configuré via les
variables d'environnement suivantes :

| Variable                    | Valeur par défaut                         | Description                                                                |
|-----------------------------|-------------------------------------------|----------------------------------------------------------------------------|
| `THEME_LIGHT_PRIMARY`       | `#172370`                                 | Couleur primaire utilisée pour les boutons, le bandeau de navigation, etc. |
| `THEME_LIGHT_PRIMARY_TEXT`  | Calculée à partir de la couleur primaire. | Couleur du texte primaire utilisé pour les liens.                          |
| `THEME_LIGHT_ON_PRIMARY`    | Calculée à partir de la couleur primaire. | Couleur du texte sur les éléments en couleur primaire.                     |
| `THEME_LIGHT_BACKGROUND`    | `#f3f4f7`                                 | Couleur des fonds de page de l'application.                                |
| `THEME_LIGHT_ON_BACKGROUND` | Calculée à partir de la couleur primaire. | Couleur du text sur les fonds de page de l'application.                    |
| `THEME_LIGHT_SURFACE`       | `#ffffff`                                 | Couleur des surfaces (carte, etc.) de l'application.                       |
| `THEME_LIGHT_ON_SURFACE`    | Calculée à partir de la couleur primaire. | Couleur du text sur les surfaces (carte, etc.) de l'application.           |
| `THEME_LIGHT_SECONDARY`     | `#f98958`                                 | Couleur additionnelle pour certains éléments graphique.                    |
| `THEME_LIGHT_ON_SECONDARY`  | Couleur de texte primaire.                | Couleur du texte sur un élément utilisant cette couleur additionnelle.     |
| `THEME_LIGHT_TERTIARY`      | `#74e2a0`                                 | Couleur additionnelle pour certains éléments graphique.                    |
| `THEME_LIGHT_ON_TERTIARY`   | Couleur de texte primaire.                | Couleur du texte sur un élément utilisant cette couleur additionnelle.     |
| `THEME_LIGHT_QUATERNARY`    | `#ccaef9`                                 | Couleur additionnelle pour certains éléments graphique.                    |
| `THEME_LIGHT_ON_QUATERNARY` | Couleur de texte primaire.                | Couleur du texte sur un élément utilisant cette couleur additionnelle.     |
| `THEME_LIGHT_QUINARY`       | `#ecbfd1`                                 | Couleur additionnelle pour certains éléments graphique.                    |
| `THEME_LIGHT_ON_QUINARY`    | Couleur de texte primaire.                | Couleur du texte sur un élément utilisant cette couleur additionnelle.     |

Ces variables peuvent également être déclinées pour définir le thème sombre
en changeant le préfixe de la variable (par exemple, `THEME_DARK_PRIMARY`
au lieu de `THEME_LIGHT_PRIMARY`).

### Tâches CRON

Il est également nécessaire de configurer plusieurs tâches CRON pour le bon fonctionnement de la plateforme.

#### Via `artisan:schedule`

La [configuration du `schedule`](app/Console/Kernel.php) permet de lancer certaines commandes tous les jours :

- Programmation et suppression des comptes utilisateurs inutilisés ;
- Envoi de la newsletter une fois par mois ;
- Récupération de données depuis les sources (lieux, articles, événements, activités et structures) ;

Vous pouvez configurer le `schedule` en suivant
[le guide de Laravel sur le sujet](https://laravel.com/docs/master/scheduling#running-the-scheduler).

#### Manuelle

Si les tâches programmées par le `schedule` ne vous conviennent pas (notamment
l'envoi de la newsletter ou la configuration de la récupération de données),
vous pouvez configurer manuellement des tâches CRON
pour que les commandes suivantes soient lancées tous les jours :

- `php artisan users:schedule-deletions` (alertes des comptes inutilisés)
- `php artisan users:run-deletions` (suppression des comptes inutilisés)
- `php artisan newsletter:dispatch` (envoi de la newsletter si la date du jour correspond à la
  [chronologie définie](#configuration-de-newsletterdispatch))
- `php artisan crawl:dispatch places` (récupération de lieux)
- `php artisan crawl:dispatch articles` (récupération d'articles)
- `php artisan crawl:dispatch events` (récupération d'événements)
- `php artisan crawl:dispatch activities` (récupération d'activités)
- `php artisan crawl:dispatch organizations` (récupération de structures, il faut idéalement lancer cette commande
  après toutes les autres, car certaines commandes vont récupérer des structures incomplètes)
- `php artisan datagouv:dispatch` (envoi de fichiers sur data.gouv.fr)

##### Configuration de `crawl:dispatch`

La commande `crawl:dispatch` propose différentes options pour personnaliser son action :

- `--limit=<N>` Limite le nombre de données récupérés à `N` ;
- `--no-create` Désactive la récupération de nouvelles données ;
- `--no-update` Désactive l'actualisation de données existantes ;
- `--option=only-accessible` Désactive la récupération des événements ou activités sans donnée d'accessibilité
  (uniquement pour InfoLocale)

##### Configuration de `newsletter:dispatch`

La commande `newsletter:dispatch` propose différentes fréquences d'envoi :

- `monthly`: envoi une fois par mois (le premier lundi du mois) ;
- `bimonthly`: envoi deux fois par mois (les premier et troisième lundis du mois) ;
- `weekly`: envoi une fois par semaine (le lundi) ;

Cette commande vérifie automatiquement si la date du jour correspond à un jour
où l'envoi doit être effectué.

### Création d'un super utilisateur

La commande `php artisan users:create` permet de créer un utilisateur disposant de tous
les rôles. Lors de l'appel, les informations de l'utilisateur vous seront
demandées (nom, email, mot de passe, etc.).

### Configuration des langues

Il est possible d'ajouter d'autres langues à la plateforme. Dans les exemples
qui suivent, la langue à ajouter sera `en_US` (anglais des USA).

Pour cela, il faut d'abord ajouter les fichiers de traduction associés :

- Copier le contenu dossier `lang/fr_FR` vers un dossier pour la nouvelle langue
  `lang/en_US` puis modifier toutes les traductions.
- Copier le fichier de traduction des librairies `resources/ts/plugins/i18n/lang/fr-FR.ts`
  vers un fichier dédié à la nouvelle langue `resources/ts/plugins/i18n/lang/en-US.ts`
  puis modifier les langues importés dans le fichier.

Ensuite, vous devrez indiquer des variables d'environnement pour modifier la liste
des langues disponibles sur la plateforme :

- `APP_LOCALES` décrit la liste des langues supportées par la plateforme.
  Vous pouvez changer sa valeur de `{"fr_FR":"Français"}` à
  `{"fr_FR":"Français","en_US":"English"}` pour ajouter la langue anglaise.
- `APP_EASY_READ_LOCALES` décrit la liste des langues qui peuvent disposer
  d'une version "simplifiée" pour les contenus. Par défaut, toutes les langues
  peuvent disposer d'une version "simplifié". Pour ne définir que le Français
  comme disposant d'une version "simplifié", il suffit de définir cette
  variable à `fr_FR`.

> :warning: Tous les codes de langue de l'application doivent être au format
> BCP47, c'est-à-dire 2 caractères, un tiret du bas puis 2 caractères.
> Exemple pour le Français de France : `fr_FR`.

### Implémentation d'un calculateur d'itinéraires

La plateforme Les accessibles dispose d'une possibilité d'intégrer le
calculateur d'itinéraire HiTinéraire, déjà implémenté. Vous pouvez également
choisir d'implémenter votre propre calculateur d'itinéraire, en fournissant
un autre implémentation dans le code TypeScript de la plateforme.

Voici l'interface à implementer :

```typescript
/**
 * Valeurs minimales pour le calcul d'un itinéraire.
 * Elles seront fournies à l'adaptateur lors d'une requête de calcul d'itinéraires.
 */
export type DirectionValues = {
  locale: string;
  origin: SyncPointOfInterest;
  destination: SyncPointOfInterest;
};

/**
 * Adaptateur pour le calcul d'un itinéraire.
 */
export type DirectionAdapter<V extends DirectionValues = DirectionValues> = {
  /**
   * Composant à afficher fournissant des informations sur le calculateur
   * d'itinéraires.
   * Si `null` est retourné par cette methode, aucune information ne sera affichée.
   */
  informationComponent(): Component | null;
  /**
   * Composant à afficher fournissant des champs de formulaire supplémentaires
   * (en complément du point de départ et d'arrivée).
   * Si `null` est retourné par cette methode, aucun champ supplémentaire ne sera affiché.
   */
  formComponent(): Component | null;
  /**
   * Définition des champs supplémentaires de formulaire
   * (en complément du point de départ et d'arrivée). Les valeurs fournies en paramètre
   * doivent être injectées comme valeurs par défaut des champs retournés.
   *
   * @param values
   */
  formInputs(values: Partial<V>): Dictionary<Input>;
  /**
   * Valeurs par défaut à définir sur les champs supplémentaires.
   */
  formDefaults(): Dictionary;
  /**
   * Calcul un ou plusieurs itinéraires à partir des valeurs fournies.
   *
   * @param values
   */
  computeDirection(values: V): Promise<Direction[]>;
};
```

L'implémentation de l'adaptateur est utilisée pour les tâches suivantes :

- Affichage du formulaire permettant de personnaliser l'itinéraire (autre que les points de départ/arrivée, comme
  l'utilisation de transport en commun) ;
- Affichage d'information sur le calculateur d'itinéraires utilisés (créateurs, etc.) ;
- Calcul d'un ou plusieurs itinéraires ;

La fonction `computeDirection` calcul les itinéraires possibles, et retourne des instances de la
classe `Direction`, chaque instance représentant un itinéraire possible pour la demande.
Chaque instance de `Direction` contient des informations générales (durée, distance, etc.) ainsi
qu'une liste d'étape, qui peuvent être des instances des classes suivantes :

- [`DirectionPedestrianManeuver`](resources/ts/utilities/places/directions/directionPedestrianManeuver.ts) :
  représente un trajet à effectuer en tant que piéton ;
- [`DirectionTransitManeuver`](resources/ts/utilities/places/directions/directionTransitManeuver.ts) :
  représente un trajet à effectuer en transport (bus/métro/etc.) ;
- [`DirectionTransitManeuverStop`](resources/ts/utilities/places/directions/directionTransitManeuverStop.ts) :
  représente les points de départ et d'arrivée (ou points intermédiaires) d'un trajet en transport ;

Vous pouvez vous inspirer du calculateur d'itinéraires HiTinéraire, implémenté via le fichier
[`makeHitineraireAdapter.ts`](resources/ts/utilities/places/implementations/makeHitineraireAdapter.ts).
Ce dernier est correctement documenté afin de faciliter la création d'une nouvelle implémentation s'en inspirant.

Une fois votre implémentation créée, il vous suffit de retourner votre implémentation dans le fichier
[`makeDirectionAdapter.ts`](resources/ts/utilities/places/directions/makeDirectionAdapter.ts) au lieu du comportement par défaut.

## Exemple de processus de mise à jour

Voici un exemple des étapes à réaliser pour une mise à jour de la plateforme sur un serveur
classique (base de données PostgreSQL, cache et média dans le système de fichiers, etc.) :

- [Optionnel] `php artisan down` (active le mode maintenance, app inaccessible pour les utilisateurs)
- Backup de la base de données et du système de fichiers (images mises en ligne par les utilisateurs)
- `git pull` (récupération des modifications)
- `composer install` (mise à jour des dépendances PHP)
- `pnpm install` (mise à jour des dépendances JS)
- `php artisan migrate --step --force` (mise à jour des tables de la BDD)
- `php artisan db:seed RolesAndPermissionsSeeder --force` (mise à jour des rôles et permissions)
- `composer warmup` (nettoyage du cache applicatif)
- `pnpm prod` (transpilation du JavaScript, CSS et images)
- Redémarrage des systèmes associés à l'application (Nginx, Apache, worker, CRON, etc.)
- [Optionnel] `php artisan up` (désactive le mode maintenance)

## License

L'application MaVille Accessible est fournie sous licence [CeCILL](LICENSE).
