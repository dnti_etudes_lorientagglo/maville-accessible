import vueI18n from '@intlify/unplugin-vue-i18n/vite';
import vue from '@vitejs/plugin-vue';
import autoprefixer from 'autoprefixer';
import laravel from 'laravel-vite-plugin';
import { resolve } from 'path';
import { visualizer } from 'rollup-plugin-visualizer';
import { defineConfig, loadEnv } from 'vite';
import { imagetools } from 'vite-imagetools';
import vuetify from 'vite-plugin-vuetify';

const entries = ['resources/ts/app.ts', 'resources/ts/embed.ts'];

const makeRegexChunk = (id, { include, exclude }) => ({
  id,
  include: (id) => (include ?? []).some((e) => e.test(id)),
  exclude: (id) => (exclude ?? []).some((e) => e.test(id)),
});
const customChunks = [
  makeRegexChunk('vendor-map', {
    include: [
      /node_modules\/maplibre/,
    ],
  }),
  makeRegexChunk('vendor-editor', {
    include: [
      /node_modules\/@tiptap/,
      /node_modules\/prosemirror/,
    ],
  }),
  makeRegexChunk('vendor-phone', {
    include: [
      /node_modules\/v-editor-input/,
      /node_modules\/awesome-phonenumber/,
    ],
  }),
  makeRegexChunk('vendor-app', {
    include: [
      /resources\/ts\/api/,
      /resources\/ts\/components\/account/,
      /resources\/ts\/components\/auth/,
      /resources\/ts\/components\/categories/,
      /resources\/ts\/components\/common/,
      /resources\/ts\/components\/layouts/,
      /resources\/ts\/components\/media/,
      /resources\/ts\/components\/navigation/,
      /resources\/ts\/components\/overlays/,
      /resources\/ts\/components\/routes/,
      /resources\/ts\/components\/transitions/,
      /resources\/ts\/components\/users/,
      /resources\/ts\/composables/,
      /resources\/ts\/errors/,
      /resources\/ts\/plugins/,
      /resources\/ts\/router/,
      /resources\/ts\/resources/,
      /resources\/ts\/stores/,
      /resources\/ts\/validation/,
      /resources\/ts\/utilities/,
    ],
    exclude: [
      /resources\/ts\/composables\/places/,
      /resources\/ts\/composables\/forms\/editor/,
      /resources\/ts\/composables\/links/,
      /resources\/ts\/plugins\/phone/,
      /resources\/ts\/plugins\/tiptap/,
      /resources\/ts\/utilities\/places/,
    ],
  }),
];

const loadEnvVariables = (mode) => {
  Object.assign(process.env, loadEnv(mode, process.cwd(), ''));
};

const manualChunks = (id) => {
  if (entries.some((e) => id.includes(e))) {
    return;
  }

  const customChunk = customChunks.find(
    (c) => c.include(id) && !c.exclude(id),
  );
  if (customChunk) {
    return customChunk.id;
  }
};

export default defineConfig(({ mode }) => {
  loadEnvVariables(mode);

  return {
    define: {
      __APP_THEME: {
        light: {
          primary: process.env.THEME_LIGHT_PRIMARY || '#172370',
          'primary-text': process.env.THEME_LIGHT_PRIMARY_TEXT,
          'on-primary': process.env.THEME_LIGHT_ON_PRIMARY,
          background: process.env.THEME_LIGHT_BACKGROUND || '#f3f4f7',
          'on-background': process.env.THEME_LIGHT_BACKGROUND,
          surface: process.env.THEME_LIGHT_SURFACE || '#ffffff',
          'on-surface': process.env.THEME_LIGHT_ON_SURFACE,
          secondary: process.env.THEME_LIGHT_SECONDARY || '#f98958',
          'on-secondary': process.env.THEME_LIGHT_ON_SECONDARY,
          tertiary: process.env.THEME_LIGHT_TERTIARY || '#74e2a0',
          'on-tertiary': process.env.THEME_LIGHT_ON_TERTIARY,
          quaternary: process.env.THEME_LIGHT_QUATERNARY || '#ccaef9',
          'on-quaternary': process.env.THEME_LIGHT_ON_QUATERNARY,
          quinary: process.env.THEME_LIGHT_QUINARY || '#ecbfd1',
          'on-quinary': process.env.THEME_LIGHT_ON_QUINARY,
        },
        dark: {
          primary: process.env.THEME_DARK_PRIMARY || '#ffffff',
          'primary-text': process.env.THEME_DARK_PRIMARY_TEXT,
          'on-primary': process.env.THEME_DARK_ON_PRIMARY,
          background: process.env.THEME_DARK_BACKGROUND || '#172370',
          'on-background': process.env.THEME_DARK_BACKGROUND || '#ffffff',
          surface: process.env.THEME_DARK_SURFACE || '#090f32',
          'on-surface': process.env.THEME_DARK_SURFACE || '#ffffff',
          secondary: process.env.THEME_DARK_SECONDARY || '#f98958',
          'on-secondary': process.env.THEME_DARK_ON_SECONDARY || '#090f32',
          tertiary: process.env.THEME_DARK_TERTIARY || '#74e2a0',
          'on-tertiary': process.env.THEME_DARK_ON_TERTIARY || '#090f32',
          quaternary: process.env.THEME_DARK_QUATERNARY || '#ccaef9',
          'on-quaternary': process.env.THEME_DARK_ON_QUATERNARY || '#090f32',
          quinary: process.env.THEME_DARK_QUINARY || '#ecbfd1',
          'on-quinary': process.env.THEME_DARK_ON_QUINARY || '#090f32',
        },
      },
    },
    plugins: [
      laravel({
        input: entries,
        refresh: true,
      }),
      vue(),
      vuetify({
        styles: { configFile: 'resources/assets/sass/vuetify/settings.scss' },
      }),
      vueI18n({
        runtimeOnly: false,
      }),
      imagetools({
        defaultDirectives: (id) => {
          if (id.searchParams.has('placeholder')) {
            return new URLSearchParams('blur=10&w=200&quality=30&format=webp');
          }

          if (id.searchParams.has('responsive')) {
            return new URLSearchParams('w=200;500;900;1200;1600&format=webp&as=srcset');
          }

          return new URLSearchParams();
        },
      }),
    ],
    css: {
      postcss: {
        plugins: [
          autoprefixer(),
        ],
      },
      preprocessorOptions: {
        scss: {
          api: 'modern-compiler',
        },
      },
    },
    optimizeDeps: {
      exclude: ['vuetify'],
    },
    build: {
      rollupOptions: {
        plugins: [
          process.env.BUILD_ANALYZE ? visualizer({
            filename: 'public/build/bundle-stats.html',
            open: true,
          }) : undefined,
        ],
        output: {
          manualChunks,
        },
      },
    },
    resolve: {
      alias: {
        '@': resolve(__dirname, './resources'),
      },
    },
  };
});
