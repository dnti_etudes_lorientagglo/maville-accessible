<?php

namespace Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;

trait RefreshAndSeedDatabase
{
    use RefreshDatabase;

    /**
     * @var bool Enable database seeding.
     */
    protected bool $seed = true;
}
