<?php

use App\Models\User;
use Tests\RefreshAndSeedDatabase;

use function Pest\Laravel\{postJson};

uses(RefreshAndSeedDatabase::class);

describe('authentication', function () {
    it('should fail login with invalid password', function () {
        User::factory()->create([
            'email' => 'john.doe@example.com',
        ]);

        $response = postJson('/auth/login', [
            'data' => [
                'email'    => 'john.doe@example.com',
                'password' => 'wrong password',
            ],
        ]);

        expect($response)
            ->assertStatus(422)
            ->assertJsonFragment([
                'message' => 'Adresse email ou mot de passe invalide.',
            ]);
    });

    it('should pass login with valid password', function () {
        $user = User::factory()->create([
            'email' => 'john.doe@example.com',
        ]);

        $response = postJson('/auth/login', [
            'data' => [
                'email'    => 'john.doe@example.com',
                'password' => 'password',
            ],
        ]);

        expect($response)
            ->assertStatus(200)
            ->assertJsonFragment([
                'message' => 'Connexion au compte réussie.',
                'id'      => $user->id,
            ]);
    });
});
