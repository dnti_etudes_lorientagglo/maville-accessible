# Executables
DOCKER_COMPOSE?=docker compose
DOCKER_EXEC?=$(DOCKER_COMPOSE) exec -it
COMPOSER?=$(DOCKER_EXEC) application composer
ARTISAN?=$(DOCKER_EXEC) application php artisan
PNPM?=$(DOCKER_EXEC) node pnpm

# Misc
default: help

##
## —— Setup ————————————————————————————————————————————————————————————————————

.PHONY: build
build: ## Build and start containers.
	@$(DOCKER_COMPOSE) up --build --no-recreate -d

.PHONY: rebuild
rebuild: ## Force rebuild and start all containers.
	@$(DOCKER_COMPOSE) up --build --force-recreate --remove-orphans -d

.PHONY: up
up: ## Start containers without building.
	@$(DOCKER_COMPOSE) up -d

.PHONY: stop
stop: ## Stop containers.
	@$(DOCKER_COMPOSE) stop

.PHONY: down
down: ## Stop and remove containers.
	@$(DOCKER_COMPOSE) down --remove-orphans --timeout=2

.PHONY: restart-service
restart-service: ## Restart a container (e.g. make restart-service c="queue").
	@$(DOCKER_COMPOSE) restart $(c)

.PHONY: first
first: ## Build and start for the first time on a local env.
	@$(MAKE) --no-print-directory first-build
	@echo "[Starting JS client...]"
	@$(MAKE) --no-print-directory dev

.PHONY: first-ci
first-ci: ## Build and start for the first time on a CI env.
	@$(MAKE) --no-print-directory first-build

.PHONY: first-build
first-build: # [internal] Build and start for the first time.
	@echo "[Copying env...]"
	@cp --no-clobber .env.example .env
	@echo "[Building containers...]"
	@$(MAKE) --no-print-directory build

##
## —— Executables ——————————————————————————————————————————————————————————————

.PHONY: pnpm
pnpm: ## Run a PNPM command (e.g. make pnpm c="upgrade").
	@$(PNPM) $(c)

.PHONY: composer
composer: ## Run a Composer command (e.g. make composer c="update").
	@$(COMPOSER) $(c)

.PHONY: artisan
artisan: ## Execute an Artisan command (e.g. make artisan c="about").
	@$(ARTISAN) $(c)

##
## —— Dev ——————————————————————————————————————————————————————————————————————

.PHONY: start
start: up dev ## Start containers and dev build.

.PHONY: restart
restart: stop start ## Restart containers and dev build

.PHONY: dev
dev: ## Make a dev (HMR) frontend build.
	@$(PNPM) dev --host

.PHONY: prod
prod: ## Make a production frontend build.
	@$(PNPM) prod

##
## —— Lint and tests ———————————————————————————————————————————————————————————

.PHONY: lint-php
lint-php: ## Lint PHP code.
	@$(COMPOSER) lint

.PHONY: pest
pest: ## Run PHP tests.
	@$(COMPOSER) pest

.PHONY: lint-ts
lint-ts: ## Lint TS code.
	@$(PNPM) lint

.PHONY: tsc
tsc: ## Compile TS code.
	@$(PNPM) tsc

.PHONY: lint
lint: lint-php lint-ts ## Lint code.

.PHONY: test
test: lint tsc pest ## Test code.

##
## —— Utilities ————————————————————————————————————————————————————————————————

.PHONY: bash
bash: ## Run BASH on PHP container.
	@$(DOCKER_EXEC) application bash

.PHONY: help
help: ## Show help for each of the Makefile recipes.
	@grep -E '(^[a-zA-Z0-9\./_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
