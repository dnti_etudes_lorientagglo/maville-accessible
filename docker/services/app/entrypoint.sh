#!/bin/bash

if [ ! -d vendor ]
then
  echo "[Installing dependencies...]"
  composer install --no-progress --no-interaction
fi

echo "[Setting up app...]"
php artisan app:setup

exec "$@"
