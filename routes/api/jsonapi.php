<?php

use App\Features\FeatureName;
use App\Features\Features;
use App\Helpers\JsonApiHelper;
use App\Http\Controllers\JsonApi\V1\ActivitiesController;
use App\Http\Controllers\JsonApi\V1\ArticlesController;
use App\Http\Controllers\JsonApi\V1\CategoriesController;
use App\Http\Controllers\JsonApi\V1\ConfigsController;
use App\Http\Controllers\JsonApi\V1\MediaController;
use App\Http\Controllers\JsonApi\V1\NotificationsController;
use App\Http\Controllers\JsonApi\V1\OrganizationInvitationsController;
use App\Http\Controllers\JsonApi\V1\OrganizationsController;
use App\Http\Controllers\JsonApi\V1\PlacesController;
use App\Http\Controllers\JsonApi\V1\ReactionsController;
use App\Http\Controllers\JsonApi\V1\ReportsController;
use App\Http\Controllers\JsonApi\V1\RequestsController;
use App\Http\Controllers\JsonApi\V1\ReviewRepliesController;
use App\Http\Controllers\JsonApi\V1\ReviewsController;
use App\Http\Controllers\JsonApi\V1\Services\AccessibilityController;
use App\Http\Controllers\JsonApi\V1\Services\NewslettersController;
use App\Http\Controllers\JsonApi\V1\Services\TranslationsController;
use App\Http\Controllers\JsonApi\V1\UsersController;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use LaravelJsonApi\Laravel\Facades\JsonApiRoute;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;
use LaravelJsonApi\Laravel\Routing\ActionRegistrar;
use LaravelJsonApi\Laravel\Routing\Relationships;
use LaravelJsonApi\Laravel\Routing\ResourceRegistrar;

JsonApiRoute::server('v1')->prefix('v1')->resources(static function (ResourceRegistrar $server) {
    $server->resource('configs', ConfigsController::class)
        ->only('show', 'update')
        ->actions('-actions', static function (ActionRegistrar $actions) {
            $actions->withId()->post('services/activate/hitineraire', 'activateServiceHitineraire');
            $actions->withId()->post('services/activate/acceslibre', 'activateServiceAcceslibre');
            $actions->withId()->post('services/update/acceslibre', 'updateServiceAcceslibre');
            $actions->withId()->post('services/activate/infolocale', 'activateServiceInfolocale');
            $actions->withId()->post('services/update/infolocale', 'updateServiceInfolocale');
            $actions->withId()->post('services/activate/datagouv', 'activateServiceDataGouv');
            $actions->withId()->post('services/update/datagouv', 'updateServiceDataGouv');
            $actions->withId()->post('services/activate/matomo', 'activateServiceMatomo');
            $actions->withId()->post('services/activate/google-login', 'activateServiceGoogleLogin');
            $actions->withId()->post('services/activate/facebook-login', 'activateServiceFacebookLogin');
            $actions->withId()->post('services/activate/france-connect-login', 'activateServiceFranceConnectLogin');
            $actions->withId()->post('services/remove', 'removeService');
        });

    $server->resource('permissions', JsonApiController::class)
        ->readOnly();

    $server->resource('roles', JsonApiController::class)
        ->readOnly();

    $server->resource('users', UsersController::class)
        ->except('create', 'delete')
        ->actions('-actions', static function (ActionRegistrar $actions) {
            $actions->withId()->post('verify');
            $actions->withId()->post('block');
            $actions->withId()->post('unblock');
            $actions->withId()->post('schedule-deletion');
        });

    $server->resource('categories', CategoriesController::class)
        ->actions('-actions', static function (ActionRegistrar $actions) {
            JsonApiHelper::publishableRoutes($actions);
            JsonApiHelper::sourceableRoutes($actions);
        });

    $server->resource('media', MediaController::class)
        ->except('create')
        ->actions('-actions', static function (ActionRegistrar $actions) {
            $actions->post('upload');
            $actions->post('client-side/prepare-upload', 'prepareClientSideUpload');
            $actions->post('client-side/confirm-upload', 'confirmClientSideUpload');
        });

    $server->resource('notifications', NotificationsController::class)
        ->except('create')
        ->actions('-actions', static function (ActionRegistrar $actions) {
            $actions->post('read-all');
        });

    $server->resource('report-types', JsonApiController::class);

    $server->resource('reports', ReportsController::class)
        ->except('delete');

    $server->resource('request-types', JsonApiController::class)
        ->readOnly();

    $server->resource('requests', RequestsController::class)
        ->except('delete')
        ->actions('-actions', static function (ActionRegistrar $actions) {
            $actions->withId()->post('accept');
            $actions->withId()->post('refuse');
        });

    $server->resource('reviews', ReviewsController::class)
        ->except('index');

    $server->resource('review-replies', ReviewRepliesController::class)
        ->except('index');

    $server->resource('reactions', ReactionsController::class)
        ->except('index');

    $server->resource('sending-campaign-types', JsonApiController::class)
        ->readOnly();

    $server->resource('sending-campaign-batches', JsonApiController::class)
        ->readOnly();

    $server->resource('sending-campaigns', JsonApiController::class)
        ->readOnly();

    $server->resource('organization-types', JsonApiController::class)
        ->readOnly();

    $server->resource('organizations', OrganizationsController::class)
        ->actions('-actions', static function (ActionRegistrar $actions) {
            JsonApiHelper::publishableRoutes($actions);
            JsonApiHelper::sourceableRoutes($actions);
        });

    $server->resource('organization-members', JsonApiController::class)
        ->except('index', 'show');

    $server->resource('organization-invitations', OrganizationInvitationsController::class)
        ->except('index')
        ->actions('-actions', static function (ActionRegistrar $actions) {
            $actions->withId()->post('accept');
            $actions->withId()->post('refuse');
        });

    $server->resource('places', PlacesController::class)
        ->relationships(static function (Relationships $relationships) {
            JsonApiHelper::reviewableRelations($relationships);
        })
        ->actions('-actions', static function (ActionRegistrar $actions) {
            JsonApiHelper::publishableRoutes($actions);
            JsonApiHelper::sourceableRoutes($actions);
        });

    $server->resource('articles', ArticlesController::class)
        ->actions('-actions', static function (ActionRegistrar $actions) {
            JsonApiHelper::pinnableRoutes($actions);
            JsonApiHelper::publishableRoutes($actions);
            JsonApiHelper::sourceableRoutes($actions);
        });

    $server->resource('activity-types', JsonApiController::class)
        ->readOnly();

    $server->resource('activities', ActivitiesController::class)
        ->relationships(static function (Relationships $relationships) {
            JsonApiHelper::reviewableRelations($relationships);
        })
        ->actions('-actions', static function (ActionRegistrar $actions) {
            JsonApiHelper::pinnableRoutes($actions);
            JsonApiHelper::publishableRoutes($actions);
            JsonApiHelper::sourceableRoutes($actions);
        });

    Route::prefix('-actions')->group(static function (Router $router) {
        $router->get('translations', [TranslationsController::class, 'index']);

        $router->get('accessibility/criteria', [AccessibilityController::class, 'criteria']);

        if (Features::check(FeatureName::NEWSLETTER)) {
            $router->get('newsletters/subscription/{email}', [NewslettersController::class, 'read'])
                ->middleware([ThrottleRequests::with(6, 1)]);
            $router->post('newsletters/subscription/{email}', [NewslettersController::class, 'subscribe'])
                ->middleware([ThrottleRequests::with(6, 1)]);
            $router->delete('newsletters/subscription/{email}', [NewslettersController::class, 'unsubscribe'])
                ->middleware([ThrottleRequests::with(6, 1)]);
        }
    });
});
