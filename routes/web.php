<?php

use App\Http\Controllers\Web\AppController;
use App\Http\Controllers\Web\EmbedController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Authentication routes.
require __DIR__ . '/web/auth.php';

// Custom actions routes.
require __DIR__ . '/web/actions.php';

// Local (dev) only routes.
require __DIR__ . '/web/local.php';

Route::get('embed/{type}', [EmbedController::class, '__invoke']);

Route::get('/{any}', [AppController::class, '__invoke'])
    ->where('any', '^(?!api).*$')
    ->name('app');
