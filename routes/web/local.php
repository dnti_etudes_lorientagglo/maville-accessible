<?php

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;

if (App::environment('local')) {
    $redirectImagetools = static fn(Request $request) => new RedirectResponse(
        'http://[::]:5173/' . $request->path(),
    );

    Route::get('/@imagetools/{any}', $redirectImagetools)
        ->where('any', '^.*$')
        ->name('imagetools');
}
