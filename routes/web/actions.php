<?php

use App\Http\Controllers\Web\HealthcheckController;
use App\Http\Controllers\Web\Services\MediaController;
use App\Http\Controllers\Web\Services\SendingCampaignController;
use App\Http\Middleware\Authenticate;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

Route::prefix('-actions')->name('actions.')->group(static function (Router $router) {
    $router->get('healthcheck', [HealthcheckController::class, '__invoke']);

    $router->get('sending-campaigns/preview/{campaign}', [SendingCampaignController::class, 'preview'])
        ->name('sending-campaigns.preview');
    $router->get('sending-campaigns/browser/{campaign}/{recipient}', [SendingCampaignController::class, 'browser'])
        ->name('sending-campaigns.browser');

    $router->middleware([Authenticate::class])->group(static function (Router $router) {
        $router->get('media/{media}/redirect', [MediaController::class, 'redirect'])
            ->name('media.redirect')
            ->whereUuid('media');
        $router->get('media/{media}/resolve', [MediaController::class, 'resolve'])
            ->name('media.resolve')
            ->whereUuid('media');
    });
});
