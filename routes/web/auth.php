<?php

use App\Http\Controllers\Web\AppController;
use App\Http\Controllers\Web\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Web\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Web\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Web\Auth\NewPasswordController;
use App\Http\Controllers\Web\Auth\PasswordResetLinkController;
use App\Http\Controllers\Web\Auth\RegisteredUserController;
use App\Http\Controllers\Web\Auth\SocialAuthenticatedSessionController;
use App\Http\Controllers\Web\Auth\VerifyEmailController;
use App\Http\Middleware\Authenticate;
use App\Http\Middleware\RedirectIfAuthenticated;
use App\Http\Middleware\ValidateSignature;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

Route::prefix('auth')->group(static function (Router $router) {
    $router->middleware([RedirectIfAuthenticated::class])->group(static function (Router $router) {
        $router->post('register', [RegisteredUserController::class, 'store']);

        $router->get('sign-in', [AppController::class, '__invoke'])->name('login');
        $router->post('login', [AuthenticatedSessionController::class, 'store']);

        $router->post('forgot-password', [PasswordResetLinkController::class, 'store'])
            ->middleware([ThrottleRequests::with(6, 1)]);

        $router->post('reset-password', [NewPasswordController::class, 'store']);

        $router->get('social/{network}/redirect', [SocialAuthenticatedSessionController::class, 'redirect']);
        $router->get('social/{network}/callback', [SocialAuthenticatedSessionController::class, 'callback']);
    });

    $router->middleware([Authenticate::class])->group(static function (Router $router) {
        $router->get('verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])
            ->middleware([ValidateSignature::class, ThrottleRequests::with(6, 1)])
            ->name('verification.verify');

        $router->post('email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
            ->middleware([ThrottleRequests::with(6, 1)]);

        $router->post('confirm-password', [ConfirmablePasswordController::class, 'store']);

        $router->post('logout', [AuthenticatedSessionController::class, 'destroy']);
    });
});
