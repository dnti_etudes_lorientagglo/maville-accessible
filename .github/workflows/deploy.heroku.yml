name: deploy.heroku
run-name: "release [${{ inputs.target }}] by @${{ github.actor }}"

on:
  workflow_dispatch:
    inputs:
      target:
        type: choice
        description: Choose target app env
        options:
          - dev
          - staging
          - production
        default: dev
  workflow_call:
    inputs:
      target:
        type: string
        description: Target app env
        default: dev

jobs:
  deploy:
    name: Deploy Heroku
    runs-on: ubuntu-latest
    timeout-minutes: 30
    steps:
      - uses: actions/checkout@v4
        with:
          fetch-depth: 0

      - name: Heroku install
        run: curl https://cli-assets.heroku.com/install.sh | sh
      - name: Heroku version
        run: echo $(heroku --version)
      - name: Heroku login
        run: |
          cat > ~/.netrc <<EOF
            machine api.heroku.com
              login $HEROKU_EMAIL
              password $HEROKU_API_KEY
            machine git.heroku.com
              login $HEROKU_EMAIL
              password $HEROKU_API_KEY
          EOF
        env:
          HEROKU_EMAIL: ${{ secrets.HEROKU_EMAIL }}
          HEROKU_API_KEY: ${{ secrets.HEROKU_API_KEY }}
      - name: Heroku connect remote
        run: heroku git:remote -a lorient-accessible-${{ inputs.target }}

      - name: Create .env file
        run: |
          touch .env

      - name: Extract env from Heroku
        run: |
          echo "APP_KEY=" >> .env
          echo "APP_URL=$(heroku config:get APP_URL)" >> .env
          echo "THEME_LIGHT_PRIMARY=\"$(heroku config:get THEME_LIGHT_PRIMARY)\"" >> .env
          echo "THEME_LIGHT_PRIMARY_TEXT=\"$(heroku config:get THEME_LIGHT_PRIMARY_TEXT)\"" >> .env
          echo "THEME_LIGHT_ON_PRIMARY=\"$(heroku config:get THEME_LIGHT_ON_PRIMARY)\"" >> .env
          echo "THEME_DARK_PRIMARY=\"$(heroku config:get THEME_DARK_PRIMARY)\"" >> .env
          echo "THEME_DARK_PRIMARY_TEXT=\"$(heroku config:get THEME_DARK_PRIMARY_TEXT)\"" >> .env
          echo "THEME_DARK_ON_PRIMARY=\"$(heroku config:get THEME_DARK_ON_PRIMARY)\"" >> .env

      - name: Login to Docker Hub
        uses: docker/login-action@v3
        with:
          username: ${{ secrets.DOCKERHUB_USERNAME }}
          password: ${{ secrets.DOCKERHUB_TOKEN }}

      - name: Install and build
        run: make first-ci

      - name: Remove build exclusion from gitignore
        run: sed -i '/^\/public/d' .gitignore

      - name: Copy dedicated config files
        run: cp deploy/heroku/nginx.conf deploy/heroku/Procfile ./

      - name: Commit build changes
        run: |
          git config user.name "heroku-deploy"
          git config user.email "${{ secrets.HEROKU_EMAIL }}"
          git add -A
          git commit -m "release: lorient-accessible-${{ inputs.target }}"

      - name: Heroku enable maintenance
        run: heroku maintenance:on

      - name: Heroku DB backup
        run: heroku pg:backups:capture

      - name: Heroku deploy
        run: git push heroku HEAD:refs/heads/main -f

      - name: Heroku DB migrations
        run: heroku run -x "php artisan migrate --step --force"

      - name: Heroku disable maintenance
        run: heroku maintenance:off

      - name: Show failure logs
        if: failure()
        run: |
          docker logs application
          cat storage/logs/laravel.log
