export default function shouldIgnoreError(error: unknown) {
  if (typeof error === 'string') {
    return /^ResizeObserver loop/.test(error);
  }

  return false;
}
