import handleError from '@/ts/errors/handleError';
import { App } from 'vue';
import { Router } from 'vue-router';

export default function prepareErrorHandler(app: App, router?: Router) {
  // eslint-disable-next-line no-param-reassign
  app.config.errorHandler = handleError;
  if (router) {
    router.onError(handleError);
  }

  window.onerror = handleError;
  window.onunhandledrejection = (event) => {
    handleError(event);

    event.preventDefault();
  };
}
