import RedirectableError from '@/ts/errors/redirectable/redirectableError';

export default function makeForbiddenError() {
  return new RedirectableError({ name: 'errors.forbidden' });
}
