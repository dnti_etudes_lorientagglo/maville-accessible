import RedirectableError from '@/ts/errors/redirectable/redirectableError';
import routeWithIntended from '@/ts/utilities/routes/routeWithIntended';
import { RouteLocationNormalized } from 'vue-router';

export default function makeUnauthorizedError(to?: RouteLocationNormalized) {
  return new RedirectableError(routeWithIntended(
    { name: 'auth.sign-in' },
    to
      ? to.fullPath
      : `${window.location.pathname}${window.location.search}${window.location.hash}`,
  ));
}
