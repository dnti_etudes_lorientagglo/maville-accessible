import AppError from '@/ts/errors/appError';
import { RouteLocationRaw } from 'vue-router';

export default class RedirectableError extends AppError {
  public readonly redirectTo: RouteLocationRaw;

  public constructor(redirectTo: RouteLocationRaw, message?: string) {
    super(message);

    this.redirectTo = redirectTo;
  }
}
