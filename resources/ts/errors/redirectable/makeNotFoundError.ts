import RedirectableError from '@/ts/errors/redirectable/redirectableError';

export default function makeNotFoundError() {
  return new RedirectableError({ name: 'errors.not-found' });
}
