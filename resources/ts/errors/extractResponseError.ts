import { JsonApiDocument } from '@foscia/jsonapi';
import { HttpResponseError } from '@foscia/http';

export default async function extractResponseError(error: HttpResponseError) {
  const document: JsonApiDocument = await error.response.json();

  return document.errors?.[0]?.detail ?? null;
}
