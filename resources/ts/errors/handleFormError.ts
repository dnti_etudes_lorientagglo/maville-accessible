import { Input } from '@/ts/composables/forms/useInput';
import { i18nInstance } from '@/ts/plugins/i18n';
import mapWithKeys from '@/ts/utilities/objects/mapWithKeys';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { HttpInvalidRequestError, HttpTooManyRequestsError } from '@foscia/http';
import { JsonApiDocument } from '@foscia/jsonapi';

export default async function handleFormError(inputs: Dictionary<Input>, error: unknown) {
  if (!(error instanceof HttpInvalidRequestError) || error instanceof HttpTooManyRequestsError) {
    return false;
  }

  const document: JsonApiDocument = await error.response.json();

  const errorsMap = mapWithKeys(
    document.errors ?? [],
    (err, _, errors: Dictionary<string[]>) => {
      const sourceKey = err.source?.pointer ?? err.source?.parameter;
      if (sourceKey) {
        const errorKey = sourceKey.split('/').pop() as string;

        return {
          [errorKey]: [
            ...(errors[errorKey] ?? []),
            err.detail || i18nInstance.t('errors.unknown.description'),
          ],
        };
      }

      return {};
    },
  );

  const handledErrors = Object.entries(errorsMap).map(([key, errors]) => {
    const input = inputs[key];
    if (input) {
      const replaceAttr = (text: string, attr: string, label: string) => text
        .replaceAll(`:data.${attr}`, `"${label}"`)
        .replaceAll(`:${attr}`, `"${label}"`);

      input.props.errorMessages = errors.map(
        (err) => replaceAttr(err, input.props.name, input.props.label),
      );

      return true;
    }

    return false;
  });

  return handledErrors.some((v) => v);
}
