import makeForbiddenError from '@/ts/errors/redirectable/makeForbiddenError';
import makeNotFoundError from '@/ts/errors/redirectable/makeNotFoundError';
import makeUnauthorizedError from '@/ts/errors/redirectable/makeUnauthorizedError';
import RedirectableError from '@/ts/errors/redirectable/redirectableError';
import shouldIgnoreError from '@/ts/errors/shouldIgnoreError';
import { i18nInstance } from '@/ts/plugins/i18n';
import router from '@/ts/router';
import { useAuthStore } from '@/ts/stores/auth';
import { useRoutingStore } from '@/ts/stores/routing';
import { useSnackbarStore } from '@/ts/stores/snackbar';
import { ExpectedRunFailureError } from '@foscia/core';
import { mdiClockOutline } from '@mdi/js';
import {
  HttpForbiddenError,
  HttpInvalidRequestError,
  HttpNotFoundError,
  HttpTooManyRequestsError,
  HttpUnauthorizedError,
} from '@foscia/http';

export default function handleError(error: unknown): void {
  const auth = useAuthStore();
  const routing = useRoutingStore();
  const snackbar = useSnackbarStore();

  if (error instanceof ErrorEvent) {
    return handleError(error.error);
  }

  if (error instanceof PromiseRejectionEvent) {
    return handleError(error.reason);
  }

  if (error instanceof HttpUnauthorizedError) {
    if (auth.auth) {
      snackbar.toast({
        message: i18nInstance.t('errors.expiredSession.toast'),
        icon: '$error',
      });
      auth.logoutUser();
    }

    return handleError(makeUnauthorizedError());
  }

  if (error instanceof HttpForbiddenError) {
    return handleError(makeForbiddenError());
  }

  if (error instanceof HttpNotFoundError || error instanceof ExpectedRunFailureError) {
    return handleError(makeNotFoundError());
  }

  if (error instanceof HttpTooManyRequestsError) {
    const currentUnix = new Date().getTime();
    const resetUnix = Number(
      error.response.headers.get('x-ratelimit-reset') ?? 60,
    ) * 1000;
    const resetSeconds = Math.ceil((resetUnix - currentUnix) / 1000);

    snackbar.toast({
      message: i18nInstance.t('errors.tooManyAttempts.description', {
        seconds: resetSeconds.toFixed(),
      }),
      icon: '$error',
    });

    return undefined;
  }

  if (
    error instanceof HttpInvalidRequestError
    && error.response.status === 419
  ) {
    routing.stopPreventLeaving();

    const reloadDelay = 3;

    snackbar.toast({
      message: i18nInstance.t('errors.expiredPage.toast', { n: reloadDelay }),
      icon: mdiClockOutline,
    });

    setTimeout(() => {
      window.location.reload();
    }, 2 * 1000);

    return undefined;
  }

  if (error instanceof RedirectableError) {
    routing.stopPreventLeaving();

    router.replace(error.redirectTo);

    return undefined;
  }

  if (shouldIgnoreError(error)) {
    return undefined;
  }

  // eslint-disable-next-line no-console
  console.error(error);

  snackbar.toast({
    message: i18nInstance.t('errors.unknown.description'),
    icon: '$error',
  });

  return undefined;
}
