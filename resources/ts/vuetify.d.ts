declare module 'vuetify/lib/util/colors' {
  import { Dictionary } from '@/ts/utilities/types/dictionary';

  const colors: Dictionary;

  export default colors;
}
