import { Awaitable } from '@/ts/utilities/types/awaitable';

export type Rule<T = any> = {
  name: string;
  validate: (value: T, ...args: any[]) => Awaitable<boolean>;
  translateFor: (label: string, value: T, ...args: any[]) => string;
};

export type VuetifyRuleResult = string | boolean;
export type VuetifyRule<T = any> =
  | ((value: T, ...args: any[]) => VuetifyRuleResult)
  | ((value: T, ...args: any[]) => PromiseLike<VuetifyRuleResult>);
