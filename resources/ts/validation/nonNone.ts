import isNone from '@/ts/utilities/common/isNone';
import { Awaitable } from '@/ts/utilities/types/awaitable';

export default function nonNone(validate: (value: any, ...args: any[]) => Awaitable<boolean>) {
  return (value: any, ...args: any[]) => isNone(value) || validate(value, ...args);
}
