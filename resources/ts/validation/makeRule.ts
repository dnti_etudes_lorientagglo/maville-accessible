import { Rule } from '@/ts/validation/rule';
import { i18nInstance } from '@/ts/plugins/i18n';
import { Awaitable } from '@/ts/utilities/types/awaitable';
import { Dictionary } from '@/ts/utilities/types/dictionary';

type TranslationResolverData = {
  rule: string;
  translation: string;
  attribute: string;
  label: string;
  value: any;
};

type TranslationResolver<R> = (data: TranslationResolverData) => R;

type RuleOptions = {
  translationKey: TranslationResolver<string>;
  translationParams: TranslationResolver<Dictionary>;
};

const defaultTranslationKey = ({ translation }: TranslationResolverData) => translation;

export default function makeRule(
  name: string,
  validate: (value: any, ...args: any[]) => Awaitable<boolean>,
  options?: Partial<RuleOptions>,
): Rule {
  return {
    name,
    validate,
    translateFor(label: string, value: unknown) {
      const data = {
        rule: name,
        translation: `validation.${name}`,
        label,
        value,
        attribute: `"${label}"`,
      };

      const translationKey = options?.translationKey || defaultTranslationKey;

      return i18nInstance.t(translationKey(data), {
        ...data,
        ...(options?.translationParams ? options.translationParams(data) : {}),
      });
    },
  };
}
