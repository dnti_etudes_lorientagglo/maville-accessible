import { makeSizeRule } from '@/ts/validation/rules/size';

export default makeSizeRule('max', (size, value) => value <= size);
