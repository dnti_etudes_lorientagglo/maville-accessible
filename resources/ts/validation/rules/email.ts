import makeRule from '@/ts/validation/makeRule';
import nonNone from '@/ts/validation/nonNone';
import EmailValidator from 'email-validator';

export default makeRule('email', nonNone((value) => EmailValidator.validate(value)));
