import normalizeColor from '@/ts/utilities/colors/normalizeColor';
import makeRule from '@/ts/validation/makeRule';
import nonNone from '@/ts/validation/nonNone';

export default makeRule('color', nonNone((value) => normalizeColor(value) !== null), {
  translationKey: () => 'validation.custom.color.invalid',
});
