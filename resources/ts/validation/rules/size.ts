import makeRule from '@/ts/validation/makeRule';
import nonNone from '@/ts/validation/nonNone';
import getValidationSize from '@/ts/validation/rules/utils/getValidationSize';
import getValidationType from '@/ts/validation/rules/utils/getValidationType';

export type SizeRuleOptions = {
  parseSize?: boolean;
  computeSize?: (value: any) => number;
  computeType?: (value: any) => string;
};

export function makeSizeRule(
  name: string,
  comparator: (size: number, value: number) => boolean,
) {
  return (
    size: any,
    options: SizeRuleOptions = {},
  ) => makeRule(
    name,
    nonNone((value) => comparator(
      typeof size !== 'number' ? (options.computeSize ?? getValidationSize)(size) : size,
      (options.computeSize ?? getValidationSize)(value),
    )),
    {
      translationKey: ({ translation, value }) => (
        `${translation}.${(options.computeType ?? getValidationType)(value)}`
      ),
      translationParams: () => ({ [name]: size }),
    },
  );
}

export default makeSizeRule('size', (size, value) => value === size);
