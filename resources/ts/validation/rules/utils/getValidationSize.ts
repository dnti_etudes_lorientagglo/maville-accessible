export default function getValidationSize(value: string | number | any[]): number {
  if (typeof value === 'string') {
    return value.length;
  }

  if (typeof value === 'number') {
    return value;
  }

  return value.length;
}
