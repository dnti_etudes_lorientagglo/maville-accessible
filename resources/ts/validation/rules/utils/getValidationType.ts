export default function getValidationType(value: string | number | any[]): string {
  if (typeof value === 'string') {
    return 'string';
  }

  if (typeof value === 'number') {
    return 'numeric';
  }

  return 'array';
}
