import makeRule from '@/ts/validation/makeRule';
import nonNone from '@/ts/validation/nonNone';
import { validate } from 'uuid';

export default makeRule('uuid', nonNone((value) => typeof value === 'string' && validate(value)));
