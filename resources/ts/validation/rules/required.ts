import makeRule from '@/ts/validation/makeRule';
import isNone from '@/ts/utilities/common/isNone';

export default makeRule('required', (value) => !isNone(value));
