import { makeSizeRule } from '@/ts/validation/rules/size';

export default makeSizeRule('min', (size, value) => value >= size);
