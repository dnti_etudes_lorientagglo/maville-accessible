import normalizeURL from '@/ts/utilities/strings/normalizeURL';
import { Arrayable } from '@/ts/utilities/types/arrayable';
import makeRule from '@/ts/validation/makeRule';
import nonNone from '@/ts/validation/nonNone';

export default (schemes?: Arrayable<string>) => makeRule('url', nonNone((value) => {
  const normalized = normalizeURL(value, schemes);

  return normalized !== null;
}));
