import { Rule, VuetifyRule } from '@/ts/validation/rule';

export default function toVuetifyRule(label: string, rule: Rule | VuetifyRule): VuetifyRule {
  if (typeof rule === 'function') {
    return rule;
  }

  return async (value: unknown, ...args: any[]) => {
    const valid = (await rule.validate(value, ...args));
    if (valid) {
      return true;
    }

    return rule.translateFor(label, value, ...args);
  };
}
