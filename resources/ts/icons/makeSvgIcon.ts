import { VueComponent } from '@/ts/utilities/components/types';
import mapWithKeys from '@/ts/utilities/objects/mapWithKeys';
import { h, useAttrs } from 'vue';

export type IconValue = string | VueComponent;

export default function makeSvgIcon(icon: string) {
  return () => {
    const parser = new DOMParser();
    const doc = parser.parseFromString(icon, 'image/svg+xml');
    const attrs = useAttrs();

    return h('svg', {
      innerHTML: doc.documentElement.innerHTML,
      class: 'v-icon__svg',
      ...attrs,
      ...mapWithKeys(Object.values(doc.documentElement.attributes), (attr) => ({
        [attr.name]: attr.value,
      })),
    });
  };
}
