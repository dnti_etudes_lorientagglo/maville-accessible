import linkedinSvg from '@/assets/icons/linkedin.svg?raw';
import makeSvgIcon from '@/ts/icons/makeSvgIcon';

export default makeSvgIcon(linkedinSvg);
