import googleSvg from '@/assets/icons/google.svg?raw';
import makeSvgIcon from '@/ts/icons/makeSvgIcon';

export default makeSvgIcon(googleSvg);
