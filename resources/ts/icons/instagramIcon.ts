import instagramSvg from '@/assets/icons/instagram.svg?raw';
import makeSvgIcon from '@/ts/icons/makeSvgIcon';

export default makeSvgIcon(instagramSvg);
