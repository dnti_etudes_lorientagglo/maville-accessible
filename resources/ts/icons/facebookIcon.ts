import facebookSvg from '@/assets/icons/facebook.svg?raw';
import makeSvgIcon from '@/ts/icons/makeSvgIcon';

export default makeSvgIcon(facebookSvg);
