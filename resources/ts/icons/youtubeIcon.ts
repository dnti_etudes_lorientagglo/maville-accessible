import youtubeSvg from '@/assets/icons/youtube.svg?raw';
import makeSvgIcon from '@/ts/icons/makeSvgIcon';

export default makeSvgIcon(youtubeSvg);
