export type ColSpec = { cols?: number; sm?: number; md?: number; lg?: number; xl?: number; };
export type ColSize = ColSpec | 'small' | 'normal' | 'n-normal' | 'large' | 'half' | 'full';

export default function useCol(size?: ColSize): ColSpec {
  if (size && typeof size === 'object') {
    return size;
  }

  return {
    full: { cols: 12 },
    half: { cols: 12, sm: 6 },
    small: { cols: 12, sm: 6, md: 3 },
    normal: { cols: 12, sm: 6, md: 4, xl: 3 },
    large: { cols: 12, md: 8, xl: 6 },
    'n-normal': { cols: 12, sm: 6, md: 8, xl: 9 },
  }[size ?? 'normal'];
}
