import { OpeningHour } from '@/ts/utilities/dates/types';
import { useI18n } from 'vue-i18n';

export default function useOpeningHourFormatter() {
  const i18n = useI18n();

  return (hour: OpeningHour) => (
    hour.start === hour.end ? hour.start : i18n.t('common.units.period', hour)
  );
}
