import { THEME_DARK_HIGH_CONTRAST } from '@/ts/plugins/vuetify/themes/darkHighContrast';
import { THEME_LIGHT_HIGH_CONTRAST } from '@/ts/plugins/vuetify/themes/lightHighContrast';
import { computed } from 'vue';
import { useTheme } from 'vuetify';

export default function useIsHighContrast() {
  const theme = useTheme();

  return computed(() => (
    theme.name.value === THEME_LIGHT_HIGH_CONTRAST
    || theme.name.value === THEME_DARK_HIGH_CONTRAST
  ));
}
