import useIsHighContrast from '@/ts/composables/themes/useIsHighContrast';
import { THEME_DARK } from '@/ts/plugins/vuetify/themes/dark';
import { THEME_DARK_HIGH_CONTRAST } from '@/ts/plugins/vuetify/themes/darkHighContrast';
import { THEME_LIGHT } from '@/ts/plugins/vuetify/themes/light';
import { THEME_LIGHT_HIGH_CONTRAST } from '@/ts/plugins/vuetify/themes/lightHighContrast';
import { computed, MaybeRef, unref } from 'vue';

export default function useThemeVariant(dark: MaybeRef<boolean>) {
  const highContrast = useIsHighContrast();

  return computed(() => {
    if (unref(dark)) {
      return highContrast.value ? THEME_DARK_HIGH_CONTRAST : THEME_DARK;
    }

    return highContrast.value ? THEME_LIGHT_HIGH_CONTRAST : THEME_LIGHT;
  });
}
