import Activity from '@/ts/api/models/activity';
import Place from '@/ts/api/models/place';
import { Optional } from '@/ts/utilities/types/optional';
import { computed, MaybeRef, unref } from 'vue';
import { useI18n } from 'vue-i18n';

export default function useClaimTitle(requestableRef: MaybeRef<Optional<Place | Activity>>) {
  const i18n = useI18n();

  return computed(() => {
    const requestable = unref(requestableRef);
    if (!requestable) {
      return '';
    }

    const type = requestable instanceof Activity
      ? requestable.activityType.code
      : requestable.$model.$type;

    return i18n.t(`requests.claim.title.${type}`);
  });
}
