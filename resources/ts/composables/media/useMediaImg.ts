import Media from '@/ts/api/models/media';
import mediaToDisplayableImg, {
  DisplayableImgOptions,
} from '@/ts/utilities/media/mediaToDisplayableImg';
import { Optional } from '@/ts/utilities/types/optional';
import { computed, MaybeRef, unref } from 'vue';

export default function useMediaImg(
  media: MaybeRef<Optional<Media>>,
  options: MaybeRef<DisplayableImgOptions> = {},
) {
  return computed(() => mediaToDisplayableImg(unref(media), unref(options)));
}
