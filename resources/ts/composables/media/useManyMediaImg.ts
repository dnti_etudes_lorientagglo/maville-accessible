import Media from '@/ts/api/models/media';
import uniqBy from '@/ts/utilities/arrays/uniqBy';
import mediaToDisplayableImg, {
  DisplayableImg,
  DisplayableImgOptions,
} from '@/ts/utilities/media/mediaToDisplayableImg';
import { Optional } from '@/ts/utilities/types/optional';
import { computed, MaybeRef, unref } from 'vue';

export default function useManyMediaImg(
  media: MaybeRef<Optional<Media>[]>,
  options: MaybeRef<DisplayableImgOptions> = {},
) {
  return computed(() => (
    uniqBy(unref(media), 'id')
      .map((m) => mediaToDisplayableImg(m, unref(options)))
      .filter((m) => m) as DisplayableImg[]
  ));
}
