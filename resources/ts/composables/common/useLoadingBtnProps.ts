import useProgressProps from '@/ts/composables/common/useProgressProps';
import { computed, MaybeRef, unref } from 'vue';

export default function useLoadingBtnProps(loading: MaybeRef<boolean>) {
  const progressProps = useProgressProps(loading);

  return computed(() => ({
    loading: unref(loading),
    ...progressProps.value,
  }));
}
