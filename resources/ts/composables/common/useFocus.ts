import { ref } from 'vue';

export default function useFocus() {
  const focused = ref(false);

  const focus = () => {
    focused.value = true;
  };

  const blur = () => {
    focused.value = false;
  };

  return { focused, focus, blur };
}
