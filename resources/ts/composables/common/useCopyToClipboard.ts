import { MaybeRef, ref, unref } from 'vue';

export default function useCopyToClipboard(text: MaybeRef<string>) {
  const copyLoading = ref(false);
  const copySuccess = ref(false);
  const copyError = ref(false);

  const onCopy = async () => {
    if (copyLoading.value) {
      return null;
    }

    copyLoading.value = true;
    copySuccess.value = false;
    copyError.value = false;

    try {
      await navigator.clipboard.writeText(unref(text));

      copySuccess.value = true;
    } catch {
      copyError.value = true;
    }

    setTimeout(() => {
      copySuccess.value = false;
      copyError.value = false;
    }, 2000);

    copyLoading.value = false;

    return copySuccess.value;
  };

  return { copyLoading, copySuccess, copyError, onCopy };
}
