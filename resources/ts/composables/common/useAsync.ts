import useLoadingBtnProps from '@/ts/composables/common/useLoadingBtnProps';
import { Awaitable } from '@/ts/utilities/types/awaitable';
import { ref } from 'vue';

type AsyncOptions = {
  currently?: boolean;
  prevent?: boolean;
};

export default function useAsync(options: AsyncOptions = {}) {
  const loading = ref(options.currently ?? false);
  const btnLoading = useLoadingBtnProps(loading);

  const asyncRun = async <T>(callback: () => Awaitable<T>) => {
    if (loading.value && options.prevent) {
      return undefined;
    }

    loading.value = true;

    try {
      return await callback();
    } finally {
      loading.value = false;
    }
  };

  return { loading, btnLoading, asyncRun };
}
