import isOpen24Hours from '@/ts/utilities/dates/hours/isOpen24Hours';
import { OpeningHour } from '@/ts/utilities/dates/types';
import { computed, MaybeRef, unref } from 'vue';

export default function useHoursProps(hours: MaybeRef<OpeningHour[]>) {
  const closed = computed(() => unref(hours).length === 0);
  const open24Hours = computed(() => (
    unref(hours).length === 1 && isOpen24Hours(unref(hours)[0])
  ));

  return { closed, open24Hours };
}
