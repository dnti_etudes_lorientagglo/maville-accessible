import { onActivated, onDeactivated, onMounted, onUnmounted, ref } from 'vue';

export default function useActive() {
  const active = ref(false);

  onMounted(() => {
    active.value = true;
  });

  onActivated(() => {
    active.value = true;
  });

  onDeactivated(() => {
    active.value = false;
  });

  onUnmounted(() => {
    active.value = false;
  });

  return active;
}
