import sizes, { SizeName } from '@/ts/utilities/common/sizes';
import { computed } from 'vue';

export default function useSize(name?: SizeName) {
  const size = computed(() => (name ? sizes[name] : undefined));

  return { size };
}
