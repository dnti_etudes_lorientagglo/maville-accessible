import useEventListener from '@/ts/composables/dom/useEventListener';
import { computed, onMounted, ref } from 'vue';

export default function useAppStyles() {
  const viewportHeight = ref(null as number | null);

  const styles = computed(() => ({
    '--viewport-height': viewportHeight.value
      ? `${viewportHeight.value}px`
      : '100vh',
  }));

  const onResize = () => {
    viewportHeight.value = document.documentElement.clientHeight;
  };

  useEventListener(window, 'resize', onResize);

  onMounted(() => {
    onResize();
  });

  return styles;
}
