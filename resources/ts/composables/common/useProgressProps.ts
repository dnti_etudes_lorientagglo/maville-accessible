import { computed, MaybeRef, unref } from 'vue';
import { useI18n } from 'vue-i18n';

export default function useProgressProps(loading: MaybeRef<boolean> = true) {
  const i18n = useI18n();

  return computed(() => {
    if (!unref(loading)) {
      return {};
    }

    return i18n.te('states.loading') ? {
      'aria-label': i18n.t('states.loading'),
      lang: undefined,
    } : {
      'aria-label': 'Chargement...',
      lang: 'fr',
    };
  });
}
