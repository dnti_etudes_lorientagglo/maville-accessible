import { ComponentPublicInstance } from 'vue';

export type Portal = ComponentPublicInstance | Element;
