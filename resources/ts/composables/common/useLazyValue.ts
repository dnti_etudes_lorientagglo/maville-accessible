import { Ref, ref, UnwrapRef, watch } from 'vue';

export default function useLazyValue<T>(valueRef: Ref<T>) {
  const lazyValue = ref(valueRef.value);

  watch(valueRef, (value) => {
    lazyValue.value = value as UnwrapRef<T>;
  });

  return lazyValue as unknown as Ref<T>;
}
