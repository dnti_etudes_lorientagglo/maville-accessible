import { Optional } from '@/ts/utilities/types/optional';
import { ModelInstance } from '@foscia/core';
import { computed, unref } from 'vue';
import { MaybeRef } from 'vue/dist/vue';

export default function useArticleRoute(
  articleRef: MaybeRef<ModelInstance>,
  instanceRef?: Optional<MaybeRef<Optional<ModelInstance>>>,
) {
  return computed(() => {
    const instance = unref(instanceRef);
    const article = unref(articleRef);

    return {
      name: instance ? 'articles.about.read' : 'articles.read',
      params: {
        ...(instance ? {
          type: instance.$model.$type,
          id: instance.slug ?? instance.id,
        } : {}),
        article: article.slug ?? article.id,
      },
    };
  });
}
