import { Optional } from '@/ts/utilities/types/optional';
import { ModelInstance } from '@foscia/core';
import { computed, unref } from 'vue';
import { MaybeRef } from 'vue/dist/vue';

export default function useArticlesRoute(
  instanceRef: Optional<MaybeRef<Optional<ModelInstance>>>,
) {
  return computed(() => {
    const instance = unref(instanceRef);

    return {
      name: instance ? 'articles.about' : 'articles',
      params: instance ? {
        type: instance.$model.$type,
        id: instance.slug ?? instance.id,
      } : {},
    };
  });
}
