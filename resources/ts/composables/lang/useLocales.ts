import env from '@/ts/env';
import easyReadLocale from '@/ts/utilities/lang/easyReadLocale';
import hasEasyReadLocale from '@/ts/utilities/lang/hasEasyReadLocale';
import mapWithKeys from '@/ts/utilities/objects/mapWithKeys';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { useI18n } from 'vue-i18n';

export type LocaleObject = {
  code: string;
  shortName: string;
  longName: string;
};

export default function useLocales() {
  const i18n = useI18n();

  const easyReadName = (name: string, key: string) => i18n.t(
    `lang.easyReadOf.${key}`,
    { locale: name },
  );

  return mapWithKeys(env.services.i18n.locales, (name, locale) => {
    const newLocales: Dictionary<LocaleObject> = {
      [locale]: { code: locale, shortName: name, longName: name },
    };
    if (hasEasyReadLocale(locale)) {
      newLocales[easyReadLocale(locale)] = {
        code: easyReadLocale(locale),
        shortName: easyReadName(name, 'short'),
        longName: easyReadName(name, 'long'),
      };
    }

    return newLocales;
  });
}
