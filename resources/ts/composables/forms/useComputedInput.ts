import { Input } from '@/ts/composables/forms/useInput';
import { ref, Ref, unref } from 'vue';

export default function useComputedInput<N extends string = string, T = any>(
  name: N,
  computedRef: Ref<T>,
): Input<N, T, T> {
  return {
    inputRef: ref(null),
    valueRef: computedRef,
    get value() {
      return unref(computedRef);
    },
    set value(_: T) {
      // Ignoring value setter because of computed.
    },
    useRules: () => {
      // Ignoring rules setter because of computed.
    },
    props: {
      name,
      ref() {
      },
      get modelValue() {
        return unref(computedRef);
      },
      set modelValue(_: T) {
        // Ignoring model value setter because of computed.
      },
      label: name,
      errorMessages: [],
    },
  };
}
