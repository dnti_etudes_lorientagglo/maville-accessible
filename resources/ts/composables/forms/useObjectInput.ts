import { FormInputValues } from '@/ts/composables/forms/useForm';
import useInput, { Input, InputOptions } from '@/ts/composables/forms/useInput';
import get from '@/ts/utilities/objects/get';
import mapWithKeys from '@/ts/utilities/objects/mapWithKeys';
import { Optional } from '@/ts/utilities/types/optional';
import { watch } from 'vue';

export default function useObjectInput<
  I extends readonly Input[],
  N extends string,
  T extends Optional<Partial<FormInputValues<I>>>,
>(props: InputOptions<N, T>, inputs: I) {
  const mainInput = useInput(props);

  watch(mainInput.valueRef, (values) => {
    inputs.forEach((input) => {
      // eslint-disable-next-line no-param-reassign
      input.value = get(values, input.props.name, input.value);
    });
  });

  watch(inputs.map((input) => input.valueRef), () => {
    mainInput.value = mapWithKeys(inputs, (input) => ({
      [input.props.name]: input.value,
    })) as T;
  });

  return mainInput;
}
