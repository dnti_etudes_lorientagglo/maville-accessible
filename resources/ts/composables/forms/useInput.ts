import useLocales from '@/ts/composables/lang/useLocales';
import { useLangStore } from '@/ts/stores/lang';
import wrap from '@/ts/utilities/arrays/wrap';
import equals from '@/ts/utilities/common/equals';
import translate, { Translation } from '@/ts/utilities/lang/translate';
import toTrim from '@/ts/utilities/strings/toTrim';
import { Arrayable } from '@/ts/utilities/types/arrayable';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { Optional } from '@/ts/utilities/types/optional';
import { Rule, VuetifyRule, VuetifyRuleResult } from '@/ts/validation/rule';
import required from '@/ts/validation/rules/required';
import toVuetifyRule from '@/ts/validation/toVuetifyRule';
import {
  ComponentPublicInstance,
  reactive,
  Ref,
  ref,
  ShallowRef,
  shallowRef,
  UnwrapRef,
  watch,
} from 'vue';
import { useI18n } from 'vue-i18n';

export type InputProps<N extends string = string, U = any> = {
  ref: (instance: any) => void;
  modelValue: U;
  name: N;
  label: string;
  errorMessages: string[];
  [key: string]: any;
};

export type Input<N extends string = string, T = any, U = any> = {
  inputRef: ShallowRef<ComponentPublicInstance | null>;
  valueRef: Ref<T>;
  value: T;
  useRules: (rules?: Arrayable<Rule<U> | VuetifyRule<U>>) => void;
  props: InputProps<N, U>;
};

type InputTransformer<T> = (value: T) => T;

type InputPrepare<N extends string, T, U> = (input: Input<N, T, U>) => void;

export type InputOptions<N extends string, T, U = T> = {
  name: N;
  value: T;
  modelValue?: U;
  toValue?: (context: { value: T; modelValue: U }) => T;
  toModelValue?: (context: { value: T; modelValue: U }) => U;
  label?: string;
  translatable?: boolean;
  rules?: Arrayable<Rule<T> | VuetifyRule<T>>;
  transforms?: Arrayable<InputTransformer<U>>;
  [key: string]: unknown;
};

function makeTransformer<N extends string, T, U>(
  props: InputProps<UnwrapRef<N>, UnwrapRef<U>>,
  options: InputOptions<N, T, U>,
) {
  const wrappedTransforms = wrap(options.transforms);
  if (typeof props.modelValue === 'string') {
    wrappedTransforms.unshift(toTrim as unknown as InputTransformer<U>);
  }

  if (wrappedTransforms.length) {
    return (value: U) => wrappedTransforms
      .reduce((prev, transformer) => transformer(prev), value) as U;
  }

  return (value: U) => value;
}

function makeRulesConverter<N extends string, T, U = T>(
  props: InputProps<UnwrapRef<N>, UnwrapRef<U>>,
  options: InputOptions<N, T, U>,
) {
  const i18n = useI18n();
  const lang = useLangStore();
  const locales = useLocales();

  return (
    valueRef: Ref<UnwrapRef<T>>,
    rules: Optional<Arrayable<Rule<T> | VuetifyRule<T>>>,
  ) => wrap(rules).map((rule) => {
    const vuetifyRule = toVuetifyRule(props.label, rule);
    if (options.translatable) {
      return async (_: any, ...args: any[]) => {
        const translation = (valueRef.value ?? {}) as Translation<any>;
        const messages = {} as Translation<VuetifyRuleResult>;

        await Promise.all(Object.keys(locales).map(async (locale) => {
          messages[locale] = await vuetifyRule(translation[locale], ...args);
        }));

        if (rule === required && Object.values(messages).some((v) => v === true)) {
          return true;
        }

        if (messages[lang.editionLocale] !== true) {
          return messages[lang.editionLocale];
        }

        const firstMessageLocale = Object.keys(messages).find(
          (locale) => typeof messages[locale] === 'string',
        );

        return firstMessageLocale
          ? i18n.t('validation.custom.translation.messageFor', {
            message: messages[firstMessageLocale],
            locale: locales[firstMessageLocale].shortName,
          })
          : true;
      };
    }

    return (_: any, ...args: any[]) => vuetifyRule(valueRef.value, ...args);
  });
}

export default function useInput<N extends string, T, U = T>(options: InputOptions<N, T, U>) {
  const {
    value,
    modelValue,
    toValue,
    toModelValue,
    name,
    label,
    translatable,
    rules,
    transforms,
    ...otherOptions
  } = options;

  const inputRef = shallowRef<ComponentPublicInstance | null>(null);
  const valueRef = ref(value);

  const prepares: InputPrepare<N, T, U>[] = [];
  const otherProps: Dictionary = {};

  Object.entries(otherOptions).forEach(([key, option]) => {
    if (key.startsWith('prepare')) {
      prepares.push(option as InputPrepare<N, T, U>);
    } else {
      otherProps[key] = option;
    }
  });

  const props = reactive({
    ref: (instance: any) => {
      inputRef.value = instance;
    },
    name,
    modelValue: modelValue ?? (
      toModelValue ? toModelValue({ value, modelValue: undefined as U }) : value
    ),
    label: label ?? translate(name),
    errorMessages: [],
    size: undefined,
    required: undefined,
    'data-required': undefined,
    ...otherProps,
  } as InputProps<N, U>);

  const rulesConverter = makeRulesConverter(props, options);
  const transformer = makeTransformer(props, options);

  const useRules = (newRules?: Arrayable<Rule<T> | VuetifyRule<T>>) => {
    const isRequired = wrap(newRules).indexOf(required) !== -1;

    props['data-required'] = isRequired ? true : undefined;
    props.required = isRequired ? true : undefined;

    props.rules = rulesConverter(valueRef as Ref<UnwrapRef<T>>, newRules);
  };

  useRules(rules);

  const input = {
    inputRef,
    valueRef,
    get value() {
      return valueRef.value as T;
    },
    set value(newValue: T) {
      valueRef.value = newValue as UnwrapRef<T>;
    },
    useRules,
    props,
  } as Input<N, T, U>;

  input.props['onUpdate:modelValue'] = (newValue: U) => {
    input.props.modelValue = newValue;
  };

  const transform = () => {
    input.props.modelValue = transformer(input.props.modelValue);
  };

  input.props.onBlur = () => transform();
  input.props.onKeydown = (event: KeyboardEvent) => {
    const { target } = event;
    if (
      event.key === 'Enter'
      && (target instanceof HTMLInputElement || target instanceof HTMLTextAreaElement)
    ) {
      if (target instanceof HTMLTextAreaElement && !(event.ctrlKey || event.metaKey)) {
        return;
      }

      transform();
      if (target.form) {
        event.preventDefault();
        target.form.requestSubmit();
      }
    }
  };

  watch(() => input.props.modelValue, (nextValue) => {
    const nextInputValue = toValue
      ? toValue({ value: input.value, modelValue: nextValue })
      : nextValue as unknown as T;
    if (equals(nextInputValue, input.value)) {
      return;
    }

    input.value = nextInputValue;
  }, { deep: true });

  watch(input.valueRef, (nextValue) => {
    const nextModelValue = toModelValue
      ? toModelValue({ value: nextValue, modelValue: input.props.modelValue })
      : nextValue as unknown as U;
    if (equals(nextModelValue, input.props.modelValue)) {
      return;
    }

    input.props.modelValue = nextModelValue;
  }, { deep: true });

  prepares.forEach((p) => p(input));

  return input;
}
