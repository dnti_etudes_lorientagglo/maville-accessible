import wrap from '@/ts/utilities/arrays/wrap';
import { computed, MaybeRef, unref } from 'vue';

export default function useIsSelected<T>(value: MaybeRef<T | T[]>) {
  return computed(() => (valueToCheck: T) => wrap(unref(value)).indexOf(valueToCheck) !== -1);
}
