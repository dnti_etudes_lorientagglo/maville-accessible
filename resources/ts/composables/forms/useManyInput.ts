import { Input } from '@/ts/composables/forms/useInput';
import { ValueRetriever } from '@/ts/utilities/objects/get';
import { Awaitable } from '@/ts/utilities/types/awaitable';
import { Dictionary } from '@/ts/utilities/types/dictionary';

export type ManyInputOptions<I = any> = {
  addLabel?: string;
  noneLabel?: string;
  itemTitle?: ValueRetriever<I>;
  itemSubtitle?: ValueRetriever<I>;
  itemKey?: ValueRetriever<I>;
  itemFactory?: (attrs: Dictionary, index: number) => Awaitable<I>;
  itemUpdater?: (item: I, attrs: Dictionary, index: number) => Awaitable<void>;
  canCreate?: () => boolean;
  canUpdate?: (item: I) => boolean;
  canDelete?: (item: I) => boolean;
  formInputs?: (item: I | null, items: I[]) => Dictionary<Input>;
  formBoot?: (item: I | null, items: I[]) => Awaitable<void>;
  formValues?: (item: I) => Awaitable<Dictionary>;
  ordered?: boolean;
};

export default function useManyInput<I = any>(options: ManyInputOptions<I>) {
  return { ...options };
}
