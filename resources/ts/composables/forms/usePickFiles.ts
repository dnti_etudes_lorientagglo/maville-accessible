import { Awaitable } from '@/ts/utilities/types/awaitable';
import { computed, MaybeRef, onBeforeUnmount, onMounted, ref, unref } from 'vue';

export type FilePickerOptions = {
  loading?: MaybeRef<boolean>;
  accept?: string;
  multiple?: boolean;
  onInput?: (files: File[]) => Awaitable<void>;
};

export default function usePickFiles(options: FilePickerOptions = {}) {
  const inputRef = ref<HTMLInputElement | null>(null);
  const draggedCount = ref(0);

  const dragging = computed(() => draggedCount.value > 0);

  const onInput = (files: File[]) => {
    if (unref(options.loading) || files.length === 0) {
      return;
    }

    options.onInput?.(files);
  };

  const ensureHasDropFiles = (event: DragEvent) => (
    event.dataTransfer?.items
      ? Array.from(event.dataTransfer.items).filter((item) => item.kind === 'file').length > 0
      : (event.dataTransfer?.files.length ?? 0) > 0
  );

  const resolveDropFiles = (event: DragEvent) => (
    event.dataTransfer?.items
      ? Array.from(event.dataTransfer.items)
        .map((item) => item.getAsFile())
        .filter((file) => !!file) as File[]
      : Array.from(event.dataTransfer?.files ?? [])
  );

  const onDragenter = (event: DragEvent) => {
    event.preventDefault();
    if (ensureHasDropFiles(event)) {
      draggedCount.value += 1;
    }
  };

  const onDragover = (event: DragEvent) => {
    event.preventDefault();
  };

  const onDragleave = (event: DragEvent) => {
    event.preventDefault();
    if (ensureHasDropFiles(event)) {
      draggedCount.value -= 1;
    }
  };

  const onDrop = (event: DragEvent) => {
    event.preventDefault();
    if (ensureHasDropFiles(event)) {
      draggedCount.value = 0;

      onInput(resolveDropFiles(event));
    }
  };

  const onDragAndDrop = {
    onDragenter,
    onDragleave,
    onDragover,
    onDrop,
  };

  const onPaste = (event: Event) => {
    const { files } = (event as ClipboardEvent).clipboardData || (window as any).clipboardData;

    onInput(Array.from(files ?? []));
  };

  const onChoose = () => {
    if (!inputRef.value) {
      return;
    }

    inputRef.value.onchange = (e) => {
      const { files } = (e.target as HTMLInputElement);

      onInput(Array.from(files ?? []));
      if (inputRef.value) {
        inputRef.value.onchange = null;
      }
    };

    inputRef.value.click();
  };

  onMounted(() => {
    const input = document.createElement('input');
    input.type = 'file';
    input.hidden = true;
    input.multiple = options.multiple ?? false;
    if (options.accept) {
      input.accept = options.accept;
    }

    document.body.appendChild(input);

    inputRef.value = input;

    window.addEventListener('paste', onPaste);
  });

  onBeforeUnmount(() => {
    inputRef.value?.remove();

    window.removeEventListener('paste', onPaste);
  });

  return {
    dragging,
    onChoose,
    onDragAndDrop,
  };
}
