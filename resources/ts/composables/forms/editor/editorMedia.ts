import Media from '@/ts/api/models/media';
import { computed, inject, MaybeRef, provide, Ref, ref, unref } from 'vue';

const EDITOR_MEDIA_PROVIDE_KEY = Symbol('editor media provide key');

export function provideEditorMedia(media: Ref<Media[]>) {
  provide(EDITOR_MEDIA_PROVIDE_KEY, media);
}

export function injectEditorMedia() {
  return inject(EDITOR_MEDIA_PROVIDE_KEY, ref([] as Media[]));
}

export function useOneEditorMedia(media: Ref<Media[]>, mediaId: MaybeRef<string>) {
  return computed(() => media.value.find(({ id }) => id === unref(mediaId)));
}
