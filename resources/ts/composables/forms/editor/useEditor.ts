import TTHeading from '@/ts/plugins/tiptap/extensions/ttHeading';
import TTLink from '@/ts/plugins/tiptap/extensions/ttLink';
import TTMediaImage from '@/ts/plugins/tiptap/extensions/ttMediaImage';
import isNil from '@/ts/utilities/common/isNil';
import { AnyExtension, EditorOptions } from '@tiptap/core';
import Focus from '@tiptap/extension-focus';
import StarterKit from '@tiptap/starter-kit';
import { useEditor as useBaseEditor } from '@tiptap/vue-3';

export type HeadingLevel = 1 | 2 | 3 | 4 | 5 | 6;

export type CustomEditorOptions = {
  noImage?: boolean;
  noList?: boolean;
  noLink?: boolean;
  noHeading?: boolean;
};

export default function useEditor(
  options: CustomEditorOptions = {},
  editorOptions: Partial<EditorOptions> = {},
) {
  const editable = editorOptions.editable ?? true;
  const extensions = [
    options.noImage ? null : TTMediaImage,
    options.noHeading ? null : TTHeading.configure({
      HTMLAttributes: { class: 'app-heading' },
      renderedLevels: [1, 2, 3, 4, 5, 6],
      levels: [1, 2, 3],
    }),
    options.noLink ? null : TTLink.configure({
      openOnClick: false,
      autolink: false,
      linkOnPaste: false,
      protocols: [{
        scheme: 'mailto',
        optionalSlashes: true,
      }, {
        scheme: 'tel',
        optionalSlashes: true,
      }],
      enhance: !editable,
      HTMLAttributes: {
        target: null,
      },
    }),
    Focus,
    StarterKit.configure({
      bulletList: options.noList ? false : undefined,
      orderedList: options.noList ? false : undefined,
      listItem: options.noList ? false : undefined,
      heading: false,
      horizontalRule: false,
      codeBlock: false,
      code: false,
      strike: false,
      blockquote: false,
    }),
  ].filter((e) => !isNil(e)) as AnyExtension[];

  return useBaseEditor({
    extensions,
    ...editorOptions,
  });
}
