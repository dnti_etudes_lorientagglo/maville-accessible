import isNone from '@/ts/utilities/common/isNone';
import formatStandardDate from '@/ts/utilities/dates/formatStandardDate';
import parseStandardDate from '@/ts/utilities/dates/parseStandardDate';

export type NumberInputOptions = {
  value?: Date | null;
};

export default function useDateInput(options?: NumberInputOptions) {
  return {
    value: (options?.value ?? null) as Date | null,
    toValue: (
      { modelValue }: { modelValue: string; },
    ): Date | null => parseStandardDate(modelValue) ?? null,
    toModelValue: (
      { modelValue, value }: { modelValue: string; value: Date | null; },
    ) => (
      (isNone(modelValue) ? formatStandardDate(value) : modelValue) ?? ''
    ),
  };
}
