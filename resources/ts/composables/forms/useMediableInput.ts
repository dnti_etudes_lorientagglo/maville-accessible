import Media from '@/ts/api/models/media';
import { ref } from 'vue';

export default function useMediableInput() {
  const media = ref([] as Media[]);

  const onUpdateMedia = (newMedia: Media[]) => {
    media.value = newMedia;
  };

  return {
    media,
    'onUpdate:media': onUpdateMedia,
  };
}
