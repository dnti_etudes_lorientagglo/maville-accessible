import isNone from '@/ts/utilities/common/isNone';

export type NumberInputOptions = {
  value?: number;
  parser?: (value: string) => number;
};

export default function useNumberInput(options?: NumberInputOptions) {
  const parse = options?.parser ?? ((value: string) => Number.parseInt(value, 10));

  return {
    value: (options?.value ?? null) as number | null,
    toValue: (
      { modelValue }: { value: number | null; modelValue: string; },
    ): number | null => {
      const nextValue = parse(modelValue);

      return Number.isNaN(nextValue) ? null : nextValue;
    },
    toModelValue: (
      { value }: { value: number | null; },
    ) => (isNone(value) ? '' : value.toString()),
  };
}
