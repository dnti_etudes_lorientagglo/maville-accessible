import { Input } from '@/ts/composables/forms/useInput';
import { useLangStore } from '@/ts/stores/lang';
import { Translation } from '@/ts/utilities/lang/translate';
import excludeNone from '@/ts/utilities/objects/excludeNone';
import { watch } from 'vue';

export default function useTranslatableInput() {
  const lang = useLangStore();

  return {
    translatable: true,
    value: {} as Translation | null,
    toValue: (
      { value, modelValue }: { value: Translation | null; modelValue: string; },
    ): Translation => {
      const nextValue = { ...value, [lang.editionLocale]: modelValue };
      if (Object.keys(excludeNone(nextValue)).length === 0) {
        return null as any;
      }

      return nextValue;
    },
    toModelValue: (
      { value }: { value: Translation | null; },
    ) => (value ?? {})[lang.editionLocale] ?? '',
    prepareTranslatableInput: (input: Input<any, Translation | null, string>) => {
      watch(() => lang.editionLocale, () => {
        // eslint-disable-next-line no-param-reassign
        input.props.modelValue = (input.value ?? {})[lang.editionLocale] ?? '';
      });
    },
  };
}
