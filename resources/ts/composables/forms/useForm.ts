import useAsync from '@/ts/composables/common/useAsync';
import { Input } from '@/ts/composables/forms/useInput';
import handleFormError from '@/ts/errors/handleFormError';
import { useRoutingStore } from '@/ts/stores/routing';
import wrap from '@/ts/utilities/arrays/wrap';
import equals from '@/ts/utilities/common/equals';
import isNil from '@/ts/utilities/common/isNil';
import scrollTo from '@/ts/utilities/dom/scrollTo';
import mapValues from '@/ts/utilities/objects/mapValues';
import mapWithKeys from '@/ts/utilities/objects/mapWithKeys';
import { Arrayable } from '@/ts/utilities/types/arrayable';
import { Awaitable } from '@/ts/utilities/types/awaitable';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { v4 as uuidV4 } from 'uuid';
import { computed, nextTick, onMounted, onUnmounted, ref, shallowRef, UnwrapRef, watch } from 'vue';
import { SubmitEventPromise } from 'vuetify';
import { VForm } from 'vuetify/components/VForm';

export type FormInputMap<Inputs extends readonly Input[]> = {
  [I in Extract<keyof Inputs, `${number}`> as Inputs[I]['props']['name']]: Inputs[I];
};

export type FormInputValues<Inputs extends readonly Input[]> = {
  [I in Extract<keyof Inputs, `${number}`> as Inputs[I]['props']['name']]: UnwrapRef<Inputs[I]['value']>;
};

export type FormEvent<Inputs extends readonly Input[]> = {
  values: Partial<FormInputValues<Inputs>>;
  allValues: FormInputValues<Inputs>;
};

export type FormOptions<Inputs extends readonly Input[]> = {
  preventLeaving?: boolean;
  boot?: () => Awaitable<void>;
  unmount?: () => Awaitable<void>;
  onValidate?: () => Awaitable<boolean>;
  onChange?: (event: FormEvent<Inputs>) => void;
  onSubmit?: (event: FormEvent<Inputs>) => Awaitable<void>;
  onSuccess?: () => Awaitable<void>;
  onFailure?: (error: unknown) => Awaitable<boolean | void>;
};

export default function useForm<Inputs extends readonly Input[]>(
  inputs: Inputs,
  options: FormOptions<Inputs> = {},
) {
  const id = uuidV4();
  const routing = useRoutingStore();

  const inputsMap = mapWithKeys(inputs, (input) => ({ [input.props.name]: input }));

  const formRef = ref(null as VForm | null);
  const booting = ref(true);
  const errors = ref({} as Dictionary<string[]>);
  const { loading, btnLoading, asyncRun } = useAsync();

  const errorsCount = computed(
    () => Object.values(errors.value).filter((m) => m && m.length > 0).length,
  );
  const values = computed(() => mapWithKeys(inputsMap, (input, name) => ({
    [name]: input.value,
  })) as FormInputValues<Inputs>);

  const defaults = shallowRef({ ...values.value });
  const original = shallowRef({ ...defaults.value });

  const changedValues = computed(() => mapWithKeys(values.value, (value, name) => (
    equals(value, original.value[name as keyof FormInputValues<Inputs>]) ? {} : { [name]: value }
  )));

  const changed = computed(() => Object.keys(changedValues.value).length > 0);

  const updatePreventLeaving = () => {
    routing.preventLeaving(id, options.preventLeaving === true && changed.value);
  };

  watch(values, () => {
    updatePreventLeaving();

    options.onChange?.({
      values: changedValues.value,
      allValues: values.value,
    } as FormEvent<Inputs>);
  }, { deep: true });

  watch(original, () => {
    updatePreventLeaving();
  }, { deep: true });

  const fill = (valuesToUse: Partial<FormInputValues<Inputs>>) => {
    mapValues(valuesToUse, (value, key) => {
      const input = inputsMap[key];
      if (input) {
        input.value = value;
      }
    });
  };

  const resetUsing = (
    valuesToUse: Partial<FormInputValues<Inputs>>,
    keys?: Arrayable<keyof FormInputMap<Inputs>>,
  ) => {
    (isNil(keys) ? Object.keys(valuesToUse) : wrap(keys))
      .forEach((name) => {
        inputsMap[name].value = valuesToUse[name as keyof FormInputValues<Inputs>];
      });
  };

  const reset = (keys?: Arrayable<keyof FormInputMap<Inputs>>) => {
    resetUsing(original.value as any, keys);
  };

  const resetDefaults = (keys?: Arrayable<keyof FormInputMap<Inputs>>) => {
    resetUsing(defaults.value as any, keys);
  };

  const resetCustomValidation = () => {
    Object.values(inputsMap).forEach((input) => {
      // eslint-disable-next-line no-param-reassign
      input.props.errorMessages = [];
    });
  };

  const updateErrors = () => {
    const messagesMap = {} as Dictionary<string[]>;
    const appendMessages = (inputId: string, inputMessages: string[]) => {
      messagesMap[inputId] = [...(messagesMap[inputId] ?? []), ...inputMessages];
    };

    Object.values(inputsMap).forEach((input) => {
      appendMessages(input.props.name, input.props.errorMessages);
    });

    const form = formRef.value;
    if (form) {
      form.errors.forEach((bag) => {
        appendMessages(String(bag.id), bag.errorMessages);
      });
    }

    errors.value = messagesMap;
  };

  const onSubmit = async (event?: SubmitEventPromise) => {
    event?.preventDefault();

    if (loading.value) {
      return;
    }

    await asyncRun(async () => {
      const submitCallback = options.onSubmit;
      if (!submitCallback) {
        return;
      }

      resetCustomValidation();

      const { valid } = event ? await event : { valid: true };
      if (!valid || (options.onValidate && !await options.onValidate())) {
        loading.value = false;

        updateErrors();

        const firstErroredInput = document.querySelector('.v-input.v-input--error');
        if (firstErroredInput) {
          scrollTo(firstErroredInput as HTMLElement, 124);
        }

        return;
      }

      resetCustomValidation();
      errors.value = {};

      await nextTick();

      const currentOriginal = original.value;
      const currentChangedValues = changedValues.value;
      const currentValues = values.value;

      try {
        original.value = { ...currentValues };

        await submitCallback({
          values: currentChangedValues,
          allValues: currentValues,
        } as FormEvent<Inputs>);

        await options.onSuccess?.();
      } catch (error) {
        original.value = { ...currentOriginal };

        const ignore = await options.onFailure?.(error);
        if (ignore) {
          return;
        }

        if (!await handleFormError(inputsMap, error)) {
          throw error;
        }
      }
    });
  };

  const boot = async () => {
    booting.value = true;

    await options.boot?.();

    defaults.value = { ...values.value };
    original.value = { ...defaults.value };

    booting.value = false;
  };

  onMounted(async () => {
    await boot();
  });

  onUnmounted(async () => {
    await options.unmount?.();

    resetDefaults();
  });

  return {
    id,
    formRef,
    booting,
    loading,
    btnLoading,
    changed,
    inputs: inputsMap as FormInputMap<Inputs>,
    values,
    errors,
    errorsCount,
    boot,
    fill,
    reset,
    resetDefaults,
    onSubmit,
    asyncRun,
  };
}
