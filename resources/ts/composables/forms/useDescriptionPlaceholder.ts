import { Input } from '@/ts/composables/forms/useInput';
import isNone from '@/ts/utilities/common/isNone';
import stripHtml from '@/ts/utilities/strings/stripHtml';
import truncate from '@/ts/utilities/strings/truncate';
import { watch } from 'vue';

export default function useDescriptionPlaceholder(
  bodyInput: Input<string, unknown, string>,
  descriptionInput: Input<string, unknown, string>,
) {
  watch(() => bodyInput.props.modelValue, (body) => {
    const placeholder = truncate(stripHtml(body ?? ''));

    /* eslint-disable no-param-reassign */
    descriptionInput.props.placeholder = placeholder;
    descriptionInput.props.persistentPlaceholder = !isNone(placeholder);
    /* eslint-enable */
  });
}
