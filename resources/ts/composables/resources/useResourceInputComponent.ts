import VCategoryInput from '@/ts/components/forms/instances/VCategoryInput.vue';
import VInstancesInput from '@/ts/components/forms/instances/VInstancesInput.vue';
import VOrganizationInput from '@/ts/components/forms/instances/VOrganizationInput.vue';
import VPlaceInput from '@/ts/components/forms/instances/VPlaceInput.vue';
import useResourceMeta from '@/ts/composables/resources/core/useResourceMeta';
import useResourceType from '@/ts/composables/resources/core/useResourceType';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { ModelClass, ModelInstance } from '@foscia/core';
import { computed, ComputedRef, DefineComponent } from 'vue';

type ResourceComponentDef = {
  Component: DefineComponent<any, any, any>,
  props: Dictionary,
};

export default function useResourceInputComponent(
  model: ModelClass,
): ComputedRef<ResourceComponentDef> {
  const type = useResourceType(model.$type);
  const meta = useResourceMeta(type);

  return computed(() => {
    if (type === 'categories') {
      return {
        Component: VCategoryInput,
        props: {},
      } as ResourceComponentDef;
    }

    if (type === 'organizations') {
      return {
        Component: VOrganizationInput,
        props: {},
      } as ResourceComponentDef;
    }

    if (type === 'places') {
      return {
        Component: VPlaceInput,
        props: {},
      } as ResourceComponentDef;
    }

    return {
      Component: VInstancesInput,
      props: {
        model,
        itemTitle: meta.title,
        itemValue: (instance: ModelInstance) => instance.id,
      },
    } as ResourceComponentDef;
  });
}
