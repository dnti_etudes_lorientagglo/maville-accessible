import makeTableHeader from '@/ts/components/data/makeTableHeader';
import useCommonHeaders from '@/ts/composables/resources/composables/core/useCommonHeaders';
import translate from '@/ts/utilities/lang/translate';

export default function useReportTypeHeaders() {
  return [
    makeTableHeader('name', { sortable: true }),
    makeTableHeader('global', { label: translate('resources.reportTypes.labels.global') }),
    ...useCommonHeaders(),
  ];
}
