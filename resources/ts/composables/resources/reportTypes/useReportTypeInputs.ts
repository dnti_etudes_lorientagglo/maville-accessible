import useInput from '@/ts/composables/forms/useInput';
import useTranslatableInput from '@/ts/composables/forms/useTranslatableInput';
import max from '@/ts/validation/rules/max';
import required from '@/ts/validation/rules/required';
import { useI18n } from 'vue-i18n';

export default function useReportTypeInputs() {
  const i18n = useI18n();

  const nameInput = useInput({
    ...useTranslatableInput(),
    name: 'name',
    rules: [required, max(50)],
  });

  const globalInput = useInput({
    name: 'global',
    label: i18n.t('resources.reportTypes.labels.global'),
    hint: i18n.t('resources.reportTypes.hints.global'),
    persistentHint: true,
    value: false,
  });

  const inputs = [nameInput, globalInput] as const;

  return {
    nameInput,
    globalInput,
    inputs,
  };
}
