import makeTableHeader from '@/ts/components/data/makeTableHeader';
import useAccessibleHeaders
  from '@/ts/composables/resources/composables/accessible/useAccessibleHeaders';
import useCategorizableHeaders
  from '@/ts/composables/resources/composables/categorizable/useCategorizableHeaders';
import useContactableHeaders
  from '@/ts/composables/resources/composables/contactable/useContactableHeaders';
import useCommonHeaders from '@/ts/composables/resources/composables/core/useCommonHeaders';
import useCoverableHeaders
  from '@/ts/composables/resources/composables/coverable/useCoverableHeaders';
import useImageableHeaders
  from '@/ts/composables/resources/composables/imageable/useImageableHeaders';
import useLinkableHeaders from '@/ts/composables/resources/composables/linkable/useLinkableHeaders';
import useOwnableHeaders from '@/ts/composables/resources/composables/ownable/useOwnableHeaders';
import usePublishableHeaders
  from '@/ts/composables/resources/composables/publishable/usePublishableHeaders';
import useSourceableHeaders
  from '@/ts/composables/resources/composables/sourceable/useSourceableHeaders';

export default function usePlaceHeaders() {
  return [
    ...useCoverableHeaders(),
    ...useImageableHeaders(),
    makeTableHeader('name', { sortable: true }),
    makeTableHeader('address'),
    ...useCategorizableHeaders(),
    makeTableHeader('description'),
    ...useAccessibleHeaders(),
    makeTableHeader('openingHours'),
    ...useContactableHeaders(),
    ...useLinkableHeaders(),
    ...useOwnableHeaders(),
    ...useCommonHeaders(),
    ...usePublishableHeaders(),
    ...useSourceableHeaders(),
  ];
}
