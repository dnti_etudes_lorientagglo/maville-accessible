import Place from '@/ts/api/models/place';
import useInput from '@/ts/composables/forms/useInput';
import useTranslatableInput from '@/ts/composables/forms/useTranslatableInput';
import useAccessibleInputs
  from '@/ts/composables/resources/composables/accessible/useAccessibleInputs';
import useCategorizableInputs
  from '@/ts/composables/resources/composables/categorizable/useCategorizableInputs';
import useContactableInputs
  from '@/ts/composables/resources/composables/contactable/useContactableInputs';
import useCoverableInputs
  from '@/ts/composables/resources/composables/coverable/useCoverableInputs';
import useImageableInputs
  from '@/ts/composables/resources/composables/imageable/useImageableInputs';
import useLinkableInputs from '@/ts/composables/resources/composables/linkable/useLinkableInputs';
import useOwnableInputs from '@/ts/composables/resources/composables/ownable/useOwnableInputs';
import { WeekdayOpeningHour } from '@/ts/utilities/dates/types';
import { AddressObject } from '@/ts/utilities/places/types';
import { Optional } from '@/ts/utilities/types/optional';
import max from '@/ts/validation/rules/max';
import required from '@/ts/validation/rules/required';

export default function usePlaceInputs() {
  const nameInput = useInput({
    ...useTranslatableInput(),
    name: 'name',
    rules: [required, max(100)],
  });

  const descriptionInput = useInput({
    ...useTranslatableInput(),
    name: 'description',
    rules: [max(255)],
  });

  const addressInput = useInput({
    name: 'address',
    value: null as Optional<AddressObject>,
    rules: [required],
  });

  const openingHoursInput = useInput({
    name: 'openingHours',
    value: [] as WeekdayOpeningHour[],
  });

  const { accessibilityInput } = useAccessibleInputs();
  const { coverInput } = useCoverableInputs();
  const { imagesInput } = useImageableInputs();
  const { ownerOrganizationInput, bootOwnableInputs } = useOwnableInputs();
  const { categoriesInput, bootCategorizableInputs } = useCategorizableInputs({
    rules: [required],
  }, ownerOrganizationInput);
  const { emailInput, phoneInput } = useContactableInputs();
  const { linksInput } = useLinkableInputs();

  const boot = async (instance: Place) => {
    await bootOwnableInputs(instance);
    await bootCategorizableInputs();
  };

  return {
    coverInput,
    imagesInput,
    nameInput,
    descriptionInput,
    addressInput,
    openingHoursInput,
    categoriesInput,
    accessibilityInput,
    emailInput,
    phoneInput,
    linksInput,
    ownerOrganizationInput,
    boot,
  };
}
