import Article from '@/ts/api/models/article';
import useDescriptionPlaceholder from '@/ts/composables/forms/useDescriptionPlaceholder';
import useInput from '@/ts/composables/forms/useInput';
import useMediableInput from '@/ts/composables/forms/useMediableInput';
import useTranslatableInput from '@/ts/composables/forms/useTranslatableInput';
import useCategorizableInputs
  from '@/ts/composables/resources/composables/categorizable/useCategorizableInputs';
import useCoverableInputs
  from '@/ts/composables/resources/composables/coverable/useCoverableInputs';
import useImageableInputs
  from '@/ts/composables/resources/composables/imageable/useImageableInputs';
import useLocalizableInputs
  from '@/ts/composables/resources/composables/localizable/useLocalizableInputs';
import useMediableInputs from '@/ts/composables/resources/composables/mediable/useMediableInputs';
import useOwnableInputs from '@/ts/composables/resources/composables/ownable/useOwnableInputs';
import translate from '@/ts/utilities/lang/translate';
import max from '@/ts/validation/rules/max';
import required from '@/ts/validation/rules/required';

export default function useArticleInputs() {
  const nameInput = useInput({
    ...useTranslatableInput(),
    name: 'name',
    label: translate('title'),
    rules: [required, max(100)],
  });

  const bodyInput = useInput({
    ...useMediableInput(),
    ...useTranslatableInput(),
    name: 'body',
    rules: [required, max(50000)],
  });

  const descriptionInput = useInput({
    ...useTranslatableInput(),
    name: 'description',
    rules: [max(255)],
  });

  const { coverInput } = useCoverableInputs({ rules: [required] });
  const { imagesInput } = useImageableInputs();
  const { placesInput } = useLocalizableInputs();
  const { ownerOrganizationInput, bootOwnableInputs } = useOwnableInputs();
  const { categoriesInput, bootCategorizableInputs } = useCategorizableInputs(
    {},
    ownerOrganizationInput,
  );
  const { mediaInput, bootMediableInputs } = useMediableInputs([bodyInput]);

  const boot = async (instance: Article) => {
    await bootMediableInputs(instance);
    await bootOwnableInputs(instance);
    await bootCategorizableInputs();
  };

  useDescriptionPlaceholder(bodyInput, descriptionInput);

  return {
    coverInput,
    imagesInput,
    nameInput,
    bodyInput,
    descriptionInput,
    categoriesInput,
    ownerOrganizationInput,
    placesInput,
    mediaInput,
    boot,
  };
}
