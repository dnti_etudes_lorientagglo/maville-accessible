import makeTableHeader from '@/ts/components/data/makeTableHeader';
import useCategorizableHeaders
  from '@/ts/composables/resources/composables/categorizable/useCategorizableHeaders';
import useCommonHeaders from '@/ts/composables/resources/composables/core/useCommonHeaders';
import useCoverableHeaders
  from '@/ts/composables/resources/composables/coverable/useCoverableHeaders';
import useImageableHeaders
  from '@/ts/composables/resources/composables/imageable/useImageableHeaders';
import useLocalizableHeaders
  from '@/ts/composables/resources/composables/localizable/useLocalizableHeaders';
import useOwnableHeaders from '@/ts/composables/resources/composables/ownable/useOwnableHeaders';
import usePublishableHeaders
  from '@/ts/composables/resources/composables/publishable/usePublishableHeaders';
import useSourceableHeaders
  from '@/ts/composables/resources/composables/sourceable/useSourceableHeaders';
import translate from '@/ts/utilities/lang/translate';

export default function useArticleHeaders() {
  return [
    ...useCoverableHeaders(),
    ...useImageableHeaders(),
    makeTableHeader('name', { label: translate('title'), sortable: true }),
    ...useCategorizableHeaders(),
    makeTableHeader('description'),
    makeTableHeader('body'),
    ...useLocalizableHeaders(),
    ...useOwnableHeaders(),
    ...useCommonHeaders(),
    ...usePublishableHeaders(),
    ...useSourceableHeaders(),
  ];
}
