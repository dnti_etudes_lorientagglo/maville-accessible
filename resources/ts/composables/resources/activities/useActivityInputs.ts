import ActivityPublicCode from '@/ts/api/enums/activityPublicCode';
import ActivityTypeCode from '@/ts/api/enums/activityTypeCode';
import Activity from '@/ts/api/models/activity';
import Place from '@/ts/api/models/place';
import useDateInput from '@/ts/composables/forms/useDateInput';
import useDescriptionPlaceholder from '@/ts/composables/forms/useDescriptionPlaceholder';
import useInput from '@/ts/composables/forms/useInput';
import useManyInput from '@/ts/composables/forms/useManyInput';
import useMediableInput from '@/ts/composables/forms/useMediableInput';
import useTranslatableInput from '@/ts/composables/forms/useTranslatableInput';
import useAccessibleInputs
  from '@/ts/composables/resources/composables/accessible/useAccessibleInputs';
import useCategorizableInputs
  from '@/ts/composables/resources/composables/categorizable/useCategorizableInputs';
import useContactableInputs
  from '@/ts/composables/resources/composables/contactable/useContactableInputs';
import useCoverableInputs
  from '@/ts/composables/resources/composables/coverable/useCoverableInputs';
import useImageableInputs
  from '@/ts/composables/resources/composables/imageable/useImageableInputs';
import useLinkableInputs from '@/ts/composables/resources/composables/linkable/useLinkableInputs';
import useMediableInputs from '@/ts/composables/resources/composables/mediable/useMediableInputs';
import useOwnableInputs from '@/ts/composables/resources/composables/ownable/useOwnableInputs';
import { useLangStore } from '@/ts/stores/lang';
import isNil from '@/ts/utilities/common/isNil';
import datesOpeningHoursToGroup from '@/ts/utilities/dates/datesOpeningHoursToGroup';
import formatDate from '@/ts/utilities/dates/formatDate';
import {
  DateOpeningHour,
  DateOpeningHourGroup,
  OpeningHour,
  OpeningPeriod,
  WeekdayOpeningHour,
} from '@/ts/utilities/dates/types';
import translate from '@/ts/utilities/lang/translate';
import { AddressObject } from '@/ts/utilities/places/types';
import { Optional } from '@/ts/utilities/types/optional';
import max from '@/ts/validation/rules/max';
import required from '@/ts/validation/rules/required';
import { useI18n } from 'vue-i18n';

export default function useActivityInputs(activityType: ActivityTypeCode) {
  const i18n = useI18n();
  const lang = useLangStore();

  const nameInput = useInput({
    ...useTranslatableInput(),
    name: 'name',
    rules: [required, max(100)],
  });

  const bodyInput = useInput({
    ...useMediableInput(),
    ...useTranslatableInput(),
    name: 'body',
    rules: [max(50000)],
  });

  const descriptionInput = useInput({
    ...useTranslatableInput(),
    name: 'description',
    rules: [max(255)],
  });

  const openingOnPeriodsInput = useInput({
    name: 'openingOnPeriods',
    label: i18n.t('resources.activities.labels.openingOnPeriods'),
    value: ActivityTypeCode.ACTIVITIES.is(activityType),
  });

  const openingPeriodsInput = useInput({
    name: 'openingPeriods',
    label: i18n.t('resources.activities.labels.openingPeriods.label'),
    value: [] as OpeningPeriod[] | null,
    rules: [
      (dates) => !(dates ?? []).some((d, i) => (dates ?? []).some((sd, si) => (
        i !== si && (
          (d.start >= sd.start && d.start <= sd.end) || (d.end >= sd.start && d.end <= sd.end)
        )
      ))) || i18n.t('resources.activities.errors.periodsOverlap'),
    ],
    toValue: ({ modelValue }: { modelValue: OpeningPeriod[] | null }) => [...(modelValue ?? [])]
      .map((p) => ({ ...p })),
    toModelValue: ({ value }: { value: OpeningPeriod[] | null }) => [...(value ?? [])]
      .map((p) => ({ ...p })),
    ...useManyInput<OpeningPeriod>({
      addLabel: i18n.t('resources.activities.labels.openingPeriods.add'),
      noneLabel: i18n.t('resources.activities.labels.openingPeriods.none'),
      itemTitle: (item) => i18n.t('resources.activities.states.period', {
        start: formatDate(item.start, lang.locale),
        end: formatDate(item.end, lang.locale),
      }),
      formInputs: () => ({
        startInput: useInput({
          ...useDateInput(),
          name: 'start',
          label: i18n.t('resources.activities.labels.openingPeriods.start'),
          rules: [required],
        }),
        endInput: useInput({
          ...useDateInput(),
          name: 'end',
          label: i18n.t('resources.activities.labels.openingPeriods.end'),
          rules: [required],
        }),
      }),
    }),
  });

  const openingDatesHoursInput = useInput({
    name: 'openingDatesHours',
    label: i18n.t('resources.activities.labels.openingDatesHours.label'),
    value: [] as DateOpeningHour[] | null,
    rules: [
      required,
      max(15),
    ],
    toValue: (
      { modelValue }: { modelValue: DateOpeningHourGroup[]; },
    ) => modelValue.reduce((values, { date, hours }) => [
      ...values,
      ...hours.map((hour) => ({ date, start: hour.start, end: hour.end })),
    ], [] as DateOpeningHour[]),
    toModelValue: (
      { value }: { value: DateOpeningHour[] | null; },
    ) => datesOpeningHoursToGroup(value),
    ...useManyInput<DateOpeningHourGroup>({
      addLabel: i18n.t('resources.activities.labels.openingDatesHours.add'),
      noneLabel: i18n.t('resources.activities.labels.openingDatesHours.none'),
      itemTitle: (item) => i18n.t('resources.activities.states.date', {
        date: formatDate(item.date, lang.locale),
      }),
      formInputs: () => ({
        dateInput: useInput({
          ...useDateInput(),
          name: 'date',
          rules: [required],
        }),
        hoursInput: useInput({
          name: 'hours',
          label: translate('openingHours'),
          value: [{ start: '00:00', end: '00:00' }] as OpeningHour[],
          rules: [required],
        }),
      }),
    }),
  });

  const openingWeekdaysHoursInput = useInput({
    name: 'openingWeekdaysHours',
    label: i18n.t('resources.activities.labels.openingWeekdaysHours'),
    value: [] as WeekdayOpeningHour[],
  });

  const pricingCodeInput = useInput({
    name: 'pricingCode',
    label: i18n.t('resources.activities.labels.pricingCode'),
    value: '' as string | null,
    toModelValue: ({ value }: { value: string | null }) => (
      isNil(value) ? '' : value
    ),
  });

  const pricingDescriptionInput = useInput({
    ...useTranslatableInput(),
    name: 'pricingDescription',
    label: i18n.t('resources.activities.labels.pricingDescription'),
    rules: [max(500)],
  });

  const publicCodesInput = useInput({
    name: 'publicCodes',
    label: i18n.t('resources.activities.labels.publics'),
    rules: [required],
    value: [ActivityPublicCode.ALL.value] as string[] | null,
    toModelValue: ({ value }: { value: string[] | null }) => (
      isNil(value) || value.length === 0 ? [ActivityPublicCode.ALL.value] : value
    ),
  });

  const placeInput = useInput({
    name: 'place',
    label: i18n.t('resources.activities.labels.place'),
    value: null as Optional<Place>,
    rules: [required],
  });

  const addressInput = useInput({
    name: 'address',
    value: null as Optional<AddressObject>,
    rules: [required],
  });

  const { accessibilityInput } = useAccessibleInputs();
  const { coverInput } = useCoverableInputs({ rules: [required] });
  const { imagesInput } = useImageableInputs();
  const { ownerOrganizationInput, bootOwnableInputs } = useOwnableInputs();
  const { categoriesInput, bootCategorizableInputs } = useCategorizableInputs(
    {},
    ownerOrganizationInput,
  );
  const { emailInput, phoneInput } = useContactableInputs();
  const { linksInput } = useLinkableInputs();
  const { mediaInput, bootMediableInputs } = useMediableInputs([bodyInput]);

  const boot = async (instance: Activity) => {
    await bootMediableInputs(instance);
    await bootOwnableInputs(instance);
    await bootCategorizableInputs();
  };

  useDescriptionPlaceholder(bodyInput, descriptionInput);

  return {
    coverInput,
    imagesInput,
    nameInput,
    bodyInput,
    descriptionInput,
    placeInput,
    addressInput,
    openingOnPeriodsInput,
    openingPeriodsInput,
    openingDatesHoursInput,
    openingWeekdaysHoursInput,
    pricingCodeInput,
    pricingDescriptionInput,
    publicCodesInput,
    categoriesInput,
    accessibilityInput,
    emailInput,
    phoneInput,
    linksInput,
    ownerOrganizationInput,
    mediaInput,
    boot,
  };
}
