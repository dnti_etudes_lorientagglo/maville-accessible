import Activity from '@/ts/api/models/activity';
import makeTableHeader from '@/ts/components/data/makeTableHeader';
import useAccessibleHeaders
  from '@/ts/composables/resources/composables/accessible/useAccessibleHeaders';
import useCategorizableHeaders
  from '@/ts/composables/resources/composables/categorizable/useCategorizableHeaders';
import useContactableHeaders
  from '@/ts/composables/resources/composables/contactable/useContactableHeaders';
import useCommonHeaders from '@/ts/composables/resources/composables/core/useCommonHeaders';
import useCoverableHeaders
  from '@/ts/composables/resources/composables/coverable/useCoverableHeaders';
import useImageableHeaders
  from '@/ts/composables/resources/composables/imageable/useImageableHeaders';
import useLinkableHeaders from '@/ts/composables/resources/composables/linkable/useLinkableHeaders';
import useOwnableHeaders from '@/ts/composables/resources/composables/ownable/useOwnableHeaders';
import usePublishableHeaders
  from '@/ts/composables/resources/composables/publishable/usePublishableHeaders';
import useSourceableHeaders
  from '@/ts/composables/resources/composables/sourceable/useSourceableHeaders';
import { useI18n } from 'vue-i18n';

export default function useActivityHeaders() {
  const i18n = useI18n();

  return [
    ...useCoverableHeaders(),
    ...useImageableHeaders(),
    makeTableHeader('name', { sortable: true }),
    ...useCategorizableHeaders(),
    makeTableHeader('place', {
      label: i18n.t('resources.activities.labels.place'),
    }),
    makeTableHeader('openingPeriods', {
      label: i18n.t('resources.activities.labels.openingPeriods.label'),
      when: (a: Activity) => a.openingOnPeriods,
    }),
    makeTableHeader('openingWeekdaysHours', {
      label: i18n.t('resources.activities.labels.openingWeekdaysHours'),
      when: (a: Activity) => a.openingOnPeriods,
    }),
    makeTableHeader('openingDatesHours', {
      label: i18n.t('resources.activities.labels.openingDatesHours.label'),
      when: (a: Activity) => !a.openingOnPeriods,
    }),
    makeTableHeader('publics', {
      label: i18n.t('resources.activities.labels.publics'),
    }),
    makeTableHeader('pricing', {
      label: i18n.t('resources.activities.labels.pricing'),
    }),
    makeTableHeader('description'),
    makeTableHeader('body'),
    ...useAccessibleHeaders(),
    ...useContactableHeaders(),
    ...useLinkableHeaders(),
    ...useOwnableHeaders(),
    ...useCommonHeaders(),
    ...usePublishableHeaders(),
    ...useSourceableHeaders(),
  ];
}
