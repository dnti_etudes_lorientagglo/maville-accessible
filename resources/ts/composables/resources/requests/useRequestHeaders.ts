import RequestTypeCode from '@/ts/api/enums/requestTypeCode';
import Request from '@/ts/api/models/request';
import makeTableHeader from '@/ts/components/data/makeTableHeader';
import useCommonHeaders from '@/ts/composables/resources/composables/core/useCommonHeaders';
import useUserOwnableHeaders
  from '@/ts/composables/resources/composables/userOwnable/useUserOwnableHeaders';
import translate from '@/ts/utilities/lang/translate';

export default function useRequestHeaders() {
  return [
    makeTableHeader('status', { label: translate('resources.requests.labels.status.label') }),
    makeTableHeader('requestType', { label: translate('resources.requests.labels.requestType') }),
    makeTableHeader('requestable', {
      label: translate('resources.requests.labels.claim.requestable'),
      when: (request: Request) => RequestTypeCode.CLAIM.match(request.requestType.code),
    }),
    makeTableHeader('requestable', {
      label: translate('resources.requests.labels.join.requestable'),
      when: (request: Request) => RequestTypeCode.JOIN.match(request.requestType.code),
    }),
    makeTableHeader('resultable', {
      label: translate('resources.requests.labels.claim.resultable'),
      when: (request: Request) => RequestTypeCode.CLAIM.match(request.requestType.code),
    }),
    makeTableHeader('phone'),
    makeTableHeader('comment'),
    ...useUserOwnableHeaders(),
    ...useCommonHeaders(),
  ];
}
