import Organization, { OrganizationAdministrativeData } from '@/ts/api/models/organization';
import OrganizationInvitation from '@/ts/api/models/organizationInvitation';
import OrganizationMember from '@/ts/api/models/organizationMember';
import OrganizationType from '@/ts/api/models/organizationType';
import Role from '@/ts/api/models/role';
import User from '@/ts/api/models/user';
import useComputedInput from '@/ts/composables/forms/useComputedInput';
import useDescriptionPlaceholder from '@/ts/composables/forms/useDescriptionPlaceholder';
import useInput, { Input } from '@/ts/composables/forms/useInput';
import useManyInput from '@/ts/composables/forms/useManyInput';
import useMediableInput from '@/ts/composables/forms/useMediableInput';
import useObjectInput from '@/ts/composables/forms/useObjectInput';
import useTranslatableInput from '@/ts/composables/forms/useTranslatableInput';
import useCategorizableInputs
  from '@/ts/composables/resources/composables/categorizable/useCategorizableInputs';
import useContactableInputs
  from '@/ts/composables/resources/composables/contactable/useContactableInputs';
import useCoverableInputs
  from '@/ts/composables/resources/composables/coverable/useCoverableInputs';
import useLinkableInputs from '@/ts/composables/resources/composables/linkable/useLinkableInputs';
import useMediableInputs from '@/ts/composables/resources/composables/mediable/useMediableInputs';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';
import { Optional } from '@/ts/utilities/types/optional';
import makeRule from '@/ts/validation/makeRule';
import max from '@/ts/validation/rules/max';
import required from '@/ts/validation/rules/required';
import size from '@/ts/validation/rules/size';
import { fill } from '@foscia/core';
import { computed } from 'vue';
import { useI18n } from 'vue-i18n';

export default function useOrganizationInputs() {
  const i18n = useI18n();

  const nameInput = useInput({
    ...useTranslatableInput(),
    name: 'name',
    rules: [required, max(100)],
  });

  const organizationTypeInput = useInput({
    name: 'organizationType',
    label: i18n.t('resources.organizations.labels.organizationType'),
    value: null as OrganizationType | null,
    rules: [required],
  });

  const canAddMembers = hasPermissionOn('organization-members.create');

  const membersInput: Input<'members', (OrganizationMember | OrganizationInvitation)[]> = useInput({
    name: 'members',
    label: i18n.t('resources.organizations.labels.members.label'),
    value: [] as (OrganizationMember | OrganizationInvitation)[],
    ...useManyInput<OrganizationMember | OrganizationInvitation>({
      addLabel: canAddMembers
        ? i18n.t('resources.organizations.labels.members.add')
        : i18n.t('resources.organizations.labels.members.invite'),
      noneLabel: i18n.t('resources.organizations.labels.members.none'),
      itemKey: (item) => (
        item instanceof OrganizationMember ? item.user.id : item.email
      ),
      itemTitle: (item) => (
        item instanceof OrganizationMember ? item.user.fullName : item.email
      ),
      itemFactory: (attrs) => (
        canAddMembers
          ? fill(new OrganizationMember(), attrs)
          : fill(new OrganizationInvitation(), attrs)
      ),
      itemUpdater: (item, attrs) => {
        fill(item, attrs);
      },
      canCreate: () => (
        hasPermissionOn('organization-members.create')
        || hasPermissionOn('organization-invitations.create')
      ),
      canUpdate: (item) => (item.$exists ? hasPermissionOn('update', item) : true),
      canDelete: (item) => (item.$exists ? hasPermissionOn('delete', item) : true),
      formInputs: () => ({
        ...(canAddMembers ? {
          userInput: useInput({
            name: 'user',
            value: null as User | null,
            rules: [required],
          }),
        } : {
          emailInput: useInput({
            name: 'email',
            value: '',
            rules: [
              required,
              makeRule('alreadyInvited', (value) => !membersInput.value.some((item) => (
                item instanceof OrganizationMember
                  ? item.user.email === value
                  : item.email === value
              )), {
                translationKey: () => 'resources.organizations.errors.alreadyInvited',
              }),
            ],
          }),
        }),
        roleInput: useInput({
          name: 'role',
          value: null as Role | null,
          rules: [required],
        }),
      }),
    }),
  });

  const membersOfOrganizationInput = useComputedInput('membersOfOrganization', computed(() => (
    membersInput.value.filter((m) => m instanceof OrganizationMember)
  )));

  const invitationsInput = useComputedInput('invitations', computed(() => (
    membersInput.value.filter((m) => m instanceof OrganizationInvitation)
  )));

  const bodyInput = useInput({
    ...useMediableInput(),
    ...useTranslatableInput(),
    name: 'body',
    rules: [max(50000)],
  });

  const descriptionInput = useInput({
    ...useTranslatableInput(),
    name: 'description',
    rules: [max(255)],
  });

  const siretIdentifierInput = useInput({
    name: 'siretIdentifier',
    label: i18n.t('resources.organizations.labels.siretIdentifier'),
    value: '',
    rules: [size(14)],
  });

  const rnaIdentifierInput = useInput({
    name: 'rnaIdentifier',
    label: i18n.t('resources.organizations.labels.rnaIdentifier'),
    value: '',
    rules: [size(10)],
  });

  const administrativeDataInput = useObjectInput({
    name: 'administrativeData',
    label: i18n.t('resources.organizations.labels.administrativeData'),
    value: null as Optional<OrganizationAdministrativeData>,
  }, [siretIdentifierInput, rnaIdentifierInput]);

  const { coverInput } = useCoverableInputs();
  const { categoriesInput, bootCategorizableInputs } = useCategorizableInputs({
    rules: [required],
  });
  const { emailInput, phoneInput } = useContactableInputs();
  const { linksInput } = useLinkableInputs();
  const { mediaInput, bootMediableInputs } = useMediableInputs([bodyInput]);

  const boot = async (organization: Organization) => {
    await bootMediableInputs(organization);
    await bootCategorizableInputs();

    membersInput.value = [
      ...(organization.membersOfOrganization ?? []),
      ...(organization.invitations ?? []),
    ];
  };

  useDescriptionPlaceholder(bodyInput, descriptionInput);

  return {
    coverInput,
    nameInput,
    organizationTypeInput,
    membersInput,
    membersOfOrganizationInput,
    invitationsInput,
    bodyInput,
    descriptionInput,
    categoriesInput,
    emailInput,
    phoneInput,
    linksInput,
    siretIdentifierInput,
    rnaIdentifierInput,
    administrativeDataInput,
    mediaInput,
    boot,
  };
}
