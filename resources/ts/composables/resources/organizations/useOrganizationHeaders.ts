import makeTableHeader from '@/ts/components/data/makeTableHeader';
import useCategorizableHeaders
  from '@/ts/composables/resources/composables/categorizable/useCategorizableHeaders';
import useContactableHeaders
  from '@/ts/composables/resources/composables/contactable/useContactableHeaders';
import useCommonHeaders from '@/ts/composables/resources/composables/core/useCommonHeaders';
import useCoverableHeaders
  from '@/ts/composables/resources/composables/coverable/useCoverableHeaders';
import useLinkableHeaders from '@/ts/composables/resources/composables/linkable/useLinkableHeaders';
import usePublishableHeaders
  from '@/ts/composables/resources/composables/publishable/usePublishableHeaders';
import useSourceableHeaders
  from '@/ts/composables/resources/composables/sourceable/useSourceableHeaders';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';
import translate from '@/ts/utilities/lang/translate';

export function useOrganizationInformationHeaders() {
  return [
    ...useCoverableHeaders(),
    makeTableHeader('name', { sortable: true }),
    makeTableHeader('organizationType', { label: translate('resources.organizations.labels.organizationType') }),
    makeTableHeader('members', {
      label: translate('resources.organizations.labels.members.label'),
      when: () => hasPermissionOn('organization-members.showAny'),
    }),
    ...useCategorizableHeaders(),
    makeTableHeader('description'),
    makeTableHeader('body'),
    ...useContactableHeaders(),
    ...useLinkableHeaders(),
    ...useCommonHeaders(),
    ...usePublishableHeaders(),
    ...useSourceableHeaders(),
  ];
}

export function useOrganizationAdministrativeHeaders() {
  return [
    makeTableHeader('siretIdentifier', { label: translate('resources.organizations.labels.siretIdentifier') }),
    makeTableHeader('rnaIdentifier', { label: translate('resources.organizations.labels.rnaIdentifier') }),
  ];
}

export default function useOrganizationHeaders() {
  return [
    ...useOrganizationInformationHeaders(),
    ...useOrganizationAdministrativeHeaders(),
  ];
}
