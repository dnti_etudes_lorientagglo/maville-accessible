import Organization from '@/ts/api/models/organization';
import saveWithRelations from '@/ts/api/utilities/saveWithRelations';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { fill } from '@foscia/core';

export default function useOrganizationSave() {
  return async (organization: Organization, values: Dictionary) => {
    fill(organization, values);

    await saveWithRelations(organization, ['membersOfOrganization', 'invitations'], {
      prepareRelated: ({ instance, related }) => {
        if (!related.$exists) {
          fill(related, { organization: instance });
        }
      },
    });

    return organization;
  };
}
