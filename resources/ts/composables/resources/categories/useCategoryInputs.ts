import action from '@/ts/api/action';
import Category from '@/ts/api/models/category';
import first from '@/ts/api/runners/first';
import useInput from '@/ts/composables/forms/useInput';
import useTranslatableInput from '@/ts/composables/forms/useTranslatableInput';
import normalizeColor from '@/ts/utilities/colors/normalizeColor';
import isNone from '@/ts/utilities/common/isNone';
import color from '@/ts/validation/rules/color';
import max from '@/ts/validation/rules/max';
import required from '@/ts/validation/rules/required';
import { query, when } from '@foscia/core';
import { filterBy } from '@foscia/jsonapi';
import { useI18n } from 'vue-i18n';

export default function useCategoryInputs() {
  const i18n = useI18n();

  const nameInput = useInput({
    ...useTranslatableInput(),
    name: 'name',
    rules: [required, max(50)],
  });

  const colorInput = useInput({
    name: 'color',
    value: null as string | null,
    rules: [color],
    transforms: [(v) => normalizeColor(v) ?? v],
  });

  const iconInput = useInput({
    name: 'icon',
    value: null as string | null,
  });

  const descriptionInput = useInput({
    ...useTranslatableInput(),
    name: 'description',
    rules: [max(255)],
  });

  const parentsInput = useInput({
    name: 'parents',
    label: i18n.t('resources.categories.labels.parents'),
    value: [] as Category[],
    rules: [required],
  });

  const childrenInput = useInput({
    name: 'children',
    label: i18n.t('resources.categories.labels.children'),
    value: [] as Category[],
  });

  const boot = async (category: Category) => {
    nameInput.useRules([required, max(50), async (value) => {
      if (isNone(value)) {
        return true;
      }

      const matchingCategory = await action()
        .use(query(Category))
        .use(when(category.$exists, filterBy('notId', [category.id])))
        .use(filterBy('searchName', value))
        .run(first());

      return !matchingCategory || i18n.t('resources.categories.validation.unique');
    }]);
  };

  return {
    nameInput,
    parentsInput,
    childrenInput,
    colorInput,
    iconInput,
    descriptionInput,
    boot,
  };
}
