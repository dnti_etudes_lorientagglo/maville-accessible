import Category from '@/ts/api/models/category';
import makeTableHeader from '@/ts/components/data/makeTableHeader';
import useCommonHeaders from '@/ts/composables/resources/composables/core/useCommonHeaders';
import usePublishableHeaders
  from '@/ts/composables/resources/composables/publishable/usePublishableHeaders';
import useSourceableHeaders
  from '@/ts/composables/resources/composables/sourceable/useSourceableHeaders';
import translate from '@/ts/utilities/lang/translate';

export default function useCategoryHeaders() {
  return [
    makeTableHeader('name', { sortable: true }),
    makeTableHeader('description'),
    makeTableHeader('childOf', {
      label: translate('resources.categories.labels.childOf'),
      when: (category: Category) => category.parents.length > 0,
    }),
    makeTableHeader('children', {
      label: translate('resources.categories.labels.children'),
      when: (category: Category) => category.children.length > 0,
    }),
    ...useCommonHeaders(),
    ...usePublishableHeaders(),
    ...useSourceableHeaders(),
  ];
}
