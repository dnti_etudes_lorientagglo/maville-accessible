import makeTableHeader from '@/ts/components/data/makeTableHeader';
import useCommonHeaders from '@/ts/composables/resources/composables/core/useCommonHeaders';
import { useI18n } from 'vue-i18n';

export default function useSendingCampaignHeaders() {
  const i18n = useI18n();

  return [
    makeTableHeader('name', { sortable: true }),
    makeTableHeader('campaignType', { label: i18n.t('resources.sendingCampaigns.labels.campaignType') }),
    makeTableHeader('status', { label: i18n.t('resources.sendingCampaigns.labels.status') }),
    makeTableHeader('summary', { label: i18n.t('resources.sendingCampaigns.labels.summary.label') }),
    ...useCommonHeaders(),
    makeTableHeader('startedAt', { label: i18n.t('resources.sendingCampaigns.labels.startedAt') }),
    makeTableHeader('finishedAt', { label: i18n.t('resources.sendingCampaigns.labels.finishedAt') }),
  ];
}
