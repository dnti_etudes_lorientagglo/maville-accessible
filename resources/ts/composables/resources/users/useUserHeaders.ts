import makeTableHeader from '@/ts/components/data/makeTableHeader';
import useCommonHeaders from '@/ts/composables/resources/composables/core/useCommonHeaders';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';
import translate from '@/ts/utilities/lang/translate';

export default function useUserHeaders() {
  return [
    makeTableHeader('fullName', { sortable: true }),
    makeTableHeader('email', { sortable: true }),
    makeTableHeader('roles', {
      when: () => hasPermissionOn('roles.assignAny'),
    }),
    makeTableHeader('memberOfOrganizations', {
      label: translate('resources.users.labels.memberOfOrganizations'),
    }),
    ...useCommonHeaders(),
    makeTableHeader('lastLoginAt', {
      label: translate('resources.users.labels.lastLoginAt'),
      sortable: true,
    }),
    makeTableHeader('emailVerifiedAt', {
      label: translate('resources.users.labels.emailVerifiedAt'),
      sortable: true,
    }),
    makeTableHeader('blockedAt', {
      label: translate('resources.users.labels.blockedAt'),
      sortable: true,
    }),
    makeTableHeader('scheduledDeletionAt', {
      label: translate('resources.users.labels.scheduledDeletionAt'),
      sortable: true,
    }),
  ];
}
