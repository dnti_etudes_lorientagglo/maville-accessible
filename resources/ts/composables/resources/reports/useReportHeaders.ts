import makeTableHeader from '@/ts/components/data/makeTableHeader';
import useCommonHeaders from '@/ts/composables/resources/composables/core/useCommonHeaders';
import useSoftDeletableHeaders
  from '@/ts/composables/resources/composables/softDeletable/useSoftDeletableHeaders';
import useUserOwnableHeaders
  from '@/ts/composables/resources/composables/userOwnable/useUserOwnableHeaders';
import translate from '@/ts/utilities/lang/translate';

export default function useReportHeaders() {
  return [
    makeTableHeader('reportType', { label: translate('resources.reports.labels.reportType') }),
    makeTableHeader('comment'),
    ...useUserOwnableHeaders(),
    ...useCommonHeaders(),
    ...useSoftDeletableHeaders(),
  ];
}
