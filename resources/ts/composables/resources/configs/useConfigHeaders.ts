import makeTableHeader from '@/ts/components/data/makeTableHeader';
import useCommonHeaders from '@/ts/composables/resources/composables/core/useCommonHeaders';
import translate from '@/ts/utilities/lang/translate';

export function useConfigInformationHeaders() {
  return [
    makeTableHeader('description'),
    ...useCommonHeaders(),
  ];
}

export function useConfigMapHeaders() {
  return [
    makeTableHeader('mapCenter', { label: translate('resources.configs.labels.mapCenter') }),
    makeTableHeader('mapZoom', { label: translate('resources.configs.labels.mapZoom') }),
    makeTableHeader('mapSearchableCategories', { label: translate('resources.configs.labels.mapSearchableCategories') }),
    makeTableHeader('mapDefaultCategories', { label: translate('resources.configs.labels.mapDefaultCategories') }),
  ];
}

export default function useConfigHeaders() {
  return [
    ...useConfigInformationHeaders(),
    ...useConfigMapHeaders(),
  ];
}
