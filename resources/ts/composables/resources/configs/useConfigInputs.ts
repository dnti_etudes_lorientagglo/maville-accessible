import Category from '@/ts/api/models/category';
import useInput from '@/ts/composables/forms/useInput';
import useTranslatableInput from '@/ts/composables/forms/useTranslatableInput';
import { AddressObject } from '@/ts/utilities/places/types';
import max from '@/ts/validation/rules/max';
import { useI18n } from 'vue-i18n';

export default function useConfigInputs() {
  const i18n = useI18n();

  const descriptionInput = useInput({
    ...useTranslatableInput(),
    name: 'description',
    rules: [max(255)],
  });

  const mapCenterInput = useInput({
    name: 'mapCenter',
    label: i18n.t('resources.configs.labels.mapCenter'),
    value: null as AddressObject | null,
  });

  const mapZoomInput = useInput({
    name: 'mapZoom',
    label: i18n.t('resources.configs.labels.mapZoom'),
    value: null as number | null,
  });

  const mapSearchableCategoriesInput = useInput({
    name: 'mapSearchableCategories',
    label: i18n.t('resources.configs.labels.mapSearchableCategories'),
    value: [] as Category[],
    rules: [max(8)],
  });

  const mapDefaultCategories = useInput({
    name: 'mapDefaultCategories',
    label: i18n.t('resources.configs.labels.mapDefaultCategories'),
    value: [] as Category[],
  });

  return {
    descriptionInput,
    mapCenterInput,
    mapZoomInput,
    mapSearchableCategoriesInput,
    mapDefaultCategories,
  };
}
