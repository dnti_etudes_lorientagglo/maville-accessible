import makeTableHeader from '@/ts/components/data/makeTableHeader';

export default function useLinkableHeaders() {
  return [
    makeTableHeader('links'),
  ];
}
