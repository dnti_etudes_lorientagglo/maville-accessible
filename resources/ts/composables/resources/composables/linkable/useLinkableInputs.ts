import { LinkableLinks } from '@/ts/api/composables/linkable';
import useInput from '@/ts/composables/forms/useInput';

export default function useLinkableInputs() {
  const linksInput = useInput({
    name: 'links',
    value: {} as LinkableLinks,
  });

  return {
    linksInput,
  };
}
