import makeTableHeader from '@/ts/components/data/makeTableHeader';

export default function usePublishableHeaders() {
  return [
    makeTableHeader('publishedAt', { sortable: true }),
  ];
}
