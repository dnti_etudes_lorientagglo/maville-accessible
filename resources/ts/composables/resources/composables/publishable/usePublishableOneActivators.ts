import { Publishable } from '@/ts/api/composables/publishable';
import PublishActivator
  from '@/ts/components/resources/composables/publishable/PublishActivator.vue';
import UnPublishActivator
  from '@/ts/components/resources/composables/publishable/UnPublishActivator.vue';
import { OneActivatorsContext } from '@/ts/composables/resources/composables/core/types';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';

export default function usePublishableOneActivators() {
  return ({ instance, onSingle }: OneActivatorsContext<Publishable>) => {
    const activators = [];

    if (onSingle && hasPermissionOn('publish', instance)) {
      activators.push(PublishActivator);
    }

    if (onSingle && hasPermissionOn('unPublish', instance)) {
      activators.push(UnPublishActivator);
    }

    return activators;
  };
}
