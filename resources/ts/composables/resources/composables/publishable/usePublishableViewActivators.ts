import { Publishable } from '@/ts/api/composables/publishable';
import isPublishedAvailable from '@/ts/api/utilities/publishable/isPublishedAvailable';
import ViewPublishableActivator
  from '@/ts/components/resources/composables/publishable/ViewPublishableActivator.vue';
import { OneActivatorsContext } from '@/ts/composables/resources/composables/core/types';

export default function usePublishableViewActivators() {
  return ({ instance, onSingle }: OneActivatorsContext<Publishable>) => {
    const activators = [];

    if (onSingle && isPublishedAvailable(instance)) {
      activators.push(ViewPublishableActivator);
    }

    return activators;
  };
}
