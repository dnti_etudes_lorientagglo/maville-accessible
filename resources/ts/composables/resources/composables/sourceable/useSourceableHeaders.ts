import makeTableHeader from '@/ts/components/data/makeTableHeader';

export default function useSourceableHeaders() {
  return [
    makeTableHeader('sources'),
  ];
}
