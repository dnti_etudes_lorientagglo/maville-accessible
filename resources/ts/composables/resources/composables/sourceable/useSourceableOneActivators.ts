import { Sourceable } from '@/ts/api/composables/sourceable';
import MergeSourcesActivator
  from '@/ts/components/resources/composables/sourceable/MergeSourcesActivator.vue';
import SyncSourcesActivator
  from '@/ts/components/resources/composables/sourceable/SyncSourcesActivator.vue';
import { OneActivatorsContext } from '@/ts/composables/resources/composables/core/types';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';

export default function useSourceableOneActivators() {
  return ({ instance, onSingle }: OneActivatorsContext<Sourceable>) => [
    ...(
      onSingle && hasPermissionOn('mergeSources', instance) && instance.sourced
        ? [MergeSourcesActivator] : []
    ),
    ...(
      onSingle && hasPermissionOn('syncSources', instance) && instance.sourced && !instance.isSyncedWithSources
        ? [SyncSourcesActivator] : []
    ),
  ];
}
