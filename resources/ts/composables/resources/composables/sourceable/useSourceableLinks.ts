import { Sourceable } from '@/ts/api/composables/sourceable';
import SourceOrigin from '@/ts/api/enums/sourceOrigin';
import isNone from '@/ts/utilities/common/isNone';
import tap from '@/ts/utilities/functions/tap';
import { Optional } from '@/ts/utilities/types/optional';
import { computed, MaybeRef, unref } from 'vue';

export default function useSourceableLinks(sourceable: MaybeRef<Sourceable>) {
  return computed(() => (unref(sourceable).sources ?? []).reduce(
    (sourcesMap, source) => tap(sourcesMap, () => {
      const sourceOrigin = SourceOrigin.all.get(source.name);
      if (sourceOrigin && (
        !sourcesMap.has(sourceOrigin)
        || (isNone(sourcesMap.get(sourceOrigin)) && !isNone(source.data.website))
      )) {
        sourcesMap.set(sourceOrigin, source.data.website);
      }
    }),
    new Map<SourceOrigin, Optional<string>>(),
  ));
}
