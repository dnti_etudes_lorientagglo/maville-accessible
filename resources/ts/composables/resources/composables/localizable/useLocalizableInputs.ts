import Place from '@/ts/api/models/place';
import useInput from '@/ts/composables/forms/useInput';

export default function useLocalizableInputs() {
  const placesInput = useInput({
    name: 'places',
    value: [] as Place[],
  });

  return { placesInput };
}
