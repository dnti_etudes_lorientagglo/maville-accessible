import makeTableHeader from '@/ts/components/data/makeTableHeader';
import FeatureName from '@/ts/utilities/features/featureName';

export default function useLocalizableHeaders() {
  return [
    makeTableHeader('places', {
      when: FeatureName.PLACES.check(),
    }),
  ];
}
