import useInput from '@/ts/composables/forms/useInput';
import toLowerCase from '@/ts/utilities/strings/toLowerCase';
import email from '@/ts/validation/rules/email';

export default function useContactableInputs() {
  const emailInput = useInput({
    name: 'email',
    value: '',
    rules: [email],
    transforms: toLowerCase,
  });

  const phoneInput = useInput({
    name: 'phone',
    value: '',
  });

  return {
    emailInput,
    phoneInput,
  };
}
