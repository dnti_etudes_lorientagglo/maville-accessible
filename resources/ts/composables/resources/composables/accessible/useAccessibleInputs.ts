import { AccessibilityObject } from '@/ts/api/composables/accessible';
import useInput from '@/ts/composables/forms/useInput';
import mapValues from '@/ts/utilities/objects/mapValues';

export default function useAccessibleInputs() {
  const accessibilityInput = useInput({
    name: 'accessibility',
    value: null as AccessibilityObject | null,
    toModelValue: ({ value }: { value: AccessibilityObject | null }) => (
      value
        ? mapValues(value, (item) => ({ ...item }))
        : null
    ),
  });

  return { accessibilityInput };
}
