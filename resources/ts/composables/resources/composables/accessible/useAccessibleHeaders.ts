import makeTableHeader from '@/ts/components/data/makeTableHeader';
import FeatureName from '@/ts/utilities/features/featureName';

export default function useAccessibleHeaders() {
  return [
    makeTableHeader('accessibility', {
      when: () => FeatureName.ACCESSIBILITY.check(),
    }),
  ];
}
