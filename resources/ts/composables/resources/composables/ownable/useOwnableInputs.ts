import { Ownable } from '@/ts/api/composables/ownable';
import Organization from '@/ts/api/models/organization';
import useInput from '@/ts/composables/forms/useInput';
import useResourceMeta from '@/ts/composables/resources/core/useResourceMeta';
import useResourceType from '@/ts/composables/resources/core/useResourceType';
import actingForOrganization from '@/ts/utilities/authorizations/actingForOrganization';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';

export default function useOwnableInputs() {
  const ownerOrganizationInput = useInput({
    name: 'ownerOrganization',
    value: null as Organization | null,
  });

  const bootOwnableInputs = (instance: Ownable) => {
    const type = useResourceType(instance);
    const meta = useResourceMeta(type);
    const canAdmin = hasPermissionOn(`${meta.policyType}.admin`);
    if (!canAdmin) {
      ownerOrganizationInput.props.readonly = true;
    }

    if (!instance.$exists && !canAdmin) {
      ownerOrganizationInput.value = actingForOrganization()?.organization ?? null;
    }
  };

  return {
    ownerOrganizationInput,
    bootOwnableInputs,
  };
}
