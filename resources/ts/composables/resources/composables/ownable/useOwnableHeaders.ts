import makeTableHeader from '@/ts/components/data/makeTableHeader';
import useUserOwnableHeaders
  from '@/ts/composables/resources/composables/userOwnable/useUserOwnableHeaders';

export default function useOwnableHeaders() {
  return [
    makeTableHeader('ownerOrganization'),
    ...useUserOwnableHeaders(),
  ];
}
