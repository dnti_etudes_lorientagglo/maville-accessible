import makeTableHeader from '@/ts/components/data/makeTableHeader';

export default function useUserOwnableHeaders() {
  return [
    makeTableHeader('ownerUser'),
  ];
}
