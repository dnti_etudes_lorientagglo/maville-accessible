import EditActivator from '@/ts/components/resources/core/activators/one/EditActivator.vue';
import { OneActivatorsContext } from '@/ts/composables/resources/composables/core/types';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';

export default function useEditActivator({ instance }: OneActivatorsContext) {
  return hasPermissionOn('update', instance) ? [EditActivator] : [];
}
