import CreateCopyActivator
  from '@/ts/components/resources/core/activators/one/CreateCopyActivator.vue';
import { OneActivatorsContext } from '@/ts/composables/resources/composables/core/types';
import useResourceMeta from '@/ts/composables/resources/core/useResourceMeta';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';

export default function useCreateCopyActivator({ type, instance }: OneActivatorsContext) {
  const meta = useResourceMeta(type);

  return hasPermissionOn(`${meta.policyType}.create`) && hasPermissionOn('show', instance)
    ? [CreateCopyActivator]
    : [];
}
