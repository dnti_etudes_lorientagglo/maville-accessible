import DeleteActivator from '@/ts/components/resources/core/activators/one/DeleteActivator.vue';
import { OneActivatorsContext } from '@/ts/composables/resources/composables/core/types';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';

export default function useDeleteActivator({ instance }: OneActivatorsContext) {
  return hasPermissionOn('delete', instance) ? [DeleteActivator] : [];
}
