import makeTableHeader from '@/ts/components/data/makeTableHeader';

export default function useCommonHeaders() {
  return [
    makeTableHeader('createdAt', { sortable: true }),
    makeTableHeader('updatedAt', { sortable: true }),
  ];
}
