import { OneActivatorsContext } from '@/ts/composables/resources/composables/core/types';
import useDeleteActivator from '@/ts/composables/resources/composables/core/useDeleteActivator';
import useEditActivator from '@/ts/composables/resources/composables/core/useEditActivator';
import useViewActivator from '@/ts/composables/resources/composables/core/useViewActivator';

export default function useCoreOneActivators() {
  return (context: OneActivatorsContext) => [
    ...useViewActivator(context),
    ...useEditActivator(context),
    ...useDeleteActivator(context),
  ];
}
