import ViewActivator from '@/ts/components/resources/core/activators/one/ViewActivator.vue';
import { OneActivatorsContext } from '@/ts/composables/resources/composables/core/types';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';

export default function useViewActivator({ instance, onSingle }: OneActivatorsContext) {
  return !onSingle && hasPermissionOn('show', instance) ? [ViewActivator] : [];
}
