import { ManyActivatorsContext } from '@/ts/composables/resources/composables/core/types';
import useCreateActivator from '@/ts/composables/resources/composables/core/useCreateActivator';

export default function useCoreManyActivators() {
  return (context: ManyActivatorsContext) => [
    ...useCreateActivator(context),
  ];
}
