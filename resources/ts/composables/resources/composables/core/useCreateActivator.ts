import CreateActivator from '@/ts/components/resources/core/activators/many/CreateActivator.vue';
import { ManyActivatorsContext } from '@/ts/composables/resources/composables/core/types';
import useResourceMeta from '@/ts/composables/resources/core/useResourceMeta';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';

export default function useCreateActivator({ type }: ManyActivatorsContext) {
  const meta = useResourceMeta(type);

  return hasPermissionOn(`${meta.policyType}.create`) ? [CreateActivator] : [];
}
