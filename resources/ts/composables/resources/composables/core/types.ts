import { ModelInstance } from '@foscia/core';

export type ManyActivatorsContext = {
  type: string;
};

export type ManyActivatorsFactory = (context: ManyActivatorsContext) => any[];

export type OneActivatorsContext<I extends ModelInstance = any> = {
  type: string;
  instance: I;
  onSingle?: boolean;
};

export type OneActivatorsFactory = (context: OneActivatorsContext) => any[];
