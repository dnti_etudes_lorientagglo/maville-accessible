import Media from '@/ts/api/models/media';
import useInput, { InputOptions } from '@/ts/composables/forms/useInput';

export default function useCoverableInputs(
  coverInputOptions: Partial<InputOptions<'cover', Media | null>> = {},
) {
  const coverInput = useInput({
    name: 'cover',
    value: null as Media | null,
    ...coverInputOptions,
  });

  return { coverInput };
}
