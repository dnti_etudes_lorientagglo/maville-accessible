import makeTableHeader from '@/ts/components/data/makeTableHeader';

export default function useCoverableHeaders() {
  return [
    makeTableHeader('cover'),
  ];
}
