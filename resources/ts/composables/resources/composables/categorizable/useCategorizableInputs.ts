import Category from '@/ts/api/models/category';
import Organization from '@/ts/api/models/organization';
import useInput, { Input, InputOptions } from '@/ts/composables/forms/useInput';
import { watch } from 'vue';

export default function useCategorizableInputs(
  categoriesInputOptions: Partial<InputOptions<'categories', Category[]>> = {},
  organizationInput?: Input<string, Organization | null>,
) {
  const categoriesInput = useInput({
    name: 'categories',
    value: [] as Category[],
    ...categoriesInputOptions,
  });

  const fillFromOrganization = (defaultValue?: Category[]) => {
    const organizationCategories = organizationInput?.value?.categories ?? [];
    if (!categoriesInput.value.length && organizationCategories.length) {
      categoriesInput.value = [...organizationCategories];
    } else if (defaultValue) {
      categoriesInput.value = defaultValue;
    }
  };

  const bootCategorizableInputs = () => {
    if (organizationInput) {
      fillFromOrganization([]);
    }
  };

  if (organizationInput) {
    watch(organizationInput.valueRef, () => {
      fillFromOrganization();
    });
  }

  return {
    categoriesInput,
    bootCategorizableInputs,
  };
}
