import useInput from '@/ts/composables/forms/useInput';
import translate from '@/ts/utilities/lang/translate';

export default function useCategorizableFilters() {
  const categoryInput = useInput({
    name: 'category',
    label: translate('categories'),
    value: [],
  });

  return { categoryInput };
}
