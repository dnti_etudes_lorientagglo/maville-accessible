import makeTableHeader from '@/ts/components/data/makeTableHeader';

export default function useCategorizableHeaders() {
  return [
    makeTableHeader('categories'),
  ];
}
