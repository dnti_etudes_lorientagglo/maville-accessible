import useInput from '@/ts/composables/forms/useInput';

export default function useSearchableFilters() {
  const searchInput = useInput({
    name: 'search',
    value: '',
  });

  return { searchInput };
}
