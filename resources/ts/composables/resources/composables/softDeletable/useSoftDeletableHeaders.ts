import makeTableHeader from '@/ts/components/data/makeTableHeader';

export default function useSoftDeletableHeaders() {
  return [
    makeTableHeader('deletedAt', { sortable: true }),
  ];
}
