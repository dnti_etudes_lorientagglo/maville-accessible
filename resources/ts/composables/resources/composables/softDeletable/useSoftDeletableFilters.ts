import useInput from '@/ts/composables/forms/useInput';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { useI18n } from 'vue-i18n';

type SoftDeletableFiltersOptions = {
  label?: string;
};

export default function useSoftDeletableFilters(options: SoftDeletableFiltersOptions = {}) {
  const i18n = useI18n();

  const softDeletableInput = useInput({
    name: 'deleted',
    label: options.label ?? i18n.t('resources.composables.softDeletable.filters.softDeleted.label'),
    value: null as string | null,
  });

  const softDeletableTransformer = (filters: Dictionary) => ({
    ...filters,
    deleted: undefined,
    withTrashed: filters.deleted === 'withTrashed' ? true : undefined,
    onlyTrashed: filters.deleted === 'onlyTrashed' ? true : undefined,
  });

  return {
    softDeletableInput,
    softDeletableTransformer,
  };
}
