import { SoftDeletable } from '@/ts/api/composables/softDeletable';
import SoftRestoreActivator
  from '@/ts/components/resources/composables/softDeletable/SoftRestoreActivator.vue';
import { OneActivatorsContext } from '@/ts/composables/resources/composables/core/types';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';

export default function useSoftRestoreActivator(
  { instance }: OneActivatorsContext<SoftDeletable>,
) {
  return instance.deleted && hasPermissionOn('update', instance) ? [SoftRestoreActivator] : [];
}
