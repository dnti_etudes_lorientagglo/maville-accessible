import { SoftDeletable } from '@/ts/api/composables/softDeletable';
import SoftDeleteActivator
  from '@/ts/components/resources/composables/softDeletable/SoftDeleteActivator.vue';
import { OneActivatorsContext } from '@/ts/composables/resources/composables/core/types';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';

export default function useSoftDeleteActivator(
  { instance }: OneActivatorsContext<SoftDeletable>,
) {
  return !instance.deleted && hasPermissionOn('update', instance) ? [SoftDeleteActivator] : [];
}
