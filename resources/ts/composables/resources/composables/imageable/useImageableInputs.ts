import Media from '@/ts/api/models/media';
import useInput from '@/ts/composables/forms/useInput';

export default function useImageableInputs() {
  const imagesInput = useInput({
    name: 'images',
    value: [] as Media[],
  });

  return { imagesInput };
}
