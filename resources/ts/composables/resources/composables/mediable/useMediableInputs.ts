import { Mediable } from '@/ts/api/composables/mediable';
import Media from '@/ts/api/models/media';
import useComputedInput from '@/ts/composables/forms/useComputedInput';
import { Input } from '@/ts/composables/forms/useInput';
import localesKeys from '@/ts/utilities/lang/localesKeys';
import { useLangStore } from '@/ts/stores/lang';
import uniqBy from '@/ts/utilities/arrays/uniqBy';
import wrap from '@/ts/utilities/arrays/wrap';
import mapWithKeys from '@/ts/utilities/objects/mapWithKeys';
import { computed, reactive, unref, watch } from 'vue';

export default function useMediableInputs(inputs: Input[]) {
  const lang = useLangStore();
  const locales = localesKeys();

  const mediaByLocale = reactive(mapWithKeys(locales, (locale) => ({ [locale]: [] as Media[] })));
  const mediaForLocale = computed(() => uniqBy(inputs.reduce((media, input) => {
    media.push(...wrap(unref(input.props.media)));

    return media;
  }, [] as Media[]), 'id'));
  const allMedia = computed(() => Object.values(mediaByLocale).flat());

  const mediaInput = useComputedInput('media', allMedia);

  watch(mediaForLocale, (media) => {
    mediaByLocale[lang.editionLocale] = media;
  });

  watch(() => lang.editionLocale, () => {
    inputs.forEach((input) => {
      // eslint-disable-next-line no-param-reassign
      input.props.media = allMedia.value;
    });
  });

  const bootMediableInputs = async (mediable: Mediable) => {
    inputs.forEach((input) => {
      // eslint-disable-next-line no-param-reassign
      input.props.media = mediable.media;
    });
  };

  return {
    mediaInput,
    bootMediableInputs,
  };
}
