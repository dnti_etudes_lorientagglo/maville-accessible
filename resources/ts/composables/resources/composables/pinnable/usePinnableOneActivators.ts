import { Pinnable } from '@/ts/api/composables/pinnable';
import PinActivator from '@/ts/components/resources/composables/pinnable/PinActivator.vue';
import UnpinActivator from '@/ts/components/resources/composables/pinnable/UnpinActivator.vue';
import { OneActivatorsContext } from '@/ts/composables/resources/composables/core/types';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';

export default function usePinnableOneActivators() {
  return ({ instance, onSingle }: OneActivatorsContext<Pinnable>) => {
    const activators = [];

    if (onSingle && hasPermissionOn('pin', instance)) {
      activators.push(PinActivator);
    }

    if (onSingle && hasPermissionOn('unpin', instance)) {
      activators.push(UnpinActivator);
    }

    return activators;
  };
}
