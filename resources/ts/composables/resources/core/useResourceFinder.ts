import resourcesFinders from '@/ts/resources/resourcesFinders';
import inferResourceModel from '@/ts/resources/utilities/inferResourceModel';
import makeResourceFinder from '@/ts/resources/utilities/makeResourceFinder';
import toCamelCase from '@/ts/utilities/strings/toCamelCase';

const defaultResourceFinder = (type: string) => async () => {
  const Model = await inferResourceModel(type);

  return makeResourceFinder(Model);
};

export default function useResourceFinder(type: string) {
  const finder = resourcesFinders[`./${toCamelCase(type)}/finder.ts`]
    ?? defaultResourceFinder(type);

  return finder();
}
