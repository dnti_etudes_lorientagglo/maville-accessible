import resourcesFactories from '@/ts/resources/resourcesFactories';
import inferResourceModel from '@/ts/resources/utilities/inferResourceModel';
import toCamelCase from '@/ts/utilities/strings/toCamelCase';

const defaultResourceFactory = (type: string) => async () => {
  const Model = await inferResourceModel(type);

  return () => new Model();
};

export default function useResourceFactory(type: string) {
  const factory = resourcesFactories[`./${toCamelCase(type)}/factory.ts`]
    ?? defaultResourceFactory(type);

  return factory();
}
