import resourcesFetchers from '@/ts/resources/resourcesFetchers';
import inferResourceModel from '@/ts/resources/utilities/inferResourceModel';
import makeResourceFetcher from '@/ts/resources/utilities/makeResourceFetcher';
import toCamelCase from '@/ts/utilities/strings/toCamelCase';

const defaultResourceFetcher = (type: string) => async () => {
  const Model = await inferResourceModel(type);

  return makeResourceFetcher(Model);
};

export default function useResourceFetcher(type: string) {
  const fetcher = resourcesFetchers[`./${toCamelCase(type)}/fetcher.ts`]
    ?? defaultResourceFetcher(type);

  return fetcher();
}
