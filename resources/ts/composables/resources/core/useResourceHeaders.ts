import { TableHeader } from '@/ts/components/data/makeTableHeader';
import { Optional } from '@/ts/utilities/types/optional';
import { computed, MaybeRef, unref } from 'vue';

export default function useResourceHeaders(
  headers: MaybeRef<TableHeader[]>,
  only: MaybeRef<Optional<string[]>>,
  large?: boolean,
) {
  return computed(() => {
    const onlyHeaders = unref(only) ?? [];

    return unref(headers).filter((header) => (
      (onlyHeaders.length === 0 || onlyHeaders.indexOf(header.name) !== -1)
      && (!large || !header.large)
    ));
  });
}
