import useActive from '@/ts/composables/common/useActive';
import useResourceFactory from '@/ts/composables/resources/core/useResourceFactory';
import useResourceFinder from '@/ts/composables/resources/core/useResourceFinder';
import authorizeGuard from '@/ts/router/guards/authorizeGuard';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';
import { ModelInstance } from '@foscia/core';
import { nextTick, onDeactivated, onMounted, ref, watch } from 'vue';
import { useRoute } from 'vue-router';

export default function useResourceNewInstance<I extends ModelInstance>(type: string) {
  const route = useRoute();
  const active = useActive();
  const loading = ref(true);
  const instance = ref(null as I | null);
  const copyInstance = ref(null as I | null);

  const makeInstance = async () => {
    loading.value = true;

    instance.value = null;
    copyInstance.value = null;

    try {
      if (typeof route.query.copy === 'string') {
        const finder = await useResourceFinder(type);
        const copiedInstance = await finder(route.query.copy);

        await authorizeGuard(() => hasPermissionOn('show', copiedInstance))(route);

        copyInstance.value = copiedInstance;
      }
    } catch {
      copyInstance.value = null;
    }

    const factory = await useResourceFactory(type);

    instance.value = await factory();

    loading.value = false;
  };

  watch(() => route.path, async (nextPath, prevPath) => {
    await nextTick();
    copyInstance.value = null;
    if (active.value && String(nextPath ?? '') !== String(prevPath ?? '')) {
      await makeInstance();
    }
  });

  onMounted(async () => {
    await makeInstance();
  });

  onDeactivated(() => {
    loading.value = true;
  });

  return {
    loading,
    instance,
    copyInstance,
  };
}
