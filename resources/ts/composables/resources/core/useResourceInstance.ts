import useActive from '@/ts/composables/common/useActive';
import useResourceFinder from '@/ts/composables/resources/core/useResourceFinder';
import useResourceRouteMany from '@/ts/composables/resources/core/useResourceRouteMany';
import useResourceTitle from '@/ts/composables/resources/core/useResourceTitle';
import authorizeGuard from '@/ts/router/guards/authorizeGuard';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';
import { ExpectedRunFailureError, ModelInstance } from '@foscia/core';
import { nextTick, onDeactivated, onMounted, ref, triggerRef, watch } from 'vue';
import { useRoute, useRouter } from 'vue-router';

export default function useResourceInstance<I extends ModelInstance>(
  type: string,
  action?: string,
) {
  const router = useRouter();
  const route = useRoute();
  const active = useActive();
  const loading = ref(true);
  const instance = ref(null as I | null);
  const title = useResourceTitle(instance, action);

  const fetch = async () => {
    loading.value = true;

    const newId = route.params.instance as string;
    if (!newId) {
      return;
    }

    const prevId = instance.value?.id;
    if (prevId !== newId) {
      instance.value = null;
    }

    const finder = await useResourceFinder(type);

    try {
      const prevInstance = instance.value;

      instance.value = await finder(newId);

      await authorizeGuard(() => hasPermissionOn('show', instance.value!))(route);

      if (prevInstance === instance.value) {
        triggerRef(instance);
      }
    } catch (error) {
      if (error instanceof ExpectedRunFailureError) {
        await router.replace(useResourceRouteMany(type));

        return;
      }

      throw error;
    }

    loading.value = false;
  };

  watch(() => route.path, async (nextPath, prevPath) => {
    await nextTick();
    if (active.value && String(nextPath ?? '') !== String(prevPath ?? '')) {
      await fetch();
    }
  });

  onMounted(async () => {
    await fetch();
  });

  onDeactivated(() => {
    loading.value = true;
  });

  return {
    loading,
    title,
    instance,
    fetch,
  };
}
