import resourcesEntries from '@/ts/resources/resourcesEntries';
import resourcesGroups from '@/ts/resources/resourcesGroups';
import { ResourceEntry } from '@/ts/resources/utilities/makeResourceEntry';
import { ResourceGroup } from '@/ts/resources/utilities/makeResourceGroup';
import orderBy from '@/ts/utilities/arrays/orderBy';
import isNil from '@/ts/utilities/common/isNil';
import isExternalRoute from '@/ts/utilities/routes/isExternalRoute';
import resolveRoute from '@/ts/utilities/routes/resolveRoute';
import { computed } from 'vue';
import { useRouter } from 'vue-router';

export default function useResourcesEntries() {
  const router = useRouter();

  return computed(() => {
    const rawGroups = resourcesGroups();
    const rawEntries = orderBy(Object.values(resourcesEntries()).flat(), 'position');

    const roots = [] as (ResourceGroup | ResourceEntry)[];

    rawEntries.forEach((entry) => {
      if (isExternalRoute(resolveRoute(router, entry.to))) {
        return;
      }

      if (isNil(entry.group)) {
        roots.push(entry);
      } else {
        const group = rawGroups[entry.group];

        group.entries.push(entry);

        if (roots.indexOf(group) === -1) {
          roots.push(group);
        }
      }
    });

    return orderBy(roots, 'position');
  });
}
