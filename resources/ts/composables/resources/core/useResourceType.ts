import Activity from '@/ts/api/models/activity';
import isNil from '@/ts/utilities/common/isNil';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { Optional } from '@/ts/utilities/types/optional';
import { ModelInstance } from '@foscia/core';
import { useRoute } from 'vue-router';

export type ResourceRawContext = ModelInstance | string;

const INSTANCE_TYPES_RESOLVERS: Dictionary<(instance: any) => string> = {
  activities: (activity: Activity) => activity.activityType.code,
};

export default function useResourceType(
  rawContext?: Optional<ResourceRawContext>,
): string {
  if (isNil(rawContext)) {
    const route = useRoute();

    return route.meta.resource as string;
  }

  if (typeof rawContext === 'string') {
    return rawContext;
  }

  const type = rawContext.$model.$type;

  return type in INSTANCE_TYPES_RESOLVERS
    ? INSTANCE_TYPES_RESOLVERS[type](rawContext)
    : rawContext.$model.$type;
}
