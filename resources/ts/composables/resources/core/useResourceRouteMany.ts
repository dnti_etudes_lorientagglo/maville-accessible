export default function useResourceRouteMany(
  type: string,
  action?: string,
) {
  return `/resources/${type}/${action ?? 'view-many'}`;
}
