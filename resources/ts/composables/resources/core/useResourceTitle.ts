import useResourceMeta from '@/ts/composables/resources/core/useResourceMeta';
import useResourceType, {
  ResourceRawContext,
} from '@/ts/composables/resources/core/useResourceType';
import isNil from '@/ts/utilities/common/isNil';
import toCamelCase from '@/ts/utilities/strings/toCamelCase';
import { Optional } from '@/ts/utilities/types/optional';
import { computed, MaybeRef, unref } from 'vue';
import { useI18n } from 'vue-i18n';

export default function useResourceTitle(
  rawContext?: MaybeRef<Optional<ResourceRawContext>>,
  action?: string,
) {
  const i18n = useI18n();

  return computed(() => {
    const derefRawContext = unref(rawContext);
    if (isNil(derefRawContext)) {
      return [];
    }

    const type = useResourceType(derefRawContext);
    const meta = useResourceMeta(type);
    const titles = [i18n.t(`resources.common.types.${toCamelCase(meta.type)}.plural`)];
    if (typeof derefRawContext !== 'string') {
      titles.push(
        isNil(action)
          ? meta.title(derefRawContext)
          : i18n.t(`actions.scoped.${action}`, {
            name: meta.title(derefRawContext),
          }),
      );
    } else if (!isNil(action)) {
      titles.push(i18n.t(`actions.${action}`));
    }

    return titles;
  });
}
