import resourcesMeta from '@/ts/resources/resourcesMeta';
import value from '@/ts/utilities/functions/value';
import { ModelInstance } from '@foscia/core';

export type ResourceRawMeta = {
  type: string;
  modelType?: string;
  policyType?: string;
  icon: string | (() => string);
  color: string | (() => string);
  title: (i: ModelInstance) => string;
};

export type ResourceMeta = {
  type: string;
  modelType: string;
  policyType: string;
  icon: string;
  color: string;
  title: (i: ModelInstance) => string;
};

export default function useResourceMeta(type: string) {
  const meta = resourcesMeta[type];

  return {
    type: meta.type,
    policyType: meta.policyType ?? meta.type,
    modelType: meta.modelType ?? meta.type,
    icon: value(meta.icon),
    color: value(meta.color),
    title: meta.title,
  };
}
