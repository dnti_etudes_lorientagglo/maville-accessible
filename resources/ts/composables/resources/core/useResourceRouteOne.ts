import useResourceType from '@/ts/composables/resources/core/useResourceType';
import { ModelInstance } from '@foscia/core';

export default function useResourceRouteOne(
  instance: ModelInstance,
  action?: string,
) {
  const type = useResourceType(instance);

  return `/resources/${type}/${action ?? 'view'}/${instance.id}`;
}
