import { TableHeader } from '@/ts/components/data/makeTableHeader';
import useDynamicDocumentSubtitle from '@/ts/composables/accessibility/useDynamicDocumentSubtitle';
import useForm from '@/ts/composables/forms/useForm';
import { Input } from '@/ts/composables/forms/useInput';
import useAnnouncer from '@/ts/composables/pages/useAnnouncer';
import useResourceFetcher from '@/ts/composables/resources/core/useResourceFetcher';
import useResourceMeta from '@/ts/composables/resources/core/useResourceMeta';
import useResourceTitle from '@/ts/composables/resources/core/useResourceTitle';
import { i18nInstance } from '@/ts/plugins/i18n';
import authorizeGuard from '@/ts/router/guards/authorizeGuard';
import { useSnackbarStore } from '@/ts/stores/snackbar';
import sortsToString from '@/ts/utilities/arrays/sortsToString';
import stringToSorts from '@/ts/utilities/arrays/stringToSorts';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';
import isNone from '@/ts/utilities/common/isNone';
import debounce from '@/ts/utilities/functions/debounce';
import tap from '@/ts/utilities/functions/tap';
import filterValues from '@/ts/utilities/objects/filterValues';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { SortDirection } from '@/ts/utilities/types/sort';
import { ModelInstance } from '@foscia/core';
import { computed, onActivated, onMounted, onUnmounted, ref } from 'vue';
import { useRoute, useRouter } from 'vue-router';

export type ResourceQueryOptions<F extends readonly Input[]> = {
  headers?: TableHeader[];
  filters?: F;
  filtersTransformers?: ((filters: Dictionary) => Dictionary)[];
};

export type ResourceQueryData<I> = {
  instances: I[];
  page: {
    total: number;
    current: number;
    last: number;
  };
};

export default function useResourceInstances<I extends ModelInstance, F extends readonly Input[]>(
  type: string,
  options: ResourceQueryOptions<F> = {},
) {
  const snackbar = useSnackbarStore();
  const route = useRoute();
  const router = useRouter();
  const announcer = useAnnouncer();

  const meta = useResourceMeta(type);
  const baseTitle = useResourceTitle(type);

  const booting = ref(true);
  const loading = ref(true);
  const rawQuery = ref({ page: '1', ...route.query } as Dictionary);
  const data = ref({
    instances: [],
    page: { total: 0, current: 1, last: 1 },
  } as ResourceQueryData<I>);

  const query = computed(() => filterValues(rawQuery.value, (v) => !isNone(v)));
  const sortables = computed(() => (options.headers ?? []).reduce((s, h) => tap(s, () => {
    if (h.sortName) {
      s.push(h.sortName);
    }
  }), [] as string[]));
  const subtitle = useDynamicDocumentSubtitle({
    total: computed(() => data.value.page.total),
    page: computed(() => data.value.page),
    search: computed(() => query.value.search),
    filters: computed(() => {
      const { page, search, sort, ...filters } = query.value;

      return Object.values(filters).filter((v) => !isNone(v)).length;
    }),
  });
  const title = computed(() => tap([
    ...baseTitle.value,
  ], (t) => t.unshift(subtitle.value.join(' | '))));
  const sorts = computed(() => (
    typeof query.value.sort === 'string'
      ? filterValues(stringToSorts(query.value.sort), (_, k) => sortables.value.indexOf(k) !== -1)
      : undefined
  ));

  const replaceLocalQuery = (newQuery: Dictionary) => {
    rawQuery.value = { ...query.value, ...newQuery };
  };

  const replaceRouteQuery = () => router.replace({ query: query.value });

  const fetch = async () => {
    loading.value = true;

    const fetcher = await useResourceFetcher(type);

    await replaceRouteQuery();

    const { page, search, sort, ...filters } = query.value;
    const transformedFilters = (options.filtersTransformers ?? []).reduce((f, t) => t(f), filters);

    const allData = await fetcher({ page, search, sort, filters: transformedFilters });

    data.value = {
      instances: allData.instances,
      page: {
        total: allData.document.meta!.page.total,
        current: allData.document.meta!.page.currentPage,
        last: allData.document.meta!.page.lastPage,
      },
    };

    if (!data.value.instances.length
      && data.value.page.current !== 1
      && data.value.page.total > 0
    ) {
      replaceLocalQuery({ page: String(data.value.page.last) });
      await fetch();
    }

    loading.value = false;
  };

  const debounceFetch = debounce(fetch);

  const { values, fill, onSubmit } = useForm(options.filters ?? [], {
    onChange({ allValues }) {
      if (booting.value) {
        return;
      }

      loading.value = true;
      replaceLocalQuery({ page: '1', ...allValues });
      debounceFetch();
    },
    async onSubmit({ allValues }) {
      replaceLocalQuery({ page: '1', ...allValues });
      await fetch();
      snackbar.toast(i18nInstance.t('navigation.listLoaded'));
    },
  });

  const onPage = async (newPage: number) => {
    if (loading.value) {
      return;
    }

    replaceLocalQuery({ page: String(newPage) });
    await fetch();
    announcer.announce({ title: title.value, scrollTo: 0 });
  };

  const onSort = (newSorts?: Dictionary<SortDirection>) => {
    const sortStr = sortsToString(newSorts);
    if (query.value.sort !== sortStr) {
      loading.value = true;
      replaceLocalQuery({ sort: sortsToString(newSorts) });
      debounceFetch();
    }
  };

  onMounted(async () => {
    await authorizeGuard(() => hasPermissionOn(`${meta.policyType}.showAny`))(route);

    fill(query.value as any);
    replaceLocalQuery(values.value);
    await replaceRouteQuery();
  });

  onActivated(async () => {
    await authorizeGuard(() => hasPermissionOn(`${meta.policyType}.showAny`))(route);

    const routeLength = Object.keys(route.query).length;
    const localLength = Object.keys(rawQuery.value).length;
    if (routeLength === 0 && localLength > 0) {
      await replaceRouteQuery();
    }

    if (loading.value === true) {
      await fetch();
      booting.value = false;
    }

    await announcer.announceAfterTick({ title: title.value });
  });

  onUnmounted(() => {
    loading.value = true;
  });

  return {
    sorts,
    loading,
    title,
    data,
    fetch,
    onSubmit,
    onPage,
    onSort,
  };
}
