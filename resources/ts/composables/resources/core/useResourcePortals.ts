import { TableHeader } from '@/ts/components/data/makeTableHeader';
import { Portal } from '@/ts/composables/common/types';
import mapWithKeys from '@/ts/utilities/objects/mapWithKeys';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { Optional } from '@/ts/utilities/types/optional';
import { MaybeRef, shallowReactive, unref, watch } from 'vue';

export type ResourcePortal = Optional<Portal>;

export type ResourcePortals = Dictionary<ResourcePortal>;

export default function useResourcePortals(headers: MaybeRef<TableHeader[]>) {
  const portals = shallowReactive(mapWithKeys(unref(headers), (header) => ({
    [header.name]: null as ResourcePortal,
  })) as ResourcePortals);

  watch(() => unref(headers), () => {
    unref(headers).forEach((header) => {
      portals[header.name] = portals[header.name] ?? null;
    });
  });

  const onPortalRef = (header: TableHeader, portal: ResourcePortal) => {
    portals[header.name] = portal;
  };

  return { portals, onPortalRef };
}
