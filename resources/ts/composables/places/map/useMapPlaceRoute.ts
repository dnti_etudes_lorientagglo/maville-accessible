import Place from '@/ts/api/models/place';
import filterValues from '@/ts/utilities/objects/filterValues';
import { Optional } from '@/ts/utilities/types/optional';
import { computed, MaybeRef, unref } from 'vue';
import { useRoute } from 'vue-router';

export default function useMapPlaceRoute(
  placeRef?: Optional<MaybeRef<Optional<Place>>>,
  viewRef?: Optional<MaybeRef<Optional<string>>>,
) {
  const route = useRoute();

  return computed(() => {
    const place = unref(placeRef)?.slug ?? undefined;
    const view = unref(viewRef) ?? (place === undefined ? 'search' : 'details');

    return {
      name: 'places.map',
      params: { place, view },
      query: view !== 'directions'
        ? filterValues(route.query, (_, key) => key !== 't' && key !== 'ti')
        : route.query,
    };
  });
}
