import { MglMapRef } from '@/ts/utilities/places/map/types';
import { ref } from 'vue';

export default function useMapRef() {
  return ref(null as MglMapRef | null);
}
