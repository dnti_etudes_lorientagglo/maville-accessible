import { ControlPosition, IControl, Map } from 'maplibre-gl';
import { onBeforeUnmount, onMounted, ref } from 'vue';

export default function useMglMapControl(map: Map, position: ControlPosition) {
  const controlRef = ref<HTMLElement | null>(null);
  const control = ref<IControl | null>(null);

  onMounted(() => {
    control.value = {
      onAdd() {
        return controlRef.value!;
      },
      onRemove() {
      },
    };

    map.addControl(control.value, position);
  });

  onBeforeUnmount(() => {
    if (control.value) {
      map.removeControl(control.value);
      control.value = null;
    }
  });

  return { controlRef };
}
