import { MglMapRef } from '@/ts/utilities/places/map/types';
import { Optional } from '@/ts/utilities/types/optional';
import { MaybeRef, unref, watch } from 'vue';

export default function useMapCursorSwap(
  mapRef: MaybeRef<Optional<MglMapRef>>,
  active: MaybeRef<boolean>,
  cursor: string,
) {
  watch(() => unref(active), (value) => {
    const map = unref(mapRef)?.map?.();
    if (map) {
      map.getCanvas().style.setProperty('cursor', value ? cursor : '');
    }
  });
}
