import cachedCategories from '@/ts/api/cached/cachedCategories';
import withReviewsSummary from '@/ts/api/enhancers/withReviewsSummary';
import Category from '@/ts/api/models/category';
import Place from '@/ts/api/models/place';
import sortByAccessibility from '@/ts/api/utilities/accessible/sortByAccessibility';
import onlyPublishedCategories from '@/ts/api/utilities/categories/onlyPublishedCategories';
import loadReviewsSummary from '@/ts/api/utilities/reviewable/loadReviewsSummary';
import useAccessibilityPrioritizeCategories
  from '@/ts/composables/accessibility/useAccessibilityPrioritizeCategories';
import useAsync from '@/ts/composables/common/useAsync';
import useAbortController from '@/ts/composables/dom/useAbortController';
import useMapRef from '@/ts/composables/places/map/mgl/useMapRef';
import env from '@/ts/env';
import isNone from '@/ts/utilities/common/isNone';
import focusElement from '@/ts/utilities/dom/focusElement';
import parseEnvCategories from '@/ts/utilities/env/parseEnvCategories';
import debounce from '@/ts/utilities/functions/debounce';
import addressToGeoCoordinates from '@/ts/utilities/places/addresses/addressToGeoCoordinates';
import addressFromQuery from '@/ts/utilities/places/map/addressFromQuery';
import addressToQuery from '@/ts/utilities/places/map/addressToQuery';
import distance from '@/ts/utilities/places/map/distance';
import parseGeoCoordinates from '@/ts/utilities/places/map/mgl/parseGeoCoordinates';
import toGeoCoordinates from '@/ts/utilities/places/map/mgl/toGeoCoordinates';
import toStrCoordinates from '@/ts/utilities/places/map/mgl/toStrCoordinates';
import { GeoCoordinates, MglMapRef } from '@/ts/utilities/places/map/types';
import mapPlacesAction from '@/ts/utilities/places/mapPlacesAction';
import { AddressObject } from '@/ts/utilities/places/types';
import toTrim from '@/ts/utilities/strings/toTrim';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { Optional } from '@/ts/utilities/types/optional';
import { all, catchIf, include, one, when } from '@foscia/core';
import { abortSignal, HttpAbortedError, param } from '@foscia/http';
import { filterBy, JsonApiDocument, paginate, sortBy, usingDocument } from '@foscia/jsonapi';
import { Map } from 'maplibre-gl';
import {
  ComponentPublicInstance,
  computed,
  inject,
  nextTick,
  onBeforeMount,
  provide,
  Ref,
  ref,
  triggerRef,
  watch,
} from 'vue';
import { useRoute, useRouter } from 'vue-router';

export type MapPlacesSearchQuery = {
  search?: Optional<string>;
  place?: Optional<Place>;
  address?: Optional<AddressObject>;
  category?: Optional<Category>;
};

export type MapData = {
  withMap: (callback: (map: Map) => void) => void;
  mapRef: Readonly<Ref<MglMapRef | null>>;
  mapRegionRef: Readonly<Ref<ComponentPublicInstance | null>>;
  mapScrollViewRef: Readonly<Ref<HTMLElement | null>>;
  mapTopRef: Readonly<Ref<HTMLElement | null>>;
  mapCardOpened: Ref<boolean>;
  mapSearch: Readonly<Ref<MapPlacesSearchQuery>>;
  mapSearchPrependRef: Readonly<Ref<HTMLElement | null>>;
  mapSearchAppendRef: Readonly<Ref<HTMLElement | null>>;
  mapLoading: Readonly<Ref<boolean>>;
  mapMoved: Readonly<Ref<boolean>>;
  mapCenter: Readonly<Ref<GeoCoordinates>>;
  mapPlaces: Readonly<Ref<Place[] | null>>;
  mapPlacesLimit: Readonly<Ref<number>>;
  mapPlacesTotal: Readonly<Ref<number>>;
  mapPlacesMore: Readonly<Ref<boolean>>;
  mapPlacesOpened: Readonly<Ref<boolean>>;
  mapPlacesDefault: Readonly<Ref<boolean>>;
  mapPlace: Readonly<Ref<Place | null>>;
  mapFocusedPlace: Readonly<Ref<Place | null>>;
  onMapAutoMoving: () => void;
  onMapListToggle: (opened: boolean) => void;
  onMapSearchZone: () => void;
  onFocusPlace: (place: Place) => void;
  onBlurPlace: () => void;
  onSearchChange: (search: MapPlacesSearchQuery) => void;
  refreshPlaceReviewsSummary: () => Promise<void>;
};

const MAP_DATA_PROVIDE_KEY = Symbol('editor media provide key');

export function provideMapData(data: MapData) {
  provide(MAP_DATA_PROVIDE_KEY, data);
}

export function injectMapData() {
  const data = inject<MapData>(MAP_DATA_PROVIDE_KEY);
  if (data) {
    return data;
  }

  throw new Error('map data are not injected');
}

export default function useMapPlaces() {
  const route = useRoute();
  const router = useRouter();
  const { abortController, abortPrevious } = useAbortController();
  const { loading: mapLoading, asyncRun } = useAsync();
  const accessibilityPrioritizeCategories = useAccessibilityPrioritizeCategories();

  const mapRef = useMapRef();
  const mapTopRef = ref(null as HTMLElement | null);
  const mapSearchPrependRef = ref(null as HTMLElement | null);
  const mapSearchAppendRef = ref(null as HTMLElement | null);
  const mapRegionRef = ref(null as ComponentPublicInstance | null);
  const mapScrollViewRef = ref(null as HTMLElement | null);
  const mapCardOpened = ref(false);
  const mapCenter = ref(addressToGeoCoordinates(env.services.map.center));
  const mapZoom = ref(env.services.map.zoom);
  const mapMoved = ref(false);
  const mapAutoMoving = ref(false);
  const mapSearch = ref({} as MapPlacesSearchQuery);

  const mapCenterPrev = ref({ ...mapCenter.value });
  const mapZoomPrev = ref(mapZoom.value);

  const mapPlacesOpened = ref(false);
  const mapPlacesLimit = ref(50);
  const mapPlacesTotal = ref(0);
  const mapPlaces = ref(null as Place[] | null);
  const mapPlace = ref(null as Place | null);
  const mapFocusedPlace = ref(null as Place | null);
  const mapPlacesDefault = ref(false);
  const hasMapPlace = ref(false);

  const mapPlacesMore = computed(() => (
    mapPlacesTotal.value !== 0
    && mapPlacesTotal.value > mapPlacesLimit.value
  ));
  const hasMapSearch = computed(() => (
    !isNone(mapSearch.value.search)
    || !!mapSearch.value.place
    || !!mapSearch.value.category
    || !!mapSearch.value.address
  ));

  const savePrevLocation = () => {
    mapCenterPrev.value = { ...mapCenter.value };
    mapZoomPrev.value = mapZoom.value;
  };

  const setMapMoved = (moved: boolean) => {
    if (!mapAutoMoving.value) {
      mapMoved.value = moved;
    } else {
      mapAutoMoving.value = false;
      savePrevLocation();
    }
  };

  const debounceSetMapMoved = debounce(setMapMoved);

  const searchRadius = (min: number) => {
    const map = mapRef.value?.map();
    if (!map) {
      return 10 * 1000;
    }

    const bounds = map.getBounds();
    const mapDistance = distance(
      toGeoCoordinates(bounds.getNorthEast()),
      toGeoCoordinates(bounds.getSouthWest()),
    ) * 0.75;

    return Math.min(50, Math.max(Math.min(min, 15), mapDistance)) * 1000;
  };

  const updatePlaces = async (
    fetcher: () => Promise<{ instances: Place[], document: JsonApiDocument } | null>,
  ) => {
    abortPrevious();

    const data = await fetcher();
    if (data === null) {
      return;
    }

    savePrevLocation();

    mapPlaces.value = null;
    mapPlacesTotal.value = 0;

    await nextTick();

    debounceSetMapMoved(false);
    mapPlaces.value = data.instances;
    mapPlacesTotal.value = data.document.meta!.page.total;
  };

  const searchPlaces = (keepRadius = false) => asyncRun(async () => {
    const search = mapSearch.value;
    const addressRadius = (address: AddressObject) => (address.type === 'municipality' ? 10 : 2) * 1000;

    await updatePlaces(
      () => mapPlacesAction()
        .use(abortSignal(abortController.value))
        .use(sortByAccessibility())
        .use(withReviewsSummary())
        .use(param('location', search.address ? {
          longitude: search.address.geometry.coordinates[0],
          latitude: search.address.geometry.coordinates[1],
          radius: keepRadius ? searchRadius(1) : addressRadius(search.address),
        } : {
          longitude: mapCenter.value.longitude,
          latitude: mapCenter.value.latitude,
          radius: keepRadius ? searchRadius(1) : searchRadius(50),
        }))
        .use(sortBy('proximity'))
        .use(when(search.search, (a, s) => a.use(filterBy({ search: s })).use(sortBy('search'))))
        .use(when(search.place, (a, p) => a.use(filterBy({ id: [p.id] }))))
        .use(when(search.category, (a, c) => a.use(filterBy({ category: [c.id] }))))
        .use(paginate({ size: mapPlacesLimit.value }))
        .run(catchIf(all(usingDocument), (e) => e instanceof HttpAbortedError)),
    );
  });

  const fetchDefaultPlaces = async () => {
    const envCategories = await parseEnvCategories(env.services.map.defaultCategories);

    await updatePlaces(
      () => mapPlacesAction()
        .use(abortSignal(abortController.value))
        .use(sortByAccessibility())
        .use(withReviewsSummary())
        .use(when(
          envCategories.length,
          filterBy('category', envCategories.map((c) => c.id)),
        ))
        .use(param('location', {
          longitude: mapCenter.value.longitude,
          latitude: mapCenter.value.latitude,
          radius: searchRadius(1),
        }))
        .use(sortBy('proximity'))
        .use(paginate({ size: mapPlacesLimit.value }))
        .run(catchIf(all(usingDocument), (e) => e instanceof HttpAbortedError)),
    );
  };

  const onMapSearchInitial = async (keepRadius = false) => {
    mapPlacesDefault.value = false;
    await searchPlaces(keepRadius);
  };

  const onMapSearchDefaults = async () => {
    mapPlacesDefault.value = true;
    await fetchDefaultPlaces();
  };

  const onDebouncedMapSearchInitial = debounce(onMapSearchInitial);

  const onDebouncedMapSearchChange = debounce(() => {
    if (route.name === 'places.map') {
      let addressQuery: Dictionary | undefined;
      if (mapSearch.value.address) {
        addressQuery = addressToQuery(mapSearch.value.address);
      }

      router.replace({
        query: {
          t: route.query.t,
          ti: route.query.ti,
          q: mapSearch.value.search,
          c: mapSearch.value.category?.slug ?? mapSearch.value.category?.id,
          at: toStrCoordinates(mapCenter.value),
          z: mapZoom.value.toFixed(1),
          ...addressQuery,
        },
      });
    }
  });

  const onSearchChange = (search: MapPlacesSearchQuery) => {
    if (route.name !== 'places.map') {
      return;
    }

    mapSearch.value = search;
    if (hasMapSearch.value) {
      const place = mapSearch.value.place?.slug ?? mapSearch.value.place?.id;
      if (isNone(place)) {
        hasMapPlace.value = false;
        router.replace({ params: { place: '', view: 'search' } });
      } else {
        router.replace({ params: { place, view: 'details' } });
      }
    }

    if (hasMapSearch.value && !hasMapPlace.value) {
      mapLoading.value = true;
      if (mapSearch.value.address) {
        mapRef.value?.flyTo(addressToGeoCoordinates(mapSearch.value.address));
      }

      onDebouncedMapSearchInitial(false);
      if (mapRegionRef.value) {
        focusElement(mapRegionRef.value.$el);
      }
    } else if (!hasMapPlace.value) {
      mapPlaces.value = null;
      onMapSearchDefaults();
    }

    onDebouncedMapSearchChange();
  };

  const onFocusPlace = (place: Place) => {
    mapFocusedPlace.value = place;
  };

  const onBlurPlace = () => {
    mapFocusedPlace.value = null;
  };

  const onMapAutoMoving = () => {
    mapAutoMoving.value = true;
    mapMoved.value = false;
  };

  const onMapListToggle = (value: boolean) => {
    mapPlacesOpened.value = value;
  };

  const onMapSearchZone = () => {
    if (mapPlacesDefault.value) {
      onMapSearchDefaults();
    } else {
      onMapSearchInitial(true);
    }
  };

  const refreshPlaceReviewsSummary = async () => {
    await loadReviewsSummary(mapPlace.value!);
    triggerRef(mapPlace);
  };

  watch([mapCenter, mapZoom], () => {
    if (mapRef.value) {
      debounceSetMapMoved(
        Math.abs(mapZoom.value - mapZoomPrev.value) > 1
        || Math.abs(mapCenter.value.longitude - mapCenterPrev.value.longitude) > 0.06
        || Math.abs(mapCenter.value.latitude - mapCenterPrev.value.latitude) > 0.06,
      );
    }

    onDebouncedMapSearchChange();
  });

  const onRoutePlaceChange = async (placeId: string | string[] | undefined) => {
    if (!isNone(placeId) && typeof placeId === 'string') {
      hasMapPlace.value = true;
      mapPlace.value = await mapPlacesAction()
        .use(withReviewsSummary())
        .use(include('cover', 'images', 'sources', 'ownerOrganization.cover'))
        .use(filterBy('slug', placeId))
        .run(one());
      if (mapPlace.value) {
        mapRef.value?.flyTo(addressToGeoCoordinates(mapPlace.value.address));
      } else {
        // Close the place details if not found.
        router.replace({
          params: { place: '', view: 'search' },
        });
      }
    } else {
      hasMapPlace.value = false;
      mapPlace.value = null;
      if (mapPlaces.value === null) {
        // Trigger a search on place closing if not already searching.
        onSearchChange(mapSearch.value);
      }
    }
  };

  watch(() => route.params.place, onRoutePlaceChange);

  watch(accessibilityPrioritizeCategories, async () => {
    if (route.name !== 'places.map') {
      return;
    }

    if (hasMapSearch.value && !hasMapPlace.value) {
      onDebouncedMapSearchInitial(false);
    } else if (!hasMapPlace.value) {
      await onMapSearchDefaults();
    }
  });

  onBeforeMount(async () => {
    const { query } = route;

    const queryCenter = parseGeoCoordinates(query.at);
    if (queryCenter) {
      mapCenter.value = queryCenter;
    }

    if (typeof query.z === 'string') {
      const zoom = Number.parseFloat(query.z);
      if (!Number.isNaN(zoom) && zoom >= 1 && zoom <= 19) {
        mapZoom.value = Number.parseInt(query.z, 10);
      }
    }

    const queryAddress = addressFromQuery(query);
    if (queryAddress) {
      mapSearch.value = {
        address: queryAddress,
      };
    } else if (typeof query.c === 'string' && !isNone(toTrim((query.c)))) {
      mapSearch.value = {
        category: onlyPublishedCategories(await cachedCategories.value)
          .find((c) => c.id === query.c || c.slug === query.c),
      };
    } else if (typeof query.q === 'string' && !isNone(toTrim(query.q))) {
      mapSearch.value = {
        search: toTrim(query.q),
      };
    }

    if (!isNone(route.params.place)) {
      onRoutePlaceChange(route.params.place);
    } else if (!hasMapPlace.value && hasMapSearch.value) {
      onDebouncedMapSearchInitial(false);
    } else if (!hasMapPlace.value && !hasMapSearch.value) {
      onMapSearchDefaults();
    }
  });

  provideMapData({
    withMap: (callback: (map: Map) => void) => {
      const map = mapRef.value?.map();
      if (map) {
        callback(map);
      }
    },
    mapRef,
    mapRegionRef,
    mapScrollViewRef,
    mapTopRef,
    mapCardOpened,
    mapSearch,
    mapSearchPrependRef,
    mapSearchAppendRef,
    mapLoading,
    mapMoved,
    mapCenter,
    mapPlaces,
    mapPlacesLimit,
    mapPlacesTotal,
    mapPlacesMore,
    mapPlacesDefault,
    mapPlacesOpened,
    mapPlace,
    mapFocusedPlace,
    onMapAutoMoving,
    onMapListToggle,
    onMapSearchZone,
    onFocusPlace,
    onBlurPlace,
    onSearchChange,
    refreshPlaceReviewsSummary,
  });

  return {
    mapRef,
    mapRegionRef,
    mapScrollViewRef,
    mapTopRef,
    mapCardOpened,
    mapSearchPrependRef,
    mapSearchAppendRef,
    mapSearch,
    mapCenter,
    mapZoom,
    mapLoading,
    mapPlaces,
    mapPlace,
    hasMapPlace,
    onSearchChange,
  };
}
