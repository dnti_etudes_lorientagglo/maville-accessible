import isNil from '@/ts/utilities/common/isNil';
import FeatureName from '@/ts/utilities/features/featureName';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { computed, defineAsyncComponent } from 'vue';
import { useRoute } from 'vue-router';

type MapView = {
  component: any;
  validate?: (params: Dictionary) => boolean;
  markers?: boolean;
  addressMarker?: boolean;
  categories?: boolean;
};

const mapViews: Dictionary<MapView> = {
  search: {
    component: defineAsyncComponent(() => import('@/ts/views/places/map/MapSearchView.vue')),
    validate: (params) => isNil(params.place),
    markers: true,
    categories: true,
  },
  details: {
    component: defineAsyncComponent(() => import('@/ts/views/places/map/MapDetailsView.vue')),
    validate: (params) => !isNil(params.place) && typeof params.place === 'string',
    markers: true,
  },
  address: {
    component: defineAsyncComponent(() => import('@/ts/views/places/map/MapAddressView.vue')),
    addressMarker: true,
  },
  reviews: {
    component: defineAsyncComponent(() => import('@/ts/views/places/map/MapReviewsView.vue')),
    validate: (params) => FeatureName.REVIEWS.check() && !isNil(params.place) && typeof params.place === 'string',
    markers: true,
  },
  directions: {
    component: defineAsyncComponent(() => import('@/ts/views/places/map/MapDirectionsView.vue')),
    validate: (params) => isNil(params.place) || typeof params.place === 'string',
  },
};

export function validateMapViewParams(params: Dictionary) {
  if (typeof params.view !== 'string') {
    return false;
  }

  const view = mapViews[params.view];

  return view !== undefined && (
    !view.validate || view.validate(params)
  );
}

export default function useMapView() {
  const route = useRoute();

  return computed(() => (
    typeof route.params.view === 'string'
      ? mapViews[route.params.view]
      : undefined
  ));
}
