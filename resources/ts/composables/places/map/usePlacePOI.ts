import Place from '@/ts/api/models/place';
import { useLangStore } from '@/ts/stores/lang';
import addressToGeoCoordinates from '@/ts/utilities/places/addresses/addressToGeoCoordinates';
import distance from '@/ts/utilities/places/map/distance';
import formatDistance from '@/ts/utilities/places/map/formatDistance';
import placeToPOI from '@/ts/utilities/places/map/placeToPOI';
import { AccurateGeoLocation } from '@/ts/utilities/places/map/types';
import { computed, MaybeRef, unref } from 'vue';

export default (
  placeRef: MaybeRef<Place>,
  locationRef?: MaybeRef<AccurateGeoLocation | null | undefined>,
) => {
  const lang = useLangStore();

  return computed(() => placeToPOI(unref(placeRef), {
    prepend: unref(locationRef) ? [formatDistance(
      distance(unref(locationRef)!, addressToGeoCoordinates(unref(placeRef).address)),
      lang.locale,
    )] : [],
  }));
};
