import focusElement from '@/ts/utilities/dom/focusElement';
import scrollTo from '@/ts/utilities/dom/scrollTo';
import { nextTick } from 'vue';

export default async function focusFaqSection(sectionId: string) {
  const element = document.getElementById(sectionId);
  if (element) {
    focusElement(element);
    await nextTick(() => {
      scrollTo(element, 124, null);
    });
  }
}
