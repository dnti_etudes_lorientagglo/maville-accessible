import normalize from '@/ts/utilities/strings/normalize';
import toKebabCase from '@/ts/utilities/strings/toKebabCase';
import { computed, Ref } from 'vue';

export default function useFaqSectionTitle(title: Ref<string>) {
  return computed(() => toKebabCase(normalize(title.value)));
}
