import ActivityTypeCode from '@/ts/api/enums/activityTypeCode';
import Activity from '@/ts/api/models/activity';
import useRouteActivityType from '@/ts/composables/activities/useRouteActivityType';
import MainFeature from '@/ts/utilities/features/mainFeature';
import { computed, MaybeRef, unref } from 'vue';

export default function useActivityFeature(activity: MaybeRef<Activity | null> = null) {
  const activityType = useRouteActivityType();

  return computed(() => {
    const code = unref(activity)?.activityType?.code;

    return (
      ActivityTypeCode.ACTIVITIES.match(code ?? activityType.value.value)
        ? MainFeature.ACTIVITIES
        : MainFeature.EVENTS
    );
  });
}
