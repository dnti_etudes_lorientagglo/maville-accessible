import useRouteActivityType from '@/ts/composables/activities/useRouteActivityType';
import { Optional } from '@/ts/utilities/types/optional';
import { ModelInstance } from '@foscia/core';
import { computed, MaybeRef, unref } from 'vue';

export default function useActivityRoute(
  activityRef: MaybeRef<ModelInstance>,
  instanceRef?: Optional<MaybeRef<Optional<ModelInstance>>>,
) {
  const activityType = useRouteActivityType();

  return computed(() => {
    const instance = unref(instanceRef);
    const activity = unref(activityRef);

    return {
      name: instance
        ? `${activityType.value.value}.about.read`
        : `${activityType.value.value}.read`,
      params: {
        ...(instance ? {
          type: instance.$model.$type,
          id: instance.slug ?? instance.id,
        } : {}),
        activity: activity.slug ?? activity.id,
      },
    };
  });
}
