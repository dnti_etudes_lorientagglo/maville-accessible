import ActivityTypeCode from '@/ts/api/enums/activityTypeCode';
import { computed } from 'vue';
import { useRoute } from 'vue-router';

export default function useRouteActivityType() {
  const route = useRoute();

  return computed(() => route.meta.activityType as ActivityTypeCode);
}
