import useRouteActivityType from '@/ts/composables/activities/useRouteActivityType';
import { Optional } from '@/ts/utilities/types/optional';
import { ModelInstance } from '@foscia/core';
import { computed, MaybeRef, unref } from 'vue';

export default function useActivitiesRoute(
  instanceRef: Optional<MaybeRef<Optional<ModelInstance>>>,
) {
  const activityType = useRouteActivityType();

  return computed(() => {
    const instance = unref(instanceRef);

    return {
      name: instance
        ? `${activityType.value.value}.about`
        : activityType.value.value,
      params: instance ? {
        type: instance.$model.$type,
        id: instance.slug ?? instance.id,
      } : {},
    };
  });
}
