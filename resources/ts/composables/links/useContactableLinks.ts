import { ContactableValues } from '@/ts/api/composables/contactable';
import { LinkObject } from '@/ts/composables/links/types';
import useEmailLink from '@/ts/composables/links/useEmailLink';
import usePhoneLink from '@/ts/composables/links/usePhoneLink';
import { Optional } from '@/ts/utilities/types/optional';
import { computed, MaybeRef, unref } from 'vue';

export default function useContactableLinks(contactable: MaybeRef<Optional<ContactableValues>>) {
  const phoneLink = usePhoneLink(computed(() => unref(contactable)?.phone));
  const emailLink = useEmailLink(computed(() => unref(contactable)?.email));

  return computed(() => [
    phoneLink.value,
    emailLink.value,
  ].filter((l) => l) as LinkObject[]);
}
