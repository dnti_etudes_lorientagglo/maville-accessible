import { LinkObject } from '@/ts/composables/links/types';
import isNil from '@/ts/utilities/common/isNil';
import { Optional } from '@/ts/utilities/types/optional';
import { mdiPhoneOutline } from '@mdi/js';
import { formatPhone, makePhone } from 'v-phone-input';
import { computed, MaybeRef, unref } from 'vue';
import { useI18n } from 'vue-i18n';

export default function usePhoneLink(phoneRef: MaybeRef<Optional<string>>) {
  const i18n = useI18n();

  return computed(() => {
    const phone = unref(phoneRef);
    const formattedPhone = isNil(phone) ? null : formatPhone(makePhone(phone), 'national');

    return isNil(phone) ? null : {
      icon: mdiPhoneOutline,
      label: formattedPhone,
      ariaLabel: i18n.t('actions.scoped.call', { phone: formattedPhone }),
      href: `tel:${phone}`,
    } as LinkObject;
  });
}
