import { IconValue } from '@/ts/icons/makeSvgIcon';

export type LinkObject = {
  network?: string;
  icon: IconValue;
  href: string;
  label: string;
  ariaLabel?: string;
  openInNew?: boolean;
};
