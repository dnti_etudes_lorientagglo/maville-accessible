import { LinkableValues } from '@/ts/api/composables/linkable';
import { LinkObject } from '@/ts/composables/links/types';
import facebookIcon from '@/ts/icons/facebookIcon';
import instagramIcon from '@/ts/icons/instagramIcon';
import linkedinIcon from '@/ts/icons/linkedinIcon';
import youtubeIcon from '@/ts/icons/youtubeIcon';
import isNone from '@/ts/utilities/common/isNone';
import mapValues from '@/ts/utilities/objects/mapValues';
import normalizeURL from '@/ts/utilities/strings/normalizeURL';
import { Optional } from '@/ts/utilities/types/optional';
import { mdiWeb } from '@mdi/js';
import { computed, MaybeRef, unref } from 'vue';
import { useI18n } from 'vue-i18n';

export default function useLinkableLinks(linkable: MaybeRef<Optional<LinkableValues>>) {
  const i18n = useI18n();

  const icons = {
    facebook: facebookIcon,
    instagram: instagramIcon,
    linkedin: linkedinIcon,
    youtube: youtubeIcon,
    website: mdiWeb,
  };

  return computed(() => Object.values(mapValues(unref(linkable)?.links ?? {}, (href, network) => {
    const url = normalizeURL(href);

    return isNone(url) ? undefined : {
      network,
      icon: icons[network],
      label: i18n.t(`forms.links.${network}`),
      href: url.toString(),
      openInNew: true,
    };
  })).filter((l) => l) as LinkObject[]);
}
