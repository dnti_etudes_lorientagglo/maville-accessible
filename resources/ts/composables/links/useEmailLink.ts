import { LinkObject } from '@/ts/composables/links/types';
import isNil from '@/ts/utilities/common/isNil';
import { Optional } from '@/ts/utilities/types/optional';
import { mdiEmailArrowRightOutline } from '@mdi/js';
import { computed, MaybeRef, unref } from 'vue';
import { useI18n } from 'vue-i18n';

export default function useEmailLink(email: MaybeRef<Optional<string>>) {
  const i18n = useI18n();

  return computed(() => (isNil(unref(email)) ? null : {
    icon: mdiEmailArrowRightOutline,
    label: unref(email),
    ariaLabel: i18n.t('actions.scoped.mail', { email: unref(email) }),
    href: `mailto:${unref(email)}`,
  } as LinkObject));
}
