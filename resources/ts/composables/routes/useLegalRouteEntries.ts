import { RouteEntry } from '@/ts/utilities/routes/types';
import { computed } from 'vue';
import { useI18n } from 'vue-i18n';

export default function useLegalRouteEntries() {
  const i18n = useI18n();

  return computed(() => [
    {
      to: { name: 'legal.site-map' },
      title: i18n.t('navigation.items.siteMap'),
    },
    {
      to: { name: 'legal.legal-notice' },
      title: i18n.t('navigation.items.legalNotice'),
    },
    {
      to: { name: 'legal.terms-of-use' },
      title: i18n.t('navigation.items.termsOfUse'),
    },
    {
      to: { name: 'legal.privacy-policy' },
      title: i18n.t('navigation.items.privacyPolicy'),
    },
    {
      to: { name: 'legal.accessibility-declaration' },
      title: i18n.t('navigation.items.accessibilityDeclaration'),
    },
  ] as RouteEntry[]);
}
