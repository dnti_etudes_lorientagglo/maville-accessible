import wrap from '@/ts/utilities/arrays/wrap';
import FeatureName from '@/ts/utilities/features/featureName';
import MainFeature from '@/ts/utilities/features/mainFeature';
import { RouteEntry } from '@/ts/utilities/routes/types';
import { computed } from 'vue';
import { useI18n } from 'vue-i18n';

export default function useMainRouteEntries() {
  const i18n = useI18n();

  return computed(() => [
    ...wrap(FeatureName.PLACES.when({
      icon: MainFeature.PLACES.props.icon,
      color: MainFeature.PLACES.props.color,
      to: { name: 'places.map', params: { view: 'search' } },
      title: i18n.t('navigation.items.map'),
    })),
    ...wrap(FeatureName.EVENTS.when({
      icon: MainFeature.EVENTS.props.icon,
      color: MainFeature.EVENTS.props.color,
      to: { name: 'events' },
      title: i18n.t('navigation.items.events'),
    })),
    ...wrap(FeatureName.ACTIVITIES.when({
      icon: MainFeature.ACTIVITIES.props.icon,
      color: MainFeature.ACTIVITIES.props.color,
      to: { name: 'activities' },
      title: i18n.t('navigation.items.activities'),
    })),
    ...wrap(FeatureName.ARTICLES.when({
      icon: MainFeature.ARTICLES.props.icon,
      color: MainFeature.ARTICLES.props.color,
      to: { name: 'articles' },
      title: i18n.t('navigation.items.articles'),
    })),
  ] as RouteEntry[]);
}
