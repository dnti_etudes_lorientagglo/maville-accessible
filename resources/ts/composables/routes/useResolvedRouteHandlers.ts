import router from '@/ts/router';
import resolveRoute from '@/ts/utilities/routes/resolveRoute';
import { MaybeRef, unref } from 'vue';

export default function useResolvedRouteHandlers(href: MaybeRef<string>) {
  const handleEvent = (event: KeyboardEvent | MouseEvent) => {
    if (event instanceof MouseEvent && event.button !== 0) {
      return false;
    }

    const route = resolveRoute(router, unref(href));
    if (typeof route !== 'string') {
      event.preventDefault();

      router.push(route);

      return true;
    }

    return false;
  };

  const onClick = (event: MouseEvent) => !handleEvent(event);
  const onKeydown = (event: KeyboardEvent) => (event.key === 'Enter' ? !handleEvent(event) : false);

  return { onClick, onKeydown };
}
