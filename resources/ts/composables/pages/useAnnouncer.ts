import { usePagesStore } from '@/ts/stores/pages';
import { useRoutingStore } from '@/ts/stores/routing';
import isNone from '@/ts/utilities/common/isNone';
import focusElement from '@/ts/utilities/dom/focusElement';
import scrollTo from '@/ts/utilities/dom/scrollTo';
import { Arrayable } from '@/ts/utilities/types/arrayable';
import { nextTick, onMounted } from 'vue';

type AnnouncerOptions = {
  title: Arrayable<string> | null;
  focus: HTMLElement | null;
  scrollTo: HTMLElement | number | null;
};

export default function useAnnouncer() {
  const scrollOnInit = window.scrollY;
  const routing = useRoutingStore();
  const pages = usePagesStore();

  const announce = (options: Partial<AnnouncerOptions> = {}) => {
    if (options.title !== null) {
      pages.defineTitle(options.title);
    }

    if (options.focus !== null) {
      const element = options.focus || document.getElementById('app') as HTMLElement;

      focusElement(element);
    }

    if (options.scrollTo !== null) {
      const position = options.scrollTo || routing.savedPosition?.top || 0;
      if (!isNone(options.scrollTo) || scrollOnInit === window.scrollY) {
        scrollTo(position, 0, position === 0 ? null : 'smooth');
      }
    }
  };

  const announceAfterTick = async (options: Partial<AnnouncerOptions> = {}) => {
    await nextTick();

    announce(options);
  };

  const announceOnMounted = (options: Partial<AnnouncerOptions> = {}) => {
    onMounted(() => {
      announce(options);
    });
  };

  return {
    announce,
    announceAfterTick,
    announceOnMounted,
  };
}
