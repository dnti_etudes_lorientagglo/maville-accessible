import { Optional } from '@/ts/utilities/types/optional';
import { computed, MaybeRef, unref } from 'vue';

export default function useClickListener(
  listener?: Optional<MaybeRef<Optional<(e: Event) => void>>>,
) {
  return computed(() => (unref(listener) ? {
    onClick: (e: Event) => {
      unref(listener)?.(e);
    },
    onKeydown: (e: KeyboardEvent) => {
      if (e.key === ' ' || e.key === 'Enter') {
        e.stopPropagation();
        unref(listener)?.(e);
      }
    },
  } : {}));
}
