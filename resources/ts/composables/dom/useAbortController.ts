import { ref } from 'vue';

export default function useAbortController() {
  const abortController = ref(null as AbortController | null);

  return {
    abortController,
    abortPrevious() {
      abortController.value?.abort();
      abortController.value = new AbortController();
    },
  };
}
