import { Optional } from '@/ts/utilities/types/optional';
import { isRef, onBeforeUnmount, onMounted, Ref, unref, watch } from 'vue';

export default function useEventListener<E extends Event>(
  target: Ref<Optional<EventTarget>> | EventTarget,
  event: string,
  handler: (e: E) => any,
) {
  if (isRef(target)) {
    watch(target, (value, oldValue) => {
      oldValue?.removeEventListener(event, handler as EventListener);
      value?.addEventListener(event, handler as EventListener);
    }, { immediate: true });
  } else {
    onMounted(() => {
      target.addEventListener(event, handler as EventListener);
    });
  }

  onBeforeUnmount(() => {
    unref(target)?.removeEventListener(event, handler as EventListener);
  });
}
