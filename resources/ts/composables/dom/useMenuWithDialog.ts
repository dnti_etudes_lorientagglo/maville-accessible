import type AppMenu from '@/ts/components/common/menu/AppMenu.vue';
import { computed, ref } from 'vue';

type AppMenuRef = InstanceType<typeof AppMenu>;

export default function useMenuWithDialog() {
  const menuRef = ref(null as AppMenuRef | null);

  const dialogActivatorRef = computed(() => menuRef.value?.activatorEl ?? null);
  const dialogActivatorProps = computed(() => (menuRef.value ? {
    'aria-expanded': menuRef.value.activatorProps['aria-expanded'],
    'aria-haspopup': undefined,
  } : {}));

  return {
    menuRef,
    dialogActivatorRef,
    dialogActivatorProps,
  };
}
