import useEventListener from '@/ts/composables/dom/useEventListener';
import resolveEventTarget from '@/ts/utilities/dom/resolveEventTarget';
import { Optional } from '@/ts/utilities/types/optional';
import { nextTick, Ref } from 'vue';

export default function useInfiniteScroll(
  element: Ref<Optional<Document | HTMLElement>> | Document | HTMLElement,
  callback: () => void,
  immediate = false,
) {
  useEventListener(element, 'scroll', async (event: Event) => {
    const scrollView = resolveEventTarget(event);
    const threshold = scrollView.clientHeight * 0.75;
    const remaining = scrollView.scrollHeight - (scrollView.scrollTop + scrollView.clientHeight);
    if (remaining < threshold) {
      await nextTick();
      callback();
    }
  });

  if (immediate) {
    callback();
  }
}
