import { useAuthStore } from '@/ts/stores/auth';
import { mdiBellOutline, mdiBellRingOutline } from '@mdi/js';
import { computed } from 'vue';
import { useI18n } from 'vue-i18n';

export default function useNotificationsState() {
  const i18n = useI18n();
  const auth = useAuthStore();

  const count = computed(() => (auth.auth ? auth.user.unreadNotificationsCount : 0) ?? 0);

  return computed(() => ({
    unread: count.value > 0,
    title: i18n.t('notifications.common.front.unreadCount', { n: count.value }),
    icon: count.value ? mdiBellRingOutline : mdiBellOutline,
  }));
}
