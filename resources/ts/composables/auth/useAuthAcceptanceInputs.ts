import useInput from '@/ts/composables/forms/useInput';
import { useI18n } from 'vue-i18n';

export default function useAuthAcceptanceInputs() {
  const i18n = useI18n();

  const termsInput = useInput({
    name: 'terms',
    value: false,
    rules: (value) => !!value || i18n.t('views.auth.signUp.terms.rule'),
  });

  return {
    termsInput,
  };
}
