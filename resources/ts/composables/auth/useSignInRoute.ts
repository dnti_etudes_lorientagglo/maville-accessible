import routeWithIntended from '@/ts/utilities/routes/routeWithIntended';
import { computed } from 'vue';
import { useRoute } from 'vue-router';

export default function useSignInRoute() {
  const route = useRoute();

  return computed(() => routeWithIntended({
    name: 'auth.sign-in',
  }, route.fullPath));
}
