import action from '@/ts/api/action';
import { GUEST_HOME, useAuthStore } from '@/ts/stores/auth';
import { useRoutingStore } from '@/ts/stores/routing';
import { useSnackbarStore } from '@/ts/stores/snackbar';
import { none } from '@foscia/core';
import { makePost } from '@foscia/http';
import { ref } from 'vue';
import { useI18n } from 'vue-i18n';
import { useRouter } from 'vue-router';

const loggingOut = ref(false);

export default function useLogout() {
  const router = useRouter();
  const i18n = useI18n();
  const auth = useAuthStore();
  const routing = useRoutingStore();
  const snackbar = useSnackbarStore();

  const logout = async () => {
    if (loggingOut.value) {
      return;
    }

    loggingOut.value = true;

    await router.push(GUEST_HOME);

    try {
      await action().use(makePost('/auth/logout')).run(none());
    } finally {
      auth.logoutUser();

      loggingOut.value = false;

      snackbar.toast(i18n.t('auth.logout.success'));
    }
  };

  return {
    logout: () => {
      if (!routing.preventedLeaving(() => logout())) {
        logout();
      }
    },
  };
}
