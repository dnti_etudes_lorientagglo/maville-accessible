import isNone from '@/ts/utilities/common/isNone';
import routeWithIntended, { INTENDED_QUERY_KEY } from '@/ts/utilities/routes/routeWithIntended';
import { computed } from 'vue';
import {
  RouteLocationNamedRaw,
  RouteLocationPathRaw,
  RouteLocationRaw,
  useRoute,
  useRouter,
} from 'vue-router';

export default function useIntended() {
  const router = useRouter();
  const route = useRoute();

  const hasIntended = computed(() => !isNone(route.query[INTENDED_QUERY_KEY]));

  const toWithIntended = (to: RouteLocationPathRaw | RouteLocationNamedRaw) => routeWithIntended(
    to,
    route.query[INTENDED_QUERY_KEY],
  );

  const redirectToIntended = (defaultTo: RouteLocationRaw) => {
    const intended = router.currentRoute.value.query[INTENDED_QUERY_KEY];
    if (!isNone(intended)) {
      return router.push(decodeURIComponent(intended as string));
    }

    return router.push(defaultTo);
  };

  return {
    hasIntended,
    toWithIntended,
    redirectToIntended,
  };
}
