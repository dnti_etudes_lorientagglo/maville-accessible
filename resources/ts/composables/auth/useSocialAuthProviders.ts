import env from '@/ts/env';
import facebookIcon from '@/ts/icons/facebookIcon';
import googleIcon from '@/ts/icons/googleIcon';
import { IconValue } from '@/ts/icons/makeSvgIcon';
import toCamelCase from '@/ts/utilities/strings/toCamelCase';
import toKebabCase from '@/ts/utilities/strings/toKebabCase';

export type SocialAuthProvider = {
  id: string;
  name: string;
  icon?: IconValue;
  color?: string;
  redirectURL: string;
};

const makeProvider = (
  name: string,
  icon?: IconValue,
  color?: string,
) => ({
  id: toCamelCase(name),
  name,
  icon,
  color,
  redirectURL: `${env.app.url}/auth/social/${toKebabCase(name)}/redirect`,
});

export const SOCIAL_AUTH_PROVIDERS = {
  google: makeProvider('Google', googleIcon, '#db3733'),
  facebook: makeProvider('Facebook', facebookIcon, '#0e70e9'),
  franceConnect: makeProvider('France Connect'),
};

export default function useSocialAuthProviders() {
  const providers = Object.values(SOCIAL_AUTH_PROVIDERS).filter(
    (n) => env.services.social.providers.indexOf(n.id) !== -1,
  );

  return { providers };
}
