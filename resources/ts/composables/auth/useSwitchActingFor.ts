import OrganizationMember from '@/ts/api/models/organizationMember';
import useResourceRouteOne from '@/ts/composables/resources/core/useResourceRouteOne';
import { useAppStore } from '@/ts/stores/app';
import { useAuthStore } from '@/ts/stores/auth';
import actingForOrganization from '@/ts/utilities/authorizations/actingForOrganization';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';
import { computed, nextTick } from 'vue';
import { useRouter } from 'vue-router';

export default function useSwitchActingFor() {
  const router = useRouter();
  const app = useAppStore();
  const auth = useAuthStore();

  const actingFor = computed(() => actingForOrganization());
  const canSeeActingFor = computed(() => (
    auth.auth
    && !auth.blocked
    && !hasPermissionOn('organizations.admin')
  ));
  const canSwitchActingFor = computed(() => (
    canSeeActingFor.value && auth.user.memberOfOrganizations.length > 1
  ));

  const switchActingFor = async (member: OrganizationMember) => {
    app.booting = true;

    await nextTick();

    await auth.switchActingFor(member);
    await router.push(useResourceRouteOne(member.organization));
    window.location.reload();
  };

  return {
    actingFor,
    canSeeActingFor,
    canSwitchActingFor,
    switchActingFor,
  };
}
