import action from '@/ts/api/action';
import useIntended from '@/ts/composables/auth/useIntended';
import { useAuthStore } from '@/ts/stores/auth';
import { useSnackbarStore } from '@/ts/stores/snackbar';
import { raw } from '@foscia/core';
import { makePost } from '@foscia/http';
import { useI18n } from 'vue-i18n';

type RegisterData = {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
};

export default function useRegister() {
  const i18n = useI18n();
  const snackbar = useSnackbarStore();
  const auth = useAuthStore();
  const { redirectToIntended } = useIntended();

  return {
    register: async (registerData: RegisterData) => {
      const { data } = await action()
        .use(makePost('/auth/register', {
          data: {
            firstName: registerData.firstName,
            lastName: registerData.lastName,
            email: registerData.email,
            password: registerData.password,
          },
        }))
        .run(raw((r) => r.json()));

      await auth.loginUser(data.id);

      await redirectToIntended({ name: 'account.me' });

      snackbar.toast(i18n.t('auth.register.success'));
    },
  };
}
