import action from '@/ts/api/action';
import useIntended from '@/ts/composables/auth/useIntended';
import { AUTH_HOME, useAuthStore } from '@/ts/stores/auth';
import { useSnackbarStore } from '@/ts/stores/snackbar';
import { raw } from '@foscia/core';
import { makePost } from '@foscia/http';
import { useI18n } from 'vue-i18n';

type LoginData = {
  email: string;
  password: string;
};

export default function useLogin() {
  const i18n = useI18n();
  const snackbar = useSnackbarStore();
  const auth = useAuthStore();
  const { redirectToIntended } = useIntended();

  return {
    login: async (loginData: LoginData) => {
      const { data } = await action()
        .use(makePost('/auth/login', {
          data: {
            email: loginData.email,
            password: loginData.password,
          },
        }))
        .run(raw((r) => r.json()));

      await auth.loginUser(data.id);

      await redirectToIntended(AUTH_HOME);

      snackbar.toast(i18n.t('auth.login.success'));
    },
  };
}
