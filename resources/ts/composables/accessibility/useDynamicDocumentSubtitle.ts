import isNil from '@/ts/utilities/common/isNil';
import isNone from '@/ts/utilities/common/isNone';
import { computed, MaybeRef, unref } from 'vue';
import { useI18n } from 'vue-i18n';

type DocumentSubtitleOptions = {
  total?: MaybeRef<number | null>;
  page?: MaybeRef<{ current: number; last: number; } | null>;
  search?: MaybeRef<string | null>;
  filters?: MaybeRef<number | null>;
};

export default function useDynamicDocumentSubtitle(options: DocumentSubtitleOptions) {
  const i18n = useI18n();

  const totalState = computed(() => ((total) => (
    isNil(total) ? null : i18n.t('navigation.state.total', { total }, total)
  ))(unref(options.total)));

  const pageState = computed(() => (([total, page]) => (
    isNil(page) || total === 0 ? null : i18n.t('navigation.state.paged', page)
  ))([
    unref(options.total), unref(options.page),
  ] as const));

  const searchState = computed(() => ((search) => (
    isNone(search) ? null : i18n.t('navigation.state.searched', { search })
  ))(unref(options.search)));

  const filterState = computed(() => ((filters) => (
    !filters ? null : i18n.t('navigation.state.filtered', { total: filters }, filters)
  ))(unref(options.filters)));

  return computed(() => [
    totalState.value,
    pageState.value,
    searchState.value,
    filterState.value,
  ].filter((v) => !isNil(v)));
}
