import { useAccessibilityStore } from '@/ts/stores/accessibility';
import { computed } from 'vue';

export default function useMediaAutoplayAttrs() {
  const accessibility = useAccessibilityStore();

  return computed(() => {
    if (accessibility.preferences.mediaAutoplay === 'on') {
      return { autoplay: true };
    }

    if (accessibility.preferences.mediaAutoplay === 'on-muted') {
      return { autoplay: true, muted: true };
    }

    return {};
  });
}
