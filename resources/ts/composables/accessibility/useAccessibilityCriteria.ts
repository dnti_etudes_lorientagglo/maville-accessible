import cachedAccessibilityCriteria from '@/ts/api/cached/cachedAccessibilityCriteria';
import AccessibilityCriteria from '@/ts/api/enums/accessibilityCriteria';
import { onMounted, ref } from 'vue';

export default function useAccessibilityCriteria() {
  const criteria = ref([] as AccessibilityCriteria[]);

  onMounted(async () => {
    criteria.value = await cachedAccessibilityCriteria.value;
  });

  return criteria;
}
