import { useAccessibilityStore } from '@/ts/stores/accessibility';
import FeatureName from '@/ts/utilities/features/featureName';
import { computed } from 'vue';

export default function useAccessibilityPrioritizeCategories() {
  const accessibility = useAccessibilityStore();

  return computed(() => (
    FeatureName.ACCESSIBILITY.check()
      ? accessibility.preferences.prioritizeCategories
      : []
  ));
}
