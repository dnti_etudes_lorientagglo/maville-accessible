import { useAccessibilityStore } from '@/ts/stores/accessibility';
import { computed, MaybeRef, unref } from 'vue';
import { useTheme } from 'vuetify';

export default function useReducedColor(
  color: MaybeRef<string>,
  options: { dark?: MaybeRef<boolean>; reduced?: MaybeRef<boolean>; } = {},
) {
  const theme = useTheme();
  const accessibility = useAccessibilityStore();

  return computed(() => {
    const reduced = unref(options.reduced);
    if (reduced !== undefined ? reduced : accessibility.preferences.reducedColor) {
      const dark = unref(options.dark) !== undefined
        ? unref(options.dark)
        : theme.current.value.dark;

      return dark ? '#90a4ae' : '#37474f';
    }

    return unref(color);
  });
}
