import { useLangStore } from '@/ts/stores/lang';
import isNil from '@/ts/utilities/common/isNil';
import formatNumber from '@/ts/utilities/math/formatNumber';
import { Optional } from '@/ts/utilities/types/optional';
import { computed, MaybeRef, unref } from 'vue';

export default function useRatingValue(rating: MaybeRef<Optional<number>>) {
  const lang = useLangStore();

  return computed(() => {
    const value = unref(rating);

    return isNil(value) ? null : formatNumber(value, lang.locale, {
      minimumFractionDigits: 1, maximumFractionDigits: 1,
    });
  });
}
