import { ref } from 'vue';

export default function useEmbedContainer() {
  const containerRef = ref(null as HTMLElement | null);

  const resizeEmbeddingFrame = () => {
    const iframe = window.frameElement as HTMLIFrameElement | null;
    if (iframe && containerRef.value) {
      iframe.height = `${containerRef.value.offsetHeight}`;
    }
  };

  return { containerRef, resizeEmbeddingFrame };
}
