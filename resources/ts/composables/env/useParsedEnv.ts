import { MaybeRef, ref, unref, UnwrapRef, watch } from 'vue';

export default function useParsedEnv<T, U, D = undefined>(
  envValue: MaybeRef<T>,
  parser: (value: T) => Promise<U>,
  defaultValue?: D,
) {
  const parsedValue = ref(defaultValue as U | D);

  watch(() => unref(envValue), async (value) => {
    parsedValue.value = (value ? await parser(value) : defaultValue) as UnwrapRef<U | D>;
  }, { immediate: true });

  return parsedValue;
}
