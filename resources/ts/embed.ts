import '@/assets/sass/app.scss';
import EmbedApp from '@/ts/EmbedApp.vue';
import registerEmbedPlugins from '@/ts/plugins/registerEmbedPlugins';
import { createApp } from 'vue';

/*
 |--------------------------------------------------------------------------
 | Embed views.
 |--------------------------------------------------------------------------
 |
 | This provides embed iframe for other web applications.
 | Supported parameters: `locale`, `theme`.
 | Each embed views support specific parameters (e.g. `dense`, `type`, `id`).
 |
 | ```html
 | <iframe
 |   src="<BASE_URL>/embed/reviews?type=places&id=<ID>&locale=fr-FR"
 |   height="0"
 |   width="100%"
 |   style="border: none;overflow:hidden;"
 | />
 | ```
 */

const app = createApp(EmbedApp);

registerEmbedPlugins(app);

app.mount('#app');
