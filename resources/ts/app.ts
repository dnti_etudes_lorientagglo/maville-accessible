// Import logo so it is managed by build tool.
import '@/assets/img/logo.png';
import '@/assets/sass/app.scss';
import App from '@/ts/App.vue';
import registerPlugins from '@/ts/plugins/registerPlugins';
import { createApp } from 'vue';

const app = createApp(App);

registerPlugins(app);

app.mount('#app');
