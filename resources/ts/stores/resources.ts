import { Dictionary } from '@/ts/utilities/types/dictionary';
import { defineStore } from 'pinia';
import { computed, ref } from 'vue';

export const useResourcesStore = defineStore('resources', () => {
  const invalidationKeys = ref({} as Partial<Dictionary<number>>);
  const invalidatingKeys = ref({} as Partial<Dictionary<boolean>>);

  const invalidating = computed(
    () => (type: string) => invalidatingKeys.value[type] ?? false,
  );
  const invalidationKey = computed(
    () => (type: string) => `${type}-${invalidationKeys.value[type] ?? 0}`,
  );

  const invalidateResource = (type: string) => {
    invalidatingKeys.value = { ...invalidatingKeys.value, [type]: true };
    invalidationKeys.value = {
      ...invalidationKeys.value,
      [type]: (invalidationKeys.value[type] ?? 0) + 1,
    };

    return () => {
      invalidatingKeys.value = { ...invalidatingKeys.value, [type]: false };
    };
  };

  return {
    invalidating,
    invalidationKey,
    invalidateResource,
  };
});
