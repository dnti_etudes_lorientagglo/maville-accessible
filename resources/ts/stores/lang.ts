import { i18nInstance } from '@/ts/plugins/i18n';
import decomposeLocale from '@/ts/utilities/lang/decomposeLocale';
import detectLocale from '@/ts/utilities/lang/detectLocale';
import localePreference from '@/ts/utilities/lang/localePreference';
import verifyLocale from '@/ts/utilities/lang/verifyLocale';
import { Optional } from '@/ts/utilities/types/optional';
import { defineStore } from 'pinia';
import { ref, triggerRef } from 'vue';

export type LangMessagesFetcher = (locale: string) => Promise<object>;

export const useLangStore = defineStore('locale', () => {
  let fetchMessages: LangMessagesFetcher;

  const locale = ref(detectLocale());
  const editionLocale = ref(locale.value);

  const changeEditionLocale = (newEditionLocale: string) => {
    editionLocale.value = newEditionLocale;
  };

  const applyLocale = async (newLocale: string) => {
    locale.value = newLocale;

    const [language] = decomposeLocale(newLocale);
    const messages = await fetchMessages(locale.value);
    i18nInstance.setLocaleMessage(language, messages);

    document.documentElement.lang = language;

    if (i18nInstance.locale.value === language) {
      triggerRef(i18nInstance.locale);
    } else {
      i18nInstance.locale.value = language;
    }
  };

  const changeLocale = async (newLocale: string) => {
    localePreference.value = newLocale;

    await applyLocale(newLocale);
  };

  const boot = async (useFetchMessages: LangMessagesFetcher) => {
    fetchMessages = useFetchMessages;

    await applyLocale(locale.value);
  };

  const bootEmbed = async (
    useFetchMessages: LangMessagesFetcher,
    useLocale?: Optional<string>,
  ) => {
    locale.value = verifyLocale(useLocale);

    await boot(useFetchMessages);
  };

  return {
    locale,
    editionLocale,
    changeLocale,
    changeEditionLocale,
    boot,
    bootEmbed,
  };
});
