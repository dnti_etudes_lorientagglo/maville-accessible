import { defineStore } from 'pinia';
import { ref } from 'vue';

export type SnackbarMessage = string | {
  message: string;
  icon: string;
};

export const useSnackbarStore = defineStore('snackbar', () => {
  const message = ref(null as SnackbarMessage | null);

  const toast = (content: SnackbarMessage) => {
    message.value = content;
  };

  const dismiss = () => {
    message.value = null;
  };

  return {
    message,
    toast,
    dismiss,
  };
});
