import vuetify from '@/ts/plugins/vuetify';
import themes from '@/ts/plugins/vuetify/themes';
import accessibilityPreferences, {
  AccessibilityPreferences,
  AccessibilityTheme,
  DEFAULT_ACCESSIBILITY_PREFERENCES,
} from '@/ts/utilities/accessibility/accessibilityPreferences';
import isNone from '@/ts/utilities/common/isNone';
import filterValues from '@/ts/utilities/objects/filterValues';
import { Optional } from '@/ts/utilities/types/optional';
import { defineStore } from 'pinia';
import { reactive, ref } from 'vue';

function useCustomDocumentProperty<T>(
  name: keyof CSSStyleDeclaration,
  value: T,
  defaultValue: T,
  cssValue: (v: T) => string = (v) => `${v}`,
) {
  if (value === defaultValue) {
    window.document.documentElement.style[name as any] = '';
    window.document.documentElement.classList.remove(`app-custom--${name}`);
  } else {
    window.document.documentElement.style[name as any] = cssValue(value);
    window.document.documentElement.classList.add(`app-custom--${name}`);
  }
}

function applyAccessibilityPreferences(preferences: AccessibilityPreferences) {
  vuetify.theme.global.name.value = Object.keys(themes).indexOf(preferences.theme) !== -1
    ? preferences.theme : 'light';

  useCustomDocumentProperty('fontSize', preferences.fontSize, 16, (v) => `${v}px`);
  useCustomDocumentProperty('letterSpacing', preferences.letterSpacing, 0, (v) => `${v}rem`);
  useCustomDocumentProperty('lineHeight', preferences.lineHeight, 1.5, (v) => `${v}`);
}

export const useAccessibilityStore = defineStore('accessibility', () => {
  const announcement = ref('');
  const preferences = reactive({
    ...DEFAULT_ACCESSIBILITY_PREFERENCES,
    ...filterValues(accessibilityPreferences.value, (value) => !isNone(value)),
  });

  const announce = (content: string) => {
    announcement.value = content;
  };

  const updatePreferences = (newPreferences: Partial<AccessibilityPreferences>) => {
    Object.assign(preferences, newPreferences);

    applyAccessibilityPreferences(preferences);

    accessibilityPreferences.value = preferences;
  };

  const resetPreferences = () => {
    updatePreferences(DEFAULT_ACCESSIBILITY_PREFERENCES);
  };

  const boot = () => {
    applyAccessibilityPreferences(preferences);
  };

  const bootEmbed = (useTheme?: Optional<string>) => {
    applyAccessibilityPreferences({
      ...DEFAULT_ACCESSIBILITY_PREFERENCES,
      theme: useTheme && Object.keys(themes).indexOf(useTheme) !== -1
        ? useTheme as AccessibilityTheme
        : 'light',
    });
  };

  return {
    announcement,
    preferences,
    announce,
    updatePreferences,
    resetPreferences,
    boot,
    bootEmbed,
  };
});
