import env from '@/ts/env';
import { i18nInstance } from '@/ts/plugins/i18n';
import { useAccessibilityStore } from '@/ts/stores/accessibility';
import wrap from '@/ts/utilities/arrays/wrap';
import { Arrayable } from '@/ts/utilities/types/arrayable';
import { defineStore } from 'pinia';
import { computed, ref, watch } from 'vue';

export const usePagesStore = defineStore('pages', () => {
  const accessibility = useAccessibilityStore();

  const titleChain = ref([] as string[]);

  const title = computed(() => {
    if (!titleChain.value.length) {
      return null;
    }

    const reversedTitleChain = [...titleChain.value].reverse();

    return reversedTitleChain.join(' | ');
  });

  const baseTitle = `${env.app.name}, ${env.app.territory}`;
  const documentTitle = computed(() => (
    title.value
      ? `${title.value} | ${baseTitle}`
      : baseTitle
  ));

  watch(title, () => {
    accessibility.announce(i18nInstance.t('navigation.pageLoaded', {
      page: title.value || i18nInstance.t('navigation.items.home'),
    }));
  });

  const defineTitle = (chain?: Arrayable<string>) => {
    titleChain.value = wrap(chain);

    document.title = documentTitle.value;
  };

  return {
    defineTitle,
    title,
  };
});
