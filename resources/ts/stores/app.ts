import cachedCategories from '@/ts/api/cached/cachedCategories';
import env from '@/ts/env';
import fetchMessages from '@/ts/plugins/i18n/fetchMessages';
import installMatomo from '@/ts/plugins/matomo/installMatomo';
import { useAccessibilityStore } from '@/ts/stores/accessibility';
import { useAuthStore } from '@/ts/stores/auth';
import { useLangStore } from '@/ts/stores/lang';
import { defineStore } from 'pinia';
import { ref } from 'vue';
import { Router } from 'vue-router';

export const useAppStore = defineStore('app', () => {
  const booted = ref(false);
  const booting = ref(false);
  const updated = ref(false);

  const boot = async (router: Router) => {
    booting.value = true;

    await useAuthStore().boot();
    await useLangStore().boot(fetchMessages);
    await useAccessibilityStore().boot();
    await cachedCategories.fetch();

    document.documentElement.removeAttribute('aria-busy');

    booting.value = false;
    booted.value = true;

    const matomoSiteId = env.services.trackers.matomo.siteId;
    if (matomoSiteId) {
      installMatomo({
        ...env.services.trackers.matomo,
        siteId: matomoSiteId,
        router,
      });
    }
  };

  const bootEmbed = async (query: URLSearchParams) => {
    booting.value = true;

    await useLangStore().bootEmbed(fetchMessages, query.get('locale'));
    await useAccessibilityStore().bootEmbed(query.get('theme'));

    document.documentElement.removeAttribute('aria-busy');

    booting.value = false;
    booted.value = true;
  };

  const markUpdated = () => {
    updated.value = true;
  };

  return {
    booted,
    booting,
    updated,
    boot,
    bootEmbed,
    markUpdated,
  };
});
