import { defineStore } from 'pinia';
import { computed, ref, shallowRef } from 'vue';
import { RouteLocationNormalized } from 'vue-router';

type ScrollPosition = {
  left: number;
  top: number;
};

const onBeforeUnload = (event: BeforeUnloadEvent) => {
  const message = 'You have unsaved changes, do you confirm leaving?';
  event.preventDefault();
  // eslint-disable-next-line no-param-reassign
  event.returnValue = message;

  return message;
};

export const useRoutingStore = defineStore('routing', () => {
  const savedPosition = ref<ScrollPosition | null>(null);
  const isNavigating = ref(false);
  const previousRoutes = ref([] as RouteLocationNormalized[]);
  const preventingLeaveMap = ref({});
  const preventedLeaveNext = shallowRef<Function | null>(null);
  const preventedLeaveActivator = shallowRef<Element | null>(null);

  const previousRoute = computed(() => previousRoutes.value[0] ?? null);
  const preventingLeave = computed(
    () => Object.values(preventingLeaveMap.value).indexOf(true) !== -1,
  );

  const savePosition = (position: ScrollPosition | null) => {
    savedPosition.value = position;
  };

  const navigating = (value: boolean) => {
    isNavigating.value = value;
  };

  const savePreviousRoute = (to: RouteLocationNormalized, from: RouteLocationNormalized) => {
    if (previousRoutes.value[0]?.fullPath === to.fullPath) {
      previousRoutes.value.shift();
    } else {
      previousRoutes.value.unshift(from);
    }
  };

  const preventLeaving = (id: string, shouldPreventLeave: boolean) => {
    preventingLeaveMap.value = { ...preventingLeaveMap.value, [id]: shouldPreventLeave };

    if (preventingLeave.value) {
      window.addEventListener('beforeunload', onBeforeUnload);
    } else {
      window.removeEventListener('beforeunload', onBeforeUnload);
    }
  };

  const preventedLeaving = (next: Function | null) => {
    if (preventingLeave.value) {
      preventedLeaveActivator.value = document.activeElement;
      preventedLeaveNext.value = next;

      return true;
    }

    return false;
  };

  const stopPreventLeaving = () => {
    preventingLeaveMap.value = {};
    preventedLeaveNext.value = null;
    preventedLeaveActivator.value = null;
    window.removeEventListener('beforeunload', onBeforeUnload);
  };

  const confirmPreventedLeaving = () => {
    if (preventedLeaveNext.value) {
      preventedLeaveNext.value();
      stopPreventLeaving();
    }
  };

  return {
    savedPosition,
    isNavigating,
    previousRoute,
    preventedLeaveNext,
    preventedLeaveActivator,
    savePosition,
    navigating,
    savePreviousRoute,
    preventLeaving,
    preventedLeaving,
    stopPreventLeaving,
    confirmPreventedLeaving,
  };
});
