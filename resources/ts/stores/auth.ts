import action from '@/ts/api/action';
import OrganizationMember from '@/ts/api/models/organizationMember';
import User from '@/ts/api/models/user';
import fillAndSave from '@/ts/api/utilities/fillAndSave';
import env from '@/ts/env';
import { include, ModelIdType, oneOrFail, query } from '@foscia/core';
import { param } from '@foscia/http';
import { defineStore } from 'pinia';
import { computed, ref } from 'vue';

export const GUEST_HOME = { name: 'home' };
export const AUTH_HOME = { name: 'account.me' };

export const useAuthStore = defineStore('auth', () => {
  const internalUser = ref(null as User | null);

  const id = computed(() => internalUser.value?.id);
  const user = computed(() => {
    if (!internalUser.value) {
      throw new Error('accessing null user is not permitted');
    }

    return internalUser.value!;
  });
  const guest = computed(() => !internalUser.value);
  const auth = computed(() => !!internalUser.value);
  const verified = computed(() => !!internalUser.value?.verified);
  const blocked = computed(() => !!internalUser.value?.blocked);

  const setUser = (instance: User | null) => {
    internalUser.value = instance;
  };

  const loginUser = async (userId: ModelIdType) => {
    const loggedUser = await action()
      .use(query(User, userId))
      .use(include([
        'actingForOrganization.organization',
        'actingForOrganization.organization.categories',
        'memberOfOrganizations.organization',
        'memberOfOrganizations.organization.categories',
      ]))
      .use(param('withNotificationsCount', true))
      .run(oneOrFail());

    setUser(loggedUser);
  };

  const logoutUser = () => {
    setUser(null);
  };

  const switchActingFor = async (member: OrganizationMember) => {
    await fillAndSave(user.value, {
      actingForOrganization: member,
    });
  };

  const boot = async () => {
    if (env.auth.id) {
      try {
        await loginUser(env.auth.id);
      } catch {
        // Probably a deleted user. This case should not occur.
      }
    }
  };

  return {
    id,
    user,
    guest,
    auth,
    verified,
    blocked,
    loginUser,
    logoutUser,
    switchActingFor,
    boot,
  };
});
