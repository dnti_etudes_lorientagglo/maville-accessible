import { ListItemOptions, ListItemParsed, ListItemRaw } from '@/ts/utilities/common/menu/types';
import get from '@/ts/utilities/objects/get';
import { isInstance } from '@foscia/core';
import { v4 as uuidV4 } from 'uuid';

function parseValue(raw: ListItemRaw, options: ListItemOptions) {
  const value = get(raw, options.itemValue ?? 'value');

  return value === undefined ? raw : value;
}

function parseId(raw: ListItemRaw, value: any) {
  if (typeof value === 'string' || typeof value === 'number') {
    return String(value);
  }

  if (isInstance(raw)) {
    return raw.id;
  }

  return uuidV4();
}

export default function parseItem(raw: ListItemRaw, options: ListItemOptions): ListItemParsed {
  const value = parseValue(raw, options);
  const title = get(raw, options.itemTitle ?? 'title') as string;
  const props = get(raw, options.itemProps ?? 'props');

  return {
    id: parseId(raw, value),
    value,
    title,
    props,
  };
}
