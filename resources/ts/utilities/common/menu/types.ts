import { ValueRetriever } from '@/ts/utilities/objects/get';
import { MaybeRef } from 'vue';

export type ListItemResolver<T> = T | ((raw: ListItemRaw) => T);

export type ListItemDividerRaw = {
  divider: true;
};

export type ListItemGroupRaw = {
  title: ListItemResolver<string>;
  items: ListItemRaw[];
};

export type ListItemValueRaw = any;

export type ListItemRaw = ListItemDividerRaw | ListItemGroupRaw | ListItemValueRaw;

export type ListItemParsed = {
  id: string;
  title: string;
  value: any;
  props: any;
};

export type ListItem = {
  raw: ListItemRaw;
  active: MaybeRef<boolean>;
  selected: MaybeRef<boolean>;
  disabled: MaybeRef<boolean | undefined>;
  parsed: {
    id: string;
    title: string;
    value: any;
    props: any;
    items?: ListItem[];
    divider?: boolean;
  };
};

export type ListItemOptions = {
  itemTitle?: ValueRetriever<any>;
  itemValue?: ValueRetriever<any>;
  itemDisabled?: ValueRetriever<any>;
  itemProps?: ValueRetriever<any>;
};
