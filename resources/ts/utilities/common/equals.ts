import { isSame } from '@foscia/core';
import { toRaw } from 'vue';

export default function equals(value: unknown, otherValue: unknown): boolean {
  if (Array.isArray(value)) {
    return Array.isArray(otherValue)
      && value.length === otherValue.length
      && value.every((v, i) => equals(v, otherValue[i]));
  }

  const rawValue = toRaw(value);
  const rawOtherValue = toRaw(otherValue);
  if (isSame(rawValue, rawOtherValue)) {
    return true;
  }

  if (rawValue instanceof Date && rawOtherValue instanceof Date) {
    return rawValue.getTime() === rawOtherValue.getTime();
  }

  if (!!rawValue && typeof rawValue === 'object') {
    return !!rawOtherValue
      && typeof rawOtherValue === 'object'
      && Object.keys(rawValue).length === Object.keys(rawOtherValue).length
      && Object.entries(rawValue).every(([k, v]) => {
        const otherV = rawOtherValue[k as keyof typeof rawOtherValue];

        return (typeof v === 'function' && typeof otherV === 'function')
          || equals(v, otherV);
      });
  }

  return rawValue === rawOtherValue;
}
