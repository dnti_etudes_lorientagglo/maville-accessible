import isNil from '@/ts/utilities/common/isNil';

export default function compare<T>(a: T, b: T): number {
  if (a === b) {
    return 0;
  }

  if (isNil(a)) {
    return -1;
  }

  if (isNil(b)) {
    return 1;
  }

  if (typeof a === 'string' && typeof b === 'string') {
    return a.localeCompare(b, 'standard', { sensitivity: 'case' });
  }

  return a < b ? -1 : 1;
}
