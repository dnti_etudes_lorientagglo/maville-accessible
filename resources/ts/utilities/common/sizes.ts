const sizes = {
  small: 400,
  normal: 600,
  large: 800,
  'x-large': 1200,
};

export type SizeName = keyof typeof sizes;

export default sizes;
