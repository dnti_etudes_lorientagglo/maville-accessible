import { RouteLocationRaw } from 'vue-router';

export type RouteEntry = {
  icon?: string;
  color?: string;
  to: RouteLocationRaw;
  title: string;
};
