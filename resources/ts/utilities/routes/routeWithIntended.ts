import { RouteLocationNamedRaw, RouteLocationPathRaw } from 'vue-router';

export const INTENDED_QUERY_KEY = 'redirect';

export default function routeWithIntended(
  to: RouteLocationPathRaw | RouteLocationNamedRaw,
  intended?: unknown,
) {
  return {
    ...to,
    query: {
      ...(to.query || {}),
      [INTENDED_QUERY_KEY]: typeof intended === 'string'
        ? encodeURIComponent(decodeURIComponent(intended))
        : undefined,
    },
  };
}
