import { ResolvedRoute } from '@/ts/utilities/routes/resolveRoute';

export default function isExternalRoute(route: ResolvedRoute) {
  return typeof route === 'string'
    && !['tel:'].some((scheme) => route.startsWith(scheme));
}
