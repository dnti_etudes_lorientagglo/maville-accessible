import env from '@/ts/env';
import ROUTE_NOT_FOUND_PATH from '@/ts/router/notFoundPathName';
import { RouteLocation, RouteLocationRaw, Router } from 'vue-router';

export type ResolvedRoute = string | RouteLocation;

export default function resolveRoute(router: Router, href: RouteLocationRaw) {
  if (typeof href !== 'string' || href.startsWith('/') || href.startsWith(env.app.url)) {
    const route = router.resolve(
      typeof href === 'string' ? href.replace(env.app.url, '') : href,
    );
    if (route.matched.length && route.matched[0].name !== ROUTE_NOT_FOUND_PATH) {
      return route;
    }
  }

  return href as string;
}
