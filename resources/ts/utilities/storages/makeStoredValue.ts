import isNil from '@/ts/utilities/common/isNil';
import isNone from '@/ts/utilities/common/isNone';

export type StoredValueOptions<T> = {
  storage?: Storage;
  defaultValue?: T;
};

export default function makeStoredValue<T = unknown>(
  key: string,
  options?: StoredValueOptions<T>,
) {
  const storage = options?.storage ?? window.localStorage;

  return {
    get value() {
      const value = storage.getItem(key);

      return isNone(value)
        ? options?.defaultValue as T
        : JSON.parse(value) as T;
    },
    set value(value: T) {
      if (!isNil(value)) {
        storage.setItem(key, JSON.stringify(value));
      } else {
        storage.removeItem(key);
      }
    },
  };
}
