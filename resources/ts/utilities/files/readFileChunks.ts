import { Awaitable } from '@/ts/utilities/types/awaitable';

export default function readFileChunks(
  file: Blob,
  callback: (chunk: string) => Awaitable<void>,
) {
  return new Promise<void>((resolve, reject) => {
    const fileSize = file.size;
    const chunkSize = 10 * 1024 * 1024; // 10MB
    let offset = 0;

    const reader = new FileReader();

    function readNext() {
      const fileSlice = file.slice(offset, offset + chunkSize);
      reader.readAsBinaryString(fileSlice);
    }

    reader.onload = async () => {
      offset += (reader.result as string).length;
      try {
        await callback(reader.result as unknown as string);
      } catch (error) {
        reject(error);

        return;
      }

      if (offset >= fileSize) {
        resolve();

        return;
      }

      readNext();
    };

    reader.onerror = (error) => {
      reject(error);
    };

    readNext();
  });
}
