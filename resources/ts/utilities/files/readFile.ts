function readFile(file: Blob): Promise<string | undefined>;

function readFile(file: Blob, readAs: 'readAsArrayBuffer'): Promise<ArrayBuffer | undefined>;

function readFile(file: Blob, readAs?: 'readAsText' | 'readAsArrayBuffer') {
  return new Promise<string | ArrayBuffer | undefined>((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = (event) => {
      resolve((event.target?.result ?? undefined) as string | ArrayBuffer | undefined);
    };

    reader.onerror = (error) => {
      reject(error);
    };

    reader[readAs ?? 'readAsText'](file);
  });
}

export default readFile;
