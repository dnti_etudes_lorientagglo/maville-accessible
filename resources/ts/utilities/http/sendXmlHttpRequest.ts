import XmlHttpRequestError from '@/ts/utilities/http/xmlHttpRequestError';
import { Dictionary } from '@/ts/utilities/types/dictionary';

type XmlHttpRequestOptions = {
  method?: 'GET' | 'POST' | 'PUT';
  data?: FormData | File;
  headers?: Dictionary;
  uploadProgressCallback?: (progress: number) => void;
  downloadProgressCallback?: (progress: number) => void;
};

export default function sendXmlHttpRequest(
  endpoint: string,
  options: XmlHttpRequestOptions,
) {
  return new Promise<XMLHttpRequest>((resolve, reject) => {
    const xhr = new XMLHttpRequest();

    xhr.upload.addEventListener('progress', (event) => {
      options.uploadProgressCallback?.(event.loaded / event.total);
    });

    xhr.addEventListener('progress', (event) => {
      options.downloadProgressCallback?.(event.loaded / event.total);
    });

    xhr.addEventListener('readystatechange', () => {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        if (xhr.status >= 200 && xhr.status < 300) {
          resolve(xhr);
        } else {
          reject(new XmlHttpRequestError(xhr));
        }
      }
    });

    xhr.open(options.method ?? 'POST', endpoint, true);

    Object.entries(options.headers ?? {}).forEach(([header, value]) => {
      xhr.setRequestHeader(header, value);
    });

    xhr.send(options.data);
  });
}
