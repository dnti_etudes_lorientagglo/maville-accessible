import AppError from '@/ts/errors/appError';

export default class XmlHttpRequestError extends AppError {
  public constructor(public readonly xhr: XMLHttpRequest) {
    super(`XMLHttpRequest error: ${xhr.statusText}`);
  }
}
