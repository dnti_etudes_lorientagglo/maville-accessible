import isNil from '@/ts/utilities/common/isNil';

export type ValueRetriever<T> = undefined | null | keyof T | string | ((v: T) => unknown);

export default function get<T>(
  value: T,
  key?: ValueRetriever<T>,
  defaultValue?: unknown,
): unknown {
  if (isNil(value)) {
    return defaultValue;
  }

  if (typeof key === 'function') {
    return key(value);
  }

  if (isNil(key) || typeof value !== 'object') {
    return value ?? defaultValue;
  }

  if (typeof key !== 'string') {
    return value![key] ?? defaultValue;
  }

  const [firstKey, ...rest] = key.split('.');
  if (!(firstKey in value!)) {
    return defaultValue;
  }

  const subValue = value![firstKey as keyof typeof value];
  if (rest.length) {
    return get(subValue, rest.join('.'), defaultValue);
  }

  return subValue;
}
