import { Dictionary } from '@/ts/utilities/types/dictionary';

export default function deepMapValues<T, U>(
  object: Dictionary<T>,
  callback: (value: T, key: string) => U,
): Dictionary<U> {
  return Object.entries(object).reduce((values, [k, v]) => ({
    ...values,
    [k]: v && typeof v === 'object'
      ? deepMapValues(v as Dictionary<T>, callback) as U
      : callback(v, k),
  }), {} as Dictionary<U>);
}
