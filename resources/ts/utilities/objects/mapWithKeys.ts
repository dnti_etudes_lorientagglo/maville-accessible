import { Dictionary } from '@/ts/utilities/types/dictionary';

export default function mapWithKeys<T, U extends {}>(
  object: Dictionary<T> | T[] | readonly T[],
  callback: (value: T, key: string, values: U) => U,
): U {
  return Object.entries(object).reduce((values, [k, v]) => ({
    ...values,
    ...callback(v, k, values),
  }), {} as U);
}
