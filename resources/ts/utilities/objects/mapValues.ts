import { Dictionary } from '@/ts/utilities/types/dictionary';

export default function mapValues<T, U, O extends {}>(
  object: O & Dictionary<T>,
  callback: (value: T, key: string & keyof O) => U,
): Record<string, U> {
  return Object.entries(object).reduce((values, [k, v]) => ({
    ...values,
    [k]: callback(v, k as keyof O & string),
  }), {} as Record<string, U>);
}
