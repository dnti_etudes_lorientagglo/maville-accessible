import mapWithKeys from '@/ts/utilities/objects/mapWithKeys';

export default function filterValues<T>(
  object: Record<string, T>,
  callback: (value: T, key: string) => boolean,
): Record<string, T> {
  return mapWithKeys(object, (value, key) => (callback(value, key) ? {
    [key]: value,
  } : {}));
}
