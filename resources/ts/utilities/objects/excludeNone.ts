import isNone from '@/ts/utilities/common/isNone';
import filterValues from '@/ts/utilities/objects/filterValues';

export default function excludeNone<T extends {}>(values: T) {
  return filterValues(values, (value) => !isNone(value)) as NonNullable<T>;
}
