export default function intersectTarget(target: Element) {
  return target.closest('.v-dialog .v-card-text') ?? document;
}
