import { nextTick } from 'vue';

export default async function selectInputText(element: HTMLInputElement) {
  await nextTick();
  element.select();
}
