import scrollableParent from '@/ts/utilities/dom/scrollableParent';

export default function scrollIntoViewIfVisible(element: HTMLElement) {
  const scrollview = scrollableParent(element);

  const bottom = scrollview?.getBoundingClientRect()?.bottom ?? window.innerHeight;
  const top = scrollview?.getBoundingClientRect()?.top ?? 0;

  if (element.getBoundingClientRect().bottom > bottom) {
    element.scrollIntoView(false);
  }

  if (element.getBoundingClientRect().top < top) {
    element.scrollIntoView();
  }
}
