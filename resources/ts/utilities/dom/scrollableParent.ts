export default function scrollableParent(element: Element | null): Element | null {
  if (element == null) {
    return null;
  }

  if (element.scrollHeight > element.clientHeight) {
    return element;
  }

  return scrollableParent(element.parentNode as Element | null);
}
