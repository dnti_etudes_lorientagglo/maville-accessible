export default function resolveEventTarget<E = HTMLElement>(event: Event) {
  const { target } = event;

  return (target instanceof Document ? target.documentElement : target) as unknown as E;
}
