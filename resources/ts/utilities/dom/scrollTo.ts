export default function scrollTo(
  position: HTMLElement | number,
  offsetTop = 0,
  behavior: ScrollBehavior | null = 'smooth',
) {
  let top: number;
  if (typeof position === 'number') {
    top = position;
  } else {
    top = position.getBoundingClientRect().top + window.scrollY;
  }

  window.scroll({
    behavior: behavior ?? undefined,
    top: top - offsetTop,
  });
}
