export default function focusElement(element: HTMLElement) {
  const previousTabIndex = element.getAttribute('tabindex');
  element.setAttribute('tabindex', '-1');
  element.focus();
  if (previousTabIndex !== null) {
    element.setAttribute('tabindex', previousTabIndex);
  } else {
    element.removeAttribute('tabindex');
  }
}
