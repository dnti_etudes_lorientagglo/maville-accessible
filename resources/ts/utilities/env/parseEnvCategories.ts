import cachedCategories from '@/ts/api/cached/cachedCategories';
import Category from '@/ts/api/models/category';
import onlyPublishedCategories from '@/ts/api/utilities/categories/onlyPublishedCategories';
import { Dictionary } from '@/ts/utilities/types/dictionary';

export default async function parseEnvCategories(envCategories: Dictionary[]) {
  const categories = onlyPublishedCategories(await cachedCategories.value);

  return envCategories
    .map((envCategory) => categories.find((c) => c.id === envCategory.id))
    .filter((category) => !!category) as Category[];
}
