import Media from '@/ts/api/models/media';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { fill } from '@foscia/core';

export default async function parseEnvMedia(envMedia: Dictionary) {
  return fill(new Media(), envMedia);
}
