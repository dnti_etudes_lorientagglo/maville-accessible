import isFunction from '@/ts/utilities/functions/isFunction';
import { AnyFunction } from '@/ts/utilities/types/anyFunction';

export default function value<T, A extends unknown[]>(
  valueOrCallable: T,
  ...params: A
): T extends AnyFunction ? ReturnType<T> : T {
  if (isFunction(valueOrCallable)) {
    return valueOrCallable(...params);
  }

  return valueOrCallable as any;
}
