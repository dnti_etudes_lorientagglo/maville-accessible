export default function once<T>(computeValue: () => T) {
  let hasCachedReturnValue = false;
  let cachedReturnValue = null as T;

  const fetch = () => {
    if (!hasCachedReturnValue) {
      cachedReturnValue = computeValue();
      hasCachedReturnValue = true;
    }

    return cachedReturnValue;
  };

  return {
    fetch,
    get value() {
      return fetch();
    },
    get cached() {
      return hasCachedReturnValue;
    },
    forget() {
      hasCachedReturnValue = false;
      cachedReturnValue = null as T;
    },
  };
}
