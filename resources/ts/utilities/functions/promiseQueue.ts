type PromiseGenerator = () => Promise<void>;

export default async function promiseQueue(promises: PromiseGenerator[]): Promise<void> {
  return promises.reduce(async (prev, current) => {
    await prev;

    return current();
  }, Promise.resolve());
}
