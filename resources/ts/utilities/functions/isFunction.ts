import { AnyFunction } from '@/ts/utilities/types/anyFunction';

export default function isFunction(value: unknown): value is AnyFunction {
  return typeof value === 'function';
}
