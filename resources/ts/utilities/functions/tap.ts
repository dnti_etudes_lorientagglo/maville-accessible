export default function tap<T>(value: T, callback: (v: T) => void) {
  callback(value);

  return value;
}
