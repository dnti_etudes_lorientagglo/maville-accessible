import value from '@/ts/utilities/functions/value';

export default function when<T, TR>(
  condition: T,
  trueCallback: (value: T) => TR,
) {
  const evaluation = value(condition);
  if (evaluation) {
    trueCallback(evaluation);
  }
}
