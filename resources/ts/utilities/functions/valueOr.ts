import isNone from '@/ts/utilities/common/isNone';
import { Optional } from '@/ts/utilities/types/optional';

export default function valueOr<T>(value: Optional<T>, defaultValue: T) {
  return isNone(value) ? defaultValue : value;
}
