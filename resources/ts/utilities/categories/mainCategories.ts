import Category from '@/ts/api/models/category';
import onlyPublished from '@/ts/api/utilities/publishable/onlyPublished';

export default function mainCategories(allCategories: Category[]) {
  const publishedCategories = onlyPublished(allCategories);

  const categories = [] as Category[];

  publishedCategories.forEach((category) => {
    const isChildOrSoloParent = !category.isRoot || !publishedCategories.some(
      (otherCategory) => (otherCategory.parents ?? []).some(
        (parentCategory) => parentCategory.id === category.id,
      ),
    );
    if (isChildOrSoloParent) {
      categories.push(category);
    }
  });

  return categories;
}
