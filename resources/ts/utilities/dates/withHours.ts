import tap from '@/ts/utilities/functions/tap';

export default function withHours(date: Date, hours = 0, minutes = 0, seconds = 0, ms = 0) {
  return tap(new Date(date.getTime()), (clone) => {
    clone.setHours(hours, minutes, seconds, ms);
  });
}
