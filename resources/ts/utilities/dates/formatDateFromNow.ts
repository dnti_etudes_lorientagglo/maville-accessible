import formatDateFrom from '@/ts/utilities/dates/formatDateFrom';

export default function formatDateFromNow(date: Date, locale: string) {
  return formatDateFrom(date, new Date(), locale);
}
