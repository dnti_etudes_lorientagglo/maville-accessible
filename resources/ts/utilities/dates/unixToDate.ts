import isNone from '@/ts/utilities/common/isNone';
import { Optional } from '@/ts/utilities/types/optional';

export default function unixToDate(unix?: Optional<number>) {
  if (isNone(unix)) {
    return null;
  }

  const date = new Date();
  date.setTime(unix);

  return date;
}
