import isNone from '@/ts/utilities/common/isNone';
import { Optional } from '@/ts/utilities/types/optional';

export default function parseStandardDate<T extends Optional<string>>(
  date: T,
): T extends null | undefined ? undefined : Date {
  if (isNone(date)) {
    return undefined as any;
  }

  const [y, m, d] = date.split('-');

  return new Date(
    Number.parseInt(y, 10),
    Number.parseInt(m, 10) - 1,
    Number.parseInt(d, 10),
  ) as any;
}
