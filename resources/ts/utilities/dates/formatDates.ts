import formatDateUsing, { DateFormatOptions } from '@/ts/utilities/dates/formatDateUsing';

export default function formatDates(dates: Date[], locale: string, options: DateFormatOptions) {
  const nextYear = new Date();
  nextYear.setFullYear(nextYear.getFullYear() + 1);

  const year = dates.some((date) => (
    date >= nextYear
    || date < new Date()
    || dates.some((o) => date.getFullYear() !== o.getFullYear())
  ));

  return dates.map((d) => formatDateUsing(d, locale, { ...options, year }));
}
