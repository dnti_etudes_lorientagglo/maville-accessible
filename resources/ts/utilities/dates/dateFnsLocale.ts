import { i18nInstance } from '@/ts/plugins/i18n';

export default function dateFnsLocale(locale: string) {
  return (i18nInstance.messages.value as any)[locale].$dateFns;
}
