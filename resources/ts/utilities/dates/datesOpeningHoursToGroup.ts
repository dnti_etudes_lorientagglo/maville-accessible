import formatStandardDate from '@/ts/utilities/dates/formatStandardDate';
import { DateOpeningHour, DateOpeningHourGroup } from '@/ts/utilities/dates/types';
import tap from '@/ts/utilities/functions/tap';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { Optional } from '@/ts/utilities/types/optional';

export default function datesOpeningHoursToGroup(dates: Optional<DateOpeningHour[]>) {
  return Object.values((dates ?? []).reduce((values, { date, start, end }) => tap(values, () => {
    const dateString = formatStandardDate(date);
    if (!values[dateString]) {
      // eslint-disable-next-line no-param-reassign
      values[dateString] = { date, hours: [] };
    }

    values[dateString].hours.push({ start, end });
  }), {} as Dictionary<DateOpeningHourGroup>));
}
