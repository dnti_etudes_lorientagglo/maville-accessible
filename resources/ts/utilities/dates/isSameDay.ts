import formatStandardDate from '@/ts/utilities/dates/formatStandardDate';

export default function isSameDay(a: Date, b: Date) {
  return formatStandardDate(a) === formatStandardDate(b);
}
