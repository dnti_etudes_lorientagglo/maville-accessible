import decomposeLocale from '@/ts/utilities/lang/decomposeLocale';

type FormatDateOptions = {
  time?: boolean;
};

export default function formatDate(date: Date, locale: string, options?: FormatDateOptions) {
  if (options?.time) {
    return date.toLocaleString(decomposeLocale(locale)[0]);
  }

  return date.toLocaleDateString(decomposeLocale(locale)[0]);
}
