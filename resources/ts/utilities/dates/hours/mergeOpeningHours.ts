import clearOpeningHours from '@/ts/utilities/dates/hours/clearOpeningHours';
import sortOpeningHours from '@/ts/utilities/dates/hours/sortOpeningHours';
import isoWeekdays from '@/ts/utilities/dates/isoWeekdays';
import { IsoWeekday, WeekdayOpeningHour } from '@/ts/utilities/dates/types';

export default function mergeOpeningHours(
  weekdays: IsoWeekday[],
  prevOpeningHours: WeekdayOpeningHour[],
  nextOpeningHours: WeekdayOpeningHour[],
) {
  const mergedOpeningHours = [] as WeekdayOpeningHour[];

  isoWeekdays().forEach((weekday) => {
    const nextWeekdayOpeningHours = nextOpeningHours.filter((h) => h.weekday === weekday);

    mergedOpeningHours.push(...(
      weekdays.indexOf(weekday) !== -1
        ? nextWeekdayOpeningHours
        : prevOpeningHours.filter((h) => h.weekday === weekday)
    ));
  });

  return sortOpeningHours(clearOpeningHours(mergedOpeningHours));
}
