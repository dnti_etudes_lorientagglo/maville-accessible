export default function timeToSeconds(time: string) {
  const [hours, minutes, seconds] = time.split(':');

  return (Number(hours || 0) * 60 * 60) + (Number(minutes || 0) * 60) + Number(seconds || 0);
}
