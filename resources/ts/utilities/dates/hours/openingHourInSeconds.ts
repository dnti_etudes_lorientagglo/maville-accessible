import isOpen24Hours from '@/ts/utilities/dates/hours/isOpen24Hours';
import timeToSeconds from '@/ts/utilities/dates/hours/timeToSeconds';
import { OpeningHour } from '@/ts/utilities/dates/types';

export default function openingHourInSeconds(hour: OpeningHour) {
  return {
    start: timeToSeconds(hour.start),
    end: isOpen24Hours(hour)
      ? timeToSeconds('23:59')
      : timeToSeconds(hour.end),
  };
}
