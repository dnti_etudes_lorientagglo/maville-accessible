import dateToSeconds from '@/ts/utilities/dates/hours/dateToSeconds';
import openingHourInSeconds from '@/ts/utilities/dates/hours/openingHourInSeconds';
import isoWeekday from '@/ts/utilities/dates/isoWeekday';
import { WeekdayOpeningHour } from '@/ts/utilities/dates/types';

export default function currentOpeningHour(
  hours: WeekdayOpeningHour[],
  date: Date = new Date(),
) {
  const oneDaySeconds = 24 * 60 * 60;
  const currentSeconds = dateToSeconds(date);
  const currentWeekday = isoWeekday(date);
  const nextSeconds = currentSeconds + oneDaySeconds;

  return hours.find((hour) => {
    const { start, end } = openingHourInSeconds(hour);

    // Classic hour on one weekday.
    if (start <= end) {
      return currentWeekday === hour.weekday
        && currentSeconds >= start
        && currentSeconds < end;
    }

    // Split hour on two weekday (first weekday part check).
    if (currentWeekday === hour.weekday
      && currentSeconds >= start
      && currentSeconds < oneDaySeconds
    ) {
      return true;
    }

    const prevWeekday = (currentWeekday - 1) === 0 ? 7 : currentWeekday - 1;

    // Split hour on two weekday (second weekday part check).
    return prevWeekday === hour.weekday && nextSeconds < (end + oneDaySeconds);
  });
}
