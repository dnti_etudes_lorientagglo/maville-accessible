import range from '@/ts/utilities/arrays/range';
import sortOpeningHours from '@/ts/utilities/dates/hours/sortOpeningHours';
import timeToSeconds from '@/ts/utilities/dates/hours/timeToSeconds';
import { IsoWeekday, OpeningHour, WeekdayOpeningHour } from '@/ts/utilities/dates/types';

type TimeRange = {
  start: number;
  end: number;
};

const isReversed = (timeRange: TimeRange) => timeRange.start >= timeRange.end;

const containsTime = (timeRange: TimeRange, time: number) => (
  time >= timeRange.start && (isReversed(timeRange) || time < timeRange.end)
);

const overlaps = (timeRange: TimeRange, otherRange: TimeRange) => (
  containsTime(timeRange, otherRange.start) || containsTime(timeRange, otherRange.end)
);

const toTimeRange = (hour: OpeningHour) => {
  const start = timeToSeconds(hour.start);
  const end = timeToSeconds(hour.end);

  return {
    start, end: end < start ? end + timeToSeconds('24') : end,
  };
};

export default function clearOpeningHours(
  openingHours: WeekdayOpeningHour[],
): WeekdayOpeningHour[] {
  const decomposedOpeningHours = [] as WeekdayOpeningHour[];

  sortOpeningHours(openingHours).forEach((hour) => {
    const hourRange = toTimeRange(hour);
    if (hourRange.end > timeToSeconds('36')) {
      decomposedOpeningHours.push({
        weekday: hour.weekday, start: hour.start, end: '00:00',
      }, {
        weekday: (hour.weekday % 7) + 1 as IsoWeekday, start: '00:00', end: hour.end,
      });
    } else {
      decomposedOpeningHours.push(hour);
    }
  });

  const mergedOpeningHoursIndexes = [] as number[];
  const newOpeningHours = [] as WeekdayOpeningHour[];

  decomposedOpeningHours.forEach((hour, index) => {
    if (mergedOpeningHoursIndexes.indexOf(index) !== -1) {
      return;
    }

    const merged = range(index + 1, decomposedOpeningHours.length - 1).some((otherIndex) => {
      if (mergedOpeningHoursIndexes.indexOf(otherIndex) !== -1) {
        return false;
      }

      const otherHour = decomposedOpeningHours[otherIndex];
      if (hour.weekday !== otherHour.weekday) {
        return false;
      }

      const otherHourRange = toTimeRange(otherHour);
      const hourRange = toTimeRange(hour);
      if (overlaps(hourRange, otherHourRange) || overlaps(otherHourRange, hourRange)) {
        mergedOpeningHoursIndexes.push(index, otherIndex);
        newOpeningHours.push({
          weekday: hour.weekday,
          start: otherHourRange.start < hourRange.start ? otherHour.start : hour.start,
          end: otherHourRange.end > hourRange.end ? otherHour.end : hour.end,
        });

        return true;
      }

      return false;
    });

    if (!merged) {
      newOpeningHours.push(hour);
    }
  });

  if (openingHours.length !== newOpeningHours.length) {
    return clearOpeningHours(newOpeningHours);
  }

  return newOpeningHours;
}
