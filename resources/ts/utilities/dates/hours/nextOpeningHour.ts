import currentOpeningHour from '@/ts/utilities/dates/hours/currentOpeningHour';
import dateToSeconds from '@/ts/utilities/dates/hours/dateToSeconds';
import timeToSeconds from '@/ts/utilities/dates/hours/timeToSeconds';
import isoWeekday from '@/ts/utilities/dates/isoWeekday';
import relativeIsoWeekdays from '@/ts/utilities/dates/relativeIsoWeekdays';
import { WeekdayOpeningHour } from '@/ts/utilities/dates/types';

export default function nextOpeningHour(
  hours: WeekdayOpeningHour[],
  date: Date = new Date(),
) {
  const currentHour = currentOpeningHour(hours, date);
  const currentWeekday = isoWeekday(date);
  const currentSeconds = dateToSeconds(date);
  const weekdays = relativeIsoWeekdays(date);

  let nextHour: WeekdayOpeningHour | undefined;
  weekdays.some((weekday) => {
    nextHour = hours.find((hour) => {
      if (hour.weekday !== weekday || hour === currentHour) {
        return false;
      }

      if (hour.weekday !== currentWeekday) {
        return true;
      }

      return timeToSeconds(hour.start) >= currentSeconds;
    });

    return nextHour;
  });

  return nextHour;
}
