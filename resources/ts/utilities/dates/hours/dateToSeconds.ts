import timeToSeconds from '@/ts/utilities/dates/hours/timeToSeconds';

/**
 * Get the date seconds since midnight.
 *
 * @param date
 */
export default function dateToSeconds(date: Date) {
  return timeToSeconds(`${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`);
}
