import { OpeningHour } from '@/ts/utilities/dates/types';

export default function isOpen24Hours(hour: OpeningHour) {
  return hour.start === '00:00' && hour.end === '00:00';
}
