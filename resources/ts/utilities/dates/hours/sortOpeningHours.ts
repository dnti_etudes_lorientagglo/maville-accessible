import orderBy from '@/ts/utilities/arrays/orderBy';
import timeToSeconds from '@/ts/utilities/dates/hours/timeToSeconds';
import { WeekdayOpeningHour } from '@/ts/utilities/dates/types';

export default function sortOpeningHours(openingHours: WeekdayOpeningHour[]) {
  return orderBy(openingHours, ['weekday', (h) => timeToSeconds(h.start)]);
}
