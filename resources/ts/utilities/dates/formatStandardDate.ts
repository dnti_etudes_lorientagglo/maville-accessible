import isNone from '@/ts/utilities/common/isNone';
import { Optional } from '@/ts/utilities/types/optional';

export default function formatStandardDate<T extends Optional<Date>>(
  date: T,
): T extends null | undefined ? undefined : string {
  if (isNone(date)) {
    return undefined as any;
  }

  const offset = date.getTimezoneOffset();

  return new Date(date.getTime() - (offset * 60 * 1000))
    .toISOString()
    .split('T')[0] as any;
}
