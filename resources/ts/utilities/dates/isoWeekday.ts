import { IsoWeekday } from '@/ts/utilities/dates/types';

export default function isoWeekday(date: Date) {
  return ((date.getDay() + 6) % 7) + 1 as IsoWeekday;
}
