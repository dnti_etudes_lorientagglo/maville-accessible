import { i18nInstance } from '@/ts/plugins/i18n';

export default function formatDuration(seconds: number) {
  const minutes = seconds <= 60 ? Math.ceil(seconds / 60) : Math.round(seconds / 60);
  const hours = Math.floor(minutes / 60);
  const days = Math.floor(hours / 24);

  return [
    days > 0 ? i18nInstance.t('common.units.short.days', { n: days }) : null,
    hours > 0 ? i18nInstance.t('common.units.short.hours', { n: hours - (days * 24) }) : null,
    minutes > 0 ? i18nInstance.t('common.units.short.minutes', { n: minutes - (hours * 60) }) : null,
  ].filter((t) => t !== null).join(' ');
}
