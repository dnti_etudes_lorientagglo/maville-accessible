import dateFnsLocale from '@/ts/utilities/dates/dateFnsLocale';
import { formatDistance } from 'date-fns';

export default function formatDateFrom(date: Date, from: Date, locale: string) {
  return formatDistance(date, from, { locale: dateFnsLocale(locale), addSuffix: true });
}
