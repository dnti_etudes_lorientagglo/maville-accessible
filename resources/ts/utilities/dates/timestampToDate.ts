import isNone from '@/ts/utilities/common/isNone';
import unixToDate from '@/ts/utilities/dates/unixToDate';
import { Optional } from '@/ts/utilities/types/optional';

export default function timestampToDate(timestamp?: Optional<number>) {
  return unixToDate(isNone(timestamp) ? null : (timestamp * 1000));
}
