import range from '@/ts/utilities/arrays/range';
import { IsoWeekday } from '@/ts/utilities/dates/types';

export default function isoWeekdays() {
  return range(1, 7) as IsoWeekday[];
}
