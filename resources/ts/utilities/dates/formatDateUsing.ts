import decomposeLocale from '@/ts/utilities/lang/decomposeLocale';

export type DateFormat = 'short' | 'long';

export type DateFormatOptions = {
  format: DateFormat;
  weekday?: boolean;
  year?: boolean;
};

function formatterOptions(options: DateFormatOptions): Intl.DateTimeFormatOptions {
  if (options.format === 'short') {
    return {
      weekday: options.weekday ? 'short' : undefined,
      day: 'numeric',
      month: 'short',
      year: options.year ? 'numeric' : undefined,
    };
  }

  if (options.format === 'long') {
    return {
      weekday: options.weekday ? 'long' : undefined,
      day: 'numeric',
      month: 'long',
      year: options.year ? 'numeric' : undefined,
    };
  }

  throw new Error('invalid date format given');
}

export default function formatDateUsing(date: Date, locale: string, options: DateFormatOptions) {
  const language = decomposeLocale(locale)[0];

  const formatter = new Intl.DateTimeFormat(language, formatterOptions(options));

  return formatter.format(date);
}
