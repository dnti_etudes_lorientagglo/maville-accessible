import joursFeries from '@socialgouv/jours-feries';

export default function publicHolidays(year: number) {
  return joursFeries(year);
}
