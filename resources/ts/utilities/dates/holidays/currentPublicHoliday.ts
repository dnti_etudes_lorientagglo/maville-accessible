import publicHolidays from '@/ts/utilities/dates/holidays/publicHolidays';
import { isSameDay } from 'date-fns';

export default function currentPublicHoliday(date = new Date()): string | null {
  const publicHolidaysMap = publicHolidays(date.getFullYear());

  return Object.keys(publicHolidaysMap)
    .find((label) => isSameDay(publicHolidaysMap[label], date)) ?? null;
}
