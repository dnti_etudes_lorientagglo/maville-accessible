import decomposeLocale from '@/ts/utilities/lang/decomposeLocale';

export default function formatTime(date: Date, locale: string) {
  return date.toLocaleTimeString(decomposeLocale(locale)[0], {
    hour: '2-digit',
    minute: '2-digit',
  });
}
