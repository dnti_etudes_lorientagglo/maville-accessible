import decomposeLocale from '@/ts/utilities/lang/decomposeLocale';
import { IsoWeekday } from '@/ts/utilities/dates/types';

export default function formatWeekday(
  weekday: IsoWeekday,
  locale: string,
  format: 'short' | 'long' = 'long',
) {
  // The 2 January 2023 is a Monday.
  return new Date(2023, 0, 1 + weekday)
    .toLocaleString(decomposeLocale(locale)[0], { weekday: format });
}
