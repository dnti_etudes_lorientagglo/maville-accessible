export type IsoWeekday = 1 | 2 | 3 | 4 | 5 | 6 | 7;

export type OpeningPeriod = {
  start: Date;
  end: Date;
};

export type OpeningHour = {
  start: string;
  end: string;
};

export type DateOpeningHour = OpeningHour & {
  date: Date;
};

export type WeekdayOpeningHour = OpeningHour & {
  weekday: IsoWeekday;
};

export type DateOpeningHourGroup = {
  date: Date;
  hours: OpeningHour[];
};
