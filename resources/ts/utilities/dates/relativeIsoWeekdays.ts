import isoWeekday from '@/ts/utilities/dates/isoWeekday';
import isoWeekdays from '@/ts/utilities/dates/isoWeekdays';

export default function relativeIsoWeekdays(date: Date = new Date()) {
  const weekdays = isoWeekdays();
  const currentWeekday = isoWeekday(date);
  const currentWeekdayIndex = weekdays.indexOf(currentWeekday);

  return [
    ...weekdays.slice(currentWeekdayIndex, 7),
    ...weekdays.slice(0, currentWeekdayIndex),
  ];
}
