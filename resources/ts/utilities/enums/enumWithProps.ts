import EnumOf from '@/ts/utilities/enums/enumOf';

export default abstract class EnumWithProps<T, C> extends EnumOf<T> {
  public readonly props: C;

  public constructor(value: T, props: C) {
    super(value);
    this.props = props;
  }
}
