import { toRaw } from 'vue';

/**
 * Class EnumOf.
 *
 * An enum class to replace the limited TypeScript enums.
 */
export default class EnumOf<T> {
  public readonly value: T;

  public constructor(value: T) {
    this.value = value;
  }

  public is(other: unknown) {
    return toRaw(this) === toRaw(other);
  }

  public match(value: string) {
    return this.value === value;
  }
}
