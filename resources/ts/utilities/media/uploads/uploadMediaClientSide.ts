import action from '@/ts/api/action';
import Media, { MediaVisibility } from '@/ts/api/models/media';
import { useAuthStore } from '@/ts/stores/auth';
import sendXmlHttpRequest from '@/ts/utilities/http/sendXmlHttpRequest';
import sumBy from '@/ts/utilities/math/sumBy';
import computeHashForFile from '@/ts/utilities/media/uploads/computeHashForFile';
import ExceededUploadSizeError from '@/ts/utilities/media/uploads/exceededUploadSizeError';
import { MediaUploadOptions } from '@/ts/utilities/media/uploads/types';
import filterValues from '@/ts/utilities/objects/filterValues';
import mapValues from '@/ts/utilities/objects/mapValues';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { all, include, none, query, raw } from '@foscia/core';
import { HttpInvalidRequestError, makePost } from '@foscia/http';
import { filterBy } from '@foscia/jsonapi';

type FilesByHash = Dictionary<File>;

type FileData = {
  name: string;
  hash: string;
  size: number;
  mimeType: string;
  visibility: MediaVisibility;
};

type PreparedMediaData = {
  type: 'media';
  data: {
    media: string;
  };
};

type PreparedUploadActionData = {
  type: 'action',
  data: {
    media: string;
    action: {
      method: 'POST' | 'PUT';
      endpoint: string;
      field: string | null;
      headers: Dictionary;
      inputs: Dictionary;
    };
  };
};

async function mapFilesByHash(files: File[]) {
  return files.reduce(async (filesByHash, file) => ({
    ...(await filesByHash),
    [await computeHashForFile(file)]: file,
  }), Promise.resolve({} as FilesByHash));
}

async function prepareClientSideUpload(
  filesByHash: FilesByHash,
  options: MediaUploadOptions,
): Promise<Dictionary<PreparedMediaData | PreparedUploadActionData>> {
  try {
    return await action()
      .use(query(Media))
      .use(makePost('-actions/client-side/prepare-upload', {
        data: {
          files: Object.entries(filesByHash).reduce((files, [hash, file]) => [
            ...files,
            {
              hash,
              name: file.name,
              size: file.size,
              mimeType: file.type || 'application/octet-stream',
              visibility: options.visibility,
            },
          ], [] as FileData[]),
        },
      }))
      .run(raw(async (r) => (await r.json()).data));
  } catch (error) {
    if (error instanceof HttpInvalidRequestError) {
      if (error.response.status === 413) {
        throw new ExceededUploadSizeError();
      }
    }

    throw error;
  }
}

function extractPreparedUploadActionData(
  dataByHash: Dictionary<PreparedMediaData | PreparedUploadActionData>,
) {
  return filterValues(dataByHash, (v) => v.type === 'action') as Dictionary<PreparedUploadActionData>;
}

function prepareUploadRequestData(preparedAction: PreparedUploadActionData, file: File) {
  if (preparedAction.data.action.field) {
    const formData = new FormData();

    Object.entries(preparedAction.data.action.inputs).forEach(([key, value]) => {
      formData.append(key, value);
    });
    formData.append(preparedAction.data.action.field, file);

    return formData;
  }

  return file;
}

async function uploadFiles(
  filesByHash: FilesByHash,
  dataByHash: Dictionary<PreparedUploadActionData>,
  options: MediaUploadOptions,
  progressFactor: number,
) {
  const progressesCount = Object.keys(dataByHash).length;
  const progresses = mapValues(dataByHash, () => 0);
  const runProgressCallback = (hash: string, progress: number) => {
    progresses[hash] = progress;
    options.progressCallback?.(
      (sumBy(Object.values(progresses)) / progressesCount) * progressFactor,
    );
  };

  await Promise.all(Object.entries(dataByHash).map(async ([hash, data]) => {
    await sendXmlHttpRequest(data.data.action.endpoint, {
      method: data.data.action.method,
      data: prepareUploadRequestData(data, filesByHash[hash]),
      headers: data.data.action.headers,
      uploadProgressCallback: (progress) => {
        runProgressCallback(hash, progress);
      },
    });

    runProgressCallback(hash, 1);
  }));
}

async function confirmFiles(
  dataByHash: Dictionary<PreparedUploadActionData>,
) {
  await action()
    .use(query(Media))
    .use(makePost('-actions/client-side/confirm-upload', {
      data: {
        media: Object.values(dataByHash).map((data) => data.data.media),
      },
    }))
    .run(none());
}

export default async function uploadMediaClientSide(
  files: File[],
  options: MediaUploadOptions,
) {
  options.progressCallback?.(-1);
  try {
    const auth = useAuthStore();

    const filesByHash = await mapFilesByHash(files);
    options.progressCallback?.(0.05);

    const preparedUploadData = await prepareClientSideUpload(filesByHash, options);
    options.progressCallback?.(0.1);

    const uploadActionData = extractPreparedUploadActionData(preparedUploadData);
    if (Object.keys(uploadActionData).length > 0) {
      await uploadFiles(filesByHash, uploadActionData, options, 0.8);
      options.progressCallback?.(0.9);

      await confirmFiles(uploadActionData);
      options.progressCallback?.(0.95);
    }

    const media = await action()
      .use(query(Media))
      .use(include('owner'))
      .use(filterBy('owner', [auth.id]))
      .use(filterBy('id', Object.values(preparedUploadData).map(({ data }) => data.media)))
      .run(all());

    options.progressCallback?.(1);

    return media;
  } catch (error) {
    options.progressCallback?.(1);

    if (error instanceof HttpInvalidRequestError) {
      if (error.response.status === 413) {
        throw new ExceededUploadSizeError();
      }
    }

    throw error;
  }
}
