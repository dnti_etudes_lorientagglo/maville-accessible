import AppError from '@/ts/errors/appError';

export default class OnlyVideosAllowedError extends AppError {
  public constructor() {
    super('only videos are allowed for upload');
  }
}
