import env from '@/ts/env';
import OnlyImagesAllowedError from '@/ts/utilities/media/uploads/onlyImagesAllowedError';
import OnlyVideosAllowedError from '@/ts/utilities/media/uploads/onlyVideosAllowedError';
import { MediaUploadOptions } from '@/ts/utilities/media/uploads/types';
import uploadMediaClientSide from '@/ts/utilities/media/uploads/uploadMediaClientSide';
import uploadMediaServerSide from '@/ts/utilities/media/uploads/uploadMediaServerSide';

export default async function uploadMedia(
  files: File[],
  options: MediaUploadOptions,
) {
  options.progressCallback?.(0);

  files.forEach((file) => {
    if (options.only === 'image' && !file.type?.startsWith('image/')) {
      throw new OnlyImagesAllowedError();
    }

    if (options.only === 'video' && !file.type?.startsWith('video/')) {
      throw new OnlyVideosAllowedError();
    }
  });

  return env.services.files.supportsClientUploads
    ? uploadMediaClientSide(files, options)
    : uploadMediaServerSide(files, options);
}
