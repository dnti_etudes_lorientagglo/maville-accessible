import action, { actionHeaders, deserializer } from '@/ts/api/action';
import Media from '@/ts/api/models/media';
import sendXmlHttpRequest from '@/ts/utilities/http/sendXmlHttpRequest';
import XmlHttpRequestError from '@/ts/utilities/http/xmlHttpRequestError';
import ExceededUploadSizeError from '@/ts/utilities/media/uploads/exceededUploadSizeError';
import { MediaUploadOptions } from '@/ts/utilities/media/uploads/types';
import { query } from '@foscia/core';

export default async function uploadMediaServerSide(
  files: File[],
  options: MediaUploadOptions,
) {
  options.progressCallback?.(-1);

  const formData = new FormData();
  files.forEach((file, index) => {
    formData.append(`data[files][${index}][file]`, file);
    formData.append(`data[files][${index}][visibility]`, options.visibility);
  });

  try {
    const xhr = await sendXmlHttpRequest('/api/v1/media/-actions/upload', {
      method: 'POST',
      data: formData,
      headers: {
        ...actionHeaders(),
        Accept: 'application/vnd.api+json',
      },
      uploadProgressCallback: (progress) => {
        options.progressCallback?.(progress);
      },
    });

    const context = await action()
      .use(query(Media))
      .useContext();

    const { instances } = await deserializer.deserialize(JSON.parse(xhr.response), context);

    options.progressCallback?.(1);

    return instances;
  } catch (error) {
    options.progressCallback?.(1);

    if (
      error instanceof XmlHttpRequestError
      && (error.xhr.status === 413 || error.xhr.status === 422)
    ) {
      throw new ExceededUploadSizeError();
    }

    throw error;
  }
}
