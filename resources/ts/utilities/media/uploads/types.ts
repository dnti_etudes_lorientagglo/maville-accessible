import type { MediaVisibility } from '@/ts/api/models/media';

export type MediaUploadOptions = {
  visibility: MediaVisibility;
  only?: 'image' | 'video' | undefined;
  progressCallback?: (progress: number) => void;
};
