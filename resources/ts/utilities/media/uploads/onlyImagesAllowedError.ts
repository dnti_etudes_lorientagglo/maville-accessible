import AppError from '@/ts/errors/appError';

export default class OnlyImagesAllowedError extends AppError {
  public constructor() {
    super('only images are allowed for upload');
  }
}
