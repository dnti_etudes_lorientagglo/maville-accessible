import readFileChunks from '@/ts/utilities/files/readFileChunks';
import CryptoJS from 'crypto-js';

export default async function computeHashForFile(file: File) {
  const pendingHash = CryptoJS.algo.MD5.create();

  await readFileChunks(file, (chunk) => {
    pendingHash.update(CryptoJS.enc.Latin1.parse(chunk));
  });

  return pendingHash.finalize().toString(CryptoJS.enc.Hex);
}
