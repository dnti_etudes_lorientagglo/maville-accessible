import AppError from '@/ts/errors/appError';

export default class ExceededUploadSizeError extends AppError {
  public constructor() {
    super('upload size has been exceeded during upload');
  }
}
