import MediaType from '@/ts/api/enums/mediaType';
import Media from '@/ts/api/models/media';
import isNil from '@/ts/utilities/common/isNil';
import isNone from '@/ts/utilities/common/isNone';
import { Optional } from '@/ts/utilities/types/optional';

export type DisplayableImgOptions = {
  keepOriginalWidth?: boolean;
  keepOriginalHeight?: boolean;
};

export type DisplayableImg = {
  src: string;
  alt?: string;
  srcset?: string;
  lazySrc?: string;
  width?: number;
  height?: number;
};

export default function mediaToDisplayableImg(
  media: Optional<Media>,
  options: DisplayableImgOptions = {},
): DisplayableImg | null {
  if (!media || !MediaType.IMAGE.is(media.mediaType) || isNil(media.originalURL)) {
    return null;
  }

  return {
    src: media.originalURL,
    srcset: media.optimizedSrcset ?? undefined,
    lazySrc: media.placeholderURL ?? undefined,
    width: options.keepOriginalWidth && !isNone(media.width) ? media.width : undefined,
    height: options.keepOriginalHeight && !isNone(media.height) ? media.height : undefined,
  };
}
