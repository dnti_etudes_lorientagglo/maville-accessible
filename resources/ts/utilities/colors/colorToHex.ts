import { RgbColor } from '@/ts/utilities/colors/types';

export default function colorToHex(color: RgbColor) {
  return `#${color.map((component) => {
    const hex = component.toString(16);

    return hex.length === 1 ? `0${hex}` : hex;
  }).join('')}`;
}
