import colorToHex from '@/ts/utilities/colors/colorToHex';
import colorToRgb from '@/ts/utilities/colors/colorToRgb';
import { RgbColor } from '@/ts/utilities/colors/types';

function computeModifierAndBgComponent(modifier: number) {
  return modifier <= 0
    ? [1 + modifier, 0]
    : [Math.abs(1 - modifier), 255];
}

export default function colorShade(color: string, percent: number) {
  const [factor, bgComponent] = computeModifierAndBgComponent(percent);

  return colorToHex(
    colorToRgb(color).map(
      (component) => Math.floor(
        (component * factor) + (bgComponent * (1 - factor)),
      ),
    ) as RgbColor,
  );
}
