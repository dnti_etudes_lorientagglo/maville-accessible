import { RgbColor } from '@/ts/utilities/colors/types';

export default function colorToRgb(color: string): RgbColor {
  const hexRegExp = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i;

  const result = hexRegExp.exec(color);

  return result ? [
    parseInt(result[1], 16),
    parseInt(result[2], 16),
    parseInt(result[3], 16),
  ] : [255, 255, 255];
}
