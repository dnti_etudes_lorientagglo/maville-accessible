import User from '@/ts/api/models/user';
import randomColor from '@/ts/utilities/colors/randomColor';

export default function userColor(user: User) {
  return randomColor(user.fullName);
}
