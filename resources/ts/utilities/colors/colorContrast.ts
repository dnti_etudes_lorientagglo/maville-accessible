import colorLuminance from '@/ts/utilities/colors/colorLuminance';

export default function colorContrast(color: string, otherColor: string) {
  const colorLum = colorLuminance(color);
  const otherColorLum = colorLuminance(otherColor);
  const brightest = Math.max(colorLum, otherColorLum);
  const darkest = Math.min(colorLum, otherColorLum);

  return (brightest + 0.05) / (darkest + 0.05);
}
