import colorToRgb from '@/ts/utilities/colors/colorToRgb';

const RED = 0.2126;
const GREEN = 0.7152;
const BLUE = 0.0722;

const GAMMA = 2.4;

export default function colorLuminance(color: string) {
  const a = colorToRgb(color).map((v) => {
    // eslint-disable-next-line no-param-reassign
    v /= 255;
    return v <= 0.03928
      ? (v / 12.92)
      : (((v + 0.055) / 1.055) ** GAMMA);
  });

  return a[0] * RED + a[1] * GREEN + a[2] * BLUE;
}
