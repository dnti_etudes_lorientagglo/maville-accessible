import colors from '@/ts/utilities/colors/colors';

export default function randomColor(seed: string) {
  const keys = Object.keys(colors).filter((c) => c !== 'shades');

  // https://stackoverflow.com/a/7493982/13389135
  const charCodes = `${seed}xx`.split('').reduce((a, b) => (a + b.charCodeAt(0)), 0);

  return colors[keys[charCodes % keys.length]];
}
