import mapWithKeys from '@/ts/utilities/objects/mapWithKeys';
import vuetifyColors from 'vuetify/lib/util/colors';

export default mapWithKeys(vuetifyColors, (color, key) => (
  ['shades', 'blueGrey', 'grey'].indexOf(key) === -1
    ? { [key]: color.base }
    : {}
));
