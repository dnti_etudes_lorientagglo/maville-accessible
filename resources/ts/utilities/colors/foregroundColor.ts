import isDarkColor from '@/ts/utilities/colors/isDarkColor';

export default function foregroundColor(color: string) {
  return isDarkColor(color) ? '#ffffff' : '#000000';
}
