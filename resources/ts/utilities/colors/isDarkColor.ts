import colorContrast from '@/ts/utilities/colors/colorContrast';

export default function isDarkColor(color: string) {
  return colorContrast(color, '#ffffff') > 4.5;
}
