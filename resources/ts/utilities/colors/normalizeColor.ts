import { Optional } from '@/ts/utilities/types/optional';

export default function normalizeColor(color: Optional<string>) {
  const regex = /^#([a-f0-9]{6}|[a-f0-9]{3})$/i;
  if (!color || !regex.test(color)) {
    return null;
  }

  if (color.length === 4) {
    const [r, g, b] = color.substring(1);

    return `#${r}${r}${g}${g}${b}${b}`.toLowerCase();
  }

  return color.toLowerCase();
}
