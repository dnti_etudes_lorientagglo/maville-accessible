import foregroundColor from '@/ts/utilities/colors/foregroundColor';

export default function colorDef(color: string) {
  return {
    bg: color,
    fg: foregroundColor(color),
  };
}
