import Category from '@/ts/api/models/category';
import randomColor from '@/ts/utilities/colors/randomColor';

export default function categoryColor(category: Category) {
  return category.color || randomColor(category.slug);
}
