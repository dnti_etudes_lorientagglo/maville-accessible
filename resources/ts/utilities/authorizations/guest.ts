import { useAuthStore } from '@/ts/stores/auth';

export default function guest() {
  return useAuthStore().guest;
}
