import { BaseModelInstance } from '@/ts/api/makeModel';
import { useAuthStore } from '@/ts/stores/auth';
import { ModelInstance } from '@foscia/core';

export default function hasPermissionOn(permission: string, instance?: ModelInstance) {
  if (instance) {
    return (instance as BaseModelInstance).allows.indexOf(permission) !== -1;
  }

  const auth = useAuthStore();

  return auth.auth && auth.user.allows.indexOf(permission) !== -1;
}
