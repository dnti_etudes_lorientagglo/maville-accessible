import { useAuthStore } from '@/ts/stores/auth';

export default function blocked() {
  return useAuthStore().blocked;
}
