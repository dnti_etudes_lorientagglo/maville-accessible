import { useAuthStore } from '@/ts/stores/auth';

export default function auth() {
  return useAuthStore().auth;
}
