import { useAuthStore } from '@/ts/stores/auth';

export default function actingForOrganization() {
  const auth = useAuthStore();

  return auth.auth
    ? auth.user.actingForOrganization
    : null;
}
