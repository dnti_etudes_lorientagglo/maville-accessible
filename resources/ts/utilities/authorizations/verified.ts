import { useAuthStore } from '@/ts/stores/auth';

export default function verified() {
  return useAuthStore().verified;
}
