import type User from '@/ts/api/models/user';
import { i18nInstance } from '@/ts/plugins/i18n';
import { useAuthStore } from '@/ts/stores/auth';

export default function fullNameWithYou(user: User, allowInitials = false) {
  const fullName = user.displayInitials && allowInitials
    ? user.initials
    : user.fullName;

  return user.id && useAuthStore().id === user.id
    ? i18nInstance.t('resources.users.labels.fullNameWithYou', { fullName })
    : fullName;
}
