import env from '@/ts/env';
import makeStoredValue from '@/ts/utilities/storages/makeStoredValue';

export default makeStoredValue('app.i18n.locale', {
  defaultValue: env.services.i18n.defaultLocale,
});
