import localePreference from '@/ts/utilities/lang/localePreference';
import verifyLocale from '@/ts/utilities/lang/verifyLocale';

export default function detectLocale() {
  return verifyLocale(localePreference.value);
}
