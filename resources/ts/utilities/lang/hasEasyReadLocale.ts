import env from '@/ts/env';

export default function hasEasyReadLocale(locale: string) {
  return env.services.i18n.easyReadLocales.indexOf(locale) !== -1;
}
