export default function easyReadLocale(locale: string) {
  return `${locale}--ER`;
}
