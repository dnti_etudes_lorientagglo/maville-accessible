import env from '@/ts/env';
import localesKeys from '@/ts/utilities/lang/localesKeys';
import { Optional } from '@/ts/utilities/types/optional';

export default function verifyLocale(locale: Optional<string>) {
  const locales = localesKeys();

  return !locale || locales.indexOf(locale) === -1
    ? env.services.i18n.defaultLocale
    : locale;
}
