import { Dictionary } from '@/ts/utilities/types/dictionary';

export default function languageHeaders(language: string, preferEasyRead: boolean) {
  const headers = { 'Accept-Language': language } as Dictionary;
  if (preferEasyRead) {
    headers['X-Prefer-Easy-Read'] = 'true';
  }

  return headers;
}
