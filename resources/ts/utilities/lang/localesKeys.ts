import env from '@/ts/env';
import easyReadLocale from '@/ts/utilities/lang/easyReadLocale';
import hasEasyReadLocale from '@/ts/utilities/lang/hasEasyReadLocale';

export default function localesKeys() {
  return Object.keys(env.services.i18n.locales).reduce((locales, locale) => {
    const newLocales = [locale];
    if (hasEasyReadLocale(locale)) {
      newLocales.push(easyReadLocale(locale));
    }

    return [...locales, ...newLocales];
  }, [] as string[]);
}
