export default function decomposeLocale(locale: string) {
  const [language, preferEasyRead] = locale.split('--');

  return [language, !!preferEasyRead] as const;
}
