import localesKeys from '@/ts/utilities/lang/localesKeys';
import { i18nInstance } from '@/ts/plugins/i18n';
import { useLangStore } from '@/ts/stores/lang';
import orderBy from '@/ts/utilities/arrays/orderBy';
import isNil from '@/ts/utilities/common/isNil';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import pluralize from 'pluralize';

export type Translation<T = string> = Dictionary<T>;

export type TranslationOptions = {
  editing: boolean;
};

function translateForKeyOr(key: string, orValue: () => string) {
  if (i18nInstance.te(key)) {
    return i18nInstance.t(key);
  }

  return orValue();
}

export default function translate<A extends Translation | string | undefined | null>(
  value: A,
  options?: Partial<TranslationOptions>,
): A extends undefined | null ? string | undefined : string {
  if (isNil(value)) {
    return undefined as any;
  }

  if (typeof value === 'string') {
    return translateForKeyOr(
      `forms.labels.${value}`,
      () => translateForKeyOr(
        `resources.common.types.${value}.plural`,
        () => translateForKeyOr(
          `resources.common.types.${pluralize(value)}.singular`,
          () => translateForKeyOr(
            value,
            () => value,
          ),
        ),
      ),
    );
  }

  const lang = useLangStore();
  const locale = options?.editing ? lang.editionLocale : lang.locale;
  const locales = orderBy(
    localesKeys(),
    [(l) => l !== locale, (l) => !l.startsWith(locale.replace('--ER', ''))],
  );

  const firstLocale = locales.find((l) => value[l]);
  if (!firstLocale) {
    // Theoretically, this should never occur.
    return undefined as any;
  }

  return value[firstLocale].replace(/ ([!:?;])/g, '\u00A0$1');
}
