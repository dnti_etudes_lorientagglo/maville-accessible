import isNil from '@/ts/utilities/common/isNil';
import { Arrayable } from '@/ts/utilities/types/arrayable';
import { Optional } from '@/ts/utilities/types/optional';

export default function wrap<T>(value?: Optional<Arrayable<T>>): T[] {
  if (!isNil(value)) {
    return Array.isArray(value) ? value : [value];
  }

  return [];
}
