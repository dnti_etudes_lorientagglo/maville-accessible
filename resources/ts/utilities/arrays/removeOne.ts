import { toRaw } from 'vue';

export default function removeOne<T>(values: T[], value: T): T[] {
  const index = values.indexOf(value);
  if (index !== -1) {
    values.splice(index, 1);
  } else {
    const rawValue = toRaw(value);
    if (rawValue !== value) {
      removeOne(values, toRaw(value));
    }
  }

  return values;
}
