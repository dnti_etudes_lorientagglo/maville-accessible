import { Dictionary } from '@/ts/utilities/types/dictionary';
import { SortDirection } from '@/ts/utilities/types/sort';

export default function sortsToString(sorts?: Dictionary<SortDirection>) {
  return Object.entries(sorts ?? {})
    .map(([sortName, sortValue]) => `${sortValue === 'asc' ? '' : '-'}${sortName}`)
    .join(',') || undefined;
}
