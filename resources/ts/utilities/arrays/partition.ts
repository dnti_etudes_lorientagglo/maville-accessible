import tap from '@/ts/utilities/functions/tap';

export default function partition<T>(values: T[], predicate: (value: T) => boolean) {
  return values.reduce((partitions, value) => tap(partitions, () => {
    (predicate(value) ? partitions[0] : partitions[1]).push(value);
  }), [[], []] as [T[], T[]]);
}
