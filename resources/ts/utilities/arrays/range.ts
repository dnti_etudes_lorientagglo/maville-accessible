export default function range(min: number, max: number, step = 1) {
  const values = [] as number[];

  for (let i = min; i <= max; i += step) {
    values.push(i);
  }

  return values;
}
