import get, { ValueRetriever } from '@/ts/utilities/objects/get';

export default function uniqBy<T>(
  values: T[],
  key?: ValueRetriever<T>,
) {
  return [...new Map(values.map((value) => [get(value, key), value])).values()];
}
