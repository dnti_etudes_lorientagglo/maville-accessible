export default function last<T>(values: T[], offset = 1) {
  return values[values.length - offset];
}
