import wrap from '@/ts/utilities/arrays/wrap';
import compare from '@/ts/utilities/common/compare';
import get, { ValueRetriever } from '@/ts/utilities/objects/get';
import { Arrayable } from '@/ts/utilities/types/arrayable';
import { SortDirection } from '@/ts/utilities/types/sort';

export default function orderBy<T>(
  values: T[],
  keys: Arrayable<ValueRetriever<T>>,
  directions: Arrayable<SortDirection> = 'asc',
) {
  const newValues = [...values];
  const wrappedKeys = wrap(keys);
  const wrappedDirections = wrap(directions);

  const compareWithDirection = (a: T, b: T, key: ValueRetriever<T>, direction: SortDirection) => (
    direction === 'asc'
      ? compare(get(a, key), get(b, key))
      : compare(get(b, key), get(a, key))
  );

  newValues.sort((a, b) => wrappedKeys.reduce((prev, key, index) => Number(
    prev || compareWithDirection(a, b, key, wrappedDirections[index] ?? 'asc'),
  ), 0));

  return newValues;
}
