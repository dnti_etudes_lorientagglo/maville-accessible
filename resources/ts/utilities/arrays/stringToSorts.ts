import { Dictionary } from '@/ts/utilities/types/dictionary';
import { SortDirection } from '@/ts/utilities/types/sort';

function parseSortString(str: string): [string, SortDirection] {
  const parts = str.split('-');

  return parts.length === 1
    ? [parts[0], 'asc']
    : [parts[1], 'desc'];
}

export default function stringToSorts(str?: string): Dictionary<SortDirection> {
  return (str || '').split(',').reduce((sorts, sortStr) => {
    const [sortName, sortValue] = parseSortString(sortStr);

    return { ...sorts, [sortName]: sortValue };
  }, {});
}
