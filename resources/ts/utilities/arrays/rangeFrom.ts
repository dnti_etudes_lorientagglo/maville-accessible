export default (length: number, start = 0): number[] => Array.from({ length }, (v, k) => start + k);
