import valueOr from '@/ts/utilities/functions/valueOr';
import toUpperFirst from '@/ts/utilities/strings/toUpperFirst';
import { Optional } from '@/ts/utilities/types/optional';

export default function toNameCase(value: Optional<string>) {
  return valueOr(value, '').replace(
    /[A-Za-zÀ-ÖØ-öø-ÿ][^-.\s]*/g,
    toUpperFirst,
  );
}
