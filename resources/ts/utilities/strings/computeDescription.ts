import isNone from '@/ts/utilities/common/isNone';
import { Translation } from '@/ts/utilities/lang/translate';
import mapValues from '@/ts/utilities/objects/mapValues';
import stripHtml from '@/ts/utilities/strings/stripHtml';
import truncate from '@/ts/utilities/strings/truncate';
import { Optional } from '@/ts/utilities/types/optional';

export default function computeDescription(
  description: Optional<Translation>,
  body?: Optional<Translation>,
) {
  if (!isNone(description)) {
    return description;
  }

  if (!isNone(body)) {
    return mapValues(body, (value) => truncate(stripHtml(value ?? '')));
  }

  return null;
}
