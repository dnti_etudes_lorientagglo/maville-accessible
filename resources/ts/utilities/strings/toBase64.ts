export default function toBase64(value: string) {
  return btoa(
    String.fromCodePoint(...new TextEncoder().encode(value)),
  );
}
