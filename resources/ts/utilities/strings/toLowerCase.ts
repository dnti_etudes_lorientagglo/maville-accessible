import valueOr from '@/ts/utilities/functions/valueOr';
import { Optional } from '@/ts/utilities/types/optional';

export default function toLowerCase(value: Optional<string>) {
  return valueOr(value, '').toLowerCase();
}
