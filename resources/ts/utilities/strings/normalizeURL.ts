import wrap from '@/ts/utilities/arrays/wrap';
import { Arrayable } from '@/ts/utilities/types/arrayable';

function normalizeURLPossibility(value: string, validSchemes: string[]) {
  try {
    const url = new URL(value);

    return validSchemes.indexOf(url.protocol.replace(/:$/, '')) !== -1
      ? url
      : null;
  } catch {
    return null;
  }
}

export default function normalizeURL(value: string, schemes?: Arrayable<string>) {
  const validSchemes = [...(schemes ? wrap(schemes) : []), 'https', 'http'];

  return normalizeURLPossibility(value, validSchemes)
    ?? normalizeURLPossibility(`https://${value}`, validSchemes);
}
