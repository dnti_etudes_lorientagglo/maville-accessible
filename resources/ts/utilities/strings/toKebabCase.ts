import toCase from '@/ts/utilities/strings/toCase';

export default function toKebabCase(value: string): string {
  return toCase(value, (w) => w.toLowerCase(), '-');
}
