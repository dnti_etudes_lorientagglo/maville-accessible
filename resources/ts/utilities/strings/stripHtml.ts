import parseHtml from '@/ts/utilities/strings/parseHtml';
import toTrim from '@/ts/utilities/strings/toTrim';

export type StripHtmlOptions = {
  withLR?: boolean;
};

export default function stripHtml(html: string, options: StripHtmlOptions = {}) {
  const element = parseHtml(
    html.replace(/<br>/g, '\n').replace(/<\/(h[1-6]|p)>/g, '</$1>\n'),
  );

  return toTrim(
    (element.textContent ?? '')
      .replace(/[\n\r]+/g, options.withLR ? '\n' : ' ')
      .replace(/ +/g, ' '),
  );
}
