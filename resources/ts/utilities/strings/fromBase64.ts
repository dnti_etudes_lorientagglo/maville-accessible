export default function fromBase64(value: string) {
  return new TextDecoder().decode(
    Uint8Array.from(atob(value), (m) => m.codePointAt(0)!),
  );
}
