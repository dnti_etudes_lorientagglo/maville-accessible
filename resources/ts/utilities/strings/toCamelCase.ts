import toLowerFirst from '@/ts/utilities/strings/toLowerFirst';
import toPascalCase from '@/ts/utilities/strings/toPascalCase';

export default function toCamelCase(value: string): string {
  return toLowerFirst(toPascalCase(value));
}
