import toCaseFirst from '@/ts/utilities/strings/toCaseFirst';
import { Optional } from '@/ts/utilities/types/optional';

export default function toLowerFirst(value: Optional<string>) {
  return toCaseFirst(value, (v) => v.toLowerCase());
}
