import valueOr from '@/ts/utilities/functions/valueOr';
import { Optional } from '@/ts/utilities/types/optional';

export default function toCaseFirst(
  value: Optional<string>,
  applyCase: (v: string) => string,
) {
  const valueOrEmpty = valueOr(value, '');

  return `${applyCase(valueOrEmpty.charAt(0))}${valueOrEmpty.slice(1)}`;
}
