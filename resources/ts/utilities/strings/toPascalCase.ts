import toCase from '@/ts/utilities/strings/toCase';
import toUpperFirst from '@/ts/utilities/strings/toUpperFirst';

export default function toPascalCase(value: string): string {
  return toCase(value, (w) => toUpperFirst(w.toLowerCase()), '');
}
