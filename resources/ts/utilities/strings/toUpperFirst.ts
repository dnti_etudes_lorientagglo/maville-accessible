import toCaseFirst from '@/ts/utilities/strings/toCaseFirst';
import { Optional } from '@/ts/utilities/types/optional';

export default function toUpperFirst(value: Optional<string>) {
  return toCaseFirst(value, (v) => v.toUpperCase());
}
