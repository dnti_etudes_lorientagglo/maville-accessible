export type TruncateOptions = {
  size?: number;
  end?: string;
};

export default function truncate(text: string, options: TruncateOptions = {}) {
  const size = options.size ?? 255;
  if (text.length <= size) {
    return text;
  }

  const end = options.end ?? '...';
  const maxLength = size - end.length;
  const truncatedText = text.substring(0, maxLength);
  const lastSpaceIndex = truncatedText.lastIndexOf(' ');

  return lastSpaceIndex !== -1
    ? `${truncatedText.substring(0, Math.min(truncatedText.length, lastSpaceIndex))}${end}`
    : `${truncatedText}${end}`;
}
