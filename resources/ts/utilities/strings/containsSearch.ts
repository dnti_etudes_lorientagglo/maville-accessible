import isNil from '@/ts/utilities/common/isNil';
import isNone from '@/ts/utilities/common/isNone';
import normalize from '@/ts/utilities/strings/normalize';
import { Optional } from '@/ts/utilities/types/optional';

export default function containsSearch(value?: Optional<string>, search?: Optional<string>) {
  if (isNil(value) || isNone(search)) {
    return false;
  }

  return normalize(value).indexOf(normalize(search)) !== -1;
}
