export default function toCase(
  value: string,
  converter: (word: string) => string,
  separator: string,
): string {
  const matches = value.match(
    /[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g,
  );
  if (matches === null) {
    return value;
  }

  return matches.map(converter).join(separator);
}
