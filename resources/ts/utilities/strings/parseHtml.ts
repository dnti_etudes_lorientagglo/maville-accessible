export default function parseHtml(html: string) {
  return new DOMParser().parseFromString(html, 'text/html').body;
}
