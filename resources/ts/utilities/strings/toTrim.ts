import valueOr from '@/ts/utilities/functions/valueOr';
import { Optional } from '@/ts/utilities/types/optional';

export default function toTrim(value: Optional<string>) {
  return valueOr(value, '').trim();
}
