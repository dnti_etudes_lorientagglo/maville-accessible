import makeStoredValue from '@/ts/utilities/storages/makeStoredValue';

export default makeStoredValue('app.navigation.rail', {
  defaultValue: false,
});
