import type themes from '@/ts/plugins/vuetify/themes';
import { THEME_LIGHT } from '@/ts/plugins/vuetify/themes/light';
import makeStoredValue from '@/ts/utilities/storages/makeStoredValue';

export type AccessibilityTheme = keyof typeof themes;

export type AccessibilityPreferences = {
  theme: AccessibilityTheme;
  snackbarTimeout: number;
  reducedColor: boolean;
  fontSize: number;
  letterSpacing: number;
  lineHeight: number;
  prioritizeCategories: string[];
  mediaAutoplay: 'on' | 'on-muted' | 'off',
};

export const DEFAULT_ACCESSIBILITY_PREFERENCES = {
  theme: THEME_LIGHT,
  snackbarTimeout: 5000,
  reducedColor: false,
  fontSize: 16,
  letterSpacing: 0,
  lineHeight: 1.5,
  prioritizeCategories: [],
  mediaAutoplay: 'on',
} as AccessibilityPreferences;

export default makeStoredValue('app.accessibility.preferences', {
  defaultValue: DEFAULT_ACCESSIBILITY_PREFERENCES,
});
