import AccessibilityCriteria from '@/ts/api/enums/accessibilityCriteria';
import isNone from '@/ts/utilities/common/isNone';
import { Dictionary } from '@/ts/utilities/types/dictionary';

export default function isCriteriaDisplayed(values: Dictionary, criteria: AccessibilityCriteria) {
  return Object.entries(criteria.metadata.dependsOn ?? {}).every(([key, value]) => {
    const currentValue = values[key];
    if (value === 'any') {
      return !isNone(currentValue);
    }

    if (Array.isArray(currentValue)) {
      return currentValue.indexOf(value) !== -1;
    }

    return currentValue === value;
  });
}
