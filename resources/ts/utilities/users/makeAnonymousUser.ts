import User from '@/ts/api/models/user';
import { i18nInstance } from '@/ts/plugins/i18n';
import { fill } from '@foscia/core';

export default function makeAnonymousUser() {
  return fill(new User(), {
    firstName: i18nInstance.t('users.anonymous.firstName'),
    lastName: i18nInstance.t('users.anonymous.lastName'),
  });
}
