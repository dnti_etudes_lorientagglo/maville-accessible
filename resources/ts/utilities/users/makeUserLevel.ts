import User from '@/ts/api/models/user';
import { IconValue } from '@/ts/icons/makeSvgIcon';
import { i18nInstance } from '@/ts/plugins/i18n';
import colors from '@/ts/utilities/colors/colors';
import { mdiStarFourPoints } from '@mdi/js';

export type UserLevel = {
  label: string;
  icon: IconValue;
  color: string;
};

export default function makeUserLevel(user: User): UserLevel | null {
  if (user.reviewsCount > 10) {
    return {
      label: i18nInstance.t('users.levels.default'),
      color: colors.purple,
      icon: mdiStarFourPoints,
    };
  }

  return null;
}
