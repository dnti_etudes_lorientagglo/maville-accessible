import { Optional } from '@/ts/utilities/types/optional';

export default function loadScript(script: string, crossOrigin?: Optional<string>) {
  return new Promise((resolve, reject) => {
    const scriptElement = document.createElement('script');
    scriptElement.async = true;
    scriptElement.defer = true;
    scriptElement.src = script;

    if (crossOrigin && ['anonymous', 'use-credentials'].includes(crossOrigin)) {
      scriptElement.crossOrigin = crossOrigin;
    }

    const head = document.head || document.getElementsByTagName('head')[0];
    head.appendChild(scriptElement);

    scriptElement.onload = resolve;
    scriptElement.onerror = reject;
  });
}
