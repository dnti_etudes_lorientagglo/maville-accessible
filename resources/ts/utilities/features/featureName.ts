import env from '@/ts/env';
import EnumOf from '@/ts/utilities/enums/enumOf';

export default class FeatureName extends EnumOf<string> {
  public static readonly PLACES = new FeatureName('PLACES');

  public static readonly ARTICLES = new FeatureName('ARTICLES');

  public static readonly ACTIVITIES = new FeatureName('ACTIVITIES');

  public static readonly EVENTS = new FeatureName('EVENTS');

  public static readonly NEWSLETTER = new FeatureName('NEWSLETTER');

  public static readonly COMMUNICATION = new FeatureName('COMMUNICATION');

  public static readonly ACCESSIBILITY = new FeatureName('ACCESSIBILITY');

  public static readonly REVIEWS = new FeatureName('REVIEWS');

  public static readonly REVIEWS_BODY = new FeatureName('REVIEWS_BODY');

  public check() {
    return !!env.features[this.value];
  }

  public when<T, U = undefined>(whenEnabled: T, whenDisabled?: U) {
    return (this.check() ? whenEnabled : whenDisabled) as T | U;
  }
}
