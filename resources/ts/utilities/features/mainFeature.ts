import EnumWithProps from '@/ts/utilities/enums/enumWithProps';
import FeatureName from '@/ts/utilities/features/featureName';
import {
  mdiCalendarRangeOutline,
  mdiMapMarkerOutline,
  mdiNewspaperVariantOutline,
  mdiViewGridOutline,
} from '@mdi/js';

type MainFeatureProps = {
  color: string;
  icon: string;
};

export default class MainFeature extends EnumWithProps<string, MainFeatureProps> {
  public static readonly PLACES = new MainFeature(FeatureName.PLACES.value, {
    icon: mdiMapMarkerOutline,
    color: 'secondary',
  });

  public static readonly ARTICLES = new MainFeature(FeatureName.ARTICLES.value, {
    icon: mdiNewspaperVariantOutline,
    color: 'tertiary',
  });

  public static readonly EVENTS = new MainFeature(FeatureName.EVENTS.value, {
    icon: mdiCalendarRangeOutline,
    color: 'quaternary',
  });

  public static readonly ACTIVITIES = new MainFeature(FeatureName.ACTIVITIES.value, {
    icon: mdiViewGridOutline,
    color: 'quinary',
  });
}
