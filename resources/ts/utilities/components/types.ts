import { ComponentPublicInstance, FunctionalComponent } from 'vue';

export type VueComponent<Props = any> =
  | { new(): ComponentPublicInstance<Props> }
  | FunctionalComponent<Props>;
