export type AnyFunction<A extends any[] = any[], R = any> = (...args: A) => R;
