import decomposeLocale from '@/ts/utilities/lang/decomposeLocale';

export default function formatNumber(
  value: number,
  locale: string,
  options: Intl.NumberFormatOptions = {},
) {
  return value.toLocaleString(decomposeLocale(locale)[0], options);
}
