import formatNumber from '@/ts/utilities/math/formatNumber';

export default function humanFileSize(bytes: number, locale: string, decimals = 1) {
  if (!+bytes) return '0 Bytes';

  const k = 1024;
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return `${formatNumber(bytes / (k ** i), locale, {
    maximumFractionDigits: decimals,
  })}\u00A0${sizes[i]}`;
}
