import { i18nInstance } from '@/ts/plugins/i18n';

export type Unit = 'percent' | 'second';

export default function withUnit(value: unknown, unit: Unit) {
  return i18nInstance.t(`common.units.${unit}`, { n: value });
}
