import get, { ValueRetriever } from '@/ts/utilities/objects/get';

export default function sumBy<T>(
  values: T[],
  key?: ValueRetriever<T>,
) {
  return values.reduce((total, value) => total + (get(value, key, 0) as number), 0);
}
