import action from '@/ts/api/action';
import Place from '@/ts/api/models/place';
import { include, query } from '@foscia/core';
import { filterBy } from '@foscia/jsonapi';

export default function mapPlacesAction() {
  return action()
    .use(query(Place))
    .use(filterBy('category', ['any']))
    .use(include('categories', 'categories.parents', 'ownerOrganization'));
}
