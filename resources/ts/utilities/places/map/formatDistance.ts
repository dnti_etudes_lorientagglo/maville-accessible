import formatNumber from '@/ts/utilities/math/formatNumber';

export default function formatDistance(distance: number, locale: string) {
  if (distance >= 20) {
    return `${formatNumber(distance, locale, { maximumFractionDigits: 0 })}km`;
  }

  if (distance >= 1) {
    return `${formatNumber(distance, locale, {
      minimumFractionDigits: 1,
      maximumFractionDigits: 1,
    })}km`;
  }

  return `${formatNumber(distance * 1000, locale, { maximumFractionDigits: 0 })}m`;
}
