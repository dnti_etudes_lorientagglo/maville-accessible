import { GeoCoordinates } from '@/ts/utilities/places/map/types';

function radial(value: number) {
  return (value * Math.PI) / 180;
}

export default function distance(
  coordinates: GeoCoordinates,
  otherCoordinates: GeoCoordinates,
) {
  const radius = 6371; // km
  const dLat = radial(otherCoordinates.latitude - coordinates.latitude);
  const dLon = radial(otherCoordinates.longitude - coordinates.longitude);
  const rLat = radial(coordinates.latitude);
  const rOtherLat = radial(otherCoordinates.latitude);

  const a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
    + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(rLat) * Math.cos(rOtherLat);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

  return radius * c;
}
