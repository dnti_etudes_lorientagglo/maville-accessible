import Place from '@/ts/api/models/place';
import makeSvgIcon from '@/ts/icons/makeSvgIcon';
import mainCategories from '@/ts/utilities/categories/mainCategories';
import categoryColor from '@/ts/utilities/colors/categoryColor';
import translate from '@/ts/utilities/lang/translate';
import addressLabel from '@/ts/utilities/places/addresses/addressLabel';
import addressToGeoCoordinates from '@/ts/utilities/places/addresses/addressToGeoCoordinates';
import { SyncPointOfInterest } from '@/ts/utilities/places/map/types';
import { mdiOfficeBuildingOutline } from '@mdi/js';

export default function placeToPOI(
  place: Place,
  options?: { prepend?: string[]; append?: string[]; },
): SyncPointOfInterest {
  const categories = mainCategories(place.categories);
  const color = categories[0] ? categoryColor(categories[0]) : undefined;
  const icon = categories[0]?.icon
    ? makeSvgIcon(categories[0].icon)
    : mdiOfficeBuildingOutline;

  return {
    id: place.id,
    icon,
    color,
    coordinates: addressToGeoCoordinates(place.address),
    title: translate(place.name),
    subtitle: [
      ...(options?.prepend ?? []),
      categories.map((c) => translate(c.name)).join(', '),
      addressLabel(place.address),
      ...(options?.append ?? []),
    ].filter((l) => l).join(' · '),
  };
}
