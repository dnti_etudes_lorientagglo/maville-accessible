import { IconValue } from '@/ts/icons/makeSvgIcon';
import { Awaitable } from '@/ts/utilities/types/awaitable';
import { ModelIdType } from '@foscia/core';
import { FitBoundsOptions, FlyToOptions, Map } from 'maplibre-gl';
import { Ref } from 'vue';

export type GeoCoordinates = {
  latitude: number;
  longitude: number;
};

export type AccurateGeoLocation = GeoCoordinates & {
  accuracy: number;
};

export type PointOfInterest = {
  id: ModelIdType;
  title: string;
  subtitle?: string;
  icon: IconValue;
  color?: string;
  coordinates: GeoCoordinates | (() => Awaitable<GeoCoordinates | null>);
};

export type SyncPointOfInterest = {
  id: ModelIdType;
  title: string;
  subtitle?: string;
  icon: IconValue;
  color?: string;
  coordinates: GeoCoordinates;
};

export type GeoBounds = [GeoCoordinates, GeoCoordinates];

export type MglMapLocationRef = {
  canGetLocation(): Promise<boolean>;
  getLocation(): Promise<AccurateGeoLocation | null>;
  getLocationRef(): Ref<AccurateGeoLocation | null>;
};

export type MglMapRef = MglMapLocationRef & {
  loaded: boolean;
  map(): Map | null;
  flyTo(coordinates: GeoCoordinates, options?: FlyToOptions): void;
  fitBounds(
    bounds: GeoBounds,
    options?: FitBoundsOptions,
    eventData?: any,
  ): void;
};
