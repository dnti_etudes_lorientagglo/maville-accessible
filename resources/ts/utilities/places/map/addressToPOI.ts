import addressIcon from '@/ts/utilities/places/addresses/addressIcon';
import addressTitles from '@/ts/utilities/places/addresses/addressTitles';
import addressToGeoCoordinates from '@/ts/utilities/places/addresses/addressToGeoCoordinates';
import { SyncPointOfInterest } from '@/ts/utilities/places/map/types';
import { AddressObject } from '@/ts/utilities/places/types';

export default function addressToPOI(address: AddressObject): SyncPointOfInterest {
  const [title, subtitle] = addressTitles(address);

  return {
    id: address.id,
    coordinates: addressToGeoCoordinates(address),
    icon: addressIcon(address),
    title,
    subtitle,
  };
}
