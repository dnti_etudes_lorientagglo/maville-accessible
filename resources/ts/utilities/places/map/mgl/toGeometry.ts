import { GeoCoordinates } from '@/ts/utilities/places/map/types';
import { Geometry } from '@/ts/utilities/places/types';

export default function toGeometry(coordinates: GeoCoordinates): Geometry {
  return {
    type: 'Point',
    coordinates: [coordinates.longitude, coordinates.latitude],
  };
}
