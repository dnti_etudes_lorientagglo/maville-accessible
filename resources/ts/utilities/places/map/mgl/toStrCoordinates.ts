import { GeoCoordinates } from '@/ts/utilities/places/map/types';

export default function toStrCoordinates(coordinates: GeoCoordinates) {
  return [
    coordinates.longitude.toFixed(6),
    coordinates.latitude.toFixed(6),
  ].join(',');
}
