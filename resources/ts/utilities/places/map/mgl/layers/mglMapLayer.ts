import makeStoredValue from '@/ts/utilities/storages/makeStoredValue';

export default makeStoredValue('app.map.layer', {
  defaultValue: null as string | null,
});
