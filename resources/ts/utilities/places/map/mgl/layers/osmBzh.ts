import makeMglMapLayer from '@/ts/utilities/places/map/mgl/layers/makeMglMapLayer';

export default makeMglMapLayer('osmBzh', {
  version: 8,
  sources: {
    'openstreetmap.bzh': {
      type: 'raster',
      tiles: ['https://tile.openstreetmap.bzh/br/{z}/{x}/{y}.png'],
      tileSize: 256,
    },
  },
  layers: [
    {
      id: 'osmbzh',
      type: 'raster',
      source: 'openstreetmap.bzh',
      minzoom: 1,
      maxzoom: 20,
    },
  ],
});
