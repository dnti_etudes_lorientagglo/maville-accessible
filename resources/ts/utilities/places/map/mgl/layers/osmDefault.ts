import makeMglMapLayer from '@/ts/utilities/places/map/mgl/layers/makeMglMapLayer';

export default makeMglMapLayer('osmDefault', {
  version: 8,
  sources: {
    'openstreetmap.org': {
      type: 'raster',
      tiles: ['a', 'b', 'c'].map(
        (balancer) => `https://${balancer}.tile.openstreetmap.org/{z}/{x}/{y}.png`,
      ),
      tileSize: 256,
    },
  },
  layers: [
    {
      id: 'osm',
      type: 'raster',
      source: 'openstreetmap.org',
      minzoom: 1,
      maxzoom: 20,
    },
  ],
});
