import makeMglMapLayer from '@/ts/utilities/places/map/mgl/layers/makeMglMapLayer';

export default makeMglMapLayer('osmFr', {
  version: 8,
  sources: {
    'openstreetmap.fr': {
      type: 'raster',
      tiles: ['a', 'b', 'c'].map(
        (balancer) => `https://${balancer}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png`,
      ),
      tileSize: 256,
    },
  },
  layers: [
    {
      id: 'osmfr',
      type: 'raster',
      source: 'openstreetmap.fr',
      minzoom: 1,
      maxzoom: 20,
    },
  ],
});
