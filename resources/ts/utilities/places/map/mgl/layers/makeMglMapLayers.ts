import env from '@/ts/env';
import orderBy from '@/ts/utilities/arrays/orderBy';
import { MglMapLayer } from '@/ts/utilities/places/map/mgl/layers/makeMglMapLayer';
import osmBzh from '@/ts/utilities/places/map/mgl/layers/osmBzh';
import osmDefault from '@/ts/utilities/places/map/mgl/layers/osmDefault';
import osmFr from '@/ts/utilities/places/map/mgl/layers/osmFr';
import { Dictionary } from '@/ts/utilities/types/dictionary';

export default function makeMglMapLayers(): Dictionary<MglMapLayer> {
  const customLayers = orderBy(
    [
      osmFr,
      osmBzh,
      osmDefault,
    ].filter((l) => env.services.map.layers.indexOf(l.id) >= 0),
    (l) => env.services.map.layers.indexOf(l.id),
  );

  return (customLayers.length ? customLayers : [osmFr])
    .reduce((layers, layer) => ({
      ...layers,
      [layer.id]: layer,
    }), {} as Dictionary<MglMapLayer>);
}
