import { StyleSpecification } from 'maplibre-gl';
import { Composer } from 'vue-i18n';

export type MglMapLayer = {
  id: string;
  title: (i18n: Composer) => string;
  attribution: (i18n: Composer) => string;
  style: StyleSpecification;
};

export default function makeMglMapLayer(
  id: string,
  style: StyleSpecification,
): MglMapLayer {
  return {
    id,
    title: (i18n: Composer) => i18n.t(`map.map.layers.${id}.title`),
    attribution: (i18n: Composer) => i18n.t(`map.map.layers.${id}.attribution`),
    style,
  };
}
