import { MglMapLayer } from '@/ts/utilities/places/map/mgl/layers/makeMglMapLayer';
import { GeoCoordinates } from '@/ts/utilities/places/map/types';
import { RasterSourceSpecification } from 'maplibre-gl';

/*
 * Implementation from https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#X_and_Y.
 */

function longitudeToTile(lon: number, zoom: number) {
  return Math.floor(
    ((lon + 180) / 360) * (2 ** zoom),
  );
}

function latitudeToTile(lat: number, zoom: number) {
  const latRad = (lat * Math.PI) / 180;

  return Math.floor(
    ((1 - Math.log(Math.tan(latRad) + 1 / Math.cos(latRad)) / Math.PI) / 2) * (2 ** zoom),
  );
}

export default function makeMglMapLayerPreview(
  style: MglMapLayer,
  coordinates: GeoCoordinates,
  zoom: number,
) {
  const source = Object.values(style.style.sources)[0] as RasterSourceSpecification;
  const tile = (source.tiles ?? [])[0];
  if (!tile) {
    throw new Error('only raster tiles are working with map');
  }

  const x = longitudeToTile(coordinates.longitude, zoom);
  const y = latitudeToTile(coordinates.latitude, zoom);

  return tile
    .replace('{z}', String(zoom))
    .replace('{x}', String(x))
    .replace('{y}', String(y));
}
