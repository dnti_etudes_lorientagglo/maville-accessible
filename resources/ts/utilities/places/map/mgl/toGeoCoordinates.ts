import { GeoCoordinates } from '@/ts/utilities/places/map/types';
import { LngLat } from 'maplibre-gl';

export default function toGeoCoordinates(coordinates: LngLat): GeoCoordinates {
  return {
    latitude: coordinates.lat,
    longitude: coordinates.lng,
  };
}
