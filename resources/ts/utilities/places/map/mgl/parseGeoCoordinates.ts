import { GeoCoordinates } from '@/ts/utilities/places/map/types';

export default function parseGeoCoordinates(position: unknown): GeoCoordinates | null {
  if (typeof position === 'string') {
    const [lngStr, latStr] = position.split(',');
    const [lng, lat] = [Number.parseFloat(lngStr), Number.parseFloat(latStr ?? 'NaN')];
    if (!Number.isNaN(lng) && !Number.isNaN(lat)) {
      return { longitude: lng, latitude: lat };
    }
  }

  return null;
}
