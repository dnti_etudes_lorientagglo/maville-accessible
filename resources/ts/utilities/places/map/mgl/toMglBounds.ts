import toMglCoordinates from '@/ts/utilities/places/map/mgl/toMglCoordinates';
import { GeoBounds } from '@/ts/utilities/places/map/types';
import { LngLatBounds } from 'maplibre-gl';

export default function toMglBounds(bounds: GeoBounds): LngLatBounds {
  const mglBounds = new LngLatBounds(toMglCoordinates(bounds[0]), toMglCoordinates(bounds[0]));

  return mglBounds.extend(toMglCoordinates(bounds[1]));
}
