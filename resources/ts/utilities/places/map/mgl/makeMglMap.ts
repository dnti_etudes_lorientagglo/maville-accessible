import { Map, MapOptions, StyleSpecification } from 'maplibre-gl';

export default async function makeMglMap(
  container: HTMLElement,
  style: StyleSpecification,
  options: Partial<MapOptions> = {},
) {
  return new Map({
    container,
    style,
    attributionControl: false,
    minZoom: 1,
    maxZoom: 17.9,
    ...options,
  });
}
