import { GeoCoordinates } from '@/ts/utilities/places/map/types';
import { Geometry } from '@/ts/utilities/places/types';

export default function geometryToGeoCoordinates(geometry: Geometry) {
  return {
    longitude: geometry.coordinates[0],
    latitude: geometry.coordinates[1],
  } as GeoCoordinates;
}
