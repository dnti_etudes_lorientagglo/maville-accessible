import toGeoCoordinates from '@/ts/utilities/places/map/mgl/toGeoCoordinates';
import { GeoBounds } from '@/ts/utilities/places/map/types';
import { LngLatBounds } from 'maplibre-gl';

export default function toGeoBounds(bounds: LngLatBounds): GeoBounds {
  return [toGeoCoordinates(bounds.getSouthWest()), toGeoCoordinates(bounds.getNorthEast())];
}
