import { GeoCoordinates } from '@/ts/utilities/places/map/types';
import { LngLat } from 'maplibre-gl';

export default function toMglCoordinates(coordinates: GeoCoordinates): LngLat {
  return new LngLat(coordinates.longitude, coordinates.latitude);
}
