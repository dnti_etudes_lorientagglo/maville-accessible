import isNone from '@/ts/utilities/common/isNone';
import addressLabel from '@/ts/utilities/places/addresses/addressLabel';
import addressToGeoCoordinates from '@/ts/utilities/places/addresses/addressToGeoCoordinates';
import toStrCoordinates from '@/ts/utilities/places/map/mgl/toStrCoordinates';
import { AddressObject } from '@/ts/utilities/places/types';

export default function addressToQuery(address: AddressObject) {
  return {
    'addr.q': isNone(address.postalCode) ? address.address : addressLabel(address),
    'addr.t': address.type,
    'addr.p': toStrCoordinates(addressToGeoCoordinates(address)),
  };
}
