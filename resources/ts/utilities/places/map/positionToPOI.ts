import { i18nInstance } from '@/ts/plugins/i18n';
import { MglMapRef, PointOfInterest } from '@/ts/utilities/places/map/types';
import { mdiCrosshairsGps } from '@mdi/js';

export default function positionToPOI(map: MglMapRef): PointOfInterest {
  return {
    id: 'position',
    coordinates: () => map.getLocation(),
    icon: mdiCrosshairsGps,
    title: i18nInstance.t('views.places.direction.poi.position.title'),
  };
}
