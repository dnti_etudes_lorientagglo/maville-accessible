import { i18nInstance } from '@/ts/plugins/i18n';
import addressTitles from '@/ts/utilities/places/addresses/addressTitles';
import { GeoCoordinates, SyncPointOfInterest } from '@/ts/utilities/places/map/types';
import { AddressObject } from '@/ts/utilities/places/types';
import { Optional } from '@/ts/utilities/types/optional';
import { mdiPinOutline } from '@mdi/js';

export default function customToPOI(
  coordinates: GeoCoordinates,
  address?: Optional<AddressObject>,
): SyncPointOfInterest {
  const [title, subtitle] = address ? addressTitles(address) : [
    i18nInstance.t('views.places.direction.poi.custom.title'),
    i18nInstance.t('views.places.direction.poi.custom.subtitle'),
  ];

  return {
    id: 'custom',
    coordinates,
    icon: mdiPinOutline,
    title,
    subtitle,
  };
}
