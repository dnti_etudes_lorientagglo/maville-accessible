import isNone from '@/ts/utilities/common/isNone';
import parseGeoCoordinates from '@/ts/utilities/places/map/mgl/parseGeoCoordinates';
import toGeometry from '@/ts/utilities/places/map/mgl/toGeometry';
import { AddressType } from '@/ts/utilities/places/types';
import toTrim from '@/ts/utilities/strings/toTrim';
import { LocationQuery } from 'vue-router';

export default function addressFromQuery(query: LocationQuery) {
  const queryAddressCenter = parseGeoCoordinates(query['addr.p']);

  if (
    typeof query['addr.q'] === 'string'
    && !isNone(toTrim(query['addr.q']))
    && typeof query['addr.t'] === 'string'
    && !isNone(toTrim(query['addr.t']))
    && queryAddressCenter
  ) {
    return {
      id: 'custom-search',
      type: toTrim(query['addr.t']) as AddressType,
      address: toTrim(query['addr.q']),
      city: '',
      postalCode: '',
      inseeCode: '',
      geometry: toGeometry(queryAddressCenter),
    };
  }

  return undefined;
}
