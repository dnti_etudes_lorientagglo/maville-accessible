import { i18nInstance } from '@/ts/plugins/i18n';
import lightHighContrast from '@/ts/plugins/vuetify/themes/lightHighContrast';
import EnumWithProps from '@/ts/utilities/enums/enumWithProps';

export type DirectionManeuverAccessibilityProps = {
  chipColor: string;
  lineColor: (contrasted: boolean) => string;
};

export default class DirectionManeuverAccessibility
  extends EnumWithProps<string, DirectionManeuverAccessibilityProps> {
  public static readonly GREAT = new DirectionManeuverAccessibility('great', {
    chipColor: 'success',
    lineColor: (contrasted) => (contrasted ? lightHighContrast.colors.success : '#4caf50'),
  });

  public static readonly PARTIAL = new DirectionManeuverAccessibility('partial', {
    chipColor: 'warning',
    lineColor: (contrasted) => (contrasted ? lightHighContrast.colors.warning : '#ffa726'),
  });

  public static readonly WITH_ASSISTANCE = new DirectionManeuverAccessibility('withAssistance', {
    chipColor: 'warning',
    lineColor: (contrasted) => (contrasted ? lightHighContrast.colors.warning : '#ffa726'),
  });

  public static readonly BAD = new DirectionManeuverAccessibility('bad', {
    chipColor: 'error',
    lineColor: (contrasted) => (contrasted ? lightHighContrast.colors.error : '#ef5350'),
  });

  public static readonly UNKNOWN = new DirectionManeuverAccessibility('unknown', {
    chipColor: '#37474f',
    lineColor: (contrasted) => (contrasted ? '#37474f' : '#78909c'),
  });

  public get label() {
    return i18nInstance.t(`views.places.direction.accessibility.${this.value}`);
  }
}
