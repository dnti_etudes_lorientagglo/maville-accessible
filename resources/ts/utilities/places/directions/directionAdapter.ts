import makeDirectionAdapter from '@/ts/utilities/places/directions/makeDirectionAdapter';

export default makeDirectionAdapter();
