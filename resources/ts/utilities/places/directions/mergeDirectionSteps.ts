import { DirectionStep } from '@/ts/utilities/places/directions/direction';
import DirectionPedestrianManeuver
  from '@/ts/utilities/places/directions/directionPedestrianManeuver';

export default function mergeDirectionSteps(steps: DirectionStep[]) {
  return steps.reduce((mergedSteps, step) => {
    const lastStepIndex = mergedSteps.length - 1;
    const lastStep = mergedSteps[lastStepIndex];
    if (
      lastStep instanceof DirectionPedestrianManeuver
      && step instanceof DirectionPedestrianManeuver
    ) {
      mergedSteps.splice(lastStepIndex, 1, DirectionPedestrianManeuver.merge(lastStep, step));
    } else {
      mergedSteps.push(step);
    }

    return mergedSteps;
  }, [] as DirectionStep[]);
}
