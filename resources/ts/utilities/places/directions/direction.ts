import DirectionManeuver from '@/ts/utilities/places/directions/directionManeuver';
import DirectionTransitManeuverStop
  from '@/ts/utilities/places/directions/directionTransitManeuverStop';
import { GeoBounds } from '@/ts/utilities/places/map/types';
import { v4 as uuidV4 } from 'uuid';

export type DirectionSummary = {
  distance: number;
  time: number;
  bounds: GeoBounds;
  link: string | null;
  obstacles: boolean;
  departureAt?: Date;
  arrivalAt?: Date;
};

export type DirectionStep =
  | DirectionManeuver
  | DirectionTransitManeuverStop;

export default class Direction {
  public readonly id = uuidV4();

  public constructor(
    public readonly summary: DirectionSummary,
    public readonly steps: DirectionStep[],
  ) {
  }
}
