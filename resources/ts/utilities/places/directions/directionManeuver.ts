/* eslint-disable class-methods-use-this */
import foregroundColor from '@/ts/utilities/colors/foregroundColor';
import DirectionManeuverAccessibility
  from '@/ts/utilities/places/directions/directionManeuverAccessibility';
import DirectionManeuverType from '@/ts/utilities/places/directions/directionManeuverType';
import DirectionTravelType from '@/ts/utilities/places/directions/directionTravelType';
import makeLineLayers from '@/ts/utilities/places/directions/layers/makeLineLayers';
import {
  DirectionDrawableStep,
  DirectionDrawableStepLayersOptions,
} from '@/ts/utilities/places/directions/types';
import { GeoCoordinates } from '@/ts/utilities/places/map/types';
import { SourceSpecification } from 'maplibre-gl';

export type DirectionManeuverSummary = {
  name: string;
  instruction: string;
  distance: number;
  time: number;
};

export type DirectionManeuverAccessibilitySummary = {
  level: DirectionManeuverAccessibility;
  obstacles: boolean;
};

export default abstract class DirectionManeuver implements DirectionDrawableStep {
  protected constructor(
    public readonly maneuverType: DirectionManeuverType,
    public readonly travelType: DirectionTravelType,
    public readonly accessibility: DirectionManeuverAccessibilitySummary,
    public readonly summary: DirectionManeuverSummary,
    public readonly line: GeoCoordinates[],
  ) {
  }

  public get bgColor() {
    return undefined as string | undefined;
  }

  public get fgColor() {
    return (this.bgColor ? foregroundColor(this.bgColor) : undefined);
  }

  public source() {
    return {
      type: 'geojson',
      data: {
        type: 'Feature',
        properties: {},
        geometry: {
          type: 'LineString',
          coordinates: this.line.map(({ longitude, latitude }) => [longitude, latitude]),
        },
      },
    } as SourceSpecification;
  }

  public layers(source: string, options: DirectionDrawableStepLayersOptions) {
    return options.disabled
      ? makeLineLayers(source, {
        color: options.contrasted ? '#6f87d9' : '#b6c3ec',
        visible: options.visible,
      })
      : makeLineLayers(source, {
        color: options.color
          ?? this.accessibility.level.props.lineColor(options.contrasted ?? false),
        visible: options.visible,
        zIndex: 20,
      });
  }
}
