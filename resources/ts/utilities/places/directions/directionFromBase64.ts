import { DirectionValues } from '@/ts/utilities/places/directions/types';
import fromBase64 from '@/ts/utilities/strings/fromBase64';
import { mdiOfficeBuildingOutline } from '@mdi/js';

export default function directionFromBase64(value: unknown): Partial<DirectionValues> | null {
  try {
    if (typeof value !== 'string') {
      return null;
    }

    const parsed = JSON.parse(fromBase64(value)) as Partial<DirectionValues>;

    return {
      ...parsed,
      origin: parsed.origin && { ...parsed.origin, icon: mdiOfficeBuildingOutline },
      destination: parsed.destination && { ...parsed.destination, icon: mdiOfficeBuildingOutline },
    };
  } catch {
    return null;
  }
}
