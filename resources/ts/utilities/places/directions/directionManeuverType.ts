import mdiArrowUp from '@/assets/icons/map/arrow-up.svg?raw';
import mdiTurnLeft from '@/assets/icons/map/turn-left.svg?raw';
import mdiTurnRight from '@/assets/icons/map/turn-right.svg?raw';
import mdiTurnSharpLeft from '@/assets/icons/map/turn-sharp-left.svg?raw';
import mdiTurnSharpRight from '@/assets/icons/map/turn-sharp-right.svg?raw';
import mdiTurnSlightLeft from '@/assets/icons/map/turn-slight-left.svg?raw';
import mdiTurnSlightRight from '@/assets/icons/map/turn-slight-right.svg?raw';
import mdiUTurnLeft from '@/assets/icons/map/u-turn-left.svg?raw';
import mdiUTurnRight from '@/assets/icons/map/u-turn-right.svg?raw';
import makeSvgIcon, { IconValue } from '@/ts/icons/makeSvgIcon';
import EnumWithProps from '@/ts/utilities/enums/enumWithProps';
import { mdiArrowBottomLeft, mdiArrowBottomRight, mdiCircleOutline, mdiTrainBus } from '@mdi/js';

export type DirectionManeuverTypeProps = {
  icon: IconValue;
};

export default class DirectionManeuverType
  extends EnumWithProps<string, DirectionManeuverTypeProps> {
  public static readonly START = new DirectionManeuverType('start', {
    icon: makeSvgIcon(mdiArrowUp),
  });

  public static readonly START_RIGHT = new DirectionManeuverType('start-right', {
    icon: makeSvgIcon(mdiArrowUp),
  });

  public static readonly START_LEFT = new DirectionManeuverType('start-left', {
    icon: makeSvgIcon(mdiArrowUp),
  });

  public static readonly DESTINATION = new DirectionManeuverType('destination', {
    icon: mdiCircleOutline,
  });

  public static readonly DESTINATION_RIGHT = new DirectionManeuverType('destination-right', {
    icon: mdiCircleOutline,
  });

  public static readonly DESTINATION_LEFT = new DirectionManeuverType('destination-left', {
    icon: mdiCircleOutline,
  });

  public static readonly SLIGHT_RIGHT = new DirectionManeuverType('slight-right', {
    icon: makeSvgIcon(mdiTurnSlightRight),
  });

  public static readonly RIGHT = new DirectionManeuverType('right', {
    icon: makeSvgIcon(mdiTurnRight),
  });

  public static readonly SHARP_RIGHT = new DirectionManeuverType('sharp-right', {
    icon: makeSvgIcon(mdiTurnSharpRight),
  });

  public static readonly U_TURN_RIGHT = new DirectionManeuverType('u-turn-right', {
    icon: makeSvgIcon(mdiUTurnRight),
  });

  public static readonly U_TURN_LEFT = new DirectionManeuverType('u-turn-left', {
    icon: makeSvgIcon(mdiUTurnLeft),
  });

  public static readonly SHARP_LEFT = new DirectionManeuverType('sharp-left', {
    icon: makeSvgIcon(mdiTurnSharpLeft),
  });

  public static readonly LEFT = new DirectionManeuverType('left', {
    icon: makeSvgIcon(mdiTurnLeft),
  });

  public static readonly SLIGHT_LEFT = new DirectionManeuverType('slight-left', {
    icon: makeSvgIcon(mdiTurnSlightLeft),
  });

  public static readonly STAY_STRAIGHT = new DirectionManeuverType('stay-straight', {
    icon: makeSvgIcon(mdiArrowUp),
  });

  public static readonly STAY_RIGHT = new DirectionManeuverType('stay-right', {
    icon: mdiArrowBottomRight,
  });

  public static readonly STAY_LEFT = new DirectionManeuverType('stay-left', {
    icon: mdiArrowBottomLeft,
  });

  public static readonly TRANSIT = new DirectionManeuverType('transit', {
    icon: mdiTrainBus,
  });

  public static readonly UNKNOWN = new DirectionManeuverType('unknown', {
    icon: makeSvgIcon(mdiArrowUp),
  });
}
