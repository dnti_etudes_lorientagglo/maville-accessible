import env from '@/ts/env';
import makeHitineraireAdapter
  from '@/ts/utilities/places/implementations/makeHitineraireAdapter';
import { DirectionAdapter } from '@/ts/utilities/places/directions/types';

export default function makeDirectionAdapter(): DirectionAdapter | null {
  // You can replace this whole function content by a simple return statement
  // with your custom implementation.
  if (env.services.map.directions.implementation === 'hitineraire') {
    return makeHitineraireAdapter(env.services.map.directions.options);
  }

  return null;
}
