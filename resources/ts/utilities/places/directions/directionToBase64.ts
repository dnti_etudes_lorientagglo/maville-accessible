import { DirectionValues } from '@/ts/utilities/places/directions/types';
import { SyncPointOfInterest } from '@/ts/utilities/places/map/types';
import toBase64 from '@/ts/utilities/strings/toBase64';
import { toRaw } from 'vue';

export default function directionToBase64(values: Partial<DirectionValues>): string | null {
  const { origin, destination, locale, ...others } = values;

  const serializePOI = (poi: SyncPointOfInterest) => ({
    title: poi.title,
    subtitle: poi.subtitle,
    coordinates: poi.coordinates,
  });

  return toBase64(JSON.stringify(toRaw({
    origin: origin ? serializePOI(origin) : undefined,
    destination: destination ? serializePOI(destination) : undefined,
    ...others,
  })));
}
