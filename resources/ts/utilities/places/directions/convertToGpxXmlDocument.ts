import env from '@/ts/env';
import isNone from '@/ts/utilities/common/isNone';
import Direction from '@/ts/utilities/places/directions/direction';
import DirectionManeuver from '@/ts/utilities/places/directions/directionManeuver';
import DirectionTransitManeuverStop
  from '@/ts/utilities/places/directions/directionTransitManeuverStop';
import { GeoCoordinates, SyncPointOfInterest } from '@/ts/utilities/places/map/types';
import { Dictionary } from '@/ts/utilities/types/dictionary';

export default (
  metadata: {
    name: string;
    link: string;
    origin: SyncPointOfInterest;
    destination: SyncPointOfInterest;
  },
  direction: Direction,
) => {
  const doc = document.implementation.createDocument('http://www.topografix.com/GPX/1/1', '');
  const instruct = doc.createProcessingInstruction('xml', 'version="1.0" encoding="iso-8859-1"');
  doc.append(instruct);

  const makeTextElement = (tagName: string, text?: string) => {
    if (isNone(text)) {
      return null;
    }

    const element = doc.createElement(tagName);

    element.appendChild(doc.createTextNode(text));

    return element;
  };

  const makeElement = (
    tagName: string,
    children: (Node | null)[],
    attributes: Dictionary = {},
  ) => {
    const element = doc.createElement(tagName);

    children.forEach((child) => (child ? element.appendChild(child) : undefined));
    Object.entries(attributes).forEach(([key, value]) => {
      element.setAttribute(key, value);
    });

    return element;
  };

  const makeCoordinatesAttrs = (point: GeoCoordinates) => ({
    lat: point.latitude,
    lon: point.longitude,
  });

  doc.appendChild(makeElement('gpx', [
    makeElement('metadata', [
      makeTextElement('name', metadata.name),
      makeTextElement('link', metadata.link),
      makeElement('author', [
        makeTextElement('name', `${env.app.name}, ${env.app.territory}`),
      ]),
    ]),
    makeElement('wpt', [
      makeTextElement('name', metadata.origin.title),
      makeTextElement('desc', metadata.origin.subtitle),
    ], makeCoordinatesAttrs(metadata.origin.coordinates)),
    ...direction.steps.reduce((waypoints, step) => [
      ...waypoints,
      ...(step instanceof DirectionTransitManeuverStop ? [
        makeElement('wpt', [
          makeTextElement('name', step.name),
        ], makeCoordinatesAttrs(step.point)),
      ] : []),
    ], [] as Node[]),
    makeElement('wpt', [
      makeTextElement('name', metadata.destination.title),
      makeTextElement('desc', metadata.destination.subtitle),
    ], makeCoordinatesAttrs(metadata.destination.coordinates)),
    makeElement('trk', [
      makeElement('trkseg', [
        ...direction.steps.reduce((points, step) => {
          if (step instanceof DirectionManeuver) {
            const [firstPoint, ...otherPoints] = step.line;

            points.push(makeElement('trkpt', [
              makeTextElement('name', step.summary.name),
              makeTextElement('desc', step.summary.instruction),
            ], makeCoordinatesAttrs(firstPoint)));

            otherPoints.forEach((p) => points.push(
              makeElement('trkpt', [], makeCoordinatesAttrs(p)),
            ));
          }

          return points;
        }, [] as Node[]),
      ]),
    ]),
  ], {
    version: '1.1',
    creator: `${env.app.name}, ${env.app.territory}`,
    xmlns: 'http://www.topografix.com/GPX/1/1',
    'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
    'xsi:schemaLocation': 'http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd',
  }));

  return doc;
};
