import { i18nInstance } from '@/ts/plugins/i18n';
import EnumWithProps from '@/ts/utilities/enums/enumWithProps';
import {
  mdiBus,
  mdiFerry, mdiGondola,
  mdiSubwayVariant,
  mdiTrain,
  mdiTram,
  mdiWalk,
  mdiWheelchairAccessibility,
} from '@mdi/js';

type DirectionTravelTypeProps = {
  icon: string;
};

export default class DirectionTravelType extends EnumWithProps<string, DirectionTravelTypeProps> {
  public static readonly PEDESTRIAN_FOOT = new DirectionTravelType('pedestrian.foot', {
    icon: mdiWalk,
  });

  public static readonly PEDESTRIAN_WHEELCHAIR = new DirectionTravelType('pedestrian.wheelchair', {
    icon: mdiWheelchairAccessibility,
  });

  public static readonly TRANSIT_TRAM = new DirectionTravelType('transit.tram', {
    icon: mdiTram,
  });

  public static readonly TRANSIT_METRO = new DirectionTravelType('transit.metro', {
    icon: mdiSubwayVariant,
  });

  public static readonly TRANSIT_RAIL = new DirectionTravelType('transit.rail', {
    icon: mdiTrain,
  });

  public static readonly TRANSIT_BUS = new DirectionTravelType('transit.bus', {
    icon: mdiBus,
  });

  public static readonly TRANSIT_FERRY = new DirectionTravelType('transit.ferry', {
    icon: mdiFerry,
  });

  public static readonly TRANSIT_CABLE_CAR = new DirectionTravelType('transit.cableCar', {
    icon: mdiGondola,
  });

  public static readonly TRANSIT_GONDOLA = new DirectionTravelType('transit.gondola', {
    icon: mdiGondola,
  });

  public static readonly TRANSIT_FUNICULAR = new DirectionTravelType('transit.funicular', {
    icon: mdiBus,
  });

  public static readonly UNKNOWN = DirectionTravelType.PEDESTRIAN_FOOT;

  public get label() {
    return i18nInstance.t(`views.places.direction.travelTypes.${this.value}`);
  }
}
