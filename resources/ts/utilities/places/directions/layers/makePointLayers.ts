import { DirectionStepLayerSpec } from '@/ts/utilities/places/directions/types';

type LineLayerOptions = {
  background?: string;
  stroke: string;
  radius?: number;
  visible?: boolean;
  zIndex?: number;
};

export default function makePointLayers(
  source: string,
  options: LineLayerOptions,
): DirectionStepLayerSpec[] {
  return [
    {
      layerMapZIndex: options.zIndex ?? 0,
      id: source,
      type: 'circle',
      source,
      layout: {
        visibility: options.visible ? 'visible' : 'none',
      },
      paint: {
        'circle-color': options.background ?? '#ffffff',
        'circle-radius': options.radius ?? 7,
        'circle-stroke-color': options.stroke,
        'circle-stroke-width': 4,
      },
    },
  ];
}
