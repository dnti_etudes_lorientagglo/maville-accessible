import colorShade from '@/ts/utilities/colors/colorShade';
import { DirectionStepLayerSpec } from '@/ts/utilities/places/directions/types';

type LineLayerOptions = {
  color: string;
  width?: number;
  visible?: boolean;
  zIndex?: number;
};

export default function makeLineLayers(
  source: string,
  options: LineLayerOptions,
): DirectionStepLayerSpec[] {
  return [
    {
      layerMapZIndex: options.zIndex ?? 0,
      id: `${source}-outer`,
      type: 'line',
      source,
      layout: {
        'line-join': 'round',
        'line-cap': 'round',
        visibility: options.visible ? 'visible' : 'none',
      },
      paint: {
        'line-color': colorShade(options.color, -0.2),
        'line-width': (options.width ?? 10),
      },
    },
    {
      layerMapZIndex: (options.zIndex ?? 0) + 1,
      id: `${source}-inner`,
      type: 'line',
      source,
      layout: {
        'line-join': 'round',
        'line-cap': 'round',
        visibility: options.visible ? 'visible' : 'none',
      },
      paint: {
        'line-color': options.color,
        'line-width': (options.width ?? 10) - 6,
      },
    },
  ];
}
