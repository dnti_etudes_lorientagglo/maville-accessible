import DirectionManeuver, {
  DirectionManeuverAccessibilitySummary,
  DirectionManeuverSummary,
} from '@/ts/utilities/places/directions/directionManeuver';
import DirectionManeuverType from '@/ts/utilities/places/directions/directionManeuverType';
import DirectionTravelType from '@/ts/utilities/places/directions/directionTravelType';
import { GeoCoordinates } from '@/ts/utilities/places/map/types';

export default class DirectionPedestrianManeuver extends DirectionManeuver {
  public static merge(
    maneuver: DirectionPedestrianManeuver,
    nextManeuver: DirectionPedestrianManeuver,
  ) {
    return new DirectionPedestrianManeuver(
      maneuver.maneuverType,
      maneuver.travelType,
      maneuver.accessibility,
      {
        name: `${maneuver.summary.name} ${nextManeuver.summary.name}`,
        instruction: `${maneuver.summary.instruction}\n${nextManeuver.summary.instruction}`,
        distance: maneuver.summary.distance + nextManeuver.summary.distance,
        time: maneuver.summary.time + nextManeuver.summary.time,
      },
      [
        ...maneuver.line.slice(0, -1),
        ...nextManeuver.line,
      ],
    );
  }

  public constructor(
    maneuverType: DirectionManeuverType,
    travelType: DirectionTravelType,
    accessibility: DirectionManeuverAccessibilitySummary,
    summary: DirectionManeuverSummary,
    line: GeoCoordinates[],
  ) {
    super(maneuverType, travelType, accessibility, summary, line);
  }
}
