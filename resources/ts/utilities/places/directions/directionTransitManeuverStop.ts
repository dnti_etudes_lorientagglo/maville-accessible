/* eslint-disable class-methods-use-this */
import makePointLayers from '@/ts/utilities/places/directions/layers/makePointLayers';
import {
  DirectionDrawableStep,
  DirectionDrawableStepLayersOptions,
} from '@/ts/utilities/places/directions/types';
import { GeoCoordinates } from '@/ts/utilities/places/map/types';
import { SourceSpecification } from 'maplibre-gl';

export default class DirectionTransitManeuverStop implements DirectionDrawableStep {
  public constructor(
    public readonly name: string,
    public readonly color: string,
    public readonly departureAt: Date,
    public readonly arrivalAt: Date,
    public readonly point: GeoCoordinates,
  ) {
  }

  public source() {
    return {
      type: 'geojson',
      data: {
        type: 'Feature',
        properties: {},
        geometry: {
          type: 'Point',
          coordinates: [this.point.longitude, this.point.latitude],
        },
      },
    } as SourceSpecification;
  }

  public layers(source: string, options: DirectionDrawableStepLayersOptions) {
    return options.disabled
      ? makePointLayers(source, {
        stroke: options.contrasted ? '#6f87d9' : '#b6c3ec',
        visible: options.visible,
        zIndex: 30,
      })
      : makePointLayers(source, {
        stroke: options.color ?? this.color,
        visible: options.visible,
        zIndex: 30,
      });
  }
}
