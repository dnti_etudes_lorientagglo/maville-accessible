/* eslint-disable class-methods-use-this */
import DirectionManeuver, {
  DirectionManeuverAccessibilitySummary,
  DirectionManeuverSummary,
} from '@/ts/utilities/places/directions/directionManeuver';
import DirectionManeuverType from '@/ts/utilities/places/directions/directionManeuverType';
import DirectionTransitManeuverStop
  from '@/ts/utilities/places/directions/directionTransitManeuverStop';
import DirectionTravelType from '@/ts/utilities/places/directions/directionTravelType';
import makeLineLayers from '@/ts/utilities/places/directions/layers/makeLineLayers';
import { DirectionDrawableStepLayersOptions } from '@/ts/utilities/places/directions/types';
import { GeoCoordinates } from '@/ts/utilities/places/map/types';

export type DirectionTransitManeuverSummary = {
  shortName: string;
  longName: string;
  directionName: string;
  color: string;
  stops: DirectionTransitManeuverStop[];
};

export default class DirectionTransitManeuver extends DirectionManeuver {
  public constructor(
    maneuverType: DirectionManeuverType,
    travelType: DirectionTravelType,
    accessibility: DirectionManeuverAccessibilitySummary,
    summary: DirectionManeuverSummary,
    line: GeoCoordinates[],
    public readonly transit: DirectionTransitManeuverSummary,
  ) {
    super(maneuverType, travelType, accessibility, summary, line);
  }

  public get bgColor() {
    return this.transit.color;
  }

  public layers(source: string, options: DirectionDrawableStepLayersOptions) {
    return options.disabled
      ? super.layers(source, options)
      : makeLineLayers(source, {
        width: 12,
        color: options.color ?? this.transit.color,
        visible: options.visible,
        zIndex: 10,
      });
  }
}
