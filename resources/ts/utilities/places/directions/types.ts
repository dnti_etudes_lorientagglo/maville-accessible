import { Input } from '@/ts/composables/forms/useInput';
import type Direction from '@/ts/utilities/places/directions/direction';
import { SyncPointOfInterest } from '@/ts/utilities/places/map/types';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { LayerSpecification, SourceSpecification } from 'maplibre-gl';
import { Component } from 'vue';

export type DirectionDrawableStepLayersOptions = {
  visible?: boolean;
  color?: string;
  contrasted?: boolean;
  disabled?: boolean;
};

export type DirectionStepLayerSpec =
  & LayerSpecification
  & { layerMapZIndex: number; };

export type DirectionDrawableStep = {
  source(): SourceSpecification;
  layers(source: string, options: DirectionDrawableStepLayersOptions): DirectionStepLayerSpec[];
};

/**
 * Valeurs minimales pour le calcul d'un itinéraire.
 * Elles seront fournies à l'adaptateur lors d'une requête de calcul d'itinéraires.
 */
export type DirectionValues = {
  locale: string;
  origin: SyncPointOfInterest;
  destination: SyncPointOfInterest;
};

/**
 * Adaptateur pour le calcul d'un itinéraire.
 */
export type DirectionAdapter<V extends DirectionValues = DirectionValues> = {
  /**
   * Name of the service.
   */
  readonly name: string;
  /**
   * Composant à afficher fournissant des informations sur le calculateur
   * d'itinéraires.
   * Si `null` est retourné par cette methode, aucune information ne sera affichée.
   */
  informationComponent(): Component | null;
  /**
   * Composant à afficher fournissant des champs de formulaire supplémentaires
   * (en complément du point de départ et d'arrivée).
   * Si `null` est retourné par cette methode, aucun champ supplémentaire ne sera affiché.
   */
  formComponent(): Component | null;
  /**
   * Définition des champs supplémentaires de formulaire
   * (en complément du point de départ et d'arrivée). Les valeurs fournies en paramètre
   * doivent être injectées comme valeurs par défaut des champs retournés.
   *
   * @param values
   */
  formInputs(values: Partial<V>): Dictionary<Input>;
  /**
   * Valeurs par défaut à définir sur les champs supplémentaires.
   */
  formDefaults(): Dictionary;
  /**
   * Calcul un ou plusieurs itinéraires à partir des valeurs fournies.
   *
   * @param values
   */
  computeDirection(values: V): Promise<Direction[]>;
};
