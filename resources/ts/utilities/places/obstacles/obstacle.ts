import { GeoCoordinates } from '@/ts/utilities/places/map/types';

export default class Obstacle {
  public constructor(
    public readonly id: string,
    public readonly image: string | null,
    public readonly name: string,
    public readonly description: string | null,
    public readonly reportedAt: Date,
    public readonly point: GeoCoordinates,
  ) {
  }
}
