import Obstacle from '@/ts/utilities/places/obstacles/obstacle';

/**
 * Adaptateur pour la récupération des obstacles temporaires.
 */
export type ObstacleAdapter = {
  /**
   * Récupère les obstacles temporaires présents sur le territoire.
   */
  computeObstacles(): Promise<Obstacle[]>;
};
