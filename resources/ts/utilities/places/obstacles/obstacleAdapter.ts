import makeObstacleAdapter from '@/ts/utilities/places/obstacles/makeObstacleAdapter';

export default makeObstacleAdapter();
