import env from '@/ts/env';
import makeHitineraireAdapter from '@/ts/utilities/places/implementations/makeHitineraireAdapter';
import { ObstacleAdapter } from '@/ts/utilities/places/obstacles/types';

export default function makeObstacleAdapter(): ObstacleAdapter | null {
  // You can replace this whole function content by a simple return statement
  // with your custom implementation.
  if (env.services.map.directions.implementation === 'hitineraire') {
    return makeHitineraireAdapter(env.services.map.directions.options);
  }

  return null;
}
