import isNone from '@/ts/utilities/common/isNone';
import { AddressObject } from '@/ts/utilities/places/types';

export default function addressTitles(address: AddressObject) {
  const postalCodeAndCity = `${address.postalCode} ${address.city}`;

  return isNone(address.address)
    ? [postalCodeAndCity]
    : [address.address, postalCodeAndCity];
}
