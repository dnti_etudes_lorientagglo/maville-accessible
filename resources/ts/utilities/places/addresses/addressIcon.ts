import isNone from '@/ts/utilities/common/isNone';
import { AddressObject } from '@/ts/utilities/places/types';
import { mdiCityVariantOutline, mdiMapMarkerOutline } from '@mdi/js';

export default function addressIcon(address: AddressObject) {
  return isNone(address.address)
    ? mdiCityVariantOutline
    : mdiMapMarkerOutline;
}
