import fetchAddresses from '@/ts/utilities/places/addresses/fetchAddresses';
import { GeoCoordinates } from '@/ts/utilities/places/map/types';

export default async function reverseSearchAddress(
  coordinates: GeoCoordinates,
) {
  const addresses = await fetchAddresses({
    path: 'reverse',
    limit: 1,
    lon: coordinates.longitude,
    lat: coordinates.latitude,
  });

  return addresses[0] ?? null;
}
