import { AddressObject } from '@/ts/utilities/places/types';

export default function addressToCityAddress(address: AddressObject) {
  return {
    id: address.inseeCode,
    type: 'municipality',
    address: undefined,
    city: address.city,
    postalCode: address.postalCode,
    inseeCode: address.inseeCode,
    geometry: address.geometry,
  } as AddressObject;
}
