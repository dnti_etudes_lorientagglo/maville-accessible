import addressTitles from '@/ts/utilities/places/addresses/addressTitles';
import { AddressObject } from '@/ts/utilities/places/types';

export default function addressLabel(address: AddressObject) {
  return addressTitles(address).join(', ');
}
