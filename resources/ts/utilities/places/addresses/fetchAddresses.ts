import action from '@/ts/api/action';
import { AddressObject, AddressType } from '@/ts/utilities/places/types';
import { raw } from '@foscia/core';
import { makeGet } from '@foscia/http';

/**
 * French addresses search API format.
 *
 * @see https://adresse.data.gouv.fr/api-doc/adresse
 */
type SearchAddress = {
  geometry: {
    type: 'Point';
    coordinates: [number, number];
  };
  properties: {
    type: 'housenumber' | 'street' | 'locality' | 'municipality';
    id: string;
    label: string;
    name: string;
    housenumber?: string;
    street?: string;
    district?: string;
    postcode: string;
    citycode: string;
    city: string;
    context: string;
  };
};

export type SearchAddressesOptions = {
  path?: string;
  query?: string;
  limit?: number;
  addressType?: AddressType;
  lon?: number | string;
  lat?: number | string;
};

// TODO Filter on app postal codes.
export default async function fetchAddresses(options: SearchAddressesOptions = {}) {
  const data = await action()
    .use(makeGet(options.path ?? 'search', {
      baseURL: 'https://api-adresse.data.gouv.fr',
      params: {
        q: options.query,
        limit: options.limit,
        type: options.addressType,
        lon: options.lon,
        lat: options.lat,
        autocomplete: 0,
      },
    }))
    .run(raw((r) => r.json()));

  return (data.features as SearchAddress[]).map(({ properties, geometry }): AddressObject => ({
    id: properties.id,
    type: properties.type,
    address: properties.name !== properties.city ? properties.name : undefined,
    city: properties.city,
    postalCode: properties.postcode,
    inseeCode: properties.citycode,
    geometry: {
      type: 'Point',
      coordinates: geometry.coordinates,
    },
  }));
}
