import env from '@/ts/env';
import fetchAddresses from '@/ts/utilities/places/addresses/fetchAddresses';
import { GeoCoordinates } from '@/ts/utilities/places/map/types';
import { AddressType } from '@/ts/utilities/places/types';

type SearchOptions = {
  center?: GeoCoordinates;
  limit?: number;
  addressType?: AddressType;
};

export default async function searchAddresses(
  search: string,
  options: SearchOptions = {},
) {
  if (search.length < 3 || search.length > 200) {
    return [];
  }

  return fetchAddresses({
    ...options,
    limit: options.limit,
    addressType: options.addressType,
    query: search,
    lon: options.center?.longitude ?? env.services.map.center.geometry.coordinates[0],
    lat: options.center?.latitude ?? env.services.map.center.geometry.coordinates[1],
  });
}
