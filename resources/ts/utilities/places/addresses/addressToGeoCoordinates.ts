import geometryToGeoCoordinates from '@/ts/utilities/places/map/mgl/geometryToGeoCoordinates';
import { AddressObject } from '@/ts/utilities/places/types';

export default function addressToGeoCoordinates(address: AddressObject) {
  return geometryToGeoCoordinates(address.geometry);
}
