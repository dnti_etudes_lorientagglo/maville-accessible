/**
 * Type of an address. Taken from gouv.fr addresses API.
 * This should not be changed.
 */
export type AddressType = 'housenumber' | 'street' | 'locality' | 'municipality';

export type Geometry = {
  type: 'Point';
  coordinates: [number, number];
};

/**
 * Standardize address object. Address property may be different depending on type.
 *  - "housenumber": house number followed by street name.
 *  - "street": street name.
 *  - "locality": locality name.
 *  - "municipality": city name (or a place in the city, such as a park, etc.).
 */
export type AddressObject = {
  id: string;
  type: AddressType;
  address?: string;
  city: string;
  postalCode: string;
  inseeCode: string;
  geometry: Geometry;
};
