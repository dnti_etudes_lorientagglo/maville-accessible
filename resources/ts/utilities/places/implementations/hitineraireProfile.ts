import makeStoredValue from '@/ts/utilities/storages/makeStoredValue';

/**
 * Available profile for Hitineraire API.
 * fm - Fauteuil manuel
 * fe - Fauteuil electrique
 * md - Marchant difficilement
 * da - Déficience auditive
 * dat - Déficience auditive totale
 * dv - Déficience visuelle
 * dvt - Déficience visuelle totale
 * dm - Déficience mentale
 * mm - Maladie mentale
 */
export type HitineraireProfile = 'fm' | 'fe' | 'md' | 'da' | 'dat' | 'dv' | 'dvt' | 'dm' | 'mm';

/**
 * Available profile for local inputs.
 */
export type HitineraireProfileObject = {
  profile: string;
  walk: string;
  visual: string;
  physical: string;
  hearing: string;
  cognitive: string;
};

export const DEFAULT_HITINERAIRE_PROFILE = {
  profile: 'physical',
  walk: 'good',
  visual: 'blind',
  physical: 'manualWheelchair',
  hearing: 'deaf',
  cognitive: 'psychic',
} as HitineraireProfileObject;

export default makeStoredValue('app.map.hitineraireProfile', {
  defaultValue: null as HitineraireProfileObject | null,
});
