import action from '@/ts/api/action';
import AccessibilityCategory from '@/ts/api/enums/accessibilityCategory';
import useInput from '@/ts/composables/forms/useInput';
import { useAccessibilityStore } from '@/ts/stores/accessibility';
import last from '@/ts/utilities/arrays/last';
import isNone from '@/ts/utilities/common/isNone';
import formatStandardDate from '@/ts/utilities/dates/formatStandardDate';
import timeToSeconds from '@/ts/utilities/dates/hours/timeToSeconds';
import parseStandardDate from '@/ts/utilities/dates/parseStandardDate';
import withHours from '@/ts/utilities/dates/withHours';
import Direction, { DirectionStep } from '@/ts/utilities/places/directions/direction';
import DirectionManeuverAccessibility
  from '@/ts/utilities/places/directions/directionManeuverAccessibility';
import DirectionManeuverType from '@/ts/utilities/places/directions/directionManeuverType';
import DirectionPedestrianManeuver
  from '@/ts/utilities/places/directions/directionPedestrianManeuver';
import DirectionTransitManeuver from '@/ts/utilities/places/directions/directionTransitManeuver';
import DirectionTransitManeuverStop
  from '@/ts/utilities/places/directions/directionTransitManeuverStop';
import DirectionTravelType from '@/ts/utilities/places/directions/directionTravelType';
import { DirectionAdapter, DirectionValues } from '@/ts/utilities/places/directions/types';
import hitineraireProfile, {
  DEFAULT_HITINERAIRE_PROFILE,
  HitineraireProfile,
  HitineraireProfileObject,
} from '@/ts/utilities/places/implementations/hitineraireProfile';
import geometryToGeoCoordinates from '@/ts/utilities/places/map/mgl/geometryToGeoCoordinates';
import toStrCoordinates from '@/ts/utilities/places/map/mgl/toStrCoordinates';
import Obstacle from '@/ts/utilities/places/obstacles/obstacle';
import { ObstacleAdapter } from '@/ts/utilities/places/obstacles/types';
import { Geometry } from '@/ts/utilities/places/types';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { raw } from '@foscia/core';
import { HttpInvalidRequestError, makeGet, makePost } from '@foscia/http';
import { toGeoJSON } from '@mapbox/polyline';
import { addSeconds } from 'date-fns';
import { LineString } from 'geojson';
import { defineAsyncComponent } from 'vue';
import { useI18n } from 'vue-i18n';

/**
 * Environment options for HiTineraire implementation.
 */
type HitineraireOptions = {
  endpoint: string;
  username: string;
  password: string;
};

/**
 * Additional options for HiTineraire direction computation.
 */
type HitineraireDirectionValues = DirectionValues & {
  profile: HitineraireProfileObject;
  multimodal: boolean;
  departureAtDate: string;
  departureAtTime: string;
};

/*
 * -----------------------------------------------------------------------------
 * HiTineraire data structure typings.
 * -----------------------------------------------------------------------------
 */

type HitineraireTripLegTransitManeuverStop = {
  name: string;
  lon: number;
  lat: number;
  departure_date_time?: string;
  arrival_date_time?: string;
};

type HitineraireTripLegManeuver = {
  type: number;
  instruction: string;
  street_names?: string[];
  begin_shape_index: number;
  end_shape_index: number;
  time: number;
  length: number;
  access_notes: Record<`acc_${HitineraireProfile}`, number>;
  has_obstacles: boolean;
  travel_mode: string;
  travel_type: string;
  transit_info?: {
    color: number;
    headsign: string;
    long_name: string;
    short_name: string;
    operator_name: string;
    operator_url: string;
    transit_stops: HitineraireTripLegTransitManeuverStop[];
  };
};

type HitineraireTripLeg = {
  maneuvers: HitineraireTripLegManeuver[];
  shape: string;
  summary: {
    length: number;
    time: number;
    min_lon: number;
    max_lon: number;
    min_lat: number;
    max_lat: number;
    has_obstacles: boolean;
  };
};

type HitineraireTrip = {
  legs: HitineraireTripLeg[];
  summary: {
    length: number;
    time: number;
    min_lon: number;
    max_lon: number;
    min_lat: number;
    max_lat: number;
    has_obstacles: boolean;
  };
};

type HitineraireTripResult = {
  trip: HitineraireTrip;
};

type HitineraireTripResponse = Partial<HitineraireTripResult> & {
  alternates?: HitineraireTripResult[];
};

type HitineraireObstacleResponse = {
  features: {
    geometry: Geometry;
    properties: {
      token: string;
      comment: string;
      explanation: string;
      datetime: string;
    };
  }[];
};

/*
 * -----------------------------------------------------------------------------
 * HiTineraire data parsing.
 * -----------------------------------------------------------------------------
 */

/**
 * Mapping between HiTineraire maneuvers types IDs and internal enum.
 */
const MANEUVER_TYPES_MAP = new Map([
  [1, DirectionManeuverType.START],
  [2, DirectionManeuverType.START_RIGHT],
  [3, DirectionManeuverType.START_LEFT],
  [4, DirectionManeuverType.DESTINATION],
  [5, DirectionManeuverType.DESTINATION_RIGHT],
  [6, DirectionManeuverType.DESTINATION_LEFT],
  [9, DirectionManeuverType.SLIGHT_RIGHT],
  [10, DirectionManeuverType.RIGHT],
  [11, DirectionManeuverType.SHARP_RIGHT],
  [12, DirectionManeuverType.U_TURN_RIGHT],
  [13, DirectionManeuverType.U_TURN_LEFT],
  [14, DirectionManeuverType.SHARP_LEFT],
  [15, DirectionManeuverType.LEFT],
  [16, DirectionManeuverType.SLIGHT_LEFT],
  [22, DirectionManeuverType.STAY_STRAIGHT],
  [23, DirectionManeuverType.STAY_RIGHT],
  [24, DirectionManeuverType.STAY_LEFT],
  [30, DirectionManeuverType.TRANSIT],
]);

/**
 * Mapping between HiTineraire travelling modes and internal enum.
 */
const TRAVEL_TYPES_MAP = new Map([
  ['foot', DirectionTravelType.PEDESTRIAN_FOOT],
  ['wheelchair', DirectionTravelType.PEDESTRIAN_WHEELCHAIR],
  ['tram', DirectionTravelType.TRANSIT_TRAM],
  ['metro', DirectionTravelType.TRANSIT_METRO],
  ['rail', DirectionTravelType.TRANSIT_RAIL],
  ['bus', DirectionTravelType.TRANSIT_BUS],
  ['ferry', DirectionTravelType.TRANSIT_FERRY],
  ['cable_car', DirectionTravelType.TRANSIT_CABLE_CAR],
  ['gondola', DirectionTravelType.TRANSIT_GONDOLA],
  ['funicular', DirectionTravelType.TRANSIT_FUNICULAR],
]);

/**
 * Mapping between HiTineraire accessibility levels and internal enum.
 */
const ACCESSIBILITY_MAP = new Map([
  [0, DirectionManeuverAccessibility.UNKNOWN],
  [1, DirectionManeuverAccessibility.GREAT],
  [2, DirectionManeuverAccessibility.PARTIAL],
  [3, DirectionManeuverAccessibility.WITH_ASSISTANCE],
  [4, DirectionManeuverAccessibility.BAD],
]);

const PROFILE_MAP = new Map([
  ['dvt', 'Déficience visuelle'],
  ['dv', 'Déficience visuelle'],
  ['fe', 'Déficience motrice'],
  ['fm', 'Déficience motrice'],
  ['dat', 'Déficience auditive'],
  ['da', 'Déficience auditive'],
  ['mm', 'Déficience mentale'],
  ['dm', 'Déficience mentale'],
  ['md', 'Marche'],
  [null, 'Marche'],
] as const);

const PROFILE_CONDITION_MAP = new Map([
  ['dvt', 'Totale (non-voyant)'],
  ['dv', 'Partielle (malvoyant)'],
  ['fe', 'Fauteuil roulant électrique'],
  ['fm', 'Fauteuil roulant manuel'],
  ['dat', 'Totale (sourd)'],
  ['da', 'Partielle (malentendant)'],
  ['mm', 'Déficience psychique'],
  ['dm', 'Déficience intellectuelle'],
  ['md', 'Marche difficilement'],
  [null, 'Endurance normale'],
] as const);

/**
 * Compute HiTineraire locale (BCP47 format).
 *
 * @param locale
 */
function computeHitineraireLocale(locale: string) {
  return locale.split('--')[0];
}

/**
 * Compute HiTineraire locale (two chars language code).
 *
 * @param locale
 */
function computeHitineraireQueryLocale(locale: string) {
  return computeHitineraireLocale(locale).split('-')[0];
}

/**
 * Convert an integer color to an hex color string.
 *
 * @param color
 */
function parseIntegerColor(color: number) {
  return `#${color.toString(16).padStart(6, '0')}`;
}

/**
 * Convert a string date to a Date object.
 *
 * @param date
 */
function parseDateTime(date: string) {
  return new Date(Date.parse(date));
}

/**
 * Parse an HiTineraire maneuver into a pedestrian or transit maneuver
 * internal object.
 *
 * @param maneuver
 * @param shape
 * @param profile
 */
function parseHitineraireManeuver(
  maneuver: HitineraireTripLegManeuver,
  shape: LineString,
  profile: HitineraireProfile | null,
) {
  // Compute common values to all maneuvers types.
  const maneuverType = MANEUVER_TYPES_MAP.get(maneuver.type)
    ?? DirectionManeuverType.UNKNOWN;
  const travelType = TRAVEL_TYPES_MAP.get(maneuver.travel_type)
    ?? DirectionTravelType.UNKNOWN;
  const hitineraireAccessNote = profile && maneuver.access_notes[`acc_${profile}`];
  const accessibility = {
    level: (hitineraireAccessNote && ACCESSIBILITY_MAP.get(hitineraireAccessNote))
      || DirectionManeuverAccessibility.UNKNOWN,
    obstacles: maneuver.has_obstacles,
  };
  const summary = {
    instruction: maneuver.instruction,
    distance: maneuver.length,
    time: maneuver.time,
  };
  const line = shape.coordinates.slice(
    maneuver.begin_shape_index,
    maneuver.end_shape_index + 1,
  ).map(([longitude, latitude]) => ({ longitude, latitude }));

  // When travel mode is "transit", create a transit maneuver with each
  // transit and stops information.
  if (maneuver.travel_mode === 'transit') {
    return new DirectionTransitManeuver(
      maneuverType,
      travelType,
      accessibility,
      {
        ...summary,
        name: `${travelType.label} ${maneuver.transit_info!.short_name} ${maneuver.transit_info!.headsign}`,
      },
      line,
      {
        color: parseIntegerColor(maneuver.transit_info!.color),
        shortName: maneuver.transit_info!.short_name,
        longName: maneuver.transit_info!.long_name,
        directionName: maneuver.transit_info!.headsign,
        stops: maneuver.transit_info!.transit_stops.map(
          (stop: HitineraireTripLegTransitManeuverStop) => new DirectionTransitManeuverStop(
            stop.name,
            parseIntegerColor(maneuver.transit_info!.color),
            stop.departure_date_time
              ? parseDateTime(stop.departure_date_time)
              : parseDateTime(stop.arrival_date_time!),
            stop.arrival_date_time
              ? parseDateTime(stop.arrival_date_time)
              : parseDateTime(stop.departure_date_time!),
            {
              longitude: stop.lon,
              latitude: stop.lat,
            },
          ),
        ),
      },
    );
  }

  // Otherwise, create a pedestrian maneuver.
  return new DirectionPedestrianManeuver(
    maneuverType,
    travelType,
    accessibility,
    { ...summary, name: (maneuver.street_names ?? []).join(', ') },
    line,
  );
}

/**
 * Parse an HiTineraire trip into a list of directions internal instances.
 *
 * @param trip
 * @param profile
 * @param values
 */
function parseHitineraireTrip(
  trip: HitineraireTrip,
  profile: HitineraireProfile | null,
  values: HitineraireDirectionValues,
) {
  return trip.legs.map((leg: HitineraireTripLeg, index) => {
    const link = `https://hitineraires.lorient-agglo.bzh/hitineraire/?${Object.entries({
      origin: toStrCoordinates(values.origin.coordinates),
      destination: toStrCoordinates(values.destination.coordinates),
      profil: encodeURIComponent(PROFILE_MAP.get(profile)!),
      condition: encodeURIComponent(PROFILE_CONDITION_MAP.get(profile)!),
      selectedPath: String(index),
      lng: computeHitineraireQueryLocale(values.locale),
      ...(values.multimodal ? {
        multimodal: 'true',
        time: `${values.departureAtDate}T${values.departureAtTime}`,
      } : {}),
    }).map((pair) => pair.join('=')).join('&')}`;

    // HiTineraire shape is an encoded Polyline for the whole trip,
    // so we will parse it and decompose it across all maneuvers.
    const directionShape = toGeoJSON(leg.shape, 6);
    // Parse each maneuvers for the direction.
    const directionSteps = leg.maneuvers.slice(0, -1).reduce(
      (steps: DirectionStep[], maneuver: HitineraireTripLegManeuver) => {
        const parsedManeuver = parseHitineraireManeuver(maneuver, directionShape, profile);

        // When encountering a transit maneuver, we prepend and append initial
        // and final stops to the direction steps.
        if (parsedManeuver instanceof DirectionTransitManeuver) {
          steps.push(parsedManeuver.transit.stops[0]);
        }

        steps.push(parsedManeuver);

        if (parsedManeuver instanceof DirectionTransitManeuver) {
          steps.push(last(parsedManeuver.transit.stops));
        }

        return steps;
      },
      [] as DirectionStep[],
    );

    // When requested direction is multimodal, we will also compute
    // the departure and arrival dates and times.
    let departureAt: Date | undefined;
    let arrivalAt: Date | undefined;
    if (values.multimodal && values.departureAtDate && values.departureAtTime) {
      const date = withHours(parseStandardDate(values.departureAtDate));
      departureAt = addSeconds(date, timeToSeconds(values.departureAtTime));
      arrivalAt = addSeconds(departureAt, leg.summary.time);
    }

    return new Direction({
      link,
      distance: leg.summary.length,
      time: leg.summary.time,
      obstacles: leg.summary.has_obstacles,
      departureAt,
      arrivalAt,
      bounds: [
        { longitude: leg.summary.min_lon, latitude: leg.summary.min_lat },
        { longitude: leg.summary.max_lon, latitude: leg.summary.max_lat },
      ],
    }, directionSteps);
  });
}

/*
 * -----------------------------------------------------------------------------
 * HiTineraire data fetching.
 * -----------------------------------------------------------------------------
 */

/**
 * Compute HiTineraire profile string from input values.
 *
 * @param profile
 */
function computeHitineraireProfile(
  profile: HitineraireProfileObject,
): HitineraireProfile | null {
  if (profile.profile === 'visual') {
    if (profile.visual === 'blind') {
      return 'dvt';
    }

    return 'dv';
  }

  if (profile.profile === 'physical') {
    if (profile.physical === 'electricWheelchair') {
      return 'fe';
    }

    return 'fm';
  }

  if (profile.profile === 'hearing') {
    if (profile.hearing === 'deaf') {
      return 'dat';
    }

    return 'da';
  }

  if (profile.profile === 'cognitive') {
    if (profile.cognitive === 'psychic') {
      return 'mm';
    }

    return 'dm';
  }

  if (profile.profile === 'walk' && profile.walk === 'difficulties') {
    return 'md';
  }

  return null;
}

/**
 * Compute HiTineraire request payload from profile and input values.
 *
 * @param profile
 * @param values
 */
function computeHitinerairePayload(
  profile: HitineraireProfile | null,
  values: HitineraireDirectionValues,
) {
  const options: Dictionary = {
    costing: 'walking',
    locations: [
      {
        lon: values.origin.coordinates.longitude,
        lat: values.origin.coordinates.latitude,
      },
      {
        lon: values.destination.coordinates.longitude,
        lat: values.destination.coordinates.latitude,
      },
    ],
    directions_options: {
      language: computeHitineraireLocale(values.locale),
    },
    alternates: 2,
  };

  if (profile) {
    options.costing = 'access';
    options.costing_options = {
      access: {
        impaired_profile: profile,
      },
    };
  }

  if (values.multimodal) {
    options.pedestrian_costing = options.costing;
    options.costing = 'multimodal';
    options.date_time = {
      type: 1,
      value: `${values.departureAtDate}T${values.departureAtTime}`,
    };
  }

  return options;
}

const makeHitineraireHeaders = (options: HitineraireOptions) => ({
  Authorization: `Basic ${btoa(`${options.username}:${options.password}`)}`,
  Accept: 'application/json',
  'Content-Type': 'application/json',
});

/**
 * Fetch HiTineraire direction raw data.
 *
 * @param options
 * @param profile
 * @param values
 */
async function fetchHitineraireTrips(
  options: HitineraireOptions,
  profile: HitineraireProfile | null,
  values: HitineraireDirectionValues,
): Promise<HitineraireTripResponse> {
  try {
    return await action().run(
      makePost(`${options.endpoint}/api/route`, computeHitinerairePayload(profile, values), {
        headers: makeHitineraireHeaders(options),
      }),
      raw((response) => response.json()),
    );
  } catch (error) {
    if (error instanceof HttpInvalidRequestError) {
      return {};
    }

    throw error;
  }
}

async function fetchHitineraireObstacles(
  options: HitineraireOptions,
): Promise<HitineraireObstacleResponse> {
  return action().run(
    makeGet(`${options.endpoint}/obstacles.geojson`, {
      headers: makeHitineraireHeaders(options),
    }),
    raw((response) => response.json()),
  );
}

/*
 * -----------------------------------------------------------------------------
 * HiTineraire implementation.
 * -----------------------------------------------------------------------------
 */

/**
 * Map between app prioritized accessibility categories and HiTineraire access
 * profiles.
 */
const PRIORITIZE_CATEGORIES_PROFILES_MAP = new Map([
  [AccessibilityCategory.PHYSICAL.value, 'physical'],
  [AccessibilityCategory.VISUAL.value, 'visual'],
  [AccessibilityCategory.HEARING.value, 'hearing'],
  [AccessibilityCategory.COGNITIVE.value, 'cognitive'],
]);

/**
 * Create the HiTineraire adapter using environment options.
 *
 * @param options
 */
export default function makeHitineraireAdapter(
  options: HitineraireOptions,
): DirectionAdapter<HitineraireDirectionValues> & ObstacleAdapter {
  const informationComponent = () => defineAsyncComponent(
    () => import('@/ts/components/places/direction/implementations/HitineraireInformation.vue'),
  );
  const formComponent = () => defineAsyncComponent(
    () => import('@/ts/components/places/direction/implementations/HitineraireDirectionInputs.vue'),
  );
  const formDefaults = () => {
    // Retrieve the profile object from local storage if available.
    let profile = hitineraireProfile.value;
    if (!profile) {
      profile = { ...DEFAULT_HITINERAIRE_PROFILE };

      // Define profile from prioritized accessibility category if possible.
      const accessibility = useAccessibilityStore();
      const prioritizedCategory = accessibility.preferences.prioritizeCategories[0];
      if (prioritizedCategory && PRIORITIZE_CATEGORIES_PROFILES_MAP.has(prioritizedCategory)) {
        profile.profile = PRIORITIZE_CATEGORIES_PROFILES_MAP.get(prioritizedCategory)!;
      }
    }

    return {
      profile,
      multimodal: true,
      departureAtDate: formatStandardDate(new Date()),
      departureAtTime: `${new Date().getHours().toString().padStart(2, '0')}:${new Date().getMinutes().toString().padStart(2, '0')}`,
    };
  };
  const formInputs = (values: HitineraireDirectionValues) => {
    const i18n = useI18n();

    return {
      profileInput: useInput({
        name: 'profile',
        label: i18n.t('views.places.direction.implementations.hitineraire.labels.profile'),
        value: values.profile,
      }),
      multimodalInput: useInput({
        name: 'multimodal',
        label: i18n.t('views.places.direction.implementations.hitineraire.labels.multimodal'),
        value: values.multimodal,
      }),
      departureAtDateInput: useInput({
        name: 'departureAtDate',
        label: i18n.t('views.places.direction.implementations.hitineraire.labels.departureAtDate'),
        value: values.departureAtDate,
      }),
      departureAtTimeInput: useInput({
        name: 'departureAtTime',
        label: i18n.t('views.places.direction.implementations.hitineraire.labels.departureAtTime'),
        value: values.departureAtTime,
      }),
    };
  };

  const computeDirection = async (values: HitineraireDirectionValues) => {
    const profile = computeHitineraireProfile(values.profile);
    const data = await fetchHitineraireTrips(options, profile, values);

    return [
      ...(data.trip ? parseHitineraireTrip(data.trip, profile, values) : []),
      ...(data.alternates ?? []).map(
        ({ trip }: HitineraireTripResult) => parseHitineraireTrip(trip, profile, values),
      ).flat(1),
    ];
  };

  const computeObstacles = async () => {
    const data = await fetchHitineraireObstacles(options);

    return data.features.map((item) => new Obstacle(
      item.properties.token,
      `https://vigilo.claav.fr/get_photo.php?token=${item.properties.token}`,
      item.properties.comment,
      isNone(item.properties.explanation) ? null : item.properties.explanation,
      parseDateTime(item.properties.datetime),
      geometryToGeoCoordinates(item.geometry),
    ));
  };

  return {
    name: 'HiTinéraire',
    informationComponent,
    formComponent,
    formDefaults,
    formInputs,
    computeDirection,
    computeObstacles,
  };
}
