import mapWithKeys from '@/ts/utilities/objects/mapWithKeys';
import toKebabCase from '@/ts/utilities/strings/toKebabCase';

export default mapWithKeys(
  import.meta.glob(['./*/*.vue'], { import: 'default' }),
  (componentModule, file) => {
    const actionName = toKebabCase(file.replace(/(^\.\/[a-zA-Z]+\/|View\.vue$)/g, ''));

    return { [actionName]: componentModule };
  },
);
