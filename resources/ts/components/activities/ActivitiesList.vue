<script
  lang="ts"
  setup
>
import accessibilitySvg from '@/assets/icons/accessibility.svg?raw';
import action from '@/ts/api/action';
import cachedCategories from '@/ts/api/cached/cachedCategories';
import withReviewsSummary from '@/ts/api/enhancers/withReviewsSummary';
import AccessibilityCategory from '@/ts/api/enums/accessibilityCategory';
import ActivityTypeCode from '@/ts/api/enums/activityTypeCode';
import Activity from '@/ts/api/models/activity';
import Category from '@/ts/api/models/category';
import Place from '@/ts/api/models/place';
import sortByAccessibility from '@/ts/api/utilities/accessible/sortByAccessibility';
import AccessibilityPriorizationAlert
  from '@/ts/components/accessibility/AccessibilityPriorizationAlert.vue';
import ActivityListItem from '@/ts/components/activities/ActivityListItem.vue';
import AppPagination from '@/ts/components/common/AppPagination.vue';
import VCategoryInput from '@/ts/components/forms/instances/VCategoryInput.vue';
import VSelectInput from '@/ts/components/forms/selects/VSelectInput.vue';
import VInputClear from '@/ts/components/forms/VInputClear.vue';
import VPublicCodesInput from '@/ts/components/forms/VPublicCodesInput.vue';
import useAccessibilityPrioritizeCategories
  from '@/ts/composables/accessibility/useAccessibilityPrioritizeCategories';
import useDynamicDocumentSubtitle from '@/ts/composables/accessibility/useDynamicDocumentSubtitle';
import useRouteActivityType from '@/ts/composables/activities/useRouteActivityType';
import useAsync from '@/ts/composables/common/useAsync';
import useForm from '@/ts/composables/forms/useForm';
import useInput from '@/ts/composables/forms/useInput';
import useCol from '@/ts/composables/layouts/useCol';
import useAnnouncer from '@/ts/composables/pages/useAnnouncer';
import makeSvgIcon from '@/ts/icons/makeSvgIcon';
import { useSnackbarStore } from '@/ts/stores/snackbar';
import isNone from '@/ts/utilities/common/isNone';
import formatStandardDate from '@/ts/utilities/dates/formatStandardDate';
import debounce from '@/ts/utilities/functions/debounce';
import translate from '@/ts/utilities/lang/translate';
import toUpperFirst from '@/ts/utilities/strings/toUpperFirst';
import { Optional } from '@/ts/utilities/types/optional';
import { all, include, ModelInstance, query, when } from '@foscia/core';
import { param } from '@foscia/http';
import { filterBy, paginate, sortBy, sortByDesc, usingDocument } from '@foscia/jsonapi';
import {
  mdiAccountMultipleCheckOutline,
  mdiFilterMinusOutline,
  mdiFilterPlusOutline,
  mdiMagnify,
  mdiTagMultipleOutline,
} from '@mdi/js';
import { computed, onActivated, ref, watch } from 'vue';
import { useI18n } from 'vue-i18n';
import { useRoute, useRouter } from 'vue-router';
import { useDisplay } from 'vuetify';

const props = defineProps<{
  about?: Optional<ModelInstance>;
  title: string;
}>();

type ActivityQuery = {
  page: string;
  past?: boolean;
  search?: Optional<string>;
  categories?: Optional<Category[]>;
  publics?: Optional<string[]>;
  accessible?: Optional<string[]>;
};

const accessibilityIcon = makeSvgIcon(accessibilitySvg);

const { xs, smAndDown } = useDisplay();
const i18n = useI18n();
const route = useRoute();
const router = useRouter();
const snackbar = useSnackbarStore();
const { loading, asyncRun } = useAsync({ currently: true });
const announcer = useAnnouncer();

const accessibilityPrioritizeCategories = useAccessibilityPrioritizeCategories();
const activityType = useRouteActivityType();

const normalCol = useCol();
const nNormalCol = useCol('n-normal');

const accessibleItems = computed(
  () => [...AccessibilityCategory.all.values()]
    .filter((category) => !AccessibilityCategory.RESTROOMS.is(category))
    .map((c) => ({ value: c.value, title: toUpperFirst(c.label) })),
);

const searchInput = useInput({
  name: 'search',
  value: '',
});

const categoriesInput = useInput({
  name: 'categories',
  value: [] as Category[],
});

const publicsInput = useInput({
  name: 'publics',
  label: i18n.t('resources.activities.labels.publics'),
  value: [] as string[],
});

const accessibleInput = useInput({
  name: 'accessible',
  label: i18n.t('views.activities.activities.labels.accessible'),
  value: [] as string[],
});

const pastInput = useInput({
  name: 'past',
  label: i18n.t(`activities.${activityType.value.value}.labels.past`),
  value: false,
});

const mounted = ref(false);
const initialized = ref(false);
const moreFilters = ref(false);
const queryValue = ref({ page: '1' } as ActivityQuery);
const data = ref({
  instances: [] as Activity[],
  page: { total: 0, current: 1, last: 1 },
});

const documentSubtitle = useDynamicDocumentSubtitle({
  total: computed(() => data.value.page.total),
  page: computed(() => data.value.page),
  search: searchInput.valueRef,
  filters: computed(() => (
    (categoriesInput.value.length ? 1 : 0)
    + (publicsInput.value.length ? 1 : 0)
    + (accessibleInput.value.length ? 1 : 0)
    + (pastInput.value ? 1 : 0)
  )),
});
const documentTitle = computed(() => [
  documentSubtitle.value.join(' | '),
  props.title,
]);

const moreFiltersIcon = computed(() => (
  moreFilters.value ? mdiFilterMinusOutline : mdiFilterPlusOutline
));
const moreFiltersTitle = computed(() => (
  moreFilters.value
    ? i18n.t('views.activities.activities.labels.lessFilters')
    : i18n.t('views.activities.activities.labels.moreFilters')
));

const replaceRoute = () => router.replace({
  query: {
    ...queryValue.value,
    categories: (queryValue.value.categories ?? []).map((c) => c.id),
    past: queryValue.value.past ? '1' : undefined,
    publics: (queryValue.value.publics ?? []),
    accessible: (queryValue.value.accessible ?? []),
  },
});

const replaceQuery = (newQuery: ActivityQuery) => {
  queryValue.value = { ...queryValue.value, ...newQuery };
};

const fetchInstances = () => asyncRun(async () => {
  await replaceRoute();

  const { instances, document } = await action()
    .use(query(Activity))
    .use(include(['cover', 'categories', 'place']))
    .use(sortByDesc('pinned', 'pinnedAt'))
    .use(sortByAccessibility())
    .use(when(ActivityTypeCode.ACTIVITIES.is(activityType.value), withReviewsSummary()))
    .use(filterBy('activityType', activityType.value.value))
    .use(param('withOpeningDaysDifferenceOver', formatStandardDate(new Date())))
    .use(when(
      queryValue.value.past,
      (a) => a.use(
        filterBy('hasNextOpening', false),
        filterBy('openedAtMostDaysAgo', 14),
        sortBy('openedDaysAgo'),
      ),
      (a) => a.use(
        filterBy('openingInAtLeastDays', 0),
        sortBy('openingInDays'),
      ),
    ))
    .use(when(
      (queryValue.value.publics ?? []).length,
      filterBy('publicCode', queryValue.value.publics),
    ))
    .use(when(
      (queryValue.value.accessible ?? []).length,
      filterBy('accessible', queryValue.value.accessible),
    ))
    .use(when(queryValue.value.search, (a, s) => a.use(filterBy('search', s)).use(sortBy('search'))))
    .use(when(queryValue.value.categories, (a, c) => a.use(filterBy('category', c.map(({ id }) => id)))))
    .use(when(props.about instanceof Place, (a) => a.use(filterBy('place', [props.about!.id]))))
    .use(sortByDesc('publishedAt'))
    .use(paginate({ number: queryValue.value.page, size: 16 }))
    .run(all(usingDocument));

  data.value = {
    instances,
    page: {
      total: document.meta!.page.total,
      current: document.meta!.page.currentPage,
      last: document.meta!.page.lastPage,
    },
  };

  announcer.announce({ title: documentTitle.value, scrollTo: null, focus: null });
});

const debounceFetchInstances = debounce(fetchInstances);

const { onSubmit } = useForm([
  searchInput, categoriesInput, publicsInput, accessibleInput, pastInput,
] as const, {
  onChange({ allValues }) {
    loading.value = true;
    replaceQuery({ page: '1', ...allValues });
    if (initialized.value) {
      debounceFetchInstances();
    }
  },
  async onSubmit({ allValues }) {
    replaceQuery({ page: '1', ...allValues });
    await fetchInstances();
  },
});

watch(accessibilityPrioritizeCategories, async () => {
  await fetchInstances();
  snackbar.toast(i18n.t('navigation.listLoaded'));
});

const onClickCategory = (category: Category) => {
  categoriesInput.value = [category];
  snackbar.toast(i18n.t('navigation.filterOn', { name: translate(category.name) }));
};

const onClearCategories = () => {
  categoriesInput.value = [];
};

const onClearPublics = () => {
  publicsInput.value = [];
};

const onClearAccessible = () => {
  accessibleInput.value = [];
};

const onToggleMoreFilters = () => {
  moreFilters.value = !moreFilters.value;
};

const onPage = async (newPage: number) => {
  if (loading.value) {
    return;
  }

  replaceQuery({ page: String(newPage) });
  await fetchInstances();
  announcer.announce({ title: documentTitle.value, scrollTo: 0 });
};

onActivated(async () => {
  if (!mounted.value) {
    mounted.value = true;

    if (typeof route.query.search === 'string') {
      searchInput.value = route.query.search;
      queryValue.value.search = searchInput.value;
    }

    if (Array.isArray(route.query.categories)) {
      const categories = await cachedCategories.value;

      categoriesInput.value = route.query.categories
        .map((id) => categories.find((c) => c.id === id))
        .filter((c) => !!c) as Category[];
      queryValue.value.categories = categoriesInput.value;
    }

    if (Array.isArray(route.query.publics) && route.query.publics.length) {
      publicsInput.value = route.query.publics as string[];
      queryValue.value.publics = route.query.publics as string[];
      moreFilters.value = true;
    }

    if (Array.isArray(route.query.accessible) && route.query.accessible.length) {
      accessibleInput.value = route.query.accessible as string[];
      queryValue.value.accessible = route.query.accessible as string[];
      moreFilters.value = true;
    }

    if (route.query.past) {
      pastInput.value = true;
      queryValue.value.past = pastInput.value;
      moreFilters.value = true;
    }

    await fetchInstances();

    initialized.value = true;

    announcer.announce({ title: documentTitle.value });
  } else {
    announcer.announce({ title: documentTitle.value, scrollTo: null, focus: null });
  }
});
</script>

<template>
  <v-row>
    <v-col
      v-if="accessibilityPrioritizeCategories.length"
      cols="12"
    >
      <accessibility-priorization-alert />
    </v-col>
    <v-col cols="12">
      <v-form
        :aria-label="$t('accessibility.forms.search.labels.page')"
        :aria-description="$t('accessibility.forms.search.descriptions.list')"
        role="search"
        @submit="onSubmit"
      >
        <v-row dense>
          <v-col v-bind="normalCol">
            <v-text-field
              :prepend-inner-icon="mdiMagnify"
              autocomplete="off"
              hide-details
              v-bind="searchInput.props"
            />
          </v-col>
          <v-spacer />
          <v-col v-bind="nNormalCol">
            <div class="d-flex align-center">
              <v-category-input
                :prepend-inner-icon="mdiTagMultipleOutline"
                multiple
                hide-details
                v-bind="categoriesInput.props"
              >
                <template #append-inner>
                  <v-input-clear
                    :label="categoriesInput.props.label"
                    :active="!isNone(categoriesInput.props.modelValue)"
                    @clear="onClearCategories"
                  />
                </template>
              </v-category-input>
              <v-btn
                v-if="smAndDown"
                :title="moreFiltersTitle"
                :icon="moreFiltersIcon"
                :active="moreFilters"
                :aria-expanded="String(moreFilters)"
                aria-controls="activities-additional-filters"
                variant="tonal"
                class="ml-2"
                @click="onToggleMoreFilters"
              />
            </div>
          </v-col>
          <v-expand-transition>
            <v-col
              v-show="!smAndDown || moreFilters"
              id="activities-additional-filters"
              cols="12"
            >
              <v-row dense>
                <v-col v-bind="normalCol">
                  <v-public-codes-input
                    :prepend-inner-icon="mdiAccountMultipleCheckOutline"
                    hide-details
                    v-bind="publicsInput.props"
                  >
                    <template #append-inner>
                      <v-input-clear
                        :label="publicsInput.props.label"
                        :active="!isNone(publicsInput.props.modelValue)"
                        @clear="onClearPublics"
                      />
                    </template>
                  </v-public-codes-input>
                </v-col>
                <v-spacer />
                <v-col v-bind="nNormalCol">
                  <v-select-input
                    :prepend-inner-icon="accessibilityIcon"
                    :items="accessibleItems"
                    multiple
                    hide-details
                    v-bind="accessibleInput.props"
                  >
                    <template #append-inner>
                      <v-input-clear
                        :label="accessibleInput.props.label"
                        :active="!isNone(accessibleInput.props.modelValue)"
                        @clear="onClearAccessible"
                      />
                    </template>
                  </v-select-input>
                </v-col>
                <v-col cols="12">
                  <v-switch
                    hide-details
                    v-bind="pastInput.props"
                  />
                </v-col>
              </v-row>
            </v-col>
          </v-expand-transition>
        </v-row>
      </v-form>
    </v-col>
    <v-col cols="12">
      <p
        v-if="!loading && !data.instances.length"
        class="text-large text-center"
      >
        {{ $t(`activities.${activityType.value}.none`) }}
      </p>
      <v-row role="list">
        <v-col
          v-for="activity in data.instances"
          :key="`instances-${activity.id}`"
          role="listitem"
          cols="12"
        >
          <activity-list-item
            :about="about"
            :activity="activity"
            class="d-flex flex-column h-100"
            @click:category="onClickCategory"
          />
        </v-col>
      </v-row>
    </v-col>
    <v-col cols="12">
      <app-pagination
        :model-value="data.page.current"
        :length="data.page.last"
        :total-visible="xs ? 1 : 4"
        @update:model-value="onPage"
      />
    </v-col>
  </v-row>
</template>
