<script
  lang="ts"
  setup
>
import MenuList from '@/ts/components/common/menu/MenuList.vue';
import useLazyValue from '@/ts/composables/common/useLazyValue';
import last from '@/ts/utilities/arrays/last';
import removeOne from '@/ts/utilities/arrays/removeOne';
import uniqBy from '@/ts/utilities/arrays/uniqBy';
import wrap from '@/ts/utilities/arrays/wrap';
import equals from '@/ts/utilities/common/equals';
import isNone from '@/ts/utilities/common/isNone';
import parseItem from '@/ts/utilities/common/menu/parseItem';
import { ListItem, ListItemRaw } from '@/ts/utilities/common/menu/types';
import scrollIntoViewIfVisible from '@/ts/utilities/dom/scrollIntoViewIfVisible';
import tap from '@/ts/utilities/functions/tap';
import get, { ValueRetriever } from '@/ts/utilities/objects/get';
import containsSearch from '@/ts/utilities/strings/containsSearch';
import normalize from '@/ts/utilities/strings/normalize';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { v4 as uuidV4 } from 'uuid';
import { computed, nextTick, ref, toRaw, unref, watch } from 'vue';
import { useI18n } from 'vue-i18n';

const emit = defineEmits<{
  (e: 'keydown', event: KeyboardEvent): void;
  (e: 'item:click', item: ListItem): void;
  (e: 'update:menu', menu: boolean): void;
  (e: 'update:model-value', value: any): void;
}>();

const props = defineProps<{
  modelValue?: any;
  appendValue?: boolean;
  menu?: boolean;
  mode: 'menu' | 'listbox';
  items: ListItemRaw[];
  itemTitle?: ValueRetriever<any>;
  itemValue?: ValueRetriever<any>;
  itemDisabled?: ValueRetriever<any>;
  itemProps?: ValueRetriever<any>;
  search?: string;
  autocomplete?: boolean;
  multiple?: boolean;
  returnObject?: boolean;
  readonly?: boolean;
  width?: string | number;
  maxHeight?: string | number;
  noData?: string;
  hideNoData?: boolean;
  hideCheck?: boolean;
  onItemActivate?: (item: ListItemRaw) => boolean;
}>();

const i18n = useI18n();

const overlayRef = ref(null as { activatorEl: HTMLElement | null; } | null);

const lazyMenu = useLazyValue(computed(() => props.menu ?? false));
const lazyValue = ref([] as ListItemRaw[]);

const id = ref(`list-${uuidV4()}`);
const activeId = ref(null as string | null);
const parsedItems = ref({} as Dictionary<ListItem>);
const displayedItems = ref([] as ListItem[]);
const cachedItems = ref([] as ListItem[]);

const isItem = (raw: ListItemRaw, otherRaw: ListItemRaw) => (
  toRaw(raw) === toRaw(otherRaw)
  || toRaw(parseItem(raw, props).value) === toRaw(parseItem(otherRaw, props).value)
);
const hasItem = (
  items: ListItem[],
  item: ListItem,
): boolean => items.some(
  (i) => isItem(i.raw, item.raw) || (item.parsed.items && hasItem(item.parsed.items, item)),
);

const makeListItem = (raw: ListItemRaw): ListItem => {
  const parsed = parseItem(raw, props);
  const itemId = `${id.value}-item-${parsed.id}`;

  if (!parsedItems.value[itemId]) {
    parsedItems.value[itemId] = {
      raw,
      active: computed(() => itemId === activeId.value),
      selected: computed(() => lazyValue.value.some((i) => isItem(raw, i))),
      disabled: computed(() => get(raw, props.itemDisabled ?? 'disabled') as boolean | undefined),
      parsed: {
        ...parsed,
        id: itemId,
        items: 'items' in raw ? raw.items.map(makeListItem) : undefined,
        divider: 'divider' in raw ? true : undefined,
      },
    };
  } else {
    parsedItems.value[itemId].parsed = {
      ...parsed,
      id: itemId,
      items: 'items' in raw ? raw.items.map(makeListItem) : undefined,
      divider: 'divider' in raw ? true : undefined,
    };
  }

  return parsedItems.value[itemId];
};

const modelToLazyValue = () => (
  props.returnObject
    ? [...wrap(props.modelValue)].map((v) => tap(v, (value) => {
      cachedItems.value = uniqBy([
        ...cachedItems.value,
        makeListItem(value) as unknown as ListItem,
      ], 'parsed.id') as any;
    }))
    : wrap(props.modelValue).map(
      (v) => props.items.find((i) => parseItem(i, props).value === v)
        ?? cachedItems.value.find((i) => i.parsed.value === v)?.raw,
    ).filter((v) => !!v)
);

lazyValue.value = modelToLazyValue();

const activatorEl = computed(() => overlayRef.value?.activatorEl ?? null);
const maxHeightPx = computed(() => `${props.maxHeight ?? '320'}px`);
const noDataText = computed(
  () => props.noData ?? i18n.t('forms.autocomplete.refineSearch', { n: 0 }),
);
const valueItems = computed(() => lazyValue.value.map(
  (v) => cachedItems.value.find((i) => parseItem(v, props).value === i.parsed.value),
).filter((v) => !!v) as ListItem[]);
const allDisplayedItems = computed(() => {
  const allItems = [...displayedItems.value];
  valueItems.value.forEach((item) => {
    if (!hasItem(allItems, item)) {
      if (props.appendValue) {
        allItems.push(item as any);
      } else {
        allItems.unshift(item as any);
      }
    }
  });

  return allItems;
});
const interactableItems = computed(() => allDisplayedItems.value.reduce((items, currentItem) => {
  const pushItem = (item: ListItem) => {
    if (unref(item.disabled)) {
      return;
    }

    if (item.parsed.items) {
      item.parsed.items.forEach(pushItem);
    } else if (!item.parsed.divider) {
      items.push(item);
    }
  };

  pushItem(currentItem);

  return items;
}, [] as ListItem[]));

const activeIndex = computed(() => interactableItems.value.findIndex(
  (item) => item.parsed.id === activeId.value,
));

watch([() => props.items, () => props.search], ([items, search]) => {
  const filterItems = (allItems: ListItem[]) => allItems.reduce((newItems, item) => {
    let newItem = item;
    if (newItem.parsed.items) {
      newItem = {
        ...newItem,
        parsed: {
          ...newItem.parsed,
          items: filterItems(newItem.parsed.items),
        },
      };
    }

    if ((newItem.parsed.items && newItem.parsed.items.length > 0)
      || containsSearch(newItem.parsed.title, search ?? null)
    ) {
      newItems.push(newItem);
    }

    return newItems;
  }, [] as ListItem[]);

  displayedItems.value = isNone(search)
    ? items.map(makeListItem) as any
    : filterItems(items.map(makeListItem) as ListItem[]) as any;
  cachedItems.value = uniqBy([
    ...cachedItems.value,
    ...interactableItems.value,
  ], 'parsed.id') as any;

  if (activeId.value && activeIndex.value === -1) {
    activeId.value = interactableItems.value[0]?.parsed?.id ?? null;
  }

  const newLazyValue = modelToLazyValue();
  if (!equals(newLazyValue, lazyValue.value)) {
    lazyValue.value = newLazyValue;
  }
}, { immediate: true });

watch(() => props.modelValue, (nextValue, prevValue) => {
  if (!equals(nextValue, prevValue)) {
    lazyValue.value = modelToLazyValue();
  }
});

watch(lazyValue, (nextValue, prevValue) => {
  if (!equals(nextValue, prevValue)) {
    const emittedValue = props.returnObject
      ? [...nextValue]
      : nextValue.map((i) => parseItem(i, props).value);
    if (props.multiple) {
      emit('update:model-value', emittedValue);
    } else {
      emit('update:model-value', emittedValue[0] ?? null);
    }
  }
});

watch(lazyMenu, (value) => {
  emit('update:menu', value);
});

const onFocusIndex = (index: number) => {
  const item = interactableItems.value[index];
  if (item) {
    activeId.value = item.parsed.id;

    const element = document.getElementById(activeId.value);
    if (element) {
      scrollIntoViewIfVisible(element);
    }
  }
};
const onFocusPrev = () => onFocusIndex(
  activeIndex.value > 0
    ? activeIndex.value - 1
    : interactableItems.value.length - 1,
);
const onFocusNext = () => onFocusIndex(
  (activeIndex.value + 1) < interactableItems.value.length
    ? activeIndex.value + 1
    : 0,
);
const onFocusFirst = () => onFocusIndex(0);
const onFocusLast = () => onFocusIndex(interactableItems.value.length - 1);
const onFocusStartingWith = (letters: string) => {
  const index = interactableItems.value.findIndex((item) => (
    normalize(item.parsed.title ?? '').startsWith(normalize(letters))
  ));
  if (index !== -1) {
    onFocusIndex(index);
  } else {
    onFocusFirst();
  }
};

const onActivate = () => {
  const elementId = activeId.value ?? interactableItems.value[0]?.parsed.id;
  if (elementId) {
    const element = document.getElementById(elementId);
    if (element) {
      element.click();
    }
  }
};

const onAddItem = (item: ListItem) => {
  if (props.multiple) {
    lazyValue.value = [...lazyValue.value, toRaw(item.raw)];
  } else {
    lazyValue.value = [toRaw(item.raw)];
  }
};

const onRemoveItem = (item: ListItem) => {
  const newValue = [...lazyValue.value];
  removeOne(newValue, item.raw);
  lazyValue.value = newValue;
};

const onRemoveLast = () => {
  const lastItem = last(valueItems.value);
  if (lastItem) {
    onRemoveItem(lastItem);
  }
};

const onItemClick = ({ item }: { event: Event; item: ListItem; }) => {
  emit('item:click', item);

  if (!props.onItemActivate || !props.onItemActivate(item.raw)) {
    if (props.mode === 'listbox' && !props.readonly) {
      if (unref(item.selected) && props.multiple) {
        onRemoveItem(item);
      } else {
        onAddItem(item);
      }
    }
  }

  if (props.mode === 'menu' || !props.multiple) {
    lazyMenu.value = false;
    activeId.value = null;
  }
};

const onBlur = (event: FocusEvent) => {
  // When blur comes outside the list, we should close the menu.
  try {
    const listElement = document.getElementById(id.value);
    if (
      props.mode === 'menu'
      && listElement
      && (!event.relatedTarget || !listElement.contains(event.relatedTarget as any))
    ) {
      lazyMenu.value = false;
      activeId.value = null;
    }
  } catch {
    // Ignore errors.
  }
};

const onKeydown = async (event: KeyboardEvent) => {
  const keysListeners: Dictionary<(() => void) | undefined> = lazyMenu.value ? {
    Home: onFocusFirst,
    End: onFocusLast,
    ArrowUp: onFocusPrev,
    ArrowDown: onFocusNext,
    Enter: onActivate,
    ' ': props.autocomplete ? undefined : onActivate,
  } : {
    ArrowUp: onFocusLast,
    ArrowDown: onFocusFirst,
    Enter: onFocusFirst,
    ' ': onFocusFirst,
  };

  if (props.mode === 'listbox' && event.key === 'Enter' && (event.ctrlKey || event.metaKey)) {
    emit('keydown', event);
    lazyMenu.value = false;
    activeId.value = null;

    return;
  }

  const keyListener = keysListeners[event.key];
  if (keyListener) {
    if (props.readonly) {
      return;
    }

    event.preventDefault();
    event.stopPropagation();
    if (!lazyMenu.value) {
      lazyMenu.value = true;
      await nextTick();
    }

    keyListener();
  }

  if (!props.readonly && props.search === '' && event.key === 'Backspace') {
    onRemoveLast();
  }

  if (event.key === 'Tab' || event.key === 'Escape') {
    lazyMenu.value = false;
    activeId.value = null;
  } else if (
    event.key !== undefined
    && event.key.length === 1
    && event.key !== ' '
    && !event.altKey
    && !event.ctrlKey
    && !event.metaKey
    && !props.readonly
  ) {
    lazyMenu.value = true;
    onFocusStartingWith(event.key);
  }
};

const onClick = async (event: Event) => {
  if (props.readonly) {
    return;
  }

  event.preventDefault();
  event.stopPropagation();
  await nextTick();
  lazyMenu.value = !lazyMenu.value;
  activeId.value = null;
};

const activatorProps = computed(() => (props.mode === 'menu' ? {
  role: 'button',
  tabindex: '0',
  'aria-expanded': String(lazyMenu.value),
  'aria-controls': id.value,
  onClick,
  onBlur,
} : {
  role: 'combobox',
  tabindex: '0',
  'aria-expanded': String(lazyMenu.value),
  'aria-haspopup': props.mode,
  'aria-controls': id.value,
  'aria-activedescendant': activeId.value ?? '',
  'aria-multiselectable': props.mode === 'listbox' ? String(props.multiple) : undefined,
  onKeydown,
  onClick,
}));

defineExpose({
  valueItems,
  activatorEl,
  activatorProps,
});
</script>

<template>
  <v-overlay
    ref="overlayRef"
    v-model="lazyMenu"
    :attach="true"
    :scrim="false"
    :max-height="maxHeightPx"
    :disabled="readonly"
    location-strategy="connected"
    location="bottom end"
    scroll-strategy="reposition"
    transition="dialog-transition"
    open-delay="300"
    close-delay="250"
    class="app-menu__overlay"
    eager
    v-bind="$attrs"
  >
    <template #activator="{ props: defaultActivatorProps }">
      <slot
        name="activator"
        v-bind="{
          props: { ...defaultActivatorProps, ...activatorProps },
          value: valueItems,
        }"
      />
    </template>
    <v-sheet
      v-if="allDisplayedItems.length || !hideNoData"
      :style="{ 'max-height': maxHeightPx }"
      :width="width"
      class="app-menu__wrapper"
      elevation="5"
    >
      <menu-list
        :id="id"
        :mode="mode"
        :items="allDisplayedItems"
        :multiple="multiple"
        :hide-check="hideCheck"
        :style="{ 'max-height': maxHeightPx }"
        class="app-menu__content"
        @item:click="onItemClick"
        @item:blur="onBlur($event.event)"
      >
        <template
          v-if="!allDisplayedItems.length"
          #prepend
        >
          <slot name="prepend-item" />
          <slot
            v-if="!hideNoData && !allDisplayedItems.length"
            name="no-data"
          >
            <v-list-item
              role="presentation"
              :subtitle="noDataText"
            />
          </slot>
          <slot name="prepend-item" />
        </template>
        <template
          v-if="$slots.item"
          #item="slotProps"
        >
          <slot
            name="item"
            v-bind="slotProps"
          />
        </template>
        <template #append>
          <slot name="append-item" />
        </template>
      </menu-list>
    </v-sheet>
  </v-overlay>
</template>

<style
  lang="scss"
  scoped
>
  .app-menu__overlay {
    overflow: visible;

    > :deep(.v-overlay__content) {
      contain: none;
    }
  }

  .app-menu__wrapper {
    overflow-y: hidden;
    border-radius: 16px;

    > .app-menu__content {
      overflow-y: auto;
    }
  }
</style>
