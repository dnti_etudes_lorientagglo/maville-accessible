<script
  setup
  lang="ts"
>
import AppList from '@/ts/components/data/AppList.vue';
import AppListItem from '@/ts/components/data/AppListItem.vue';
import MapDirectionActions from '@/ts/components/places/direction/MapDirectionActions.vue';
import MapDirectionEndpointItem
  from '@/ts/components/places/direction/MapDirectionEndpointItem.vue';
import MapDirectionObstacleChip
  from '@/ts/components/places/direction/MapDirectionObstacleChip.vue';
import MapDirectionStepItem from '@/ts/components/places/direction/MapDirectionStepItem.vue';
import MapManeuverChip from '@/ts/components/places/direction/MapManeuverChip.vue';
import useMapCursorSwap from '@/ts/composables/places/map/mgl/useMapCursorSwap';
import { injectMapData } from '@/ts/composables/places/map/useMapPlaces';
import useIsHighContrast from '@/ts/composables/themes/useIsHighContrast';
import { useLangStore } from '@/ts/stores/lang';
import orderBy from '@/ts/utilities/arrays/orderBy';
import formatDuration from '@/ts/utilities/dates/formatDuration';
import wait from '@/ts/utilities/dates/wait';
import debounce from '@/ts/utilities/functions/debounce';
import Direction, { DirectionStep } from '@/ts/utilities/places/directions/direction';
import DirectionManeuver from '@/ts/utilities/places/directions/directionManeuver';
import DirectionTransitManeuver from '@/ts/utilities/places/directions/directionTransitManeuver';
import mergeDirectionSteps from '@/ts/utilities/places/directions/mergeDirectionSteps';
import { DirectionStepLayerSpec } from '@/ts/utilities/places/directions/types';
import formatDistance from '@/ts/utilities/places/map/formatDistance';
import { SyncPointOfInterest } from '@/ts/utilities/places/map/types';
import { mdiChevronDown, mdiChevronRight, mdiChevronUp, mdiFormatListBulleted } from '@mdi/js';
import { MapMouseEvent } from 'maplibre-gl';
import { v4 as uuidV4 } from 'uuid';
import { computed, nextTick, onBeforeUnmount, ref, watch } from 'vue';
import { useDisplay } from 'vuetify';

const emit = defineEmits<{
  (e: 'update:model-value', value: boolean): void;
}>();

const props = defineProps<{
  modelValue?: boolean;
  position: number;
  origin: SyncPointOfInterest;
  destination: SyncPointOfInterest;
  direction: Direction;
}>();

const { smAndDown } = useDisplay();
const lang = useLangStore();
const { withMap, mapRef, mapScrollViewRef } = injectMapData();
const highContrast = useIsHighContrast();

const showSteps = ref(false);
const activeStep = ref(null as number | null);
const mapLayerHovered = ref(false);
const mapIdPrefix = ref(`main-${uuidV4()}`);
const mapSources = ref([] as string[]);
const mapLayers = ref([] as string[]);

useMapCursorSwap(mapRef, mapLayerHovered, 'pointer');

const mainSteps = computed(() => props.direction.steps);
const mainManeuvers = computed(() => mergeDirectionSteps(props.direction.steps).filter(
  (m) => m instanceof DirectionManeuver,
));

const makeStepId = (step: number) => `${mapIdPrefix.value}-${step}`;

const makeOnLayerClick = (
  layers: DirectionStepLayerSpec[],
  step: number,
) => (event: MapMouseEvent) => withMap(async (map) => {
  // Do not handle the click if this is not the top layer.
  const layersIds = layers.map((l) => l.id);
  const features = map.queryRenderedFeatures(event.point);
  if (!features[0] || layersIds.indexOf(features[0].layer.id) === -1) {
    return;
  }

  if (!props.modelValue) {
    emit('update:model-value', true);

    return;
  }

  if (!showSteps.value) {
    showSteps.value = true;
    await wait(300);
  }

  activeStep.value = step;

  const scrollView = mapScrollViewRef.value;
  const element = document.getElementById(makeStepId(step));
  if (scrollView && element) {
    const scrollViewTop = scrollView.getBoundingClientRect().top;
    const scrollViewBottom = scrollView.getBoundingClientRect().bottom;
    const elementTop = element.getBoundingClientRect().top;
    const elementBottom = element.getBoundingClientRect().bottom;
    if (elementBottom > scrollViewBottom || elementTop < scrollViewTop) {
      scrollView.scrollTo({
        behavior: 'smooth',
        top: ((elementTop - scrollViewTop) + scrollView.scrollTop) - 12,
      });
    }
  }
});

const onStepActivate = async (index: number) => {
  await nextTick();
  activeStep.value = index;
};

const onStepDeactivate = async (index: number) => {
  await nextTick();
  activeStep.value = activeStep.value === index ? null : activeStep.value;
};

const onLayerMouseenter = () => {
  mapLayerHovered.value = true;
};

const onLayerMouseleave = () => {
  mapLayerHovered.value = false;
};

const onExpandClick = () => {
  emit('update:model-value', !props.modelValue);
};

const onStepsClick = () => {
  showSteps.value = !showSteps.value;
};

const computeSourceId = (step: number) => `${mapIdPrefix.value}-${step}`;

const addSourcesAndLayers = (steps: DirectionStep[]) => withMap((map) => {
  steps.forEach((step, index) => {
    const sourceId = computeSourceId(index);
    const layers = step.layers(sourceId, {});
    const onLayerClick = debounce(makeOnLayerClick(layers, index), 0);

    map.addSource(sourceId, step.source());
    mapSources.value.push(sourceId);

    layers.forEach((layer) => {
      map.addLayer(layer);
      mapLayers.value.push(layer.id);

      map.on('click', layer.id, onLayerClick);
      map.on('mouseenter', layer.id, onLayerMouseenter);
      map.on('mouseleave', layer.id, onLayerMouseleave);
    });
  });
});

const updateLayersPainting = (steps: DirectionStep[]) => withMap(async (map) => {
  const isAlternate = !props.modelValue;
  const layerOptions = {
    visible: true,
    contrasted: highContrast.value,
    disabled: isAlternate,
  };

  const layersZIndexes = steps.reduce((layers, step, index) => {
    const sourceId = computeSourceId(index);

    step.layers(sourceId, layerOptions).forEach((layer) => {
      layers.push({ layer: layer.id, zIndex: layer.layerMapZIndex });

      Object.entries(layer.layout ?? {}).forEach(
        ([key, value]) => map.setLayoutProperty(layer.id, key, value),
      );
      Object.entries(layer.paint ?? {}).forEach(
        ([key, value]) => map.setPaintProperty(layer.id, key, value),
      );
    });

    return layers;
  }, [] as { layer: string; zIndex: number }[]);

  if (!isAlternate) {
    await nextTick();
  }

  orderBy(layersZIndexes, 'zIndex').forEach(({ layer }) => {
    if (map.getLayer(layer)) {
      map.moveLayer(layer);
    }
  });
});

const removeLayersAndSources = () => withMap((map) => {
  mapLayers.value.forEach((layerId) => {
    map.removeLayer(layerId);
  });
  mapLayers.value = [];

  mapSources.value.forEach((sourceId) => {
    map.removeSource(sourceId);
  });
  mapSources.value = [];
});

watch(mainSteps, (steps) => {
  removeLayersAndSources();
  addSourcesAndLayers(steps);
  updateLayersPainting(steps);
}, { immediate: true });

watch(() => props.modelValue, () => {
  updateLayersPainting(mainSteps.value);
});

onBeforeUnmount(() => {
  removeLayersAndSources();
});
</script>

<template>
  <v-card
    class="position-static"
    color="background"
    elevation="0"
  >
    <v-btn
      :aria-expanded="String(modelValue)"
      :aria-controls="`direction-${direction.id}`"
      :append-icon="modelValue ? mdiChevronUp : mdiChevronDown"
      color="on-background"
      variant="text"
      size="large"
      class="justify-space-between"
      block
      @click="onExpandClick"
    >
      <span class="font-weight-bold">
        {{ formatDuration(direction.summary.time) }}
      </span>
      &nbsp;
      <span>
        ({{ formatDistance(direction.summary.distance, lang.locale) }})
      </span>
    </v-btn>
    <v-expand-transition>
      <div
        v-show="modelValue"
        :id="`direction-${direction.id}`"
      >
        <v-card-text class="px-2 pb-2 pt-0">
          <app-list
            v-if="mainManeuvers.length > 1 || direction.summary.obstacles"
            :items="mainManeuvers.length > 1 ? mainManeuvers : []"
            class="align-center my-2"
          >
            <template
              v-if="direction.summary.obstacles"
              #prepend
            >
              <app-list-item
                role="listitem"
                class="mr-1 mb-1"
              >
                <map-direction-obstacle-chip
                  :icon="mainManeuvers.length > 1"
                  size="small"
                />
              </app-list-item>
            </template>
            <template #item="{ item, index }">
              <map-maneuver-chip
                :maneuver="item"
                size="small"
              >
                <span v-if="item instanceof DirectionTransitManeuver">
                  {{ item.transit.shortName }}
                  ·&nbsp;
                </span>
                {{ formatDuration(item.summary.time) }}
              </map-maneuver-chip>
              <v-icon
                v-if="(index as any) < (mainManeuvers.length - 1)"
                :icon="mdiChevronRight"
                size="small"
              />
            </template>
          </app-list>
          <div class="d-flex align-center justify-space-between mt-2">
            <map-direction-actions
              :position="position"
              :origin="origin"
              :destination="destination"
              :direction="direction"
            />
            <v-btn
              :aria-expanded="String(showSteps)"
              :aria-controls="`direction-${direction.id}-steps`"
              :prepend-icon="mdiFormatListBulleted"
              :append-icon="showSteps ? mdiChevronUp : mdiChevronDown"
              :size="smAndDown ? 'large' : undefined"
              variant="flat"
              @click="onStepsClick"
            >
              {{ $t('views.places.direction.labels.steps') }}
            </v-btn>
          </div>
          <v-expand-transition>
            <div
              v-show="showSteps"
              :id="`direction-${direction.id}-steps`"
              role="list"
              class="mt-2"
            >
              <div role="listitem">
                <map-direction-endpoint-item
                  :active="activeStep !== null && activeStep === -1"
                  :direction="direction"
                  :poi="origin"
                  :date="direction.summary.departureAt"
                  color="success"
                  @activate="onStepActivate(-1)"
                  @deactivate="onStepDeactivate(-1)"
                />
              </div>
              <div
                v-for="(step, index) in direction.steps"
                :id="makeStepId(index)"
                :key="`steps-${index}`"
                role="listitem"
              >
                <map-direction-step-item
                  :active="activeStep !== null && activeStep === index"
                  :step="step"
                  @activate="onStepActivate(index)"
                  @deactivate="onStepDeactivate(index)"
                />
              </div>
              <div role="listitem">
                <map-direction-endpoint-item
                  :active="activeStep !== null && activeStep === direction.steps.length"
                  :direction="direction"
                  :poi="destination"
                  :date="direction.summary.arrivalAt"
                  color="error"
                  @activate="onStepActivate(direction.steps.length)"
                  @deactivate="onStepDeactivate(direction.steps.length)"
                />
              </div>
            </div>
          </v-expand-transition>
        </v-card-text>
      </div>
    </v-expand-transition>
  </v-card>
</template>
