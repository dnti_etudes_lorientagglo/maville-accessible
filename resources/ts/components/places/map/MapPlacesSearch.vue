<script
  lang="ts"
  setup
>
import Category from '@/ts/api/models/category';
import Place from '@/ts/api/models/place';
import sortByAccessibility from '@/ts/api/utilities/accessible/sortByAccessibility';
import VSelectInput from '@/ts/components/forms/selects/VSelectInput.vue';
import VInputClear from '@/ts/components/forms/VInputClear.vue';
import MapPlacesSearchCategories from '@/ts/components/places/map/MapPlacesSearchCategories.vue';
import useLoadingBtnProps from '@/ts/composables/common/useLoadingBtnProps';
import useAbortController from '@/ts/composables/dom/useAbortController';
import { injectMapData, MapPlacesSearchQuery } from '@/ts/composables/places/map/useMapPlaces';
import equals from '@/ts/utilities/common/equals';
import isNone from '@/ts/utilities/common/isNone';
import debounce from '@/ts/utilities/functions/debounce';
import translate from '@/ts/utilities/lang/translate';
import searchAddresses from '@/ts/utilities/places/addresses/searchAddresses';
import addressToPOI from '@/ts/utilities/places/map/addressToPOI';
import placeToPOI from '@/ts/utilities/places/map/placeToPOI';
import mapPlacesAction from '@/ts/utilities/places/mapPlacesAction';
import { AddressObject } from '@/ts/utilities/places/types';
import toTrim from '@/ts/utilities/strings/toTrim';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { mdiMagnify, mdiTagOutline } from '@mdi/js';
import { all, catchIf, ModelIdType } from '@foscia/core';
import { HttpAbortedError, abortSignal, param } from '@foscia/http';
import { filterBy, paginate, sortBy } from '@foscia/jsonapi';
import { computed, onMounted, ref, toRef, watch } from 'vue';
import { useRoute } from 'vue-router';
import { useDisplay } from 'vuetify';

type MapSearchItem = {
  value: ModelIdType;
  title: string;
  props: Dictionary;
  search: Place | Category | AddressObject | string;
};

const SEARCH_ITEM_VALUE = 'search';

const emit = defineEmits<{
  (e: 'update:search', query: MapPlacesSearchQuery): void;
}>();

const props = defineProps<{
  search?: MapPlacesSearchQuery;
  categories?: boolean;
  loading?: boolean;
}>();

const { mapTopRef, mapCenter, mapMoved, mapSearchPrependRef, onMapSearchZone } = injectMapData();

const route = useRoute();
const { xs, smAndDown } = useDisplay();
const { abortController, abortPrevious } = useAbortController();
const btnLoading = useLoadingBtnProps(toRef(props, 'loading'));

const lazyMenu = ref(false);
const lazySearch = ref('');
const lazySearchItem = ref(null as MapSearchItem | null);
const lazyValue = ref(null as MapSearchItem | null);
const lazyItems = ref([] as MapSearchItem[]);

const onSearchView = computed(() => route.params.view === 'search');
const categoriesContainer = computed(() => (
  smAndDown.value ? mapSearchPrependRef.value : mapTopRef.value
));
const trimSearch = computed(() => toTrim(lazySearch.value));
const allItems = computed(() => [
  lazySearchItem.value,
  ...lazyItems.value,
].filter((i) => i));

const placeToSearchItem = (place: Place): MapSearchItem => {
  const poi = placeToPOI(place);

  return {
    value: place.id,
    title: poi.title,
    props: { prependIcon: poi.icon, subtitle: poi.subtitle },
    search: place,
  };
};

const categoryToSearchItem = (category: Category): MapSearchItem => ({
  value: category.id,
  title: translate(category.name),
  props: { prependIcon: mdiTagOutline },
  search: category,
});

const addressToSearchItem = (address: AddressObject): MapSearchItem => {
  const poi = addressToPOI(address);

  return {
    value: poi.id,
    title: poi.title,
    props: { prependIcon: poi.icon, subtitle: poi.subtitle },
    search: address,
  };
};

const searchToSearchItem = (search: string): MapSearchItem => ({
  value: SEARCH_ITEM_VALUE,
  title: search,
  props: { prependIcon: mdiMagnify },
  search,
});

const fetchPlaces = async (search: string) => {
  const places = await mapPlacesAction()
    .use(abortSignal(abortController.value))
    .use(sortByAccessibility())
    .use(filterBy({ search }))
    .use(param('location', {
      longitude: mapCenter.value.longitude,
      latitude: mapCenter.value.latitude,
      radius: 50 * 1000,
    }))
    .use(sortBy('proximity'))
    .use(sortBy('search'))
    .use(paginate({ size: 10 }))
    .run(catchIf(all(), (e) => e instanceof HttpAbortedError));

  return places ? places.map(placeToSearchItem) : null;
};

const fetchAddresses = async (search: string) => {
  const addresses = await searchAddresses(search, {
    center: mapCenter.value,
    limit: 10,
  });

  return addresses.map(addressToSearchItem);
};

const fetchItems = async () => {
  const search = trimSearch.value;
  if (isNone(search)) {
    return;
  }

  abortPrevious();

  const [newPlaces, newAddresses] = await Promise.all([
    fetchPlaces(search),
    fetchAddresses(search),
  ]);
  if (newPlaces) {
    lazyItems.value = [...newPlaces, ...newAddresses];
  }
};

const debouncedFetchItems = debounce(fetchItems);
const itemProps: any = (item: MapSearchItem) => ({
  prependIcon: item.props.prependIcon,
  subtitle: item.props.subtitle,
});

const onSubmit = () => {
  const search = lazyValue.value?.search;
  const place = search instanceof Place ? search : undefined;
  const category = search instanceof Category ? search : undefined;

  emit('update:search', {
    search: typeof search === 'string' ? search : undefined,
    address: !place && !category && typeof search !== 'string'
      ? search as AddressObject | undefined
      : undefined,
    place,
    category,
  });
};

const onLazyValueClear = () => {
  lazyValue.value = null;
  lazySearch.value = '';
  onSubmit();
};

const onLazySearchChange = () => {
  debouncedFetchItems();
  lazySearchItem.value = searchToSearchItem(trimSearch.value);
};

const onItemActivate = async (value: MapSearchItem) => {
  lazyValue.value = value.value === SEARCH_ITEM_VALUE
    ? lazySearchItem.value
    : value;

  onSubmit();

  return true;
};

const onSearchCategory = async (category: Category) => {
  lazyItems.value = [categoryToSearchItem(category)];
  [lazyValue.value] = lazyItems.value;
  onSubmit();
};

watch(lazySearch, onLazySearchChange);

watch(() => props.search, async (nextSearch, prevSearch) => {
  if (equals(nextSearch, prevSearch)) {
    return;
  }

  let item = null as MapSearchItem | null;
  if (nextSearch?.place) {
    item = placeToSearchItem(nextSearch.place);
  } else if (nextSearch?.category) {
    item = categoryToSearchItem(nextSearch.category);
  } else if (nextSearch?.address) {
    item = addressToSearchItem(nextSearch.address);
  } else if (!isNone(nextSearch?.search)) {
    item = searchToSearchItem(toTrim(nextSearch!.search));
    lazySearchItem.value = item;
  }

  if (item !== null) {
    if (item.value !== SEARCH_ITEM_VALUE) {
      lazyItems.value = [item];
    }

    lazyValue.value = item;
  }
});

onMounted(() => {
  if (!isNone(props.search?.search)) {
    lazySearchItem.value = searchToSearchItem(toTrim(props.search!.search));
    lazyValue.value = lazySearchItem.value;
  }
});
</script>

<template>
  <v-form @submit.prevent="onSubmit">
    <v-card class="d-flex align-center map__tool">
      <v-select-input
        v-model:menu="lazyMenu"
        v-model:search="lazySearch"
        :model-value="lazyValue"
        :items="allItems"
        :item-props="itemProps"
        :menu-props="{
          width: 444,
          appendValue: true,
          onItemActivate,
        }"
        :label="$t('forms.labels.search')"
        :aria-label="$t('views.places.map.search.label')"
        class="map-search__input"
        variant="solo"
        autocomplete
        no-filter
        hide-details
        hide-no-data
        hide-check
        return-object
      >
        <template #append-inner>
          <v-input-clear
            :label="$t('forms.labels.search')"
            :active="!!lazyValue"
            @clear="onLazyValueClear"
          />
        </template>
      </v-select-input>
      <v-btn
        :title="$t('actions.search')"
        :icon="mdiMagnify"
        color="primary"
        variant="text"
        size="x-large"
        density="compact"
        class="mr-2"
        type="submit"
        v-bind="btnLoading"
      />
    </v-card>
    <teleport
      v-if="categoriesContainer"
      :to="categoriesContainer"
    >
      <v-expand-transition>
        <map-places-search-categories
          v-if="categories"
          class="mt-2 mb-n1"
          :class="{ 'overflow-x-auto map__tool': smAndDown }"
          :no-wrap="smAndDown"
          @search="onSearchCategory"
        />
      </v-expand-transition>
      <v-expand-transition>
        <div
          v-if="mapMoved && onSearchView"
          class="mt-2"
        >
          <v-btn
            :block="xs"
            color="on-surface"
            density="comfortable"
            class="map__tool"
            v-bind="btnLoading"
            @click="onMapSearchZone"
          >
            {{ $t('views.places.map.search.zone') }}
          </v-btn>
        </div>
      </v-expand-transition>
    </teleport>
  </v-form>
</template>

<style
  lang="scss"
  scoped
>
  :deep(.map-search__input .v-field) {
    border-radius: 8px !important;
    box-shadow: none !important;
  }
</style>
