<script
  lang="ts"
  setup
>
import MglMapBtn from '@/ts/components/places/map/mgl/controls/MglMapBtn.vue';
import MglMapBtnGroup from '@/ts/components/places/map/mgl/controls/MglMapBtnGroup.vue';
import MglMapMarker from '@/ts/components/places/map/mgl/MglMapMarker.vue';
import useLoadingBtnProps from '@/ts/composables/common/useLoadingBtnProps';
import useThemeVariant from '@/ts/composables/themes/useThemeVariant';
import { useSnackbarStore } from '@/ts/stores/snackbar';
import isNil from '@/ts/utilities/common/isNil';
import toGeoBounds from '@/ts/utilities/places/map/mgl/toGeoBounds';
import toMglCoordinates from '@/ts/utilities/places/map/mgl/toMglCoordinates';
import { AccurateGeoLocation, GeoBounds, MglMapLocationRef } from '@/ts/utilities/places/map/types';
import { mdiCrosshairs, mdiCrosshairsGps, mdiCrosshairsOff } from '@mdi/js';
import { FitBoundsOptions, LngLatBounds, Map } from 'maplibre-gl';
import { computed, nextTick, onBeforeUnmount, onMounted, ref, watch } from 'vue';
import { useI18n } from 'vue-i18n';

const props = defineProps<{
  map: Map;
  fitBounds: (bounds: GeoBounds, options?: FitBoundsOptions, eventData?: any) => void;
}>();

const i18n = useI18n();
const snackbar = useSnackbarStore();
const lightVariant = useThemeVariant(false);

const locationResolvers = [] as ((location: AccurateGeoLocation | null) => void)[];

const locationDisabled = ref(false);
const locationLoading = ref(false);
const locationWatchId = ref<number | null>(null);
const lastLocationCoordinates = ref<AccurateGeoLocation | null>(null);
const lastLocationAccuracySize = ref(0);
const lastLocationIsCenter = ref(false);
const lastLocationShouldCenter = ref(true);

const locationProps = useLoadingBtnProps(locationLoading);

const locationIcon = computed(() => {
  if (locationWatchId.value === null) {
    return mdiCrosshairsOff;
  }

  if (lastLocationIsCenter.value) {
    return mdiCrosshairsGps;
  }

  return mdiCrosshairs;
});
const locationLabel = computed(() => {
  if (locationWatchId.value === null) {
    return i18n.t('map.map.labels.location.show');
  }

  if (lastLocationIsCenter.value) {
    return i18n.t('map.map.labels.location.hide');
  }

  return i18n.t('map.map.labels.location.go');
});
const locationAccuracyStyle = computed(() => ({
  height: `${lastLocationAccuracySize.value}px`,
  width: `${lastLocationAccuracySize.value}px`,
}));

const resolveLocationResolvers = (location: AccurateGeoLocation | null) => {
  const resolvers = [...locationResolvers];
  locationResolvers.length = 0;
  resolvers.forEach((resolver) => resolver(location));
  lastLocationShouldCenter.value = true;
};

const updateLastLocationIsCenter = () => {
  lastLocationIsCenter.value = !isNil(lastLocationCoordinates.value)
    && props.map.getBounds().contains(toMglCoordinates(lastLocationCoordinates.value));
};

const updateLastLocationAccuracySize = () => {
  if (lastLocationCoordinates.value === null) {
    lastLocationAccuracySize.value = 0;
  } else {
    const y = props.map.getContainer().clientHeight / 2;
    const a = props.map.unproject([0, y]);
    const b = props.map.unproject([1, y]);
    const metersPerPixel = a.distanceTo(b);
    lastLocationAccuracySize.value = Math.ceil(
      (2.0 * lastLocationCoordinates.value.accuracy) / metersPerPixel,
    );
  }
};

const onLocationDisable = () => {
  const watchId = structuredClone(locationWatchId.value!);
  locationWatchId.value = null;
  lastLocationCoordinates.value = null;
  locationLoading.value = false;
  navigator.geolocation.clearWatch(watchId);

  resolveLocationResolvers(null);
};

const onLocationSuccess = async (position: GeolocationPosition) => {
  if (locationWatchId.value === null) {
    return;
  }

  lastLocationCoordinates.value = {
    latitude: position.coords.latitude,
    longitude: position.coords.longitude,
    accuracy: position.coords.accuracy,
  };
  updateLastLocationIsCenter();
  updateLastLocationAccuracySize();
  locationLoading.value = false;

  await nextTick();

  resolveLocationResolvers(lastLocationCoordinates.value);
};

const onLocationError = (error: GeolocationPositionError) => {
  onLocationDisable();
  if (error.code === GeolocationPositionError.PERMISSION_DENIED) {
    locationDisabled.value = true;
    snackbar.toast({
      message: i18n.t('map.map.toasts.location.permissionDenied'),
      icon: '$error',
    });
  } else {
    snackbar.toast({
      message: i18n.t('map.map.toasts.location.positionUnavailable'),
      icon: '$error',
    });
  }

  resolveLocationResolvers(null);
};

const onLocationEnable = () => {
  locationLoading.value = true;
  locationWatchId.value = navigator.geolocation.watchPosition(
    onLocationSuccess,
    onLocationError,
    {
      timeout: 10 * 1000,
      maximumAge: 60 * 1000,
      enableHighAccuracy: true,
    },
  );
};

const onGoToLocation = (coordinates: AccurateGeoLocation) => {
  if (!lastLocationShouldCenter.value) {
    return;
  }

  const center = toMglCoordinates(coordinates);

  props.fitBounds(toGeoBounds(LngLatBounds.fromLngLat(center, coordinates.accuracy)), {
    bearing: props.map.getBearing(),
  }, { geolocateSource: true });
};

const onToggleLocation = () => {
  if (locationWatchId.value !== null) {
    if (lastLocationIsCenter.value || !lastLocationCoordinates.value) {
      onLocationDisable();
    } else {
      onGoToLocation(lastLocationCoordinates.value);
    }
  } else {
    onLocationEnable();
    locationLoading.value = true;
  }
};

const onMapZoom = () => {
  updateLastLocationAccuracySize();
};

const onMapMove = () => {
  updateLastLocationIsCenter();
  updateLastLocationAccuracySize();
};

watch(lastLocationCoordinates, (nextCoordinates, prevCoordinates) => {
  if (nextCoordinates) {
    if (prevCoordinates === null) {
      onGoToLocation(nextCoordinates);
    }

    updateLastLocationAccuracySize();
  }
}, { deep: true });

onMounted(() => {
  props.map.on('zoom', onMapZoom);
  props.map.on('move', onMapMove);
});

onBeforeUnmount(() => {
  if (locationWatchId.value !== null) {
    onLocationDisable();
  }

  props.map.off('zoom', onMapZoom);
  props.map.off('move', onMapMove);

  locationResolvers.length = 0;
});

const canGetLocation = async () => {
  const permission = await navigator.permissions.query({ name: 'geolocation' });

  return permission.state === 'granted';
};

const getLocation = () => new Promise<AccurateGeoLocation | null>((resolve) => {
  if (locationWatchId.value !== null) {
    if (!locationLoading.value) {
      resolve(lastLocationCoordinates.value);

      return;
    }
  } else {
    lastLocationShouldCenter.value = false;
    onLocationEnable();
  }

  locationResolvers.push(resolve);
});

const getLocationRef = () => lastLocationCoordinates;

defineExpose<MglMapLocationRef>({ canGetLocation, getLocation, getLocationRef });
</script>

<template>
  <mgl-map-btn-group vertical>
    <mgl-map-btn
      :aria-label="locationLabel"
      :aria-pressed="locationWatchId !== null"
      :disabled="locationDisabled"
      :icon="locationIcon"
      v-bind="locationProps"
      @click="onToggleLocation"
    />
    <template v-if="lastLocationCoordinates !== null">
      <mgl-map-marker
        :map="map"
        :coordinates="lastLocationCoordinates"
        pitch-alignment="map"
        anchor="center"
        marker-class="mgl-map__location__no-events"
        minor
      >
        <v-sheet
          :theme="lightVariant"
          color="transparent"
        >
          <div
            class="mgl-map__location__accuracy-circle rounded-circle"
            :style="locationAccuracyStyle"
          />
        </v-sheet>
      </mgl-map-marker>
      <mgl-map-marker
        :map="map"
        :coordinates="lastLocationCoordinates"
        pitch-alignment="map"
        anchor="center"
        marker-class="mgl-map__location__no-events"
        minor
      >
        <v-sheet
          :theme="lightVariant"
          color="transparent"
        >
          <div
            :title="$t('map.map.labels.location.title')"
            class="mgl-map__location__dot rounded-circle elevation-3"
          />
        </v-sheet>
      </mgl-map-marker>
    </template>
  </mgl-map-btn-group>
</template>

<style
  lang="scss"
  scoped
>
  :global(.mgl-map__location__no-events) {
    pointer-events: none;
  }

  .mgl-map__location__accuracy-circle {
    background: rgba(var(--v-theme-primary), 0.15);
  }

  .mgl-map__location__dot {
    pointer-events: auto;
    width: 20px;
    height: 20px;
    background: rgb(var(--v-theme-primary));
    border: 2px solid rgb(var(--v-theme-surface));
  }
</style>
