<script
  lang="ts"
  setup
>
import MglMapAttribution from '@/ts/components/places/map/mgl/controls/MglMapAttribution.vue';
import MglMapControl from '@/ts/components/places/map/mgl/controls/MglMapControl.vue';
import MglMapLayerControl from '@/ts/components/places/map/mgl/controls/MglMapLayerControl.vue';
import MglMapLocationControl
  from '@/ts/components/places/map/mgl/controls/MglMapLocationControl.vue';
import MglMapObstaclesControl
  from '@/ts/components/places/map/mgl/controls/MglMapObstaclesControl.vue';
import MglMapZoomControl from '@/ts/components/places/map/mgl/controls/MglMapZoomControl.vue';
import useFocus from '@/ts/composables/common/useFocus';
import env from '@/ts/env';
import equals from '@/ts/utilities/common/equals';
import addressToGeoCoordinates from '@/ts/utilities/places/addresses/addressToGeoCoordinates';
import makeMglMapLayers from '@/ts/utilities/places/map/mgl/layers/makeMglMapLayers';
import mglMapLayer from '@/ts/utilities/places/map/mgl/layers/mglMapLayer';
import makeMglMap from '@/ts/utilities/places/map/mgl/makeMglMap';
import toGeoBounds from '@/ts/utilities/places/map/mgl/toGeoBounds';
import toGeoCoordinates from '@/ts/utilities/places/map/mgl/toGeoCoordinates';
import toMglBounds from '@/ts/utilities/places/map/mgl/toMglBounds';
import toMglCoordinates from '@/ts/utilities/places/map/mgl/toMglCoordinates';
import { GeoBounds, GeoCoordinates, MglMapLocationRef } from '@/ts/utilities/places/map/types';
import { FitBoundsOptions, FlyToOptions, Map } from 'maplibre-gl';
import 'maplibre-gl/dist/maplibre-gl.css';
import { computed, onBeforeUnmount, onMounted, ref, shallowRef, watch } from 'vue';
import { useI18n } from 'vue-i18n';

const emit = defineEmits<{
  (e: 'update:bounds', v: GeoBounds): void;
  (e: 'update:center', v: GeoCoordinates): void;
  (e: 'update:zoom', v: number): void;
}>();

const props = defineProps<{
  title?: string;
  center?: GeoCoordinates;
  zoom?: number;
  padding?: { top: number; right: number; bottom: number; left: number; };
}>();

const i18n = useI18n();
const { focused, focus, blur } = useFocus();

const mapLayers = makeMglMapLayers();
const mapLayersKeys = Object.keys(mapLayers);
const defaultMapLayer = mglMapLayer.value ?? mapLayersKeys[0];

const mapRef = ref<HTMLElement | null>(null);
const mapLocationRef = ref<MglMapLocationRef | null>();

const map = shallowRef<Map | null>(null);
const mapLoaded = ref(false);
const lazyCenter = ref(props.center ?? addressToGeoCoordinates(env.services.map.center));
const lazyZoom = ref(props.zoom ?? env.services.map.zoom);
const lazyLayer = ref(
  mapLayersKeys.indexOf(defaultMapLayer) < 0 ? mapLayersKeys[0] : defaultMapLayer,
);

const mapTitle = computed(() => props.title ?? i18n.t('map.map.title'));
const mapLayer = computed(() => mapLayers[lazyLayer.value]);

const flyTo = (coordinates: GeoCoordinates, options?: FlyToOptions) => {
  map.value?.flyTo({
    center: toMglCoordinates(coordinates),
    ...options,
  });
};

const fitBounds = (
  bounds: GeoBounds,
  options?: FitBoundsOptions,
  eventData?: any,
) => {
  map.value?.fitBounds(toMglBounds(bounds), {
    maxZoom: 15,
    ...options,
  }, eventData ?? {});
};

const canGetLocation = () => {
  if (mapLocationRef.value) {
    return mapLocationRef.value.canGetLocation();
  }

  return Promise.resolve(false);
};

const getLocation = () => {
  if (mapLocationRef.value) {
    return mapLocationRef.value.getLocation();
  }

  return Promise.resolve(null);
};

const locationRef = computed(() => (
  mapLocationRef.value ? mapLocationRef.value.getLocationRef().value : null
));

const getLocationRef = () => locationRef;

defineExpose({
  loaded: mapLoaded,
  flyTo,
  fitBounds,
  canGetLocation,
  getLocation,
  getLocationRef,
  map() {
    return map.value;
  },
});

const updateCanvas = (callback: (canvas: HTMLCanvasElement) => void) => {
  const canvas = map.value?.getCanvas?.();
  if (canvas) {
    callback(canvas);
  }
};

const updateBounds = () => {
  const bounds = map.value?.getBounds?.();
  if (bounds) {
    emit('update:bounds', toGeoBounds(bounds));
  }
};

watch([mapTitle, map], () => {
  updateCanvas((canvas) => {
    canvas.setAttribute('aria-label', mapTitle.value);
  });
});

watch(lazyLayer, () => {
  if (map.value) {
    map.value.setStyle(mapLayer.value.style);
    mglMapLayer.value = mapLayer.value.id;
  }
});

watch(() => props.center, (center) => {
  const nextCenter = center ?? addressToGeoCoordinates(env.services.map.center);
  if (map.value && !equals(lazyCenter.value, nextCenter)) {
    lazyCenter.value = nextCenter;
    map.value.setCenter(toMglCoordinates(lazyCenter.value));
  }
});

watch(() => props.zoom, (zoom) => {
  const nextZoom = zoom ?? env.services.map.zoom;
  if (map.value && !equals(lazyZoom.value, nextZoom)) {
    lazyZoom.value = nextZoom;
    map.value.setZoom(lazyZoom.value);
  }
});

watch(() => props.padding, (padding) => {
  if (map.value) {
    map.value.setPadding(padding ?? { top: 0, right: 0, bottom: 0, left: 0 });
  }
});

onMounted(async () => {
  map.value = await makeMglMap(mapRef.value!, mapLayer.value.style, {
    center: toMglCoordinates(lazyCenter.value),
    zoom: lazyZoom.value,
  });

  map.value.dragRotate.disable();
  map.value.touchZoomRotate.disableRotation();

  if (props.padding) {
    map.value.setPadding(props.padding);
  }

  updateBounds();
  updateCanvas((canvas) => {
    canvas.addEventListener('focus', focus);
    canvas.addEventListener('blur', blur);
  });

  map.value.on('load', () => {
    mapLoaded.value = true;
  });

  map.value.on('zoom', () => {
    if (map.value) {
      lazyZoom.value = map.value.getZoom();
      emit('update:zoom', lazyZoom.value);
      updateBounds();
    }
  });

  map.value.on('move', () => {
    if (map.value) {
      lazyCenter.value = toGeoCoordinates(map.value.getCenter());
      emit('update:center', lazyCenter.value);
      updateBounds();
    }
  });
});

onBeforeUnmount(() => {
  if (!map.value) {
    return;
  }

  updateCanvas((canvas) => {
    canvas.removeEventListener('focus', focus);
    canvas.removeEventListener('blur', blur);
  });

  map.value.remove();
  map.value = null;
});
</script>

<template>
  <v-sheet
    :class="{ 'focusable--focused': focused }"
    class="mgl-map focusable"
  >
    <div
      ref="mapRef"
      class="mgl-map__container"
    />
    <template v-if="map">
      <mgl-map-attribution
        :map="map"
        :layer="mapLayer"
      />
      <mgl-map-control
        :map="map"
        position="bottom-right"
      >
        <mgl-map-obstacles-control
          :map="map"
          :center="lazyCenter"
          :zoom="lazyZoom"
        />
        <mgl-map-layer-control
          v-if="mapLayersKeys.length > 1"
          v-model="lazyLayer"
          :layers="mapLayers"
        />
        <mgl-map-location-control
          ref="mapLocationRef"
          :map="map"
          :fit-bounds="fitBounds"
        />
        <mgl-map-zoom-control :map="map" />
      </mgl-map-control>
      <slot />
    </template>
  </v-sheet>
</template>

<style
  lang="scss"
  scoped
>
  .mgl-map {
    display: flex;
    border-radius: inherit;

    &:after {
      z-index: 5;
      border-width: 3px;
      border-color: rgba(var(--v-theme-on-background), 1) !important;
    }

    &.focusable--focused:after {
      opacity: 0.65;
    }

    .mgl-map__container {
      flex-grow: 1;
      border-radius: inherit;

      :deep(.maplibregl-canvas-container) {
        border-radius: inherit;
        width: 100%;
        height: 100%;

        canvas.maplibregl-canvas {
          border-radius: inherit;
          width: 100%;
          height: 100%;
          outline: none;
        }
      }

      :deep(.maplibregl-control-container) {
        border-radius: inherit;

        > [class^="maplibregl-ctrl-"] {
          border-radius: inherit;
        }

        > .maplibregl-ctrl-bottom-right {
          display: flex;
          flex-direction: column;
          align-items: flex-end;
        }
      }
    }
  }
</style>
