<script
  lang="ts"
  setup
>
import action from '@/ts/api/action';
import cachedCategories from '@/ts/api/cached/cachedCategories';
import Article from '@/ts/api/models/article';
import Category from '@/ts/api/models/category';
import Place from '@/ts/api/models/place';
import ArticleListItem from '@/ts/components/articles/ArticleListItem.vue';
import AppPagination from '@/ts/components/common/AppPagination.vue';
import VCategoryInput from '@/ts/components/forms/instances/VCategoryInput.vue';
import VInputClear from '@/ts/components/forms/VInputClear.vue';
import useDynamicDocumentSubtitle from '@/ts/composables/accessibility/useDynamicDocumentSubtitle';
import useAsync from '@/ts/composables/common/useAsync';
import useForm from '@/ts/composables/forms/useForm';
import useInput from '@/ts/composables/forms/useInput';
import useCol from '@/ts/composables/layouts/useCol';
import useAnnouncer from '@/ts/composables/pages/useAnnouncer';
import { useSnackbarStore } from '@/ts/stores/snackbar';
import isNone from '@/ts/utilities/common/isNone';
import debounce from '@/ts/utilities/functions/debounce';
import translate from '@/ts/utilities/lang/translate';
import { Optional } from '@/ts/utilities/types/optional';
import { all, include, ModelInstance, query, when } from '@foscia/core';
import { filterBy, paginate, sortBy, sortByDesc, usingDocument } from '@foscia/jsonapi';
import { mdiMagnify, mdiTagMultipleOutline } from '@mdi/js';
import { computed, onActivated, ref } from 'vue';
import { useI18n } from 'vue-i18n';
import { useRoute, useRouter } from 'vue-router';
import { useDisplay } from 'vuetify';

const props = defineProps<{
  about?: Optional<ModelInstance>;
  title: string;
}>();

type ArticleQuery = {
  page: string;
  search?: Optional<string>;
  categories?: Optional<Category[]>;
};

const { xs } = useDisplay();
const i18n = useI18n();
const route = useRoute();
const router = useRouter();
const snackbar = useSnackbarStore();
const { loading, asyncRun } = useAsync({ currently: true });
const announcer = useAnnouncer();

const normalCol = useCol();
const nNormalCol = useCol('n-normal');

const searchInput = useInput({
  name: 'search',
  value: '',
});

const categoriesInput = useInput({
  name: 'categories',
  value: [] as Category[],
});

const mounted = ref(false);
const queryValue = ref({ page: '1' } as ArticleQuery);
const data = ref({
  instances: [] as Article[],
  page: { total: 0, current: 1, last: 1 },
});

const documentSubtitle = useDynamicDocumentSubtitle({
  total: computed(() => data.value.page.total),
  page: computed(() => data.value.page),
  search: searchInput.valueRef,
  filters: computed(() => (categoriesInput.value.length ? 1 : 0)),
});
const documentTitle = computed(() => [
  documentSubtitle.value.join(' | '),
  props.title,
]);

const replaceRoute = () => router.replace({
  query: {
    ...queryValue.value,
    categories: (queryValue.value.categories ?? []).map((c) => c.id),
  },
});

const replaceQuery = (newQuery: ArticleQuery) => {
  queryValue.value = { ...queryValue.value, ...newQuery };
};

const fetchInstances = () => asyncRun(async () => {
  await replaceRoute();

  const { instances, document } = await action()
    .use(query(Article))
    .use(include(['cover', 'categories']))
    .use(sortByDesc('pinned', 'pinnedAt'))
    .use(when(queryValue.value.search, (a, s) => a.use(filterBy('search', s)).use(sortBy('search'))))
    .use(when(queryValue.value.categories, (a, c) => a.use(filterBy('category', c.map(({ id }) => id)))))
    .use(when(props.about instanceof Place, (a) => a.use(filterBy('place', [props.about!.id]))))
    .use(sortByDesc('publishedAt'))
    .use(paginate({ number: queryValue.value.page, size: 16 }))
    .run(all(usingDocument));

  data.value = {
    instances,
    page: {
      total: document.meta!.page.total,
      current: document.meta!.page.currentPage,
      last: document.meta!.page.lastPage,
    },
  };

  announcer.announce({ title: documentTitle.value, scrollTo: null, focus: null });
});

const debounceFetchInstances = debounce(fetchInstances);

const { onSubmit } = useForm([
  searchInput, categoriesInput,
] as const, {
  onChange({ allValues }) {
    loading.value = true;
    replaceQuery({ page: '1', ...allValues });
    debounceFetchInstances();
  },
  async onSubmit({ allValues }) {
    replaceQuery({ page: '1', ...allValues });
    await fetchInstances();
  },
});

const onClickCategory = (category: Category) => {
  categoriesInput.value = [category];
  snackbar.toast(i18n.t('navigation.filterOn', { name: translate(category.name) }));
};

const onClearCategories = () => {
  categoriesInput.value = [];
};

const onPage = async (newPage: number) => {
  if (loading.value) {
    return;
  }

  replaceQuery({ page: String(newPage) });
  await fetchInstances();
  announcer.announce({ title: documentTitle.value, scrollTo: 0 });
};

onActivated(async () => {
  if (!mounted.value) {
    mounted.value = true;

    if (typeof route.query.search === 'string') {
      searchInput.value = route.query.search;
      queryValue.value.search = searchInput.value;
    }

    if (Array.isArray(route.query.categories)) {
      const categories = await cachedCategories.value;

      categoriesInput.value = route.query.categories
        .map((id) => categories.find((c) => c.id === id))
        .filter((c) => !!c) as Category[];
      queryValue.value.categories = categoriesInput.value;
    }

    await fetchInstances();

    announcer.announce({ title: documentTitle.value });
  } else {
    announcer.announce({ title: documentTitle.value, scrollTo: null, focus: null });
  }
});
</script>

<template>
  <v-row>
    <v-col cols="12">
      <v-form
        :aria-label="$t('accessibility.forms.search.labels.page')"
        :aria-description="$t('accessibility.forms.search.descriptions.list')"
        role="search"
        @submit="onSubmit"
      >
        <v-row dense>
          <v-col v-bind="normalCol">
            <v-text-field
              :prepend-inner-icon="mdiMagnify"
              autocomplete="off"
              hide-details
              v-bind="searchInput.props"
            />
          </v-col>
          <v-spacer />
          <v-col v-bind="nNormalCol">
            <v-category-input
              :prepend-inner-icon="mdiTagMultipleOutline"
              multiple
              hide-details
              v-bind="categoriesInput.props"
            >
              <template #append-inner>
                <v-input-clear
                  :label="categoriesInput.props.label"
                  :active="!isNone(categoriesInput.props.modelValue)"
                  @clear="onClearCategories"
                />
              </template>
            </v-category-input>
          </v-col>
        </v-row>
      </v-form>
    </v-col>
    <v-col cols="12">
      <p
        v-if="!loading && !data.instances.length"
        class="text-large text-center"
      >
        {{ $t('articles.none') }}
      </p>
      <v-row role="list">
        <v-col
          v-for="article in data.instances"
          :key="`instances-${article.id}`"
          role="listitem"
          cols="12"
        >
          <article-list-item
            :about="about"
            :article="article"
            class="d-flex flex-column h-100"
            @click:category="onClickCategory"
          />
        </v-col>
      </v-row>
    </v-col>
    <v-col cols="12">
      <app-pagination
        :model-value="data.page.current"
        :length="data.page.last"
        :total-visible="xs ? 1 : 4"
        @update:model-value="onPage"
      />
    </v-col>
  </v-row>
</template>
