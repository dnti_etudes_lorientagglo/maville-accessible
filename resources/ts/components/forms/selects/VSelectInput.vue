<script
  lang="ts"
  setup
>
import AppMenu from '@/ts/components/common/menu/AppMenu.vue';
import useLazyValue from '@/ts/composables/common/useLazyValue';
import isNone from '@/ts/utilities/common/isNone';
import { ListItem, ListItemRaw } from '@/ts/utilities/common/menu/types';
import wait from '@/ts/utilities/dates/wait';
import selectInputText from '@/ts/utilities/dom/selectInputText';
import { ValueRetriever } from '@/ts/utilities/objects/get';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { mdiMenuDown, mdiMenuUp } from '@mdi/js';
import { ComponentPublicInstance, computed, nextTick, onMounted, onUnmounted, ref, toRef, watch } from 'vue';
import { v4 as uuidV4 } from 'uuid';

const emit = defineEmits<{
  (e: 'update:model-value', value: any): void;
  (e: 'update:search', value: string): void;
  (e: 'update:focused', value: boolean): void;
}>();

const props = defineProps<{
  modelValue?: any;
  search?: string;
  focused?: boolean;
  label?: string;
  items?: ListItemRaw[];
  itemTitle?: ValueRetriever<any>;
  itemValue?: ValueRetriever<any>;
  itemDisabled?: ValueRetriever<any>;
  itemProps?: ValueRetriever<any>;
  multiple?: boolean;
  returnObject?: boolean;
  autocomplete?: boolean;
  readonly?: boolean;
  noFilter?: boolean;
  placeholder?: string;
  noData?: string;
  hideCheck?: boolean;
  hideNoData?: boolean;
  menuProps?: Dictionary;
  onKeydown?: (event: KeyboardEvent) => void;
}>();

const readOnlyWatcherInputName = uuidV4();

const lazyValue = useLazyValue(toRef(props, 'modelValue'));
const lazySearch = useLazyValue(computed(() => props.search ?? ''));
const lazyFilterSearch = ref(lazySearch.value);
const lazyFocused = useLazyValue(computed(() => props.focused ?? false));

const activatorRef = ref(null as ComponentPublicInstance | null);
const menuRef = ref(null as { valueItems: ListItem[]; } | null);
const menuActivatorRef = ref(null as Element | null);
const readOnlyWatcherInputEl = ref(null as ComponentPublicInstance | null);

const menu = ref(false);
const internalReadonly = ref(false);
const selectionText = ref(null as string | null);

const showSelectionText = computed(() => props.autocomplete && props.multiple);
const ariaLabel = computed(() => (
  showSelectionText.value && selectionText.value !== null
    ? `${props.label} ${selectionText.value}`
    : props.label
));

const getValueItems = () => menuRef.value?.valueItems ?? [];

const getInput = () => menuActivatorRef.value?.querySelector('input');

const getReadOnlyWatcherInput = () => readOnlyWatcherInputEl.value?.$el?.querySelector?.('input');

const focusInput = () => {
  const inputEl = getInput();
  if (inputEl) {
    inputEl.focus();
  }
};

const updateSelectionText = async () => {
  await nextTick();

  const items = getValueItems();

  selectionText.value = items.length
    ? items.map(({ parsed }) => parsed.title).join(', ')
    : null;
};

const updateSearch = async () => {
  await updateSelectionText();

  await wait(10);
  if (showSelectionText.value) {
    lazySearch.value = '';
  } else {
    lazySearch.value = selectionText.value ?? '';
  }
};

const updateFilterSearch = () => {
  if (!props.noFilter && props.autocomplete) {
    lazyFilterSearch.value = lazySearch.value;
  }
};

watch(lazyValue, async (value) => {
  emit('update:model-value', value);
  await updateSearch();
  updateFilterSearch();
});

watch(lazySearch, (value) => {
  emit('update:search', value);
});

watch(lazyFocused, async (value) => {
  emit('update:focused', value);
  if (value) {
    const inputEl = getInput();
    if (inputEl && props.autocomplete && !isNone(lazySearch.value)) {
      await selectInputText(inputEl);
    }
  } else {
    await updateSearch();
    await wait(250);
    if (!lazyFocused.value) {
      updateFilterSearch();
    }
  }
});

const onLazySearchInput = (value: string) => {
  lazySearch.value = value;
  updateFilterSearch();
};

const onClick = async (event: MouseEvent) => {
  if (lazyFocused.value) {
    return;
  }

  event.preventDefault();
  event.stopPropagation();

  await nextTick();
  focusInput();
};

const onItemClick = () => {
  if (props.multiple) {
    focusInput();
  }
};

const onReadonlyChange = () => {
  const readOnlyWatcherInput = getReadOnlyWatcherInput();
  if (readOnlyWatcherInput) {
    internalReadonly.value = readOnlyWatcherInput.readOnly;
  }
};

const attributesObserver = new MutationObserver((mutations) => {
  mutations.forEach((mutation) => {
    if (mutation.type === 'attributes' && mutation.attributeName === 'readonly') {
      onReadonlyChange();
    }
  });
});

onMounted(async () => {
  await nextTick();
  menuActivatorRef.value = activatorRef.value?.$el.querySelector('.v-field');

  onReadonlyChange();

  // Manually define "role" on input, because vuetify is not passing role to input.
  const input = getInput();
  if (input) {
    input.setAttribute('role', 'combobox');
  }

  const readOnlyWatcherInput = getReadOnlyWatcherInput();
  if (readOnlyWatcherInput) {
    attributesObserver.observe(readOnlyWatcherInput, { attributes: true });
  }

  await updateSearch();
});

onUnmounted(() => {
  attributesObserver.disconnect();
});
</script>

<script lang="ts">
export default {
  inheritAttrs: false,
};
</script>

<template>
  <app-menu
    ref="menuRef"
    v-model="lazyValue"
    v-model:menu="menu"
    :activator="menuActivatorRef"
    :search="lazyFilterSearch"
    :items="items ?? []"
    :item-title="itemTitle"
    :item-value="itemValue"
    :item-disabled="itemDisabled"
    :item-props="itemProps"
    :multiple="multiple"
    :return-object="returnObject"
    :autocomplete="autocomplete"
    :readonly="readonly || internalReadonly"
    :no-data="noData"
    :hide-no-data="hideNoData"
    :hide-check="hideCheck"
    mode="listbox"
    location="bottom"
    transition="slide-y-transition"
    v-bind="{ ...menuProps, onKeydown }"
    @item:click="onItemClick"
  >
    <template #activator="{ props: activatorProps, value: valueItems }">
      <v-text-field
        ref="readOnlyWatcherInputEl"
        :name="readOnlyWatcherInputName"
        type="hidden"
        class="d-none"
      />
      <!--eslint-disable vue/attributes-order -->
      <v-text-field
        :model-value="lazySearch"
        v-model:focused="lazyFocused"
        :label="label"
        :aria-label="ariaLabel"
        :validation-value="lazyValue"
        :dirty="menu || valueItems.length > 0"
        :placeholder="lazyFocused ? placeholder : ''"
        :readonly="readonly || internalReadonly || !autocomplete"
        :aria-readonly="readonly || internalReadonly"
        :class="{ 'v-select-input--autocomplete': autocomplete }"
        class="v-select-input"
        autocomplete="off"
        v-bind="{ ...$attrs, ...activatorProps, role: undefined }"
        @click:control="onClick"
        @update:model-value="onLazySearchInput"
        ref="activatorRef"
      >
        <!--eslint-enable -->
        <template
          v-if="$slots['prepend']"
          #prepend
        >
          <slot name="prepend" />
        </template>
        <template
          v-if="$slots['prepend-inner']"
          #prepend-inner
        >
          <slot name="prepend-inner" />
        </template>
        <slot
          v-if="showSelectionText"
          name="value"
        >
          <div
            v-for="(valueItem, index) in valueItems"
            :key="`values-${valueItem.parsed.id}`"
            class="v-select-input__selection"
            aria-hidden="true"
          >
            {{
              `${valueItem.parsed.title}${(index + 1) < valueItems.length ? ',&nbsp;' : '&nbsp;'}`
            }}
          </div>
        </slot>
        <template #append-inner>
          <slot name="append-inner" />
          <v-icon :icon="menu ? mdiMenuUp : mdiMenuDown" />
        </template>
      </v-text-field>
    </template>
    <template
      v-if="$slots['prepend-item']"
      #prepend-item
    >
      <slot name="prepend-item" />
    </template>
    <template
      v-if="$slots.item"
      #item="slotProps"
    >
      <slot
        name="item"
        v-bind="slotProps"
      />
    </template>
    <template
      v-if="$slots['append-item']"
      #append-item
    >
      <slot name="append-item" />
    </template>
  </app-menu>
</template>

<style
  lang="scss"
  scoped
>
  .v-select-input {
    :deep(input) {
      display: inline-flex;
    }

    &__selection {
      display: inline-flex;
      align-items: center;
      margin-top: var(--v-input-chips-margin-top);
      margin-bottom: var(--v-input-chips-margin-bottom);
    }

    :deep(.v-field), :deep(input) {
      cursor: pointer;
    }

    &--autocomplete {
      :deep(.v-field), :deep(input) {
        cursor: text;
      }

      :deep(.v-field.v-field--focused input) {
        min-width: 20%;
      }
    }
  }
</style>
