<script
  lang="ts"
  setup
>
import AppDivider from '@/ts/components/common/AppDivider.vue';
import VOpeningHoursInputDialog from '@/ts/components/forms/openingHours/VOpeningHoursInputDialog.vue';
import VWeekdayOpeningHoursInput from '@/ts/components/forms/openingHours/VWeekdayOpeningHoursInput.vue';
import AppDialog from '@/ts/components/overlays/AppDialog.vue';
import useHoursProps from '@/ts/composables/common/hours/useHoursProps';
import useOpeningHourFormatter from '@/ts/composables/dates/hours/useOpeningHourFormatter';
import { useLangStore } from '@/ts/stores/lang';
import last from '@/ts/utilities/arrays/last';
import formatWeekday from '@/ts/utilities/dates/formatWeekday';
import mergeOpeningHours from '@/ts/utilities/dates/hours/mergeOpeningHours';
import isoWeekdays from '@/ts/utilities/dates/isoWeekdays';
import { IsoWeekday, WeekdayOpeningHour } from '@/ts/utilities/dates/types';
import toUpperFirst from '@/ts/utilities/strings/toUpperFirst';
import { Arrayable } from '@/ts/utilities/types/arrayable';
import { Rule, VuetifyRule } from '@/ts/validation/rule';
import { mdiPencilOutline } from '@mdi/js';
import { computed, ref, watch } from 'vue';
import { useI18n } from 'vue-i18n';

const emit = defineEmits<{
  (e: 'update:model-value', value?: WeekdayOpeningHour[]): void;
}>();

const props = defineProps<{
  modelValue?: WeekdayOpeningHour[];
  name?: string;
  required?: boolean;
  label: string;
  density?: 'compact' | 'comfortable' | 'default';
  rules?: Arrayable<Rule | VuetifyRule>;
  errorMessages?: string[];
  open24Label?: string;
}>();

const i18n = useI18n();
const lang = useLangStore();
const formatOpeningHour = useOpeningHourFormatter();

const lazyValue = ref([...(props.modelValue ?? [])]);
const dialog = ref(false);

const weekdaysIndexes = isoWeekdays();
const mondayToFridayIndexes = weekdaysIndexes.slice(0, 5);
const weekendIndexes = weekdaysIndexes.slice(-2);
const defaultHoursForWeekdays = (weekdays: IsoWeekday[]) => {
  const hoursByWeekdays = weekdays.map(
    (weekday) => lazyValue.value.filter((h) => h.weekday === weekday),
  );
  const diff = Object.values(hoursByWeekdays).some((hours) => Object.values(hoursByWeekdays).some(
    (otherHours) => hours.length !== otherHours.length || hours.some(
      (h, i) => h.start !== otherHours[i].start || h.end !== otherHours[i].end,
    ),
  ));

  return diff ? [{ start: '', end: '' }] : hoursByWeekdays[0];
};
const bulkEditsDialogs = computed(() => [
  {
    title: i18n.t('forms.openingHours.labels.editAll'),
    weekdays: weekdaysIndexes,
    hours: defaultHoursForWeekdays(weekdaysIndexes),
  },
  {
    title: i18n.t('forms.openingHours.labels.editFromTo', {
      start: formatWeekday(mondayToFridayIndexes[0], lang.locale, 'short'),
      end: formatWeekday(last(mondayToFridayIndexes), lang.locale, 'short'),
    }),
    weekdays: mondayToFridayIndexes,
    hours: defaultHoursForWeekdays(mondayToFridayIndexes),
  },
  {
    title: i18n.t('forms.openingHours.labels.editFromTo', {
      start: formatWeekday(weekendIndexes[0], lang.locale, 'short'),
      end: formatWeekday(last(weekendIndexes), lang.locale, 'short'),
    }),
    weekdays: weekendIndexes,
    hours: defaultHoursForWeekdays(weekendIndexes),
  },
]);

const textValue = computed(() => weekdaysIndexes.map((weekday) => {
  const hours = (props.modelValue ?? []).filter((h) => weekday === h.weekday);
  const { closed, open24Hours } = useHoursProps(hours);
  const withWeekday = (hoursLabel: string) => i18n.t('forms.openingHours.labels.scoped.hours', {
    weekday: toUpperFirst(formatWeekday(weekday, lang.locale)),
    hours: hoursLabel,
  });
  if (closed.value) {
    return withWeekday(i18n.t('common.openingHours.labels.closed'));
  }

  if (open24Hours.value) {
    return withWeekday(props.open24Label ?? i18n.t('common.openingHours.labels.open24Hours'));
  }

  return withWeekday(hours.map((h) => formatOpeningHour(h)).join(', '));
}).join('\n'));

watch(() => props.modelValue, (value) => {
  lazyValue.value = [...(value ?? [])];
});

watch(dialog, (value) => {
  if (!value) {
    lazyValue.value = [...(props.modelValue ?? [])];
  }
});

const onNewHours = (newHours: { weekdays: IsoWeekday[]; hours: WeekdayOpeningHour[]; }) => {
  lazyValue.value = mergeOpeningHours(newHours.weekdays, lazyValue.value, newHours.hours);
};

const onCancel = () => {
  dialog.value = false;
};

const onConfirm = () => {
  dialog.value = false;
  emit('update:model-value', [...lazyValue.value]);
};
</script>

<template>
  <v-textarea
    :model-value="textValue"
    :name="name"
    :label="label"
    :required="required"
    :density="density"
    readonly
    auto-grow
    v-bind="$attrs"
  >
    <template #append-inner>
      <app-dialog
        v-model="dialog"
        :title="label"
        size="small"
      >
        <template #activator="{ props: activatorProps }">
          <v-btn
            :density="density === 'compact' ? 'comfortable' : undefined"
            :title="$t('actions.scoped.edit', { name: label })"
            :icon="mdiPencilOutline"
            color="primary"
            variant="text"
            v-bind="activatorProps"
          />
        </template>
        <app-divider />
        <v-card-text>
          <v-opening-hours-input-dialog
            v-for="({ title, weekdays, hours }, index) in bulkEditsDialogs"
            :key="index"
            :weekdays="weekdays"
            :hours="hours"
            @new-hours="onNewHours"
          >
            <template #activator="{ props: activatorProps }">
              <v-btn
                variant="tonal"
                class="mb-2"
                block
                v-bind="activatorProps"
              >
                {{ title }}
              </v-btn>
            </template>
          </v-opening-hours-input-dialog>
          <v-weekday-opening-hours-input
            v-for="weekday in weekdaysIndexes"
            :key="weekday"
            v-model="lazyValue"
            :weekday="weekday"
            class="px-0"
          />
        </v-card-text>
        <app-divider />
        <v-card-actions class="justify-end">
          <v-btn
            variant="text"
            @click="onCancel"
          >
            {{ $t('actions.cancel') }}
          </v-btn>
          <v-btn
            variant="elevated"
            color="primary"
            @click="onConfirm"
          >
            {{ $t('actions.confirm') }}
          </v-btn>
        </v-card-actions>
      </app-dialog>
    </template>
  </v-textarea>
</template>
