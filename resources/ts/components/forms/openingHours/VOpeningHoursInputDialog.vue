<script
  lang="ts"
  setup
>
import AppDivider from '@/ts/components/common/AppDivider.vue';
import AppDialog from '@/ts/components/overlays/AppDialog.vue';
import useHoursProps from '@/ts/composables/common/hours/useHoursProps';
import useForm from '@/ts/composables/forms/useForm';
import useInput from '@/ts/composables/forms/useInput';
import { useLangStore } from '@/ts/stores/lang';
import formatWeekday from '@/ts/utilities/dates/formatWeekday';
import isoWeekdays from '@/ts/utilities/dates/isoWeekdays';
import { IsoWeekday, OpeningHour, WeekdayOpeningHour } from '@/ts/utilities/dates/types';
import toUpperFirst from '@/ts/utilities/strings/toUpperFirst';
import required from '@/ts/validation/rules/required';
import toVuetifyRule from '@/ts/validation/toVuetifyRule';
import { mdiPlus, mdiTrashCanOutline } from '@mdi/js';
import { computed, nextTick, ref, toRef, watch } from 'vue';
import { useI18n } from 'vue-i18n';
import { useDisplay } from 'vuetify';

const emit = defineEmits<{
  (e: 'new-hours', value: { weekdays: IsoWeekday[]; hours: WeekdayOpeningHour[]; }): void;
}>();

const props = defineProps<{
  hours: OpeningHour[];
  weekdays?: IsoWeekday[];
  open24Label?: string;
  noClose?: boolean;
}>();

const isoWeekdaysIndexes = isoWeekdays();

const { xs } = useDisplay();
const i18n = useI18n();
const lang = useLangStore();
const { closed, open24Hours } = useHoursProps(toRef(props, 'hours'));

const dialog = ref(false);

const weekdaysInput = useInput({
  name: 'weekdays',
  label: i18n.t('forms.openingHours.labels.applyToWeekdays'),
  value: [] as IsoWeekday[],
});

const open24HoursInput = useInput({
  name: 'open24Hours',
  label: props.open24Label ?? i18n.t('common.openingHours.labels.open24Hours'),
  value: false,
});

const closedInput = useInput({
  name: 'closed',
  label: i18n.t('common.openingHours.labels.closed'),
  value: true,
});

const hoursInput = useInput({
  name: 'hours',
  value: [] as OpeningHour[],
  rules: [required],
});

const openNot24Hours = computed(() => !closedInput.value && !open24HoursInput.value);
const openingHourInputLabel = (index: number) => i18n.t(
  'forms.openingHours.labels.scoped.openingHour',
  { index: index + 1 },
);
const closingHourInputLabel = (index: number) => i18n.t(
  'forms.openingHours.labels.scoped.closingHour',
  { index: index + 1 },
);
const hourInputRules = (label: string) => [
  toVuetifyRule(label, required),
];

watch(open24HoursInput.valueRef, (value) => {
  if (value) {
    closedInput.value = !value;
    hoursInput.value = [{ start: '00:00', end: '00:00' }];
  }
});

watch(closedInput.valueRef, (value) => {
  if (value) {
    open24HoursInput.value = !value;
    hoursInput.value = [];
  }
});

watch(hoursInput.valueRef, (value) => {
  if (value.length === 0) {
    closedInput.value = true;
  }
}, { deep: true });

watch(openNot24Hours, (value) => {
  if (value) {
    hoursInput.value = props.hours.length
      ? [...props.hours].map((h) => ({ ...h }))
      : [{ start: '', end: '' }];
  }
});

const { loading, btnLoading, fill, onSubmit } = useForm([
  weekdaysInput, open24HoursInput, closedInput, hoursInput,
] as const, {
  async onSubmit({ allValues }) {
    const weekdaysForHours = props.weekdays ? allValues.weekdays : [0];

    emit('new-hours', {
      weekdays: allValues.weekdays,
      hours: allValues.closed ? [] : weekdaysForHours.map((weekday) => allValues.hours.map(
        ({ start, end }) => ({ weekday, start, end } as WeekdayOpeningHour),
      )).flat(),
    });

    dialog.value = false;
  },
});

watch(dialog, async (value) => {
  await nextTick();

  if (value) {
    fill({
      weekdays: [...(props.weekdays ?? [])],
      hours: [...props.hours].map((h) => ({ ...h })),
      open24Hours: open24Hours.value,
      closed: closed.value,
    });
  } else {
    fill({ weekdays: [], open24Hours: false, closed: true });
  }
});

const onAddHour = () => {
  hoursInput.value.push({
    start: '',
    end: '',
  });
};

const onRemoveHour = (index: number) => {
  hoursInput.value.splice(index, 1);
};

const onClose = () => {
  dialog.value = false;
};
</script>

<template>
  <app-dialog
    v-model="dialog"
    :title="$t('forms.openingHours.labels.selectHours')"
    :persistent="loading"
    :form-props="{ readonly: loading, onSubmit }"
  >
    <template #activator="activatorProps">
      <slot
        name="activator"
        v-bind="activatorProps"
      />
    </template>
    <app-divider />
    <v-card-text>
      <v-row dense>
        <v-col
          v-if="weekdays"
          cols="12"
        >
          <v-btn-toggle
            :model-value="weekdaysInput.props.modelValue"
            :aria-label="weekdaysInput.props.label"
            :density="xs ? 'comfortable' : undefined"
            class="justify-space-between w-100"
            role="group"
            color="primary"
            variant="text"
            mandatory
            multiple
            @update:model-value="weekdaysInput.props['onUpdate:modelValue']"
          >
            <v-btn
              v-for="weekday in isoWeekdaysIndexes"
              :key="weekday"
              :title="toUpperFirst(formatWeekday(weekday, lang.locale))"
              :aria-pressed="String(weekdaysInput.value.indexOf(weekday) !== -1)"
              :value="weekday"
              class="v-btn--toggle"
              rounded="circle"
              icon
            >
              <span class="text-uppercase">
                {{ formatWeekday(weekday, lang.locale).charAt(0) }}
              </span>
            </v-btn>
          </v-btn-toggle>
        </v-col>
        <v-col cols="12">
          <div class="d-flex flex-wrap align-center">
            <v-checkbox
              class="flex-grow-0 mr-4"
              hide-details
              v-bind="open24HoursInput.props"
            />
            <v-checkbox
              v-if="!noClose"
              class="flex-grow-0 mr-4"
              hide-details
              v-bind="closedInput.props"
            />
          </div>
        </v-col>
        <v-expand-transition>
          <v-col
            v-if="openNot24Hours"
            cols="12"
          >
            <div
              v-for="(hour, i) in hoursInput.value"
              :key="i"
              class="d-flex align-start"
            >
              <div class="d-flex flex-wrap flex-sm-nowrap flex-grow-1 align-start">
                <v-text-field
                  v-model="hour.start"
                  :label="$t('forms.openingHours.labels.openingHour')"
                  :aria-label="openingHourInputLabel(i)"
                  :rules="hourInputRules(openingHourInputLabel(i))"
                  :style="{ flex: xs ? undefined : 1 }"
                  class="mr-4"
                  type="time"
                  required
                />
                <v-text-field
                  v-model="hour.end"
                  :label="$t('forms.openingHours.labels.closingHour')"
                  :aria-label="closingHourInputLabel(i)"
                  :rules="hourInputRules(closingHourInputLabel(i))"
                  :style="{ flex: xs ? undefined : 1 }"
                  class="mr-4"
                  type="time"
                  required
                />
              </div>
              <v-btn
                :title="$t('forms.openingHours.labels.scoped.deleteHour', { index: i + 1 })"
                :icon="mdiTrashCanOutline"
                variant="text"
                @click="onRemoveHour(i)"
              />
            </div>
            <v-btn
              :prepend-icon="mdiPlus"
              variant="text"
              @click="onAddHour"
            >
              {{ $t('forms.openingHours.labels.addHour') }}
            </v-btn>
          </v-col>
        </v-expand-transition>
      </v-row>
    </v-card-text>
    <app-divider />
    <v-card-actions class="justify-end">
      <v-btn
        variant="text"
        @click="onClose"
      >
        {{ $t('actions.cancel') }}
      </v-btn>
      <v-btn
        type="submit"
        variant="elevated"
        color="primary"
        v-bind="btnLoading"
      >
        {{ $t('actions.apply') }}
      </v-btn>
    </v-card-actions>
  </app-dialog>
</template>
