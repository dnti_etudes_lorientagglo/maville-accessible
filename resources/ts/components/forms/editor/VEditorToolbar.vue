<script
  lang="ts"
  setup
>
import Media from '@/ts/api/models/media';
import VEditorCommandBold from '@/ts/components/forms/editor/commands/VEditorCommandBold.vue';
import VEditorCommandBulletList from '@/ts/components/forms/editor/commands/VEditorCommandBulletList.vue';
import VEditorCommandExpand from '@/ts/components/forms/editor/commands/VEditorCommandExpand.vue';
import VEditorCommandHeading from '@/ts/components/forms/editor/commands/VEditorCommandHeading.vue';
import VEditorCommandImage from '@/ts/components/forms/editor/commands/VEditorCommandImage.vue';
import VEditorCommandItalic from '@/ts/components/forms/editor/commands/VEditorCommandItalic.vue';
import VEditorCommandLink from '@/ts/components/forms/editor/commands/VEditorCommandLink.vue';
import VEditorCommandOrderedList from '@/ts/components/forms/editor/commands/VEditorCommandOrderedList.vue';
import VEditorCommandRedo from '@/ts/components/forms/editor/commands/VEditorCommandRedo.vue';
import VEditorCommandUndo from '@/ts/components/forms/editor/commands/VEditorCommandUndo.vue';
import VEditorCommandUnlink from '@/ts/components/forms/editor/commands/VEditorCommandUnlink.vue';
import allFocusableElements from '@/ts/utilities/dom/allFocusableElements';
import focusElement from '@/ts/utilities/dom/focusElement';
import intersectTarget from '@/ts/utilities/dom/intersectTarget';
import { Editor } from '@tiptap/vue-3';
import { ComponentPublicInstance, computed, nextTick, onBeforeUnmount, onMounted, ref, watch } from 'vue';
import { useDisplay } from 'vuetify';

type IndexOperation = number | 'current' | 'first' | 'last';

const emit = defineEmits<{
  (e: 'use:media', media: Media): void;
}>();

const props = defineProps<{
  id?: string;
  label: string;
  editor: Editor;
  noImage?: boolean;
  noList?: boolean;
  noLink?: boolean;
  noHeading?: boolean;
}>();

const { mdAndUp } = useDisplay();

const toolbarRef = ref<ComponentPublicInstance | null>(null);
const toolbarContentRef = ref<HTMLElement | null>(null);
const toolbarExpanded = ref(mdAndUp.value);
const toolbarStuck = ref(false);
const toolbarStuckObserver = ref(null as IntersectionObserver | null);

const attribute = computed(() => `"${props.label}"`);

const computeCommands = () => Array.from(
  allFocusableElements(toolbarContentRef.value!),
) as HTMLButtonElement[];

const computeIndex = (
  commands: HTMLElement[],
  currentIndex: number,
  indexOperation: IndexOperation,
) => {
  if (typeof indexOperation === 'string') {
    if (indexOperation === 'current') {
      const newIndex = commands.indexOf(document.activeElement as HTMLButtonElement);

      return newIndex !== -1 ? newIndex : currentIndex;
    }

    if (indexOperation === 'first') {
      return 0;
    }

    return commands.length - 1;
  }

  const newIndex = currentIndex + indexOperation;

  if (newIndex < 0) {
    return commands.length - 1;
  }

  if (newIndex >= commands.length) {
    return 0;
  }

  return newIndex;
};

const computeIndexIn = (commands: HTMLButtonElement[]) => {
  if (document.activeElement) {
    const index = commands.indexOf(document.activeElement as HTMLButtonElement);

    return index === -1 ? 0 : index;
  }
  return 0;
};

const resetTabindex = (commands: HTMLButtonElement[]) => {
  commands.forEach((command) => {
    command.setAttribute('tabindex', '-1');
  });
};

const changeAndFocus = (indexOperation: IndexOperation, force = false) => {
  if (toolbarExpanded.value || force) {
    const commands = computeCommands();
    const nonDisabledCommands = commands.filter((c) => !c.disabled);
    const currentIndex = computeIndexIn(nonDisabledCommands);
    const focusIndex = computeIndex(nonDisabledCommands, currentIndex, indexOperation);

    resetTabindex(commands);
    nonDisabledCommands[focusIndex].setAttribute('tabindex', '0');
    focusElement(nonDisabledCommands[focusIndex]);
  }
};

const onFocusCurrent = () => {
  changeAndFocus('current');
};

const onFocusFirst = () => {
  changeAndFocus('first');
};

const onFocusLast = () => {
  changeAndFocus('last');
};

const onFocusPrev = () => {
  changeAndFocus(-1);
};

const onFocusNext = () => {
  changeAndFocus(+1);
};

const onTabindexReset = async () => {
  await nextTick();
  const commands = computeCommands();
  resetTabindex(commands);
  commands[0].setAttribute('tabindex', '0');
};

const commandListeners = {
  'onFocus:current': onFocusCurrent,
  'onFocus:first': onFocusFirst,
  'onFocus:last': onFocusLast,
  'onFocus:prev': onFocusPrev,
  'onFocus:next': onFocusNext,
  onTabindexReset,
};

watch(toolbarExpanded, onTabindexReset);

onMounted(() => {
  onTabindexReset();

  const toolbarEl = toolbarRef.value?.$el;
  if (toolbarEl) {
    toolbarStuckObserver.value = new IntersectionObserver(([e]) => {
      toolbarStuck.value = e.intersectionRatio < 1;
    }, { threshold: [1], root: intersectTarget(toolbarEl) });

    toolbarStuckObserver.value.observe(toolbarEl);
  }
});

onBeforeUnmount(() => {
  if (toolbarStuckObserver.value) {
    toolbarStuckObserver.value.disconnect();
  }
});

const onUseMedia = (media: Media) => {
  emit('use:media', media);
};
</script>

<template>
  <v-toolbar
    ref="toolbarRef"
    :aria-label="$t('forms.editor.toolbar.label', { attribute })"
    :for="id"
    :class="{ 'editor__toolbar--stuck': toolbarStuck }"
    tag="div"
    role="toolbar"
    density="compact"
    color="surface"
    class="editor__toolbar border-b"
    height="auto"
  >
    <div class="editor__toolbar__underlay position-absolute position--fill" />
    <div
      ref="toolbarContentRef"
      class="editor__toolbar__content d-flex align-center flex-wrap py-1"
    >
      <v-editor-command-expand
        v-model="toolbarExpanded"
        :aria-expanded="String(toolbarExpanded)"
        :aria-controls="`${id}-toolbar-actions`"
        v-bind="commandListeners"
      />
      <v-expand-transition>
        <div
          v-show="toolbarExpanded"
          :id="`${id}-toolbar-actions`"
          class="flex-wrap align-center"
          style="display: inline-flex;"
        >
          <v-editor-command-undo
            :editor="editor"
            v-bind="commandListeners"
          />
          <v-editor-command-redo
            :editor="editor"
            v-bind="commandListeners"
          />
          <v-editor-command-bold
            :editor="editor"
            v-bind="commandListeners"
          />
          <v-editor-command-italic
            :editor="editor"
            v-bind="commandListeners"
          />
          <template v-if="!noHeading">
            <v-editor-command-heading
              v-for="(level) in [1, 2, 3]"
              :key="level"
              :editor="editor"
              :level="level"
              v-bind="commandListeners"
            />
          </template>
          <v-editor-command-link
            v-if="!noLink"
            :editor="editor"
            v-bind="commandListeners"
          />
          <v-editor-command-unlink
            v-if="!noLink"
            :editor="editor"
            v-bind="commandListeners"
          />
          <v-editor-command-image
            v-if="!noImage"
            :editor="editor"
            v-bind="commandListeners"
            @use:media="onUseMedia"
          />
          <v-editor-command-bullet-list
            v-if="!noList"
            :editor="editor"
            v-bind="commandListeners"
          />
          <v-editor-command-ordered-list
            v-if="!noList"
            :editor="editor"
            v-bind="commandListeners"
          />
        </div>
      </v-expand-transition>
    </div>
  </v-toolbar>
</template>

<style
  lang="scss"
  scoped
>
  .v-dialog .v-card-text .editor__toolbar {
    --editor-toolbar-top: -17px;
  }

  .editor__toolbar {
    --editor-toolbar-top: -1px;

    position: sticky;
    top: var(--editor-toolbar-top);
    border-top-left-radius: 8px;
    border-top-right-radius: 8px;
    z-index: 1;

    :deep(> .v-toolbar__content) {
      border-top-left-radius: inherit;
      border-top-right-radius: inherit;
    }

    &--stuck {
      border-top-left-radius: 0;
      border-top-right-radius: 0;
      padding-top: var(--v-layout-top, 0);
    }

    &__underlay {
      border-top-left-radius: inherit;
      border-top-right-radius: inherit;
      background-color: currentColor;
      opacity: 0.04;
    }

    &__content {
      border-radius: inherit;
      padding-left: 12px;
      padding-right: 12px;

      :deep(.editor__command) {
        margin-left: 2px;
        margin-right: 2px;
      }
    }
  }
</style>
