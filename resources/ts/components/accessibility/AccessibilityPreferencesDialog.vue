<script
  lang="ts"
  setup
>
import AccessibilityCategory from '@/ts/api/enums/accessibilityCategory';
import AppDivider from '@/ts/components/common/AppDivider.vue';
import VSelectInput from '@/ts/components/forms/selects/VSelectInput.vue';
import AppDialog from '@/ts/components/overlays/AppDialog.vue';
import useReducedColor from '@/ts/composables/accessibility/useReducedColor';
import useForm from '@/ts/composables/forms/useForm';
import useInput from '@/ts/composables/forms/useInput';
import useLocales from '@/ts/composables/lang/useLocales';
import { THEME_BLACK } from '@/ts/plugins/vuetify/themes/black';
import { THEME_BLACK_HIGH_CONTRAST } from '@/ts/plugins/vuetify/themes/blackHighContrast';
import { THEME_DARK } from '@/ts/plugins/vuetify/themes/dark';
import { THEME_DARK_HIGH_CONTRAST } from '@/ts/plugins/vuetify/themes/darkHighContrast';
import { THEME_LIGHT } from '@/ts/plugins/vuetify/themes/light';
import { THEME_LIGHT_HIGH_CONTRAST } from '@/ts/plugins/vuetify/themes/lightHighContrast';
import { useAccessibilityStore } from '@/ts/stores/accessibility';
import { useLangStore } from '@/ts/stores/lang';
import { DEFAULT_ACCESSIBILITY_PREFERENCES } from '@/ts/utilities/accessibility/accessibilityPreferences';
import foregroundColor from '@/ts/utilities/colors/foregroundColor';
import FeatureName from '@/ts/utilities/features/featureName';
import formatNumber from '@/ts/utilities/math/formatNumber';
import withUnit from '@/ts/utilities/math/withUnit';
import toUpperFirst from '@/ts/utilities/strings/toUpperFirst';
import { mdiPaletteSwatchOutline } from '@mdi/js';
import { computed, ref, watch } from 'vue';
import { useI18n } from 'vue-i18n';

const i18n = useI18n();
const lang = useLangStore();
const accessibility = useAccessibilityStore();
const locales = useLocales();

const dialog = ref(false);
const tab = ref(null as string | null);

const makeItem = (value: unknown, title: string, isDefault = false) => ({
  value,
  title: `${title}${isDefault ? ` (${i18n.t('accessibility.preferences.default')})` : ''}`,
});

const localeItems = Object.entries(locales).map(([locale, { longName }]) => ({
  value: locale, title: longName,
}));

const localeInput = useInput({
  name: 'locale',
  label: i18n.t('accessibility.preferences.locale'),
  value: lang.locale,
  items: localeItems,
});

const themeInput = useInput({
  name: 'theme',
  label: i18n.t('accessibility.preferences.theme'),
  value: accessibility.preferences.theme,
  items: [
    makeItem(THEME_LIGHT, i18n.t('accessibility.preferences.themes.light'), true),
    makeItem(THEME_DARK, i18n.t('accessibility.preferences.themes.dark')),
    makeItem(THEME_BLACK, i18n.t('accessibility.preferences.themes.black')),
    makeItem(THEME_LIGHT_HIGH_CONTRAST, i18n.t('accessibility.preferences.themes.lightHighContrast')),
    makeItem(THEME_DARK_HIGH_CONTRAST, i18n.t('accessibility.preferences.themes.darkHighContrast')),
    makeItem(THEME_BLACK_HIGH_CONTRAST, i18n.t('accessibility.preferences.themes.blackHighContrast')),
  ],
});

const snackbarTimeoutInput = useInput({
  name: 'snackbarTimeout',
  label: i18n.t('accessibility.preferences.snackbarTimeout'),
  value: accessibility.preferences.snackbarTimeout,
  items: [-1, 3, 5, 7, 10].map((value) => makeItem(
    value === -1
      ? -1
      : value * 1000,
    value === -1
      ? i18n.t('accessibility.preferences.noSnackbarTimeout')
      : withUnit(value, 'second'),
    value === 5,
  )),
});

const reducedColorInput = useInput({
  name: 'reducedColor',
  label: i18n.t('accessibility.preferences.reducedColor'),
  value: accessibility.preferences.reducedColor,
});

const fontSizeInput = useInput({
  name: 'fontSize',
  label: i18n.t('accessibility.preferences.fontSize'),
  value: accessibility.preferences.fontSize,
  items: [12, 16, 20, 24, 28, 32].map((value) => makeItem(
    value,
    withUnit(formatNumber((value / 16) * 100, lang.locale), 'percent'),
    value === 16,
  )),
});

const letterSpacingInput = useInput({
  name: 'letterSpacing',
  label: i18n.t('accessibility.preferences.letterSpacing'),
  value: accessibility.preferences.letterSpacing,
  items: [0, 0.05, 0.1, 0.15].map((value) => makeItem(
    value,
    value === 0
      ? i18n.t('accessibility.preferences.noLetterSpacing')
      : withUnit(formatNumber(value * 100, lang.locale), 'percent'),
    value === 0,
  )),
});

const lineHeightInput = useInput({
  name: 'lineHeight',
  label: i18n.t('accessibility.preferences.lineHeight'),
  value: accessibility.preferences.lineHeight,
  items: [1.5, 2, 2.5, 3].map((value) => makeItem(
    value,
    formatNumber(value, lang.locale, { maximumFractionDigits: 1 }),
    value === 1.5,
  )),
});

const mediaAutoplayInput = useInput({
  name: 'mediaAutoplay',
  label: i18n.t('accessibility.preferences.mediaAutoplay.label'),
  value: accessibility.preferences.mediaAutoplay,
  items: [
    makeItem('on', i18n.t('accessibility.preferences.mediaAutoplay.items.on'), true),
    makeItem('on-muted', i18n.t('accessibility.preferences.mediaAutoplay.items.on-muted')),
    makeItem('off', i18n.t('accessibility.preferences.mediaAutoplay.items.off')),
  ],
});

const prioritizeCategoriesInput = useInput({
  name: 'prioritizeCategories',
  label: i18n.t('accessibility.preferences.prioritizeCategories.label'),
  value: accessibility.preferences.prioritizeCategories,
  items: [...AccessibilityCategory.all.values()]
    .filter((category) => !AccessibilityCategory.RESTROOMS.is(category))
    .map((category) => makeItem(
      category.value,
      toUpperFirst(category.label),
      false,
    )),
});

const { loading, fill, btnLoading, onSubmit } = useForm([
  localeInput,
  themeInput,
  snackbarTimeoutInput,
  reducedColorInput,
  fontSizeInput,
  letterSpacingInput,
  lineHeightInput,
  mediaAutoplayInput,
  prioritizeCategoriesInput,
] as const, {
  onSubmit: async ({ values }) => {
    const { locale, ...newPreferences } = values;

    accessibility.updatePreferences(newPreferences);

    if (locale && locale !== lang.locale) {
      await lang.changeLocale(locale);
    }

    dialog.value = false;
  },
});

const avatarColor = (color: string) => useReducedColor(color, {
  dark: [THEME_DARK, THEME_DARK_HIGH_CONTRAST].indexOf(themeInput.value) !== -1,
  reduced: reducedColorInput.value,
}).value;
const iconColor = (color: string) => (
  avatarColor(color).startsWith('#')
    ? foregroundColor(avatarColor(color))
    : color
);
const textPreviewStyles = computed(() => ({
  'font-size': `${fontSizeInput.value}px`,
  'line-height': lineHeightInput.value,
  'letter-spacing': letterSpacingInput.value !== 0
    ? `${letterSpacingInput.value}em`
    : undefined,
}));

watch(dialog, (opened) => {
  if (!opened) {
    fill({
      locale: lang.locale,
      ...accessibility.preferences,
    });

    tab.value = null;
  }
});

const onReset = () => {
  fill({
    locale: Object.keys(locales)[0],
    ...DEFAULT_ACCESSIBILITY_PREFERENCES,
  });

  onSubmit();
};
</script>

<template>
  <app-dialog
    v-model="dialog"
    :title="$t('accessibility.preferences.title')"
    :persistent="loading"
    :form-props="{ readonly: loading, onSubmit }"
    size="large"
  >
    <template #activator="activatorProps">
      <slot
        name="activator"
        v-bind="activatorProps"
      />
    </template>
    <app-divider />
    <v-card-text>
      <v-row>
        <v-col
          cols="12"
          md="6"
        >
          <p class="mt-0 mb-4">
            {{ $t('accessibility.preferences.description') }}
          </p>
          <div class="top-0 z-index-1">
            <v-card
              :theme="themeInput.value"
              color="background"
              role="presentation"
              aria-hidden="true"
              elevation="0"
            >
              <v-card-text class="d-flex align-center pb-0">
                <v-avatar
                  color="primary"
                  class="mr-2"
                >
                  <v-icon
                    :icon="mdiPaletteSwatchOutline"
                    :color="iconColor('on-primary')"
                  />
                </v-avatar>
                <v-avatar
                  :color="avatarColor('secondary')"
                  class="mr-2"
                >
                  <v-icon
                    :icon="mdiPaletteSwatchOutline"
                    :color="iconColor('on-secondary')"
                  />
                </v-avatar>
                <v-avatar
                  :color="avatarColor('tertiary')"
                  class="mr-2"
                >
                  <v-icon
                    :icon="mdiPaletteSwatchOutline"
                    :color="iconColor('on-tertiary')"
                  />
                </v-avatar>
              </v-card-text>
              <v-card-text class="d-flex align-center">
                <div
                  :style="textPreviewStyles"
                  class="text-truncate ml-2"
                >
                  <span class="accessibility-dialog__preview__text">
                    <span class="text-primary">
                      {{ $t('accessibility.preferences.preview.title') }}
                    </span>
                    <br>
                    {{ $t('accessibility.preferences.preview.text') }}
                  </span>
                </div>
              </v-card-text>
            </v-card>
          </div>
        </v-col>
        <v-col
          cols="12"
          md="6"
        >
          <v-tabs
            v-model="tab"
            mandatory
          >
            <v-tab value="general">
              {{ $t('accessibility.preferences.tabs.general') }}
            </v-tab>
            <v-tab value="text">
              {{ $t('accessibility.preferences.tabs.text') }}
            </v-tab>
            <v-tab value="contents">
              {{ $t('accessibility.preferences.tabs.contents') }}
            </v-tab>
          </v-tabs>
          <v-window
            v-model="tab"
            class="pt-4"
          >
            <v-window-item value="general">
              <v-row>
                <v-col
                  v-if="localeItems.length > 1"
                  cols="12"
                >
                  <v-select-input
                    hide-details
                    v-bind="localeInput.props"
                  />
                </v-col>
                <v-col cols="12">
                  <v-select-input
                    hide-details
                    v-bind="themeInput.props"
                  />
                </v-col>
                <v-col cols="12">
                  <v-select-input
                    hide-details
                    v-bind="snackbarTimeoutInput.props"
                  />
                </v-col>
                <v-col cols="12">
                  <v-switch
                    hide-details
                    v-bind="reducedColorInput.props"
                  />
                </v-col>
              </v-row>
            </v-window-item>
            <v-window-item value="text">
              <v-row>
                <v-col cols="12">
                  <v-select-input
                    hide-details
                    v-bind="fontSizeInput.props"
                  />
                </v-col>
                <v-col cols="12">
                  <v-select-input
                    hide-details
                    v-bind="letterSpacingInput.props"
                  />
                </v-col>
                <v-col cols="12">
                  <v-select-input
                    hide-details
                    v-bind="lineHeightInput.props"
                  />
                </v-col>
              </v-row>
            </v-window-item>
            <v-window-item value="contents">
              <v-row>
                <v-col
                  v-if="FeatureName.ACCESSIBILITY.check()"
                  cols="12"
                >
                  <v-select-input
                    :hint="$t('accessibility.preferences.prioritizeCategories.hint')"
                    persistent-hint
                    multiple
                    v-bind="prioritizeCategoriesInput.props"
                  />
                </v-col>
                <v-col
                  v-if="false"
                  cols="12"
                >
                  <v-select-input v-bind="mediaAutoplayInput.props" />
                </v-col>
              </v-row>
            </v-window-item>
          </v-window>
        </v-col>
      </v-row>
    </v-card-text>
    <app-divider />
    <v-card-actions class="justify-end">
      <v-btn
        variant="text"
        @click="onReset"
      >
        {{ $t('actions.reset') }}
      </v-btn>
      <v-btn
        type="submit"
        variant="elevated"
        color="primary"
        v-bind="btnLoading"
      >
        {{ $t('actions.apply') }}
      </v-btn>
    </v-card-actions>
  </app-dialog>
</template>

<style
  lang="scss"
  scoped
>
  .accessibility-dialog__preview__text {
    font-size: 1em !important;
  }
</style>
