<script
  lang="ts"
  setup
>
import action from '@/ts/api/action';
import { Reportable } from '@/ts/api/composables/reportable';
import RequestTypeCode from '@/ts/api/enums/requestTypeCode';
import Organization from '@/ts/api/models/organization';
import OrganizationType from '@/ts/api/models/organizationType';
import Report from '@/ts/api/models/report';
import ReportType from '@/ts/api/models/reportType';
import Request from '@/ts/api/models/request';
import RequestType from '@/ts/api/models/requestType';
import first from '@/ts/api/runners/first';
import fillAndSave from '@/ts/api/utilities/fillAndSave';
import BlockedAccountAlert from '@/ts/components/account/BlockedAccountAlert.vue';
import RequiresVerifiedEmailAlert from '@/ts/components/account/RequiresVerifiedEmailAlert.vue';
import AppAlert from '@/ts/components/common/AppAlert.vue';
import AppDivider from '@/ts/components/common/AppDivider.vue';
import VOrganizationInput from '@/ts/components/forms/instances/VOrganizationInput.vue';
import VOrganizationTypeInput from '@/ts/components/forms/instances/VOrganizationTypeInput.vue';
import VReportTypeInput from '@/ts/components/forms/instances/VReportTypeInput.vue';
import VCustomPhoneInput from '@/ts/components/forms/phone/VCustomPhoneInput.vue';
import RequiredInputsHint from '@/ts/components/forms/RequiredInputsHint.vue';
import VInputClear from '@/ts/components/forms/VInputClear.vue';
import AppDialog from '@/ts/components/overlays/AppDialog.vue';
import useSignInRoute from '@/ts/composables/auth/useSignInRoute';
import useLazyValue from '@/ts/composables/common/useLazyValue';
import useForm from '@/ts/composables/forms/useForm';
import useInput from '@/ts/composables/forms/useInput';
import extractResponseError from '@/ts/errors/extractResponseError';
import { useAuthStore } from '@/ts/stores/auth';
import { useSnackbarStore } from '@/ts/stores/snackbar';
import isNone from '@/ts/utilities/common/isNone';
import { Optional } from '@/ts/utilities/types/optional';
import { VuetifyRule } from '@/ts/validation/rule';
import max from '@/ts/validation/rules/max';
import required from '@/ts/validation/rules/required';
import { query } from '@foscia/core';
import { HttpConflictError } from '@foscia/http';
import { filterBy } from '@foscia/jsonapi';
import { ComponentPublicInstance, computed, ref, watch } from 'vue';
import { useI18n } from 'vue-i18n';

const emit = defineEmits<{
  (e: 'done'): void;
  (e: 'update:model-value', v: boolean): void;
}>();

const props = defineProps<{
  modelValue?: boolean;
  reportable?: Reportable;
  activator?: Optional<string | Element | ComponentPublicInstance>;
}>();

const i18n = useI18n();
const auth = useAuthStore();
const snackbar = useSnackbarStore();
const signInRoute = useSignInRoute();

const lazyValue = useLazyValue(computed(() => props.modelValue ?? false));

const error = ref(null as string | null);

const reportTypeInput = useInput({
  name: 'reportType',
  label: i18n.t('resources.reports.labels.reportType'),
  value: null as ReportType | RequestType | null,
  rules: [required],
});

const isJoinRequest = computed(() => (
  reportTypeInput.value instanceof RequestType
  && RequestTypeCode.JOIN.match(reportTypeInput.value.code)
));

const whenJoinRequestRule = <T>(rule: VuetifyRule<T>) => async (value: T) => (
  isJoinRequest.value ? rule(value) : true
);

const joinPhoneInput = useInput({
  name: 'joinPhone',
  label: i18n.t('forms.labels.phone'),
  value: '',
  rules: [required],
});

const joinExistingOrganizationInput = useInput({
  name: 'joinExistingOrganization',
  label: i18n.t('requests.join.labels.existingOrganization'),
  value: null as Organization | null,
});

const joinNewOrganizationInput = useInput({
  name: 'joinNewOrganization',
  label: i18n.t('requests.join.labels.newOrganization'),
  value: '',
  rules: [
    whenJoinRequestRule((value: string) => !isNone(value) || i18n.t('requests.join.errors.newOrganization')),
    whenJoinRequestRule(async (value: string) => {
      if (isNone(value)) {
        return true;
      }

      const matchingOrganization = await action()
        .use(query(Organization))
        .use(filterBy('searchName', value))
        .run(first());

      return !matchingOrganization || i18n.t('requests.join.errors.existingOrganization');
    }),
  ],
});

const joinNewOrganizationTypeInput = useInput({
  name: 'joinNewOrganizationType',
  label: i18n.t('requests.join.labels.newOrganizationType'),
  value: null as OrganizationType | null,
  rules: [required],
});

const onJoinExistingOrganizationClear = () => {
  joinExistingOrganizationInput.value = null;
};

const bodyInput = useInput({
  label: i18n.t('forms.labels.comment'),
  name: 'body',
  value: '',
  rules: [max(500)],
});

const { loading, btnLoading, resetDefaults, onSubmit } = useForm([
  reportTypeInput,
  bodyInput,
  joinPhoneInput,
  joinExistingOrganizationInput,
  joinNewOrganizationInput,
  joinNewOrganizationTypeInput,
] as const, {
  onValidate() {
    return auth.verified && !auth.blocked;
  },
  async onSubmit({ allValues }) {
    error.value = null;

    if (isJoinRequest.value) {
      await fillAndSave(new Request(), {
        url: window.location.href,
        body: allValues.body,
        data: {
          phone: allValues.joinPhone,
          ...(allValues.joinExistingOrganization ? {} : {
            name: allValues.joinNewOrganization,
            type: allValues.joinNewOrganizationType!.code,
          }),
        },
        requestable: allValues.joinExistingOrganization,
        requestType: allValues.reportType! as RequestType,
      });

      snackbar.toast(i18n.t('requests.common.toast'));
    } else {
      await fillAndSave(new Report(), {
        url: window.location.href,
        body: allValues.body,
        reportType: allValues.reportType! as ReportType,
        reportable: props.reportable ?? null,
      });

      snackbar.toast(i18n.t('reports.report.toast'));
    }

    lazyValue.value = false;

    emit('done');
  },
  async onFailure(formError) {
    if (formError instanceof HttpConflictError) {
      error.value = await extractResponseError(formError);
    }

    return error.value !== null;
  },
});

watch(lazyValue, (value) => {
  if (!value) {
    resetDefaults();
    error.value = null;
  }

  emit('update:model-value', value);
});

const onClose = () => {
  lazyValue.value = false;
};
</script>

<script lang="ts">
export default {
  inheritAttrs: false,
};
</script>

<template>
  <slot
    v-if="auth.guest"
    name="activator"
    :props="{ to: signInRoute }"
  />
  <app-dialog
    v-if="auth.auth"
    v-model="lazyValue"
    :activator="activator"
    :title="$t('reports.report.title')"
    :persistent="loading"
    :form-props="{ readonly: loading, onSubmit }"
    v-bind="$attrs"
  >
    <template #activator="activatorProps">
      <slot
        name="activator"
        v-bind="{ ...activatorProps, props: { ...activatorProps.props, role: 'button' } }"
      />
    </template>
    <app-divider />
    <v-card-text>
      <v-row dense>
        <v-col cols="12">
          <required-inputs-hint />
        </v-col>
        <v-expand-transition>
          <v-col
            v-if="error"
            cols="12"
          >
            <app-alert
              type="error"
              class="mb-3"
            >
              {{ error }}
            </app-alert>
          </v-col>
        </v-expand-transition>
        <v-col
          v-if="auth.blocked"
          cols="12"
        >
          <blocked-account-alert />
        </v-col>
        <v-col
          v-else-if="!auth.verified"
          cols="12"
        >
          <requires-verified-email-alert
            :message="$t('reports.report.needVerifiedEmail')"
            immediate
            @verify="onClose"
          />
        </v-col>
        <v-col cols="12">
          <v-report-type-input
            :global="!reportable"
            :disabled="!auth.verified || auth.blocked"
            v-bind="reportTypeInput.props"
          />
        </v-col>
        <v-col cols="12">
          <v-textarea
            :disabled="!auth.verified || auth.blocked"
            maxlength="500"
            rows="2"
            counter
            v-bind="bodyInput.props"
          />
        </v-col>
        <v-expand-transition>
          <v-col
            v-if="isJoinRequest"
            cols="12"
          >
            <v-row dense>
              <v-col cols="12">
                <v-custom-phone-input
                  :disabled="!auth.verified || auth.blocked"
                  autocomplete="tel"
                  v-bind="joinPhoneInput.props"
                />
              </v-col>
              <v-col cols="12">
                {{ $t('requests.join.description') }}
              </v-col>
              <v-col cols="12">
                <v-organization-input
                  :disabled="!auth.verified || auth.blocked"
                  v-bind="joinExistingOrganizationInput.props"
                >
                  <template #append-inner>
                    <v-input-clear
                      :active="!!joinExistingOrganizationInput.props.modelValue"
                      :label="joinExistingOrganizationInput.props.label"
                      @clear="onJoinExistingOrganizationClear"
                    />
                  </template>
                </v-organization-input>
              </v-col>
              <v-expand-transition>
                <v-col
                  v-if="!joinExistingOrganizationInput.value"
                  cols="12"
                >
                  <v-row dense>
                    <v-col cols="12">
                      <v-text-field
                        :disabled="!auth.verified || auth.blocked"
                        v-bind="joinNewOrganizationInput.props"
                      />
                    </v-col>
                    <v-expand-transition>
                      <v-col
                        v-if="!isNone(joinNewOrganizationInput.value)"
                        cols="12"
                      >
                        <v-organization-type-input
                          :disabled="!auth.verified || auth.blocked"
                          v-bind="joinNewOrganizationTypeInput.props"
                        />
                      </v-col>
                    </v-expand-transition>
                  </v-row>
                </v-col>
              </v-expand-transition>
            </v-row>
          </v-col>
        </v-expand-transition>
      </v-row>
    </v-card-text>
    <app-divider />
    <v-card-actions class="justify-end">
      <v-btn @click="onClose">
        {{ $t('actions.cancel') }}
      </v-btn>
      <v-btn
        :disabled="!auth.verified || auth.blocked"
        type="submit"
        variant="elevated"
        color="primary"
        v-bind="btnLoading"
      >
        {{ $t('actions.send') }}
      </v-btn>
    </v-card-actions>
  </app-dialog>
</template>
