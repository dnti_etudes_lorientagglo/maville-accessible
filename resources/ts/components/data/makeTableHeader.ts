import translate from '@/ts/utilities/lang/translate';

export type TableHeaderOptions<I = any> = {
  label?: string;
  sortable?: boolean | string;
  classes?: string;
  when?: boolean | ((item: I) => boolean);
  large?: boolean;
};

export type TableHeader<I = any> = {
  name: string;
  label: string;
  sortName?: string;
  classes?: string;
  when?: boolean | ((item: I) => boolean);
  large?: boolean;
};

export default function makeTableHeader<I = any>(
  name: string,
  options: TableHeaderOptions<I> = {},
): TableHeader<I> {
  let sortName;
  if (options.sortable) {
    sortName = typeof options.sortable === 'string' ? options.sortable : name;
  }

  return {
    name,
    label: options.label ?? translate(name),
    sortName,
    classes: options.classes,
    when: options.when,
    large: options.large,
  };
}
