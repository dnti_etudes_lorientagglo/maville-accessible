<script
  lang="ts"
  setup
>
import { AccessibilityObject } from '@/ts/api/composables/accessible';
import { Reviewable } from '@/ts/api/composables/reviewable';
import Review from '@/ts/api/models/review';
import hardDelete from '@/ts/api/utilities/hardDelete';
import softDelete from '@/ts/api/utilities/softDelete';
import softRestore from '@/ts/api/utilities/softRestore';
import AccessibilityRatingPanel
  from '@/ts/components/accessibility/new/AccessibilityRatingPanel.vue';
import AppMenu from '@/ts/components/common/menu/AppMenu.vue';
import ConfirmDialog from '@/ts/components/overlays/ConfirmDialog.vue';
import ReactBtnGroup from '@/ts/components/reactions/ReactBtnGroup.vue';
import ReportDialog from '@/ts/components/reports/ReportDialog.vue';
import ReviewDialog from '@/ts/components/reviews/ReviewDialog.vue';
import ReviewRating from '@/ts/components/reviews/ReviewRating.vue';
import ReviewReplyDialog from '@/ts/components/reviews/ReviewReplyDialog.vue';
import UserAvatar from '@/ts/components/users/UserAvatar.vue';
import useSignInRoute from '@/ts/composables/auth/useSignInRoute';
import useMenuWithDialog from '@/ts/composables/dom/useMenuWithDialog';
import { useAuthStore } from '@/ts/stores/auth';
import { useLangStore } from '@/ts/stores/lang';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';
import formatDateFromNow from '@/ts/utilities/dates/formatDateFromNow';
import FeatureName from '@/ts/utilities/features/featureName';
import fullNameWithYou from '@/ts/utilities/lang/fullNameWithYou';
import translate from '@/ts/utilities/lang/translate';
import mapValues from '@/ts/utilities/objects/mapValues';
import makeAnonymousUser from '@/ts/utilities/users/makeAnonymousUser';
import makeUserLevel from '@/ts/utilities/users/makeUserLevel';
import { isSame } from '@foscia/core';
import { mdiDotsVertical, mdiMessageReplyTextOutline } from '@mdi/js';
import { computed, Ref, ref } from 'vue';
import { useI18n } from 'vue-i18n';

const emit = defineEmits<{
  (e: 'updated', review: Review): void;
  (e: 'deleted', review: Review): void;
}>();

const props = defineProps<{
  reviewable?: Reviewable;
  review: Review;
  large?: boolean;
}>();

const i18n = useI18n();
const auth = useAuthStore();
const lang = useLangStore();
const anonymousUser = makeAnonymousUser();
const signInRoute = useSignInRoute();

const { menuRef, dialogActivatorRef, dialogActivatorProps } = useMenuWithDialog();

const lazyReportDialog = ref(false);
const lazyUpdateDialog = ref(false);
const lazyDeleteDialog = ref(false);
const lazyRestoreDialog = ref(false);

const reportTo = computed(() => (auth.auth ? undefined : signInRoute.value));
const isModerated = computed(() => props.review.deleted);
const author = computed(() => props.review.ownerUser ?? anonymousUser);
const authorLevel = computed(() => makeUserLevel(author.value));
const isAuthor = computed(() => auth.auth && isSame(auth.user, props.review.ownerUser));
const displayedAuthor = computed(() => (
  !isAuthor.value && isModerated.value ? anonymousUser : author.value
));
const accessibility = computed(() => {
  if (isModerated.value) {
    const { criteria, ...ratings } = props.review.accessibility ?? {};

    return {
      criteria, ...mapValues(ratings, ({ rating }) => ({ rating })),
    } as AccessibilityObject;
  }

  return props.review.accessibility;
});
const canReport = computed(() => !isAuthor.value);
const canEdit = computed(() => isAuthor.value && hasPermissionOn('update', props.review));
const canModerate = computed(() => auth.auth && !isAuthor.value && hasPermissionOn('delete', props.review));
const canDelete = computed(() => !isModerated.value && (canModerate.value || isAuthor.value));
const canRestore = computed(() => isModerated.value && canModerate.value);

const deleteTitle = computed(() => (
  isAuthor.value
    ? i18n.t('reviews.review.delete.title')
    : i18n.t('reviews.review.moderate.title')
));
const deleteDescription = computed(() => (
  isAuthor.value
    ? i18n.t('reviews.review.delete.description', { name: fullNameWithYou(author.value, true) })
    : i18n.t('reviews.review.moderate.description', { name: fullNameWithYou(author.value, true) })
));

const makeMenuItemProps = (dialog: Ref<boolean>) => ({
  props: {
    'aria-haspopup': 'dialog',
    role: 'button',
    onClick: () => {
      // eslint-disable-next-line no-param-reassign
      dialog.value = true;
    },
  },
});
const menuItems = computed(() => [
  canReport.value
    ? {
      title: i18n.t('reviews.review.report.title'),
      props: reportTo.value ? { to: reportTo.value } : makeMenuItemProps(lazyReportDialog).props,
    }
    : undefined,
  canEdit.value
    ? {
      title: i18n.t('reviews.review.update.title'),
      ...makeMenuItemProps(lazyUpdateDialog),
    }
    : undefined,
  canDelete.value
    ? { title: deleteTitle.value, ...makeMenuItemProps(lazyDeleteDialog) }
    : undefined,
  canRestore.value
    ? { title: i18n.t('reviews.review.restore.action'), ...makeMenuItemProps(lazyRestoreDialog) }
    : undefined,
].filter((i) => !!i));

const onUpdated = () => {
  emit('updated', props.review);
};

const onDelete = async () => {
  if (isAuthor.value) {
    await hardDelete(props.review);
    emit('deleted', props.review);
  } else {
    await softDelete(props.review);
  }
};

const onRestore = async () => {
  await softRestore(props.review);
};
</script>

<template>
  <div role="listitem">
    <v-list-item class="position-static pr-2">
      <template #prepend>
        <user-avatar
          :user="displayedAuthor"
          show-level
        />
      </template>
      <v-list-item-title>
        {{ fullNameWithYou(displayedAuthor, true) }}
      </v-list-item-title>
      <v-list-item-subtitle v-if="displayedAuthor !== anonymousUser">
        {{ authorLevel ? `${authorLevel.label} ·` : '' }}
        {{ $t('reviews.scoped.count', { n: displayedAuthor.reviewsCount ?? 0 }) }}
      </v-list-item-subtitle>
      <template
        v-if="(!isAuthor || !auth.blocked) && (canReport || canEdit || canDelete || canRestore)"
        #append
      >
        <v-list-item-action end>
          <app-menu
            ref="menuRef"
            :items="menuItems"
            mode="menu"
            location="left"
          >
            <template #activator="{ props: menuProps }">
              <v-btn
                :title="$t('reviews.review.actions.label', { name: fullNameWithYou(author, true) })"
                :icon="mdiDotsVertical"
                variant="text"
                density="compact"
                size="x-large"
                color="on-surface"
                v-bind="menuProps"
              />
            </template>
          </app-menu>
          <report-dialog
            v-model="lazyReportDialog"
            :activator="dialogActivatorRef"
            :activator-props="dialogActivatorProps"
            :open-on-click="false"
            :reportable="review"
          />
          <review-dialog
            v-model="lazyUpdateDialog"
            :activator="dialogActivatorRef"
            :activator-props="dialogActivatorProps"
            :open-on-click="false"
            :review="review"
            @done="onUpdated"
          />
          <confirm-dialog
            v-model="lazyDeleteDialog"
            :activator="dialogActivatorRef"
            :activator-props="dialogActivatorProps"
            :open-on-click="false"
            :title="deleteTitle"
            :description="deleteDescription"
            :run="onDelete"
            color="error"
          />
          <confirm-dialog
            v-model="lazyRestoreDialog"
            :activator="dialogActivatorRef"
            :activator-props="dialogActivatorProps"
            :open-on-click="false"
            :title="$t('reviews.review.restore.title')"
            :description="$t('reviews.review.restore.description', {
              name: fullNameWithYou(author, true)
            })"
            :run="onRestore"
            color="error"
          />
        </v-list-item-action>
      </template>
    </v-list-item>
    <v-list-item
      density="compact"
      class="pb-0"
    >
      <div class="d-flex flex-wrap align-center">
        <div class="text-body-2">
          <review-rating
            :rating="review.rating"
            icon
          />
        </div>
        <v-list-item-subtitle class="ml-2">
          {{ formatDateFromNow(review.createdAt, lang.locale) }}
        </v-list-item-subtitle>
      </div>
    </v-list-item>
    <accessibility-rating-panel
      :large="large"
      :values="accessibility ?? {}"
      context="review"
      density="compact"
    />
    <v-list-item v-if="isModerated">
      <div class="font-italic">
        {{ isAuthor ? $t('reviews.review.moderated.you') : $t('reviews.review.moderated.any') }}
      </div>
    </v-list-item>
    <v-list-item v-else-if="FeatureName.REVIEWS_BODY.check() && translate(review.body)">
      <p class="text-formatted my-0">
        {{ translate(review.body) }}
      </p>
    </v-list-item>
    <v-list-item class="py-0">
      <react-btn-group :reactable="review" />
      <review-reply-dialog
        v-if="reviewable"
        :reviewable="reviewable"
        :review="review"
      >
        <template #activator="{ props: activatorProps }">
          <v-btn
            :prepend-icon="mdiMessageReplyTextOutline"
            variant="text"
            class="mr-1 mb-1"
            v-bind="activatorProps"
          >
            {{ $t('reviews.reply.create.title') }}
          </v-btn>
        </template>
      </review-reply-dialog>
    </v-list-item>
    <v-expand-transition>
      <v-list-item
        v-if="review.replies.length"
        class="py-0"
      >
        <div role="list">
          <div
            v-for="reply in review.replies"
            :key="`replies-${reply.id}`"
            role="listitem"
            class="mb-1"
          >
            <v-card
              color="background"
              elevation="0"
            >
              <v-card-text class="text-body-2">
                <div>
                  <span class="font-weight-bold">
                    {{ $t('reviews.reply.title') }}
                  </span>
                  ·
                  <span>
                    {{ formatDateFromNow(reply.createdAt, lang.locale) }}
                  </span>
                </div>
                <p class="text-formatted my-0">
                  {{ reply.body }}
                </p>
              </v-card-text>
            </v-card>
          </div>
        </div>
      </v-list-item>
    </v-expand-transition>
  </div>
</template>
