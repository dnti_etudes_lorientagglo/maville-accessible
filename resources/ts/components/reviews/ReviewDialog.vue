<script
  lang="ts"
  setup
>
import Review from '@/ts/api/models/review';
import fillAndSave from '@/ts/api/utilities/fillAndSave';
import BlockedAccountAlert from '@/ts/components/account/BlockedAccountAlert.vue';
import RequiresVerifiedEmailAlert from '@/ts/components/account/RequiresVerifiedEmailAlert.vue';
import AppDivider from '@/ts/components/common/AppDivider.vue';
import RequiredInputsHint from '@/ts/components/forms/RequiredInputsHint.vue';
import AppDialog from '@/ts/components/overlays/AppDialog.vue';
import AccessibleInputs
  from '@/ts/components/resources/composables/accessible/AccessibleInputs.vue';
import useSignInRoute from '@/ts/composables/auth/useSignInRoute';
import useLazyValue from '@/ts/composables/common/useLazyValue';
import useForm from '@/ts/composables/forms/useForm';
import useInput from '@/ts/composables/forms/useInput';
import useTranslatableInput from '@/ts/composables/forms/useTranslatableInput';
import useAccessibleInputs
  from '@/ts/composables/resources/composables/accessible/useAccessibleInputs';
import { useAuthStore } from '@/ts/stores/auth';
import { useSnackbarStore } from '@/ts/stores/snackbar';
import isNil from '@/ts/utilities/common/isNil';
import FeatureName from '@/ts/utilities/features/featureName';
import { Optional } from '@/ts/utilities/types/optional';
import max from '@/ts/validation/rules/max';
import required from '@/ts/validation/rules/required';
import { ComponentPublicInstance, computed, watch } from 'vue';
import { useI18n } from 'vue-i18n';

const emit = defineEmits<{
  (e: 'done'): void;
  (e: 'update:model-value', v: boolean): void;
}>();

const props = defineProps<{
  modelValue?: boolean;
  review: Review;
  activator?: Optional<string | Element | ComponentPublicInstance>;
}>();

const i18n = useI18n();
const auth = useAuthStore();
const snackbar = useSnackbarStore();
const { accessibilityInput } = useAccessibleInputs();
const signInRoute = useSignInRoute();

const lazyValue = useLazyValue(computed(() => props.modelValue ?? false));

const title = computed(() => (
  props.review.$exists
    ? i18n.t('reviews.review.update.title')
    : i18n.t('reviews.review.create.title')
));
const action = computed(() => (
  props.review.$exists
    ? i18n.t('reviews.review.update.action')
    : i18n.t('reviews.review.create.action')
));

const ratingInput = useInput({
  name: 'rating',
  label: i18n.t('reviews.rating.label'),
  value: 0,
});

const bodyInput = useInput({
  ...useTranslatableInput(),
  label: i18n.t('forms.labels.comment'),
  name: 'body',
  rules: [max(500)],
});

const ratingError = computed(() => ratingInput.props.errorMessages.length > 0);

const { id, loading, btnLoading, fill, reset, onSubmit } = useForm([
  ratingInput, bodyInput, accessibilityInput,
] as const, {
  boot() {
    fill({
      rating: props.review.rating,
      body: props.review.body,
      accessibility: props.review.accessibility,
    });
  },
  onValidate() {
    if (!auth.verified || auth.blocked) {
      return false;
    }

    ratingInput.props.errorMessages = isNil(ratingInput.value) || ratingInput.value === 0
      ? [required.translateFor(ratingInput.props.label, ratingInput.value)]
      : [];

    return !ratingError.value;
  },
  async onSubmit({ allValues }) {
    const toast = props.review.$exists
      ? i18n.t('reviews.review.update.toast')
      : i18n.t('reviews.review.create.toast');

    await fillAndSave(props.review, allValues);

    lazyValue.value = false;

    snackbar.toast(toast);

    emit('done');
  },
});

watch(lazyValue, (value) => {
  if (!value) {
    reset();
    ratingInput.props.errorMessages = [];
  }

  emit('update:model-value', value);
});

const onClose = () => {
  lazyValue.value = false;
};
</script>

<script lang="ts">
export default {
  inheritAttrs: false,
};
</script>

<template>
  <slot
    v-if="auth.guest"
    name="activator"
    :props="{ to: signInRoute }"
  />
  <app-dialog
    v-else
    v-model="lazyValue"
    :activator="activator"
    :title="title"
    :persistent="loading"
    :form-props="{ readonly: loading, onSubmit }"
    size="large"
    v-bind="$attrs"
  >
    <template #activator="activatorProps">
      <slot
        name="activator"
        v-bind="{ ...activatorProps, props: { ...activatorProps.props, role: 'button' } }"
      />
    </template>
    <app-divider />
    <v-card-text>
      <v-row dense>
        <v-col
          v-if="auth.blocked"
          cols="12"
        >
          <blocked-account-alert />
        </v-col>
        <v-col
          v-else-if="!auth.verified"
          cols="12"
        >
          <requires-verified-email-alert
            :message="$t('reviews.review.needVerifiedEmail')"
            immediate
            @verify="onClose"
          />
        </v-col>
        <v-col cols="12">
          <required-inputs-hint />
        </v-col>
        <v-col
          cols="12"
          class="text-center"
          :class="{ 'rating--disabled': !auth.verified || auth.blocked }"
          data-required
        >
          <v-label
            :id="`rating-label-${id}`"
            :class="{ 'text-error': ratingError }"
          >
            {{ ratingInput.props.label }}
          </v-label>
          <div>
            <v-rating
              :model-value="ratingInput.props.modelValue"
              :aria-labelledby="`rating-label-${id}`"
              :aria-describedby="`rating-messages-${id}`"
              :disabled="!auth.verified || auth.blocked"
              :color="ratingError ? 'error' : 'primary'"
              role="radiogroup"
              @update:model-value="ratingInput.props['onUpdate:modelValue']"
            />
          </div>
          <v-messages
            :id="`rating-messages-${id}`"
            :active="ratingError"
            :messages="ratingInput.props.errorMessages"
            role="alert"
            aria-live="polite"
            class="text-error"
          />
        </v-col>
        <v-col
          v-if="FeatureName.REVIEWS_BODY.check()"
          cols="12"
        >
          <v-textarea
            :disabled="!auth.verified || auth.blocked"
            maxlength="500"
            rows="2"
            counter
            v-bind="bodyInput.props"
          />
        </v-col>
        <v-expand-transition>
          <accessible-inputs
            v-if="ratingInput.value"
            :accessibility-input="accessibilityInput"
            rating
            dense
          />
        </v-expand-transition>
      </v-row>
    </v-card-text>
    <app-divider />
    <v-card-actions class="justify-end">
      <v-btn @click="onClose">
        {{ $t('actions.cancel') }}
      </v-btn>
      <v-btn
        :disabled="!auth.verified || auth.blocked"
        type="submit"
        variant="elevated"
        color="primary"
        v-bind="btnLoading"
      >
        {{ action }}
      </v-btn>
    </v-card-actions>
  </app-dialog>
</template>

<style
  lang="scss"
  scoped
>
  .rating--disabled {
    opacity: var(--v-disabled-opacity);

    :deep(.v-rating__item label) {
      cursor: default;
    }
  }
</style>
