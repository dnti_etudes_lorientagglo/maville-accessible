<script
  lang="ts"
  setup
>
import { findCachedRequestType } from '@/ts/api/cached/cachedRequestTypes';
import RequestTypeCode from '@/ts/api/enums/requestTypeCode';
import Activity from '@/ts/api/models/activity';
import Organization from '@/ts/api/models/organization';
import Place from '@/ts/api/models/place';
import Request from '@/ts/api/models/request';
import fillAndSave from '@/ts/api/utilities/fillAndSave';
import BlockedAccountAlert from '@/ts/components/account/BlockedAccountAlert.vue';
import RequiresVerifiedEmailAlert from '@/ts/components/account/RequiresVerifiedEmailAlert.vue';
import AppAlert from '@/ts/components/common/AppAlert.vue';
import AppDivider from '@/ts/components/common/AppDivider.vue';
import VOrganizationInput from '@/ts/components/forms/instances/VOrganizationInput.vue';
import VCustomPhoneInput from '@/ts/components/forms/phone/VCustomPhoneInput.vue';
import RequiredInputsHint from '@/ts/components/forms/RequiredInputsHint.vue';
import VInputClear from '@/ts/components/forms/VInputClear.vue';
import AppDialog from '@/ts/components/overlays/AppDialog.vue';
import useSignInRoute from '@/ts/composables/auth/useSignInRoute';
import useForm from '@/ts/composables/forms/useForm';
import useInput from '@/ts/composables/forms/useInput';
import useClaimTitle from '@/ts/composables/requests/useClaimTitle';
import extractResponseError from '@/ts/errors/extractResponseError';
import { useAuthStore } from '@/ts/stores/auth';
import { useSnackbarStore } from '@/ts/stores/snackbar';
import max from '@/ts/validation/rules/max';
import required from '@/ts/validation/rules/required';
import { HttpConflictError } from '@foscia/http';
import { computed, ref, toRef, watch } from 'vue';
import { useI18n } from 'vue-i18n';

const emit = defineEmits<{
  (e: 'done'): void;
}>();

const props = defineProps<{
  requestable: Place | Activity;
}>();

const i18n = useI18n();
const auth = useAuthStore();
const snackbar = useSnackbarStore();
const signInRoute = useSignInRoute();
const title = useClaimTitle(toRef(props, 'requestable'));

const dialog = ref(false);
const error = ref(null as string | null);

const organizations = computed(() => (
  auth.auth ? auth.user.memberOfOrganizations.map((m) => m.organization) : []
));

const organizationInput = useInput({
  name: 'organization',
  value: (organizations.value[0] ?? null) as Organization | null,
});

const phoneInput = useInput({
  name: 'phone',
  value: '',
  rules: [required],
});

const bodyInput = useInput({
  label: i18n.t('forms.labels.comment'),
  name: 'body',
  value: '',
  rules: [max(500)],
});

const { loading, btnLoading, resetDefaults, onSubmit } = useForm([
  phoneInput, organizationInput, bodyInput,
] as const, {
  onValidate() {
    return auth.verified && !auth.blocked;
  },
  async onSubmit({ allValues }) {
    error.value = null;

    await fillAndSave(new Request(), {
      url: window.location.href,
      body: allValues.body,
      data: { phone: allValues.phone },
      requestable: props.requestable,
      resultable: allValues.organization,
      requestType: await findCachedRequestType(
        RequestTypeCode.CLAIM.value,
      ),
    });

    dialog.value = false;

    snackbar.toast(i18n.t('requests.common.toast'));

    emit('done');
  },
  async onFailure(formError) {
    if (formError instanceof HttpConflictError) {
      error.value = await extractResponseError(formError);
    }

    return error.value !== null;
  },
});

watch(dialog, (opened) => {
  if (!opened) {
    resetDefaults();
    error.value = null;
  }
});

const onOrganizationClear = () => {
  organizationInput.value = null;
};

const onClose = () => {
  dialog.value = false;
};
</script>

<template>
  <slot
    v-if="auth.guest"
    name="activator"
    :props="{ to: signInRoute }"
  />
  <app-dialog
    v-else
    v-model="dialog"
    :title="title"
    :subtitle="$t('requests.claim.subtitle')"
    :persistent="loading"
    :form-props="{ readonly: loading, onSubmit }"
  >
    <template #activator="activatorProps">
      <slot
        name="activator"
        v-bind="{ ...activatorProps, props: { ...activatorProps.props, role: 'button' } }"
      />
    </template>
    <app-divider />
    <v-card-text>
      <v-row dense>
        <v-expand-transition>
          <v-col
            v-if="error"
            cols="12"
          >
            <app-alert
              type="error"
              class="mb-3"
            >
              {{ error }}
            </app-alert>
          </v-col>
        </v-expand-transition>
        <v-col
          v-if="auth.blocked"
          cols="12"
        >
          <blocked-account-alert />
        </v-col>
        <v-col
          v-else-if="!auth.verified"
          cols="12"
        >
          <requires-verified-email-alert
            :message="$t('requests.common.needVerifiedEmail')"
            immediate
            @verify="onClose"
          />
        </v-col>
        <v-col cols="12">
          <required-inputs-hint />
        </v-col>
        <v-col cols="12">
          <v-custom-phone-input
            :disabled="!auth.verified || auth.blocked"
            v-bind="phoneInput.props"
          />
        </v-col>
        <v-col
          v-if="organizations.length"
          cols="12"
        >
          <v-organization-input
            :disabled="!auth.verified || auth.blocked"
            :items="organizations"
            :hint="$t('requests.claim.hints.organization')"
            persistent-hint
            v-bind="organizationInput.props"
          >
            <template #append-inner>
              <v-input-clear
                :active="!!organizationInput.props.modelValue"
                :label="organizationInput.props.label"
                @clear="onOrganizationClear"
              />
            </template>
          </v-organization-input>
        </v-col>
        <v-col cols="12">
          <v-textarea
            :disabled="!auth.verified || auth.blocked"
            maxlength="500"
            rows="2"
            counter
            v-bind="bodyInput.props"
          />
        </v-col>
      </v-row>
    </v-card-text>
    <app-divider />
    <v-card-actions class="justify-end">
      <v-btn @click="onClose">
        {{ $t('actions.cancel') }}
      </v-btn>
      <v-btn
        :disabled="!auth.verified || auth.blocked"
        type="submit"
        variant="elevated"
        color="primary"
        v-bind="btnLoading"
      >
        {{ $t('requests.common.action') }}
      </v-btn>
    </v-card-actions>
  </app-dialog>
</template>
