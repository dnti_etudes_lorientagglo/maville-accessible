<script
  lang="ts"
  setup
>
import Media from '@/ts/api/models/media';
import fillAndSave from '@/ts/api/utilities/fillAndSave';
import hardDelete from '@/ts/api/utilities/hardDelete';
import OpenInNewIcon from '@/ts/components/accessibility/OpenInNewIcon.vue';
import AppAlert from '@/ts/components/common/AppAlert.vue';
import AppDivider from '@/ts/components/common/AppDivider.vue';
import AppImg from '@/ts/components/common/AppImg.vue';
import AppDialog from '@/ts/components/overlays/AppDialog.vue';
import ConfirmDialog from '@/ts/components/overlays/ConfirmDialog.vue';
import useForm from '@/ts/composables/forms/useForm';
import useInput from '@/ts/composables/forms/useInput';
import useCol from '@/ts/composables/layouts/useCol';
import useMediaImg from '@/ts/composables/media/useMediaImg';
import { useLangStore } from '@/ts/stores/lang';
import { useSnackbarStore } from '@/ts/stores/snackbar';
import isNone from '@/ts/utilities/common/isNone';
import fullNameWithYou from '@/ts/utilities/lang/fullNameWithYou';
import humanFileSize from '@/ts/utilities/math/humanFileSize';
import { Awaitable } from '@/ts/utilities/types/awaitable';
import max from '@/ts/validation/rules/max';
import required from '@/ts/validation/rules/required';
import { mdiEyeOffOutline, mdiTrashCanOutline } from '@mdi/js';
import { HttpConflictError } from '@foscia/http';
import { computed, ref, toRef, watch } from 'vue';
import { useI18n } from 'vue-i18n';

const emit = defineEmits<{
  (e: 'update:model-value', value: boolean): void;
}>();

const props = defineProps<{
  media: Media;
  onDelete?: (media: Media) => Awaitable<void>;
}>();

const i18n = useI18n();
const lang = useLangStore();
const snackbar = useSnackbarStore();
const image = useMediaImg(toRef(props, 'media'));

const dialog = ref(false);
const deleteDialog = ref(false);
const deleteConflict = ref(false);

const isPrivate = computed(() => props.media.visibility !== 'public');
const legend = computed(() => [
  props.media.fileName,
  humanFileSize(props.media.size, lang.locale),
  !isNone(props.media.width) && !isNone(props.media.height)
    ? `${props.media.width}x${props.media.height}`
    : '',
  props.media.owner
    ? i18n.t('forms.media.library.details.uploadedBy', {
      name: fullNameWithYou(props.media.owner),
    })
    : '',
].filter((v) => !isNone(v)).join(' - '));

const fullCol = useCol('full');
const halfCol = useCol('half');

const nameInput = useInput({
  name: 'name',
  value: '',
  rules: [required, max(100)],
});

const { loading, btnLoading, fill, reset, onSubmit } = useForm([nameInput] as const, {
  boot: () => {
    fill({ name: props.media.name });
  },
  onSubmit: async ({ values }) => {
    await fillAndSave(props.media, values);

    snackbar.toast(i18n.t('actions.toasts.edit', {
      name: props.media.name,
    }));

    dialog.value = false;
  },
});

watch(dialog, (value) => {
  emit('update:model-value', value);

  if (!value) {
    reset();
  }
});

watch(deleteDialog, (value) => {
  if (!value) {
    deleteConflict.value = false;
  }
});

const onClose = () => {
  dialog.value = false;
};

const onDeleteConfirm = async () => {
  deleteConflict.value = false;

  try {
    await hardDelete(props.media);
  } catch (error) {
    if (error instanceof HttpConflictError) {
      deleteConflict.value = true;

      return true;
    }

    throw error;
  }

  snackbar.toast(i18n.t('actions.toasts.delete', {
    name: props.media.name,
  }));

  onClose();
  props.onDelete?.(props.media);

  return false;
};
</script>

<template>
  <app-dialog
    v-model="dialog"
    :title="$t('forms.media.library.details.title')"
    :persistent="loading"
    :form-props="{ readonly: loading, onSubmit }"
    size="large"
  >
    <template #activator="activatorsProps">
      <slot
        name="activator"
        v-bind="activatorsProps"
      />
    </template>
    <app-divider />
    <v-card-text>
      <v-row dense>
        <v-col
          v-if="image"
          v-bind="fullCol"
          class="media__image__container text-center"
        >
          <app-img
            class="media__image rounded-lg"
            sizes="600px"
            v-bind="image"
          />
        </v-col>
        <v-col
          v-bind="fullCol"
          class="text-center"
        >
          <v-icon
            v-if="isPrivate"
            :title="$t('forms.media.library.details.private')"
            :icon="mdiEyeOffOutline"
            role="img"
            aria-hidden="false"
            color="on-surface"
          />
          {{ legend }}
        </v-col>
        <v-col v-bind="halfCol">
          <v-btn
            :href="media.redirectURL"
            target="_blank"
            rel="noopener"
            variant="tonal"
            block
          >
            {{ $t('actions.open') }}
            <open-in-new-icon />
          </v-btn>
        </v-col>
        <v-col v-bind="halfCol">
          <confirm-dialog
            v-model="deleteDialog"
            :title="$t('actions.details.delete.title')"
            :run="onDeleteConfirm"
            size="normal"
          >
            <template #activator="{ props: activatorProps }">
              <v-btn
                :prepend-icon="mdiTrashCanOutline"
                color="error"
                variant="tonal"
                block
                v-bind="activatorProps"
              >
                {{ $t('actions.delete') }}
              </v-btn>
            </template>
            <v-slide-y-transition>
              <app-alert
                v-if="deleteConflict"
                type="error"
                class="mb-3"
              >
                {{ $t('forms.media.library.alerts.deleteConflict') }}
              </app-alert>
            </v-slide-y-transition>
            <p class="my-0">
              {{ $t('actions.details.delete.confirm', { name: media.name }) }}
            </p>
          </confirm-dialog>
        </v-col>
        <v-col v-bind="fullCol">
          <v-text-field
            v-bind="nameInput.props"
          />
        </v-col>
      </v-row>
    </v-card-text>
    <app-divider />
    <v-card-actions class="justify-end">
      <v-btn
        variant="text"
        @click="onClose"
      >
        {{ $t('actions.cancel') }}
      </v-btn>
      <v-btn
        type="submit"
        variant="elevated"
        color="primary"
        v-bind="btnLoading"
      >
        {{ $t('actions.save') }}
      </v-btn>
    </v-card-actions>
  </app-dialog>
</template>

<style
  lang="scss"
  scoped
>
  .media__image__container {
    max-height: 30vh;
  }

  @media(min-width: 960px) {
    .media__image__container {
      max-height: 50vh;
    }
  }

  .media__image {
    max-width: 100%;
    max-height: 100%;
    // border: thin solid rgba(var(--v-border-color), var(--v-border-opacity));
    border-radius: inherit;
    background: repeating-conic-gradient(
        rgb(var(--v-theme-image-primary-underlay)) 0% 25%,
        rgb(var(--v-theme-image-secondary-underlay)) 0% 50%
    ) 50% / 20px 20px;
  }
</style>
