<script
  lang="ts"
  setup
>
import action from '@/ts/api/action';
import Media, { MediaVisibility } from '@/ts/api/models/media';
import AppLoader from '@/ts/components/common/AppLoader.vue';
import AppDataLabel from '@/ts/components/data/AppDataLabel.vue';
import VSelectInput from '@/ts/components/forms/selects/VSelectInput.vue';
import MediaLibraryDetailsDialog from '@/ts/components/media/MediaLibraryDetailsDialog.vue';
import MediaLibraryList from '@/ts/components/media/MediaLibraryList.vue';
import useAsync from '@/ts/composables/common/useAsync';
import useForm from '@/ts/composables/forms/useForm';
import useInput from '@/ts/composables/forms/useInput';
import useCol from '@/ts/composables/layouts/useCol';
import { i18nInstance } from '@/ts/plugins/i18n';
import { useAuthStore } from '@/ts/stores/auth';
import { useSnackbarStore } from '@/ts/stores/snackbar';
import last from '@/ts/utilities/arrays/last';
import orderBy from '@/ts/utilities/arrays/orderBy';
import uniqBy from '@/ts/utilities/arrays/uniqBy';
import wrap from '@/ts/utilities/arrays/wrap';
import isNil from '@/ts/utilities/common/isNil';
import isNone from '@/ts/utilities/common/isNone';
import focusElement from '@/ts/utilities/dom/focusElement';
import debounce from '@/ts/utilities/functions/debounce';
import { Optional } from '@/ts/utilities/types/optional';
import { Action, all, include, ModelIdType, query, when } from '@foscia/core';
import { filterBy, paginate, sortBy, sortByDesc, usingDocument } from '@foscia/jsonapi';
import { mdiEyeOffOutline, mdiEyeOutline, mdiFilterOutline, mdiMagnify, mdiTextBoxOutline } from '@mdi/js';
import { computed, onMounted, PropType, ref } from 'vue';
import { useI18n } from 'vue-i18n';

const emit = defineEmits<{
  (e: 'update:model-value', value: Optional<Media | Media[]>): void;
}>();

const props = defineProps({
  modelValue: {
    type: [Object, Array] as PropType<Optional<Media[] | Media>>,
    default: () => undefined,
  },
  only: {
    type: String as PropType<'image' | 'video'>,
    default: () => undefined,
  },
  visibility: {
    type: String as PropType<MediaVisibility>,
    default: () => undefined,
  },
  uploadProgress: {
    type: Number as PropType<number | null>,
    default: null,
  },
  selectable: Boolean,
  multiple: Boolean,
});

const i18n = useI18n();
const auth = useAuthStore();
const snackbar = useSnackbarStore();
const { loading, btnLoading, asyncRun } = useAsync();

const filterDefault = {
  image: 'myImages',
  video: 'myVideos',
  any: 'myFiles',
}[props.only ?? 'any'] as keyof typeof filtersMap;
const filtersMap = {
  myFiles: {
    disabled: () => !!props.only,
    filter<C extends {}>(a: Action<C>) {
      a.use(filterBy('owner', [auth.id]));
    },
  },
  myImages: {
    disabled: () => props.only === 'video',
    filter<C extends {}>(a: Action<C>) {
      a.use(filterBy('owner', [auth.id]))
        .use(filterBy('type', ['image']));
    },
  },
  myVideos: {
    disabled: () => props.only === 'image',
    filter<C extends {}>(a: Action<C>) {
      a.use(filterBy('owner', [auth.id]))
        .use(filterBy('type', ['video']));
    },
  },
};

const col = useCol();

const searchInput = useInput({
  name: 'search',
  value: '',
});

const filterInput = useInput({
  name: 'filter',
  label: i18n.t('forms.media.library.filter.label'),
  value: filterDefault,
  items: Object.entries(filtersMap).map(([v, f]) => ({
    value: v,
    title: i18n.t(`forms.media.library.filter.items.${v}`),
    disabled: f.disabled(),
  })),
});

const labelRef = ref<HTMLElement | null>(null);
const media = ref([] as Media[]);
const hasMore = ref(false);

const uploading = computed(() => !isNil(props.uploadProgress));
const arrayValue = computed(() => wrap(props.modelValue));
const allMedia = computed(() => {
  const allMediaArray = [...media.value];

  arrayValue.value.forEach((mediaValue) => {
    if (allMediaArray.indexOf(mediaValue) === -1) {
      allMediaArray.unshift(mediaValue);
    }
  });

  return allMediaArray.filter((m) => m.$exists);
});
const captionIcon = computed(() => ({
  public: mdiEyeOutline,
  private: mdiEyeOffOutline,
  any: undefined,
}[props.visibility ?? 'any']));
const captionKey = computed(() => ({
  public: 'forms.media.library.caption.public',
  private: 'forms.media.library.caption.private',
  any: 'forms.media.library.caption.all',
}[props.visibility ?? 'any']));

const setMedia = (newMedia: Media[]) => {
  media.value = isNone(searchInput.value)
    ? orderBy(uniqBy(newMedia, 'id'), 'createdAt', 'desc')
    : newMedia;
};

const fetch = (reset = false) => asyncRun(async () => {
  let after: ModelIdType | undefined = last(media.value)?.id;
  if (reset) {
    after = undefined;
  }

  const search = searchInput.value;

  const { instances, document } = await action()
    .use(query(Media))
    .use(include('owner'))
    .use(filtersMap[filterInput.value].filter)
    .use(when(props.visibility, filterBy('visibility', props.visibility)))
    .use(when(
      isNone(search),
      sortByDesc('createdAt'),
      (a) => a.use(filterBy('search', search)).use(sortBy('search')),
    ))
    .use(paginate({ limit: 20, after }))
    .run(all(usingDocument));

  if (reset) {
    media.value = [];
  }

  setMedia([...media.value, ...instances]);
  hasMore.value = document.meta!.page.hasMore;
});

const debounceFetch = debounce(fetch);

const { onSubmit } = useForm([
  searchInput, filterInput,
] as const, {
  onChange() {
    loading.value = true;
    debounceFetch(true);
  },
  async onSubmit() {
    await fetch(true);
    snackbar.toast(i18nInstance.t('navigation.listLoaded'));
  },
});

const onFetchMore = async () => {
  await fetch();
};

const onAdd = async (newMedia: Media[]) => {
  if (!newMedia.length) {
    return;
  }

  const filterValue = filterInput.value;
  const shouldReset = searchInput.value !== ''
    || ['myFiles'].indexOf(filterValue) === -1
    || props.only === 'image';
  if (shouldReset) {
    searchInput.value = '';
    filterInput.value = filterDefault;
    await fetch(true);
  }

  setMedia([...newMedia, ...media.value]);

  if (props.multiple && Array.isArray(props.modelValue)) {
    emit('update:model-value', uniqBy([...props.modelValue, ...newMedia], 'id'));
  }

  if (!props.multiple && !Array.isArray(props.modelValue)) {
    emit('update:model-value', newMedia[0]);
  }
};

const onDelete = (removeMedia: Media) => {
  setMedia(media.value.filter((m) => m.id !== removeMedia.id));

  setTimeout(() => {
    if (labelRef.value) {
      focusElement(labelRef.value);
    }
  }, 50);
};

onMounted(async () => {
  await fetch();
});

defineExpose({ onAdd });
</script>

<template>
  <v-form
    :aria-label="$t('accessibility.forms.search.labels.page')"
    :aria-description="$t('accessibility.forms.search.descriptions.list')"
    :readonly="uploading"
    role="search"
    @submit="onSubmit"
  >
    <v-row dense>
      <v-col>
        <v-text-field
          :prepend-inner-icon="mdiMagnify"
          autocomplete="off"
          hide-details
          v-bind="searchInput.props"
        />
      </v-col>
      <v-col v-bind="col">
        <v-select-input
          :prepend-inner-icon="mdiFilterOutline"
          hide-details
          v-bind="filterInput.props"
        />
      </v-col>
    </v-row>
  </v-form>
  <div
    ref="labelRef"
    tabindex="-1"
    class="d-flex align-center py-2 px-1"
  >
    <v-fade-transition mode="out-in">
      <div
        v-if="uploading"
        class="d-flex align-center"
      >
        <app-loader
          :model-value="uploadProgress"
          size="24"
        />
        <span class="text-overline ml-2">
          {{ $t('forms.media.library.uploading') }}
        </span>
      </div>
      <app-data-label
        v-else
        :loading="loading"
      >
        <v-icon
          v-if="captionIcon"
          :icon="captionIcon"
          start
        />
        {{ $t(captionKey, { n: allMedia.length }) }}
        <span v-if="selectable && arrayValue.length">
          {{ $t('states.scoped.selected', { n: arrayValue.length }) }}
        </span>
      </app-data-label>
    </v-fade-transition>
    <slot name="title-append" />
  </div>
  <slot name="list-prepend" />
  <media-library-list
    v-if="allMedia.length"
    :model-value="modelValue"
    :media="allMedia"
    :selectable="selectable"
    :multiple="multiple"
    density="comfortable"
    @update:model-value="emit('update:model-value', $event)"
  >
    <template #actions="{ item, itemColor }">
      <media-library-details-dialog
        :media="item"
        :on-delete="onDelete"
      >
        <template #activator="{ props: activatorsProps }">
          <v-btn
            :title="$t('actions.scoped.view', { name: item.name })"
            :icon="mdiTextBoxOutline"
            :color="itemColor"
            variant="text"
            v-bind="activatorsProps"
            @keydown.stop
          />
        </template>
      </media-library-details-dialog>
    </template>
  </media-library-list>
  <div
    v-if="hasMore"
    class="d-flex justify-center pt-2"
  >
    <v-btn
      v-bind="btnLoading"
      @click="onFetchMore"
    >
      {{ $t('actions.loadMore') }}
    </v-btn>
  </div>
</template>
