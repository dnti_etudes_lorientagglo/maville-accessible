import decorateTTLink from '@/ts/plugins/tiptap/utilities/links/decorateTTLink';
import handleTTLink from '@/ts/plugins/tiptap/utilities/links/handleTTLink';
import router from '@/ts/router';
import isExternalRoute from '@/ts/utilities/routes/isExternalRoute';
import resolveRoute from '@/ts/utilities/routes/resolveRoute';
import { mergeAttributes } from '@tiptap/core';
import Link, { LinkOptions } from '@tiptap/extension-link';

type ExtendedLinkOptions = LinkOptions & {
  enhance: boolean;
};

export default Link.extend<ExtendedLinkOptions>({
  renderHTML({ HTMLAttributes }) {
    return ['a', mergeAttributes(this.options.HTMLAttributes, HTMLAttributes, {
      target: isExternalRoute(resolveRoute(router, HTMLAttributes.href)) ? '_blank' : null,
    }), 0];
  },
  addProseMirrorPlugins() {
    const plugins = this.parent!();

    // FIXME Ensure plugins are empty before adding plugins, see:
    // https://github.com/ueberdosis/tiptap/issues/4704
    if (this.options.enhance && plugins.length === 0) {
      plugins.push(handleTTLink());
      plugins.push(decorateTTLink());
    }

    return plugins;
  },
});
