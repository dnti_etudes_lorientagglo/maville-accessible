import { mergeAttributes } from '@tiptap/core';
import Heading, { HeadingOptions, Level } from '@tiptap/extension-heading';

type ExtendedHeadingOptions = HeadingOptions & {
  renderedLevels: Level[],
  enhance: boolean;
};

export default Heading.extend<ExtendedHeadingOptions>({
  parseHTML() {
    return (this.options.renderedLevels ?? this.options.levels)
      .map((level: Level) => ({
        tag: `h${level}`,
        attrs: { level },
      }));
  },
  renderHTML({ node, HTMLAttributes }) {
    const hasLevel = (this.options.renderedLevels ?? this.options.levels)
      .includes(node.attrs.level);
    const level = hasLevel
      ? node.attrs.level
      : (this.options.renderedLevels ?? this.options.levels)[0];

    return [`h${level}`, mergeAttributes(this.options.HTMLAttributes, HTMLAttributes), 0];
  },
});
