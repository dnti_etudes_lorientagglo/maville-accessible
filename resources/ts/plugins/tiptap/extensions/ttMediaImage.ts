import TTMediaImage from '@/ts/components/forms/editor/extensions/TTMediaImage.vue';
import { mergeAttributes, Node } from '@tiptap/core';
import { VueNodeViewRenderer } from '@tiptap/vue-3';
import { ModelIdType } from '@foscia/core';

export type MediaImageOptions = {
  media: ModelIdType;
  alt?: string;
};

declare module '@tiptap/core' {
  interface Commands<ReturnType> {
    mediaImage: {
      mediaImage: (options: MediaImageOptions) => ReturnType;
    };
  }
}

export default Node.create({
  name: 'tt-media-image',
  group: 'block',
  inline: false,
  addAttributes() {
    return {
      media: {
        default: null,
      },
      alt: {
        default: null,
      },
    };
  },
  addCommands() {
    return {
      mediaImage: (options: MediaImageOptions) => ({ commands }: any) => commands.insertContent({
        type: this.name,
        attrs: options,
      }),
    };
  },
  parseHTML() {
    return [{ tag: this.name }];
  },
  renderHTML({ HTMLAttributes }) {
    return [this.name, mergeAttributes(HTMLAttributes)];
  },
  addNodeView() {
    return VueNodeViewRenderer(TTMediaImage as any);
  },
});
