import useResolvedRouteHandlers from '@/ts/composables/routes/useResolvedRouteHandlers';
import isNil from '@/ts/utilities/common/isNil';
import isNone from '@/ts/utilities/common/isNone';
import { getAttributes } from '@tiptap/core';
import { Plugin, PluginKey } from '@tiptap/pm/state';
import { EditorView } from '@tiptap/pm/view';

export default function handleTTLink() {
  const prepareHref = (view: EditorView, event: KeyboardEvent | MouseEvent) => {
    const attrs = getAttributes(view.state, 'link');
    const link = (event.target as HTMLElement)?.closest('a');
    const href = link?.href ?? attrs.href;
    if (isNone(link) || isNone(href)) {
      return null;
    }

    return href;
  };

  const runHandler = <E extends KeyboardEvent | MouseEvent>(
    view: EditorView,
    event: E,
    handler: E extends KeyboardEvent ? 'onKeydown' : 'onClick',
  ) => {
    const href = prepareHref(view, event);

    return isNil(href)
      ? false
      : !useResolvedRouteHandlers(href)[handler](event as any);
  };

  return new Plugin({
    key: new PluginKey('linkHandler'),
    props: {
      handleDOMEvents: {
        keydown: (view, event) => runHandler(view, event, 'onKeydown'),
        click: (view, event) => runHandler(view, event, 'onClick'),
      },
    },
  });
}
