import OpenInNewIcon from '@/ts/components/accessibility/OpenInNewIcon.vue';
import registerPlugins from '@/ts/plugins/registerPlugins';
import router from '@/ts/router';
import isExternalRoute from '@/ts/utilities/routes/isExternalRoute';
import resolveRoute from '@/ts/utilities/routes/resolveRoute';
import { Node } from '@tiptap/pm/model';
import { Plugin, PluginKey } from '@tiptap/pm/state';
import { Decoration, DecorationSet } from '@tiptap/pm/view';
import { createApp } from 'vue';

function getDecorations(doc: Node) {
  const decorations = [] as Decoration[];

  doc.descendants((node, position) => {
    node.marks.forEach((mark) => {
      if (mark.type.name === 'link') {
        const route = resolveRoute(router, mark.attrs.href);
        if (isExternalRoute(route)) {
          decorations.push(Decoration.widget(position + node.nodeSize, () => {
            const span = document.createElement('span');
            const app = createApp(OpenInNewIcon);
            registerPlugins(app);
            app.mount(span);
            return span;
          }, { side: -1 }));
        }
      }
    });
  });

  return DecorationSet.create(doc, decorations);
}

export default function decorateTTLink() {
  return new Plugin({
    key: new PluginKey('linkDecorator'),
    state: {
      init: (_, state) => getDecorations(state.doc),
      apply: (transaction, oldState) => (
        transaction.docChanged
          ? getDecorations(transaction.doc)
          : oldState
      ),
    },
    props: {
      decorations(state) {
        return this.getState(state);
      },
    },
  });
}
