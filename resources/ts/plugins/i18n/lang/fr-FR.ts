import { fr as $dateFns } from 'date-fns/locale';
import { fr as $vuetify } from 'vuetify/locale';

export default {
  $dateFns,
  $vuetify,
};
