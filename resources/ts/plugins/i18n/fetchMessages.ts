import action from '@/ts/api/action';
import decomposeLocale from '@/ts/utilities/lang/decomposeLocale';
import languageHeaders from '@/ts/utilities/lang/languageHeaders';
import deepMapValues from '@/ts/utilities/objects/deepMapValues';
import { raw } from '@foscia/core';
import { makeGet } from '@foscia/http';

function convertToI18nStyle(translation: string) {
  return translation
    // Replace the ":param" (Laravel format) in translation to "{param}"
    // notation (Vue I18N format).
    .replace(/:([A-Za-z]+)/g, '{$1}')
    // Escape "@" chars to "{'@'}" when they are not used as linked messages.
    // Otherwise, the Vue I18N messages compiler will crash.
    // See https://vue-i18n.intlify.dev/guide/essentials/syntax.html#interpolations
    .replace(/(@(?!(:|(\.(lower|upper|capitalize):))))/g, '{\'$1\'}');
}

async function fetchI18nAppMessages(language: string, preferEasyRead: boolean) {
  const { data } = await action()
    .use(makeGet('-actions/translations', { headers: languageHeaders(language, preferEasyRead) }))
    .run(raw((r) => r.json()));

  return deepMapValues(data, convertToI18nStyle);
}

const langModules = import.meta.glob('./lang/*.ts', { import: 'default' });

function fetchI18nLibsMessages(language: string) {
  return (langModules[`./lang/${language}.ts`] as any)();
}

export default async function fetchMessages(locale: string) {
  const [language, preferEasyRead] = decomposeLocale(locale);

  const app = await fetchI18nAppMessages(language, preferEasyRead);
  const libs = await fetchI18nLibsMessages(language);

  return { ...app, ...libs };
}
