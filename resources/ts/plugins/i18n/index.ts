import env from '@/ts/env';
import { createI18n } from 'vue-i18n';

const i18nPlugin = createI18n({
  legacy: false,
  locale: env.services.i18n.defaultLocale,
  warnHtmlMessage: false,
});

export const i18nInstance = i18nPlugin.global;

export default i18nPlugin;
