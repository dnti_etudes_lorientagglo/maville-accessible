import i18n from '@/ts/plugins/i18n';
import themes from '@/ts/plugins/vuetify/themes';
import { THEME_LIGHT } from '@/ts/plugins/vuetify/themes/light';
import {
  mdiAlertCircleOutline,
  mdiAlertDecagramOutline,
  mdiCheckCircleOutline,
  mdiInformationOutline,
  mdiPaperclip,
} from '@mdi/js';
import { useI18n } from 'vue-i18n';
import { createVuetify } from 'vuetify';
import { md3 } from 'vuetify/blueprints';
import { Intersect } from 'vuetify/directives';
import { aliases, mdi } from 'vuetify/iconsets/mdi-svg';
import { createVueI18nAdapter } from 'vuetify/locale/adapters/vue-i18n';
import 'vuetify/styles';

export default createVuetify({
  blueprint: md3,
  locale: {
    adapter: createVueI18nAdapter({ i18n, useI18n }),
  },
  theme: {
    variations: false,
    defaultTheme: THEME_LIGHT,
    themes,
  },
  directives: {
    Intersect,
  },
  icons: {
    defaultSet: 'mdi',
    aliases: {
      ...aliases,
      success: mdiCheckCircleOutline,
      info: mdiInformationOutline,
      warning: mdiAlertCircleOutline,
      error: mdiAlertDecagramOutline,
    },
    sets: {
      mdi,
    },
  },
  defaults: {
    VCard: { rounded: 'xl' },
    VChip: { tag: 'div', rounded: 'lg', variant: 'tonal' },
    VForm: { validateOn: 'submit' },
    VTextField: { color: 'primary' },
    VTextarea: { color: 'primary' },
    VSelect: { color: 'primary' },
    VAutocomplete: { color: 'primary' },
    VCombobox: { color: 'primary' },
    VCheckbox: { color: 'primary' },
    VCheckboxBtn: { color: 'primary' },
    VRadio: { color: 'primary' },
    VRadioGroup: { color: 'primary' },
    VSwitch: { color: 'primary', inset: true },
    VFileInput: { prependIcon: '', prependInnerIcon: mdiPaperclip, color: 'primary' },
    VProgressLinear: { color: 'primary' },
  },
});
