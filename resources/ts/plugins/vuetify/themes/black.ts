import dark from '@/ts/plugins/vuetify/themes/dark';

export const THEME_BLACK = 'black';

export default {
  ...dark,
  colors: {
    ...dark.colors,
    background: '#121212',
    surface: '#1e1e1e',
  },
};
