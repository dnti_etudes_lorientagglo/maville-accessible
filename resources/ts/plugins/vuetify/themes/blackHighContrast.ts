import darkHighContrast from '@/ts/plugins/vuetify/themes/darkHighContrast';

export const THEME_BLACK_HIGH_CONTRAST = 'blackHighContrast';

export default {
  ...darkHighContrast,
  colors: {
    ...darkHighContrast.colors,
    background: '#0d0d0d',
    surface: '#121212',
  },
};
