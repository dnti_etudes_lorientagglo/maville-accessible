import { envTheme } from '@/ts/env';
import commonThemeColors from '@/ts/plugins/vuetify/themes/commonThemeColors';
import {
  THEME_DARK_BACKGROUND,
  THEME_DARK_PRIMARY,
  THEME_DARK_PRIMARY_IS_DARK,
} from '@/ts/plugins/vuetify/themes/dark';
import generateAdditionalColors from '@/ts/plugins/vuetify/themes/generateAdditionalColors';
import colorContrast from '@/ts/utilities/colors/colorContrast';
import colorShade from '@/ts/utilities/colors/colorShade';

export const THEME_DARK_HIGH_CONTRAST = 'darkHighContrast';

const onPrimary = THEME_DARK_PRIMARY_IS_DARK ? '#ffffff' : '#000000';
const primary = colorContrast(THEME_DARK_PRIMARY, onPrimary) < 8
  ? colorShade(THEME_DARK_PRIMARY, THEME_DARK_PRIMARY_IS_DARK ? -0.35 : 0.35)
  : THEME_DARK_PRIMARY;
const background = colorShade(THEME_DARK_BACKGROUND, -0.1);
const primaryText = colorContrast(background, primary) < 8.5
  ? colorShade(primary, 0.35)
  : primary;

export default {
  dark: true,
  colors: {
    ...commonThemeColors,
    ...generateAdditionalColors(envTheme.dark, primaryText),
    primary,
    'primary-text': primaryText,
    'on-primary': onPrimary,
    background: colorShade(THEME_DARK_BACKGROUND, -0.1),
    'on-background': '#ffffff',
    surface: colorShade(THEME_DARK_BACKGROUND, -0.25),
    'on-surface': '#ffffff',
    'surface-variant': '#ffffff',
    'on-surface-variant': '#000000',
    info: '#97cbf9',
    'on-info': '#042d4e',
    success: '#a9dbaa',
    'on-success': '#163418',
    warning: '#ffd6a4',
    'on-warning': '#3e2300',
    error: '#ff8a9f',
    'on-error': '#3a1219',
  },
  variables: {
    'high-emphasis-opacity': 1,
    'medium-emphasis-opacity': 0.90,
  },
};
