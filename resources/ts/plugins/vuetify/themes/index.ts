import dark from '@/ts/plugins/vuetify/themes/dark';
import darkHighContrast from '@/ts/plugins/vuetify/themes/darkHighContrast';
import light from '@/ts/plugins/vuetify/themes/light';
import lightHighContrast from '@/ts/plugins/vuetify/themes/lightHighContrast';
import black from '@/ts/plugins/vuetify/themes/black';
import blackHighContrast from '@/ts/plugins/vuetify/themes/blackHighContrast';

export default { light, dark, black, lightHighContrast, darkHighContrast, blackHighContrast };
