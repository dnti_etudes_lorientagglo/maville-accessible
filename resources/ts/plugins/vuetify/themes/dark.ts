import { envTheme } from '@/ts/env';
import commonThemeColors from '@/ts/plugins/vuetify/themes/commonThemeColors';
import generateAdditionalColors from '@/ts/plugins/vuetify/themes/generateAdditionalColors';
import { THEME_LIGHT_PRIMARY } from '@/ts/plugins/vuetify/themes/light';
import colorContrast from '@/ts/utilities/colors/colorContrast';
import colorShade from '@/ts/utilities/colors/colorShade';
import isDarkColor from '@/ts/utilities/colors/isDarkColor';

export const THEME_DARK = 'dark';

export const THEME_DARK_PRIMARY = envTheme.dark.primary
  || colorShade(THEME_LIGHT_PRIMARY, isDarkColor(THEME_LIGHT_PRIMARY) ? 0.5 : 0);

export const THEME_DARK_PRIMARY_IS_DARK = isDarkColor(THEME_DARK_PRIMARY);
export const THEME_DARK_PRIMARY_IS_CONTRASTED = colorContrast(THEME_DARK_PRIMARY, '#000000') > 5;

export const THEME_DARK_ON_PRIMARY = envTheme.dark['on-primary']
  || colorShade(THEME_DARK_PRIMARY, isDarkColor(THEME_DARK_PRIMARY) ? 0.9 : -0.9);
export const THEME_DARK_PRIMARY_TEXT = envTheme.dark['primary-text']
  || colorShade(THEME_DARK_PRIMARY, THEME_DARK_PRIMARY_IS_CONTRASTED ? 0 : -0.55);

export const THEME_DARK_BACKGROUND = envTheme.dark.background
  || colorShade(THEME_DARK_ON_PRIMARY, -0.1);
export const THEME_DARK_ON_BACKGROUND = envTheme.dark['on-background']
  || colorShade(THEME_DARK_PRIMARY, 0.5);

export default {
  dark: true,
  colors: {
    ...commonThemeColors,
    ...generateAdditionalColors(envTheme.dark, THEME_DARK_PRIMARY_TEXT),
    primary: THEME_DARK_PRIMARY,
    'primary-text': THEME_DARK_PRIMARY_TEXT,
    'on-primary': THEME_DARK_ON_PRIMARY,
    background: THEME_DARK_BACKGROUND,
    'on-background': THEME_DARK_ON_BACKGROUND,
    surface: envTheme.dark.surface || colorShade(THEME_DARK_BACKGROUND, -0.25),
    'on-surface': envTheme.dark['on-surface'] || colorShade(THEME_DARK_PRIMARY, 0.5),
    'surface-variant': colorShade(THEME_DARK_PRIMARY, 0.5),
    'on-surface-variant': colorShade(THEME_DARK_BACKGROUND, -0.25),
    'on-info': '#042d4e',
    'on-success': '#163418',
    'on-warning': '#3e2300',
    'on-error': '#3a1219',
  },
};
