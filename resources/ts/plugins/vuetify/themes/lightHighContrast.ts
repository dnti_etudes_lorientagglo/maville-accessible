import { envTheme } from '@/ts/env';
import commonThemeColors from '@/ts/plugins/vuetify/themes/commonThemeColors';
import generateAdditionalColors from '@/ts/plugins/vuetify/themes/generateAdditionalColors';
import {
  THEME_LIGHT_BACKGROUND,
  THEME_LIGHT_PRIMARY,
  THEME_LIGHT_PRIMARY_IS_DARK,
} from '@/ts/plugins/vuetify/themes/light';
import colorContrast from '@/ts/utilities/colors/colorContrast';
import colorShade from '@/ts/utilities/colors/colorShade';

export const THEME_LIGHT_HIGH_CONTRAST = 'lightHighContrast';

const onPrimary = THEME_LIGHT_PRIMARY_IS_DARK ? '#ffffff' : '#000000';
const primary = colorContrast(THEME_LIGHT_PRIMARY, onPrimary) < 8
  ? colorShade(THEME_LIGHT_PRIMARY, THEME_LIGHT_PRIMARY_IS_DARK ? -0.35 : 0.35)
  : THEME_LIGHT_PRIMARY;
const background = colorShade(THEME_LIGHT_BACKGROUND, 0.1);
const primaryText = colorContrast(background, primary) < 8.5
  ? colorShade(primary, -0.35)
  : primary;

export default {
  dark: false,
  colors: {
    ...commonThemeColors,
    ...generateAdditionalColors(envTheme.light, primaryText),
    primary,
    'primary-text': primaryText,
    'on-primary': onPrimary,
    background: colorShade(THEME_LIGHT_BACKGROUND, 0.1),
    'on-background': '#000000',
    surface: '#ffffff',
    'on-surface': '#000000',
    'surface-variant': '#000000',
    'on-surface-variant': '#ffffff',
    info: '#074e8c',
    success: '#265d28',
    warning: '#ffa32f',
    'on-warning': '#000000',
    error: '#8c0019',
  },
  variables: {
    'high-emphasis-opacity': 1,
    'medium-emphasis-opacity': 0.90,
  },
};
