type AdditionalColors = {
  secondary: string;
  'on-secondary'?: string;
  tertiary: string;
  'on-tertiary'?: string;
  quaternary: string;
  'on-quaternary'?: string;
  quinary: string;
  'on-quinary'?: string;
};

export default function generateAdditionalColors(
  colors: AdditionalColors,
  defaultOnColor: string,
) {
  return {
    secondary: colors.secondary,
    'on-secondary': colors['on-secondary'] ?? defaultOnColor,
    tertiary: colors.tertiary,
    'on-tertiary': colors['on-tertiary'] ?? defaultOnColor,
    quaternary: colors.quaternary,
    'on-quaternary': colors['on-quaternary'] ?? defaultOnColor,
    quinary: colors.quinary,
    'on-quinary': colors['on-quinary'] ?? defaultOnColor,
  };
}
