import { envTheme } from '@/ts/env';
import commonThemeColors from '@/ts/plugins/vuetify/themes/commonThemeColors';
import generateAdditionalColors from '@/ts/plugins/vuetify/themes/generateAdditionalColors';
import colorContrast from '@/ts/utilities/colors/colorContrast';
import colorShade from '@/ts/utilities/colors/colorShade';
import isDarkColor from '@/ts/utilities/colors/isDarkColor';

export const THEME_LIGHT = 'light';

export const THEME_LIGHT_PRIMARY = envTheme.light.primary;

export const THEME_LIGHT_PRIMARY_IS_DARK = isDarkColor(THEME_LIGHT_PRIMARY);
export const THEME_LIGHT_PRIMARY_IS_CONTRASTED = colorContrast(THEME_LIGHT_PRIMARY, '#ffffff') > 5;

export const THEME_LIGHT_ON_PRIMARY = envTheme.light['on-primary']
  || colorShade(THEME_LIGHT_PRIMARY, THEME_LIGHT_PRIMARY_IS_DARK ? 0.9 : -0.9);
export const THEME_LIGHT_PRIMARY_TEXT = envTheme.light['primary-text']
  || colorShade(THEME_LIGHT_PRIMARY, THEME_LIGHT_PRIMARY_IS_CONTRASTED ? 0 : -0.55);

export const THEME_LIGHT_BACKGROUND = envTheme.light.background
  || colorShade(THEME_LIGHT_PRIMARY, 0.95);
export const THEME_LIGHT_ON_BACKGROUND = envTheme.light['on-background']
  || colorShade(
    THEME_LIGHT_PRIMARY,
    THEME_LIGHT_PRIMARY_IS_DARK ? -0.55 : -0.9,
  );

export default {
  dark: false,
  colors: {
    ...commonThemeColors,
    ...generateAdditionalColors(envTheme.light, THEME_LIGHT_PRIMARY_TEXT),
    primary: THEME_LIGHT_PRIMARY,
    'primary-text': THEME_LIGHT_PRIMARY_TEXT,
    'on-primary': THEME_LIGHT_ON_PRIMARY,
    background: THEME_LIGHT_BACKGROUND,
    'on-background': THEME_LIGHT_ON_BACKGROUND,
    surface: envTheme.light.surface || '#ffffff',
    'on-surface': envTheme.light['on-surface'] || THEME_LIGHT_ON_BACKGROUND,
    'surface-variant': THEME_LIGHT_ON_BACKGROUND,
    'on-surface-variant': THEME_LIGHT_BACKGROUND,
    info: '#0a68bb',
    success: '#307532',
    warning: '#fb8c00',
    'on-warning': '#4b2a00',
    error: '#b00020',
  },
  variables: {
    'high-emphasis-opacity': 0.95,
    'medium-emphasis-opacity': 0.75,
  },
};
