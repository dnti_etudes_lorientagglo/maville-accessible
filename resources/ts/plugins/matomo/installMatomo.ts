/* eslint-disable no-underscore-dangle */
import { usePagesStore } from '@/ts/stores/pages';
import loadScript from '@/ts/utilities/scripts/loadScript';
import { Optional } from '@/ts/utilities/types/optional';
import { RouteLocationNormalized, Router } from 'vue-router';

/*
 * Matomo interaction with Vue.
 * Thanks to https://github.com/AmazingDreams/vue-matomo.
 */

declare global {
  interface Window {
    _paq: any[];
    Piwik: any;
  }
}

type MatomoOptions = {
  siteId: string;
  host?: Optional<string>;
  trackerFileName?: Optional<string>;
  trackerEndpointUrl?: Optional<string>;
  trackerScriptUrl?: Optional<string>;
  crossOrigin?: Optional<string>;
  router: Router;
};

function matomoExists() {
  return new Promise<void>((resolve, reject) => {
    const checkInterval = 50;
    const timeout = 3000;
    const waitStart = Date.now();

    const interval = setInterval(() => {
      if (window.Piwik) {
        clearInterval(interval);

        resolve();

        return;
      }

      if (Date.now() >= waitStart + timeout) {
        clearInterval(interval);

        reject(new Error(`window.Piwik undefined after waiting for ${timeout}ms`));
      }
    }, checkInterval);
  });
}

function matomoTracker() {
  return window.Piwik.getAsyncTracker();
}

function trackPageView(
  options: MatomoOptions,
  to: RouteLocationNormalized,
  from?: RouteLocationNormalized,
) {
  const tracker = matomoTracker();
  const url = options.router.resolve(to.fullPath).href;
  const referrerUrl = from && from.fullPath
    ? options.router.resolve(from.fullPath).href
    : undefined;

  let title = to.meta.title ?? url;

  setTimeout(() => {
    if (options.router.resolve(options.router.currentRoute.value.fullPath).href === url) {
      title = usePagesStore().title ?? title;
    }

    if (referrerUrl) {
      tracker.setReferrerUrl(`${window.location.origin}${referrerUrl}`);
    }

    if (url) {
      tracker.setCustomUrl(`${window.location.origin}${url}`);
    }

    tracker.trackPageView(title);
  }, 1000);
}

function initMatomo(options: MatomoOptions) {
  const tracker = matomoTracker();
  const currentRoute = options.router.currentRoute.value;

  trackPageView(options, currentRoute);

  options.router.afterEach((to, from) => {
    trackPageView(options, to, from);

    tracker.enableLinkTracking();
  });
}

export default async function installMatomo(options: MatomoOptions) {
  const { host, siteId, trackerEndpointUrl, trackerScriptUrl } = options;
  const trackerFileName = options.trackerFileName ?? 'matomo';
  const trackerScript = trackerScriptUrl || `${host}/${trackerFileName}.js`;
  const trackerEndpoint = trackerEndpointUrl || `${host}/${trackerFileName}.php`;

  window._paq = window._paq || [];
  window._paq.push(['setTrackerUrl', trackerEndpoint]);
  window._paq.push(['setSiteId', siteId]);
  window._paq.push(['disableCookies']);

  try {
    await loadScript(trackerScript, options.crossOrigin);
    await matomoExists();
    initMatomo(options);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error('matomo failed to load with following error:');
    // eslint-disable-next-line no-console
    console.error(error);
  }
}
