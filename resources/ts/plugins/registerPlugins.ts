import prepareErrorHandler from '@/ts/errors/prepareErrorHandler';
import i18n from '@/ts/plugins/i18n';
import pinia from '@/ts/plugins/pinia';
import vuetify from '@/ts/plugins/vuetify';
import router from '@/ts/router';
import { App } from 'vue';

export default function registerPlugins(app: App) {
  // `registerEmbedPlugins` file should be updated in sync with this file.
  app.use(pinia);
  app.use(i18n);
  app.use(router);
  app.use(vuetify);

  prepareErrorHandler(app, router);
}
