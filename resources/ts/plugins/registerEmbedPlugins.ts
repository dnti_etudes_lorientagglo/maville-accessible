import prepareErrorHandler from '@/ts/errors/prepareErrorHandler';
import i18n from '@/ts/plugins/i18n';
import pinia from '@/ts/plugins/pinia';
import vuetify from '@/ts/plugins/vuetify';
import { App } from 'vue';

export default function registerEmbedPlugins(app: App) {
  app.use(pinia);
  app.use(i18n);
  app.use(vuetify);

  prepareErrorHandler(app);
}
