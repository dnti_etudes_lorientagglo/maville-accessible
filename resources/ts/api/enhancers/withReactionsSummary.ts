import { param } from '@foscia/http';

export default function withReactionsSummary() {
  return param('withReactionsSummary', true);
}
