import { param } from '@foscia/http';

export default function withReviewsSummary() {
  return param('withReviewsSummary', true);
}
