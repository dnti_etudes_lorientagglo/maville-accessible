import type { Reactable } from '@/ts/api/composables/reactable';
import EnumWithProps from '@/ts/utilities/enums/enumWithProps';
import Review from '@/ts/api/models/review';
import { i18nInstance } from '@/ts/plugins/i18n';
import { mdiThumbUpOutline } from '@mdi/js';

export type ReactionTypeProps = {
  icon: string;
};

export default class ReactionType
  extends EnumWithProps<string, ReactionTypeProps> {
  public static readonly LIKE = new ReactionType('like', {
    icon: mdiThumbUpOutline,
  });

  public static readonly all = new Map([
    [ReactionType.LIKE.value, ReactionType.LIKE],
  ]);

  public label(reactable: Reactable) {
    return i18nInstance.t(this.translationKey(reactable, 'label'));
  }

  public title(reactable: Reactable, n: number) {
    return i18nInstance.t(this.translationKey(reactable, 'title'), { n });
  }

  private translationKey(reactable: Reactable, key: string) {
    if (this.is(ReactionType.LIKE) && reactable instanceof Review) {
      return `reactions.types.${key}.useful`;
    }

    return `reactions.types.${key}.${this.value}`;
  }
}
