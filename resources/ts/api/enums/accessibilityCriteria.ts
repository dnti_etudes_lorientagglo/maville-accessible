import AccessibilityCategory from '@/ts/api/enums/accessibilityCategory';
import AccessibilityCriteriaCategory from '@/ts/api/enums/accessibilityCriteriaCategory';
import AccessibilityCriteriaType from '@/ts/api/enums/accessibilityCriteriaType';
import { Dictionary } from '@/ts/utilities/types/dictionary';

export default class AccessibilityCriteria {
  public constructor(
    public readonly editable: boolean,
    public readonly id: string,
    public readonly criteriaCategory: AccessibilityCriteriaCategory,
    public readonly accessibilityCategories: AccessibilityCategory[],
    public readonly type: AccessibilityCriteriaType,
    public readonly positive: boolean | string | string[] | null,
    public readonly metadata: Dictionary,
  ) {
  }
}
