import { i18nInstance } from '@/ts/plugins/i18n';
import EnumWithProps from '@/ts/utilities/enums/enumWithProps';
import {
  mdiBrain,
  mdiEarHearing,
  mdiEye,
  mdiHumanMaleFemale,
  mdiWheelchairAccessibility,
} from '@mdi/js';

export type AccessibilityCategoryProps = {
  icon: string;
};

export default class AccessibilityCategory
  extends EnumWithProps<string, AccessibilityCategoryProps> {
  public static readonly PHYSICAL = new AccessibilityCategory('physical', {
    icon: mdiWheelchairAccessibility,
  });

  public static readonly VISUAL = new AccessibilityCategory('visual', {
    icon: mdiEye,
  });

  public static readonly HEARING = new AccessibilityCategory('hearing', {
    icon: mdiEarHearing,
  });

  public static readonly COGNITIVE = new AccessibilityCategory('cognitive', {
    icon: mdiBrain,
  });

  public static readonly RESTROOMS = new AccessibilityCategory('restrooms', {
    icon: mdiHumanMaleFemale,
  });

  public static readonly all = new Map([
    [AccessibilityCategory.PHYSICAL.value, AccessibilityCategory.PHYSICAL],
    [AccessibilityCategory.VISUAL.value, AccessibilityCategory.VISUAL],
    [AccessibilityCategory.HEARING.value, AccessibilityCategory.HEARING],
    [AccessibilityCategory.COGNITIVE.value, AccessibilityCategory.COGNITIVE],
    [AccessibilityCategory.RESTROOMS.value, AccessibilityCategory.RESTROOMS],
  ]);

  public get label() {
    return i18nInstance.t(`accessibility.categories.labels.${this.value}`);
  }

  public get typology() {
    return i18nInstance.t(`accessibility.categories.typologies.${this.value}`);
  }
}
