import EnumWithProps from '@/ts/utilities/enums/enumWithProps';
import { mdiFileOutline, mdiImageOutline, mdiVideoOutline } from '@mdi/js';

export type MediaTypeProps = {
  icon: string;
  mimeRegExp: RegExp;
};

export default class MediaType extends EnumWithProps<string, MediaTypeProps> {
  public static readonly IMAGE = new MediaType('image', {
    icon: mdiImageOutline,
    mimeRegExp: /image\//,
  });

  public static readonly VIDEO = new MediaType('video', {
    icon: mdiVideoOutline,
    mimeRegExp: /video\//,
  });

  public static readonly UNKNOWN = new MediaType('unknown', {
    icon: mdiFileOutline,
    mimeRegExp: /.*/,
  });
}
