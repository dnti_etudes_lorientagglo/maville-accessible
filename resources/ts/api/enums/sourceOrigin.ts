import EnumWithProps from '@/ts/utilities/enums/enumWithProps';
import { i18nInstance } from '@/ts/plugins/i18n';

export type SourceOriginProps = {
  website: string;
};

export default class SourceOrigin extends EnumWithProps<string, SourceOriginProps> {
  public static readonly ACCESLIBRE = new SourceOrigin('acceslibre', {
    website: 'https://acceslibre.beta.gouv.fr',
  });

  public static readonly INFOLOCALE = new SourceOrigin('infolocale', {
    website: 'https://www.infolocale.fr',
  });

  public static readonly all = new Map([
    [SourceOrigin.ACCESLIBRE.value, SourceOrigin.ACCESLIBRE],
    [SourceOrigin.INFOLOCALE.value, SourceOrigin.INFOLOCALE],
  ]);

  public get label() {
    return i18nInstance.t(`composables.sourceable.sources.labels.${this.value}`);
  }
}
