import { i18nInstance } from '@/ts/plugins/i18n';
import EnumWithProps from '@/ts/utilities/enums/enumWithProps';
import {
  mdiAccountMultipleOutline,
  mdiBus,
  mdiCheckDecagramOutline,
  mdiInformationOutline,
  mdiLogin,
  mdiRoadVariant,
} from '@mdi/js';

export type AccessibilityCriteriaCategoryProps = {
  icon: string;
};

export default class AccessibilityCriteriaCategory
  extends EnumWithProps<string, AccessibilityCriteriaCategoryProps> {
  public static readonly TRANSPORT = new AccessibilityCriteriaCategory('transport', {
    icon: mdiBus,
  });

  public static readonly EXTERNAL = new AccessibilityCriteriaCategory('external', {
    icon: mdiRoadVariant,
  });

  public static readonly ENTRANCE = new AccessibilityCriteriaCategory('entrance', {
    icon: mdiLogin,
  });

  public static readonly RECEPTION = new AccessibilityCriteriaCategory('reception', {
    icon: mdiAccountMultipleOutline,
  });

  public static readonly COMMENTS = new AccessibilityCriteriaCategory('comments', {
    icon: mdiInformationOutline,
  });

  public static readonly CONFORMITY = new AccessibilityCriteriaCategory('conformity', {
    icon: mdiCheckDecagramOutline,
  });

  public static readonly all = new Map([
    [AccessibilityCriteriaCategory.TRANSPORT.value, AccessibilityCriteriaCategory.TRANSPORT],
    [AccessibilityCriteriaCategory.EXTERNAL.value, AccessibilityCriteriaCategory.EXTERNAL],
    [AccessibilityCriteriaCategory.ENTRANCE.value, AccessibilityCriteriaCategory.ENTRANCE],
    [AccessibilityCriteriaCategory.RECEPTION.value, AccessibilityCriteriaCategory.RECEPTION],
    [AccessibilityCriteriaCategory.COMMENTS.value, AccessibilityCriteriaCategory.COMMENTS],
    [AccessibilityCriteriaCategory.CONFORMITY.value, AccessibilityCriteriaCategory.CONFORMITY],
  ]);

  public get label() {
    return i18nInstance.t(`accessibility.criteria.categories.${this.value}`);
  }
}
