import EnumOf from '@/ts/utilities/enums/enumOf';

export default class RequestTypeCode extends EnumOf<string> {
  public static readonly CLAIM = new RequestTypeCode('claim');

  public static readonly JOIN = new RequestTypeCode('join');
}
