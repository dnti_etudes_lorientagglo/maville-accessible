import { i18nInstance } from '@/ts/plugins/i18n';
import EnumOf from '@/ts/utilities/enums/enumOf';

export default class ActivityPublicCode extends EnumOf<string> {
  public static readonly ALL = new ActivityPublicCode('all');

  public static readonly BABY = new ActivityPublicCode('baby');

  public static readonly CHILDREN = new ActivityPublicCode('children');

  public static readonly TEENAGER = new ActivityPublicCode('teenager');

  public static readonly ADULT = new ActivityPublicCode('adult');

  public static readonly SENIOR = new ActivityPublicCode('senior');

  public static readonly all = new Map([
    [ActivityPublicCode.ALL.value, ActivityPublicCode.ALL],
    [ActivityPublicCode.BABY.value, ActivityPublicCode.BABY],
    [ActivityPublicCode.CHILDREN.value, ActivityPublicCode.CHILDREN],
    [ActivityPublicCode.TEENAGER.value, ActivityPublicCode.TEENAGER],
    [ActivityPublicCode.ADULT.value, ActivityPublicCode.ADULT],
    [ActivityPublicCode.SENIOR.value, ActivityPublicCode.SENIOR],
  ]);

  public get label() {
    return i18nInstance.t(`activities.public.${this.value}`);
  }
}
