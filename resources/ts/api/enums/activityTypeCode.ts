import EnumOf from '@/ts/utilities/enums/enumOf';

export default class ActivityTypeCode extends EnumOf<string> {
  public static readonly EVENTS = new ActivityTypeCode('events');

  public static readonly ACTIVITIES = new ActivityTypeCode('activities');
}
