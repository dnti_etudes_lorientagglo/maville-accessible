import EnumOf from '@/ts/utilities/enums/enumOf';

export default class RoleScopeName extends EnumOf<string> {
  public static readonly GLOBAL = new RoleScopeName('global');

  public static readonly ORGANIZATION = new RoleScopeName('organization');
}
