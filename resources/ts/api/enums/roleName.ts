import EnumOf from '@/ts/utilities/enums/enumOf';

export default class RoleName extends EnumOf<string> {
  public static readonly USER_ADMIN = new RoleName('user-admin');

  public static readonly APP_ADMIN = new RoleName('app-admin');

  public static readonly CONTENT_ADMIN = new RoleName('content-admin');

  public static readonly ORGANIZATION_ADMIN = new RoleName('organization-admin');

  public static readonly ORGANIZATION_MEMBER = new RoleName('organization-member');

  public static readonly MODERATOR = new RoleName('moderator');
}
