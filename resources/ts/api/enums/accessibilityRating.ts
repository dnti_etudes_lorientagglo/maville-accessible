import EnumWithProps from '@/ts/utilities/enums/enumWithProps';
import { i18nInstance } from '@/ts/plugins/i18n';
import isNil from '@/ts/utilities/common/isNil';
import { Optional } from '@/ts/utilities/types/optional';

export type AccessibilityRatingProps = {
  color: string | undefined;
  threshold: number;
};

export default class AccessibilityRating
  extends EnumWithProps<number | null, AccessibilityRatingProps> {
  public static readonly DONT_KNOWN = new AccessibilityRating(null, {
    color: undefined,
    threshold: -1,
  });

  public static readonly NOT_AT_ALL = new AccessibilityRating(0, {
    color: 'deep-purple',
    threshold: 0,
  });

  public static readonly NOT_VERY = new AccessibilityRating(1, {
    color: 'error',
    threshold: 1,
  });

  public static readonly SOMEWHAT = new AccessibilityRating(2, {
    color: 'orange',
    threshold: 1.5,
  });

  public static readonly HIGHLY = new AccessibilityRating(3, {
    color: 'success',
    threshold: 2.8,
  });

  public static readonly all = new Map([
    [AccessibilityRating.DONT_KNOWN.value, AccessibilityRating.DONT_KNOWN],
    [AccessibilityRating.HIGHLY.value, AccessibilityRating.HIGHLY],
    [AccessibilityRating.SOMEWHAT.value, AccessibilityRating.SOMEWHAT],
    [AccessibilityRating.NOT_VERY.value, AccessibilityRating.NOT_VERY],
    [AccessibilityRating.NOT_AT_ALL.value, AccessibilityRating.NOT_AT_ALL],
  ]);

  public static fromThreshold(value: Optional<number>) {
    if (isNil(value)) {
      return AccessibilityRating.DONT_KNOWN;
    }

    const roundedValue = Math.round(value * 10) / 10;

    return [...AccessibilityRating.all.values()].find(
      (rating) => rating.value !== null && roundedValue >= rating.props.threshold,
    )!;
  }

  public label(context: string) {
    if (this.is(AccessibilityRating.DONT_KNOWN)) {
      return i18nInstance.t(`accessibility.ratings.${this.value}.${context}`);
    }

    return i18nInstance.t(`accessibility.ratings.${this.value}`);
  }
}
