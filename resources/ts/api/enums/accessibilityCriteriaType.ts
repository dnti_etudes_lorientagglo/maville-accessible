import isNone from '@/ts/utilities/common/isNone';
import EnumOf from '@/ts/utilities/enums/enumOf';
import toTrim from '@/ts/utilities/strings/toTrim';

export default class AccessibilityCriteriaType extends EnumOf<string> {
  public static readonly BOOLEAN = new AccessibilityCriteriaType('boolean');

  public static readonly STRING = new AccessibilityCriteriaType('string');

  public static readonly TEXT = new AccessibilityCriteriaType('text');

  public static readonly INTEGER = new AccessibilityCriteriaType('integer');

  public static readonly ITEM = new AccessibilityCriteriaType('item');

  public static readonly ITEMS = new AccessibilityCriteriaType('items');

  public static readonly all = new Map([
    [AccessibilityCriteriaType.BOOLEAN.value, AccessibilityCriteriaType.BOOLEAN],
    [AccessibilityCriteriaType.STRING.value, AccessibilityCriteriaType.STRING],
    [AccessibilityCriteriaType.TEXT.value, AccessibilityCriteriaType.TEXT],
    [AccessibilityCriteriaType.INTEGER.value, AccessibilityCriteriaType.INTEGER],
    [AccessibilityCriteriaType.ITEM.value, AccessibilityCriteriaType.ITEM],
    [AccessibilityCriteriaType.ITEMS.value, AccessibilityCriteriaType.ITEMS],
  ]);

  public parse(value: unknown, defaultValue: unknown = undefined) {
    if (AccessibilityCriteriaType.BOOLEAN.is(this)) {
      return typeof value === 'boolean' ? value : defaultValue;
    }

    if (AccessibilityCriteriaType.INTEGER.is(this)) {
      return Number.isInteger(value) ? value : defaultValue;
    }

    if (AccessibilityCriteriaType.TEXT.is(this) || AccessibilityCriteriaType.STRING.is(this)) {
      return typeof value === 'string' ? toTrim(value) : defaultValue;
    }

    if (AccessibilityCriteriaType.ITEM.is(this)) {
      return typeof value === 'string' ? value : defaultValue;
    }

    return Array.isArray(value)
      ? value.map((v) => (typeof v === 'string' ? v : null)).filter((v) => !isNone(v))
      : [];
  }
}
