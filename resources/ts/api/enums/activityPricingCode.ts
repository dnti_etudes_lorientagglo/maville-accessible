import { i18nInstance } from '@/ts/plugins/i18n';
import EnumWithProps from '@/ts/utilities/enums/enumWithProps';

type ActivityPricingCodeProps = {
  description: boolean;
};

export default class ActivityPricingCode extends EnumWithProps<string, ActivityPricingCodeProps> {
  public static readonly UNKNOWN = new ActivityPricingCode('unknown', {
    description: false,
  });

  public static readonly FREE = new ActivityPricingCode('free', {
    description: false,
  });

  public static readonly CONDITIONED = new ActivityPricingCode('conditioned', {
    description: true,
  });

  public static readonly PAID = new ActivityPricingCode('paid', {
    description: true,
  });

  public static readonly all = new Map([
    [ActivityPricingCode.UNKNOWN.value, ActivityPricingCode.UNKNOWN],
    [ActivityPricingCode.FREE.value, ActivityPricingCode.FREE],
    [ActivityPricingCode.CONDITIONED.value, ActivityPricingCode.CONDITIONED],
    [ActivityPricingCode.PAID.value, ActivityPricingCode.PAID],
  ]);

  public get label() {
    return i18nInstance.t(`activities.pricing.${this.value}`);
  }
}
