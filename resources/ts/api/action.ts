import models from '@/ts/api/models';
import { useLangStore } from '@/ts/stores/lang';
import getCookie from '@/ts/utilities/dom/getCookie';
import decomposeLocale from '@/ts/utilities/lang/decomposeLocale';
import languageHeaders from '@/ts/utilities/lang/languageHeaders';
import { makeActionFactory, makeCache, makeRegistry } from '@foscia/core';
import {
  makeJsonApiAdapter,
  makeJsonApiDeserializer,
  makeJsonApiSerializer,
} from '@foscia/jsonapi';

const actionHeaders = () => {
  const lang = useLangStore();
  const [language, preferEasyRead] = decomposeLocale(lang.editionLocale);

  return {
    'X-XSRF-TOKEN': getCookie('XSRF-TOKEN') as string,
    ...languageHeaders(language, preferEasyRead),
  };
};

const { cache } = makeCache();
const { registry } = makeRegistry(models);
const { deserializer } = makeJsonApiDeserializer();

export {
  actionHeaders,
  cache,
  registry,
  deserializer,
};

export default makeActionFactory({
  cache,
  registry,
  deserializer,
  ...makeJsonApiSerializer(),
  ...makeJsonApiAdapter({
    baseURL: '/api/v1',
    requestTransformers: [(request) => {
      request.headers.set('X-Requested-With', 'XMLHttpRequest');

      if (!request.url.startsWith('/') && !request.url.startsWith(window.location.origin)) {
        return request;
      }

      Object.entries(actionHeaders()).forEach(([key, value]) => {
        if (!request.headers.get(key)) {
          request.headers.set(key, value);
        }
      });

      return request;
    }],
  }),
});
