import makeModel from '@/ts/api/makeModel';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { attr, toDateTime } from '@foscia/core';

export default class SendingCampaignBatch extends makeModel('sending-campaign-batches', {
  attempts: attr<number>(),
  recipientsCounts: attr<Dictionary<number>>(),
  startedAt: attr(toDateTime()).nullable(),
  runningAt: attr(toDateTime()).nullable(),
  finishedAt: attr(toDateTime()).nullable(),
  failedAt: attr(toDateTime()).nullable(),
}) {
}
