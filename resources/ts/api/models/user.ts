import authorizable from '@/ts/api/composables/authorizable';
import makeModel from '@/ts/api/makeModel';
import type { NotificationChannel } from '@/ts/api/models/notification';
import type OrganizationMember from '@/ts/api/models/organizationMember';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { attr, hasMany, hasOne, toDateTime } from '@foscia/core';

export default class User extends makeModel('users', {
  authorizable,
  firstName: attr<string>(),
  lastName: attr<string>(),
  displayInitials: attr<boolean>(),
  email: attr<string>(),
  reviewsCount: attr(0).sync('pull'),
  unreadNotificationsCount: attr(0).sync('pull'),
  notificationsPreferences: attr<Dictionary<NotificationChannel[]>>(),
  emailVerifiedAt: attr<Date | null>(toDateTime()),
  blockedAt: attr<Date | null>(toDateTime()),
  lastLoginAt: attr<Date | null>(toDateTime()),
  scheduledDeletionAt: attr<Date | null>(toDateTime()),
  actingForOrganization: hasOne<OrganizationMember | null>(),
  memberOfOrganizations: hasMany<OrganizationMember[]>(),
}) {
  get fullName() {
    return `${this.firstName} ${this.lastName}`;
  }

  get initials() {
    return `${this.firstName.charAt(0)}${this.lastName.charAt(0)}`.toUpperCase();
  }

  get verified() {
    return !!this.emailVerifiedAt;
  }

  get blocked() {
    return !!this.blockedAt;
  }
}
