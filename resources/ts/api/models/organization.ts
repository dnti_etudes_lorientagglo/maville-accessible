import categorizable from '@/ts/api/composables/categorizable';
import contactable from '@/ts/api/composables/contactable';
import coverable from '@/ts/api/composables/coverable';
import linkable from '@/ts/api/composables/linkable';
import mediable from '@/ts/api/composables/mediable';
import publishable from '@/ts/api/composables/publishable';
import sluggable from '@/ts/api/composables/sluggable';
import sourceable from '@/ts/api/composables/sourceable';
import makeModel from '@/ts/api/makeModel';
import type OrganizationInvitation from '@/ts/api/models/organizationInvitation';
import type OrganizationMember from '@/ts/api/models/organizationMember';
import type OrganizationType from '@/ts/api/models/organizationType';
import { Translation } from '@/ts/utilities/lang/translate';
import { attr, hasMany, hasOne } from '@foscia/core';

export type OrganizationAdministrativeData = {
  siretIdentifier?: string;
  rnaIdentifier?: string;
};

export default class Organization extends makeModel('organizations', {
  categorizable,
  contactable,
  coverable,
  linkable,
  mediable,
  publishable,
  sluggable,
  sourceable,
  name: attr<Translation>(),
  description: attr<Translation | null>(),
  body: attr<Translation | null>(),
  administrativeData: attr<OrganizationAdministrativeData | null>(),
  organizationType: hasOne<OrganizationType>(),
  membersOfOrganization: hasMany<OrganizationMember[]>().sync('pull'),
  invitations: hasMany<OrganizationInvitation[]>().sync('pull'),
}) {
}
