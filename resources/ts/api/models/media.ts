import MediaType from '@/ts/api/enums/mediaType';
import makeModel from '@/ts/api/makeModel';
import type User from '@/ts/api/models/user';
import { attr, hasOne } from '@foscia/core';

export type MediaVisibility = 'public' | 'private';

export default class Media extends makeModel('media', {
  visibility: attr<MediaVisibility>(),
  name: attr<string>(),
  fileName: attr<string>(),
  size: attr<number>(),
  width: attr<number | null>(),
  height: attr<number | null>(),
  mimeType: attr<string>(),
  redirectURL: attr<string>(),
  resolveURL: attr<string>(),
  originalURL: attr<string>(),
  placeholderURL: attr<string | null>(),
  optimizedURL: attr<string | null>(),
  optimizedSrcset: attr<string | null>(),
  owner: hasOne<User>(),
}) {
  public get mediaType(): MediaType {
    if (MediaType.IMAGE.props.mimeRegExp.test(this.mimeType)) {
      return MediaType.IMAGE;
    }

    if (MediaType.VIDEO.props.mimeRegExp.test(this.mimeType)) {
      return MediaType.VIDEO;
    }

    return MediaType.UNKNOWN;
  }
}
