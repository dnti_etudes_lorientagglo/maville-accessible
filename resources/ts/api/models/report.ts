import type { Reportable } from '@/ts/api/composables/reportable';
import softDeletable from '@/ts/api/composables/softDeletable';
import userOwnable from '@/ts/api/composables/userOwnable';
import makeModel from '@/ts/api/makeModel';
import type ReportType from '@/ts/api/models/reportType';
import { attr, hasOne } from '@foscia/core';

export default class Report extends makeModel('reports', {
  softDeletable,
  userOwnable,
  url: attr<string>(),
  body: attr<string | null>(),
  reportType: hasOne<ReportType>(),
  reportable: hasOne<Reportable | null>(),
}) {
}
