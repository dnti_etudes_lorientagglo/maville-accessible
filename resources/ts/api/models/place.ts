import accessible from '@/ts/api/composables/accessible';
import categorizable from '@/ts/api/composables/categorizable';
import contactable from '@/ts/api/composables/contactable';
import coverable from '@/ts/api/composables/coverable';
import imageable from '@/ts/api/composables/imageable';
import linkable from '@/ts/api/composables/linkable';
import ownable from '@/ts/api/composables/ownable';
import publishable from '@/ts/api/composables/publishable';
import reportable from '@/ts/api/composables/reportable';
import reviewable from '@/ts/api/composables/reviewable';
import sluggable from '@/ts/api/composables/sluggable';
import sourceable from '@/ts/api/composables/sourceable';
import makeModel from '@/ts/api/makeModel';
import { WeekdayOpeningHour } from '@/ts/utilities/dates/types';
import { Translation } from '@/ts/utilities/lang/translate';
import { AddressObject } from '@/ts/utilities/places/types';
import { attr } from '@foscia/core';

export default class Place extends makeModel('places', {
  accessible,
  categorizable,
  contactable,
  coverable,
  imageable,
  linkable,
  publishable,
  ownable,
  reportable,
  reviewable,
  sluggable,
  sourceable,
  name: attr<Translation>(),
  description: attr<Translation | null>(),
  openingHours: attr<WeekdayOpeningHour[] | null>(),
  address: attr<AddressObject>(),
}) {
}
