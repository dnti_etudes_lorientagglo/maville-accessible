import makeModel from '@/ts/api/makeModel';
import type Organization from '@/ts/api/models/organization';
import type Role from '@/ts/api/models/role';
import type User from '@/ts/api/models/user';
import { hasOne } from '@foscia/core';

export default class OrganizationMember extends makeModel('organization-members', {
  organization: hasOne<Organization>(),
  user: hasOne<User>(),
  role: hasOne<Role>(),
}) {
}
