import makeModel from '@/ts/api/makeModel';
import { Translation } from '@/ts/utilities/lang/translate';
import { attr } from '@foscia/core';

export default class SendingCampaignType extends makeModel('sending-campaign-types', {
  code: attr<string>(),
  name: attr<Translation>(),
}) {
}
