import softDeletable from '@/ts/api/composables/softDeletable';
import userOwnable from '@/ts/api/composables/userOwnable';
import makeModel from '@/ts/api/makeModel';
import type Activity from '@/ts/api/models/activity';
import type Organization from '@/ts/api/models/organization';
import type Place from '@/ts/api/models/place';
import type RequestType from '@/ts/api/models/requestType';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { attr, hasOne, toDateTime } from '@foscia/core';

export default class Request extends makeModel('requests', {
  softDeletable,
  userOwnable,
  url: attr<string>(),
  body: attr<string | null>(),
  data: attr<Dictionary | null>(),
  acceptedAt: attr<Date | null>(toDateTime()),
  requestType: hasOne<RequestType>(),
  requestable: hasOne<Place | Activity | Organization | null>(),
  resultable: hasOne<Organization | null>(),
}) {
}
