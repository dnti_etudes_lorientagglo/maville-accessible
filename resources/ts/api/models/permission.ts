import makeModel from '@/ts/api/makeModel';
import { Translation } from '@/ts/utilities/lang/translate';
import { attr } from '@foscia/core';

export default class Permission extends makeModel('permissions', {
  code: attr<string>(),
  name: attr<Translation>(),
}) {
  get group() {
    return this.code.split('.')[0];
  }
}
