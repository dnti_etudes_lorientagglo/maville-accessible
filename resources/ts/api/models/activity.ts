import accessible from '@/ts/api/composables/accessible';
import categorizable from '@/ts/api/composables/categorizable';
import contactable from '@/ts/api/composables/contactable';
import coverable from '@/ts/api/composables/coverable';
import imageable from '@/ts/api/composables/imageable';
import linkable from '@/ts/api/composables/linkable';
import mediable from '@/ts/api/composables/mediable';
import ownable from '@/ts/api/composables/ownable';
import pinnable from '@/ts/api/composables/pinnable';
import publishable from '@/ts/api/composables/publishable';
import reportable from '@/ts/api/composables/reportable';
import reviewable from '@/ts/api/composables/reviewable';
import sluggable from '@/ts/api/composables/sluggable';
import sourceable from '@/ts/api/composables/sourceable';
import ActivityPricingCode from '@/ts/api/enums/activityPricingCode';
import ActivityPublicCode from '@/ts/api/enums/activityPublicCode';
import makeModel from '@/ts/api/makeModel';
import ActivityType from '@/ts/api/models/activityType';
import Place from '@/ts/api/models/place';
import orderBy from '@/ts/utilities/arrays/orderBy';
import formatStandardDate from '@/ts/utilities/dates/formatStandardDate';
import parseStandardDate from '@/ts/utilities/dates/parseStandardDate';
import { DateOpeningHour, OpeningPeriod, WeekdayOpeningHour } from '@/ts/utilities/dates/types';
import { Translation } from '@/ts/utilities/lang/translate';
import { AddressObject } from '@/ts/utilities/places/types';
import { attr, hasOne, makeTransformer } from '@foscia/core';

type SerializedOpeningPeriod = {
  start: string;
  end: string;
};

type SerializedDateOpeningHour = {
  date: string;
  start: string;
  end: string;
};

const toOpeningPeriods = () => makeTransformer(
  (value: SerializedOpeningPeriod[] | null) => (
    value
      ? orderBy(
        value.map((v) => ({ start: parseStandardDate(v.start), end: parseStandardDate(v.end) })),
        ['start', 'end'],
      )
      : null
  ),
  (value: OpeningPeriod[] | null) => (
    value
      ? value.map((v) => ({ start: formatStandardDate(v.start), end: formatStandardDate(v.end) }))
      : null
  ),
);

const toDateOpeningHours = () => makeTransformer(
  (value: SerializedDateOpeningHour[] | null) => (
    value
      ? orderBy(
        value.map((v) => ({ ...v, date: parseStandardDate(v.date) })),
        ['date'],
      )
      : null
  ),
  (value: DateOpeningHour[] | null) => (
    value
      ? value.map((v) => ({ ...v, date: formatStandardDate(v.date) }))
      : null
  ),
);

export default class Activity extends makeModel('activities', {
  accessible,
  categorizable,
  contactable,
  coverable,
  imageable,
  linkable,
  mediable,
  pinnable,
  publishable,
  ownable,
  reportable,
  reviewable,
  sluggable,
  sourceable,
  name: attr<Translation>(),
  description: attr<Translation | null>(),
  body: attr<Translation | null>(),
  pricingCode: attr<string | null>(),
  pricingDescription: attr<Translation | null>(),
  publicCodes: attr<string[] | null>(),
  openingOnPeriods: attr<boolean>(),
  openingPeriods: attr<OpeningPeriod[] | null>(toOpeningPeriods()),
  openingDatesHours: attr<DateOpeningHour[] | null>(toDateOpeningHours()),
  openingWeekdaysHours: attr<WeekdayOpeningHour[] | null>(),
  address: attr<AddressObject | null>(),
  place: hasOne<Place | null>(),
  activityType: hasOne<ActivityType>(),
}) {
  get pricing() {
    return this.pricingCode
      ? ActivityPricingCode.all.get(this.pricingCode)!
      : ActivityPricingCode.UNKNOWN;
  }

  get publics() {
    const codes = this.publicCodes ?? [];

    return codes.length && !codes.some((c) => c === ActivityPublicCode.ALL.value)
      ? orderBy(
        codes.map((c) => ActivityPublicCode.all.get(c)!),
        (c) => [...ActivityPublicCode.all.keys()].indexOf(c.value),
      )
      : [ActivityPublicCode.ALL];
  }
}
