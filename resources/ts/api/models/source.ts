import makeModel from '@/ts/api/makeModel';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { attr } from '@foscia/core';

export default class Source extends makeModel('sources', {
  name: attr<string>(),
  data: attr<Dictionary>(),
}) {
}
