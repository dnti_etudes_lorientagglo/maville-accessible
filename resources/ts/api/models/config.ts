import makeModel from '@/ts/api/makeModel';
import Category from '@/ts/api/models/category';
import { useAppStore } from '@/ts/stores/app';
import { Translation } from '@/ts/utilities/lang/translate';
import { AddressObject } from '@/ts/utilities/places/types';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { attr, hasMany, onSaved } from '@foscia/core';

export default class Config extends makeModel('configs', {
  description: attr<Translation | null>(),
  services: attr<Dictionary | null>(),
  mapCenter: attr<AddressObject>(),
  mapZoom: attr<number>(),
  mapSearchableCategories: hasMany<Category[]>(),
  mapDefaultCategories: hasMany<Category[]>(),
}) {
}

onSaved(Config, () => useAppStore().markUpdated());
