import makeModel from '@/ts/api/makeModel';
import type SendingCampaignBatch from '@/ts/api/models/sendingCampaignBatch';
import type SendingCampaignType from '@/ts/api/models/sendingCampaignType';
import { Translation } from '@/ts/utilities/lang/translate';
import sumBy from '@/ts/utilities/math/sumBy';
import { attr, hasMany, hasOne, toDateTime } from '@foscia/core';

export default class SendingCampaign extends makeModel('sending-campaigns', {
  name: attr<Translation>(),
  campaignType: hasOne<SendingCampaignType>(),
  campaignBatches: hasMany<SendingCampaignBatch[]>(),
  dispatchedAt: attr(toDateTime()).nullable(),
  startedAt: attr(toDateTime()).nullable(),
  finishedAt: attr(toDateTime()).nullable(),
}) {
  get recipientsCounts() {
    return {
      pending: sumBy(this.campaignBatches, 'recipientsCounts.pending'),
      succeeded: sumBy(this.campaignBatches, 'recipientsCounts.succeeded'),
      failed: sumBy(this.campaignBatches, 'recipientsCounts.failed'),
    };
  }
}
