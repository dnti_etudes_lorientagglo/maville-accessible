import type { Reactable } from '@/ts/api/composables/reactable';
import userOwnable from '@/ts/api/composables/userOwnable';
import makeModel from '@/ts/api/makeModel';
import { attr, hasOne } from '@foscia/core';

export default class Reaction extends makeModel('reactions', {
  userOwnable,
  reaction: attr<string>(),
  reactable: hasOne<Reactable>(),
}) {
}
