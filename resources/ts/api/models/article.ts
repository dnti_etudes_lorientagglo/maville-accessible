import categorizable from '@/ts/api/composables/categorizable';
import coverable from '@/ts/api/composables/coverable';
import imageable from '@/ts/api/composables/imageable';
import localizable from '@/ts/api/composables/localizable';
import mediable from '@/ts/api/composables/mediable';
import ownable from '@/ts/api/composables/ownable';
import pinnable from '@/ts/api/composables/pinnable';
import publishable from '@/ts/api/composables/publishable';
import reportable from '@/ts/api/composables/reportable';
import sluggable from '@/ts/api/composables/sluggable';
import sourceable from '@/ts/api/composables/sourceable';
import makeModel from '@/ts/api/makeModel';
import { Translation } from '@/ts/utilities/lang/translate';
import { attr } from '@foscia/core';

export default class Article extends makeModel('articles', {
  categorizable,
  coverable,
  imageable,
  localizable,
  mediable,
  pinnable,
  publishable,
  ownable,
  reportable,
  sluggable,
  sourceable,
  name: attr<Translation>(),
  description: attr<Translation | null>(),
  body: attr<Translation | null>(),
}) {
}
