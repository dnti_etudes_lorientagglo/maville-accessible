import makeModel from '@/ts/api/makeModel';
import type Organization from '@/ts/api/models/organization';
import type Role from '@/ts/api/models/role';
import { attr, hasOne } from '@foscia/core';

export default class OrganizationInvitation extends makeModel('organization-invitations', {
  email: attr<string>(),
  organization: hasOne<Organization>(),
  role: hasOne<Role>(),
}) {
}
