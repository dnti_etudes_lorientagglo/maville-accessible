import makeModel from '@/ts/api/makeModel';
import type Permission from '@/ts/api/models/permission';
import { Translation } from '@/ts/utilities/lang/translate';
import { attr, hasMany } from '@foscia/core';

export default class Role extends makeModel('roles', {
  code: attr<string>(),
  scopeName: attr<string>(),
  name: attr<Translation>(),
  permissions: hasMany<Permission[]>(),
}) {
}
