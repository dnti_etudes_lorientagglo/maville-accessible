import reportable from '@/ts/api/composables/reportable';
import softDeletable from '@/ts/api/composables/softDeletable';
import userOwnable from '@/ts/api/composables/userOwnable';
import makeModel from '@/ts/api/makeModel';
import type Review from '@/ts/api/models/review';
import { attr, hasOne } from '@foscia/core';

export default class ReviewReply extends makeModel('review-replies', {
  reportable,
  softDeletable,
  userOwnable,
  body: attr<string>(),
  review: hasOne<Review>(),
}) {
}
