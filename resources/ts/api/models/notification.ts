import makeModel from '@/ts/api/makeModel';
import { Optional } from '@/ts/utilities/types/optional';
import { attr, toDateTime } from '@foscia/core';

export type NotificationChannel = 'mail' | 'database';

export type NotificationData = {
  title: string;
  action?: Optional<{
    href: string;
    label: string;
  }>;
};

export default class Notification extends makeModel('notifications', {
  notificationType: attr<string>(),
  data: attr<NotificationData>(),
  readAt: attr<Date | null>(toDateTime()),
}) {
}
