import accessible from '@/ts/api/composables/accessible';
import reactable from '@/ts/api/composables/reactable';
import reportable from '@/ts/api/composables/reportable';
import type { Reviewable } from '@/ts/api/composables/reviewable';
import softDeletable from '@/ts/api/composables/softDeletable';
import userOwnable from '@/ts/api/composables/userOwnable';
import makeModel from '@/ts/api/makeModel';
import type ReviewReply from '@/ts/api/models/reviewReply';
import { Translation } from '@/ts/utilities/lang/translate';
import { attr, hasMany, hasOne } from '@foscia/core';

export default class Review extends makeModel('reviews', {
  accessible,
  reactable,
  reportable,
  softDeletable,
  userOwnable,
  rating: attr<number>(),
  body: attr<Translation | null>(),
  reviewable: hasOne<Reviewable>(),
  replies: hasMany<ReviewReply[]>().sync('pull'),
}) {
}
