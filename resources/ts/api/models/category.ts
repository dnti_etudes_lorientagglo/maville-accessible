import publishable from '@/ts/api/composables/publishable';
import sluggable from '@/ts/api/composables/sluggable';
import sourceable from '@/ts/api/composables/sourceable';
import makeModel from '@/ts/api/makeModel';
import type { Translation } from '@/ts/utilities/lang/translate';
import { attr, hasMany } from '@foscia/core';

export default class Category extends makeModel('categories', {
  publishable,
  sluggable,
  sourceable,
  name: attr<Translation>(),
  description: attr<Translation | null>(),
  icon: attr<string | null>(),
  color: attr<string | null>(),
  parents: hasMany<Category[]>(),
  children: hasMany<Category[]>(),
}) {
  get isRoot() {
    return this.parents === undefined || this.parents.length === 0;
  }
}
