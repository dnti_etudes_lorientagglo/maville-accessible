import {
  Action,
  ConsumeAdapter,
  ConsumeDeserializer,
  ConsumeModel,
  DeserializedData,
  Model,
  one,
} from '@foscia/core';
import { paginate } from '@foscia/jsonapi';

export default function first<
  C extends {},
  M extends Model,
  RawData,
  Data,
  Deserialized extends DeserializedData,
>() {
  return (
    // eslint-disable-next-line max-len
    action: Action<C & ConsumeAdapter<RawData, Data> & ConsumeDeserializer<Data, Deserialized> & ConsumeModel<M>>,
  ) => action
    .use(paginate({ size: 1, number: 1 }))
    .run(one());
}
