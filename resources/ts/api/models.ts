import { Model } from '@foscia/core';

export default Object.values(import.meta.glob('./models/*.ts', {
  import: 'default', eager: true,
})) as Model[];
