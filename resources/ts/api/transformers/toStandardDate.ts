import formatStandardDate from '@/ts/utilities/dates/formatStandardDate';
import parseStandardDate from '@/ts/utilities/dates/parseStandardDate';
import { makeTransformer } from '@foscia/core';

export default () => makeTransformer(
  (date: string) => parseStandardDate(date),
  (date: Date) => formatStandardDate(date),
);
