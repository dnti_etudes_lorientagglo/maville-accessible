import action from '@/ts/api/action';
import Role from '@/ts/api/models/role';
import once from '@/ts/utilities/functions/once';
import { all, include, query } from '@foscia/core';

export default once(
  () => action()
    .use(query(Role))
    .use(include('permissions'))
    .run(all()),
);
