import action from '@/ts/api/action';
import ActivityType from '@/ts/api/models/activityType';
import once from '@/ts/utilities/functions/once';
import { all, query } from '@foscia/core';

const cachedActivityTypes = once(
  () => action()
    .use(query(ActivityType))
    .run(all()),
);

export async function findCachedActivityType(code: string) {
  const activityTypes = await cachedActivityTypes.value;

  return activityTypes.find((t) => t.code === code)!;
}

export default cachedActivityTypes;
