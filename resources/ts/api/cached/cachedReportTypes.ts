import action from '@/ts/api/action';
import ReportType from '@/ts/api/models/reportType';
import removeOne from '@/ts/utilities/arrays/removeOne';
import once from '@/ts/utilities/functions/once';
import { all, onCreated, onDestroyed, query } from '@foscia/core';

const cachedReportTypes = once(
  () => action()
    .use(query(ReportType))
    .run(all()),
);

onCreated(ReportType, async (reportType) => {
  if (cachedReportTypes.cached) {
    (await cachedReportTypes.value).push(reportType);
  }
});

onDestroyed(ReportType, async (reportType) => {
  if (cachedReportTypes.cached) {
    removeOne(await cachedReportTypes.value, reportType);
  }
});

export default cachedReportTypes;
