import action from '@/ts/api/action';
import AccessibilityCategory from '@/ts/api/enums/accessibilityCategory';
import AccessibilityCriteria from '@/ts/api/enums/accessibilityCriteria';
import AccessibilityCriteriaCategory from '@/ts/api/enums/accessibilityCriteriaCategory';
import AccessibilityCriteriaType from '@/ts/api/enums/accessibilityCriteriaType';
import once from '@/ts/utilities/functions/once';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { raw } from '@foscia/core';
import { makeGet } from '@foscia/http';

type RawAccessibilityCriteria = {
  editable: boolean;
  id: string;
  criteriaCategory: string;
  accessibilityCategories: string[];
  type: string;
  positive: boolean | null;
  metadata: Dictionary;
};

const cachedAccessibilityCriteria = once(async () => {
  const { data } = await action()
    .use(makeGet('/api/v1/-actions/accessibility/criteria'))
    .run(raw((r) => r.json()));

  return (data as RawAccessibilityCriteria[]).map((i) => new AccessibilityCriteria(
    i.editable,
    i.id,
    AccessibilityCriteriaCategory.all.get(i.criteriaCategory)!,
    i.accessibilityCategories.map((c) => AccessibilityCategory.all.get(c)!),
    AccessibilityCriteriaType.all.get(i.type)!,
    i.positive,
    i.metadata,
  ));
});

export default cachedAccessibilityCriteria;
