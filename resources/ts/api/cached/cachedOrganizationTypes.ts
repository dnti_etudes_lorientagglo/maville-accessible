import action from '@/ts/api/action';
import OrganizationType from '@/ts/api/models/organizationType';
import once from '@/ts/utilities/functions/once';
import { all, query } from '@foscia/core';

export default once(
  () => action()
    .use(query(OrganizationType))
    .run(all()),
);
