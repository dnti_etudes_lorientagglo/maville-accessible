import action from '@/ts/api/action';
import RequestType from '@/ts/api/models/requestType';
import once from '@/ts/utilities/functions/once';
import { all, query } from '@foscia/core';

const cachedRequestTypes = once(
  () => action()
    .use(query(RequestType))
    .run(all()),
);

export async function findCachedRequestType(code: string) {
  const requestTypes = await cachedRequestTypes.value;

  return requestTypes.find((t) => t.code === code)!;
}

export default cachedRequestTypes;
