import cachedRoles from '@/ts/api/cached/cachedRoles';

export default async function cachedScopedRole(...scopes: string[]) {
  return (await cachedRoles.value).filter((r) => scopes.indexOf(r.scopeName) !== -1);
}
