import action from '@/ts/api/action';
import Category from '@/ts/api/models/category';
import once from '@/ts/utilities/functions/once';
import { all, include, onDestroyed, onSaved, query } from '@foscia/core';

const cachedCategories = once(
  () => action()
    .use(query(Category))
    .use(include('parents', 'children'))
    .run(all()),
);

onSaved(Category, () => cachedCategories.forget());
onDestroyed(Category, () => cachedCategories.forget());

export default cachedCategories;
