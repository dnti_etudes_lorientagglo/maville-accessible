import { Translation } from '@/ts/utilities/lang/translate';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { attr, makeComposable, ModelInstanceUsing } from '@foscia/core';

export type AccessibilityValue = {
  rating?: number | null;
  body?: Translation | null;
};

export type AccessibilityObject = {
  criteria?: Dictionary | null;
} & Dictionary<AccessibilityValue>;

const accessible = makeComposable({
  accessibility: attr<AccessibilityObject | null>(),
});

export type Accessible = ModelInstanceUsing<typeof accessible>;

export default accessible;
