import type Organization from '@/ts/api/models/organization';
import { hasOne, makeComposable, ModelInstanceUsing } from '@foscia/core';

const organizationOwnable = makeComposable({
  ownerOrganization: hasOne<Organization | null>(),
});

export type OrganizationOwnable = ModelInstanceUsing<typeof organizationOwnable>;

export default organizationOwnable;
