import { attr, makeComposable, ModelInstanceUsing, toDateTime } from '@foscia/core';

const pinnable = makeComposable({
  pinnedAt: attr(toDateTime()).nullable(),
  get pinned() {
    return !!this.pinnedAt;
  },
});

export type Pinnable = ModelInstanceUsing<typeof pinnable>;

export default pinnable;
