import type User from '@/ts/api/models/user';
import { hasOne, makeComposable, ModelInstanceUsing } from '@foscia/core';

const userOwnable = makeComposable({
  ownerUser: hasOne<User | null>(),
});

export type UserOwnable = ModelInstanceUsing<typeof userOwnable>;

export default userOwnable;
