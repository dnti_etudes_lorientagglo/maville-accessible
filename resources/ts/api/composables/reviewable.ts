import type Review from '@/ts/api/models/review';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { attr, hasMany, makeComposable, ModelInstanceUsing } from '@foscia/core';

export type ReviewsAccessibilitySummary = Dictionary<{
  rating: number | null;
  total: number;
  details: number;
}>;

export type ReviewsSummary = {
  total: number;
  rating: number;
  distribution: {
    1: number;
    2: number;
    3: number;
    4: number;
    5: number;
  },
  accessibility: ReviewsAccessibilitySummary;
};

const reviewable = makeComposable({
  reviewsSummary: attr<ReviewsSummary | null>().readOnly().sync('pull'),
  reviews: hasMany<Review[]>().readOnly().sync('pull'),
});

export type Reviewable = ModelInstanceUsing<typeof reviewable>;

export default reviewable;
