import type Reaction from '@/ts/api/models/reaction';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { attr, hasOne, makeComposable, ModelInstanceUsing } from '@foscia/core';

export type ReactionsSummary = Dictionary<number>;

const reactable = makeComposable({
  reactionsSummary: attr<ReactionsSummary | null>(),
  currentReaction: hasOne<Reaction | null>(),
});

export type Reactable = ModelInstanceUsing<typeof reactable>;

export default reactable;
