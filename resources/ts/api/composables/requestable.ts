import { makeComposable, ModelInstanceUsing } from '@foscia/core';

const requestable = makeComposable({});

export type Requestable = ModelInstanceUsing<typeof requestable>;

export default requestable;
