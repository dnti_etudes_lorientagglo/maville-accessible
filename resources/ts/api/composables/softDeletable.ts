import { attr, makeComposable, ModelInstanceUsing, toDateTime } from '@foscia/core';

const softDeletable = makeComposable({
  deletedAt: attr<Date | null>(toDateTime()),
  get deleted() {
    return !!this.deletedAt;
  },
});

export type SoftDeletable = ModelInstanceUsing<typeof softDeletable>;

export default softDeletable;
