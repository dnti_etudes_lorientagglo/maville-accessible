import Category from '@/ts/api/models/category';
import { hasMany, makeComposable, ModelInstanceUsing } from '@foscia/core';

const categorizable = makeComposable({
  categories: hasMany<Category[]>(),
});

export type Categorizable = ModelInstanceUsing<typeof categorizable>;

export default categorizable;
