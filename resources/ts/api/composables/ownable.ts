import organizationOwnable from '@/ts/api/composables/organizationOwnable';
import userOwnable from '@/ts/api/composables/userOwnable';
import { makeComposable, ModelInstanceUsing } from '@foscia/core';

const ownable = makeComposable({
  userOwnable,
  organizationOwnable,
});

export type Ownable = ModelInstanceUsing<typeof ownable>;

export default ownable;
