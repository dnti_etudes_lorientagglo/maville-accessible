import type Media from '@/ts/api/models/media';
import { hasOne, makeComposable, ModelInstanceUsing } from '@foscia/core';

const coverable = makeComposable({
  cover: hasOne<Media | null>(),
});

export type Coverable = ModelInstanceUsing<typeof coverable>;

export default coverable;
