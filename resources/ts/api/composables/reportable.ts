import { makeComposable, ModelInstanceUsing } from '@foscia/core';

const reportable = makeComposable({});

export type Reportable = ModelInstanceUsing<typeof reportable>;

export default reportable;
