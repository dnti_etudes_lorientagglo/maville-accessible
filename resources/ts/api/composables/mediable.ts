import type Media from '@/ts/api/models/media';
import { hasMany, makeComposable, ModelInstanceUsing } from '@foscia/core';

const mediable = makeComposable({
  media: hasMany<Media[]>(),
});

export type Mediable = ModelInstanceUsing<typeof mediable>;

export default mediable;
