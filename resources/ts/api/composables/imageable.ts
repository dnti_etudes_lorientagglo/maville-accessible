import type Media from '@/ts/api/models/media';
import { hasMany, makeComposable, ModelInstanceUsing } from '@foscia/core';

const imageable = makeComposable({
  images: hasMany<Media[]>(),
});

export type Imageable = ModelInstanceUsing<typeof imageable>;

export default imageable;
