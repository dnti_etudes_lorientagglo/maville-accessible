import type Permission from '@/ts/api/models/permission';
import type Role from '@/ts/api/models/role';
import { hasMany, makeComposable, ModelInstanceUsing } from '@foscia/core';

const authorizable = makeComposable({
  roles: hasMany<Role[]>(),
  permissions: hasMany<Permission[]>(),
});

export type Authorizable = ModelInstanceUsing<typeof authorizable>;

export default authorizable;
