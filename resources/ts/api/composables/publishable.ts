import isNone from '@/ts/utilities/common/isNone';
import { attr, makeComposable, ModelInstanceUsing, toDateTime } from '@foscia/core';

const publishable = makeComposable({
  publishedAt: attr<Date | null>(toDateTime()),
  get published() {
    return !isNone(this.publishedAt);
  },
});

export type Publishable = ModelInstanceUsing<typeof publishable>;

export default publishable;
