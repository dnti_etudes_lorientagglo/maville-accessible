import { attr, makeComposable, ModelInstanceUsing, ModelValues } from '@foscia/core';

const contactable = makeComposable({
  email: attr<string | null>(),
  phone: attr<string | null>(),
});

export type Contactable = ModelInstanceUsing<typeof contactable>;

export type ContactableValues = Omit<ModelValues<Contactable>, 'id' | 'lid'>;

export default contactable;
