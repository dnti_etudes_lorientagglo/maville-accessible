import type Place from '@/ts/api/models/place';
import { hasMany, makeComposable, ModelInstanceUsing } from '@foscia/core';

const localizable = makeComposable({
  places: hasMany<Place[]>(),
});

export type Localizable = ModelInstanceUsing<typeof localizable>;

export default localizable;
