import { attr, makeComposable, ModelInstanceUsing } from '@foscia/core';

const sluggable = makeComposable({
  slug: attr<string>(),
});

export type Sluggable = ModelInstanceUsing<typeof sluggable>;

export default sluggable;
