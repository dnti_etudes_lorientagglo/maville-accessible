import publishable from '@/ts/api/composables/publishable';
import type { BaseModelInstance } from '@/ts/api/makeModel';
import type Source from '@/ts/api/models/source';
import isNil from '@/ts/utilities/common/isNil';
import {
  attr,
  hasMany,
  isInstanceUsing,
  makeComposable,
  ModelInstanceUsing,
  toDateTime,
} from '@foscia/core';

const sourceable = makeComposable({
  sourcedAt: attr<Date | null>(toDateTime()),
  sources: hasMany<Source[]>(),
  get sourced() {
    return !isNil(this.sourcedAt) && (isNil(this.sources) || this.sources.length > 0);
  },
  get isSyncedWithSources() {
    const createdAtTime = (this as unknown as BaseModelInstance).createdAt.getTime();
    const updatedAtTime = (this as unknown as BaseModelInstance).updatedAt.getTime();

    return !isNil(this.sourcedAt)
      && this.sourcedAt.getTime() === updatedAtTime
      && (
        !isInstanceUsing(this, publishable)
        || (
          !isNil(this.publishedAt)
          && this.publishedAt.getTime() === createdAtTime
        )
      );
  },
});

export type Sourceable = ModelInstanceUsing<typeof sourceable>;

export default sourceable;
