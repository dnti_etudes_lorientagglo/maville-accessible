import { attr, makeComposable, ModelInstanceUsing, ModelValues } from '@foscia/core';

export type LinkableLinks = {
  website?: string;
  facebook?: string;
  instagram?: string;
  linkedin?: string;
  youtube?: string;
};

const linkable = makeComposable({
  links: attr<LinkableLinks | null>(),
});

export type Linkable = ModelInstanceUsing<typeof linkable>;

export type LinkableValues = Omit<ModelValues<Linkable>, 'id' | 'lid'>;

export default linkable;
