import {
  attr,
  id,
  makeModelFactory,
  ModelInstance,
  ModelParsedDefinition,
  RawModelAttribute,
  RawModelId,
  toDateTime,
} from '@foscia/core';

export type BaseModelInstance = ModelInstance<ModelParsedDefinition<{
  id: RawModelId<string>;
  createdAt: RawModelAttribute<Date>;
  updatedAt: RawModelAttribute<Date>;
  searchScore: RawModelAttribute<number | null>;
  allows: RawModelAttribute<string[]>;
}>>;

export default makeModelFactory({}, {
  id: id<string>(),
  createdAt: attr(toDateTime()),
  updatedAt: attr(toDateTime()),
  searchScore: attr<number | null>(),
  allows: attr<string[]>(),
});
