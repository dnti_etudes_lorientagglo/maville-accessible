import Category from '@/ts/api/models/category';

export default function onlyPublishedCategories(categories: Category[]) {
  return categories.filter((category) => (
    category.published
    && (!category.parents?.length || category.parents.some((p) => p.published))
  ));
}
