import type { Categorizable } from '@/ts/api/composables/categorizable';
import type { ModelInstance } from '@foscia/core';

export default function isCategorizable(instance: ModelInstance): instance is Categorizable {
  return 'categories' in instance;
}
