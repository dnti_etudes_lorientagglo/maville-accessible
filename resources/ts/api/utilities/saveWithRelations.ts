import action from '@/ts/api/action';
import fillAndSave from '@/ts/api/utilities/fillAndSave';
import hardDelete from '@/ts/api/utilities/hardDelete';
import wrap from '@/ts/utilities/arrays/wrap';
import { Arrayable } from '@/ts/utilities/types/arrayable';
import { Awaitable } from '@/ts/utilities/types/awaitable';
import { changed, ModelInstance, oneOrCurrent, save, when } from '@foscia/core';

export type SaveWithRelationsOptions = {
  prepareRelated?: (
    context: { instance: ModelInstance, relation: string, related: ModelInstance },
  ) => Awaitable<void>;
};

export default async function saveWithRelations(
  instance: ModelInstance,
  relations: Arrayable<string>,
  options: SaveWithRelationsOptions = {},
) {
  const relationsValues = wrap(relations).map((relation) => ({
    relation,
    prev: [...wrap(instance.$original.$values[relation])],
    next: [...wrap(instance.$values[relation])],
  }));

  await fillAndSave(instance, {});

  await Promise.all(relationsValues.map(async (relation) => {
    await Promise.all(relation.next.map(
      (nextModel) => action()
        .use(when(() => options.prepareRelated, async (_, callback) => {
          await callback({
            instance, relation: relation.relation, related: nextModel,
          });
        }))
        .use(save(nextModel))
        .run(when(!nextModel.$exists || changed(nextModel), oneOrCurrent(), () => nextModel)),
    ));

    await Promise.all(
      relation.prev
        .filter((prevModel) => relation.next.indexOf(prevModel) === -1)
        .map((prevModel) => hardDelete(prevModel)),
    );
  }));
}
