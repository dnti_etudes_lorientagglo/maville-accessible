import AccessibilityCategory from '@/ts/api/enums/accessibilityCategory';
import { useAccessibilityStore } from '@/ts/stores/accessibility';
import { Action } from '@foscia/core';
import { sortByDesc } from '@foscia/jsonapi';

export default function sortByAccessibility() {
  return (action: Action<any, any>) => {
    const accessibility = useAccessibilityStore();
    const sorts = accessibility.preferences.prioritizeCategories
      .filter((category) => AccessibilityCategory.all.has(category))
      .map((category) => `${category}-accessibility`);

    if (sorts.length) {
      action.use(sortByDesc(sorts));
    }
  };
}
