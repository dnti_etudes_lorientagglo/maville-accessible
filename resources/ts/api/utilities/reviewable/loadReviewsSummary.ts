import action from '@/ts/api/action';
import { Reviewable } from '@/ts/api/composables/reviewable';
import withReviewsSummary from '@/ts/api/enhancers/withReviewsSummary';
import { one, query } from '@foscia/core';
import { fields } from '@foscia/jsonapi';

export default async function loadReviewsSummary(reviewable: Reviewable) {
  await action()
    .use(query(reviewable))
    .use(withReviewsSummary())
    .use(fields('reviewsSummary'))
    .run(one());
}
