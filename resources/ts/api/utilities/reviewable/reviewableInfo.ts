import { Reviewable } from '@/ts/api/composables/reviewable';
import Activity from '@/ts/api/models/activity';
import Place from '@/ts/api/models/place';
import translate from '@/ts/utilities/lang/translate';

export default function reviewableInfo(instance: Reviewable) {
  if (instance instanceof Place) {
    return {
      title: translate(instance.name),
      to: `/places/map/reviews/${instance.slug ?? instance.id}`,
    };
  }

  if (instance instanceof Activity) {
    return {
      title: translate(instance.name),
      to: `/activities/read/${instance.slug ?? instance.id}`,
    };
  }

  throw new Error('invalid instance given');
}
