import action from '@/ts/api/action';
import { destroy, ModelInstance, none } from '@foscia/core';

export default function hardDelete(instance: ModelInstance) {
  return action()
    .use(destroy(instance))
    .run(none());
}
