import { Linkable } from '@/ts/api/composables/linkable';
import excludeNone from '@/ts/utilities/objects/excludeNone';

export default function hasLinkableLinks(linkable: Linkable) {
  return Object.keys(excludeNone(linkable.links ?? {})).length > 0;
}
