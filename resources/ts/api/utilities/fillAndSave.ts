import action from '@/ts/api/action';
import {
  Action,
  changed,
  ConsumeModel,
  fill,
  Model,
  ModelInstance,
  ModelValues,
  oneOrCurrent,
  restore,
  save,
  when,
} from '@foscia/core';

export default async function fillAndSave<D extends {}, I extends ModelInstance<D>>(
  instance: ModelInstance<D> & I,
  values: Partial<ModelValues<I>>,
  tap?: (action: Action<ConsumeModel<Model<D, I>>>) => void,
) {
  fill(instance, values);

  try {
    return await action()
      .use(save(instance))
      .use(when(() => tap, (a, t) => t(a as Action<ConsumeModel<Model<D, I>>>)))
      .run(when(
        !instance.$exists || changed(instance),
        oneOrCurrent(),
        () => instance,
      ));
  } catch (error) {
    restore(instance);

    throw error;
  }
}
