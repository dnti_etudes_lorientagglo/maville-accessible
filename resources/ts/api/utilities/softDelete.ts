import { SoftDeletable } from '@/ts/api/composables/softDeletable';
import fillAndSave from '@/ts/api/utilities/fillAndSave';

export default function softDelete(instance: SoftDeletable) {
  return fillAndSave(instance, { deletedAt: new Date() });
}
