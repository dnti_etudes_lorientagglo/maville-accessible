import Activity from '@/ts/api/models/activity';
import orderBy from '@/ts/utilities/arrays/orderBy';
import partition from '@/ts/utilities/arrays/partition';
import uniqBy from '@/ts/utilities/arrays/uniqBy';
import formatStandardDate from '@/ts/utilities/dates/formatStandardDate';
import withHours from '@/ts/utilities/dates/withHours';

export default function computeActivityPeriods(activity: Activity, past?: boolean) {
  const [beforeNow, afterNow] = partition(
    uniqBy(
      activity.openingPeriods ?? [],
      ({ start, end }) => `${formatStandardDate(start)}${formatStandardDate(end)}`,
    ),
    ({ end }) => withHours(end) < withHours(new Date()),
  );

  const periods = [
    orderBy(beforeNow, ['start', 'end'], ['desc', 'desc']),
    orderBy(afterNow, ['start', 'end']),
  ];

  return past ? periods.reverse() : periods;
}
