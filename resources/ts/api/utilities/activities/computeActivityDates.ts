import Activity from '@/ts/api/models/activity';
import orderBy from '@/ts/utilities/arrays/orderBy';
import partition from '@/ts/utilities/arrays/partition';
import uniqBy from '@/ts/utilities/arrays/uniqBy';
import formatStandardDate from '@/ts/utilities/dates/formatStandardDate';
import withHours from '@/ts/utilities/dates/withHours';

export default function computeActivityDates(
  activity: Activity,
  uniqByDay: boolean,
  past?: boolean,
) {
  const [beforeNow, afterNow] = partition(
    uniqBy(
      activity.openingDatesHours ?? [],
      ({ date, start, end }) => (
        uniqByDay ? formatStandardDate(date) : `${formatStandardDate(date)}${start}${end}`
      ),
    ),
    ({ date }) => withHours(date) < withHours(new Date()),
  );

  const dates = [
    orderBy(beforeNow, 'date', 'desc'),
    orderBy(afterNow, 'date'),
  ];

  return past ? dates.reverse() : dates;
}
