import { Publishable } from '@/ts/api/composables/publishable';
import Activity from '@/ts/api/models/activity';
import Article from '@/ts/api/models/article';
import Place from '@/ts/api/models/place';
import { RouteLocationRaw } from 'vue-router';

export default function publishableRoute(
  publishable: Publishable,
): RouteLocationRaw | undefined {
  if (!publishable.published) {
    return undefined;
  }

  if (publishable instanceof Place) {
    return {
      name: 'places.map',
      params: { place: publishable.slug, view: 'details' },
    };
  }

  if (publishable instanceof Article) {
    return {
      name: 'articles.read',
      params: { article: publishable.slug },
    };
  }

  if (publishable instanceof Activity) {
    return {
      name: `${publishable.activityType.code}.read`,
      params: { activity: publishable.slug },
    };
  }

  throw new Error('invalid instance given');
}
