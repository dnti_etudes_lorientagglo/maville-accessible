import { Publishable } from '@/ts/api/composables/publishable';
import ActivityTypeCode from '@/ts/api/enums/activityTypeCode';
import Activity from '@/ts/api/models/activity';
import Article from '@/ts/api/models/article';
import Place from '@/ts/api/models/place';
import {
  mdiCalendarRangeOutline,
  mdiMapOutline,
  mdiNewspaperVariantOutline,
  mdiViewGridOutline,
} from '@mdi/js';

export default function publishableIcon(publishable: Publishable) {
  if (publishable instanceof Place) {
    return mdiMapOutline;
  }

  if (publishable instanceof Article) {
    return mdiNewspaperVariantOutline;
  }

  if (publishable instanceof Activity) {
    return ActivityTypeCode.ACTIVITIES.match(publishable.activityType?.code)
      ? mdiViewGridOutline
      : mdiCalendarRangeOutline;
  }

  throw new Error('invalid instance given');
}
