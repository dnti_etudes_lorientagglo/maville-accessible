import { Publishable } from '@/ts/api/composables/publishable';

export default function onlyPublished<I extends Publishable>(items: I[]) {
  return items.filter((item) => item.published);
}
