import { Categorizable } from '@/ts/api/composables/categorizable';
import { Publishable } from '@/ts/api/composables/publishable';

export default function categorizablePublished(
  publishable: Publishable & Categorizable,
) {
  return publishable.published && (publishable.categories ?? []).length > 0;
}
