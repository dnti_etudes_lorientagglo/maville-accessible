import action from '@/ts/api/action';
import Article from '@/ts/api/models/article';
import Place from '@/ts/api/models/place';
import makeNotFoundError from '@/ts/errors/redirectable/makeNotFoundError';
import isNone from '@/ts/utilities/common/isNone';
import { oneOrFail, query } from '@foscia/core';
import { filterBy } from '@foscia/jsonapi';

export default async function publishableFind(type: unknown, id: unknown) {
  if (isNone(type) && isNone(id)) {
    return null;
  }

  if (typeof type !== 'string' || typeof id !== 'string') {
    throw makeNotFoundError();
  }

  try {
    if (type === Place.$type) {
      return await action()
        .use(query(Place))
        .use(filterBy('slug', id))
        .run(oneOrFail());
    }

    if (type === Article.$type) {
      return await action()
        .use(query(Article))
        .use(filterBy('slug', id))
        .run(oneOrFail());
    }
  } catch {
    throw makeNotFoundError();
  }

  throw new Error('invalid type given');
}
