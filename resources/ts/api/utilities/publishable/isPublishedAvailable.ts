import { Publishable } from '@/ts/api/composables/publishable';
import isCategorizable from '@/ts/api/utilities/categories/isCategorizable';

export default function isPublishedAvailable(publishable: Publishable): boolean {
  return publishable.published && (
    !isCategorizable(publishable)
    || !publishable.categories
    || publishable.categories.length === 0
    || publishable.categories.some((c) => c.published)
  );
}
