import { OrganizationOwnable } from '@/ts/api/composables/organizationOwnable';

export default function isOrganizationOwned(ownable: OrganizationOwnable) {
  return !!ownable.ownerOrganization?.publishedAt;
}
