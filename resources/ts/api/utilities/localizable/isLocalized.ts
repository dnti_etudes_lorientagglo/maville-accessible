import { Localizable } from '@/ts/api/composables/localizable';

export default function isLocalized(localizable: Localizable) {
  return (localizable.places ?? []).some((p) => p.publishedAt);
}
