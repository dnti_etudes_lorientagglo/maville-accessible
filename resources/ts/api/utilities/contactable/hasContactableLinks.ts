import { Contactable } from '@/ts/api/composables/contactable';
import isNone from '@/ts/utilities/common/isNone';

export default function hasContactableLinks(contactable: Contactable) {
  return !isNone(contactable.phone) || !isNone(contactable.email);
}
