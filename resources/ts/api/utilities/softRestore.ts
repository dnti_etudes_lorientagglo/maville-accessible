import { SoftDeletable } from '@/ts/api/composables/softDeletable';
import fillAndSave from '@/ts/api/utilities/fillAndSave';

export default function softRestore(instance: SoftDeletable) {
  return fillAndSave(instance, { deletedAt: null });
}
