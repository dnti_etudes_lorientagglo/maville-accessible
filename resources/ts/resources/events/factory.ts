import ActivityTypeCode from '@/ts/api/enums/activityTypeCode';
import activitiesResource from '@/ts/resources/activities/activitiesResource';

export default activitiesResource.makeFactory(ActivityTypeCode.EVENTS);
