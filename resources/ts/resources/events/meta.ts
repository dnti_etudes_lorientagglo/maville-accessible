import Activity from '@/ts/api/models/activity';
import MainFeature from '@/ts/utilities/features/mainFeature';
import translate from '@/ts/utilities/lang/translate';

export default {
  type: 'events',
  modelType: 'activities',
  policyType: 'activities',
  icon: MainFeature.EVENTS.props.icon,
  color: MainFeature.EVENTS.props.color,
  title: (a: Activity) => translate(a.name),
};
