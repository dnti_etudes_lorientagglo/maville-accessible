import withReviewsSummary from '@/ts/api/enhancers/withReviewsSummary';
import Place from '@/ts/api/models/place';
import makeResourceFinder from '@/ts/resources/utilities/makeResourceFinder';
import { include } from '@foscia/core';

export default makeResourceFinder(Place, (a) => {
  a
    .use(withReviewsSummary())
    .use(include([
      'ownerOrganization.cover',
      'ownerUser',
      'categories',
      'cover',
      'images',
      'sources',
    ]));
});
