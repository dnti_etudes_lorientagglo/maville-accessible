import Place from '@/ts/api/models/place';
import MainFeature from '@/ts/utilities/features/mainFeature';
import translate from '@/ts/utilities/lang/translate';

export default {
  type: 'places',
  icon: MainFeature.PLACES.props.icon,
  color: MainFeature.PLACES.props.color,
  title: (p: Place) => translate(p.name),
};
