import Place from '@/ts/api/models/place';
import makeResourceFetcher from '@/ts/resources/utilities/makeResourceFetcher';
import { include } from '@foscia/core';
import { filterBy } from '@foscia/jsonapi';

export default makeResourceFetcher(Place, (a) => {
  a.use(
    filterBy('private', true),
    include('categories'),
  );
});
