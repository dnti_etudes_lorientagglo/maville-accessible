import SendingCampaign from '@/ts/api/models/sendingCampaign';
import makeResourceFinder from '@/ts/resources/utilities/makeResourceFinder';
import { include } from '@foscia/core';

export default makeResourceFinder(SendingCampaign, (a) => {
  a.use(include([
    'campaignType',
    'campaignBatches',
  ]));
});
