import { ResourceMeta } from '@/ts/composables/resources/core/useResourceMeta';
import makeResourceEntry from '@/ts/resources/utilities/makeResourceEntry';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';

export default function entries(meta: ResourceMeta) {
  if (hasPermissionOn('sending-campaigns.showAny')) {
    return [makeResourceEntry(meta, { group: 'communication', position: 100 })];
  }

  return [];
}
