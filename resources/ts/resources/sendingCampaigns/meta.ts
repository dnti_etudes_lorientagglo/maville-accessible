import SendingCampaign from '@/ts/api/models/sendingCampaign';
import translate from '@/ts/utilities/lang/translate';
import { mdiMessageFastOutline } from '@mdi/js';

export default {
  type: 'sending-campaigns',
  icon: mdiMessageFastOutline,
  color: 'primary',
  title: (c: SendingCampaign) => translate(c.name),
};
