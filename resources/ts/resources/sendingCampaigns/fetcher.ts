import SendingCampaign from '@/ts/api/models/sendingCampaign';
import makeResourceFetcher from '@/ts/resources/utilities/makeResourceFetcher';
import { include } from '@foscia/core';

export default makeResourceFetcher(SendingCampaign, (a) => {
  a.use(include([
    'campaignType',
    'campaignBatches',
  ]));
});
