import Request from '@/ts/api/models/request';
import { i18nInstance } from '@/ts/plugins/i18n';
import fullNameWithYou from '@/ts/utilities/lang/fullNameWithYou';
import translate from '@/ts/utilities/lang/translate';
import makeAnonymousUser from '@/ts/utilities/users/makeAnonymousUser';
import { mdiCommentQuestionOutline } from '@mdi/js';

export default {
  type: 'requests',
  icon: mdiCommentQuestionOutline,
  color: 'error',
  title: (r: Request) => i18nInstance.t('resources.requests.title', {
    type: translate(r.requestType.name),
    name: fullNameWithYou(r.ownerUser ?? makeAnonymousUser()),
  }),
};
