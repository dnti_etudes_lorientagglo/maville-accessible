import { ResourceMeta } from '@/ts/composables/resources/core/useResourceMeta';
import makeResourceEntry from '@/ts/resources/utilities/makeResourceEntry';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';

export default function entries(meta: ResourceMeta) {
  if (hasPermissionOn('requests.showAny')) {
    return [makeResourceEntry(meta, { group: 'moderation', position: 200 })];
  }

  return [];
}
