import Request from '@/ts/api/models/request';
import makeResourceFinder from '@/ts/resources/utilities/makeResourceFinder';
import { include } from '@foscia/core';

export default makeResourceFinder(Request, (a) => {
  a.use(include([
    'requestType',
    'ownerUser',
    'requestable.ownerOrganization',
    'requestable.activityType',
    'resultable',
  ]));
});
