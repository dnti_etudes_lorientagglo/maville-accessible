import Request from '@/ts/api/models/request';
import makeResourceFetcher from '@/ts/resources/utilities/makeResourceFetcher';
import { include } from '@foscia/core';

export default makeResourceFetcher(Request, (a) => {
  a.use(include([
    'requestType',
    'ownerUser',
  ]));
});
