import Article from '@/ts/api/models/article';
import MainFeature from '@/ts/utilities/features/mainFeature';
import translate from '@/ts/utilities/lang/translate';

export default {
  type: 'articles',
  icon: MainFeature.ARTICLES.props.icon,
  color: MainFeature.ARTICLES.props.color,
  title: (a: Article) => translate(a.name),
};
