import Article from '@/ts/api/models/article';
import makeResourceFetcher from '@/ts/resources/utilities/makeResourceFetcher';
import { include } from '@foscia/core';
import { filterBy } from '@foscia/jsonapi';

export default makeResourceFetcher(Article, (a) => {
  a.use(
    filterBy('private', true),
    include('categories'),
  );
});
