import { ResourceMeta } from '@/ts/composables/resources/core/useResourceMeta';
import { i18nInstance } from '@/ts/plugins/i18n';
import makeResourceEntry from '@/ts/resources/utilities/makeResourceEntry';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';

export default function entries(meta: ResourceMeta) {
  if (hasPermissionOn('articles.admin') && hasPermissionOn('articles.showAny')) {
    return makeResourceEntry(meta, { group: 'admin', position: 100 });
  }

  if (hasPermissionOn('articles.showAny')) {
    return makeResourceEntry(meta, {
      title: i18nInstance.t('navigation.items.myArticles'),
      group: 'myContents',
      position: 100,
    });
  }

  return [];
}
