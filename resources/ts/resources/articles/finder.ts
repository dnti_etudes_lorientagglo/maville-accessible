import Article from '@/ts/api/models/article';
import makeResourceFinder from '@/ts/resources/utilities/makeResourceFinder';
import { include } from '@foscia/core';

export default makeResourceFinder(Article, (a) => {
  a.use(include([
    'ownerOrganization',
    'ownerUser',
    'media',
    'categories',
    'cover',
    'images',
    'places.categories.parents',
    'sources',
  ]));
});
