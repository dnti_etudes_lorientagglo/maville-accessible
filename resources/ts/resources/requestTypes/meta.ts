import RequestType from '@/ts/api/models/requestType';
import translate from '@/ts/utilities/lang/translate';
import { mdiTagMultipleOutline } from '@mdi/js';

export default {
  type: 'request-types',
  icon: mdiTagMultipleOutline,
  color: 'error',
  title: (t: RequestType) => translate(t.name),
};
