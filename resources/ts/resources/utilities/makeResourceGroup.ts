import { i18nInstance } from '@/ts/plugins/i18n';
import { ResourceEntry } from '@/ts/resources/utilities/makeResourceEntry';

export type ResourceGroup = {
  name: string;
  position: number;
  icon: string;
  color: string;
  title: string;
  entries: ResourceEntry[];
};

export type ResourceGroupOptions = {
  position: number;
  icon: string;
  color: string;
};

export default function makeResourceGroup(name: string, options: ResourceGroupOptions) {
  return {
    [name]: {
      name,
      position: options.position,
      title: i18nInstance.t(`resources.common.groups.${name}`),
      icon: options.icon,
      color: options.color,
      entries: [],
    },
  };
}
