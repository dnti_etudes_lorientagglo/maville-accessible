import { Guard } from '@/ts/router/guards/makeGuard';
import wrap from '@/ts/utilities/arrays/wrap';
import { Arrayable } from '@/ts/utilities/types/arrayable';
import { RouteRecordRaw } from 'vue-router';

export type ResourceRoute = RouteRecordRaw | ((type: string) => RouteRecordRaw);

export default function makeResourceRoutes(
  type: string,
  routes: Arrayable<ResourceRoute>,
  guards?: Arrayable<Guard>,
) {
  const resolvedRoutes = wrap(routes).map((r) => {
    const route = typeof r === 'function' ? r(type) : r;

    return {
      ...route,
      meta: {
        ...route.meta,
        guards: [
          ...wrap(route.meta?.guards),
          ...wrap(guards),
        ],
      },
    };
  });

  return {
    path: `${type}`,
    children: [
      {
        path: '',
        redirect: `/resources/${type}/${resolvedRoutes[0].path}`,
      },
      ...resolvedRoutes,
    ],
  };
}
