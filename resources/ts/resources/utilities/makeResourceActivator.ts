import { ModelInstance } from '@foscia/core';

export type ActivatorConditionContext = {
  type: string;
  instance?: ModelInstance;
  onSingle?: boolean;
};

export type ResourceActivator = {
  component: any;
  condition: (context: ActivatorConditionContext) => boolean;
};

export default function makeResourceActivator(
  component: any,
  condition: (context: ActivatorConditionContext) => boolean = () => true,
): ResourceActivator {
  return {
    component,
    condition,
  };
}
