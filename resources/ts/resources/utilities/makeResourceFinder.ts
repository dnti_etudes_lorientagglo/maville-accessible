import action from '@/ts/api/action';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';
import {
  Action,
  cachedOr,
  ConsumeModel,
  Model,
  ModelIdType,
  oneOrFail,
  query,
  when,
} from '@foscia/core';

type ResourceFinderOptions = {
  noCache?: boolean;
};

export default function makeResourceFinder<M extends Model>(
  modelToUse: M,
  tap?: (action: Action<ConsumeModel<M>>) => void,
  options: ResourceFinderOptions = {},
) {
  return async (id: ModelIdType) => {
    const makeAction = () => action()
      .use(query(modelToUse, id))
      .use(when(() => tap, (a, t) => t(a as Action<ConsumeModel<M>>)));

    if (!options.noCache) {
      const cachedInstance = await makeAction().run(cachedOr(() => null));
      if (cachedInstance && hasPermissionOn('show', cachedInstance)) {
        return cachedInstance;
      }
    }

    return makeAction().run(oneOrFail());
  };
}
