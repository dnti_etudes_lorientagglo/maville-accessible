import resourcesViews from '@/ts/views/resources/resourcesViews';

export default function makeResourceRouteFactory(path: string) {
  const action = path.replace('/:instance', '');

  return (type: string) => ({
    path,
    component: resourcesViews[`${type}-${action}`],
    meta: { resource: type },
  });
}
