import action from '@/ts/api/action';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { Action, all, ConsumeModel, Model, query, when } from '@foscia/core';
import { param } from '@foscia/http';
import { filterBy, paginate, sortBy, sortByDesc, usingDocument } from '@foscia/jsonapi';

export default function makeResourceFetcher<M extends Model>(
  modelToUse: M,
  tap?: (action: Action<ConsumeModel<M>>) => void,
) {
  return (queryParams: Dictionary) => {
    const { page, search, filters, sort } = queryParams;

    return action()
      .use(query(modelToUse))
      .use(filterBy(filters))
      .use(when(sort, param({ sort })))
      .use(when(search, (a, s) => a.use(filterBy('search', s)).use(sortBy('search'))))
      .use(sortByDesc('createdAt'))
      .use(paginate({ number: page }))
      .use(when(() => tap, (a, t) => t(a as Action<ConsumeModel<M>>)))
      .run(all(usingDocument));
  };
}
