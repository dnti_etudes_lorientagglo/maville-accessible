import { ResourceMeta } from '@/ts/composables/resources/core/useResourceMeta';
import { i18nInstance } from '@/ts/plugins/i18n';
import toCamelCase from '@/ts/utilities/strings/toCamelCase';
import { RouteLocationRaw } from 'vue-router';

export type ResourceEntry = {
  group?: string;
  position: number;
  icon: string;
  color: string;
  to: RouteLocationRaw;
  title: string;
};

export default function makeResourceEntry(
  meta: ResourceMeta,
  options: Partial<ResourceEntry> = {},
) {
  return {
    icon: meta.icon,
    color: meta.color,
    to: `/resources/${meta.type}`,
    title: i18nInstance.t(`resources.common.types.${toCamelCase(meta.type)}.plural`),
    ...options,
  };
}
