import { registry } from '@/ts/api/action';
import useResourceMeta from '@/ts/composables/resources/core/useResourceMeta';

export default async function inferResourceModel(type: string) {
  const meta = useResourceMeta(type);
  const model = await registry.modelFor(meta.modelType);
  if (!model) {
    throw new Error(`could found model for type ${meta.modelType}, resource's files should be define`);
  }

  return model;
}
