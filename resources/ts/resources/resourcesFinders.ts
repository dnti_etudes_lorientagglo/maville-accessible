import { Dictionary } from '@/ts/utilities/types/dictionary';
import { ModelIdType } from '@foscia/core';

export type ResourceInstanceFinder<I> = (id: ModelIdType) => Promise<I>;

export default import.meta.glob(['./*/finder.ts'], {
  import: 'default',
}) as Dictionary<() => Promise<ResourceInstanceFinder<any>>>;
