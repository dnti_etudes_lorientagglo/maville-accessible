import OrganizationType from '@/ts/api/models/organizationType';
import translate from '@/ts/utilities/lang/translate';
import { mdiOfficeBuildingOutline } from '@mdi/js';

export default {
  type: 'organization-types',
  icon: mdiOfficeBuildingOutline,
  color: 'primary',
  title: (t: OrganizationType) => translate(t.name),
};
