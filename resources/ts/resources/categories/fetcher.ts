import Category from '@/ts/api/models/category';
import makeResourceFetcher from '@/ts/resources/utilities/makeResourceFetcher';
import { filterBy } from '@foscia/jsonapi';

export default makeResourceFetcher(Category, (a) => {
  a.use(filterBy('private', true));
});
