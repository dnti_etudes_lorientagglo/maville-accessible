import Category from '@/ts/api/models/category';
import makeResourceFinder from '@/ts/resources/utilities/makeResourceFinder';
import { include } from '@foscia/core';

export default makeResourceFinder(Category, (a) => {
  a.use(include([
    'sources',
    'parents',
    'children',
  ]));
});
