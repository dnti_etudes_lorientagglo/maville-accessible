import type Category from '@/ts/api/models/category';
import translate from '@/ts/utilities/lang/translate';
import { mdiTagMultipleOutline } from '@mdi/js';

export default {
  type: 'categories',
  icon: mdiTagMultipleOutline,
  color: 'primary',
  title: (c: Category) => translate(c.name),
};
