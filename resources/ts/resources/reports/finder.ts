import Report from '@/ts/api/models/report';
import makeResourceFinder from '@/ts/resources/utilities/makeResourceFinder';
import { include } from '@foscia/core';

export default makeResourceFinder(Report, (a) => {
  a.use(include([
    'reportType',
    'reportable',
    'ownerUser',
  ]));
});
