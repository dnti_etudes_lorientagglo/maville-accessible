import Report from '@/ts/api/models/report';
import { i18nInstance } from '@/ts/plugins/i18n';
import fullNameWithYou from '@/ts/utilities/lang/fullNameWithYou';
import translate from '@/ts/utilities/lang/translate';
import makeAnonymousUser from '@/ts/utilities/users/makeAnonymousUser';
import { mdiCommentAlertOutline } from '@mdi/js';

export default {
  type: 'reports',
  icon: mdiCommentAlertOutline,
  color: 'error',
  title: (r: Report) => i18nInstance.t('resources.reports.title', {
    type: translate(r.reportType.name),
    name: fullNameWithYou(r.ownerUser ?? makeAnonymousUser()),
  }),
};
