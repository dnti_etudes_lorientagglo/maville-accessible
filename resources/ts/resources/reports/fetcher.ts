import Report from '@/ts/api/models/report';
import makeResourceFetcher from '@/ts/resources/utilities/makeResourceFetcher';
import { include } from '@foscia/core';

export default makeResourceFetcher(Report, (a) => {
  a.use(include([
    'reportType',
    'ownerUser',
  ]));
});
