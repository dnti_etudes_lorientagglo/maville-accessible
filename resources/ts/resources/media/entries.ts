import { ResourceMeta } from '@/ts/composables/resources/core/useResourceMeta';
import { i18nInstance } from '@/ts/plugins/i18n';
import auth from '@/ts/utilities/authorizations/auth';
import blocked from '@/ts/utilities/authorizations/blocked';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';

export default function entries(meta: ResourceMeta) {
  if (!auth() || blocked() || !hasPermissionOn('media.showAny')) {
    return [];
  }

  return [
    {
      group: 'myContents',
      position: 1000,
      icon: meta.icon,
      color: meta.color,
      to: '/resources/media-library',
      title: i18nInstance.t('navigation.items.myMedia'),
    },
  ];
}
