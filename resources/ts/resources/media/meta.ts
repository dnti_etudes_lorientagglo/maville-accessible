import Media from '@/ts/api/models/media';
import { mdiImageMultipleOutline } from '@mdi/js';

export default {
  type: 'media',
  icon: mdiImageMultipleOutline,
  color: 'primary',
  title: (m: Media) => m.name,
};
