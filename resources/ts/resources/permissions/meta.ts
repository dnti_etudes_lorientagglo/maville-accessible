import type Permission from '@/ts/api/models/permission';
import translate from '@/ts/utilities/lang/translate';
import { mdiShieldPlusOutline } from '@mdi/js';

export default {
  type: 'permissions',
  icon: mdiShieldPlusOutline,
  color: 'primary',
  title: (p: Permission) => translate(p.name),
};
