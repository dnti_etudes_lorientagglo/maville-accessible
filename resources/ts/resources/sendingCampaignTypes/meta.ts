import SendingCampaignType from '@/ts/api/models/sendingCampaignType';
import translate from '@/ts/utilities/lang/translate';
import { mdiMessageFastOutline } from '@mdi/js';

export default {
  type: 'sending-campaign-types',
  icon: mdiMessageFastOutline,
  color: 'primary',
  title: (c: SendingCampaignType) => translate(c.name),
};
