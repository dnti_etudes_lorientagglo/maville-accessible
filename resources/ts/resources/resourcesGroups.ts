import makeResourceGroup, { ResourceGroup } from '@/ts/resources/utilities/makeResourceGroup';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import {
  mdiAccountCardOutline,
  mdiAccountHardHatOutline,
  mdiAccountTieHatOutline,
  mdiApplicationBracesOutline,
  mdiCommentTextMultipleOutline,
} from '@mdi/js';

export default function resourcesGroups(): Dictionary<ResourceGroup> {
  return {
    ...makeResourceGroup('myContents', {
      position: 100,
      icon: mdiAccountCardOutline,
      color: 'primary',
    }),
    ...makeResourceGroup('admin', {
      position: 1000,
      icon: mdiAccountHardHatOutline,
      color: 'primary',
    }),
    ...makeResourceGroup('moderation', {
      position: 2000,
      icon: mdiAccountTieHatOutline,
      color: 'primary',
    }),
    ...makeResourceGroup('communication', {
      position: 3000,
      icon: mdiCommentTextMultipleOutline,
      color: 'primary',
    }),
    ...makeResourceGroup('app', {
      position: 9000,
      icon: mdiApplicationBracesOutline,
      color: 'primary',
    }),
  };
}
