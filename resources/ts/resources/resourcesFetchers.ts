import { Dictionary } from '@/ts/utilities/types/dictionary';
import { JsonApiDocument } from '@foscia/jsonapi';

export type ResourceInstancesFetcher<I> = (
  query: Dictionary,
) => Promise<{ instances: I[]; document: JsonApiDocument; }>;

export default import.meta.glob(['./*/fetcher.ts'], {
  import: 'default',
}) as Dictionary<() => Promise<ResourceInstancesFetcher<any>>>;
