import ReportType from '@/ts/api/models/reportType';
import translate from '@/ts/utilities/lang/translate';
import { mdiTagMultipleOutline } from '@mdi/js';

export default {
  type: 'report-types',
  icon: mdiTagMultipleOutline,
  color: 'error',
  title: (t: ReportType) => translate(t.name),
};
