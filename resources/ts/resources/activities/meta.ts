import Activity from '@/ts/api/models/activity';
import MainFeature from '@/ts/utilities/features/mainFeature';
import translate from '@/ts/utilities/lang/translate';

export default {
  type: 'activities',
  icon: MainFeature.ACTIVITIES.props.icon,
  color: MainFeature.ACTIVITIES.props.color,
  title: (a: Activity) => translate(a.name),
};
