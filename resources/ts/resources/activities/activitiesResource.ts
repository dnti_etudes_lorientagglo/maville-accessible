import { findCachedActivityType } from '@/ts/api/cached/cachedActivityTypes';
import withReviewsSummary from '@/ts/api/enhancers/withReviewsSummary';
import ActivityTypeCode from '@/ts/api/enums/activityTypeCode';
import Activity from '@/ts/api/models/activity';
import { ResourceMeta } from '@/ts/composables/resources/core/useResourceMeta';
import { i18nInstance } from '@/ts/plugins/i18n';
import makeResourceEntry from '@/ts/resources/utilities/makeResourceEntry';
import makeResourceFetcher from '@/ts/resources/utilities/makeResourceFetcher';
import makeResourceFinder from '@/ts/resources/utilities/makeResourceFinder';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';
import FeatureName from '@/ts/utilities/features/featureName';
import toUpperFirst from '@/ts/utilities/strings/toUpperFirst';
import { fill, include } from '@foscia/core';
import { filterBy } from '@foscia/jsonapi';

export default {
  makeEntries: (type: ActivityTypeCode, position: number) => (meta: ResourceMeta) => {
    if (ActivityTypeCode.ACTIVITIES.is(type) && !FeatureName.ACTIVITIES.check()) {
      return [];
    }

    if (ActivityTypeCode.EVENTS.is(type) && !FeatureName.EVENTS.check()) {
      return [];
    }

    if (hasPermissionOn('activities.admin') && hasPermissionOn('activities.showAny')) {
      return makeResourceEntry(meta, { group: 'admin', position });
    }

    if (hasPermissionOn('activities.showAny')) {
      return makeResourceEntry(meta, {
        title: i18nInstance.t(`navigation.items.my${toUpperFirst(type.value)}`),
        group: 'myContents',
        position,
      });
    }

    return [];
  },
  makeFactory: (type: ActivityTypeCode) => async () => fill(new Activity(), {
    activityType: await findCachedActivityType(type.value),
  }),
  makeFetcher: (type: ActivityTypeCode) => makeResourceFetcher(Activity, (a) => a.use(
    filterBy('private', true),
    filterBy('activityType', type.value),
    include([
      'activityType',
      'categories',
      'place.categories.parents',
    ]),
  )),
  makeFinder: (type: ActivityTypeCode) => makeResourceFinder(Activity, (a) => a.use(
    filterBy('activityType', type.value),
    withReviewsSummary(),
    include([
      'activityType',
      'ownerOrganization',
      'ownerUser',
      'media',
      'categories',
      'cover',
      'images',
      'place.categories.parents',
      'sources',
    ]),
  )),
};
