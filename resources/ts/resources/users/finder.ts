import User from '@/ts/api/models/user';
import makeResourceFinder from '@/ts/resources/utilities/makeResourceFinder';
import { include } from '@foscia/core';

export default makeResourceFinder(User, (a) => {
  a.use(include('roles', 'memberOfOrganizations.organization'));
}, { noCache: true });
