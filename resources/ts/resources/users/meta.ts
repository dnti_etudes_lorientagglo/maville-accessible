import type User from '@/ts/api/models/user';
import fullNameWithYou from '@/ts/utilities/lang/fullNameWithYou';
import { mdiAccountMultipleOutline } from '@mdi/js';

export default {
  type: 'users',
  icon: mdiAccountMultipleOutline,
  color: 'primary',
  title: (u: User) => fullNameWithYou(u),
};
