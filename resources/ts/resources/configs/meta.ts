import { i18nInstance } from '@/ts/plugins/i18n';
import { mdiCogOutline } from '@mdi/js';

export default {
  type: 'configs',
  icon: mdiCogOutline,
  color: 'primary',
  title: () => i18nInstance.t('resources.configs.title'),
};
