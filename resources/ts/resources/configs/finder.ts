import Config from '@/ts/api/models/config';
import makeResourceFinder from '@/ts/resources/utilities/makeResourceFinder';
import { include } from '@foscia/core';

export default makeResourceFinder(Config, (a) => {
  a.use(include([
    'mapSearchableCategories',
    'mapDefaultCategories',
  ]));
});
