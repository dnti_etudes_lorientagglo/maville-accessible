import Config from '@/ts/api/models/config';
import { ResourceMeta } from '@/ts/composables/resources/core/useResourceMeta';
import useResourceRouteOne from '@/ts/composables/resources/core/useResourceRouteOne';
import env from '@/ts/env';
import makeResourceEntry from '@/ts/resources/utilities/makeResourceEntry';
import hasPermissionOn from '@/ts/utilities/authorizations/hasPermissionOn';
import translate from '@/ts/utilities/lang/translate';
import { fill } from '@foscia/core';

export default function entries(meta: ResourceMeta) {
  if (hasPermissionOn('configs.showAny')) {
    return makeResourceEntry(meta, {
      title: translate('resources.configs.title'),
      to: useResourceRouteOne(fill(new Config(), { id: env.app.id })),
      group: 'app',
      position: 100,
    });
  }

  return [];
}
