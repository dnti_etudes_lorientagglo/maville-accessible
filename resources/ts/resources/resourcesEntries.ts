import useResourceMeta, { ResourceMeta } from '@/ts/composables/resources/core/useResourceMeta';
import resourcesMeta from '@/ts/resources/resourcesMeta';
import { ResourceEntry } from '@/ts/resources/utilities/makeResourceEntry';
import value from '@/ts/utilities/functions/value';
import mapValues from '@/ts/utilities/objects/mapValues';
import toCamelCase from '@/ts/utilities/strings/toCamelCase';
import { Dictionary } from '@/ts/utilities/types/dictionary';

const entriesFactories = import.meta.glob(['./*/entries.ts'], {
  import: 'default',
  eager: true,
}) as Dictionary<(meta: ResourceMeta) => ResourceEntry[]>;

export default function resourcesEntries() {
  return mapValues(resourcesMeta, (rawMeta) => {
    const meta = useResourceMeta(rawMeta.type);

    return value(entriesFactories[`./${toCamelCase(meta.type)}/entries.ts`], meta) ?? [];
  });
}
