import type Role from '@/ts/api/models/role';
import translate from '@/ts/utilities/lang/translate';
import { mdiShieldAccountOutline } from '@mdi/js';

export default {
  type: 'roles',
  icon: mdiShieldAccountOutline,
  color: 'primary',
  title: (r: Role) => translate(r.name),
};
