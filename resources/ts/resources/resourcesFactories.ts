import { Awaitable } from '@/ts/utilities/types/awaitable';
import { Dictionary } from '@/ts/utilities/types/dictionary';
import { ModelInstance } from '@foscia/core';

export type ResourceInstanceFactory<I extends ModelInstance> = () => Awaitable<I>;

export default import.meta.glob(['./*/factory.ts'], {
  import: 'default',
}) as Dictionary<() => Promise<ResourceInstanceFactory<any>>>;
