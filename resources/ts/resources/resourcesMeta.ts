import type { ResourceRawMeta } from '@/ts/composables/resources/core/useResourceMeta';
import mapWithKeys from '@/ts/utilities/objects/mapWithKeys';
import { Dictionary } from '@/ts/utilities/types/dictionary';

export default mapWithKeys(
  import.meta.glob(['./*/meta.ts'], {
    import: 'default',
    eager: true,
  }) as Dictionary<ResourceRawMeta>,
  (meta) => ({ [meta.type]: meta }),
);
