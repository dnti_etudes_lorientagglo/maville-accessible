import Organization from '@/ts/api/models/organization';
import makeResourceFinder from '@/ts/resources/utilities/makeResourceFinder';
import { include } from '@foscia/core';

export default makeResourceFinder(Organization, (a) => {
  a.use(include([
    'organizationType',
    'membersOfOrganization',
    'membersOfOrganization.user',
    'membersOfOrganization.role',
    'invitations.role',
    'media',
    'cover',
    'categories',
    'sources',
  ]));
});
