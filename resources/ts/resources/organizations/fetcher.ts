import Organization from '@/ts/api/models/organization';
import makeResourceFetcher from '@/ts/resources/utilities/makeResourceFetcher';
import { filterBy } from '@foscia/jsonapi';

export default makeResourceFetcher(Organization, (a) => {
  a.use(filterBy('private', true));
});
