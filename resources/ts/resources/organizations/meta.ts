import Organization from '@/ts/api/models/organization';
import translate from '@/ts/utilities/lang/translate';
import { mdiOfficeBuildingOutline } from '@mdi/js';

export default {
  type: 'organizations',
  icon: mdiOfficeBuildingOutline,
  color: 'primary',
  title: (o: Organization) => translate(o.name),
};
