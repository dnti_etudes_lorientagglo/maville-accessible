import type { Translation } from '@/ts/utilities/lang/translate';
import type { AddressObject } from '@/ts/utilities/places/types';
import type { Dictionary } from '@/ts/utilities/types/dictionary';

type Env = {
  auth: {
    id: string | null;
  };
  app: {
    id: string;
    name: string;
    territory: string;
    description: Translation | null;
    url: string;
  };
  features: Dictionary;
  services: {
    configurable: string[];
    i18n: {
      locale: string;
      defaultLocale: string;
      locales: Dictionary<string>;
      easyReadLocales: string[];
      countries: string[];
    };
    map: {
      center: AddressObject;
      zoom: number;
      layers: string[];
      searchableCategories: Dictionary[];
      defaultCategories: Dictionary[];
      directions: {
        implementation: 'hitineraire' | null;
        options: any;
      };
    };
    files: {
      supportsClientUploads: boolean;
      supportsVisibility: boolean;
    };
    social: {
      providers: string[];
    };
    trackers: {
      matomo: {
        siteId: string | null;
        host: string | null;
        trackerFileName: string | null;
        trackerEndpointUrl: string | null;
        trackerScriptUrl: string | null;
        crossOrigin: string | null;
        debug: string | null;
      }
    };
  };
};

type ThemeDefinition = {
  primary: string;
  'on-primary'?: string;
  'primary-text'?: string;
  background?: string;
  'on-background'?: string;
  surface?: string;
  'on-surface'?: string;
  secondary: string;
  'on-secondary'?: string;
  tertiary: string;
  'on-tertiary'?: string;
  quaternary: string;
  'on-quaternary'?: string;
  quinary: string;
  'on-quinary'?: string;
};

type Theme = {
  light: ThemeDefinition;
  dark: Omit<ThemeDefinition, 'primary'> & { primary?: string };
};

// eslint-disable-next-line @typescript-eslint/naming-convention,no-underscore-dangle
declare const __APP_ENV: Env;
// eslint-disable-next-line @typescript-eslint/naming-convention,no-underscore-dangle
declare const __APP_THEME: Theme;

export const envTheme = __APP_THEME;

export default __APP_ENV;
