export default [
  {
    name: 'legal.site-map',
    path: '/site-map',
    component: () => import('@/ts/views/legal/SiteMapView.vue'),
  },
  {
    name: 'legal.legal-notice',
    path: '/legal/notice',
    component: () => import('@/ts/views/legal/LegalNoticeView.vue'),
  },
  {
    name: 'legal.terms-of-use',
    path: '/legal/terms-of-use',
    component: () => import('@/ts/views/legal/TermsOfUseView.vue'),
  },
  {
    name: 'legal.privacy-policy',
    path: '/legal/privacy-policy',
    component: () => import('@/ts/views/legal/PrivacyPolicyView.vue'),
  },
  {
    name: 'legal.accessibility-declaration',
    path: '/accessibility-declaration',
    component: () => import('@/ts/views/legal/AccessibilityDeclarationView.vue'),
  },
];
