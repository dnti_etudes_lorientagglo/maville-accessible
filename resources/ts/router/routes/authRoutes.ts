import authGuard from '@/ts/router/guards/authGuard';
import guestGuard from '@/ts/router/guards/guestGuard';
import notVerifiedGuard from '@/ts/router/guards/notVerifiedGuard';

export default [
  {
    name: 'auth.sign-in',
    path: '/auth/sign-in',
    component: () => import('@/ts/views/auth/SignInView.vue'),
    meta: { guards: [guestGuard] },
  },
  {
    name: 'auth.sign-up',
    path: '/auth/sign-up',
    component: () => import('@/ts/views/auth/SignUpView.vue'),
    meta: { guards: [guestGuard] },
  },
  {
    name: 'auth.forgot-password',
    path: '/auth/forgot-password',
    component: () => import('@/ts/views/auth/ForgotPasswordView.vue'),
    meta: { guards: [guestGuard] },
  },
  {
    name: 'auth.reset-password',
    path: '/auth/reset-password/:email/:token',
    component: () => import('@/ts/views/auth/ResetPasswordView.vue'),
    meta: { guards: [guestGuard] },
  },
  {
    name: 'auth.verification',
    path: '/auth/verification',
    component: () => import('@/ts/views/auth/VerificationView.vue'),
    meta: { guards: [authGuard, notVerifiedGuard] },
  },
  {
    name: 'auth.invitation',
    path: '/auth/invitations/:invitation',
    component: () => import('@/ts/views/auth/InvitationView.vue'),
    meta: { guards: [authGuard] },
  },
];
