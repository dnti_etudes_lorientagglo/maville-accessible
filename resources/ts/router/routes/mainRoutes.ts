import ActivityTypeCode from '@/ts/api/enums/activityTypeCode';
import { validateMapViewParams } from '@/ts/composables/places/map/useMapView';
import wrap from '@/ts/utilities/arrays/wrap';
import FeatureName from '@/ts/utilities/features/featureName';
import { RouteLocation, RouteRecordRaw } from 'vue-router';

const makeActivitiesRoutes = (activityType: ActivityTypeCode) => ({
  path: `/${activityType.value}`,
  meta: { activityType },
  component: () => import('@/ts/views/activities/ActivitiesRouter.vue'),
  children: [
    {
      name: `${activityType.value}`,
      path: '',
      component: () => import('@/ts/views/activities/ActivitiesView.vue'),
    },
    {
      name: `${activityType.value}.about`,
      path: 'about/:type/:id',
      component: () => import('@/ts/views/activities/ActivitiesView.vue'),
    },
    {
      name: `${activityType.value}.read`,
      path: 'read/:activity',
      component: () => import('@/ts/views/activities/ActivityReadView.vue'),
    },
    {
      name: `${activityType.value}.about.read`,
      path: 'about/:type/:id/read/:activity',
      component: () => import('@/ts/views/activities/ActivityReadView.vue'),
    },
  ],
});

export default [
  ...wrap(FeatureName.PLACES.when({
    name: 'places.map',
    path: '/places/map/:view?/:place?',
    component: () => import('@/ts/views/places/map/MapView.vue'),
    beforeEnter: (to: RouteLocation) => {
      if (!validateMapViewParams(to.params)) {
        return {
          name: to.name,
          params: { view: 'search' },
          query: to.query,
        };
      }

      return undefined;
    },
  })),
  ...wrap(FeatureName.ARTICLES.when({
    path: '/articles',
    component: () => import('@/ts/views/articles/ArticlesRouter.vue'),
    children: [
      {
        name: 'articles',
        path: '',
        component: () => import('@/ts/views/articles/ArticlesView.vue'),
      },
      {
        name: 'articles.about',
        path: 'about/:type/:id',
        component: () => import('@/ts/views/articles/ArticlesView.vue'),
      },
      {
        name: 'articles.read',
        path: 'read/:article',
        component: () => import('@/ts/views/articles/ArticleReadView.vue'),
      },
      {
        name: 'articles.about.read',
        path: 'about/:type/:id/read/:article',
        component: () => import('@/ts/views/articles/ArticleReadView.vue'),
      },
    ],
  })),
  ...wrap(FeatureName.ACTIVITIES.when(makeActivitiesRoutes(ActivityTypeCode.ACTIVITIES))),
  ...wrap(FeatureName.EVENTS.when(makeActivitiesRoutes(ActivityTypeCode.EVENTS))),
] as RouteRecordRaw[];
