import authGuard from '@/ts/router/guards/authGuard';
import FeatureName from '@/ts/utilities/features/featureName';

export default [
  ...(FeatureName.NEWSLETTER.check() ? [{
    name: 'account.newsletter',
    path: '/account/newsletter/:email',
    component: () => import('@/ts/views/account/MyNewsletterView.vue'),
  }] : []),
  {
    name: 'account.me',
    path: '/account/me',
    component: () => import('@/ts/views/account/MyAccountView.vue'),
    meta: { guards: [authGuard] },
  },
  {
    name: 'account.notifications',
    path: '/account/notifications',
    component: () => import('@/ts/views/account/MyNotificationsView.vue'),
    meta: { guards: [authGuard] },
  },
];
