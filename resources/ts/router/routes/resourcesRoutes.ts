import makeResourceRouteFactory from '@/ts/resources/utilities/makeResourceRouteFactory';
import makeResourceRoutes from '@/ts/resources/utilities/makeResourceRoutes';
import authGuard from '@/ts/router/guards/authGuard';
import notBlockedGuard from '@/ts/router/guards/notBlockedGuard';
import wrap from '@/ts/utilities/arrays/wrap';
import FeatureName from '@/ts/utilities/features/featureName';

const viewManyRoute = makeResourceRouteFactory('view-many');
const createRoute = makeResourceRouteFactory('create');
const viewRoute = makeResourceRouteFactory('view/:instance');
const editRoute = makeResourceRouteFactory('edit/:instance');

export default [
  {
    path: '/resources/media-library',
    component: () => import('@/ts/views/resources/media/MediaLibraryView.vue'),
    meta: { guards: [authGuard, notBlockedGuard] },
  },
  {
    path: '/resources',
    component: () => import('@/ts/views/resources/ResourcesRouterView.vue'),
    children: [
      makeResourceRoutes('configs', [
        viewRoute,
        editRoute,
      ]),
      makeResourceRoutes('roles', [
        viewManyRoute,
        viewRoute,
      ]),
      makeResourceRoutes('users', [
        viewManyRoute,
        viewRoute,
        editRoute,
      ]),
      makeResourceRoutes('report-types', [
        viewManyRoute,
        createRoute,
        viewRoute,
        editRoute,
      ]),
      makeResourceRoutes('reports', [
        viewManyRoute,
        viewRoute,
      ]),
      makeResourceRoutes('requests', [
        viewManyRoute,
        viewRoute,
      ]),
      makeResourceRoutes('sending-campaigns', [
        viewManyRoute,
        viewRoute,
      ]),
      makeResourceRoutes('categories', [
        viewManyRoute,
        createRoute,
        viewRoute,
        editRoute,
      ]),
      makeResourceRoutes('organizations', [
        viewManyRoute,
        createRoute,
        viewRoute,
        editRoute,
      ]),
      ...wrap(FeatureName.PLACES.when(makeResourceRoutes('places', [
        viewManyRoute,
        createRoute,
        viewRoute,
        editRoute,
      ]))),
      ...wrap(FeatureName.ARTICLES.when(makeResourceRoutes('articles', [
        viewManyRoute,
        createRoute,
        viewRoute,
        editRoute,
      ]))),
      ...wrap(FeatureName.ACTIVITIES.when(makeResourceRoutes('activities', [
        viewManyRoute,
        createRoute,
        viewRoute,
        editRoute,
      ]))),
      ...wrap(FeatureName.EVENTS.when(makeResourceRoutes('events', [
        viewManyRoute,
        createRoute,
        viewRoute,
        editRoute,
      ]))),
    ],
  },
];
