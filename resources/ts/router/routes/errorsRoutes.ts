export default [
  {
    name: 'errors.unauthorized',
    path: '/errors/401-unauthorized',
    component: () => import('@/ts/views/errors/UnauthorizedErrorView.vue'),
  },
  {
    name: 'errors.forbidden',
    path: '/errors/403-forbidden',
    component: () => import('@/ts/views/errors/ForbiddenErrorView.vue'),
  },
  {
    name: 'errors.invalid-signature',
    path: '/errors/403-invalid-signature',
    component: () => import('@/ts/views/errors/InvalidSignatureErrorView.vue'),
  },
  {
    name: 'errors.not-found',
    path: '/errors/404-not-found',
    component: () => import('@/ts/views/errors/NotFoundErrorView.vue'),
  },
  {
    name: 'errors.too-many-attempts',
    path: '/errors/429-too-many-attempts',
    component: () => import('@/ts/views/errors/TooManyAttemptsErrorView.vue'),
  },
];
