import toTrim from '@/ts/utilities/strings/toTrim';
import { Optional } from '@/ts/utilities/types/optional';
import { RouteLocationNormalized, Router } from 'vue-router';

let previousTimeout: number | undefined;

const normalizeTitle = (title: Optional<string>) => toTrim(title).replace(/(( \| )|(\s)|(&nbsp;))+/g, ' ');

export default function debugPageTitle(
  router: Router,
  intended: RouteLocationNormalized,
) {
  if (import.meta.env.DEV) {
    if (previousTimeout !== undefined) {
      clearTimeout(previousTimeout);
    }

    previousTimeout = setTimeout(() => {
      previousTimeout = undefined;
      if (
        router.currentRoute.value.name !== 'home'
        && router.currentRoute.value.fullPath === intended.fullPath
      ) {
        const documentTitle = normalizeTitle(document.title);
        const pageTitleElement = document.getElementsByTagName('h1')[0]
          ?? document.querySelector('[role="heading"][aria-level="1"]');

        if (!pageTitleElement) {
          // eslint-disable-next-line no-console
          console.warn('No H1 element found in document. Did you forget to define a H1 element?');

          return;
        }

        const pageTitle = normalizeTitle(pageTitleElement.textContent);

        if (!documentTitle.startsWith(pageTitle) && !documentTitle.replace(' | ', ' ').startsWith(pageTitle)) {
          // eslint-disable-next-line no-console
          console.warn(`Page title \`${pageTitle}\` does not match document title \`${documentTitle}\`. Did you forget to use announcer?`);
        }
      }
    }, 2000);
  }
}
