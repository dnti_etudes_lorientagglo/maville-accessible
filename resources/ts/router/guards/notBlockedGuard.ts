import makeForbiddenError from '@/ts/errors/redirectable/makeForbiddenError';
import makeUnauthorizedError from '@/ts/errors/redirectable/makeUnauthorizedError';
import makeGuard from '@/ts/router/guards/makeGuard';
import auth from '@/ts/utilities/authorizations/auth';
import blocked from '@/ts/utilities/authorizations/blocked';

export default makeGuard(
  () => !blocked(),
  (to) => (auth() ? makeForbiddenError() : makeUnauthorizedError(to)),
);
