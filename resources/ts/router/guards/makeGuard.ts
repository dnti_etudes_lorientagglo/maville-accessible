import RedirectableError from '@/ts/errors/redirectable/redirectableError';
import { Awaitable } from '@/ts/utilities/types/awaitable';
import { RouteLocationNormalized } from 'vue-router';

export type Guard = (
  to: RouteLocationNormalized,
  from?: RouteLocationNormalized,
) => Promise<never | void>;

export default function makeGuard(
  condition: (
    to: RouteLocationNormalized,
    from?: RouteLocationNormalized,
  ) => Awaitable<unknown>,
  errorFactory: (
    to: RouteLocationNormalized,
    from?: RouteLocationNormalized,
  ) => Awaitable<RedirectableError>,
): Guard {
  return async (to: RouteLocationNormalized, from?: RouteLocationNormalized) => {
    if (!(await condition(to, from))) {
      throw await errorFactory(to, from);
    }
  };
}
