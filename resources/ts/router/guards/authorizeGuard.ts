import makeForbiddenError from '@/ts/errors/redirectable/makeForbiddenError';
import makeUnauthorizedError from '@/ts/errors/redirectable/makeUnauthorizedError';
import makeGuard from '@/ts/router/guards/makeGuard';
import auth from '@/ts/utilities/authorizations/auth';

export default function authorizeGuard(authorize: () => boolean) {
  return makeGuard(
    authorize,
    (to) => (auth() ? makeForbiddenError() : makeUnauthorizedError(to)),
  );
}
