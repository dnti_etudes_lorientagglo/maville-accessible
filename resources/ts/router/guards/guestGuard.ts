import RedirectableError from '@/ts/errors/redirectable/redirectableError';
import makeGuard from '@/ts/router/guards/makeGuard';
import { AUTH_HOME } from '@/ts/stores/auth';
import guest from '@/ts/utilities/authorizations/guest';

export default makeGuard(guest, () => new RedirectableError(AUTH_HOME));
