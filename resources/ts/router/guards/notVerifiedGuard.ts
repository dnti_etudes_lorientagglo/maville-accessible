import RedirectableError from '@/ts/errors/redirectable/redirectableError';
import makeGuard from '@/ts/router/guards/makeGuard';
import { AUTH_HOME } from '@/ts/stores/auth';
import verified from '@/ts/utilities/authorizations/verified';

export default makeGuard(() => !verified(), () => new RedirectableError(AUTH_HOME));
