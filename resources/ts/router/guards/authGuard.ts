import makeUnauthorizedError from '@/ts/errors/redirectable/makeUnauthorizedError';
import makeGuard from '@/ts/router/guards/makeGuard';
import auth from '@/ts/utilities/authorizations/auth';

export default makeGuard(auth, (to) => makeUnauthorizedError(to));
