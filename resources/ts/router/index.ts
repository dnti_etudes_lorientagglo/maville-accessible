import RedirectableError from '@/ts/errors/redirectable/redirectableError';
import debugPageTitle from '@/ts/router/debugPageTitle';
import ROUTE_NOT_FOUND_PATH from '@/ts/router/notFoundPathName';
import accountRoutes from '@/ts/router/routes/accountRoutes';
import authRoutes from '@/ts/router/routes/authRoutes';
import errorsRoutes from '@/ts/router/routes/errorsRoutes';
import legalRoutes from '@/ts/router/routes/legalRoutes';
import mainRoutes from '@/ts/router/routes/mainRoutes';
import resourcesRoutes from '@/ts/router/routes/resourcesRoutes';
import { useAppStore } from '@/ts/stores/app';
import { useRoutingStore } from '@/ts/stores/routing';
import promiseQueue from '@/ts/utilities/functions/promiseQueue';
import { parse as parseQuery, stringify as stringifyQuery } from 'qs';
import { createRouter, createWebHistory, LocationQuery } from 'vue-router';

const routes = [
  ...authRoutes,
  ...accountRoutes,
  ...resourcesRoutes,
  ...mainRoutes,
  ...legalRoutes,
  ...errorsRoutes,
  {
    name: 'faq',
    path: '/faq',
    component: () => import('@/ts/views/FaqView.vue'),
  },
  {
    name: 'home',
    path: '/',
    component: () => import('@/ts/views/HomeView.vue'),
  },
  {
    name: ROUTE_NOT_FOUND_PATH,
    path: '/:pathMatch(.*)*',
    redirect: { name: 'errors.not-found', params: {} },
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
  parseQuery: (query) => parseQuery(query) as LocationQuery,
  stringifyQuery: (query) => stringifyQuery(query),
  scrollBehavior: (to, from, savedPosition) => {
    useRoutingStore().savePosition(savedPosition);
  },
});

router.beforeEach(async (to, from) => {
  const routing = useRoutingStore();

  // Prevents navigation is needed.
  const redirectToTargeted = () => router.push(to);
  if (routing.preventedLeaving(redirectToTargeted)) {
    return false;
  }

  // Fix for facebook login fragment.
  if (to?.fullPath?.endsWith('#_=_')) {
    return to.fullPath.replace('#_=_', '');
  }

  // Boot application services (auth, localization, etc.).
  const app = useAppStore();
  if (!app.booted && !app.booting) {
    await app.boot(router);
  }

  // Handle routes guards.
  if (Array.isArray(to?.meta?.guards)) {
    try {
      await promiseQueue(to.meta.guards.map((g) => () => g(to, from)));
    } catch (error) {
      if (error instanceof RedirectableError) {
        return error.redirectTo;
      }

      throw error;
    }
  }

  routing.navigating(true);

  return undefined;
});

router.afterEach((to, from) => {
  const routing = useRoutingStore();

  routing.navigating(false);
  routing.savePreviousRoute(to, from);

  debugPageTitle(router, to);
});

export default router;
