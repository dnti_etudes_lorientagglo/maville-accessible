declare module '@socialgouv/jours-feries' {
  import { Dictionary } from '@/ts/utilities/types/dictionary';

  const joursFeries: (
    year: number,
    options: { zone: 'métropole' | 'alsace-moselle' } = { zone: 'métropole' },
  ) => Dictionary<Date>;

  export default joursFeries;
}
