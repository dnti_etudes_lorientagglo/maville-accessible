<!doctype html>
<html
  lang="{{ $lang }}"
  aria-busy="true"
>
<head>
  <meta charset="utf-8">
  <meta
    name="viewport"
    content="width=device-width, initial-scale=1"
  >

  <title>{{ $title }}</title>

  @stack('meta')

  @stack('scripts')

  @stack('styles')

  @stack('preloads')
</head>
<body id="app__body">
@yield('content')
</body>
</html>
