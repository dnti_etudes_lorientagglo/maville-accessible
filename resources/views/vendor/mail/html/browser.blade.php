@props(['browser'])
<tr>
  <td
    class="browser"
    width="100%"
    cellpadding="0"
    cellspacing="0"
  >
    <table
      class="browser-container"
      align="center"
      width="570"
      cellpadding="0"
      cellspacing="0"
      role="presentation"
    >
      <tr>
        <td align="center">
          <p class="browser-link">
            {!! trans('campaigns.mail.browser.html.title', [
                'action' => '<a href="'.$browser.'">'.trans('campaigns.mail.browser.html.action').'</a>',
            ]) !!}
          </p>
        </td>
      </tr>
    </table>
  </td>
</tr>
