<table class="subcopy" width="100%" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td>
{{ Illuminate\Mail\Markdown::parse($slot) }}
{{ Illuminate\Mail\Markdown::parse(trans('notifications.common.mail.noReply', [
    'email' => config('mail.from.address'),
])) }}
</td>
</tr>
</table>
