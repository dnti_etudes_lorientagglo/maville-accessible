<x-mail::layout>
{{-- Header --}}
<x-slot:header>
@isset($browser)
<x-mail::browser :browser="$browser" />
@endisset

<x-mail::header :url="config('app.url')">
<img
src="{{ \Illuminate\Support\Facades\Vite::asset('resources/assets/img/logo.png') }}"
alt="{{ config('app.name') }}"
height="60"
>
</x-mail::header>
</x-slot:header>

{{-- Body --}}
{{ $slot }}

{{-- Subcopy --}}
@isset($subcopy)
<x-slot:subcopy>
<x-mail::subcopy>
{{ $subcopy }}
</x-mail::subcopy>
</x-slot:subcopy>
@endisset

{{-- Footer --}}
<x-slot:footer>
<x-mail::footer>
© {{ date('Y') }} {{ config('app.name') }}. @lang('notifications.common.mail.allRightsReserved')
</x-mail::footer>
</x-slot:footer>
</x-mail::layout>
