@props(['browser'])
@lang('campaigns.mail.browser.text.title', [
    'action' => $browser
])
