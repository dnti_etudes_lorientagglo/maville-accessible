{{ $slot }}
@lang('notifications.common.mail.noReply', [
    'email' => config('mail.from.address'),
])
