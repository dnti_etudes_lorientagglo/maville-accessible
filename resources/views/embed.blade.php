@extends('layout', ['lang' => $env['services']['i18n']['locale'], 'title' => $title])

@push('scripts')
  <link
    rel="preload"
    href="{{ \Illuminate\Support\Facades\Vite::asset('resources/assets/fonts/Luciole/Luciole-Regular.ttf') }}"
    as="font"
    type="font/ttf"
    crossorigin="anonymous"
  >

  <script type="application/javascript">
    var __APP_ENV = @json($env);
  </script>

  @vite('resources/ts/embed.ts')
@endpush

@section('content')
  <div id="app"></div>
@endsection
