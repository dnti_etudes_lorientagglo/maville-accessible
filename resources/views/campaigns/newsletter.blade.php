<x-mail::message :browser="$browser">
<x-slot:subcopy>
@if($recipient->unsubscribe())
@lang('campaigns.newsletter.unsubscribe.title', ['name' => config('app.name')])

[@lang('campaigns.newsletter.unsubscribe.action')]({{ $recipient->unsubscribe() }})
@endif
</x-slot:subcopy>

# {{ \App\Helpers\LocaleHelper::translate($subject) }}

{{ \App\Helpers\LocaleHelper::translate($description) }}

@if(!empty($articles))
<x-mail::panel>

## @lang('campaigns.newsletter.articles.title')

@foreach($articles as $article)

### [{{ \App\Helpers\LocaleHelper::translate($article['name']) }}]({{ $article['url'] }})

@if(\App\Helpers\LocaleHelper::translate($article['description']))
{{ \App\Helpers\LocaleHelper::translate($article['description']) }}
@endif

---

@endforeach

<x-mail::button :url="url('/articles')">
@lang('campaigns.newsletter.articles.action')
</x-mail::button>

</x-mail::panel>
@endif

@if(!empty($events))
## @lang('campaigns.newsletter.events.title')

@foreach($events as $event)

<x-mail::panel>
@if($event['cover'])
![]({{ $event['cover'] }})
@endif

### [{{ \App\Helpers\LocaleHelper::translate($event['name']) }}]({{ $event['url'] }})

@if(\App\Helpers\LocaleHelper::translate($event['description']))
{{ \App\Helpers\LocaleHelper::translate($event['description']) }}
@endif

_{{ \App\Helpers\LocaleHelper::translate($event['dates']) }}_
</x-mail::panel>

@endforeach

<x-mail::button :url="url('/events')">
@lang('campaigns.newsletter.events.action')
</x-mail::button>
@endif

@if(!empty($activities))
## @lang('campaigns.newsletter.activities.title')

@foreach($activities as $activity)

<x-mail::panel>
@if($activity['cover'])
![]({{ $activity['cover'] }})
@endif

### [{{ \App\Helpers\LocaleHelper::translate($activity['name']) }}]({{ $activity['url'] }})

@if(\App\Helpers\LocaleHelper::translate($activity['description']))
{{ \App\Helpers\LocaleHelper::translate($activity['description']) }}
@endif

_{{ \App\Helpers\LocaleHelper::translate($activity['dates']) }}_
</x-mail::panel>

@endforeach

<x-mail::button :url="url('/activities')">
@lang('campaigns.newsletter.activities.action')
</x-mail::button>
@endif

@if(!empty($places))
<x-mail::panel>

## @lang('campaigns.newsletter.places.title')

@foreach($places as $place)

### [{{ \App\Helpers\LocaleHelper::translate($place['name']) }}]({{ $place['url'] }})

@if(\App\Helpers\LocaleHelper::translate($place['description']))
{{ \App\Helpers\LocaleHelper::translate($place['description']) }}
@endif

_{{ $place['address'] }}_

---

@endforeach

<x-mail::button :url="url('/places/map')">
@lang('campaigns.newsletter.places.action')
</x-mail::button>

</x-mail::panel>
@endif

</x-mail::message>
