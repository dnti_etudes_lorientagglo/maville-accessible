**Nom du fichier** : `activities.json`.

Le fichier contient à la racine une liste des activités récurrentes et
événements temporaires accessibles publiés sur la plateforme.
Chaque activité listée correspond à un objet `Activity`.

```json
[
  {
    "id": "36b3f1ed-f11d-4a0b-b349-f1e370edfef4",
    "slug": "loisirs-adaptes-la-renouee-atelier-manuel-et-creatif-plouay",
    "name": "Loisirs adaptés La Renouée : atelier manuel et créatif",
    "activityType": "events",
    ...
  },
  {
    "id": "36b3f1ed-f11d-4a0b-b349-f1e370edfef4",
    "slug": "soiree-fitness-lorient",
    "name": "Soirée fitness",
    "activityType": "events",
    ...
  },
  ...
]
```
