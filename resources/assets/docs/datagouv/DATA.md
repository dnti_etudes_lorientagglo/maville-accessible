## Données

### Notes

- Un champ indiqué comme pouvant avoir une valeur de type `null` peut être absent
  du fichier JSON. Cela permet de produire des fichiers plus légers.

### `Place`

Un lieu sur la plateforme.

- `id` `uuid` Identifiant UUID unique sur la plateforme (ne change jamais) ;
- `slug` `string` Identifiant texte unique sur la plateforme (peut changer dans le temps) ;
- `url` `string` URL sur la plateforme (peut changer dans le temps) ;
- `name` `string` Nom ;
- `description` `string|null` Description ;
- `address` [`Address`](#address) Adresse ;
- `accessibility` [`Accessibility`](#accessibility) Dictionnaire de données d'accessibilité ;
- `openingHours` [`Array<OpeningHour>`](#openinghour) Liste de tranches horaires ;
- `phone` `string|null` Numéro de téléphone de contact (au format E164) ;
- `email` `string|null` Adresse email de contact ;
- `links` `Dictionary<string>|null` Dictionnaire de liens vers des sites externes ;
- `createdAt` `string` Date, heure et fuseau horaire de création (au format ISO 8601) ;
- `updatedAt` `string` Date, heure et fuseau horaire de dernière mise à jour (au format ISO 8601) ;
- `publishedAt` `string` Date, heure et fuseau horaire de publication (au format ISO 8601) ;
- `images` [`Array<Image>`](#image)` Liste d'images présentant le lieu ;
- `categories` [`Array<Category>`](#category)` Liste des catégories ;

### `Activity`

Une activité récurrente ou un événement temporaire ou non sur la plateforme.

- `id` `uuid` Identifiant UUID unique sur la plateforme (ne change jamais) ;
- `slug` `string` Identifiant texte unique sur la plateforme (peut changer dans le temps) ;
- `url` `string` URL sur la plateforme (peut changer dans le temps) ;
- `activityType` `string` Code indiquant s'il s'agit d'une activité récurrente (`activities`) ou d'un événement temporaire (`events`) ;
- `name` `string` Nom ;
- `description` `string|null` Description ;
- `body` `string|null` Description complète de l'activité au format HTML ;
- `place` [`Place`](#place) Lieu de l'événement ;
- `address` [`Address|null`](#address) Adresse (dans le cas où aucun lieu correspondant n'existe sur la plateforme) ;
- `accessibility` [`Accessibility`](#accessibility) Dictionnaire de données d'accessibilité ;
- `openingOnPeriods` `boolean` Indique si l'ouverture de l'activité se fait sur des périodes (champs `openingPeriods` et `openingWeekdaysHours`) ou sur des dates précises (champs `openingDatesHours`) ;
- `openingPeriods` [`Array<OpeningPeriod>|null`](#openingperiod) Liste des périodes d'ouverture ;
- `openingWeekdaysHours` [`Array<OpeningHour>|null`](#openinghour) Liste de tranches horaires pour les périodes d'ouverture ;
- `openingDatesHours` [`Array<DateOpeningHour>|null`](#dateopeninghour) Liste des dates d'ouverture avec horaires associés ;
- `pricingCode` `"free"|"conditioned"|"paid"|null` Type de tarif (`free` = gratuit, `conditioned` = gratuit sous conditions, `paid` = payant, `null` = non précisé) ;
- `pricingDescription` `string|null` Description des tarifs possibles en texte libre optionnel (pour un type de tarif à `conditioned` ou `paid`) ;
- `publicCodes` `string[]` Types de public (parmi `all`, `baby`, `children`, `teenager`, `adult` et `senior`). Si le tableau est vide ou contient `all`, l'activité est considérée comme tout public ;
- `phone` `string|null` Numéro de téléphone de contact (au format E164) ;
- `email` `string|null` Adresse email de contact ;
- `links` `Dictionary<string>|null` Dictionnaire de liens vers des sites externes ;
- `createdAt` `string` Date, heure et fuseau horaire de création (au format ISO 8601) ;
- `updatedAt` `string` Date, heure et fuseau horaire de dernière mise à jour (au format ISO 8601) ;
- `publishedAt` `string` Date, heure et fuseau horaire de publication (au format ISO 8601) ;
- `images` [`Array<Image>`](#image)` Liste d'images présentant le lieu ;
- `categories` [`Array<Category>`](#category)` Liste des catégories ;

> Une activité ayant `openingOnPeriods` à `true` mais ne proposant aucune période d'ouverture
> (`openingPeriods`) est considérée comme ouverte toute l'année.
> En raison des jeux de données importés dans la plateforme, il est possible que certaines
> activités n'aient ni lieu ni adresse.

### `Address`

Une adresse de lieu sur la plateforme.

- `type` `string` Type d'adresse parmi `housenumber`, `street`, `locality` ou `municipality` ;
- `address` `string|null` Numéro, voie, etc. ;
- `city` `string` Nom de la ville ;
- `postalCode` `string` Code postal de la ville ;
- `inseeCode` `string` Code INSEE de la ville ;
- `geometry` `object` Information géographique :
    - `type` `string` Type de données (toujours à `Point`) ;
    - `coordinates` `Tuple<float, float>` Tableau contenant la longitude (premier index) et la latitude (second index) ;

### `Accessibility`

Des données d'accessibilité sur un lieu.

- `physical` [`AccessibilityReview|null`](#accessibilityreview) Un avis sur l'accessibilité motrice par l'auteur du contenu.
- `visual` [`AccessibilityReview|null`](#accessibilityreview) Un avis sur l'accessibilité visuelle par l'auteur du contenu.
- `hearing` [`AccessibilityReview|null`](#accessibilityreview) Un avis sur l'accessibilité auditive par l'auteur du contenu.
- `cognitive` [`AccessibilityReview|null`](#accessibilityreview) Un avis sur l'accessibilité cognitive par l'auteur du contenu.
- `restrooms` [`AccessibilityReview|null`](#accessibilityreview) Un avis sur l'accessibilité des toilettes par l'auteur du contenu.
- `criteria` `object|null` Dictionnaire des données d'accessibilité.
    - `transportStation` `boolean|null` Accès à proximité : Proximité d'un arrêt de transport en commun ;
    - `transportInformation` `string|null` Accès à proximité : Informations complémentaires sur les transports ;
    - `innerParking` `boolean|null` Accès à proximité : Stationnement dans l'établissement ;
    - `innerParkingPrm` `boolean|null` Accès à proximité : Stationnements adaptés dans l'établissement ;
    - `externalParking` `boolean|null` Accès à proximité : Stationnement à proximité de l'établissement ;
    - `externalParkingPrm` `boolean|null` Accès à proximité : Stationnements adaptés à proximité de l'établissement ;
    - `externalPath` `boolean|null` Cheminement extérieur : Chemin extérieur ;
    - `externalPathStable` `boolean|null` Cheminement extérieur : Revêtement extérieur stable ;
    - `externalPathFlat` `boolean|null` Cheminement extérieur : Chemin extérieur de plain-pied ;
    - `externalPathElevator` `boolean|null` Cheminement extérieur : Ascenseur/élévateur ;
    - `externalPathStairsCount` `integer|null` Cheminement extérieur : Nombre de marches ;
    - `externalPathStairsVisibility` `boolean|null` Cheminement extérieur : Marches ou escalier sécurisé(es) ;
    - `externalPathStairsDirection` `string|null` Cheminement extérieur : Sens de circulation de l'escalier (`ascending`
      ou `descending`) ;
    - `externalPathStairsHandrail` `boolean|null` Cheminement extérieur : Main courante ;
    - `externalPathStairsRail` `string|null` Cheminement extérieur : Rampe (`none`, `fixed` ou `removable`) ;
    - `externalPathSlope` `boolean|null` Cheminement extérieur : Pente ;
    - `externalPathSlopeLevel` `string|null` Cheminement extérieur : Degré de difficulté de la pente (`low` ou `high`) ;
    - `externalPathSlopeLength` `string|null` Cheminement extérieur : Longueur de la pente (`short`, `medium`
      ou `long`) ;
    - `externalPathInclinationLevel` `string|null` Cheminement extérieur : Dévers (`none`, `low` ou `high`) ;
    - `externalPathGuideStrip` `boolean|null` Cheminement extérieur : Bande de guidage ;
    - `externalPathShrink` `boolean|null` Cheminement extérieur : Rétrécissement du chemin ;
    - `entranceVisible` `boolean|null` Entrée de l'établissement : Entrée facilement repérable ;
    - `entranceWindow` `boolean|null` Entrée de l'établissement : Entrée vitrée ;
    - `entranceWindowContrast` `boolean|null` Entrée de l'établissement : Repérage de la vitre ;
    - `entranceFlat` `boolean|null` Entrée de l'établissement : Entrée de plain-pied ;
    - `entranceElevator` `boolean|null` Entrée de l'établissement : Ascenseur/élévateur ;
    - `entranceStairsCount` `integer|null` Entrée de l'établissement : Nombre de marches ;
    - `entranceStairsVisibility` `boolean|null` Entrée de l'établissement : Repérage des marches ;
    - `entranceStairsDirection` `string|null` Entrée de l'établissement : Sens de circulation de l'escalier (`ascending`
      ou `descending`) ;
    - `entranceStairsHandrail` `boolean|null` Entrée de l'établissement : Main courante ;
    - `entranceStairsRail` `string|null` Entrée de l'établissement : Rampe (`none`, `fixed` ou `removable`) ;
    - `entranceCallDevice` `boolean|null` Entrée de l'établissement : Dispositif d'appel à l'entrée ;
    - `entranceCallDeviceType` `Array<string>|null` Entrée de l'établissement : Types de dispositifs d'appel à
      l'entrée (`button`, `intercom` ou `visiophone`) ;
    - `entranceSoundBeacon` `boolean|null` Entrée de l'établissement : Balise sonore à l'entrée ;
    - `entranceHumanHelp` `boolean|null` Entrée de l'établissement : Aide humaine ;
    - `entranceWidth` `integer|null` Entrée de l'établissement : Largeur de la porte ;
    - `entrancePrm` `boolean|null` Entrée de l'établissement : Entrée spécifique PMR ;
    - `entrancePrmInformation` `string|null` Entrée de l'établissement : Informations complémentaires ;
    - `entranceDoor` `boolean|null` Entrée de l'établissement : Présence d'une porte d'entrée ;
    - `entranceDoorMechanism` `string|null` Entrée de l'établissement : Manœuvre de la
      porte (`swing`, `sliding`, `turnstile` ou `revolving`) ;
    - `entranceDoorType` `string|null` Entrée de l'établissement : Type de porte (`manual` ou `automatic`) ;
    - `receptionVisible` `boolean|null` Accueil et prestation : Visibilité de la zone d'accueil ;
    - `receptionPersonnel` `string|null` Accueil et prestation : Personnel d'accueil (`none`, `trained`
      ou `nonTrained`) ;
    - `receptionHearingEquipments` `boolean|null` Accueil et prestation : Présence d'équipements d'aide à l'audition et
      à la communication ;
    - `receptionHearingEquipmentsType` `Array<string>|null` Accueil et prestation : Types
      d'équipements (`bim`, `bmp`, `lsf`, `lpc`, `sts` ou `other`) ;
    - `receptionPathFlat` `boolean|null` Accueil et prestation : Chemin entre l'entrée principale du bâtiment et
      l'accueil de l'établissement ;
    - `receptionPathElevator` `boolean|null` Accueil et prestation : Ascenseur/élévateur ;
    - `receptionPathStairsCount` `integer|null` Accueil et prestation : Nombre de marches ;
    - `receptionPathStairsVisibility` `boolean|null` Accueil et prestation : Repérage des marches ou de l'escalier ;
    - `receptionPathStairsDirection` `string|null` Accueil et prestation : Sens de circulation de
      l'escalier (`ascending` ou `descending`) ;
    - `receptionPathStairsHandrail` `boolean|null` Accueil et prestation : Main courante ;
    - `receptionPathStairsRail` `string|null` Accueil et prestation : Rampe (`none`, `fixed` ou `removable`) ;
    - `receptionPathShrink` `boolean|null` Accueil et prestation : Rétrécissement du chemin ;
    - `restrooms` `boolean|null` Accueil et prestation : Sanitaires ;
    - `restroomsAdapted` `boolean|null` Accueil et prestation : Sanitaires adaptés ;
    - `labels` `Array<string>|null` Marques ou labels (`dpt`, `mobalib`, `th` ou `other`) ;
    - `labelsOther` `string|null` Autres labels ;
    - `labelsCategories` `Array<string>|null` Familles de handicap concernées (`physical`, `visual`, `hearing`
      ou `cognitive`) ;
    - `comment` `string|null` Commentaire ;
    - `registryURL` `string|null` Adresse internet à laquelle le registre est consultable ;
    - `conformity` `boolean|null` Statut réglementaire de conformité de l'établissement ;

### `AccessibilityReview`

Un avis sur une catégorie d'accessibilité spécifique avec une note et un commentaire.

- `rating` `integer` Une note de l'accessibilité de 0 à 3 (0 = "Pas du tout accessible" et 3 = "Très accessible") ;
- `body` `string | null` Un commentaire optionnel expliquant la notation ;

### `OpeningHour`

Une tranche horaire pour un jour de la semaine.

- `weekday` `integer` Jour de la semaine de la tranche (au format ISO : lundi est 1, dimanche est 7) ;
- `start` `string` Horaire d'ouverture de la tranche (au format `heures:minutes`, par exemple `16:30`) ;
- `end` `string` Horaire de fermeture de la tranche (au format `heures:minutes`, par exemple `16:30`) ;

> Une tranche horaire contenant `00:00` à `start` et `end` indique une ouverture
> 24h/24 sur ce jour de la semaine.

### `DateOpeningHour`

Une tranche horaire pour une date donnée.

- `date` `string` Date d'ouverture (au format ISO 8601) ;
- `start` `string` Horaire d'ouverture de la tranche (au format `heures:minutes`, par exemple `16:30`) ;
- `end` `string` Horaire de fermeture de la tranche (au format `heures:minutes`, par exemple `16:30`) ;

> Une tranche horaire contenant `00:00` à `start` et `end` indique une ouverture
> 24h/24 sur ce jour de la semaine.

### `OpeningPeriod`

Une tranche de dates.

- `start` `string` Date de début d'ouverture (au format ISO 8601) ;
- `end` `string` Date de fin d'ouverture (au format ISO 8601) ;

### `Image`

Un fichier image.

- `id` `uuid` Identifiant UUID unique sur la plateforme (ne change jamais) ;
- `url` `string` URL du fichier image ;

### `Category`

Une catégorie.

- `id` `uuid` Identifiant UUID unique sur la plateforme (ne change jamais) ;
- `slug` `string` Identifiant texte unique sur la plateforme (peut changer dans le temps) ;
- `name` `string` Nom ;
