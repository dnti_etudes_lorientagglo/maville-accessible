**Nom du fichier** : `lieux.json`.

Le fichier contient à la racine une liste des lieux accessibles publiés sur la
plateforme. Chaque lieu listé correspond à un objet `Place`.

```json
[
  {
    "id": "36b3f1ed-f11d-4a0b-b349-f1e370edfef4",
    "slug": "maison-de-lagglomeration-lorient",
    "name": "Maison de l'agllomération",
    ...
  },
  {
    "id": "36b3f1ed-f11d-4a0b-b349-f1e370edfef4",
    "slug": "mairie-lorient",
    "name": "Mairie",
    ...
  },
  ...
]
```
