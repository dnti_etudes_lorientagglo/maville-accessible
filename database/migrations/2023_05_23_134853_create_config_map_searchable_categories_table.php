<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('config_map_searchable_categories', function (Blueprint $table) {
            $table->foreignUuid('config_id')
                ->constrained('configs')
                ->cascadeOnDelete();
            $table->foreignUuid('category_id')
                ->constrained('categories')
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('config_map_searchable_categories');
    }
};
