<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('sending_campaigns', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignUuid('campaign_type_id')
                ->constrained('sending_campaign_types')
                ->cascadeOnDelete();
            $table->jsonb('data');
            $table->timestamps();
            $table->timestamp('started_at')->nullable();
            $table->timestamp('finished_at')->nullable();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('sending_campaigns');
    }
};
