<?php

use App\Helpers\MigrationsHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->uuid('id')->primary();
            MigrationsHelper::sluggable($table);
            $table->jsonb('name');
            $table->jsonb('description')->nullable();
            $table->jsonb('body')->nullable();
            MigrationsHelper::contactable($table);
            MigrationsHelper::linkable($table);
            $table->jsonb('administrative_data')->nullable();
            $table->foreignUuid('organization_type_id')
                ->constrained('organization_types')
                ->cascadeOnDelete();
            MigrationsHelper::coverable($table);
            $table->timestamps();
            MigrationsHelper::publisable($table);
            MigrationsHelper::sourceable($table);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('organizations');
    }
};
