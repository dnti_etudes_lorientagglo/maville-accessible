<?php

use App\Helpers\MigrationsHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('places', function (Blueprint $table) {
            $table->uuid('id')->primary();
            MigrationsHelper::sluggable($table);
            $table->jsonb('name');
            $table->jsonb('description')->nullable();
            $table->jsonb('address');
            $table->jsonb('opening_hours')->nullable();
            MigrationsHelper::contactable($table);
            MigrationsHelper::linkable($table);
            MigrationsHelper::accessible($table);
            MigrationsHelper::coverable($table);
            $table->timestamps();
            MigrationsHelper::publisable($table);
            MigrationsHelper::sourceable($table);
            MigrationsHelper::ownable($table);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('places');
    }
};
