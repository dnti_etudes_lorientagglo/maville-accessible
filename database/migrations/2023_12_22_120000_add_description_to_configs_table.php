<?php

use App\Helpers\MigrationsHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table('configs', function (Blueprint $table) {
            MigrationsHelper::whenPostgreSQL(function () use ($table) {
                $table->dropConstrainedForeignId('cover_id');
                $table->dropConstrainedForeignId('logo_light_id');
                $table->dropConstrainedForeignId('logo_dark_id');
            });

            $table->jsonb('description')->nullable();
        });
    }

    public function down(): void
    {
        Schema::table('configs', function (Blueprint $table) {
            MigrationsHelper::whenPostgreSQL(function () use ($table) {
                $table->foreignUuid('cover_id')
                    ->nullable()
                    ->constrained('media')
                    ->nullOnDelete();
                $table->foreignUuid('logo_light_id')
                    ->nullable()
                    ->constrained('media')
                    ->nullOnDelete();
                $table->foreignUuid('logo_dark_id')
                    ->nullable()
                    ->constrained('media')
                    ->nullOnDelete();
            });

            $table->dropColumn('description');
        });
    }
};
