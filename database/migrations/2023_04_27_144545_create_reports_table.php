<?php

use App\Helpers\MigrationsHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->text('url');
            $table->text('body')->nullable();
            $table->foreignUuid('report_type_id')
                ->constrained('report_types')
                ->cascadeOnDelete();
            $table->nullableUuidMorphs('reportable');
            $table->timestamps();
            MigrationsHelper::softDeletable($table);
            MigrationsHelper::userOwnable($table);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('reports');
    }
};
