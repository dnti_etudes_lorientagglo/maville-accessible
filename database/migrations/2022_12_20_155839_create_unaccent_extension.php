<?php

use App\Helpers\MigrationsHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration {
    public function up(): void
    {
        MigrationsHelper::whenPostgreSQL(
            fn() => DB::statement('create extension if not exists unaccent'),
        );
    }

    public function down(): void
    {
        MigrationsHelper::whenPostgreSQL(
            fn() => DB::statement('drop extension if exists unaccent'),
        );
    }
};
