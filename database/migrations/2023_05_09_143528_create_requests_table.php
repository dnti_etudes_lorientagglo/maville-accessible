<?php

use App\Helpers\MigrationsHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->text('url');
            $table->text('body')->nullable();
            $table->jsonb('data')->nullable();
            $table->foreignUuid('request_type_id')
                ->constrained('request_types')
                ->cascadeOnDelete();
            $table->nullableUuidMorphs('requestable');
            $table->nullableUuidMorphs('resultable');
            $table->timestamps();
            MigrationsHelper::softDeletable($table);
            $table->timestamp('accepted_at')->nullable();
            MigrationsHelper::userOwnable($table);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('requests');
    }
};
