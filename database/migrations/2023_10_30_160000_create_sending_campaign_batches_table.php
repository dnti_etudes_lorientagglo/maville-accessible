<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('sending_campaign_batches', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignUuid('campaign_id')
                ->constrained('sending_campaigns')
                ->cascadeOnDelete();
            $table->integer('attempts');
            $table->jsonb('recipients');
            $table->timestamps();
            $table->timestamp('started_at')->nullable();
            $table->timestamp('running_at')->nullable();
            $table->timestamp('finished_at')->nullable();
            $table->timestamp('failed_at')->nullable();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('sending_campaign_batches');
    }
};
