<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('sending_campaign_types', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('code')->nullable()->unique();
            $table->jsonb('name');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('sending_campaign_types');
    }
};
