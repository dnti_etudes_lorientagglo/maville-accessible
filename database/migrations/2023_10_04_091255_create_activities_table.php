<?php

use App\Helpers\MigrationsHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->uuid('id')->primary();
            MigrationsHelper::sluggable($table);
            $table->jsonb('name');
            $table->jsonb('description')->nullable();
            $table->jsonb('body')->nullable();
            $table->jsonb('address')->nullable();
            $table->foreignUuid('place_id')
                ->nullable()
                ->constrained('places')
                ->nullOnDelete();
            $table->boolean('opening_on_periods');
            $table->jsonb('opening_periods')->nullable();
            $table->jsonb('opening_dates_hours')->nullable();
            $table->jsonb('opening_weekdays_hours')->nullable();
            MigrationsHelper::contactable($table);
            MigrationsHelper::linkable($table);
            MigrationsHelper::accessible($table);
            MigrationsHelper::coverable($table);
            $table->foreignUuid('activity_type_id')
                ->constrained('activity_types')
                ->cascadeOnDelete();
            $table->timestamps();
            MigrationsHelper::publisable($table);
            MigrationsHelper::sourceable($table);
            MigrationsHelper::ownable($table);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('activities');
    }
};
