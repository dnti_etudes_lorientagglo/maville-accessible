<?php

use App\Helpers\MigrationsHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('rating');
            $table->jsonb('body')->nullable();
            MigrationsHelper::accessible($table);
            $table->uuidMorphs('reviewable');
            $table->timestamps();
            MigrationsHelper::softDeletable($table);
            MigrationsHelper::userOwnable($table);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('reviews');
    }
};
