<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('imageables', function (Blueprint $table) {
            $table->uuidMorphs('imageable');
            $table->foreignUuid('media_id')
                ->constrained('media')
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('imageables');
    }
};
