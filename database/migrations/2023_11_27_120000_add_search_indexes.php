<?php

use App\Helpers\MigrationsHelper;
use App\Models\Activity;
use App\Models\Article;
use App\Models\Category;
use App\Models\Contracts\FulltextSearchable;
use App\Models\Media;
use App\Models\Organization;
use App\Models\Place;
use App\Models\Report;
use App\Models\ReportType;
use App\Models\Request;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Collection;

return new class extends Migration {
    public function up(): void
    {
        $this->models()->each(function (FulltextSearchable & Model $model) {
            MigrationsHelper::addSearchable($model->getTable());
            $model->getFulltextSearchService()->updateSearchColumnsForModel($model);
        });
    }

    public function down(): void
    {
        $this->models()->each(function (FulltextSearchable & Model $model) {
            MigrationsHelper::dropSearchable($model->getTable());
        });
    }

    private function models(): Collection
    {
        return new Collection([
            new Category(),
            new Media(),
            new ReportType(),
            new Report(),
            new Request(),
            new User(),
            new Organization(),
            new Activity(),
            new Article(),
            new Place(),
        ]);
    }
};
