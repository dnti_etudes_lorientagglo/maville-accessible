<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->timestamp('pinned_at')->nullable();
        });
        Schema::table('activities', function (Blueprint $table) {
            $table->timestamp('pinned_at')->nullable();
        });
    }

    public function down(): void
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->dropColumn('pinned');
        });
        Schema::table('activities', function (Blueprint $table) {
            $table->dropColumn('pinned');
        });
    }
};
