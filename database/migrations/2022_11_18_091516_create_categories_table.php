<?php

use App\Helpers\MigrationsHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->uuid('id')->primary();
            MigrationsHelper::sluggable($table);
            $table->jsonb('name');
            $table->jsonb('description')->nullable();
            $table->text('icon')->nullable();
            $table->string('color', 10)->nullable();
            $table->uuid('parent_id')->nullable();
            $table->timestamps();
            MigrationsHelper::publisable($table);
            MigrationsHelper::sourceable($table);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('categories');
    }
};
