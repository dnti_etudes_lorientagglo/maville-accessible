<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('categories_parents', function (Blueprint $table) {
            $table->foreignUuid('category_id')
                ->constrained('categories')
                ->cascadeOnDelete();
            $table->foreignUuid('parent_id')
                ->constrained('categories')
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('categories_parents');
    }
};
