<?php

use App\Helpers\MigrationsHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('review_replies', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->text('body');
            $table->foreignUuid('review_id')
                ->constrained('reviews')
                ->cascadeOnDelete();
            $table->timestamps();
            MigrationsHelper::softDeletable($table);
            MigrationsHelper::userOwnable($table);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('review_replies');
    }
};
