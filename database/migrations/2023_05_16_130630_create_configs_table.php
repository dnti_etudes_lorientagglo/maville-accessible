<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('configs', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignUuid('logo_light_id')
                ->nullable()
                ->constrained('media')
                ->nullOnDelete();
            $table->foreignUuid('logo_dark_id')
                ->nullable()
                ->constrained('media')
                ->nullOnDelete();
            $table->jsonb('map_center')->nullable();
            $table->integer('map_zoom')->nullable();
            $table->text('services')->nullable();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('configs');
    }
};
