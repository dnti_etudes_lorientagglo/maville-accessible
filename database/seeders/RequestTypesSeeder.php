<?php

namespace Database\Seeders;

use App\Helpers\LocaleHelper;
use App\Models\Enums\RequestTypeCode;
use App\Models\RequestType;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/**
 * Class RequestTypesSeeder.
 */
class RequestTypesSeeder extends Seeder
{
    /**
     * ReportTypesSeeder constructor.
     *
     * @param Translator $translator
     */
    public function __construct(
        private readonly Translator $translator,
    ) {
    }

    /**
     * Seed all organization types to the database.
     *
     * @return void
     */
    public function run(): void
    {
        DB::transaction(function () {
            $this->data()->each(
                static fn(array $attrs) => RequestType::query()->updateOrCreate($attrs[0], $attrs[1]),
            );
        });
    }

    /**
     * Get the seeding data.
     *
     * @return Collection
     */
    private function data(): Collection
    {
        return collect(RequestTypeCode::cases())->map(fn(RequestTypeCode $code) => [
            ['code' => $code->value],
            [
                'name' => [
                    LocaleHelper::locale() => $this->translator->get(
                        'resources.requestTypes.names.' . Str::camel($code->value),
                    ),
                ],
            ],
        ]);
    }
}
