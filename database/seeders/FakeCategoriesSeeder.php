<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class FakeCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Category::factory()
            ->has(Category::factory()->count(10), 'children')
            ->count(10)
            ->create();
    }
}
