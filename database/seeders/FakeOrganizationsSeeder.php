<?php

namespace Database\Seeders;

use App\Models\Organization;
use App\Models\OrganizationType;
use Illuminate\Database\Seeder;

class FakeOrganizationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $organizationType = OrganizationType::query()
            ->inRandomOrder()
            ->first();

        Organization::factory()
            ->for($organizationType)
            ->published()
            ->count(10)
            ->create();
    }
}
