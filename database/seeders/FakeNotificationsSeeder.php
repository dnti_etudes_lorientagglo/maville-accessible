<?php

namespace Database\Seeders;

use App\Models\Notification;
use App\Models\User;
use Illuminate\Database\Seeder;

class FakeNotificationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $userId = $this->command->ask('What user ID to notify:');

        $user = User::query()->findOrFail($userId);

        Notification::factory()
            ->for($user, 'notifiable')
            ->count(10)
            ->create();
    }
}
