<?php

namespace Database\Seeders;

use App\Features\FeatureName;
use App\Features\Features;
use App\Helpers\LocaleHelper;
use App\Models\Enums\RoleName;
use App\Models\Enums\RoleScopeName;
use App\Models\Permission;
use App\Models\Role;
use App\Policies\ActivityPolicy;
use App\Policies\ActivityTypePolicy;
use App\Policies\ArticlePolicy;
use App\Policies\CategoryPolicy;
use App\Policies\ConfigPolicy;
use App\Policies\MediaPolicy;
use App\Policies\OrganizationInvitationPolicy;
use App\Policies\OrganizationMemberPolicy;
use App\Policies\OrganizationPolicy;
use App\Policies\OrganizationTypePolicy;
use App\Policies\PermissionPolicy;
use App\Policies\PlacePolicy;
use App\Policies\ReportPolicy;
use App\Policies\ReportTypePolicy;
use App\Policies\RequestPolicy;
use App\Policies\RequestTypePolicy;
use App\Policies\ReviewPolicy;
use App\Policies\RolePolicy;
use App\Policies\SendingCampaignBatchPolicy;
use App\Policies\SendingCampaignPolicy;
use App\Policies\SendingCampaignTypePolicy;
use App\Policies\UserPolicy;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Spatie\Permission\PermissionRegistrar;
use Throwable;

/**
 * Class RolesAndPermissionsSeeder.
 */
class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * RolesAndPermissionsSeeder constructor.
     *
     * @param PermissionRegistrar $permissionRegistrar
     * @param Translator          $translator
     */
    public function __construct(
        private readonly PermissionRegistrar $permissionRegistrar,
        private readonly Translator $translator,
    ) {
    }

    /**
     * Seed all roles and permissions to the database.
     *
     * @return void
     */
    public function run(): void
    {
        DB::transaction(function () {
            $this->permissionRegistrar->forgetCachedPermissions();

            Permission::query()->delete();

            $this->createRole(RoleName::USER_ADMIN, RoleScopeName::GLOBAL);
            $this->createRole(RoleName::APP_ADMIN, RoleScopeName::GLOBAL);
            $this->createRole(RoleName::CONTENT_ADMIN, RoleScopeName::GLOBAL);
            $this->createRole(RoleName::MODERATOR, RoleScopeName::GLOBAL);

            $this->createRole(
                RoleName::COMMUNICATION_ADMIN,
                RoleScopeName::GLOBAL,
                condition: Features::check(FeatureName::COMMUNICATION),
            );

            $this->createRole(RoleName::ORGANIZATION_ADMIN, RoleScopeName::ORGANIZATION);
            $this->createRole(RoleName::ORGANIZATION_MEMBER, RoleScopeName::ORGANIZATION);

            $this->createPermissionsByRoles(
                $this->createPermissionsFrom(ConfigPolicy::permissions()),
                $this->createPermissionsFrom(PermissionPolicy::permissions()),
                $this->createPermissionsFrom(RolePolicy::permissions()),
                $this->createPermissionsFrom(UserPolicy::permissions()),
                $this->createPermissionsFrom(CategoryPolicy::permissions()),
                $this->createPermissionsFrom(MediaPolicy::permissions()),
                $this->createPermissionsFrom(ReportTypePolicy::permissions()),
                $this->createPermissionsFrom(ReportPolicy::permissions()),
                $this->createPermissionsFrom(RequestTypePolicy::permissions()),
                $this->createPermissionsFrom(RequestPolicy::permissions()),
                $this->createPermissionsFrom(ReviewPolicy::permissions()),
                $this->createPermissionsFrom(SendingCampaignTypePolicy::permissions()),
                $this->createPermissionsFrom(SendingCampaignBatchPolicy::permissions()),
                $this->createPermissionsFrom(SendingCampaignPolicy::permissions()),
                $this->createPermissionsFrom(OrganizationTypePolicy::permissions()),
                $this->createPermissionsFrom(OrganizationPolicy::permissions()),
                $this->createPermissionsFrom(OrganizationMemberPolicy::permissions()),
                $this->createPermissionsFrom(OrganizationInvitationPolicy::permissions()),
                $this->createPermissionsFrom(PlacePolicy::permissions()),
                $this->createPermissionsFrom(ArticlePolicy::permissions()),
                $this->createPermissionsFrom(ActivityTypePolicy::permissions()),
                $this->createPermissionsFrom(ActivityPolicy::permissions()),
            );
        });
    }

    /**
     * Create permissions and assign them to the roles.
     *
     * @param array $permissionsByRoles
     *
     * @return array
     */
    private function createPermissionsFrom(array $permissionsByRoles): array
    {
        $permissionsIdsByNames = collect();

        return collect($permissionsByRoles)->map(
            fn(array $permissions) => collect($permissions)->map(
                function (string $permission) use ($permissionsIdsByNames) {
                    return $permissionsIdsByNames->getOrPut(
                        $permission,
                        fn() => $this->createPermission($permission)->id,
                    );
                },
            ),
        )->toArray();
    }

    /**
     * Create all permissions by roles pivot from multiple role name/permissions ids map.
     *
     * @param array ...$permissionsByRoles
     *
     * @return void
     */
    private function createPermissionsByRoles(array ...$permissionsByRoles): void
    {
        DB::table('role_has_permissions')->delete();

        collect(array_merge_recursive(...$permissionsByRoles))->each(function (array $permissions, string $role) {
            try {
                /** @var Role $role */
                $role = Role::findByName($role);
            } catch (Throwable) {
                return;
            }

            DB::table('role_has_permissions')->insert(
                collect($permissions)->map(fn(string $id) => [
                    'role_id'       => $role->id,
                    'permission_id' => $id,
                ])->toArray(),
            );
        });
    }

    /**
     * Create a role if not already existing.
     *
     * @param RoleName      $type
     * @param RoleScopeName $scope
     * @param bool          $condition
     *
     * @return void
     */
    private function createRole(RoleName $type, RoleScopeName $scope, bool $condition = true): void
    {
        if (! $condition) {
            Role::query()
                ->where('name', $type->value)
                ->delete();

            return;
        }

        Role::query()->updateOrCreate([
            'name'       => $type->value,
            'guard_name' => 'web',
        ], [
            'scope_name'      => $scope->value,
            'translated_name' => [
                LocaleHelper::locale() => $this->translator->get('authorizations.roles.' . Str::camel($type->value)),
            ],
        ]);
    }

    /**
     * Create a permission if not already existing.
     *
     * @param string $name
     *
     * @return Permission
     */
    private function createPermission(string $name): Permission
    {
        [$resource, $permission] = explode('.', Str::camel($name));
        if ($permission) {
            $defaultPermissionLangKey = "authorizations.permissions.$permission";
            $defaultPermissionTranslation = $this->translator->get("authorizations.permissions.$permission");

            $translatedName = implode(' - ', [
                $this->translator->get("resources.common.types.$resource.plural"),
                $defaultPermissionTranslation === $defaultPermissionLangKey
                    ? $this->translator->get("authorizations.permissions.custom.$resource.$permission")
                    : $defaultPermissionTranslation,
            ]);
        } else {
            $translatedName = $this->translator->get("authorizations.permissions.custom.$name");
        }

        return Permission::query()->updateOrCreate([
            'name'       => $name,
            'guard_name' => 'web',
        ], [
            'translated_name' => [
                LocaleHelper::locale() => $translatedName,
            ],
        ]);
    }
}
