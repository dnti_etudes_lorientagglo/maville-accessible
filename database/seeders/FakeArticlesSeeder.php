<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Database\Seeder;

class FakeArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $category = Category::query()
            ->inRandomOrder()
            ->first();

        Article::factory()
            ->hasAttached($category)
            ->published()
            ->count(10)
            ->create();
    }
}
