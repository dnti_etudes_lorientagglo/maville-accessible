<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

/**
 * Class DatabaseSeeder.
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $this->call([
            ConfigSeeder::class,
            RolesAndPermissionsSeeder::class,
            OrganizationTypesSeeder::class,
            SendingCampaignTypesSeeder::class,
            ReportTypesSeeder::class,
            RequestTypesSeeder::class,
            ActivityTypesSeeder::class,
        ]);
    }
}
