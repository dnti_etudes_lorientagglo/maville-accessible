<?php

namespace Database\Seeders;

use App\Models\Config;
use Illuminate\Database\Seeder;

/**
 * Class ConfigSeeder.
 */
class ConfigSeeder extends Seeder
{
    /**
     * Seed app config to the database.
     *
     * @return void
     */
    public function run(): void
    {
        if (Config::query()->doesntExist()) {
            $config = new Config();
            $config->save();
        }
    }
}
