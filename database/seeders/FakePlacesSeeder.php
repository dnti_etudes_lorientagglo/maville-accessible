<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Place;
use Illuminate\Database\Seeder;

class FakePlacesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $category = Category::query()
            ->inRandomOrder()
            ->first();

        Place::factory()
            ->hasAttached($category)
            ->published()
            ->count(10)
            ->create();
    }
}
