<?php

namespace Database\Seeders;

use App\Helpers\LocaleHelper;
use App\Models\Enums\ReportTypeCode;
use App\Models\ReportType;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/**
 * Class ReportTypesSeeder.
 */
class ReportTypesSeeder extends Seeder
{
    /**
     * Report types with a global scope.
     */
    private const GLOBAL_TYPE_CODES = [
        ReportTypeCode::BUG,
        ReportTypeCode::FEAT,
    ];

    /**
     * ReportTypesSeeder constructor.
     *
     * @param Translator $translator
     */
    public function __construct(
        private readonly Translator $translator,
    ) {
    }

    /**
     * Seed all organization types to the database.
     *
     * @return void
     */
    public function run(): void
    {
        DB::transaction(function () {
            $this->data()->each(
                static fn(array $attrs) => ReportType::query()->updateOrCreate($attrs[0], $attrs[1]),
            );
        });
    }

    /**
     * Get the seeding data.
     *
     * @return Collection
     */
    private function data(): Collection
    {
        return collect(ReportTypeCode::cases())->map(fn(ReportTypeCode $code) => [
            ['code' => $code->value],
            [
                'name'   => [
                    LocaleHelper::locale() => $this->translator->get(
                        'resources.reportTypes.names.' . Str::camel($code->value),
                    ),
                ],
                'global' => in_array($code, self::GLOBAL_TYPE_CODES),
            ],
        ]);
    }
}
