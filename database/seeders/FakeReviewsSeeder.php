<?php

namespace Database\Seeders;

use App\Models\Activity;
use App\Models\Contracts\Reviewable;
use App\Models\Place;
use App\Models\Review;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class FakeReviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $modelClass = $this->command->choice('What reviewable to feed in:', [
            Place::class    => 'places',
            Activity::class => 'activities',
        ]);
        $modelId = $this->command->ask('What reviewable ID to use:');

        /** @var Model&Reviewable $model */
        $model = $modelClass::query()->findOrFail($modelId);

        $user = User::query()
            ->inRandomOrder()
            ->first();

        Review::factory()
            ->for($model, 'reviewable')
            ->for($user, 'ownerUser')
            ->count(10)
            ->create();
    }
}
