<?php

namespace Database\Factories;

use App\Helpers\LocaleHelper;
use App\Models\Organization;
use Database\Factories\Composables\PublishableFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * Class OrganizationFactory.
 *
 * @extends Factory<Organization>
 */
class OrganizationFactory extends Factory
{
    use PublishableFactory;

    /**
     * {@inheritDoc}
     */
    public function definition(): array
    {
        return [
            'name' => [LocaleHelper::locale() => Str::ucfirst(fake()->company())],
            'body' => [LocaleHelper::locale() => Str::ucfirst(fake()->randomHtml(1))],
        ];
    }
}
