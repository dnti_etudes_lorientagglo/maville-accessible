<?php

namespace Database\Factories;

use App\Helpers\LocaleHelper;
use App\Models\Place;
use App\Services\App\AppEnv;
use Database\Factories\Composables\PublishableFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * Class PlaceFactory.
 *
 * @extends Factory<Place>
 */
class PlaceFactory extends Factory
{
    use PublishableFactory;

    /**
     * {@inheritDoc}
     */
    public function definition(): array
    {
        $publicEnv = app(AppEnv::class)->publicEnv();
        $mapCenter = $publicEnv['services']['map']['center']['geometry']['coordinates'];

        return [
            'name'    => [LocaleHelper::locale() => Str::ucfirst(fake()->word())],
            'address' => [
                'type'       => 'housenumber',
                'address'    => fake()->streetAddress(),
                'city'       => fake()->city(),
                'postalCode' => fake()->postcode(),
                'inseeCode'  => fake()->postcode(),
                'geometry'   => [
                    'type'        => 'Point',
                    'coordinates' => [
                        fake()->longitude($mapCenter[0] - 0.05, $mapCenter[0] + 0.05),
                        fake()->longitude($mapCenter[1] - 0.05, $mapCenter[1] + 0.05),
                    ],
                ],
            ],
        ];
    }
}
