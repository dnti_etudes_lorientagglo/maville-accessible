<?php

namespace Database\Factories;

use App\Helpers\LocaleHelper;
use App\Models\Notification;
use App\Notifications\Reports\NewPendingReport;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class NotificationFactory.
 *
 * @extends Factory<Notification>
 */
class NotificationFactory extends Factory
{
    /**
     * {@inheritDoc}
     */
    public function definition(): array
    {
        return [
            'type' => NewPendingReport::class,
            'data' => [
                'data' => [
                    'reportTypeName' => [
                        LocaleHelper::locale() => 'Fake notification ' . implode(' ', fake()->words()),
                    ],
                    'userName'       => fake()->name(),
                ],
            ],
        ];
    }
}
