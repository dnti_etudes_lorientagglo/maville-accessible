<?php

namespace Database\Factories\Composables;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Trait PublishableFactory.
 *
 * @mixin Factory
 */
trait PublishableFactory
{
    public function published(): static
    {
        return $this->state(fn() => [
            'published_at' => fake()->dateTimeBetween('-1 year'),
        ]);
    }
}
