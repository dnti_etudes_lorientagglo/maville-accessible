<?php

namespace Database\Factories;

use App\Helpers\LocaleHelper;
use App\Models\Review;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * Class ReviewFactory.
 *
 * @extends Factory<Review>
 */
class ReviewFactory extends Factory
{
    /**
     * {@inheritDoc}
     */
    public function definition(): array
    {
        return [
            'rating' => rand(0, 10) ? rand(3, 5) : rand(1, 2),
            'body'   => rand(0, 5)
                ? [LocaleHelper::locale() => Str::ucfirst(fake()->text(500))]
                : null,
        ];
    }
}
