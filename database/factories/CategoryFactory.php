<?php

namespace Database\Factories;

use App\Helpers\LocaleHelper;
use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * Class CategoryFactory.
 *
 * @extends Factory<Category>
 */
class CategoryFactory extends Factory
{
    /**
     * {@inheritDoc}
     */
    public function definition(): array
    {
        return [
            'name' => [LocaleHelper::locale() => Str::ucfirst(fake()->word())],
        ];
    }
}
