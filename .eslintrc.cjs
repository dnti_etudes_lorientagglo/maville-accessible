module.exports = {
  root: true,
  env: {
    es2021: true,
    jest: true,
    'vue/setup-compiler-macros': true,
  },
  extends: [
    'airbnb-base',
    'airbnb-typescript/base',
    'plugin:vue/vue3-recommended',
    '@vue/eslint-config-typescript',
  ],
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: '@typescript-eslint/parser',
    project: './tsconfig.json',
  },
  rules: {
    'no-console': 'error',
    'no-debugger': 'error',
    // See https://stackoverflow.com/a/63767419
    'no-unused-vars': ['off'],
    // See https://youtrack.jetbrains.com/issue/WEB-21182
    'import/order': ['off'],
    // See https://github.com/typescript-eslint/typescript-eslint/issues/4299
    'func-call-spacing': ['off'],
    'no-spaced-func': ['off'],
    '@typescript-eslint/func-call-spacing': ['error'],
    'object-curly-newline': ['off'],
    'vue/no-setup-props-destructure': ['off'],
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        'js': 'never',
        'jsx': 'never',
        'ts': 'never',
        'tsx': 'never',
      },
    ],
  },
  settings: {
    'import/resolver': {
      typescript: {},
    },
  },
};
