<?php

namespace App\JsonApi\Filters;

use App\JsonApi\Concerns\Core\IsDeserializable;
use Closure;
use Illuminate\Contracts\Database\Eloquent\Builder;
use LaravelJsonApi\Eloquent\Contracts\Filter;
use LaravelJsonApi\Eloquent\Filters\Concerns\IsSingular;

/**
 * Class ClosureFilter.
 */
class ClosureFilter implements Filter
{
    use IsSingular;
    use IsDeserializable;

    /**
     * Create a new filter.
     *
     * @param string  $name
     * @param Closure $using
     *
     * @return ClosureFilter
     */
    public static function make(string $name, Closure $using): static
    {
        return new static($name, $using);
    }

    /**
     * ClosureFilter constructor.
     *
     * @param string  $name
     * @param Closure $using
     */
    public function __construct(
        private readonly string $name,
        private readonly Closure $using,
    ) {
    }

    /**
     * Get the key for the filter.
     *
     * @return string
     */
    public function key(): string
    {
        return $this->name;
    }

    /**
     * Apply the filter to the query.
     *
     * @param Builder $query
     * @param mixed   $value
     *
     * @return Builder
     */
    public function apply($query, $value): Builder
    {
        call_user_func_array($this->using, [$query, $this->deserializeValue($value)]);

        return $query;
    }
}
