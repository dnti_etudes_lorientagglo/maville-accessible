<?php

namespace App\JsonApi\Filters;

use App\Helpers\LocaleHelper;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str as StrHelper;
use LaravelJsonApi\Eloquent\Contracts\Filter;
use LaravelJsonApi\Eloquent\Filters\Concerns\IsSingular;

/**
 * Class SearchNameFilter.
 */
class SearchNameFilter implements Filter
{
    use IsSingular;

    /**
     * Create a new filter.
     *
     * @param string $name
     *
     * @return SearchNameFilter
     */
    public static function make(string $name = 'searchName'): self
    {
        return new static($name);
    }

    /**
     * SearchFilter constructor.
     *
     * @param string $name
     */
    public function __construct(private readonly string $name)
    {
    }

    /**
     * Get the key for the filter.
     *
     * @return string
     */
    public function key(): string
    {
        return $this->name;
    }

    /**
     * Apply the filter to the query.
     *
     * @param Builder $query
     * @param mixed   $value
     *
     * @return Builder
     */
    public function apply($query, $value): Builder
    {
        return $query->where(
            DB::raw('LOWER(UNACCENT(name->>\'' . LocaleHelper::locale() . '\'))'),
            StrHelper::of($value)->lower()->ascii()->value(),
        );
    }
}
