<?php

namespace App\JsonApi\Filters;

use Illuminate\Contracts\Database\Eloquent\Builder;
use LaravelJsonApi\Eloquent\Contracts\Filter;
use LaravelJsonApi\Eloquent\Filters\Concerns\IsSingular;

/**
 * Class SearchFilter.
 */
class SearchFilter implements Filter
{
    use IsSingular;

    /**
     * Create a new filter.
     *
     * @param string $name
     *
     * @return SearchFilter
     */
    public static function make(string $name = 'search'): self
    {
        return new static($name);
    }

    /**
     * SearchFilter constructor.
     *
     * @param string $name
     */
    public function __construct(private readonly string $name)
    {
    }

    /**
     * Get the key for the filter.
     *
     * @return string
     */
    public function key(): string
    {
        return $this->name;
    }

    /**
     * Apply the filter to the query.
     *
     * @param Builder $query
     * @param mixed   $value
     *
     * @return Builder
     */
    public function apply($query, $value): Builder
    {
        return $query->whereSearch($value);
    }
}
