<?php

namespace App\JsonApi\Concerns\Reviewable;

use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Trait ValidatesReviewableQuery.
 */
trait ValidatesReviewableQuery
{
    /**
     * Get the specific rules.
     *
     * @return array
     */
    protected function reviewableRules(): array
    {
        return [
            'withReviewsSummary' => ['bail', 'filled', JsonApiRule::boolean()->asString()],
        ];
    }
}
