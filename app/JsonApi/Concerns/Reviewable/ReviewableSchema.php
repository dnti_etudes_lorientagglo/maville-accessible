<?php

namespace App\JsonApi\Concerns\Reviewable;

use App\Helpers\QueryHelper;
use App\JsonApi\Concerns\Core\BaseSchema;
use App\Models\Enums\AccessibilityCategory;
use App\Models\Enums\AccessibilityCriteria;
use App\Models\Review;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Casts\ArrayObject;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use LaravelJsonApi\Core\Resources\Relation;
use LaravelJsonApi\Eloquent\Fields\ArrayHash;
use LaravelJsonApi\Eloquent\Fields\Relations\HasMany;

/**
 * Trait ReviewableSchema.
 *
 * @mixin BaseSchema
 */
trait ReviewableSchema
{
    /**
     * Transform the reviewable query to load average ratings.
     *
     * @param EloquentBuilder $query
     *
     * @return EloquentBuilder
     */
    private function reviewableQuery(EloquentBuilder $query): EloquentBuilder
    {
        return QueryHelper::clarifySelects($query)->when($this->shouldLoadReviewsSummary())->addSelect([
            'reviews_summary' => static function (Builder $subQuery) use ($query) {
                $nullCase = fn(string $column) => "case when coalesce($column, 'null') = 'null' then 0 else 1 end";
                $criteriaArray = fn(AccessibilityCategory $c) => AccessibilityCriteria::all()
                    ->filter(fn(AccessibilityCriteria $i) => in_array($c, $i->accessibilityCategories))
                    ->map(fn(AccessibilityCriteria $i) => "'$i->id'")
                    ->join(', ');
                $criteriaColumn = fn(AccessibilityCategory $c) => "(" .
                    "coalesce(reviewable_reviews.accessibility->'criteria', '{}')" .
                    " ??| array[{$criteriaArray($c)}])::int";

                $summaryColumns = [
                    'total'  => ['COUNT', 'reviewable_reviews.*'],
                    'rating' => ['AVG', 'reviewable_reviews.rating'],
                    ...collect(range(1, 5))
                        ->mapWithKeys(static fn(int $r) => [
                            "rating{$r}_total" => ['SUM', "case when reviewable_reviews.rating = $r then 1 else 0 end"],
                        ]),
                    ...collect(AccessibilityCategory::cases())
                        ->mapWithKeys(static fn(AccessibilityCategory $c) => [
                            "accessibility_{$c->value}_rating"  => [
                                'AVG',
                                "cast(reviewable_reviews.accessibility->'$c->value'->'rating' as int)",
                            ],
                            "accessibility_{$c->value}_total"   => [
                                'SUM',
                                $nullCase("reviewable_reviews.accessibility->'$c->value'->'rating'"),
                            ],
                            "accessibility_{$c->value}_details" => [
                                'SUM',
                                'GREATEST(' .
                                $nullCase("reviewable_reviews.accessibility->'$c->value'->'body'") . ', ' .
                                $criteriaColumn($c) .
                                ')',
                            ],
                        ]),
                ];

                $summaryJsonArgs = collect($summaryColumns)
                    ->reduce(static fn(Collection $args, array $params, string $key) => $args->push(
                        "'$key'",
                        "$params[0]($params[1])",
                    ), collect())
                    ->join(', ');

                $model = $query->getModel();

                $subQuery
                    ->select(DB::raw("json_build_object($summaryJsonArgs)"))
                    ->from((new Review())->getTable(), 'reviewable_reviews')
                    ->where('reviewable_reviews.reviewable_type', $model->getMorphClass())
                    ->whereColumn('reviewable_reviews.reviewable_id', $query->qualifyColumn($model->getKeyName()));
            },
        ]);
    }

    /**
     * Get the specific fields.
     *
     * @return array[]
     */
    private function reviewableFields(): array
    {
        return [
            ArrayHash::make('reviewsSummary')
                ->hidden(fn() => ! $this->shouldLoadReviewsSummary())
                ->serializeUsing(static fn(ArrayObject | null $value) => ($value && $value['total'] ? [
                    'rating'        => $value['rating'],
                    'total'         => $value['total'],
                    'distribution'  => [
                        '1' => $value['rating1_total'],
                        '2' => $value['rating2_total'],
                        '3' => $value['rating3_total'],
                        '4' => $value['rating4_total'],
                        '5' => $value['rating5_total'],
                    ],
                    'accessibility' => collect(AccessibilityCategory::cases())
                        ->mapWithKeys(static fn(AccessibilityCategory $c) => [
                            $c->value => [
                                'rating'  => $value["accessibility_{$c->value}_rating"],
                                'total'   => $value["accessibility_{$c->value}_total"],
                                'details' => $value["accessibility_{$c->value}_details"],
                            ],
                        ])
                        ->all(),
                ] : null))
                ->readOnly(),
            HasMany::make('reviews')
                ->serializeUsing(static fn(Relation $r) => $r->withoutLinks())
                ->cannotEagerLoad()
                ->readOnly(),
        ];
    }

    /**
     * Check if reviews summary should be loaded.
     *
     * @return bool
     */
    private function shouldLoadReviewsSummary(): bool
    {
        return $this->uriType() === request()->route('resource_type')
            && filter_var(request()->query('withReviewsSummary'), FILTER_VALIDATE_BOOL) === true;
    }
}
