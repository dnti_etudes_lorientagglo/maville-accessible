<?php

namespace App\JsonApi\Concerns\UserOwnable;

use App\Helpers\AuthHelper;
use App\JsonApi\Sorting\ClosureSort;
use Illuminate\Contracts\Database\Eloquent\Builder;
use LaravelJsonApi\Core\Resources\Relation;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsTo;

/**
 * Trait UserOwnableSchema.
 */
trait UserOwnableSchema
{
    /**
     * Get the specific fields.
     *
     * @return array[]
     */
    private function userOwnableFields(): array
    {
        return [
            BelongsTo::make('ownerUser')
                ->readOnly()
                ->type('users')
                ->serializeUsing(static fn(Relation $r) => $r->withoutLinks()),
        ];
    }

    /**
     * Get the specific sortables.
     *
     * @return array[]
     */
    private function userOwnableSortables(): array
    {
        return [
            ClosureSort::make('userOwned', static function (Builder $q, string $dir) {
                $authId = AuthHelper::user()?->id;
                $userIdCol = $q->qualifyColumn('owner_user_id');

                return $authId ? $q->orderByRaw(
                    '(' . $userIdCol . ' is not null and ' . $userIdCol . ' = ?) ' . $dir,
                    [$authId],
                ) : $q;
            }),
        ];
    }
}
