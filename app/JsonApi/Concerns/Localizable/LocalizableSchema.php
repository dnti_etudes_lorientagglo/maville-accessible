<?php

namespace App\JsonApi\Concerns\Localizable;

use App\Helpers\QueryHelper;
use App\JsonApi\Filters\ClosureFilter;
use Illuminate\Contracts\Database\Eloquent\Builder;
use LaravelJsonApi\Core\Resources\Relation;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsToMany;

/**
 * Trait LocalizableSchema.
 */
trait LocalizableSchema
{
    /**
     * Get the specific fields.
     *
     * @return array
     */
    protected function localizableFields(): array
    {
        return [
            BelongsToMany::make('places')
                ->serializeUsing(fn(Relation $r) => $r->withoutLinks())
                ->mustValidate(),
        ];
    }

    /**
     * Get the specific fields.
     *
     * @return array
     */
    protected function localizableFitlers(): array
    {
        return [
            ClosureFilter::make(
                'place',
                static fn(Builder $query, array $places) => QueryHelper::whereRelationIn(
                    $query,
                    'places',
                    $places,
                ),
            ),
        ];
    }
}
