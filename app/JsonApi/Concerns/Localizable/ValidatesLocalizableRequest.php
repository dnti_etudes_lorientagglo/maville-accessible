<?php

namespace App\JsonApi\Concerns\Localizable;

use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Trait ValidatesLocalizableRequest.
 */
trait ValidatesLocalizableRequest
{
    /**
     * Get the specific rules.
     *
     * @return array
     */
    protected function localizableRules(): array
    {
        return [
            'places' => [JsonApiRule::toMany()],
        ];
    }
}
