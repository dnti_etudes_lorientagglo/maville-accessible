<?php

namespace App\JsonApi\Concerns\Localizable;

/**
 * Trait ValidatesLocalizableQuery.
 */
trait ValidatesLocalizableQuery
{
    /**
     * Get the specific rules.
     *
     * @return array
     */
    protected function localizableRules(): array
    {
        return [
            'filter.place'   => ['filled', 'array'],
            'filter.place.*' => ['required', 'string'],
        ];
    }
}
