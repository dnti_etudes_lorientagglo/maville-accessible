<?php

namespace App\JsonApi\Concerns\Reactable;

use App\Helpers\JsonApiHelper;
use App\Helpers\QueryHelper;
use App\Models\Reaction;
use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use LaravelJsonApi\Eloquent\Fields\ArrayHash;
use LaravelJsonApi\Eloquent\Fields\Relations\HasOne;

/**
 * Trait ReviewableSchema.
 */
trait ReactableSchema
{
    /**
     * Transform the reactable query to load average ratings.
     *
     * @param EloquentBuilder $query
     *
     * @return EloquentBuilder
     */
    private function reactableQuery(EloquentBuilder $query): EloquentBuilder
    {
        return QueryHelper::clarifySelects($query)->when($this->shouldLoadReactionsSummary())->addSelect([
            'reactions_summary' => static function (Builder $subQuery) use ($query) {
                $model = $query->getModel();

                $subQuery
                    ->selectRaw('json_object_agg(reactable_from_agg.reaction, reactable_from_agg.total)')
                    ->from(
                        static fn(Builder $fromQuery) => $fromQuery
                            ->select('reactable_from.reaction as reaction', DB::raw('count(*) as total'))
                            ->from((new Reaction())->getTable(), 'reactable_from')
                            ->where('reactable_from.reactable_type', $model->getMorphClass())
                            ->whereColumn('reactable_from.reactable_id', $query->qualifyColumn($model->getKeyName()))
                            ->groupBy('reactable_from.reaction'),
                        'reactable_from_agg',
                    );
            },
        ]);
    }

    /**
     * Get the specific fields.
     *
     * @return array[]
     */
    private function reactableFields(): array
    {
        return [
            ArrayHash::make('reactionsSummary')
                ->hidden(fn() => ! $this->shouldLoadReactionsSummary())
                ->readOnly(),
            HasOne::make('currentReaction')
                ->readOnly()
                ->type('reactions')
                ->serializeUsing(JsonApiHelper::noLinks()),
        ];
    }

    /**
     * Check if reactions summary should be loaded.
     *
     * @return bool
     */
    private function shouldLoadReactionsSummary(): bool
    {
        return filter_var(request()->query('withReactionsSummary'), FILTER_VALIDATE_BOOL) === true;
    }
}
