<?php

namespace App\JsonApi\Concerns\Pinnable;

use App\JsonApi\Sorting\ClosureSort;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use LaravelJsonApi\Eloquent\Fields\DateTime;

/**
 * Trait PinnableSchema.
 */
trait PinnableSchema
{
    /**
     * Get the specific fields.
     *
     * @return array[]
     */
    private function pinnableFields(): array
    {
        return [
            DateTime::make('pinnedAt')
                ->readOnly()
                ->retainTimezone()
                ->sortable(),
        ];
    }

    /**
     * Get the specific sortables.
     *
     * @return array[]
     */
    private function pinnableSortables(): array
    {
        return [
            ClosureSort::make(
                'pinned',
                static fn(Builder $query, string $dir) => $query
                    ->orderBy(DB::raw($query->qualifyColumn('pinned_at') . ' is not null'), $dir),
            ),
        ];
    }
}
