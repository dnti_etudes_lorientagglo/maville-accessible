<?php

namespace App\JsonApi\Concerns\Publishable;

use App\Helpers\AuthHelper;
use App\Helpers\PublishableHelper;
use App\JsonApi\Filters\ClosureFilter;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use LaravelJsonApi\Eloquent\Fields\DateTime;

/**
 * Trait PublishableSchema.
 */
trait PublishableSchema
{
    /**
     * Filter the publishable query where applicable.
     *
     * @param Request|null $request
     * @param Builder      $query
     * @param Closure      $privateFilter
     *
     * @return Builder
     *
     * @throws AuthenticationException
     */
    private function publishableQuery(
        ?Request $request,
        Builder $query,
        Closure $privateFilter,
    ): Builder {
        $private = filter_var(Arr::get($request->query(), 'filter.private'), FILTER_VALIDATE_BOOL);
        if ($private) {
            $user = AuthHelper::user();
            if (! $user) {
                throw new AuthenticationException();
            }

            if (! call_user_func($privateFilter)) {
                $query->whereNull('id');
            }
        } else {
            PublishableHelper::publishedQuery($query);
        }

        return $query;
    }

    /**
     * Get the specific fields.
     *
     * @return array[]
     */
    private function publishableFields(): array
    {
        return [
            DateTime::make('publishedAt')
                ->readOnly()
                ->retainTimezone()
                ->sortable(),
        ];
    }

    /**
     * Get the specific filters.
     *
     * @return array[]
     */
    private function publishableFilters(): array
    {
        return [
            // Filtering is done through publishable query.
            ClosureFilter::make('private', static fn() => null),
        ];
    }
}
