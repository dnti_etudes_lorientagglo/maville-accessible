<?php

namespace App\JsonApi\Concerns\Publishable;

use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Trait ValidatesPublishableQuery.
 */
trait ValidatesPublishableQuery
{
    /**
     * Get the specific rules.
     *
     * @return array[]
     */
    public function publishableRules(): array
    {
        return [
            'filter.private' => ['bail', 'nullable', JsonApiRule::boolean()->asString()],
        ];
    }
}
