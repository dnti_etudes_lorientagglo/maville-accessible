<?php

namespace App\JsonApi\Concerns\Mediable;

use LaravelJsonApi\Core\Resources\Relation;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsToMany;

/**
 * Trait MediableSchema.
 */
trait MediableSchema
{
    /**
     * Get the specific fields.
     *
     * @return array
     */
    protected function mediableFields(): array
    {
        return [
            BelongsToMany::make('media')
                ->serializeUsing(fn(Relation $r) => $r->withoutLinks()),
        ];
    }
}
