<?php

namespace App\JsonApi\Concerns\Mediable;

use App\Rules\AllowsMediaRule;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Trait ValidatesMediableRequest.
 */
trait ValidatesMediableRequest
{
    /**
     * Get the specific rules.
     *
     * @return array
     */
    protected function mediableRules(): array
    {
        return [
            'media' => [
                'bail',
                JsonApiRule::toMany(),
                AllowsMediaRule::make()->withPrevious($this->model()?->media),
            ],
        ];
    }
}
