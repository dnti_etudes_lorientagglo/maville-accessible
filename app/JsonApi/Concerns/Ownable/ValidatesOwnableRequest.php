<?php

namespace App\JsonApi\Concerns\Ownable;

use App\Helpers\AuthHelper;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Trait ValidatesOwnableRequest.
 *
 * @mixin ResourceRequest
 */
trait ValidatesOwnableRequest
{
    /**
     * Get the specific rules.
     *
     * @return array
     */
    private function ownableRules(): array
    {
        $user = AuthHelper::user();
        $model = $this->schema()::model();
        $policy = Gate::getPolicyFor($model);

        if ($user->can($policy::ADMIN, $model)) {
            return [
                'ownerOrganization' => ['nullable', JsonApiRule::toOne()],
            ];
        }

        $actingForOrganization = $user->getActingForOrganization();
        if ($actingForOrganization) {
            return [
                'ownerOrganization'    => ['required', JsonApiRule::toOne()],
                'ownerOrganization.id' => ['required', Rule::in([$actingForOrganization->organization_id])],
            ];
        }

        return [
            'ownerOrganization' => ['prohibited', JsonApiRule::toOne()],
        ];
    }
}
