<?php

namespace App\JsonApi\Concerns\Ownable;

use App\Helpers\AuthHelper;
use App\JsonApi\Concerns\UserOwnable\UserOwnableSchema;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Gate;
use LaravelJsonApi\Core\Resources\Relation;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsTo;

/**
 * Trait OwnableSchema.
 */
trait OwnableSchema
{
    use UserOwnableSchema;

    /**
     * Filter the ownable query if possible.
     *
     * @param Builder $query
     *
     * @return bool
     */
    private function ownableQuery(Builder $query): bool
    {
        $user = AuthHelper::user();
        $model = $query->getModel();
        $policy = Gate::getPolicyFor($model);

        if ($user->can($policy::ADMIN, $model::class)) {
            return true;
        }

        $actingForOrganization = $user->getActingForOrganization();
        if ($actingForOrganization) {
            $query
                ->whereNotNull('owner_organization_id')
                ->where('owner_organization_id', $actingForOrganization->organization_id);
        } else {
            $query
                ->whereNull('owner_organization_id')
                ->whereNotNull('owner_user_id')
                ->where('owner_user_id', $user->id);
        }

        return true;
    }

    /**
     * Get the specific fields.
     *
     * @return array[]
     */
    private function ownableFields(): array
    {
        return [
            ...$this->userOwnableFields(),
            BelongsTo::make('ownerOrganization')
                ->type('organizations')
                ->serializeUsing(static fn(Relation $r) => $r->withoutLinks()),
        ];
    }

    /**
     * Get the specific sortables.
     *
     * @return array[]
     */
    private function ownableSortables(): array
    {
        return [
            ...$this->userOwnableSortables(),
        ];
    }
}
