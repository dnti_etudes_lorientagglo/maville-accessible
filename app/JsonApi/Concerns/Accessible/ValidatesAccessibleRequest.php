<?php

namespace App\JsonApi\Concerns\Accessible;

use App\Helpers\RuleHelper;
use App\Models\Enums\AccessibilityCategory;
use App\Rules\TranslationRule;
use Closure;

/**
 * Trait ValidatesAccessibleRequest.
 */
trait ValidatesAccessibleRequest
{
    /**
     * Get the specific rules.
     *
     * @return array
     */
    protected function accessibleRules(): array
    {
        return [
            ...RuleHelper::arrayHash('accessibility', [
                'criteria'   => ['nullable', 'array'],
                'criteria.*' => [
                    'required',
                    static function (string $attribute, mixed $value, Closure $fail) {
                        if (! is_bool($value) && ! is_string($value) && ! is_integer($value) && ! is_array($value)) {
                            $fail(__('validation.in'));
                        }
                    },
                ],
                ...collect(AccessibilityCategory::cases())
                    ->reduce(static fn(array $rules, AccessibilityCategory $category) => [
                        ...$rules,
                        ...RuleHelper::arrayHash($category->value, [
                            'rating' => ['filled', 'integer', 'between:0,3'],
                            'body'   => [TranslationRule::make(['nullable', 'max:500'])],
                        ], ['nullable']),
                    ], []),
            ], ['nullable']),
        ];
    }
}
