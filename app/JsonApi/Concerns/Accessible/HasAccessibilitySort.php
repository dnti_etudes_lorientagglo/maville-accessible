<?php

namespace App\JsonApi\Concerns\Accessible;

use App\Models\Enums\AccessibilityCategory;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use LaravelJsonApi\Eloquent\Contracts\SortField;

/**
 * Class HasAccessibilitySort.
 */
class HasAccessibilitySort implements SortField
{
    /**
     * Create a new sort field.
     *
     * @param AccessibilityCategory $category
     *
     * @return HasAccessibilitySort
     */
    public static function make(AccessibilityCategory $category): self
    {
        return new static($category);
    }

    /**
     * HasAccessibilitySort constructor.
     *
     * @param AccessibilityCategory $category
     */
    public function __construct(private readonly AccessibilityCategory $category)
    {
    }

    /**
     * Get the name of the sort field.
     *
     * @return string
     */
    public function sortField(): string
    {
        return 'has-' . $this->category->value . '-accessibility';
    }

    /**
     * Apply the sort order to the query.
     *
     * @param Builder $query
     * @param string  $direction
     *
     * @return Builder
     */
    public function sort($query, string $direction = 'asc'): Builder
    {
        return $query->orderBy(
            DB::raw($query->qualifyColumn('accessibility') . "->'{$this->category->value}'->>'rating' IS NULL"),
            $direction,
        );
    }
}
