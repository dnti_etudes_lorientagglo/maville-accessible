<?php

namespace App\JsonApi\Concerns\Accessible;

use App\Models\Contracts\Reviewable;
use App\Models\Enums\AccessibilityCategory;
use App\Models\Review;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\Database\Query\Builder as BaseBuilder;
use Illuminate\Support\Facades\DB;
use LaravelJsonApi\Eloquent\Contracts\SortField;

/**
 * Class AccessibilitySort.
 */
class AccessibilitySort implements SortField
{
    /**
     * Create a new sort field.
     *
     * @param AccessibilityCategory $category
     *
     * @return AccessibilitySort
     */
    public static function make(AccessibilityCategory $category): self
    {
        return new static($category);
    }

    /**
     * AccessibilitySort constructor.
     *
     * @param AccessibilityCategory $category
     */
    public function __construct(private readonly AccessibilityCategory $category)
    {
    }

    /**
     * Get the name of the sort field.
     *
     * @return string
     */
    public function sortField(): string
    {
        return $this->category->value . '-accessibility';
    }

    /**
     * Apply the sort order to the query.
     *
     * @param Builder $query
     * @param string  $direction
     *
     * @return Builder
     */
    public function sort($query, string $direction = 'asc'): Builder
    {
        $model = $query->getModel();

        $defaultValue = '-0.1';
        $valuePath = fn(string $key) => "->'{$this->category->value}'->>'$key'";
        $valueDefault = fn(string $value) => "COALESCE($value, '$defaultValue')::float";
        $baseValue = $valueDefault($query->qualifyColumn('accessibility') . $valuePath('rating'));

        if (! ($model instanceof Reviewable)) {
            return $query->orderBy(DB::raw($baseValue), $direction);
        }

        $reviewsValue = $valueDefault('AVG(CAST(accessible_reviews.accessibility' . $valuePath('rating') . ' AS INT))');
        $reviewsTotal = 'count(*)';

        return $query->orderBy(
            fn(BaseBuilder $subQuery) => $subQuery
                ->selectRaw("COALESCE(accessible_summary.accessible_sort_value, $defaultValue)")
                ->from(
                    fn(BaseBuilder $subQuery) => $subQuery
                        ->selectRaw(
                            "(($baseValue + ($reviewsValue * $reviewsTotal)) / (1 + $reviewsTotal))"
                            . " as accessible_sort_value"
                        )
                        ->from((new Review())->getTable(), 'accessible_reviews')
                        ->where('accessible_reviews.reviewable_type', $model->getMorphClass())
                        ->whereColumn('accessible_reviews.reviewable_id', $query->qualifyColumn($model->getKeyName())),
                    'accessible_summary',
                ),
            $direction,
        );
    }
}
