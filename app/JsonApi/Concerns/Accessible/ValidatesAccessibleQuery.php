<?php

namespace App\JsonApi\Concerns\Accessible;

use App\Models\Enums\AccessibilityCategory;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Trait ValidatesAccessibleQuery.
 */
trait ValidatesAccessibleQuery
{
    /**
     * Get the specific rules.
     *
     * @return array
     */
    protected function accessibleRules(): array
    {
        return [
            'filter.accessible'   => ['filled', 'array'],
            'filter.accessible.*' => ['required', Rule::in(AccessibilityCategory::cases())],
            ...collect(AccessibilityCategory::cases())
                ->mapWithKeys(static fn(AccessibilityCategory $c) => [
                    'filter.has' . Str::studly($c->value) . 'AccessibilityBody' => [
                        'bail',
                        'filled',
                        JsonApiRule::boolean()->asString(),
                    ],
                ])
                ->all(),
        ];
    }
}
