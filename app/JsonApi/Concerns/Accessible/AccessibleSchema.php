<?php

namespace App\JsonApi\Concerns\Accessible;

use App\JsonApi\Filters\ClosureFilter;
use App\Models\Enums\AccessibilityCategory;
use App\Models\Enums\AccessibilityCriteria;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use LaravelJsonApi\Eloquent\Fields\ArrayHash;

/**
 * Trait AccessibleSchema.
 */
trait AccessibleSchema
{
    /**
     * Get the specific fields.
     *
     * @return array
     */
    protected function accessibleFields(): array
    {
        return [
            ArrayHash::make('accessibility'),
        ];
    }

    /**
     * Get the specific filters.
     *
     * @return array
     */
    protected function accessibleFilters(): array
    {
        $coalesce = fn(string $column, string $default) => DB::raw("coalesce($column, '$default')");
        $criteriaArray = fn(AccessibilityCategory $c) => AccessibilityCriteria::all()
            ->filter(fn(AccessibilityCriteria $i) => in_array($c, $i->accessibilityCategories))
            ->map(fn(AccessibilityCriteria $i) => "'$i->id'")
            ->join(', ');

        return [
            ClosureFilter::make(
                'accessible',
                static fn(Builder $query, array $categories) => collect($categories)->each(
                    static fn(string $category) => $query->where(
                        $coalesce($query->qualifyColumn('accessibility') . "->'$category'->'rating'", 'null'),
                        '>=',
                        2,
                    ),
                ),
            ),
            ...collect(AccessibilityCategory::cases())
                ->map(static fn(AccessibilityCategory $c) => ClosureFilter::make(
                    'has' . Str::studly($c->value) . 'AccessibilityBody',
                    static fn(Builder $query, bool $hasBody) => $query->where(
                        static fn(Builder $query) => $query
                            ->where(
                                $coalesce($query->qualifyColumn('accessibility') . "->'$c->value'->'body'", 'null'),
                                $hasBody ? '<>' : '=',
                                'null',
                            )
                            ->{$hasBody ? 'orWhere' : 'whereNot'}(
                                $coalesce($query->qualifyColumn('accessibility') . "->'criteria'", '{}'),
                                '?|',
                                DB::raw('array[' . $criteriaArray($c) . ']'),
                            ),
                    ),
                )->deserializeAsBoolean())
                ->all(),
        ];
    }

    /**
     * Get the specific sorts.
     *
     * @return array
     */
    protected function accessibleSortables(): array
    {
        return [
            ...collect(AccessibilityCategory::cases())
                ->map(static fn(AccessibilityCategory $category) => AccessibilitySort::make($category))
                ->all(),
            ...collect(AccessibilityCategory::cases())
                ->map(static fn(AccessibilityCategory $category) => HasAccessibilitySort::make($category))
                ->all(),
        ];
    }
}
