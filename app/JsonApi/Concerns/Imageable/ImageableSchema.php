<?php

namespace App\JsonApi\Concerns\Imageable;

use LaravelJsonApi\Core\Resources\Relation;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsToMany;

/**
 * Trait ImageableSchema.
 */
trait ImageableSchema
{
    /**
     * Get the specific fields.
     *
     * @return array
     */
    protected function imageableFields(): array
    {
        return [
            BelongsToMany::make('images')
                ->type('media')
                ->serializeUsing(fn(Relation $r) => $r->withoutLinks()),
        ];
    }
}
