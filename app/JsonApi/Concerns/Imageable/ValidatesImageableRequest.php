<?php

namespace App\JsonApi\Concerns\Imageable;

use App\Rules\AllowsMediaRule;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Trait ValidatesImageableRequest.
 */
trait ValidatesImageableRequest
{
    /**
     * Get the specific rules.
     *
     * @return array
     */
    protected function imageableRules(): array
    {
        return [
            'images' => [
                'bail',
                JsonApiRule::toMany(),
                AllowsMediaRule::make()->withPrevious($this->model()?->images),
            ],
        ];
    }
}
