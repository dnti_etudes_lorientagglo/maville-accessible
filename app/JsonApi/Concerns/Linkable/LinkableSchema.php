<?php

namespace App\JsonApi\Concerns\Linkable;

use LaravelJsonApi\Eloquent\Fields\ArrayHash;

/**
 * Trait LinkableSchema.
 */
trait LinkableSchema
{
    /**
     * Get the specific fields.
     *
     * @return array
     */
    protected function linkableFields(): array
    {
        return [
            ArrayHash::make('links'),
        ];
    }
}
