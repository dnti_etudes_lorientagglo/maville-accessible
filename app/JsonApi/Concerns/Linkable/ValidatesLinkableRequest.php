<?php

namespace App\JsonApi\Concerns\Linkable;

use App\Helpers\RuleHelper;
use App\Rules\PermissiveUrlRule;

/**
 * Trait ValidatesLinkableRequest.
 */
trait ValidatesLinkableRequest
{
    /**
     * Get the specific rules.
     *
     * @return array
     */
    protected function linkableRules(): array
    {
        return [
            ...RuleHelper::arrayHash('links', [
                'website'   => ['nullable', 'string', PermissiveUrlRule::make()],
                'facebook'  => ['nullable', 'string', PermissiveUrlRule::make()],
                'instagram' => ['nullable', 'string', PermissiveUrlRule::make()],
                'linkedin'  => ['nullable', 'string', PermissiveUrlRule::make()],
                'youtube'   => ['nullable', 'string', PermissiveUrlRule::make()],
            ], ['nullable']),
        ];
    }
}
