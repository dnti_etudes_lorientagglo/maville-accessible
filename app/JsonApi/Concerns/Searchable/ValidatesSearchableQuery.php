<?php

namespace App\JsonApi\Concerns\Searchable;

use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Trait ValidatesSearchableQuery.
 *
 * @mixin ResourceQuery
 */
trait ValidatesSearchableQuery
{
    /**
     * Get the specific rules.
     *
     * @return array
     */
    protected function searchableRules(): array
    {
        return [
            'filter.search' => [
                Rule::requiredIf(fn() => $this->hasSearchAwareSortInQuery()),
                'filled',
                'string',
            ],
        ];
    }

    /**
     * Check if the request query contains a search aware sort.
     *
     * @return bool
     */
    private function hasSearchAwareSortInQuery(): bool
    {
        $data = $this->validationData();
        $sort = Arr::get($data, 'sort');
        if (! is_string($sort)) {
            return false;
        }

        return in_array('search', explode(',', str_replace('-', '', $sort)));
    }
}
