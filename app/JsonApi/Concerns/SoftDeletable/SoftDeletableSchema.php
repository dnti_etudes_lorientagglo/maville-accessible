<?php

namespace App\JsonApi\Concerns\SoftDeletable;

use Closure;
use LaravelJsonApi\Eloquent\Fields\SoftDelete;
use LaravelJsonApi\Eloquent\Filters\OnlyTrashed;
use LaravelJsonApi\Eloquent\Filters\WithTrashed;
use LaravelJsonApi\Eloquent\SoftDeletes;

/**
 * Trait SoftDeletableSchema.
 */
trait SoftDeletableSchema
{
    use SoftDeletes;

    /**
     * Get the specific fields.
     *
     * @return array[]
     */
    private function softDeletableFields(bool|Closure $readOnly = false): array
    {
        return [
            SoftDelete::make('deletedAt')
                ->readOnly($readOnly)
                ->retainTimezone()
                ->sortable(),
        ];
    }

    /**
     * Get the specific filters.
     *
     * @return array[]
     */
    private function softDeletableFilters(): array
    {
        return [
            WithTrashed::make('withTrashed'),
            OnlyTrashed::make('onlyTrashed'),
        ];
    }
}
