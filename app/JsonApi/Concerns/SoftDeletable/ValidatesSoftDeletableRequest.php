<?php

namespace App\JsonApi\Concerns\SoftDeletable;

use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Trait ValidatesSoftDeletableRequest.
 *
 * @mixin ResourceRequest
 */
trait ValidatesSoftDeletableRequest
{
    /**
     * Get the specific rules.
     *
     * @return array
     */
    private function softDeletableRules(): array
    {
        return [
            'deletedAt' => ['nullable', JsonApiRule::dateTime()],
        ];
    }
}
