<?php

namespace App\JsonApi\Concerns\SoftDeletable;

use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Trait ValidatesSoftDeletableQuery.
 */
trait ValidatesSoftDeletableQuery
{
    /**
     * Get the specific rules.
     *
     * @return array
     */
    protected function softDeletableRules(): array
    {
        return [
            'filter.withTrashed' => ['bail', 'nullable', JsonApiRule::boolean()->asString()],
            'filter.onlyTrashed' => ['bail', 'nullable', JsonApiRule::boolean()->asString()],
        ];
    }
}
