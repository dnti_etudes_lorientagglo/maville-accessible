<?php

namespace App\JsonApi\Concerns\Core;

use App\JsonApi\Filters\SearchFilter;
use App\JsonApi\Sorting\SearchSort;
use LaravelJsonApi\Eloquent\Contracts\Paginator;
use LaravelJsonApi\Eloquent\Fields\ArrayList;
use LaravelJsonApi\Eloquent\Fields\DateTime;
use LaravelJsonApi\Eloquent\Fields\ID;
use LaravelJsonApi\Eloquent\Fields\Number;
use LaravelJsonApi\Eloquent\Filters\WhereIdIn;
use LaravelJsonApi\Eloquent\Filters\WhereIdNotIn;
use LaravelJsonApi\Eloquent\Pagination\PagePagination;
use LaravelJsonApi\Eloquent\Schema;

/**
 * Class BaseSchema.
 *
 * Base schema with common fields, filters and pagination.
 */
abstract class BaseSchema extends Schema
{
    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ID::make()->uuid(),
            DateTime::make('createdAt')
                ->readOnly()
                ->retainTimezone()
                ->sortable(),
            DateTime::make('updatedAt')
                ->readOnly()
                ->retainTimezone()
                ->sortable(),
            Number::make('searchScore')
                ->readOnly(),
            ArrayList::make('allows', 'allowsPermissions')
                ->readOnly(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function filters(): array
    {
        return [
            SearchFilter::make(),
            WhereIdIn::make($this),
            WhereIdNotIn::make($this, 'notId'),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function sortables(): iterable
    {
        return [
            SearchSort::make(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function pagination(): ?Paginator
    {
        return PagePagination::make();
    }
}
