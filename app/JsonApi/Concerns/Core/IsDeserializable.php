<?php

namespace App\JsonApi\Concerns\Core;

use Closure;

/**
 * Trait IsDeserializable.
 */
trait IsDeserializable
{
    /**
     * @var Closure|null The closure to deserialize value.
     */
    protected Closure | null $deserializeUsing = null;

    /**
     * Define a deserializer for value.
     *
     * @param Closure|null $deserializeUsing
     *
     * @return $this
     */
    public function deserializeUsing(Closure | null $deserializeUsing): static
    {
        $this->deserializeUsing = $deserializeUsing;

        return $this;
    }

    /**
     * Deserialize the value as a boolean.
     *
     * @return $this
     */
    public function deserializeAsBoolean(): static
    {
        return $this->deserializeUsing(
            static fn(mixed $value) => filter_var($value, FILTER_VALIDATE_BOOL) === true,
        );
    }

    /**
     * Deserialize the value as an integer.
     *
     * @return $this
     */
    public function deserializeAsInteger(): static
    {
        return $this->deserializeUsing(
            static fn(mixed $value) => intval($value),
        );
    }

    /**
     * Deserialize the value.
     *
     * @param mixed $value
     *
     * @return mixed
     */
    protected function deserializeValue(mixed $value): mixed
    {
        if ($this->deserializeUsing) {
            return call_user_func_array($this->deserializeUsing, [$value]);
        }

        return $value;
    }
}
