<?php

namespace App\JsonApi\Concerns\Core;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use LaravelJsonApi\Contracts\Schema\Filter;
use LaravelJsonApi\Eloquent\Contracts\Filter as EloquentFilter;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Trait ValidatesPaginableQuery.
 */
trait ValidatesPaginableQuery
{
    /**
     * Create the rules for the pagination of resource.
     *
     * @param bool $required
     *
     * @return array
     */
    protected function pageRules(bool $required = true): array
    {
        return [
            'page'        => ['bail', $this->getPageFillRule($required), 'array:number,size', JsonApiRule::page()],
            'page.number' => ['filled', 'integer', 'min:1'],
            'page.size'   => ['filled', 'integer', 'min:1', 'max:100'],
        ];
    }

    /**
     * Create the rules for the pagination of resource.
     *
     * @param bool $required
     *
     * @return array
     */
    protected function cursorPageRules(bool $required = true): array
    {
        return [
            'page'        => ['bail', $this->getPageFillRule($required), 'array', JsonApiRule::page()],
            'page.limit'  => ['filled', 'integer', 'min:1', 'max:100'],
            'page.after'  => ['filled', 'uuid'],
            'page.before' => ['filled', 'uuid'],
        ];
    }

    /**
     * Get the page fill rule depending on required/singular state.
     *
     * @param bool $required
     *
     * @return string
     */
    private function getPageFillRule(bool $required): string
    {
        return ($required && ! $this->isIdsFilteredQuery() && ! $this->isSingularQuery())
            ? 'required'
            : 'nullable';
    }

    /**
     * Check if the current query targets a specific set of resources.
     *
     * @return bool
     */
    private function isIdsFilteredQuery(): bool
    {
        return Arr::has($this->validationData(), 'filter.id');
    }

    /**
     * Check if the current query targets a singular response.
     *
     * @return bool
     */
    private function isSingularQuery(): bool
    {
        $singularKeys = $this->getSingularKeys();

        return $singularKeys->isNotEmpty()
            && Arr::hasAny($this->validationData(), $singularKeys->all());
    }

    /**
     * Get the key of request query which are targeting a singular response.
     *
     * @return Collection<int, string>
     */
    private function getSingularKeys(): Collection
    {
        return collect($this->schema()->filters())
            ->filter(static fn(Filter $filter) => $filter instanceof EloquentFilter && $filter->isSingular())
            ->map(static fn(Filter $filter) => "filter.{$filter->key()}")
            ->values();
    }
}
