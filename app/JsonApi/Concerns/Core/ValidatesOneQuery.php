<?php

namespace App\JsonApi\Concerns\Core;

use Closure;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Trait ValidatesOneQuery.
 */
trait ValidatesOneQuery
{
    /**
     * Create the default rules for one resource query.
     *
     * @param Closure|null $include
     * @param Closure|null $filter
     *
     * @return array
     */
    protected function defaultsRules(
        Closure|null $include = null,
        Closure|null $filter = null,
    ): array {
        $includeRule = JsonApiRule::includePaths();
        $filterRule = JsonApiRule::filter()->forget('id', 'notId', 'search');

        return [
            'fields'    => [
                'bail',
                'nullable',
                'array',
                JsonApiRule::fieldSets(),
            ],
            'include'   => [
                'bail',
                'nullable',
                'string',
                $include ? $include($includeRule) : $includeRule,
            ],
            'filter'    => [
                'bail',
                'nullable',
                'array',
                $filter ? $filter($filterRule) : $filterRule,
            ],
            'page'      => JsonApiRule::notSupported(),
            'sort'      => JsonApiRule::notSupported(),
            'withCount' => JsonApiRule::notSupported(),
        ];
    }
}
