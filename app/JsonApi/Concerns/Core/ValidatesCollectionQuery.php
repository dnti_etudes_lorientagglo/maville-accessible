<?php

namespace App\JsonApi\Concerns\Core;

use Closure;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Trait ValidatesCollectionQuery.
 */
trait ValidatesCollectionQuery
{
    /**
     * Create the default rules for collection resource query.
     *
     * @param Closure|null $include
     * @param Closure|null $filter
     *
     * @return array
     */
    protected function defaultsRules(
        Closure|null $include = null,
        Closure|null $filter = null,
    ): array {
        $includeRule = JsonApiRule::includePaths();
        $filterRule = JsonApiRule::filter();

        return [
            'fields'         => [
                'bail',
                'nullable',
                'array',
                JsonApiRule::fieldSets(),
            ],
            'include'        => [
                'bail',
                'nullable',
                'string',
                $include ? $include($includeRule) : $includeRule,
            ],
            'filter'         => [
                'bail',
                'nullable',
                'array',
                $filter ? $filter($filterRule) : $filterRule,
            ],
            'sort'           => [
                'bail',
                'nullable',
                'string',
                JsonApiRule::sort(),
            ],
            'withCount'      => JsonApiRule::notSupported(),
            // Specific default filters rules.
            'filter.id'      => [
                'filled',
                'array',
            ],
            'filter.id.*'    => [
                'required',
                'uuid',
            ],
            'filter.notId'   => [
                'filled',
                'array',
            ],
            'filter.notId.*' => [
                'required',
                'uuid',
            ],
        ];
    }
}
