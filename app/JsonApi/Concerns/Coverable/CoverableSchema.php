<?php

namespace App\JsonApi\Concerns\Coverable;

use LaravelJsonApi\Core\Resources\Relation;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsTo;

/**
 * Trait CoverableSchema.
 */
trait CoverableSchema
{
    /**
     * Get the specific fields.
     *
     * @return array
     */
    protected function coverableFields(): array
    {
        return [
            BelongsTo::make('cover')
                ->type('media')
                ->serializeUsing(fn(Relation $r) => $r->withoutLinks()),
        ];
    }
}
