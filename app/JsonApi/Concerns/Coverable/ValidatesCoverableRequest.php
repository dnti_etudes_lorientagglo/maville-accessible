<?php

namespace App\JsonApi\Concerns\Coverable;

use App\Rules\AllowsMediaRule;
use Illuminate\Validation\Rule;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Trait ValidatesCoverableRequest.
 */
trait ValidatesCoverableRequest
{
    /**
     * Get the specific rules.
     *
     * @param bool $required
     *
     * @return array
     */
    protected function coverableRules(bool $required = true): array
    {
        return [
            'cover' => [
                'bail',
                Rule::when($required, 'required'),
                JsonApiRule::toOne(),
                AllowsMediaRule::make()->withPrevious($this->model()?->cover),
            ],
        ];
    }
}
