<?php

namespace App\JsonApi\Concerns\Categorizable;

use App\Helpers\QueryHelper;
use App\JsonApi\Filters\ClosureFilter;
use Closure;
use Illuminate\Contracts\Database\Eloquent\Builder;
use LaravelJsonApi\Core\Resources\Relation;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsToMany;

/**
 * Trait CategorizableSchema.
 */
trait CategorizableSchema
{
    /**
     * Get the specific fields.
     *
     * @return array
     */
    protected function categorizableFields(): array
    {
        return [
            BelongsToMany::make('categories')
                ->serializeUsing(fn(Relation $r) => $r->withoutLinks())
                ->mustValidate(),
        ];
    }

    /**
     * Get the specific filters.
     *
     * @return array
     */
    protected function categorizableFilters(): array
    {
        return [
            ClosureFilter::make(
                'category',
                static fn(Builder $query, array $categories) => QueryHelper::whereRelationIn(
                    $query,
                    'categories',
                    $categories,
                    filterCallback: static fn(Builder $query, Closure $filterQuery) => $query
                        ->orWhereHas('parents', $filterQuery),
                ),
            ),
        ];
    }
}
