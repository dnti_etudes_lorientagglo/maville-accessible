<?php

namespace App\JsonApi\Concerns\Categorizable;

/**
 * Trait ValidatesCategorizableQuery.
 */
trait ValidatesCategorizableQuery
{
    /**
     * Get the specific rules.
     *
     * @return array
     */
    protected function categorizableRules(): array
    {
        return [
            'filter.category'   => ['filled', 'array'],
            'filter.category.*' => ['required', 'string'],
        ];
    }
}
