<?php

namespace App\JsonApi\Concerns\Categorizable;

use Illuminate\Validation\Rule;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Trait ValidatesCategorizableRequest.
 */
trait ValidatesCategorizableRequest
{
    /**
     * Get the specific rules.
     *
     * @param bool $required
     *
     * @return array
     */
    protected function categorizableRules(bool $required = true): array
    {
        return [
            'categories' => [Rule::when($required, 'required'), JsonApiRule::toMany()],
        ];
    }
}
