<?php

namespace App\JsonApi\Concerns\Sluggable;

/**
 * Trait ValidatesSluggableQuery.
 */
trait ValidatesSluggableQuery
{
    /**
     * Get the specific rules.
     *
     * @return array[]
     */
    public function sluggableRules(): array
    {
        return [
            'filter.slug' => ['filled', 'string'],
        ];
    }
}
