<?php

namespace App\JsonApi\Concerns\Sluggable;

use App\JsonApi\Filters\ClosureFilter;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Support\Str as StrHelper;
use LaravelJsonApi\Eloquent\Fields\Str;

/**
 * Trait SluggableSchema.
 */
trait SluggableSchema
{
    /**
     * Get the specific fields.
     *
     * @return array[]
     */
    private function sluggableFields(): array
    {
        return [
            Str::make('slug')
                ->readOnly(),
        ];
    }

    /**
     * Get the specific filters.
     *
     * @return array[]
     */
    private function sluggableFilters(): array
    {
        return [
            ClosureFilter::make(
                'slug',
                static fn(Builder $query, string $slug) => $query->where(
                    static fn(Builder $query) => $query->where('slug', $slug)->when(
                        StrHelper::isUuid($slug),
                        static fn(Builder $query) => $query->orWhere('id', $slug),
                    ),
                )
            )->singular(),
        ];
    }
}
