<?php

namespace App\JsonApi\Concerns\Contactable;

use LaravelJsonApi\Eloquent\Fields\Str;

/**
 * Trait ContactableSchema.
 */
trait ContactableSchema
{
    /**
     * Get the specific fields.
     *
     * @return array
     */
    protected function contactableFields(): array
    {
        return [
            Str::make('email'),
            Str::make('phone'),
        ];
    }
}
