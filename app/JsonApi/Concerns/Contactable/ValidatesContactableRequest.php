<?php

namespace App\JsonApi\Concerns\Contactable;

use Propaganistas\LaravelPhone\Rules\Phone;

/**
 * Trait ValidatesContactableRequest.
 */
trait ValidatesContactableRequest
{
    /**
     * Get the specific rules.
     *
     * @return array
     */
    protected function contactableRules(): array
    {
        return [
            'email'          => ['nullable', 'string', 'email', 'max:255'],
            'phone'          => ['nullable', new Phone()],
        ];
    }
}
