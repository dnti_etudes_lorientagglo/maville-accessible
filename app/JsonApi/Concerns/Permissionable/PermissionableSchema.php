<?php

namespace App\JsonApi\Concerns\Permissionable;

use LaravelJsonApi\Eloquent\Fields\ArrayHash;

/**
 * Trait PermissionableSchema.
 */
trait PermissionableSchema
{
    /**
     * Get the specific fields.
     *
     * @return array[]
     */
    private function permissionableFields(): array
    {
        return [
            ArrayHash::make('availablePermissions')
                ->readOnly(),
        ];
    }
}
