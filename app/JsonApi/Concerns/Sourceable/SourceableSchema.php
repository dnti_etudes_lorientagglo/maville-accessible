<?php

namespace App\JsonApi\Concerns\Sourceable;

use LaravelJsonApi\Core\Resources\Relation;
use LaravelJsonApi\Eloquent\Fields\DateTime;
use LaravelJsonApi\Eloquent\Fields\Relations\HasMany;

/**
 * Trait SourceableSchema.
 */
trait SourceableSchema
{
    /**
     * Get the specific fields.
     *
     * @return array[]
     */
    private function sourceableFields(): array
    {
        return [
            DateTime::make('sourcedAt')
                ->readOnly()
                ->retainTimezone()
                ->sortable(),
            HasMany::make('sources')
                ->serializeUsing(fn(Relation $r) => $r->withoutLinks())
                ->readOnly(),
        ];
    }
}
