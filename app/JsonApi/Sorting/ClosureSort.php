<?php

namespace App\JsonApi\Sorting;

use Closure;
use Illuminate\Contracts\Database\Eloquent\Builder;
use LaravelJsonApi\Eloquent\Contracts\SortField;

/**
 * Class ClosureSort.
 */
class ClosureSort implements SortField
{
    /**
     * Create a new sort field.
     *
     * @param string  $name
     * @param Closure $using
     *
     * @return ClosureSort
     */
    public static function make(string $name, Closure $using): self
    {
        return new static($name, $using);
    }

    /**
     * ClosureSort constructor.
     *
     * @param string  $name
     * @param Closure $using
     */
    public function __construct(
        private readonly string $name,
        private readonly Closure $using,
    ) {
    }

    /**
     * Get the name of the sort field.
     *
     * @return string
     */
    public function sortField(): string
    {
        return $this->name;
    }

    /**
     * Apply the sort order to the query.
     *
     * @param Builder $query
     * @param string  $direction
     *
     * @return Builder
     */
    public function sort($query, string $direction = 'asc'): Builder
    {
        return call_user_func_array($this->using, [$query, $direction]);
    }
}
