<?php

namespace App\JsonApi\Fields;

use App\Helpers\LocaleHelper;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use LaravelJsonApi\Eloquent\Fields\ArrayHash;

/**
 * Class Translation.
 */
class Translation extends ArrayHash
{
    /**
     * Create a translation attribute.
     *
     * @param string      $fieldName
     * @param string|null $column
     *
     * @return Translation
     */
    public static function make(string $fieldName, string $column = null): self
    {
        return new self($fieldName, $column);
    }

    /**
     * {@inheritDoc}
     */
    public function sort($query, string $direction = 'asc'): Builder
    {
        $qualifiedColumns = LocaleHelper::orderedLocales()->take(2)
            ->map(function (string $locale) use ($query) {
                $column = $query->getModel()->qualifyColumn($this->column());

                return "$column->>'$locale'";
            })
            ->join(', ');

        return $query->orderBy(DB::raw(
            "COALESCE($qualifiedColumns, '')",
        ), $direction);
    }
}
