<?php

namespace App\JsonApi\V1\OrganizationInvitations;

use App\Helpers\AuthHelper;
use App\Models\Enums\RoleScopeName;
use App\Models\Organization;
use App\Models\Role;
use App\Policies\OrganizationPolicy;
use Illuminate\Validation\Rule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Class OrganizationInvitationRequest.
 */
class OrganizationInvitationRequest extends ResourceRequest
{
    public function rules(): array
    {
        $user = AuthHelper::user();

        $organizationRules = ['organization' => ['required', JsonApiRule::toOne()]];
        if (! $user->can(OrganizationPolicy::ADMIN, Organization::class)) {
            $organizationRules['organization.id'] = [
                'required',
                Rule::in([$user->getActingForOrganization()->organization_id]),
            ];
        }

        return [
            ...$organizationRules,
            'email'   => ['required', 'string', 'email', 'max:255'],
            'role'    => ['required', JsonApiRule::toOne()],
            'role.id' => [
                'required',
                Rule::in(
                    Role::query()
                        ->where('scope_name', RoleScopeName::ORGANIZATION->value)
                        ->pluck('id'),
                ),
            ],
        ];
    }
}
