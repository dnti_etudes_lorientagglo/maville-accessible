<?php

namespace App\JsonApi\V1\OrganizationInvitations;

use App\JsonApi\Concerns\Core\ValidatesOneQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class OrganizationInvitationQuery.
 */
class OrganizationInvitationQuery extends ResourceQuery
{
    use ValidatesOneQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
        ];
    }
}
