<?php

namespace App\JsonApi\V1\OrganizationInvitations;

use App\Helpers\JsonApiHelper;
use App\JsonApi\Concerns\Core\BaseSchema;
use App\Models\OrganizationInvitation;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsTo;
use LaravelJsonApi\Eloquent\Fields\Str;

/**
 * Class OrganizationInvitationSchema.
 */
class OrganizationInvitationSchema extends BaseSchema
{
    public static string $model = OrganizationInvitation::class;

    protected int $maxDepth = 1;

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            Str::make('email')
                ->readOnlyOnUpdate()
                ->sortable(),
            BelongsTo::make('organization')
                ->serializeUsing(JsonApiHelper::noLinks()),
            BelongsTo::make('role')
                ->serializeUsing(JsonApiHelper::noLinks()),
        ];
    }
}
