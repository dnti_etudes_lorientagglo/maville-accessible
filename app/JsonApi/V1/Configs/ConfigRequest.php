<?php

namespace App\JsonApi\V1\Configs;

use App\Helpers\RuleHelper;
use App\Rules\TranslationRule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Class ConfigRequest.
 */
class ConfigRequest extends ResourceRequest
{
    public function rules(): array
    {
        return [
            'description'             => [TranslationRule::make(['nullable', 'max:255'])],
            ...RuleHelper::address('mapCenter', ['required']),
            'mapZoom'                 => ['required', 'integer', 'between:9,14'],
            'mapSearchableCategories' => ['nullable', JsonApiRule::toMany()],
            'mapDefaultCategories'    => ['nullable', JsonApiRule::toMany()],
        ];
    }
}
