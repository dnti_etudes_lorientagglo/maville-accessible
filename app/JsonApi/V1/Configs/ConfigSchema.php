<?php

namespace App\JsonApi\V1\Configs;

use App\JsonApi\Concerns\Core\BaseSchema;
use App\JsonApi\Fields\Translation;
use App\Models\Config;
use App\Services\App\AppConfig;
use App\Services\App\AppEnv;
use Illuminate\Database\Eloquent\Casts\ArrayObject;
use Illuminate\Support\Arr;
use LaravelJsonApi\Contracts\Server\Server;
use LaravelJsonApi\Core\Resources\Relation;
use LaravelJsonApi\Eloquent\Fields\ArrayHash;
use LaravelJsonApi\Eloquent\Fields\Number;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsToMany;

/**
 * Class ConfigSchema.
 */
class ConfigSchema extends BaseSchema
{
    public static string $model = Config::class;

    protected int $maxDepth = 1;

    /**
     * {@inheritDoc}
     */
    public function __construct(
        Server $server,
        private readonly AppEnv $appEnv,
        private readonly AppConfig $appConfig,
    ) {
        parent::__construct($server);
    }

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            Translation::make('description'),
            ArrayHash::make('services')
                ->readOnly()
                ->serializeUsing(fn(ArrayObject | null $services) => [
                    ...$this->serializeService($services, 'facebookLogin'),
                    ...$this->serializeService($services, 'googleLogin'),
                    ...$this->serializeService($services, 'franceConnectLogin'),
                    ...$this->serializeService($services, 'hitineraire'),
                    ...$this->serializeService($services, 'acceslibre', [
                        'draftNewCategories',
                        'sendData',
                    ]),
                    ...$this->serializeService($services, 'infolocale', [
                        'draftNewCategories',
                        'organizationsId',
                        'publicationsId',
                        'announcementsId',
                    ]),
                    ...$this->serializeService($services, 'datagouv', [
                        'placesDatasetId',
                        'activitiesDatasetId',
                    ]),
                    ...$this->serializeService($services, 'matomo'),
                ]),
            ArrayHash::make('mapCenter')
                ->serializeUsing(fn(ArrayObject | null $value) => $value ?? $this->appEnv->defaultMapCenter()),
            Number::make('mapZoom')
                ->serializeUsing(fn(int | null $value) => $value ?? $this->appEnv->defaultMapZoom()),
            BelongsToMany::make('mapSearchableCategories')
                ->type('categories')
                ->serializeUsing(fn(Relation $r) => $r->withoutLinks()),
            BelongsToMany::make('mapDefaultCategories')
                ->type('categories')
                ->serializeUsing(fn(Relation $r) => $r->withoutLinks()),
        ];
    }

    /**
     * Serialize a configured service.
     *
     * @param ArrayObject|null $services
     * @param string           $service
     * @param array            $keys
     *
     * @return array|null
     */
    private function serializeService(ArrayObject | null $services, string $service, array $keys = []): array | null
    {
        if (! $this->appConfig->isServiceConfigurable($service)) {
            return [$service => null];
        }

        $data = Arr::get($services, $service) ?? [];
        $activated = ! empty($data['activatedAt'] ?? null);

        return [
            $service => $activated ? [
                'activatedAt' => $data['activatedAt'] ?? null,
                'expireAt'    => $data['expireAt'] ?? null,
                ...Arr::mapWithKeys($keys, static fn(string $key) => [$key => Arr::get($data, $key)]),
            ] : null,
        ];
    }
}
