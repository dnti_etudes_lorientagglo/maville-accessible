<?php

namespace App\JsonApi\V1\Roles;

use App\Helpers\JsonApiHelper;
use App\JsonApi\Concerns\Core\BaseSchema;
use App\JsonApi\Fields\Translation;
use App\Models\Role;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsToMany;
use LaravelJsonApi\Eloquent\Fields\Str;

/**
 * Class RoleSchema.
 */
class RoleSchema extends BaseSchema
{
    public static string $model = Role::class;

    protected int $maxDepth = 1;

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            Str::make('code', 'name')
                ->readOnly(),
            Str::make('scopeName')
                ->readOnly(),
            Translation::make('name', 'translated_name')
                ->readOnly()
                ->sortable(),
            BelongsToMany::make('permissions')
                ->readOnly()
                ->serializeUsing(JsonApiHelper::noLinks()),
        ];
    }
}
