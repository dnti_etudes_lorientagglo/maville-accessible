<?php

namespace App\JsonApi\V1\Requests;

use App\JsonApi\Concerns\Core\ValidatesCollectionQuery;
use App\JsonApi\Concerns\Core\ValidatesPaginableQuery;
use App\JsonApi\Concerns\Searchable\ValidatesSearchableQuery;
use App\JsonApi\Concerns\SoftDeletable\ValidatesSoftDeletableQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class RequestCollectionQuery.
 */
class RequestCollectionQuery extends ResourceQuery
{
    use ValidatesCollectionQuery;
    use ValidatesPaginableQuery;
    use ValidatesSearchableQuery;
    use ValidatesSoftDeletableQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
            ...$this->pageRules(),
            ...$this->searchableRules(),
            ...$this->softDeletableRules(),
        ];
    }
}
