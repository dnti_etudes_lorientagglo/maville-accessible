<?php

namespace App\JsonApi\V1\Requests;

use App\Helpers\AuthHelper;
use App\Helpers\JsonApiHelper;
use App\JsonApi\Concerns\Core\BaseSchema;
use App\JsonApi\Concerns\SoftDeletable\SoftDeletableSchema;
use App\JsonApi\Concerns\UserOwnable\UserOwnableSchema;
use App\Models\Enums\RequestTypeCode;
use App\Models\Enums\RoleName;
use App\Models\Organization;
use App\Models\Request;
use App\Policies\RequestPolicy;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Facades\Gate;
use LaravelJsonApi\Eloquent\Fields\ArrayHash;
use LaravelJsonApi\Eloquent\Fields\DateTime;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsTo;
use LaravelJsonApi\Eloquent\Fields\Relations\MorphTo;
use LaravelJsonApi\Eloquent\Fields\Str;

/**
 * Class RequestSchema.
 */
class RequestSchema extends BaseSchema
{
    use SoftDeletableSchema;
    use UserOwnableSchema;

    public static string $model = Request::class;

    protected int $maxDepth = 2;

    /**
     * {@inheritDoc}
     */
    public function indexQuery(?HttpRequest $request, EloquentBuilder $query): EloquentBuilder
    {
        $query = parent::indexQuery($request, $query)->whereNotNull('owner_user_id');

        if (AuthHelper::hasPermissionTo(RequestPolicy::adminPermission())) {
            return $query;
        }

        return $query->where(static function (EloquentBuilder $query) {
            $user = AuthHelper::user();
            $actingAsOrganization = $user?->getActingForOrganization();
            if (
                ! $user
                || ! $actingAsOrganization
                || $actingAsOrganization->role->name !== RoleName::ORGANIZATION_ADMIN->value
            ) {
                $query->whereNull('id');

                return;
            }

            $organization = new Organization();
            $organization->id = $actingAsOrganization->organization_id;

            $query->whereRelation('requestType', 'code', RequestTypeCode::JOIN->value)
                ->whereMorphedTo('requestable', $organization);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            ...$this->softDeletableFields(static fn() => Gate::denies(RequestPolicy::updatePermission())),
            ...$this->userOwnableFields(),
            Str::make('url')
                ->readOnlyOnUpdate(),
            Str::make('body')
                ->readOnlyOnUpdate(),
            ArrayHash::make('data')
                ->readOnlyOnUpdate(),
            DateTime::make('acceptedAt')
                ->readOnly()
                ->retainTimezone()
                ->sortable(),
            BelongsTo::make('requestType')
                ->readOnlyOnUpdate()
                ->serializeUsing(JsonApiHelper::noLinks()),
            MorphTo::make('requestable')
                ->types('organizations', 'places', 'activities')
                ->readOnlyOnUpdate()
                ->serializeUsing(JsonApiHelper::noLinks()),
            BelongsTo::make('resultable')
                ->type('organizations')
                ->readOnlyOnUpdate()
                ->serializeUsing(JsonApiHelper::noLinks()),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function filters(): array
    {
        return [
            ...parent::filters(),
            ...$this->softDeletableFilters(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function sortables(): iterable
    {
        return [
            ...parent::sortables(),
            ...$this->userOwnableSortables(),
        ];
    }
}
