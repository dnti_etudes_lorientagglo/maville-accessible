<?php

namespace App\JsonApi\V1\Requests;

use App\Helpers\AuthHelper;
use App\Helpers\RuleHelper;
use App\JsonApi\Concerns\SoftDeletable\ValidatesSoftDeletableRequest;
use App\Models\Enums\OrganizationTypeCode;
use App\Models\Enums\RequestTypeCode;
use App\Models\Request;
use App\Models\RequestType;
use App\Services\Validation\Validator;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;
use Propaganistas\LaravelPhone\Rules\Phone;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class RequestRequest.
 */
class RequestRequest extends ResourceRequest
{
    use ValidatesSoftDeletableRequest;

    public function rules(): array
    {
        return [
            ...$this->softDeletableRules(),
            ...$this->getSpecificRules(),
            'url'         => ['required', 'string', 'url'],
            'requestType' => ['required', JsonApiRule::toOne()],
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param Validator $validator
     *
     * @return void
     */
    public function withValidator(Validator $validator): void
    {
        if ($this->isCreating()) {
            $validator->after(function () {
                $message = $this->getExceedingRequestsError();
                if ($message) {
                    throw new HttpException(Response::HTTP_CONFLICT, $message);
                }
            });
        }
    }

    /**
     * Get the specific rules to use depending on the request type.
     *
     * @return array
     */
    private function getSpecificRules(): array
    {
        $data = $this->validationData();
        $requestType = $this->findRequestType($data);

        return match ($requestType?->code) {
            RequestTypeCode::CLAIM->value => $this->getClaimRules(),
            RequestTypeCode::JOIN->value => $this->getJoinRules(),
            default => [],
        };
    }

    /**
     * Get the specific rules for a "become organization admin" request.
     *
     * @return array
     */
    private function getClaimRules(): array
    {
        return [
            'body'             => ['nullable', 'string', 'max:500'],
            ...RuleHelper::arrayHash('data', [
                'phone' => ['required', new Phone()],
            ], ['required']),
            'requestable'      => ['required', JsonApiRule::toOne()],
            'requestable.type' => ['required', Rule::in(['places', 'activities'])],
            'resultable'       => ['nullable', JsonApiRule::toOne()],
            'resultable.type'  => ['nullable', Rule::in(['organizations'])],
            'resultable.id'    => [
                'nullable',
                Rule::in(AuthHelper::user()->memberOfOrganizations()->pluck('organization_id')),
            ],
        ];
    }

    /**
     * Get the specific rules for a "join or create an organization" request.
     *
     * @return array
     */
    private function getJoinRules(): array
    {
        $data = $this->validationData();
        $existingOrganization = ($data['requestable'] ?? null) !== null;

        return [
            'body'             => ['nullable', 'string', 'max:500'],
            ...RuleHelper::arrayHash('data', [
                'phone' => ['required', new Phone()],
                ...($existingOrganization ? [] : [
                    'name' => ['required', 'string', 'max:100'],
                    'type' => ['required', Rule::in(OrganizationTypeCode::cases())],
                ]),
            ], ['required']),
            'requestable'      => ['nullable', JsonApiRule::toOne()],
            'requestable.type' => ['nullable', Rule::in(['organizations'])],
        ];
    }

    /**
     * Find the matching request type.
     *
     * @param array|null $data
     *
     * @return RequestType|null
     */
    private function findRequestType(array | null $data): RequestType | null
    {
        $id = $data['requestType']['id'] ?? null;
        if (! Str::isUuid($id)) {
            return null;
        }

        return RequestType::query()->find($id);
    }

    /**
     * Get a potential requests limits exceed error message.
     *
     * @return string|null
     */
    private function getExceedingRequestsError(): string | null
    {
        $data = $this->validationData();
        $user = AuthHelper::user();
        $query = Request::query()
            ->where('request_type_id', $data['requestType']['id'])
            ->whereNotNull('owner_user_id')
            ->where('owner_user_id', $user->id);

        if (! isset($data['requestable'])) {
            return $query->count() > 25
                ? __('validation.custom.requests.exceeds')
                : null;
        }

        return $query
            ->where('requestable_type', $data['requestable']['type'])
            ->where('requestable_id', $data['requestable']['id'])
            ->exists()
            ? __('validation.custom.requests.exists')
            : null;
    }
}
