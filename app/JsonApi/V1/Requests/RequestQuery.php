<?php

namespace App\JsonApi\V1\Requests;

use App\JsonApi\Concerns\Core\ValidatesOneQuery;
use App\JsonApi\Concerns\SoftDeletable\ValidatesSoftDeletableQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class RequestQuery.
 */
class RequestQuery extends ResourceQuery
{
    use ValidatesOneQuery;
    use ValidatesSoftDeletableQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
            ...$this->softDeletableRules(),
        ];
    }
}
