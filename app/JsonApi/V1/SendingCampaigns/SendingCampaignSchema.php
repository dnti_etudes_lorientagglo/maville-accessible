<?php

namespace App\JsonApi\V1\SendingCampaigns;

use App\Helpers\JsonApiHelper;
use App\JsonApi\Concerns\Core\BaseSchema;
use App\Models\SendingCampaign;
use LaravelJsonApi\Eloquent\Fields\DateTime;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsTo;
use LaravelJsonApi\Eloquent\Fields\Relations\HasMany;
use LaravelJsonApi\Eloquent\Fields\Str;

/**
 * Class SendingCampaignSchema.
 */
class SendingCampaignSchema extends BaseSchema
{
    public static string $model = SendingCampaign::class;

    protected int $maxDepth = 1;

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            Str::make('name')
                ->readOnly(),
            BelongsTo::make('campaignType')
                ->type('sending-campaign-types')
                ->readOnly()
                ->serializeUsing(JsonApiHelper::noLinks()),
            HasMany::make('campaignBatches')
                ->type('sending-campaign-batches')
                ->readOnly()
                ->serializeUsing(JsonApiHelper::noLinks()),
            DateTime::make('dispatchedAt')
                ->readOnly()
                ->retainTimezone()
                ->sortable(),
            DateTime::make('startedAt')
                ->readOnly()
                ->retainTimezone()
                ->sortable(),
            DateTime::make('finishedAt')
                ->readOnly()
                ->retainTimezone()
                ->sortable(),
        ];
    }
}
