<?php

namespace App\JsonApi\V1\SendingCampaigns;

use App\JsonApi\Concerns\Core\ValidatesCollectionQuery;
use App\JsonApi\Concerns\Core\ValidatesPaginableQuery;
use App\JsonApi\Concerns\Searchable\ValidatesSearchableQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class SendingCampaignCollectionQuery.
 */
class SendingCampaignCollectionQuery extends ResourceQuery
{
    use ValidatesCollectionQuery;
    use ValidatesPaginableQuery;
    use ValidatesSearchableQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
            ...$this->pageRules(),
            ...$this->searchableRules(),
        ];
    }
}
