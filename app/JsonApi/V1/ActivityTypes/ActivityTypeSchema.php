<?php

namespace App\JsonApi\V1\ActivityTypes;

use App\JsonApi\Concerns\Core\BaseSchema;
use App\JsonApi\Fields\Translation;
use App\Models\ActivityType;
use LaravelJsonApi\Eloquent\Fields\Str;

/**
 * Class ActivityTypeSchema.
 */
class ActivityTypeSchema extends BaseSchema
{
    public static string $model = ActivityType::class;

    protected int $maxDepth = 0;

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            Str::make('code')
                ->readOnly(),
            Translation::make('name')
                ->sortable(),
        ];
    }
}
