<?php

namespace App\JsonApi\V1\ReviewReplies;

use App\JsonApi\Concerns\Core\BaseSchema;
use App\JsonApi\Concerns\SoftDeletable\SoftDeletableSchema;
use App\JsonApi\Concerns\UserOwnable\UserOwnableSchema;
use App\Models\ReviewReply;
use App\Policies\ReviewPolicy;
use Illuminate\Support\Facades\Gate;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsTo;
use LaravelJsonApi\Eloquent\Fields\Str;

/**
 * Class ReviewReplySchema.
 */
class ReviewReplySchema extends BaseSchema
{
    use SoftDeletableSchema;
    use UserOwnableSchema;

    public static string $model = ReviewReply::class;

    protected int $maxDepth = 1;

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            ...$this->softDeletableFields(
                static fn() => Gate::denies(ReviewPolicy::updatePermission()),
            ),
            ...$this->userOwnableFields(),
            Str::make('body'),
            BelongsTo::make('review')
                ->type('reviews')
                ->readOnlyOnUpdate()
                ->hidden()
                ->cannotEagerLoad(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function filters(): array
    {
        return [
            ...parent::filters(),
            ...$this->softDeletableFilters(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function sortables(): iterable
    {
        return [
            ...parent::sortables(),
            ...$this->userOwnableSortables(),
        ];
    }
}
