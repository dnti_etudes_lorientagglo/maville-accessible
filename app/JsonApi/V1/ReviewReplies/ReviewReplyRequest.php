<?php

namespace App\JsonApi\V1\ReviewReplies;

use App\Helpers\AuthHelper;
use App\JsonApi\Concerns\Accessible\ValidatesAccessibleRequest;
use App\JsonApi\Concerns\SoftDeletable\ValidatesSoftDeletableRequest;
use App\Models\Contracts\Ownable;
use App\Models\Review;
use App\Rules\RelatedRule;
use Illuminate\Support\Collection;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Class ReviewReplyRequest.
 */
class ReviewReplyRequest extends ResourceRequest
{
    use ValidatesAccessibleRequest;
    use ValidatesSoftDeletableRequest;

    public function rules(): array
    {
        return [
            ...$this->softDeletableRules(),
            'body'   => ['required', 'string', 'max:500'],
            'review' => $this->model() ? ['missing'] : [
                'required',
                JsonApiRule::toOne(),
                RelatedRule::make(
                    static fn(Collection $data) => Review::query()
                        ->with('reviewable')
                        ->whereIn('id', $data->map(static fn(array $item) => $item['id']))
                        ->first(),
                    static function (Review | null $review) {
                        $user = AuthHelper::user();
                        $reviewable = $review?->reviewable;
                        if ($user && $reviewable instanceof Ownable) {
                            $actingForOrganization = $user->getActingForOrganization();
                            if ($actingForOrganization) {
                                return $reviewable->owner_organization_id === $actingForOrganization->organization_id;
                            }

                            return $reviewable->owner_user_id === $user->id;
                        }

                        return false;
                    },
                ),
            ],
        ];
    }
}
