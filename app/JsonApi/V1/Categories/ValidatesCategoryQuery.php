<?php

namespace App\JsonApi\V1\Categories;

use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Trait ValidatesCategoryQuery.
 */
trait ValidatesCategoryQuery
{
    /**
     * Get the specific filter rules.
     *
     * @return array[]
     */
    public function categoryFilterRules(): array
    {
        return [
            'filter.hasParents' => ['bail', 'filled', JsonApiRule::boolean()->asString()],
            'filter.searchName' => ['filled', 'string'],
        ];
    }
}
