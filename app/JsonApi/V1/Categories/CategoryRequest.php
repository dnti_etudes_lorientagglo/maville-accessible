<?php

namespace App\JsonApi\V1\Categories;

use App\Rules\ColorRule;
use App\Rules\TranslationRule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Class CategoryRequest.
 */
class CategoryRequest extends ResourceRequest
{
    public function rules(): array
    {
        return [
            'name'        => [TranslationRule::make(['required', 'max:50'])],
            'description' => [TranslationRule::make(['nullable', 'max:255'])],
            'icon'        => ['nullable', 'string'],
            'color'       => ['nullable', 'string', ColorRule::make()],
            'parents'     => [JsonApiRule::toMany()],
            'children'    => [JsonApiRule::toMany()],
        ];
    }
}
