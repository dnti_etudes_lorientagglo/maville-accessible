<?php

namespace App\JsonApi\V1\Categories;

use App\JsonApi\Concerns\Core\ValidatesCollectionQuery;
use App\JsonApi\Concerns\Core\ValidatesPaginableQuery;
use App\JsonApi\Concerns\Publishable\ValidatesPublishableQuery;
use App\JsonApi\Concerns\Searchable\ValidatesSearchableQuery;
use App\JsonApi\Concerns\Sluggable\ValidatesSluggableQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class CategoryCollectionQuery.
 */
class CategoryCollectionQuery extends ResourceQuery
{
    use ValidatesCollectionQuery;
    use ValidatesPaginableQuery;
    use ValidatesPublishableQuery;
    use ValidatesSearchableQuery;
    use ValidatesSluggableQuery;
    use ValidatesCategoryQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
            ...$this->pageRules(false),
            ...$this->publishableRules(),
            ...$this->searchableRules(),
            ...$this->sluggableRules(),
            ...$this->categoryFilterRules(),
        ];
    }
}
