<?php

namespace App\JsonApi\V1\Categories;

use App\JsonApi\Concerns\Core\ValidatesOneQuery;
use App\JsonApi\Concerns\Publishable\ValidatesPublishableQuery;
use App\JsonApi\Concerns\Sluggable\ValidatesSluggableQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class CategoryQuery.
 */
class CategoryQuery extends ResourceQuery
{
    use ValidatesOneQuery;
    use ValidatesPublishableQuery;
    use ValidatesSluggableQuery;
    use ValidatesCategoryQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
            ...$this->publishableRules(),
            ...$this->sluggableRules(),
            ...$this->categoryFilterRules(),
        ];
    }
}
