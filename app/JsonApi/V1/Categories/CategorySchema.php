<?php

namespace App\JsonApi\V1\Categories;

use App\Helpers\JsonApiHelper;
use App\JsonApi\Concerns\Core\BaseSchema;
use App\JsonApi\Concerns\Publishable\PublishableSchema;
use App\JsonApi\Concerns\Sluggable\SluggableSchema;
use App\JsonApi\Concerns\Sourceable\SourceableSchema;
use App\JsonApi\Fields\Translation;
use App\JsonApi\Filters\SearchNameFilter;
use App\Models\Category;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsToMany;
use LaravelJsonApi\Eloquent\Fields\Str;
use LaravelJsonApi\Eloquent\Filters\Has;

/**
 * Class CategorySchema.
 */
class CategorySchema extends BaseSchema
{
    use PublishableSchema;
    use SluggableSchema;
    use SourceableSchema;

    public static string $model = Category::class;

    protected int $maxDepth = 1;

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            ...$this->publishableFields(),
            ...$this->sluggableFields(),
            ...$this->sourceableFields(),
            Translation::make('name')
                ->sortable(),
            Translation::make('description'),
            Str::make('icon'),
            Str::make('color'),
            BelongsToMany::make('parents')
                ->type('categories')
                ->serializeUsing(JsonApiHelper::noLinks())
                ->mustValidate(),
            BelongsToMany::make('children')
                ->type('categories')
                ->serializeUsing(JsonApiHelper::noLinks())
                ->mustValidate(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function filters(): array
    {
        return [
            ...parent::filters(),
            ...$this->publishableFilters(),
            ...$this->sluggableFilters(),
            Has::make($this, 'parents', 'hasParents'),
            SearchNameFilter::make(),
        ];
    }
}
