<?php

namespace App\JsonApi\V1\Sources;

use App\JsonApi\Concerns\Core\BaseSchema;
use App\Models\Source;
use LaravelJsonApi\Eloquent\Fields\ArrayHash;
use LaravelJsonApi\Eloquent\Fields\Str;

/**
 * Class SourceSchema.
 */
class SourceSchema extends BaseSchema
{
    public static string $model = Source::class;

    protected int $maxDepth = 1;

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            Str::make('name')
                ->readOnly(),
            ArrayHash::make('data')
                ->camelizeFields(),
        ];
    }
}
