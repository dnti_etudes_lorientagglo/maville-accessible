<?php

namespace App\JsonApi\V1\Reviews;

use App\JsonApi\Concerns\Accessible\ValidatesAccessibleQuery;
use App\JsonApi\Concerns\Core\ValidatesOneQuery;
use App\JsonApi\Concerns\SoftDeletable\ValidatesSoftDeletableQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class ReviewQuery.
 */
class ReviewQuery extends ResourceQuery
{
    use ValidatesOneQuery;
    use ValidatesAccessibleQuery;
    use ValidatesSoftDeletableQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
            ...$this->accessibleRules(),
            ...$this->softDeletableRules(),
        ];
    }
}
