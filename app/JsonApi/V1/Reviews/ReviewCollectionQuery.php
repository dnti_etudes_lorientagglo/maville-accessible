<?php

namespace App\JsonApi\V1\Reviews;

use App\JsonApi\Concerns\Accessible\ValidatesAccessibleQuery;
use App\JsonApi\Concerns\Core\ValidatesCollectionQuery;
use App\JsonApi\Concerns\Core\ValidatesPaginableQuery;
use App\JsonApi\Concerns\SoftDeletable\ValidatesSoftDeletableQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class ReviewCollectionQuery.
 */
class ReviewCollectionQuery extends ResourceQuery
{
    use ValidatesCollectionQuery;
    use ValidatesAccessibleQuery;
    use ValidatesPaginableQuery;
    use ValidatesSoftDeletableQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
            ...$this->pageRules(),
            ...$this->accessibleRules(),
            ...$this->softDeletableRules(),
        ];
    }
}
