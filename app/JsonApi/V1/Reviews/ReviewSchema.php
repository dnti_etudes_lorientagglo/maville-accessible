<?php

namespace App\JsonApi\V1\Reviews;

use App\JsonApi\Concerns\Accessible\AccessibleSchema;
use App\JsonApi\Concerns\Core\BaseSchema;
use App\JsonApi\Concerns\Reactable\ReactableSchema;
use App\JsonApi\Concerns\SoftDeletable\SoftDeletableSchema;
use App\JsonApi\Concerns\UserOwnable\UserOwnableSchema;
use App\JsonApi\Fields\Translation;
use App\Models\Review;
use App\Policies\ReviewPolicy;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use LaravelJsonApi\Eloquent\Fields\Number;
use LaravelJsonApi\Eloquent\Fields\Relations\HasMany;
use LaravelJsonApi\Eloquent\Fields\Relations\MorphTo;
use LaravelJsonApi\Eloquent\QueryBuilder\JsonApiBuilder;

/**
 * Class ReviewSchema.
 */
class ReviewSchema extends BaseSchema
{
    use AccessibleSchema;
    use ReactableSchema;
    use SoftDeletableSchema;
    use UserOwnableSchema;

    public static string $model = Review::class;

    protected int $maxDepth = 2;

    /**
     * {@inheritDoc}
     */
    public function newQuery($query = null): JsonApiBuilder
    {
        return parent::newQuery($this->reactableQuery($query));
    }

    /**
     * {@inheritDoc}
     */
    public function relatableQuery(?Request $request, Relation $query): Relation
    {
        $this->reactableQuery($query);

        return parent::relatableQuery($request, $query);
    }

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            ...$this->accessibleFields(),
            ...$this->reactableFields(),
            ...$this->softDeletableFields(static fn() => Gate::denies(ReviewPolicy::updatePermission())),
            ...$this->userOwnableFields(),
            Number::make('rating'),
            Translation::make('body'),
            MorphTo::make('reviewable')
                ->types('places', 'activities')
                ->readOnlyOnUpdate()
                ->hidden()
                ->cannotEagerLoad(),
            HasMany::make('replies')
                ->type('review-replies')
                ->readOnly(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function filters(): array
    {
        return [
            ...parent::filters(),
            ...$this->accessibleFilters(),
            ...$this->softDeletableFilters(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function sortables(): iterable
    {
        return [
            ...parent::sortables(),
            ...$this->accessibleSortables(),
            ...$this->userOwnableSortables(),
        ];
    }
}
