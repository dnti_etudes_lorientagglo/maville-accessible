<?php

namespace App\JsonApi\V1\Reviews;

use App\Helpers\AuthHelper;
use App\JsonApi\Concerns\Accessible\ValidatesAccessibleRequest;
use App\JsonApi\Concerns\SoftDeletable\ValidatesSoftDeletableRequest;
use App\Models\Review;
use App\Rules\TranslationRule;
use App\Services\Validation\Validator;
use Illuminate\Http\Response;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class ReviewRequest.
 */
class ReviewRequest extends ResourceRequest
{
    use ValidatesAccessibleRequest;
    use ValidatesSoftDeletableRequest;

    public function rules(): array
    {
        return [
            ...$this->accessibleRules(),
            ...$this->softDeletableRules(),
            'rating'     => ['required', 'integer', 'between:0,5'],
            'body'       => [TranslationRule::make(['nullable', 'max:500'])],
            'reviewable' => [$this->model() ? 'missing' : 'required', JsonApiRule::toOne()],
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param Validator $validator
     *
     * @return void
     */
    public function withValidator(Validator $validator): void
    {
        if ($this->isCreating()) {
            $validator->after(function () {
                $message = $this->getExceedingReviewsError();
                if ($message) {
                    throw new HttpException(Response::HTTP_CONFLICT, $message);
                }
            });
        }
    }

    /**
     * Get a potential requests limits exceed error message.
     *
     * @return string|null
     */
    private function getExceedingReviewsError(): string | null
    {
        if (! ($data['reviewable']['type'] ?? null) || ! ($data['reviewable']['type'] ?? null)) {
            return null;
        }

        $data = $this->validationData();
        $user = AuthHelper::user();
        $query = Review::query()
            ->where('reviewable_type', $data['reviewable']['type'])
            ->where('reviewable_id', $data['reviewable']['id'])
            ->whereNotNull('owner_user_id')
            ->where('owner_user_id', $user->id);

        return $query->exists()
            ? __('validation.custom.reviews.exists')
            : null;
    }
}
