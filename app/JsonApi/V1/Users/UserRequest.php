<?php

namespace App\JsonApi\V1\Users;

use App\Helpers\RuleHelper;
use App\Models\Enums\RoleScopeName;
use App\Models\Role;
use App\Models\User;
use App\Policies\RolePolicy;
use App\Rules\AllowsRule;
use Illuminate\Validation\Rule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Class UserRequest.
 */
class UserRequest extends ResourceRequest
{
    public function rules(): array
    {
        /** @var User|null $user */
        $user = $this->model();

        return [
            'firstName'                => ['required', 'string', 'max:30'],
            'lastName'                 => ['required', 'string', 'max:30'],
            'displayInitials'          => ['required', 'boolean'],
            ...RuleHelper::arrayHash('notificationsPreferences', collect($user->notificationsDefaults)->mapWithKeys(
                static fn($_, string $type) => [
                    $type        => ['nullable', 'array', 'max:2'],
                    $type . '.*' => ['required', 'in:database,mail'],
                ],
            )->all(), ['nullable']),
            'roles'                    => [
                AllowsRule::make(RolePolicy::ASSIGN_ANY, Role::class),
                JsonApiRule::toMany(),
            ],
            'roles.*.id'               => [
                'required',
                Rule::in(
                    Role::query()
                        ->where('scope_name', RoleScopeName::GLOBAL->value)
                        ->pluck('id'),
                ),
            ],
            'actingForOrganization'    => [
                'nullable',
                JsonApiRule::toOne(),
            ],
            'actingForOrganization.id' => [
                'filled',
                Rule::in($user->memberOfOrganizations()->pluck('id')),
            ],
        ];
    }
}
