<?php

namespace App\JsonApi\V1\Users;

use App\Helpers\AuthHelper;
use App\Helpers\LocaleHelper;
use App\Models\User;
use App\Policies\UserPolicy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use LaravelJsonApi\Core\Resources\JsonApiResource;

/**
 * Class UserResource.
 *
 * @mixin User
 */
class UserResource extends JsonApiResource
{
    use LoadsNotificationsCount;

    /**
     * Get the resource's attributes.
     *
     * @param Request|null $request
     *
     * @return iterable
     */
    public function attributes($request): iterable
    {
        $canViewDetails = Gate::allows(UserPolicy::VIEW_DETAILS, $this->resource);

        return [
            'firstName'       => $this->first_name,
            'lastName'        => $this->last_name,
            'displayInitials' => $this->display_initials,
            'locale'          => LocaleHelper::bcp47($this->preferredLocale()),
            'createdAt'       => $this->created_at,
            'updatedAt'       => $this->updated_at,
            'searchScore'     => $this->search_score,
            'allows'          => $this->allowsPermissions,
            'reviewsCount'    => $this->reviewsCount,
            $this->mergeWhen(! $canViewDetails, [
                'email' => $this->hiddenEmail(),
            ]),
            $this->mergeWhen($canViewDetails, [
                'email'               => $this->email,
                'emailVerifiedAt'     => $this->email_verified_at,
                'blockedAt'           => $this->blocked_at,
                'lastLoginAt'         => $this->last_login_at,
                'scheduledDeletionAt' => $this->scheduled_deletion_at,
            ]),
            $this->mergeWhen(AuthHelper::user()?->id === $this->id, fn() => [
                'unreadNotificationsCount' => $this->unreadNotificationsCount,
                'notificationsPreferences' => $this->filteredNotificationsPreferences,
            ]),
        ];
    }

    /**
     * Get the resource's relationships.
     *
     * @param Request|null $request
     *
     * @return iterable
     */
    public function relationships($request): iterable
    {
        return [
            $this->mergeWhen(Gate::allows(UserPolicy::VIEW_DETAILS, $this->resource), [
                $this->relation('roles')->withoutLinks(),
                $this->relation('permissions')->withoutLinks(),
                $this->relation('actingForOrganization')->withoutLinks(),
                $this->relation('memberOfOrganizations')->withoutLinks(),
            ]),
        ];
    }

    /**
     * Get a partially hidden email address.
     *
     * @return string
     */
    private function hiddenEmail(): string
    {
        /** @var Stringable $name */
        /** @var Stringable $domain */
        [$name, $domain] = Str::of($this->email)->explode('@')->map(fn(string $s) => Str::of($s));
        $visibleLength = max(2, ceil($name->length() * 0.33));
        [$startLength, $endLength] = [ceil($visibleLength / 2), floor($visibleLength / 2)];
        $replaceLength = $name->length() - ($startLength + $endLength);
        $domainNameLength = $domain->beforeLast('.')->length();

        return implode('@', [
            $name->substrReplace(
                Str::of('*')->repeat($name->length() - $startLength),
                $startLength,
                $replaceLength,
            ),
            $domain->substrReplace(
                Str::of('*')->repeat($domainNameLength - 1),
                1,
                $domainNameLength - 1,
            ),
        ]);
    }
}
