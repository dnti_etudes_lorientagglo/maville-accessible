<?php

namespace App\JsonApi\V1\Users;

use App\JsonApi\Concerns\Core\ValidatesCollectionQuery;
use App\JsonApi\Concerns\Core\ValidatesPaginableQuery;
use App\JsonApi\Concerns\Searchable\ValidatesSearchableQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;
use LaravelJsonApi\Validation\Rules\AllowedIncludePaths;

/**
 * Class UserCollectionQuery.
 */
class UserCollectionQuery extends ResourceQuery
{
    use ValidatesCollectionQuery;
    use ValidatesPaginableQuery;
    use ValidatesSearchableQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(
                include: static fn(AllowedIncludePaths $include) => $include->forget('roles', 'permissions')
            ),
            ...$this->pageRules(),
            ...$this->searchableRules(),
        ];
    }
}
