<?php

namespace App\JsonApi\V1\Users;

/**
 * Trait LoadsNotificationsCount.
 */
trait LoadsNotificationsCount
{
    /**
     * Check if notifications count should be loaded.
     *
     * @return bool
     */
    private function shouldLoadNotificationsCount(): bool
    {
        return filter_var(request()->query('withNotificationsCount'), FILTER_VALIDATE_BOOL) === true;
    }
}
