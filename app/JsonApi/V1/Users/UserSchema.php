<?php

namespace App\JsonApi\V1\Users;

use App\Helpers\QueryHelper;
use App\JsonApi\Concerns\Core\BaseSchema;
use App\JsonApi\Sorting\ClosureSort;
use App\Models\Notification;
use App\Models\Role;
use App\Models\User;
use App\Policies\RolePolicy;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\Database\Query\Builder as BaseBuilder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use LaravelJsonApi\Eloquent\Fields\ArrayHash;
use LaravelJsonApi\Eloquent\Fields\Boolean;
use LaravelJsonApi\Eloquent\Fields\DateTime;
use LaravelJsonApi\Eloquent\Fields\Number;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsTo;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsToMany;
use LaravelJsonApi\Eloquent\Fields\Relations\HasMany;
use LaravelJsonApi\Eloquent\Fields\Str;
use LaravelJsonApi\Eloquent\QueryBuilder\JsonApiBuilder;

/**
 * Class UserSchema.
 */
class UserSchema extends BaseSchema
{
    use LoadsNotificationsCount;

    public static string $model = User::class;

    protected int $maxDepth = 3;

    /**
     * {@inheritDoc}
     */
    public function newQuery($query = null): JsonApiBuilder
    {
        return parent::newQuery(
            QueryHelper::clarifySelects($query)
                ->when($this->shouldLoadNotificationsCount())
                ->addSelect([
                    'unread_notifications_count' => static fn(BaseBuilder $subQuery) => $subQuery
                        ->select(DB::raw('count(*)'))
                        ->from((new Notification())->getTable(), 'unread_notifications')
                        ->whereNull('unread_notifications.read_at')
                        ->where('unread_notifications.notifiable_type', (new User())->getMorphClass())
                        ->whereColumn(
                            'unread_notifications.notifiable_id',
                            $query->qualifyColumn((new User())->getKeyName()),
                        ),
                ]),
        );
    }

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            Str::make('firstName'),
            Str::make('lastName'),
            Boolean::make('displayInitials'),
            Str::make('email')
                ->readOnly()
                ->sortable(),
            DateTime::make('emailVerifiedAt')
                ->readOnly()
                ->retainTimezone()
                ->sortable(),
            DateTime::make('blockedAt')
                ->readOnly()
                ->retainTimezone()
                ->sortable(),
            DateTime::make('lastLoginAt')
                ->readOnly()
                ->retainTimezone()
                ->sortable(),
            DateTime::make('scheduledDeletionAt')
                ->readOnly()
                ->retainTimezone()
                ->sortable(),
            Str::make('locale')
                ->readOnly(),
            ArrayHash::make('notificationsPreferences'),
            BelongsToMany::make('roles')
                ->readOnly(static fn() => Gate::denies(RolePolicy::ASSIGN_ANY, Role::class)),
            BelongsToMany::make('permissions')
                ->readOnly(),
            Number::make('unreadNotificationsCount')
                ->readOnly(),
            Number::make('reviewsCount')
                ->readOnly(),
            BelongsTo::make('actingForOrganization')
                ->type('organization-members'),
            HasMany::make('memberOfOrganizations')
                ->type('organization-members')
                ->readOnly(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function sortables(): iterable
    {
        return [
            ...parent::sortables(),
            ClosureSort::make(
                'fullName',
                static fn(Builder $query, string $direction) => $query
                    ->orderBy($query->getModel()->qualifyColumn('first_name'), $direction)
                    ->orderBy($query->getModel()->qualifyColumn('last_name'), $direction),
            ),
        ];
    }
}
