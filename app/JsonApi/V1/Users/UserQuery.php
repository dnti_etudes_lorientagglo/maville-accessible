<?php

namespace App\JsonApi\V1\Users;

use App\JsonApi\Concerns\Core\ValidatesOneQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Class UserQuery.
 */
class UserQuery extends ResourceQuery
{
    use ValidatesOneQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
            'withNotificationsCount' => ['bail', 'filled', JsonApiRule::boolean()->asString()],
        ];
    }
}
