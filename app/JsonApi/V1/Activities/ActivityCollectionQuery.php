<?php

namespace App\JsonApi\V1\Activities;

use App\JsonApi\Concerns\Accessible\ValidatesAccessibleQuery;
use App\JsonApi\Concerns\Categorizable\ValidatesCategorizableQuery;
use App\JsonApi\Concerns\Core\ValidatesCollectionQuery;
use App\JsonApi\Concerns\Core\ValidatesPaginableQuery;
use App\JsonApi\Concerns\Publishable\ValidatesPublishableQuery;
use App\JsonApi\Concerns\Reviewable\ValidatesReviewableQuery;
use App\JsonApi\Concerns\Searchable\ValidatesSearchableQuery;
use App\JsonApi\Concerns\Sluggable\ValidatesSluggableQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class ActivityCollectionQuery.
 */
class ActivityCollectionQuery extends ResourceQuery
{
    use ValidatesCollectionQuery;
    use ValidatesAccessibleQuery;
    use ValidatesCategorizableQuery;
    use ValidatesPaginableQuery;
    use ValidatesSearchableQuery;
    use ValidatesPublishableQuery;
    use ValidatesReviewableQuery;
    use ValidatesSluggableQuery;
    use ValidatesActivityQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
            ...$this->pageRules(),
            ...$this->searchableRules(),
            ...$this->accessibleRules(),
            ...$this->categorizableRules(),
            ...$this->publishableRules(),
            ...$this->reviewableRules(),
            ...$this->sluggableRules(),
            ...$this->activityRules(),
        ];
    }
}
