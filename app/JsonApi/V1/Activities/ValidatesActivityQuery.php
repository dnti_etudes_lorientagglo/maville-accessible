<?php

namespace App\JsonApi\V1\Activities;

use App\Models\Enums\ActivityPublicCode;
use App\Models\Enums\ActivityTypeCode;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Trait ValidatesActivityQuery.
 *
 * @mixin ResourceQuery
 */
trait ValidatesActivityQuery
{
    use QueriesActivitiesOpening;

    /**
     * Create the rules for this specific query.
     *
     * @return array
     */
    private function activityRules(): array
    {
        $openingDaysFilters = [
            'filter.openingInAtLeastDays' => [
                'filled',
                'integer',
            ],
            'filter.openedAtLeastDaysAgo' => [
                'filled',
                'integer',
            ],
            'filter.openingInAtMostDays'  => [
                'filled',
                'integer',
            ],
            'filter.openedAtMostDaysAgo'  => [
                'filled',
                'integer',
            ],
            'filter.hasNextOpening'       => [
                'filled',
                JsonApiRule::boolean()->asString(),
            ],
            'filter.hasPrevOpening'       => [
                'filled',
                JsonApiRule::boolean()->asString(),
            ],
        ];

        return [
            'filter.activityType'                  => [
                'filled',
                Rule::in(ActivityTypeCode::cases()),
            ],
            'filter.publicCode'                    => ['filled', 'array'],
            'filter.publicCode.*'                  => ['required', Rule::in(ActivityPublicCode::cases())],
            ...$openingDaysFilters,
            self::OPENING_DAYS_DIFFERENCE_OVER_KEY => [
                Rule::requiredIf(fn() => $this->hasOpeningDaysDifferenceSortInQuery()),
                'required_with:' . implode(',', array_keys($openingDaysFilters)),
                'filled',
                'date_format:Y-m-d',
            ],
        ];
    }

    /**
     * Check if the request query contains a search aware sort.
     *
     * @return bool
     */
    private function hasOpeningDaysDifferenceSortInQuery(): bool
    {
        $data = $this->validationData();
        $sort = Arr::get($data, 'sort');
        if (! is_string($sort)) {
            return false;
        }

        $sorts = explode(',', str_replace('-', '', $sort));

        return in_array('openingDaysDifference', $sorts)
            || in_array('openedDaysAgo', $sorts);
    }
}
