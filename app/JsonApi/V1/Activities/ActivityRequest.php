<?php

namespace App\JsonApi\V1\Activities;

use App\Helpers\RuleHelper;
use App\JsonApi\Concerns\Accessible\ValidatesAccessibleRequest;
use App\JsonApi\Concerns\Categorizable\ValidatesCategorizableRequest;
use App\JsonApi\Concerns\Contactable\ValidatesContactableRequest;
use App\JsonApi\Concerns\Coverable\ValidatesCoverableRequest;
use App\JsonApi\Concerns\Imageable\ValidatesImageableRequest;
use App\JsonApi\Concerns\Linkable\ValidatesLinkableRequest;
use App\JsonApi\Concerns\Mediable\ValidatesMediableRequest;
use App\JsonApi\Concerns\Ownable\ValidatesOwnableRequest;
use App\Models\Enums\ActivityPricingCode;
use App\Models\Enums\ActivityPublicCode;
use App\Rules\TranslationRule;
use Illuminate\Validation\Rule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Class ActivityRequest.
 */
class ActivityRequest extends ResourceRequest
{
    use ValidatesAccessibleRequest;
    use ValidatesCategorizableRequest;
    use ValidatesContactableRequest;
    use ValidatesCoverableRequest;
    use ValidatesImageableRequest;
    use ValidatesLinkableRequest;
    use ValidatesMediableRequest;
    use ValidatesOwnableRequest;

    public function rules(): array
    {
        return [
            ...$this->accessibleRules(),
            ...$this->categorizableRules(required: false),
            ...$this->contactableRules(),
            ...$this->coverableRules(),
            ...$this->imageableRules(),
            ...$this->linkableRules(),
            ...$this->mediableRules(),
            ...$this->ownableRules(),
            'name'               => [TranslationRule::make(['required', 'max:100'])],
            'description'        => [TranslationRule::make(['nullable', 'max:255'])],
            'body'               => [TranslationRule::make(['nullable', 'max:50000'])],
            'pricingCode'        => ['nullable', Rule::in(ActivityPricingCode::cases()),],
            'pricingDescription' => [TranslationRule::make(['nullable', 'max:500'])],
            'publicCodes'        => ['nullable', 'array'],
            'publicCodes.*'      => ['required', Rule::in(ActivityPublicCode::cases())],
            'openingOnPeriods'   => ['required', 'boolean'],
            ...RuleHelper::arrayList('openingPeriods', [
                'start' => ['required', 'date_format:Y-m-d'],
                'end'   => ['required', 'date_format:Y-m-d'],
            ], ['nullable']),
            ...RuleHelper::openingDatesHours('openingDatesHours', ['nullable']),
            ...RuleHelper::openingWeekdaysHours('openingWeekdaysHours', ['nullable']),
            ...($this->validationData()['place'] ?? null ? [
                'place' => ['required', JsonApiRule::toOne()],
            ] : [
                ...RuleHelper::address('address', ['required']),
            ]),
            'activityType'       => ['required', JsonApiRule::toOne()],
        ];
    }
}
