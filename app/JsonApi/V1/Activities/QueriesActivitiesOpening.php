<?php

namespace App\JsonApi\V1\Activities;

use App\Helpers\QueryHelper;
use App\Models\Activity;
use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Support\Facades\DB;

/**
 * Trait QueriesActivitiesOpening.
 */
trait QueriesActivitiesOpening
{
    /**
     * Query param to load opening days difference over a given date.
     */
    protected const OPENING_DAYS_DIFFERENCE_OVER_KEY = 'withOpeningDaysDifferenceOver';

    /**
     * Get the opening days difference reference date.
     *
     * @return string|null
     */
    protected function getOpeningDaysDifferenceOver(): string | null
    {
        return request()->query(self::OPENING_DAYS_DIFFERENCE_OVER_KEY);
    }

    /**
     * Load the opening days difference over a reference date.
     *
     * @param EloquentBuilder $query
     * @param Carbon          $date
     *
     * @return EloquentBuilder
     */
    protected function queryWithOpeningDaysDifference(EloquentBuilder $query, Carbon $date): EloquentBuilder
    {
        $cases = [
            'when (opening_on_periods = true and coalesce(opening_periods, \'[]\'::jsonb) = \'[]\'::jsonb) then 0',
            'when (opening_on_periods = true and periods.start <= ?::date and periods.end >= ?::date) then 0',
            'when (opening_on_periods = true) then LEAST(periods.start - ?::date, periods.end - ?::date)',
            'else dates.date - ?::date',
        ];

        $allDifferenceQuery = Activity::query()
            ->getQuery()
            ->selectRaw(
                'id, case ' . implode(' ', $cases) . ' end as difference',
                array_fill(0, 5, $date->toDateString()),
            )
            ->leftJoin(DB::raw('jsonb_to_recordset(opening_periods) as periods("start" date, "end" date)'), 'id', 'id')
            ->leftJoin(DB::raw('jsonb_to_recordset(opening_dates_hours) as dates("date" date)'), 'id', 'id');

        $aggDifferenceQuery = DB::table($allDifferenceQuery, 'activities_differences_agg')
            ->selectRaw('distinct on (id, difference >= 0) id, difference')
            ->orderByRaw('id, difference >= 0, abs(difference)');

        $differenceQuery = DB::table($aggDifferenceQuery, 'activities_differences')
            ->selectRaw(
                'id, ' .
                'min(difference) as prev_opening_days_difference, ' .
                'max(difference) as next_opening_days_difference',
            )
            ->groupBy('id');

        return QueryHelper::clarifySelects($query)
            ->addSelect(['prev_opening_days_difference', 'next_opening_days_difference'])
            ->joinSub(
                $differenceQuery,
                'activities_differences',
                'activities_differences.id',
                $query->qualifyColumn('id'),
            );
    }
}
