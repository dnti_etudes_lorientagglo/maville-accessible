<?php

namespace App\JsonApi\V1\Activities;

use App\Helpers\JsonApiHelper;
use App\JsonApi\Concerns\Accessible\AccessibleSchema;
use App\JsonApi\Concerns\Categorizable\CategorizableSchema;
use App\JsonApi\Concerns\Contactable\ContactableSchema;
use App\JsonApi\Concerns\Core\BaseSchema;
use App\JsonApi\Concerns\Coverable\CoverableSchema;
use App\JsonApi\Concerns\Imageable\ImageableSchema;
use App\JsonApi\Concerns\Linkable\LinkableSchema;
use App\JsonApi\Concerns\Mediable\MediableSchema;
use App\JsonApi\Concerns\Ownable\OwnableSchema;
use App\JsonApi\Concerns\Pinnable\PinnableSchema;
use App\JsonApi\Concerns\Publishable\PublishableSchema;
use App\JsonApi\Concerns\Reviewable\ReviewableSchema;
use App\JsonApi\Concerns\Sluggable\SluggableSchema;
use App\JsonApi\Concerns\Sourceable\SourceableSchema;
use App\JsonApi\Fields\Translation;
use App\JsonApi\Filters\ClosureFilter;
use App\JsonApi\Sorting\ClosureSort;
use App\Models\Activity;
use App\Models\Enums\ActivityPublicCode;
use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Http\Request;
use LaravelJsonApi\Eloquent\Fields\ArrayHash;
use LaravelJsonApi\Eloquent\Fields\ArrayList;
use LaravelJsonApi\Eloquent\Fields\Boolean;
use LaravelJsonApi\Eloquent\Fields\Number;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsTo;
use LaravelJsonApi\Eloquent\Fields\Str;
use LaravelJsonApi\Eloquent\QueryBuilder\JsonApiBuilder;

/**
 * Class ActivitySchema.
 */
class ActivitySchema extends BaseSchema
{
    use AccessibleSchema;
    use CategorizableSchema;
    use ContactableSchema;
    use CoverableSchema;
    use ImageableSchema;
    use LinkableSchema;
    use MediableSchema;
    use OwnableSchema;
    use PinnableSchema;
    use PublishableSchema;
    use ReviewableSchema;
    use SluggableSchema;
    use SourceableSchema;
    use QueriesActivitiesOpening;

    public static string $model = Activity::class;

    protected int $maxDepth = 3;

    /**
     * {@inheritDoc}
     */
    public function newQuery($query = null): JsonApiBuilder
    {
        return parent::newQuery($this->reviewableQuery($query));
    }

    /**
     * {@inheritDoc}
     */
    public function indexQuery(?Request $request, EloquentBuilder $query): EloquentBuilder
    {
        $query = parent::indexQuery($request, $query);

        $openingDaysDifferenceOver = $this->getOpeningDaysDifferenceOver();
        if ($openingDaysDifferenceOver) {
            $this->queryWithOpeningDaysDifference($query, Carbon::parse($openingDaysDifferenceOver));
        }

        return $this->publishableQuery(
            $request,
            $query,
            fn() => $this->ownableQuery($query),
        );
    }

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            ...$this->accessibleFields(),
            ...$this->categorizableFields(),
            ...$this->contactableFields(),
            ...$this->coverableFields(),
            ...$this->imageableFields(),
            ...$this->linkableFields(),
            ...$this->mediableFields(),
            ...$this->pinnableFields(),
            ...$this->publishableFields(),
            ...$this->ownableFields(),
            ...$this->reviewableFields(),
            ...$this->sluggableFields(),
            ...$this->sourceableFields(),
            Translation::make('name')
                ->sortable(),
            Translation::make('description'),
            Translation::make('body'),
            Str::make('pricingCode'),
            Translation::make('pricingDescription'),
            ArrayList::make('publicCodes'),
            Number::make('prevOpeningDaysDifference')
                ->readOnly(),
            Number::make('nextOpeningDaysDifference')
                ->readOnly(),
            Boolean::make('openingOnPeriods'),
            ArrayList::make('openingPeriods'),
            ArrayList::make('openingDatesHours'),
            ArrayList::make('openingWeekdaysHours'),
            ArrayHash::make('address'),
            BelongsTo::make('place')
                ->serializeUsing(JsonApiHelper::noLinks()),
            BelongsTo::make('activityType')
                ->readOnlyOnUpdate()
                ->serializeUsing(JsonApiHelper::noLinks()),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function filters(): array
    {
        return [
            ...parent::filters(),
            ...$this->accessibleFilters(),
            ...$this->categorizableFilters(),
            ...$this->publishableFilters(),
            ...$this->sluggableFilters(),
            ClosureFilter::make(
                'activityType',
                static fn(Builder $query, string $code) => $query
                    ->whereRelation('activityType', 'code', $code),
            ),
            ClosureFilter::make(
                'publicCode',
                static fn(Builder $query, array $codes) => $query->where(
                    static function (Builder $query) use ($codes) {
                        if (in_array(ActivityPublicCode::ALL->value, $codes)) {
                            $query
                                ->whereNull('public_codes')
                                ->orWhere('public_codes', '[]')
                                ->orWhereJsonContains('public_codes', ActivityPublicCode::ALL->value);
                        } else {
                            collect($codes)->each(
                                static fn(string $code) => $query->orWhereJsonContains('public_codes', $code),
                            );
                        }
                    },
                ),
            ),
            ClosureFilter::make(
                'openingInAtLeastDays',
                static fn(Builder $query, int $diff) => $query
                    ->where('activities_differences.next_opening_days_difference', '>=', $diff),
            )->deserializeAsInteger(),
            ClosureFilter::make(
                'openedAtLeastDaysAgo',
                static fn(Builder $query, int $diff) => $query
                    ->where('activities_differences.prev_opening_days_difference', '<=', -1 * $diff),
            )->deserializeAsInteger(),
            ClosureFilter::make(
                'openingInAtMostDays',
                static fn(Builder $query, int $diff) => $query
                    ->where('activities_differences.next_opening_days_difference', '<=', $diff),
            )->deserializeAsInteger(),
            ClosureFilter::make(
                'openedAtMostDaysAgo',
                static fn(Builder $query, int $diff) => $query
                    ->where('activities_differences.prev_opening_days_difference', '>=', -1 * $diff),
            )->deserializeAsInteger(),
            ClosureFilter::make(
                'hasNextOpening',
                static fn(Builder $query, bool $hasNext) => $query->where(
                    'activities_differences.next_opening_days_difference',
                    $hasNext ? '>=' : '<',
                    0,
                ),
            )->deserializeAsBoolean(),
            ClosureFilter::make(
                'hasPrevOpening',
                static fn(Builder $query, bool $hasPrev) => $query->where(
                    'activities_differences.prev_opening_days_difference',
                    $hasPrev ? '<' : '>=',
                    0,
                ),
            )->deserializeAsBoolean(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function sortables(): iterable
    {
        return [
            ...parent::sortables(),
            ...$this->accessibleSortables(),
            ...$this->ownableSortables(),
            ...$this->pinnableSortables(),
            ClosureSort::make(
                'openingInDays',
                static fn(Builder $query, string $direction) => $query
                    ->orderBy('activities_differences.next_opening_days_difference', $direction),
            ),
            ClosureSort::make(
                'openedDaysAgo',
                static fn(Builder $query, string $direction) => $query->orderBy(
                    'activities_differences.prev_opening_days_difference',
                    $direction === 'asc' ? 'desc' : 'asc',
                ),
            ),
        ];
    }
}
