<?php

namespace App\JsonApi\V1\Notifications;

use App\JsonApi\Concerns\Core\ValidatesCollectionQuery;
use App\JsonApi\Concerns\Core\ValidatesPaginableQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class NotificationCollectionQuery.
 */
class NotificationCollectionQuery extends ResourceQuery
{
    use ValidatesCollectionQuery;
    use ValidatesPaginableQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
            ...$this->cursorPageRules(),
        ];
    }
}
