<?php

namespace App\JsonApi\V1\Notifications;

use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Class NotificationRequest.
 */
class NotificationRequest extends ResourceRequest
{
    public function rules(): array
    {
        return [
            'readAt' => ['nullable', JsonApiRule::dateTime()],
        ];
    }
}
