<?php

namespace App\JsonApi\V1\Notifications;

use App\JsonApi\Concerns\Core\ValidatesOneQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class NotificationQuery.
 */
class NotificationQuery extends ResourceQuery
{
    use ValidatesOneQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
        ];
    }
}
