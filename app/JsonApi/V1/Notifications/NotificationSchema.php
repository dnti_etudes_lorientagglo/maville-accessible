<?php

namespace App\JsonApi\V1\Notifications;

use App\Helpers\AuthHelper;
use App\Helpers\NotificationsHelper;
use App\JsonApi\Concerns\Core\BaseSchema;
use App\Models\Notification;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Http\Request;
use LaravelJsonApi\CursorPagination\CursorPagination;
use LaravelJsonApi\Eloquent\Contracts\Paginator;
use LaravelJsonApi\Eloquent\Fields\ArrayHash;
use LaravelJsonApi\Eloquent\Fields\DateTime;
use LaravelJsonApi\Eloquent\Fields\Str;

/**
 * Class NotificationSchema.
 */
class NotificationSchema extends BaseSchema
{
    public static string $model = Notification::class;

    protected int $maxDepth = 0;

    /**
     * {@inheritDoc}
     */
    public function indexQuery(?Request $request, EloquentBuilder $query): EloquentBuilder
    {
        return parent::indexQuery($request, $query)
            ->whereMorphedTo('notifiable', AuthHelper::user());
    }

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            Str::make('notificationType', 'type')
                ->readOnly()
                ->serializeUsing(
                    static fn(string $type) => NotificationsHelper::notificationType($type)
                ),
            ArrayHash::make('data')
                ->readOnly(),
            DateTime::make('readAt')
                ->retainTimezone()
                ->sortable(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function pagination(): ?Paginator
    {
        return CursorPagination::make($this->id());
    }
}
