<?php

namespace App\JsonApi\V1\Places;

use Illuminate\Http\Request;

/**
 * Trait UsesLocationFilter.
 */
trait UsesLocationFilter
{
    /**
     * Get the PostgreSQL expression to compute distance from a location.
     *
     * @param Request $request
     *
     * @return array
     */
    private function locationDistanceExpression(Request $request): array
    {
        return [
            "ST_DistanceSphere(" .
            "ST_MakePoint(" .
            "(address->'geometry'->'coordinates'->0)::float, " .
            "(address->'geometry'->'coordinates'->1)::float" .
            "), " .
            "ST_MakePoint(?, ?)" .
            ")",
            $this->locationCoordinates($request),
        ];
    }

    /**
     * Get the location filter coordinates.
     *
     * @param Request $request
     *
     * @return array
     */
    private function locationCoordinates(Request $request): array
    {
        return [
            floatval($request->input('location.longitude')),
            floatval($request->input('location.latitude')),
        ];
    }

    /**
     * Get the location filter radius.
     *
     * @param Request $request
     *
     * @return float
     */
    private function locationRadius(Request $request): float
    {
        return floatval($request->input('location.radius'));
    }
}
