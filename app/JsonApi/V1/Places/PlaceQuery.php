<?php

namespace App\JsonApi\V1\Places;

use App\JsonApi\Concerns\Accessible\ValidatesAccessibleQuery;
use App\JsonApi\Concerns\Categorizable\ValidatesCategorizableQuery;
use App\JsonApi\Concerns\Core\ValidatesOneQuery;
use App\JsonApi\Concerns\Publishable\ValidatesPublishableQuery;
use App\JsonApi\Concerns\Reviewable\ValidatesReviewableQuery;
use App\JsonApi\Concerns\Sluggable\ValidatesSluggableQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class PlaceQuery.
 */
class PlaceQuery extends ResourceQuery
{
    use ValidatesOneQuery;
    use ValidatesAccessibleQuery;
    use ValidatesCategorizableQuery;
    use ValidatesPublishableQuery;
    use ValidatesReviewableQuery;
    use ValidatesSluggableQuery;
    use ValidatesPlaceQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
            ...$this->accessibleRules(),
            ...$this->categorizableRules(),
            ...$this->publishableRules(),
            ...$this->reviewableRules(),
            ...$this->sluggableRules(),
            ...$this->placeRules(),
        ];
    }
}
