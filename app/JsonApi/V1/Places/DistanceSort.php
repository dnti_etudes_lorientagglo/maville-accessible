<?php

namespace App\JsonApi\V1\Places;

use Illuminate\Contracts\Database\Eloquent\Builder;
use LaravelJsonApi\Eloquent\Contracts\SortField;

/**
 * Class DistanceSort.
 */
class DistanceSort implements SortField
{
    /**
     * Create a new sort field.
     *
     * @param string $name
     *
     * @return static
     */
    public static function make(string $name = 'distance'): static
    {
        return new static($name);
    }

    /**
     * DistanceSort constructor.
     *
     * @param string $name
     */
    public function __construct(private readonly string $name)
    {
    }

    /**
     * Get the name of the sort field.
     *
     * @return string
     */
    public function sortField(): string
    {
        return $this->name;
    }

    /**
     * Apply the sort order to the query.
     *
     * @param Builder $query
     * @param string  $direction
     *
     * @return Builder
     */
    public function sort($query, string $direction = 'asc'): Builder
    {
        return $query->orderBy('distance', $direction);
    }
}
