<?php

namespace App\JsonApi\V1\Places;

use App\Helpers\RuleHelper;
use App\JsonApi\Concerns\Accessible\ValidatesAccessibleRequest;
use App\JsonApi\Concerns\Categorizable\ValidatesCategorizableRequest;
use App\JsonApi\Concerns\Contactable\ValidatesContactableRequest;
use App\JsonApi\Concerns\Coverable\ValidatesCoverableRequest;
use App\JsonApi\Concerns\Imageable\ValidatesImageableRequest;
use App\JsonApi\Concerns\Linkable\ValidatesLinkableRequest;
use App\JsonApi\Concerns\Ownable\ValidatesOwnableRequest;
use App\Rules\TranslationRule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;

/**
 * Class PlaceRequest.
 */
class PlaceRequest extends ResourceRequest
{
    use ValidatesAccessibleRequest;
    use ValidatesCategorizableRequest;
    use ValidatesContactableRequest;
    use ValidatesCoverableRequest;
    use ValidatesImageableRequest;
    use ValidatesLinkableRequest;
    use ValidatesOwnableRequest;

    public function rules(): array
    {
        return [
            ...$this->accessibleRules(),
            ...$this->categorizableRules(),
            ...$this->contactableRules(),
            ...$this->coverableRules(required: false),
            ...$this->imageableRules(),
            ...$this->linkableRules(),
            ...$this->ownableRules(),
            'name'        => [TranslationRule::make(['required', 'max:100'])],
            'description' => [TranslationRule::make(['nullable', 'max:255'])],
            ...RuleHelper::openingWeekdaysHours('openingHours', ['nullable']),
            ...RuleHelper::address('address', ['required']),
        ];
    }
}
