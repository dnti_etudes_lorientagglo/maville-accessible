<?php

namespace App\JsonApi\V1\Places;

use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Trait ValidatesPlaceQuery.
 *
 * @mixin ResourceQuery
 */
trait ValidatesPlaceQuery
{
    /**
     * Create the rules for this specific query.
     *
     * @return array
     */
    private function placeRules(): array
    {
        return [
            'location'           => [
                Rule::requiredIf(fn() => $this->hasLocationAwareSortInQuery()),
                'filled',
                'array:longitude,latitude,radius',
            ],
            'location.longitude' => ['nullable', 'numeric', 'between:-180,180'],
            'location.latitude'  => ['nullable', 'numeric', 'between:-90,90'],
            'location.radius'    => ['nullable', 'numeric', 'max:50000'],
        ];
    }

    /**
     * Check if the request query contains a location aware sort.
     *
     * @return bool
     */
    private function hasLocationAwareSortInQuery(): bool
    {
        $data = $this->validationData();
        $sort = Arr::get($data, 'sort');
        if (! is_string($sort)) {
            return false;
        }

        return in_array('proximity', explode(',', str_replace('-', '', $sort)))
            || in_array('distance', explode(',', str_replace('-', '', $sort)));
    }
}
