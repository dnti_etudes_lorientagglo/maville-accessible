<?php

namespace App\JsonApi\V1\Places;

use App\JsonApi\Concerns\Accessible\AccessibleSchema;
use App\JsonApi\Concerns\Categorizable\CategorizableSchema;
use App\JsonApi\Concerns\Contactable\ContactableSchema;
use App\JsonApi\Concerns\Core\BaseSchema;
use App\JsonApi\Concerns\Coverable\CoverableSchema;
use App\JsonApi\Concerns\Imageable\ImageableSchema;
use App\JsonApi\Concerns\Linkable\LinkableSchema;
use App\JsonApi\Concerns\Ownable\OwnableSchema;
use App\JsonApi\Concerns\Publishable\PublishableSchema;
use App\JsonApi\Concerns\Reviewable\ReviewableSchema;
use App\JsonApi\Concerns\Sluggable\SluggableSchema;
use App\JsonApi\Concerns\Sourceable\SourceableSchema;
use App\JsonApi\Fields\Translation;
use App\Models\Place;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Http\Request;
use LaravelJsonApi\Eloquent\Fields\ArrayHash;
use LaravelJsonApi\Eloquent\Fields\ArrayList;
use LaravelJsonApi\Eloquent\QueryBuilder\JsonApiBuilder;

/**
 * Class PlaceSchema.
 */
class PlaceSchema extends BaseSchema
{
    use AccessibleSchema;
    use CategorizableSchema;
    use ContactableSchema;
    use CoverableSchema;
    use ImageableSchema;
    use LinkableSchema;
    use OwnableSchema;
    use PublishableSchema;
    use ReviewableSchema;
    use SluggableSchema;
    use SourceableSchema;

    public static string $model = Place::class;

    protected int $maxDepth = 2;

    /**
     * {@inheritDoc}
     */
    public function newQuery($query = null): JsonApiBuilder
    {
        return parent::newQuery($this->reviewableQuery($query));
    }

    /**
     * {@inheritDoc}
     */
    public function indexQuery(?Request $request, EloquentBuilder $query): EloquentBuilder
    {
        $query = parent::indexQuery($request, $query);

        return $this->publishableQuery(
            $request,
            $query,
            fn() => $this->ownableQuery($query),
        );
    }

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            ...$this->accessibleFields(),
            ...$this->categorizableFields(),
            ...$this->contactableFields(),
            ...$this->coverableFields(),
            ...$this->imageableFields(),
            ...$this->linkableFields(),
            ...$this->publishableFields(),
            ...$this->ownableFields(),
            ...$this->reviewableFields(),
            ...$this->sluggableFields(),
            ...$this->sourceableFields(),
            Translation::make('name')
                ->sortable(),
            Translation::make('description'),
            ArrayList::make('openingHours'),
            ArrayHash::make('address'),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function filters(): array
    {
        return [
            ...parent::filters(),
            ...$this->accessibleFilters(),
            ...$this->categorizableFilters(),
            ...$this->publishableFilters(),
            ...$this->sluggableFilters(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function sortables(): iterable
    {
        return [
            ...parent::sortables(),
            ...$this->accessibleSortables(),
            ...$this->ownableSortables(),
            DistanceSort::make(),
            ProximitySort::make(),
        ];
    }
}
