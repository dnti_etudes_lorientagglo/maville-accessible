<?php

namespace App\JsonApi\V1\Places;

use App\Helpers\QueryHelper;
use Illuminate\Contracts\Database\Eloquent\Builder;
use LaravelJsonApi\Eloquent\Contracts\Filter;
use LaravelJsonApi\Eloquent\Filters\Concerns\IsSingular;

/**
 * Class LocationFilter.
 */
class LocationFilter implements Filter
{
    use IsSingular;
    use UsesLocationFilter;

    /**
     * Create a new filter.
     *
     * @param string $name
     *
     * @return static
     */
    public static function make(string $name = 'location'): static
    {
        return new static($name);
    }

    /**
     * LocationFilter constructor.
     *
     * @param string $name
     */
    public function __construct(private readonly string $name)
    {
    }

    /**
     * Get the key for the filter.
     *
     * @return string
     */
    public function key(): string
    {
        return $this->name;
    }

    /**
     * Apply the filter to the query.
     *
     * @param Builder $query
     * @param mixed   $value
     *
     * @return Builder
     */
    public function apply($query, $value): Builder
    {
        if ($value) {
            [$expression, $bindings] = $this->locationDistanceExpression(request());
            $radius = $this->locationRadius(request());

            return QueryHelper::clarifySelects($query)
                ->selectRaw("$expression as distance", $bindings)
                ->whereRaw("$expression <= ?", [...$bindings, $radius]);
        }

        return $query;
    }
}
