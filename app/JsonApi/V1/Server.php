<?php

namespace App\JsonApi\V1;

use App\JsonApi\V1\Activities\ActivitySchema;
use App\JsonApi\V1\ActivityTypes\ActivityTypeSchema;
use App\JsonApi\V1\Articles\ArticleSchema;
use App\JsonApi\V1\Categories\CategorySchema;
use App\JsonApi\V1\Configs\ConfigSchema;
use App\JsonApi\V1\Media\MediaSchema;
use App\JsonApi\V1\Notifications\NotificationSchema;
use App\JsonApi\V1\OrganizationInvitations\OrganizationInvitationSchema;
use App\JsonApi\V1\OrganizationMembers\OrganizationMemberSchema;
use App\JsonApi\V1\Organizations\OrganizationSchema;
use App\JsonApi\V1\OrganizationTypes\OrganizationTypeSchema;
use App\JsonApi\V1\Permissions\PermissionSchema;
use App\JsonApi\V1\Places\PlaceSchema;
use App\JsonApi\V1\Reactions\ReactionSchema;
use App\JsonApi\V1\Reports\ReportSchema;
use App\JsonApi\V1\ReportTypes\ReportTypeSchema;
use App\JsonApi\V1\Requests\RequestSchema;
use App\JsonApi\V1\RequestTypes\RequestTypeSchema;
use App\JsonApi\V1\ReviewReplies\ReviewReplySchema;
use App\JsonApi\V1\Reviews\ReviewSchema;
use App\JsonApi\V1\Roles\RoleSchema;
use App\JsonApi\V1\SendingCampaignBatches\SendingCampaignBatchSchema;
use App\JsonApi\V1\SendingCampaigns\SendingCampaignSchema;
use App\JsonApi\V1\SendingCampaignTypes\SendingCampaignTypeSchema;
use App\JsonApi\V1\Sources\SourceSchema;
use App\JsonApi\V1\Users\UserSchema;
use LaravelJsonApi\Core\Server\Server as BaseServer;

/**
 * Class Server.
 */
class Server extends BaseServer
{
    /**
     * The base URI namespace for this server.
     *
     * @var string
     */
    protected string $baseUri = '/api/v1';

    /**
     * Bootstrap the server when it is handling an HTTP request.
     *
     * @return void
     */
    public function serving(): void
    {
    }

    /**
     * Get the server's list of schemas.
     *
     * @return array
     */
    protected function allSchemas(): array
    {
        return [
            ConfigSchema::class,
            UserSchema::class,
            RoleSchema::class,
            PermissionSchema::class,
            SourceSchema::class,
            CategorySchema::class,
            MediaSchema::class,
            NotificationSchema::class,
            ReportTypeSchema::class,
            ReportSchema::class,
            RequestTypeSchema::class,
            RequestSchema::class,
            ReviewSchema::class,
            ReviewReplySchema::class,
            ReactionSchema::class,
            SendingCampaignTypeSchema::class,
            SendingCampaignBatchSchema::class,
            SendingCampaignSchema::class,
            OrganizationTypeSchema::class,
            OrganizationSchema::class,
            OrganizationMemberSchema::class,
            OrganizationInvitationSchema::class,
            PlaceSchema::class,
            ArticleSchema::class,
            ActivityTypeSchema::class,
            ActivitySchema::class,
        ];
    }
}
