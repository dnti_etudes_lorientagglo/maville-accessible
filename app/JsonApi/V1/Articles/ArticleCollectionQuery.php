<?php

namespace App\JsonApi\V1\Articles;

use App\JsonApi\Concerns\Categorizable\ValidatesCategorizableQuery;
use App\JsonApi\Concerns\Core\ValidatesCollectionQuery;
use App\JsonApi\Concerns\Core\ValidatesPaginableQuery;
use App\JsonApi\Concerns\Localizable\ValidatesLocalizableQuery;
use App\JsonApi\Concerns\Publishable\ValidatesPublishableQuery;
use App\JsonApi\Concerns\Searchable\ValidatesSearchableQuery;
use App\JsonApi\Concerns\Sluggable\ValidatesSluggableQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class ArticleCollectionQuery.
 */
class ArticleCollectionQuery extends ResourceQuery
{
    use ValidatesCollectionQuery;
    use ValidatesCategorizableQuery;
    use ValidatesLocalizableQuery;
    use ValidatesPaginableQuery;
    use ValidatesSearchableQuery;
    use ValidatesPublishableQuery;
    use ValidatesSluggableQuery;
    use ValidatesArticleQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
            ...$this->pageRules(),
            ...$this->searchableRules(),
            ...$this->categorizableRules(),
            ...$this->localizableRules(),
            ...$this->publishableRules(),
            ...$this->sluggableRules(),
            ...$this->articleRules(),
        ];
    }
}
