<?php

namespace App\JsonApi\V1\Articles;

use App\JsonApi\Concerns\Categorizable\CategorizableSchema;
use App\JsonApi\Concerns\Core\BaseSchema;
use App\JsonApi\Concerns\Coverable\CoverableSchema;
use App\JsonApi\Concerns\Imageable\ImageableSchema;
use App\JsonApi\Concerns\Localizable\LocalizableSchema;
use App\JsonApi\Concerns\Mediable\MediableSchema;
use App\JsonApi\Concerns\Ownable\OwnableSchema;
use App\JsonApi\Concerns\Pinnable\PinnableSchema;
use App\JsonApi\Concerns\Publishable\PublishableSchema;
use App\JsonApi\Concerns\Reviewable\ReviewableSchema;
use App\JsonApi\Concerns\Sluggable\SluggableSchema;
use App\JsonApi\Concerns\Sourceable\SourceableSchema;
use App\JsonApi\Fields\Translation;
use App\Models\Article;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Http\Request;

/**
 * Class ArticleSchema.
 */
class ArticleSchema extends BaseSchema
{
    use CategorizableSchema;
    use CoverableSchema;
    use ImageableSchema;
    use LocalizableSchema;
    use MediableSchema;
    use OwnableSchema;
    use PinnableSchema;
    use PublishableSchema;
    use ReviewableSchema;
    use SluggableSchema;
    use SourceableSchema;

    public static string $model = Article::class;

    protected int $maxDepth = 3;

    /**
     * {@inheritDoc}
     */
    public function indexQuery(?Request $request, EloquentBuilder $query): EloquentBuilder
    {
        $query = parent::indexQuery($request, $query);

        return $this->publishableQuery(
            $request,
            $query,
            fn() => $this->ownableQuery($query),
        );
    }

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            ...$this->categorizableFields(),
            ...$this->coverableFields(),
            ...$this->imageableFields(),
            ...$this->localizableFields(),
            ...$this->mediableFields(),
            ...$this->pinnableFields(),
            ...$this->publishableFields(),
            ...$this->ownableFields(),
            ...$this->sluggableFields(),
            ...$this->sourceableFields(),
            Translation::make('name')
                ->sortable(),
            Translation::make('description'),
            Translation::make('body'),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function filters(): array
    {
        return [
            ...parent::filters(),
            ...$this->categorizableFilters(),
            ...$this->localizableFitlers(),
            ...$this->publishableFilters(),
            ...$this->sluggableFilters(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function sortables(): iterable
    {
        return [
            ...parent::sortables(),
            ...$this->ownableSortables(),
            ...$this->pinnableSortables(),
        ];
    }
}
