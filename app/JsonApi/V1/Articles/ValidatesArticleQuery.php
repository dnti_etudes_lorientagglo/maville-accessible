<?php

namespace App\JsonApi\V1\Articles;

use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Trait ValidatesArticleQuery.
 *
 * @mixin ResourceQuery
 */
trait ValidatesArticleQuery
{
    /**
     * Create the rules for this specific query.
     *
     * @return array
     */
    private function articleRules(): array
    {
        return [];
    }
}
