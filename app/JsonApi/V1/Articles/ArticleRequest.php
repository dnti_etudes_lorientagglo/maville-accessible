<?php

namespace App\JsonApi\V1\Articles;

use App\JsonApi\Concerns\Categorizable\ValidatesCategorizableRequest;
use App\JsonApi\Concerns\Coverable\ValidatesCoverableRequest;
use App\JsonApi\Concerns\Imageable\ValidatesImageableRequest;
use App\JsonApi\Concerns\Localizable\ValidatesLocalizableRequest;
use App\JsonApi\Concerns\Mediable\ValidatesMediableRequest;
use App\JsonApi\Concerns\Ownable\ValidatesOwnableRequest;
use App\Rules\TranslationRule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;

/**
 * Class ArticleRequest.
 */
class ArticleRequest extends ResourceRequest
{
    use ValidatesCategorizableRequest;
    use ValidatesCoverableRequest;
    use ValidatesImageableRequest;
    use ValidatesLocalizableRequest;
    use ValidatesMediableRequest;
    use ValidatesOwnableRequest;

    public function rules(): array
    {
        return [
            ...$this->categorizableRules(required: false),
            ...$this->coverableRules(),
            ...$this->imageableRules(),
            ...$this->localizableRules(),
            ...$this->mediableRules(),
            ...$this->ownableRules(),
            'name'        => [TranslationRule::make(['required', 'max:100'])],
            'description' => [TranslationRule::make(['nullable', 'max:255'])],
            'body'        => [TranslationRule::make(['required', 'max:50000'])],
        ];
    }
}
