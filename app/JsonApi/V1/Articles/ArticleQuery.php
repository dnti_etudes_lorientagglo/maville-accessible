<?php

namespace App\JsonApi\V1\Articles;

use App\JsonApi\Concerns\Categorizable\ValidatesCategorizableQuery;
use App\JsonApi\Concerns\Core\ValidatesOneQuery;
use App\JsonApi\Concerns\Localizable\ValidatesLocalizableQuery;
use App\JsonApi\Concerns\Publishable\ValidatesPublishableQuery;
use App\JsonApi\Concerns\Sluggable\ValidatesSluggableQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class ArticleQuery.
 */
class ArticleQuery extends ResourceQuery
{
    use ValidatesOneQuery;
    use ValidatesCategorizableQuery;
    use ValidatesLocalizableQuery;
    use ValidatesPublishableQuery;
    use ValidatesSluggableQuery;
    use ValidatesArticleQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
            ...$this->categorizableRules(),
            ...$this->localizableRules(),
            ...$this->publishableRules(),
            ...$this->sluggableRules(),
            ...$this->articleRules(),
        ];
    }
}
