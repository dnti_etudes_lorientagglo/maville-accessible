<?php

namespace App\JsonApi\V1\OrganizationTypes;

use App\JsonApi\Concerns\Core\BaseSchema;
use App\JsonApi\Fields\Translation;
use App\Models\OrganizationType;
use LaravelJsonApi\Eloquent\Fields\Str;

/**
 * Class OrganizationTypeSchema.
 */
class OrganizationTypeSchema extends BaseSchema
{
    public static string $model = OrganizationType::class;

    protected int $maxDepth = 0;

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            Str::make('code')
                ->readOnly()
                ->sortable(),
            Translation::make('name')
                ->readOnly()
                ->sortable(),
        ];
    }
}
