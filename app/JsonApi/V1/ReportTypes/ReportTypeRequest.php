<?php

namespace App\JsonApi\V1\ReportTypes;

use App\Rules\TranslationRule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;

/**
 * Class ReportTypeRequest.
 */
class ReportTypeRequest extends ResourceRequest
{
    public function rules(): array
    {
        return [
            'name'   => [TranslationRule::make(['required', 'max:50'])],
            'global' => ['required', 'boolean'],
        ];
    }
}
