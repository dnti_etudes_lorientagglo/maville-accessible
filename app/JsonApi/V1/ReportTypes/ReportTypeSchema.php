<?php

namespace App\JsonApi\V1\ReportTypes;

use App\JsonApi\Concerns\Core\BaseSchema;
use App\JsonApi\Fields\Translation;
use App\Models\ReportType;
use LaravelJsonApi\Eloquent\Fields\Boolean;
use LaravelJsonApi\Eloquent\Fields\Str;

/**
 * Class ReportTypeSchema.
 */
class ReportTypeSchema extends BaseSchema
{
    public static string $model = ReportType::class;

    protected int $maxDepth = 0;

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            Str::make('code')
                ->readOnly(),
            Translation::make('name')
                ->sortable(),
            Boolean::make('global'),
        ];
    }
}
