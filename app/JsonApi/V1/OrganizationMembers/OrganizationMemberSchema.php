<?php

namespace App\JsonApi\V1\OrganizationMembers;

use App\Helpers\JsonApiHelper;
use App\JsonApi\Concerns\Core\BaseSchema;
use App\Models\OrganizationMember;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsTo;

/**
 * Class OrganizationMemberSchema.
 */
class OrganizationMemberSchema extends BaseSchema
{
    public static string $model = OrganizationMember::class;

    protected int $maxDepth = 2;

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            BelongsTo::make('user')
                ->readOnlyOnUpdate()
                ->serializeUsing(JsonApiHelper::noLinks()),
            BelongsTo::make('organization')
                ->serializeUsing(JsonApiHelper::noLinks()),
            BelongsTo::make('role')
                ->serializeUsing(JsonApiHelper::noLinks()),
        ];
    }
}
