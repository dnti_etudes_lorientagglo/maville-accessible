<?php

namespace App\JsonApi\V1\OrganizationMembers;

use App\JsonApi\Concerns\Core\ValidatesCollectionQuery;
use App\JsonApi\Concerns\Core\ValidatesPaginableQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class OrganizationMemberCollectionQuery.
 */
class OrganizationMemberCollectionQuery extends ResourceQuery
{
    use ValidatesCollectionQuery;
    use ValidatesPaginableQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
            ...$this->pageRules(),
        ];
    }
}
