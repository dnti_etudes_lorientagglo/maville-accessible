<?php

namespace App\JsonApi\V1\OrganizationMembers;

use App\Helpers\AuthHelper;
use App\Models\Enums\RoleScopeName;
use App\Models\Organization;
use App\Models\Role;
use App\Policies\OrganizationPolicy;
use Illuminate\Validation\Rule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Class OrganizationMemberRequest.
 */
class OrganizationMemberRequest extends ResourceRequest
{
    public function rules(): array
    {
        $user = AuthHelper::user();

        $organizationRules = ['organization' => ['required', JsonApiRule::toOne()]];
        if (! $user->can(OrganizationPolicy::ADMIN, Organization::class)) {
            $organizationRules['organization.id'] = ['required', Rule::in([$user->getActingForOrganization()->id])];
        }

        return [
            ...$organizationRules,
            'user'    => ['required', JsonApiRule::toOne()],
            'role'    => ['required', JsonApiRule::toOne()],
            'role.id' => [
                'required',
                Rule::in(
                    Role::query()
                        ->where('scope_name', RoleScopeName::ORGANIZATION->value)
                        ->pluck('id'),
                ),
            ],
        ];
    }
}
