<?php

namespace App\JsonApi\V1\Permissions;

use App\JsonApi\Concerns\Core\ValidatesOneQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class PermissionQuery.
 */
class PermissionQuery extends ResourceQuery
{
    use ValidatesOneQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
        ];
    }
}
