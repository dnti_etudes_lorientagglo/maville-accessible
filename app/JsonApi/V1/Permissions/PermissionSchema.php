<?php

namespace App\JsonApi\V1\Permissions;

use App\JsonApi\Concerns\Core\BaseSchema;
use App\JsonApi\Fields\Translation;
use App\Models\Permission;
use LaravelJsonApi\Eloquent\Fields\Str;

/**
 * Class PermissionSchema.
 */
class PermissionSchema extends BaseSchema
{
    public static string $model = Permission::class;

    protected int $maxDepth = 0;

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            Str::make('code', 'name')
                ->readOnly(),
            Translation::make('name', 'translated_name')
                ->readOnly()
                ->sortable(),
        ];
    }
}
