<?php

namespace App\JsonApi\V1\Media;

use App\Helpers\AuthHelper;
use App\JsonApi\Concerns\Core\ValidatesCollectionQuery;
use App\JsonApi\Concerns\Core\ValidatesPaginableQuery;
use App\JsonApi\Concerns\Searchable\ValidatesSearchableQuery;
use App\Models\Enums\FileVisibility;
use Illuminate\Validation\Rule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class MediaCollectionQuery.
 */
class MediaCollectionQuery extends ResourceQuery
{
    use ValidatesCollectionQuery;
    use ValidatesPaginableQuery;
    use ValidatesSearchableQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
            ...$this->cursorPageRules(),
            ...$this->searchableRules(),
            'filter.owner'      => [
                'required',
                'array',
            ],
            'filter.owner.*'    => [
                'required',
                // TODO Allow organization members in it.
                Rule::in([AuthHelper::user()?->id]),
            ],
            'filter.type'       => [
                'filled',
                'array',
            ],
            'filter.type.*'     => [
                'required',
                Rule::in(MediaTypeFilter::ALLOWED_VALUES),
            ],
            'filter.visibility' => [
                'filled',
                Rule::in(FileVisibility::cases()),
            ],
        ];
    }
}
