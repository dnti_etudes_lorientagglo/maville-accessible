<?php

namespace App\JsonApi\V1\Media;

use App\JsonApi\Concerns\Core\ValidatesOneQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;
use LaravelJsonApi\Validation\Rules\AllowedFilterParameters;

/**
 * Class MediaQuery.
 */
class MediaQuery extends ResourceQuery
{
    use ValidatesOneQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(
                filter: static fn(AllowedFilterParameters $filter) => $filter->forget('owner', 'type', 'visibility'),
            ),
        ];
    }
}
