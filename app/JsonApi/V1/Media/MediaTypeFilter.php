<?php

namespace App\JsonApi\V1\Media;

use Illuminate\Contracts\Database\Eloquent\Builder;
use LaravelJsonApi\Eloquent\Contracts\Filter;
use LaravelJsonApi\Eloquent\Filters\Concerns\IsSingular;

/**
 * Class MediaTypeFilter.
 */
class MediaTypeFilter implements Filter
{
    use IsSingular;

    /**
     * The allowed values.
     */
    public const ALLOWED_VALUES = [
        'image',
        'video',
    ];

    /**
     * @var string
     */
    private string $name;

    /**
     * Create a new filter.
     *
     * @param string $name
     *
     * @return MediaTypeFilter
     */
    public static function make(string $name = 'type'): self
    {
        return new static($name);
    }

    /**
     * MediaTypeFilter constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * Get the key for the filter.
     *
     * @return string
     */
    public function key(): string
    {
        return $this->name;
    }

    /**
     * Apply the filter to the query.
     *
     * @param Builder $query
     * @param mixed   $value
     *
     * @return Builder
     */
    public function apply($query, $value): Builder
    {
        return $query->where(static function (Builder $query) use ($value) {
            collect($value)->each(static function (string $type) use ($query) {
                match ($type) {
                    'image' => $query->where('mime_type', 'like', 'image/%'),
                    'video' => $query->where('mime_type', 'like', 'video/%'),
                    default => $query,
                };
            });
        });
    }
}
