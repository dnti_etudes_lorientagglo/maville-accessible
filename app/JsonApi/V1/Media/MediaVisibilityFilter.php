<?php

namespace App\JsonApi\V1\Media;

use App\Models\Enums\FileVisibility;
use Illuminate\Contracts\Database\Eloquent\Builder;
use LaravelJsonApi\Eloquent\Contracts\Filter;
use LaravelJsonApi\Eloquent\Filters\Concerns\IsSingular;

/**
 * Class MediaVisibilityFilter.
 */
class MediaVisibilityFilter implements Filter
{
    use IsSingular;

    /**
     * @var string
     */
    private string $name;

    /**
     * Create a new filter.
     *
     * @param string $name
     *
     * @return MediaVisibilityFilter
     */
    public static function make(string $name = 'visibility'): self
    {
        return new static($name);
    }

    /**
     * MediaVisibilityFilter constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * Get the key for the filter.
     *
     * @return string
     */
    public function key(): string
    {
        return $this->name;
    }

    /**
     * Apply the filter to the query.
     *
     * @param Builder $query
     * @param mixed   $value
     *
     * @return Builder
     */
    public function apply($query, $value): Builder
    {
        return $query->where(static function (Builder $query) use ($value) {
            match ($value) {
                FileVisibility::PUBLIC->value => $query->whereNull('custom_properties->visibility')
                    ->orWhere('custom_properties->visibility', FileVisibility::PUBLIC->value),
                FileVisibility::PRIVATE->value => $query->whereNotNull('custom_properties->visibility')
                    ->where('custom_properties->visibility', FileVisibility::PRIVATE->value),
                default => $query,
            };
        });
    }
}
