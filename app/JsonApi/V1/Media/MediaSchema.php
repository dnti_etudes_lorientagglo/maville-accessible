<?php

namespace App\JsonApi\V1\Media;

use App\Helpers\JsonApiHelper;
use App\JsonApi\Concerns\Core\BaseSchema;
use App\Models\Media;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use LaravelJsonApi\CursorPagination\CursorPagination;
use LaravelJsonApi\Eloquent\Contracts\Paginator;
use LaravelJsonApi\Eloquent\Fields\Number;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsTo;
use LaravelJsonApi\Eloquent\Fields\Str;
use LaravelJsonApi\Eloquent\Filters\WhereIn;

/**
 * Class MediaSchema.
 */
class MediaSchema extends BaseSchema
{
    public static string $model = Media::class;

    protected int $maxDepth = 1;

    /**
     * {@inheritDoc}
     */
    public function indexQuery(?Request $request, Builder $query): Builder
    {
        return parent::indexQuery($request, $query)->whereNotNull('size');
    }

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            Str::make('visibility')
                ->readOnly(),
            Str::make('name'),
            Str::make('fileName')
                ->readOnly(),
            Str::make('size')
                ->readOnly(),
            Number::make('width')
                ->readOnly(),
            Number::make('height')
                ->readOnly(),
            Str::make('mimeType')
                ->readOnly(),
            Str::make('redirectURL')
                ->readOnly(),
            Str::make('resolveURL')
                ->readOnly(),
            Str::make('originalURL')
                ->readOnly(),
            Str::make('placeholderURL')
                ->readOnly(),
            Str::make('optimizedURL')
                ->readOnly(),
            Str::make('optimizedSrcset')
                ->readOnly(),
            BelongsTo::make('owner', 'model')
                ->type('users')
                ->readOnly()
                ->serializeUsing(JsonApiHelper::noLinks()),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function filters(): array
    {
        return [
            ...parent::filters(),
            MediaTypeFilter::make(),
            MediaVisibilityFilter::make(),
            WhereIn::make('owner', 'model_id'),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function pagination(): ?Paginator
    {
        return CursorPagination::make($this->id());
    }
}
