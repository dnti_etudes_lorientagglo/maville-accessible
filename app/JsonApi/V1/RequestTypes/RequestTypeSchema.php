<?php

namespace App\JsonApi\V1\RequestTypes;

use App\JsonApi\Concerns\Core\BaseSchema;
use App\JsonApi\Fields\Translation;
use App\Models\RequestType;
use LaravelJsonApi\Eloquent\Fields\Str;

/**
 * Class RequestTypeSchema.
 */
class RequestTypeSchema extends BaseSchema
{
    public static string $model = RequestType::class;

    protected int $maxDepth = 0;

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            Str::make('code')
                ->readOnly(),
            Translation::make('name')
                ->sortable(),
        ];
    }
}
