<?php

namespace App\JsonApi\V1\SendingCampaignTypes;

use App\JsonApi\Concerns\Core\ValidatesOneQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class SendingCampaignTypeQuery.
 */
class SendingCampaignTypeQuery extends ResourceQuery
{
    use ValidatesOneQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
        ];
    }
}
