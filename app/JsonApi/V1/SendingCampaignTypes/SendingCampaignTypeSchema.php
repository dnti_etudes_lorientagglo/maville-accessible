<?php

namespace App\JsonApi\V1\SendingCampaignTypes;

use App\JsonApi\Concerns\Core\BaseSchema;
use App\JsonApi\Fields\Translation;
use App\Models\SendingCampaignType;
use LaravelJsonApi\Eloquent\Fields\Str;

/**
 * Class SendingCampaignTypeSchema.
 */
class SendingCampaignTypeSchema extends BaseSchema
{
    public static string $model = SendingCampaignType::class;

    protected int $maxDepth = 0;

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            Str::make('code')
                ->readOnly(),
            Translation::make('name')
                ->readOnly()
                ->sortable(),
        ];
    }
}
