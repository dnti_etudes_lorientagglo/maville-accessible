<?php

namespace App\JsonApi\V1\Reactions;

use App\Helpers\AuthHelper;
use App\Models\Enums\ReactionType;
use App\Models\Reaction;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Unique;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Class ReactionRequest.
 */
class ReactionRequest extends ResourceRequest
{
    public function rules(): array
    {
        /** @var User|null $user */
        $user = AuthHelper::user();
        $data = $this->validationData();
        $type = Arr::get($data, 'reactable.type');

        return [
            'reaction'       => [
                'required',
                'string',
                Rule::in(collect(ReactionType::cases())->map(static fn(ReactionType $t) => $t->value)),
            ],
            'reactable'      => ['required', JsonApiRule::toOne()],
            'reactable.type' => ['required', Rule::in(['reviews'])],
            'reactable.id'   => [
                'required',
                Rule::unique((new Reaction())->getTable(), 'reactable_id')
                    ->when(is_string($type), static fn(Unique $query) => $query->where('reactable_type', $type))
                    ->where('owner_user_id', $user?->id),
            ],
        ];
    }
}
