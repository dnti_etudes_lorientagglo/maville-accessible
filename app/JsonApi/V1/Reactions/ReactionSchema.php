<?php

namespace App\JsonApi\V1\Reactions;

use App\JsonApi\Concerns\Core\BaseSchema;
use App\JsonApi\Concerns\UserOwnable\UserOwnableSchema;
use App\Models\Reaction;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsTo;
use LaravelJsonApi\Eloquent\Fields\Str;

/**
 * Class ReactionSchema.
 */
class ReactionSchema extends BaseSchema
{
    use UserOwnableSchema;

    public static string $model = Reaction::class;

    protected int $maxDepth = 1;

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            ...$this->userOwnableFields(),
            Str::make('reaction'),
            // TODO Replace with MorphTo if multiple reactable types.
            BelongsTo::make('reactable')
                ->type('reviews')
                ->hidden()
                ->cannotEagerLoad(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function filters(): array
    {
        return [
            ...parent::filters(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function sortables(): iterable
    {
        return [
            ...parent::sortables(),
            ...$this->userOwnableSortables(),
        ];
    }
}
