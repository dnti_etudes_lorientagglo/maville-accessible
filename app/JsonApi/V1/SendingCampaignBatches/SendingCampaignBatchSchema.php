<?php

namespace App\JsonApi\V1\SendingCampaignBatches;

use App\JsonApi\Concerns\Core\BaseSchema;
use App\Models\SendingCampaignBatch;
use LaravelJsonApi\Eloquent\Fields\ArrayHash;
use LaravelJsonApi\Eloquent\Fields\DateTime;
use LaravelJsonApi\Eloquent\Fields\Number;

/**
 * Class SendingCampaignBatchSchema.
 */
class SendingCampaignBatchSchema extends BaseSchema
{
    public static string $model = SendingCampaignBatch::class;

    protected int $maxDepth = 0;

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            Number::make('attempts')
                ->readOnly(),
            ArrayHash::make('recipientsCounts')
                ->readOnly(),
            DateTime::make('startedAt')
                ->readOnly()
                ->retainTimezone()
                ->sortable(),
            DateTime::make('runningAt')
                ->readOnly()
                ->retainTimezone()
                ->sortable(),
            DateTime::make('finishedAt')
                ->readOnly()
                ->retainTimezone()
                ->sortable(),
            DateTime::make('failedAt')
                ->readOnly()
                ->retainTimezone()
                ->sortable(),
        ];
    }
}
