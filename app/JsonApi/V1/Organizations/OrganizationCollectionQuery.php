<?php

namespace App\JsonApi\V1\Organizations;

use App\JsonApi\Concerns\Categorizable\ValidatesCategorizableQuery;
use App\JsonApi\Concerns\Core\ValidatesCollectionQuery;
use App\JsonApi\Concerns\Core\ValidatesPaginableQuery;
use App\JsonApi\Concerns\Publishable\ValidatesPublishableQuery;
use App\JsonApi\Concerns\Searchable\ValidatesSearchableQuery;
use App\JsonApi\Concerns\Sluggable\ValidatesSluggableQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class OrganizationCollectionQuery.
 */
class OrganizationCollectionQuery extends ResourceQuery
{
    use ValidatesCollectionQuery;
    use ValidatesCategorizableQuery;
    use ValidatesPaginableQuery;
    use ValidatesSearchableQuery;
    use ValidatesPublishableQuery;
    use ValidatesSluggableQuery;
    use ValidatesOrganizationQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
            ...$this->pageRules(),
            ...$this->searchableRules(),
            ...$this->categorizableRules(),
            ...$this->publishableRules(),
            ...$this->sluggableRules(),
            ...$this->organizationFilterRules(),
        ];
    }
}
