<?php

namespace App\JsonApi\V1\Organizations;

trait ValidatesOrganizationQuery
{
    /**
     * Get the specific filter rules.
     *
     * @return array[]
     */
    public function organizationFilterRules(): array
    {
        return [
            'filter.searchName' => ['filled', 'string'],
        ];
    }
}
