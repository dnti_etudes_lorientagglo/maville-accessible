<?php

namespace App\JsonApi\V1\Organizations;

use App\Helpers\AuthHelper;
use App\Helpers\JsonApiHelper;
use App\Helpers\LocaleHelper;
use App\JsonApi\Concerns\Categorizable\CategorizableSchema;
use App\JsonApi\Concerns\Contactable\ContactableSchema;
use App\JsonApi\Concerns\Core\BaseSchema;
use App\JsonApi\Concerns\Coverable\CoverableSchema;
use App\JsonApi\Concerns\Linkable\LinkableSchema;
use App\JsonApi\Concerns\Mediable\MediableSchema;
use App\JsonApi\Concerns\Publishable\PublishableSchema;
use App\JsonApi\Concerns\Sluggable\SluggableSchema;
use App\JsonApi\Concerns\Sourceable\SourceableSchema;
use App\JsonApi\Fields\Translation;
use App\JsonApi\Filters\ClosureFilter;
use App\JsonApi\Filters\SearchNameFilter;
use App\Models\Organization;
use App\Policies\OrganizationPolicy;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str as StrHelper;
use LaravelJsonApi\Eloquent\Fields\ArrayHash;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsTo;
use LaravelJsonApi\Eloquent\Fields\Relations\HasMany;

/**
 * Class OrganizationSchema.
 */
class OrganizationSchema extends BaseSchema
{
    use CategorizableSchema;
    use ContactableSchema;
    use CoverableSchema;
    use LinkableSchema;
    use MediableSchema;
    use PublishableSchema;
    use SluggableSchema;
    use SourceableSchema;

    public static string $model = Organization::class;

    protected int $maxDepth = 2;

    /**
     * {@inheritDoc}
     */
    public function indexQuery(?Request $request, EloquentBuilder $query): EloquentBuilder
    {
        $query = parent::indexQuery($request, $query);

        return $this->publishableQuery(
            $request,
            $query,
            static fn() => AuthHelper::user()->can(OrganizationPolicy::ADMIN, Organization::class),
        );
    }

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            ...$this->categorizableFields(),
            ...$this->contactableFields(),
            ...$this->coverableFields(),
            ...$this->linkableFields(),
            ...$this->mediableFields(),
            ...$this->publishableFields(),
            ...$this->sluggableFields(),
            ...$this->sourceableFields(),
            Translation::make('name')
                ->sortable(),
            Translation::make('description'),
            Translation::make('body'),
            ArrayHash::make('administrativeData')
                ->camelizeFields()
                ->snakeKeys(),
            BelongsTo::make('organizationType')
                ->serializeUsing(JsonApiHelper::noLinks()),
            HasMany::make('membersOfOrganization')
                ->readOnly()
                ->type('organization-members')
                ->serializeUsing(JsonApiHelper::noLinks()),
            HasMany::make('invitations')
                ->readOnly()
                ->type('organization-invitations')
                ->serializeUsing(JsonApiHelper::noLinks()),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function filters(): array
    {
        return [
            ...parent::filters(),
            ...$this->categorizableFilters(),
            ...$this->publishableFilters(),
            ...$this->sluggableFilters(),
            SearchNameFilter::make(),
        ];
    }
}
