<?php

namespace App\JsonApi\V1\Organizations;

use App\JsonApi\Concerns\Categorizable\ValidatesCategorizableQuery;
use App\JsonApi\Concerns\Core\ValidatesOneQuery;
use App\JsonApi\Concerns\Publishable\ValidatesPublishableQuery;
use App\JsonApi\Concerns\Sluggable\ValidatesSluggableQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class OrganizationQuery.
 */
class OrganizationQuery extends ResourceQuery
{
    use ValidatesOneQuery;
    use ValidatesCategorizableQuery;
    use ValidatesPublishableQuery;
    use ValidatesSluggableQuery;
    use ValidatesOrganizationQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
            ...$this->categorizableRules(),
            ...$this->publishableRules(),
            ...$this->sluggableRules(),
            ...$this->organizationFilterRules(),
        ];
    }
}
