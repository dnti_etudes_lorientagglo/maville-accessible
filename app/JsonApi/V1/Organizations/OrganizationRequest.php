<?php

namespace App\JsonApi\V1\Organizations;

use App\Helpers\RuleHelper;
use App\JsonApi\Concerns\Categorizable\ValidatesCategorizableRequest;
use App\JsonApi\Concerns\Contactable\ValidatesContactableRequest;
use App\JsonApi\Concerns\Coverable\ValidatesCoverableRequest;
use App\JsonApi\Concerns\Linkable\ValidatesLinkableRequest;
use App\JsonApi\Concerns\Mediable\ValidatesMediableRequest;
use App\Rules\TranslationRule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

/**
 * Class OrganizationRequest.
 */
class OrganizationRequest extends ResourceRequest
{
    use ValidatesCategorizableRequest;
    use ValidatesContactableRequest;
    use ValidatesCoverableRequest;
    use ValidatesLinkableRequest;
    use ValidatesMediableRequest;

    public function rules(): array
    {
        return [
            ...$this->categorizableRules(),
            ...$this->contactableRules(),
            ...$this->coverableRules(required: false),
            ...$this->linkableRules(),
            ...$this->mediableRules(),
            'name'             => [TranslationRule::make(['required', 'max:100'])],
            'description'      => [TranslationRule::make(['nullable', 'max:255'])],
            'body'             => [TranslationRule::make(['nullable', 'max:50000'])],
            ...RuleHelper::arrayHash('administrativeData', [
                'siretIdentifier' => ['nullable', 'string', 'max:255'],
                'rnaIdentifier'   => ['nullable', 'string', 'max:255'],
            ], ['nullable']),
            'organizationType' => ['required', JsonApiRule::toOne()],
        ];
    }
}
