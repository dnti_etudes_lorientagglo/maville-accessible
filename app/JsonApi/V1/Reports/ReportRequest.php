<?php

namespace App\JsonApi\V1\Reports;

use App\Helpers\AuthHelper;
use App\JsonApi\Concerns\SoftDeletable\ValidatesSoftDeletableRequest;
use App\Models\Report;
use App\Services\Validation\Validator;
use Illuminate\Http\Response;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class ReportRequest.
 */
class ReportRequest extends ResourceRequest
{
    use ValidatesSoftDeletableRequest;

    public function rules(): array
    {
        return [
            ...$this->softDeletableRules(),
            'url'        => ['required', 'string', 'url'],
            'body'       => ['nullable', 'string', 'max:500'],
            'reportType' => ['required', JsonApiRule::toOne()],
            'reportable' => ['nullable', JsonApiRule::toOne()],
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param Validator $validator
     *
     * @return void
     */
    public function withValidator(Validator $validator): void
    {
        if ($this->isCreating()) {
            $validator->after(function () {
                $message = $this->getExceedingReportsError();
                if ($message) {
                    throw new HttpException(Response::HTTP_CONFLICT, $message);
                }
            });
        }
    }

    /**
     * Get a potential reports limits exceed error message.
     *
     * @return string|null
     */
    private function getExceedingReportsError(): string|null
    {
        $data = $this->validationData();
        $user = AuthHelper::user();
        $query = Report::query()
            ->whereNotNull('owner_user_id')
            ->where('owner_user_id', $user->id);

        if (! isset($data['reportable'])) {
            return $query->count() > 25
                ? __('validation.custom.reports.exceeds')
                : null;
        }

        return $query
            ->where('reportable_type', $data['reportable']['type'])
            ->where('reportable_id', $data['reportable']['id'])
            ->exists()
            ? __('validation.custom.reports.exists')
            : null;
    }
}
