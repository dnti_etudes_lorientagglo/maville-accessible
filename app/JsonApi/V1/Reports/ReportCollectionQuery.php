<?php

namespace App\JsonApi\V1\Reports;

use App\JsonApi\Concerns\Core\ValidatesCollectionQuery;
use App\JsonApi\Concerns\Core\ValidatesPaginableQuery;
use App\JsonApi\Concerns\Searchable\ValidatesSearchableQuery;
use App\JsonApi\Concerns\SoftDeletable\ValidatesSoftDeletableQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class ReportCollectionQuery.
 */
class ReportCollectionQuery extends ResourceQuery
{
    use ValidatesCollectionQuery;
    use ValidatesPaginableQuery;
    use ValidatesSearchableQuery;
    use ValidatesSoftDeletableQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
            ...$this->pageRules(),
            ...$this->searchableRules(),
            ...$this->softDeletableRules(),
        ];
    }
}
