<?php

namespace App\JsonApi\V1\Reports;

use App\Helpers\JsonApiHelper;
use App\JsonApi\Concerns\Core\BaseSchema;
use App\JsonApi\Concerns\SoftDeletable\SoftDeletableSchema;
use App\JsonApi\Concerns\UserOwnable\UserOwnableSchema;
use App\Models\Report;
use App\Policies\ReportPolicy;
use Illuminate\Support\Facades\Gate;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsTo;
use LaravelJsonApi\Eloquent\Fields\Relations\MorphTo;
use LaravelJsonApi\Eloquent\Fields\Str;

/**
 * Class ReportSchema.
 */
class ReportSchema extends BaseSchema
{
    use SoftDeletableSchema;
    use UserOwnableSchema;

    public static string $model = Report::class;

    protected int $maxDepth = 1;

    /**
     * {@inheritDoc}
     */
    public function fields(): array
    {
        return [
            ...parent::fields(),
            ...$this->softDeletableFields(static fn() => Gate::denies(ReportPolicy::updatePermission())),
            ...$this->userOwnableFields(),
            Str::make('url')
                ->readOnlyOnUpdate(),
            Str::make('body')
                ->readOnlyOnUpdate(),
            BelongsTo::make('reportType')
                ->readOnlyOnUpdate()
                ->serializeUsing(JsonApiHelper::noLinks()),
            MorphTo::make('reportable')
                ->types('reviews', 'places', 'articles', 'activities')
                ->readOnlyOnUpdate()
                ->serializeUsing(JsonApiHelper::noLinks()),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function filters(): array
    {
        return [
            ...parent::filters(),
            ...$this->softDeletableFilters(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function sortables(): iterable
    {
        return [
            ...parent::sortables(),
            ...$this->userOwnableSortables(),
        ];
    }
}
