<?php

namespace App\JsonApi\V1\Reports;

use App\JsonApi\Concerns\Core\ValidatesOneQuery;
use App\JsonApi\Concerns\SoftDeletable\ValidatesSoftDeletableQuery;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;

/**
 * Class ReportQuery.
 */
class ReportQuery extends ResourceQuery
{
    use ValidatesOneQuery;
    use ValidatesSoftDeletableQuery;

    public function rules(): array
    {
        return [
            ...$this->defaultsRules(),
            ...$this->softDeletableRules(),
        ];
    }
}
