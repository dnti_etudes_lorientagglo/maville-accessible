<?php

namespace App\Helpers;

use Illuminate\Support\Str;

/**
 * Class RandomHelper.
 */
class RandomHelper
{
    /**
     * Generate a string of $length containing random $chars.
     *
     * @param int    $length
     * @param string $chars
     *
     * @return string
     */
    public static function randomString(
        int $length = 16,
        string $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
    ): string {
        $charsLength = Str::length($chars);

        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $chars[random_int(0, $charsLength - 1)];
        }

        return $randomString;
    }
}
