<?php

namespace App\Helpers;

use App\Rules\StrictNumericRule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

/**
 * Class RuleHelper.
 */
class RuleHelper
{
    /**
     * Create array hash rules.
     *
     * @param string $name
     * @param array  $items
     * @param array  $rules
     *
     * @return array[]
     */
    public static function arrayHash(string $name, array $items, array $rules = []): array
    {
        $items = collect($items);
        $keys = $items->keys()->filter(static fn(string $k) => ! Str::contains($k, '.'));

        return [
            $name => [...$rules, 'array:' . $keys->join(',')],
            ...$items->mapWithKeys(static fn(string | array $rules, string $key) => [
                $name . '.' . $key => $rules,
            ]),
        ];
    }

    /**
     * Create array list rules.
     *
     * @param string $name
     * @param array  $items
     * @param array  $rules
     *
     * @return array
     */
    public static function arrayList(string $name, array $items, array $rules = []): array
    {
        return [
            $name => [...$rules, 'array'],
            ...self::arrayHash($name . '.*', $items),
        ];
    }

    /**
     * Create rules for a dates opening hours array.
     *
     * @param string $name
     * @param array  $rules
     *
     * @return array
     */
    public static function openingDatesHours(string $name, array $rules = []): array
    {
        return self::arrayList($name, [
            'date'  => ['required', 'date_format:Y-m-d'],
            'start' => ['required', 'date_format:H:i'],
            'end'   => ['required', 'date_format:H:i'],
        ], $rules);
    }

    /**
     * Create rules for a weekdays opening hours array.
     *
     * @param string $name
     * @param array  $rules
     *
     * @return array
     */
    public static function openingWeekdaysHours(string $name, array $rules = []): array
    {
        return self::arrayList($name, [
            'weekday' => ['required', 'integer', 'between:1,7'],
            'start'   => ['required', 'date_format:H:i'],
            'end'     => ['required', 'date_format:H:i'],
        ], $rules);
    }

    /**
     * Create rules for an address object.
     *
     * @param string $name
     * @param array  $rules
     *
     * @return array
     */
    public static function address(string $name, array $rules = []): array
    {
        return self::arrayHash($name, [
            'id'         => ['nullable', 'string', 'max:255'],
            'type'       => ['required', Rule::in(['housenumber', 'street', 'locality', 'municipality'])],
            'address'    => ['nullable', 'string', 'max:500'],
            'city'       => ['required', 'string', 'max:255'],
            'postalCode' => ['required', 'string', 'size:5'],
            'inseeCode'  => ['nullable', 'string', 'max:255'],
            ...self::geometry('geometry', ['required']),
        ], $rules);
    }

    /**
     * Create rules for a geometry object.
     *
     * @param string $name
     * @param array  $rules
     *
     * @return array
     */
    public static function geometry(string $name, array $rules = []): array
    {
        return self::arrayHash($name, [
            'type'          => ['required', Rule::in('Point')],
            'coordinates'   => ['required', 'array', 'size:2'],
            'coordinates.0' => ['required', StrictNumericRule::make(), 'between:-90,90'],
            'coordinates.1' => ['required', StrictNumericRule::make(), 'between:-180,180'],
        ], $rules);
    }

    /**
     * Normalize a related data (input from client or related models).
     *
     * @param mixed $data
     *
     * @return Collection
     */
    public static function normalizeRelated(mixed $data): Collection
    {
        if (empty($data) || ($data instanceof Collection && $data->isEmpty())) {
            return new Collection();
        }

        if ($data instanceof Model || ($data[0] ?? null) instanceof Model) {
            return Collection::wrap($data)
                ->map(static fn(Model $model) => [
                    'type' => $model->getMorphClass(),
                    'id'   => $model->id,
                ])
                ->sortBy([['type'], ['id']]);
        }

        return new Collection(isset($data['type']) ? [$data] : $data);
    }
}
