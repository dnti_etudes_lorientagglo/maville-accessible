<?php

namespace App\Helpers;

use Closure;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * Class MigrationsHelper.
 *
 * Helpers methods for DB migrations.
 */
class MigrationsHelper
{
    /**
     * Check if database is using PostgreSQL driver.
     *
     * @return bool
     */
    public static function isPostgreSQL(): bool
    {
        if (DB::connection()->getDriverName() === 'pgsql') {
            return true;
        }

        // Other database drivers are only allowed when testing.
        if (! App::environment('testing')) {
            throw new \RuntimeException('Invalid database driver defined. Only PostgreSQL database is supported.');
        }

        return false;
    }

    /**
     * Run given callback only on PostgreSQL databases.
     *
     * @param Closure $callback
     *
     * @return void
     */
    public static function whenPostgreSQL(Closure $callback): void
    {
        if (self::isPostgreSQL()) {
            $callback();
        }
    }

    /**
     * Add accessible columns.
     *
     * @param Blueprint $table
     *
     * @return void
     */
    public static function accessible(Blueprint $table): void
    {
        $table->jsonb('accessibility')->nullable();
    }

    /**
     * Add contact columns.
     *
     * @param Blueprint $table
     *
     * @return void
     */
    public static function contactable(Blueprint $table): void
    {
        $table->string('email')->nullable();
        $table->string('phone')->nullable();
    }

    /**
     * Add coverable column.
     *
     * @param Blueprint $table
     *
     * @return void
     */
    public static function coverable(Blueprint $table): void
    {
        $table->foreignUuid('cover_id')
            ->nullable()
            ->constrained('media')
            ->nullOnDelete();
    }

    /**
     * Add links column.
     *
     * @param Blueprint $table
     *
     * @return void
     */
    public static function linkable(Blueprint $table): void
    {
        $table->jsonb('links')->nullable();
    }

    /**
     * Add published at column.
     *
     * @param Blueprint $table
     *
     * @return void
     */
    public static function publisable(Blueprint $table): void
    {
        $table->timestamp('published_at')->nullable();
    }

    /**
     * Add user and organization owner columns.
     *
     * @param Blueprint $table
     *
     * @return void
     */
    public static function ownable(Blueprint $table): void
    {
        self::organizationOwnable($table);
        self::userOwnable($table);
    }

    /**
     * Add slug column.
     *
     * @param Blueprint $table
     *
     * @return void
     */
    public static function sluggable(Blueprint $table): void
    {
        $table->string('slug')->unique();
    }

    /**
     * Add soft delete column.
     *
     * @param Blueprint $table
     *
     * @return void
     */
    public static function softDeletable(Blueprint $table): void
    {
        $table->softDeletes();
    }

    /**
     * Add sourceable column.
     *
     * @param Blueprint $table
     *
     * @return void
     */
    public static function sourceable(Blueprint $table): void
    {
        $table->timestamp('sourced_at')->nullable();
    }

    /**
     * Add user owner columns to table.
     *
     * @param Blueprint $table
     *
     * @return void
     */
    public static function userOwnable(Blueprint $table): void
    {
        $table->foreignUuid('owner_user_id')
            ->nullable()
            ->constrained('users')
            ->nullOnDelete();
    }

    /**
     * Add organization owner columns to table.
     *
     * @param Blueprint $table
     *
     * @return void
     */
    public static function organizationOwnable(Blueprint $table): void
    {
        $table->foreignUuid('owner_organization_id')
            ->nullable()
            ->constrained('organizations')
            ->nullOnDelete();
    }

    /**
     * Add search columns and indexes.
     *
     * @param string $table
     *
     * @return void
     */
    public static function addSearchable(string $table): void
    {
        MigrationsHelper::whenPostgreSQL(function () use ($table) {
            DB::statement('ALTER TABLE ' . $table . ' ADD COLUMN search_text TSVECTOR');
            DB::statement('CREATE INDEX ' . $table . '_search_text ON ' . $table . ' USING GIN(search_text)');
        });
    }

    /**
     * Drop search columns and indexes.
     *
     * @param string $table
     *
     * @return void
     */
    public static function dropSearchable(string $table): void
    {
        MigrationsHelper::whenPostgreSQL(function () use ($table) {
            Schema::table($table, function (Blueprint $table) {
                $table->dropIndex($table->getTable() . '_search_text');
                $table->dropColumn('search_text');
            });
        });
    }
}
