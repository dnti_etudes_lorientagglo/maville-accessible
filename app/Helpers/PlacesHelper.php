<?php

namespace App\Helpers;

use Illuminate\Database\Eloquent\Casts\ArrayObject;

/**
 * Class PlacesHelper.
 */
class PlacesHelper
{
    /**
     * Distance in KM between two points.
     *
     * @param array $point
     * @param array $otherPoint
     *
     * @return float
     */
    public static function distance(array $point, array $otherPoint): float
    {
        $radius = 6371; // km
        $dLat = self::radial($otherPoint['coordinates'][1] - $point['coordinates'][1]);
        $dLon = self::radial($otherPoint['coordinates'][0] - $point['coordinates'][0]);
        $rLat = self::radial($point['coordinates'][1]);
        $rOtherLat = self::radial($otherPoint['coordinates'][1]);

        $a = sin($dLat / 2) * sin($dLat / 2) + sin($dLon / 2) * sin($dLon / 2) * cos($rLat) * cos($rOtherLat);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

        return $radius * $c;
    }

    /**
     * Format an address object to string.
     *
     * @param ArrayObject|array $address
     *
     * @return string
     */
    public static function formatAddress(ArrayObject | array $address): string
    {
        $postalCodeAndCity = $address['postalCode'] . ' ' . $address['city'];

        return ($address['address'] ?? '')
            ? $address['address'] . ', ' . $postalCodeAndCity
            : $postalCodeAndCity;
    }

    private static function radial(float $value): float
    {
        return ($value * M_PI) / 180;
    }
}
