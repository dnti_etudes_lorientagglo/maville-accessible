<?php

namespace App\Helpers;

use Closure;
use DOMDocument;
use DOMException;
use DOMNode;

/**
 * Class DOMHelper.
 */
class DOMHelper
{
    /**
     * Create a UTF-8 document from its content.
     *
     * @param string $content
     *
     * @return DOMDocument
     */
    public static function makeDocument(string $content): DOMDocument
    {
        $head = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>';
        $html = "<!DOCTYPE html><html><head>$head</head><body>$content</body></html>";
        $document = new DOMDocument();
        $document->loadHTML($html, LIBXML_NOERROR);

        return $document;
    }

    /**
     * Replace tags inside a document.
     *
     * @param DOMDocument $document
     * @param array       $tags
     *
     * @return void
     *
     * @throws DOMException
     */
    public static function replaceTags(DOMDocument $document, array $tags): void
    {
        collect($tags)->each(static function (Closure|string|false|null $replacement, string $tag) use ($document) {
            collect($document->getElementsByTagName($tag))->each(
                static function (DOMNode $matchingEl) use ($document, $replacement) {
                    if (is_string($replacement)) {
                        $replacementEl = $document->createElement($replacement);
                        collect($matchingEl->childNodes)->each(
                            static fn(DOMNode $childEl) => $replacementEl->appendChild($childEl),
                        );
                    } else {
                        $replacementEl = is_callable($replacement) ? $replacement($matchingEl) : $replacement;
                    }

                    if ($replacementEl === false) {
                        return;
                    }

                    if ($replacementEl) {
                        $matchingEl->parentNode->replaceChild($replacementEl, $matchingEl);
                    } else {
                        $matchingEl->parentNode->removeChild($matchingEl);
                    }
                },
            );
        });
    }

    /**
     * Get the outer HTML content from node.
     *
     * @param DOMNode $node
     *
     * @return string
     */
    public static function outerHTML(DOMNode $node): string
    {
        return html_entity_decode($node->ownerDocument->saveHTML($node), ENT_QUOTES, 'UTF-8');
    }

    /**
     * Get the inner HTML content from node.
     *
     * @param DOMNode $node
     *
     * @return string
     */
    public static function innerHTML(DOMNode $node): string
    {
        return collect($node->childNodes)
            ->map(static fn(DOMNode $child) => self::outerHTML($child))
            ->join('');
    }
}
