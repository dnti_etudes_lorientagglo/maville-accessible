<?php

namespace App\Helpers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Class ReflectionHelper.
 */
class ReflectionHelper
{
    /**
     * Get a list of PHP classes within an app directory.
     *
     * @param string $directory
     *
     * @return Collection
     */
    public static function classesIn(string $directory): Collection
    {
        $rootPath = app_path();

        return (new Collection(File::allFiles(app_path($directory))))
            ->reduce(function (Collection $classes, SplFileInfo $fileInfo) use ($rootPath) {
                $path = $fileInfo->getRealPath();
                $class = Str::of($path)
                    ->after($rootPath)
                    ->beforeLast('.')
                    ->prepend(App::getNamespace() . '\\')
                    ->replace('/', '\\')
                    ->replaceMatches('/\\\\+/', '\\')
                    ->value();
                if (class_exists($class)) {
                    $classes->push($class);
                }

                return $classes;
            }, new Collection());
    }

    /**
     * Check if given class implement interface.
     *
     * @param string $class
     * @param string $interface
     *
     * @return bool
     */
    public static function classImplements(string $class, string $interface): bool
    {
        $interfaces = class_implements($class);

        return is_array($interfaces) && in_array($interface, $interfaces);
    }

    /**
     * Check if given class extends another class.
     *
     * @param string $class
     * @param string $otherClass
     *
     * @return bool
     */
    public static function classExtends(string $class, string $otherClass): bool
    {
        return is_subclass_of($class, $otherClass);
    }
}
