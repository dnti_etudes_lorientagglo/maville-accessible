<?php

namespace App\Helpers;

use ArrayAccess;
use Closure;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;

/**
 * Class LocaleHelper.
 */
class LocaleHelper
{
    /**
     * The "easy to read" suffix used in translations.
     */
    private const EASY_READ = 'ER';

    /**
     * The header used to indicate that user prefers easy read translations.
     */
    private const HEADER_PREFER_EASY_READ = 'X-Prefer-Easy-Read';

    /**
     * Get the current locale with server format.
     *
     * @return string
     */
    public static function serverLocale(): string
    {
        return App::getLocale();
    }

    /**
     * Get the current locale with BCP47 format.
     *
     * @return string
     */
    public static function locale(): string
    {
        return self::bcp47(self::serverLocale());
    }

    /**
     * Get the default locale with BCP47 format.
     *
     * @return string
     */
    public static function defaultLocale(): string
    {
        return self::allLocalesKeys()->first();
    }

    /**
     * Check if the user prefer easy read translations.
     *
     * @return bool
     */
    public static function preferEasyRead(): bool
    {
        return filter_var(Request::header(self::HEADER_PREFER_EASY_READ), FILTER_VALIDATE_BOOL);
    }

    /**
     * Decompose a locale with or without easy read marker.
     *
     * @param string $locale
     *
     * @return array
     */
    public static function decompose(string $locale): array
    {
        [$language, $preferEasyRead] = [...explode('--', $locale), null];

        return [$language, ! ! $preferEasyRead];
    }

    /**
     * Retrieve the BCP47 standardized locale ("fr-FR" for "fr_FR").
     *
     * @param string $locale
     *
     * @return string
     */
    public static function server(string $locale): string
    {
        return Str::replace('-', '_', $locale);
    }

    /**
     * Retrieve the BCP47 standardized locale ("fr-FR" for "fr_FR").
     *
     * @param string $locale
     *
     * @return string
     */
    public static function bcp47(string $locale): string
    {
        return Str::replace('_', '-', $locale);
    }

    /**
     * Retrieve the ISO-639-1 standardized locale ("fr" for "fr_FR" or "fr-FR").
     *
     * @param string $locale
     *
     * @return string
     */
    public static function iso6391(string $locale): string
    {
        return Str::before(self::bcp47($locale), '-');
    }

    /**
     * Guess the server locale for a given languages set (BCP47 or ISO-639-1).
     *
     * @param array $languages
     *
     * @return string|null
     */
    public static function guessLocale(array $languages): string | null
    {
        $locales = self::serverLocales()->keys();
        $languages = collect($languages)
            ->map(static fn(string $language) => self::server($language));

        return $languages->first(
            static fn(string $lang) => $locales->contains($lang),
        );
    }

    /**
     * Get the easy to read locale version of given one.
     *
     * @param string $locale
     *
     * @return string
     */
    public static function easyRead(string $locale): string
    {
        return $locale . '--' . self::EASY_READ;
    }

    /**
     * Check if a locale (ISO or BCP) has an "easy-read" mode.
     *
     * @param string $locale
     *
     * @return bool
     */
    public static function hasEasyRead(string $locale): bool
    {
        return self::serverEasyReadLocales()->contains(Str::replace('-', '_', $locale));
    }

    /**
     * Get the available locales with server format for application
     * (without easy to read ones).
     *
     * @return Collection<string, string>
     */
    public static function serverLocales(): Collection
    {
        return collect(Config::get('app.locales'));
    }

    /**
     * Get the server locales codes which have easy-read mode.
     *
     * @return Collection<int, string>
     */
    public static function serverEasyReadLocales(): Collection
    {
        return collect(Config::get('app.easy_read_locales'));
    }

    /**
     * Get the available locales with BCP47 format for application
     * (without easy to read ones).
     *
     * @return Collection<string, string>
     */
    public static function locales(): Collection
    {
        return self::serverLocales()->mapWithKeys(
            static fn(string $localeName, string $locale) => [
                LocaleHelper::bcp47($locale) => $localeName,
            ],
        );
    }

    /**
     * Get the locales codes which have easy-read mode.
     *
     * @return Collection<int, string>
     */
    public static function easyReadLocales(): Collection
    {
        return self::serverEasyReadLocales()->map(
            static fn(string $locale) => LocaleHelper::bcp47($locale),
        );
    }

    /**
     * Order the current locales by preference.
     *
     * @return Collection
     */
    public static function orderedLocales(): Collection
    {
        $currentLocale = self::locale();
        $targets = collect([$currentLocale])
            ->when(self::hasEasyRead($currentLocale))
            ->push(self::easyRead($currentLocale));
        if (self::preferEasyRead()) {
            $targets = $targets->reverse()->values();
        }

        return $targets;
    }

    /**
     * Get the available locales keys with BCP47 format for application
     * (with easy to read ones).
     *
     * @return Collection<int, string>
     */
    public static function allLocalesKeys(): Collection
    {
        return collect(self::locales())->reduce(
            static fn(Collection $locales, string $_, string $l) => $locales
                ->push($l)
                ->when(
                    self::hasEasyRead($l),
                    static fn(Collection $locales) => $locales->push(self::easyRead($l)),
                ),
            collect(),
        );
    }

    /**
     * Get the translation for a translatable value.
     *
     * @param ArrayAccess|array|null $translations
     *
     * @return string|null
     */
    public static function translate(ArrayAccess | array | null $translations): string | null
    {
        if ($translations === null) {
            return null;
        }

        $locales = LocaleHelper::orderedLocales();
        foreach ($locales as $locale) {
            $translationForLocale = $translations[$locale] ?? null;
            if ($translationForLocale !== null) {
                return $translationForLocale;
            }
        }

        return null;
    }

    /**
     * Create a map of callback results mapped with locales used for resulting.
     *
     * @param Closure $callback
     *
     * @return array
     */
    public static function mapWithLocales(Closure $callback): array
    {
        return self::locales()
            ->map(static fn($_, string $locale) => self::withLocale($locale, $callback))
            ->all();
    }

    /**
     * Execute a given callback with given locale (server or BCP47).
     *
     * @param string  $locale
     * @param Closure $callback
     *
     * @return mixed
     */
    public static function withLocale(string $locale, Closure $callback): mixed
    {
        $prevLocale = App::getLocale();
        App::setLocale(self::server($locale));

        $result = call_user_func_array($callback, []);

        App::setLocale($prevLocale);

        return $result;
    }
}
