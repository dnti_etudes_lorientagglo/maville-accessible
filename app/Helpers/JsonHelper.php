<?php

namespace App\Helpers;

/**
 * Class JsonHelper.
 */
class JsonHelper
{
    /**
     * JSON encode a value using UNESCAPE options by default.
     *
     * @param mixed $value
     * @param int   $options
     *
     * @return string|false
     */
    public static function encode(
        mixed $value,
        int $options = JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
    ): string|false {
        return json_encode($value, $options);
    }

    /**
     * Decode the given JSON string to an associative array.
     *
     * @param string $value
     *
     * @return array|null
     */
    public static function decode(
        string $value,
    ): array|null {
        return json_decode($value, true);
    }
}
