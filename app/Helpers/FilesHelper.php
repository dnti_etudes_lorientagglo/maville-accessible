<?php

namespace App\Helpers;

use Illuminate\Support\Str;
use League\MimeTypeDetection\GeneratedExtensionToMimeTypeMap;

/**
 * Class FilesHelper.
 */
class FilesHelper
{
    /**
     * Create a temporary file with content.
     *
     * @param string $content
     *
     * @return string
     */
    public static function temporaryFile(string $content): string
    {
        $tmpPath = tempnam(sys_get_temp_dir(), 'temporary-files');
        file_put_contents($tmpPath, $content);

        return $tmpPath;
    }

    /**
     * Detect a file extension from its mime type.
     *
     * @param string $mimeType
     *
     * @return string|null
     */
    public static function detectExtensionFromMimeType(string $mimeType): string|null
    {
        return (new GeneratedExtensionToMimeTypeMap())->lookupExtension($mimeType);
    }

    /**
     * Detect a file extension from its path.
     *
     * @param string $path
     *
     * @return string
     */
    public static function detectMimeType(string $path): string
    {
        return self::mimeTypeOrDefault(mime_content_type($path));
    }

    /**
     * Check if given mime type corresponds to an image.
     *
     * @param string $mimeType
     *
     * @return bool
     */
    public static function isImageMimeType(string $mimeType): bool
    {
        return Str::isMatch('/^image\//', $mimeType);
    }

    /**
     * Sanitize a file name.
     *
     * @param string $fileName
     *
     * @return string
     */
    public static function sanitizeFileName(string $fileName): string
    {
        $fileName = preg_replace('#\p{C}+#u', '', $fileName);

        return str_replace(['#', '/', '\\', ' '], '-', $fileName);
    }

    /**
     * Get a default mime type if given mime type is invalid.
     *
     * @param string|false|null $mimeType
     *
     * @return string
     */
    private static function mimeTypeOrDefault(string|null|false $mimeType): string
    {
        return $mimeType === null || $mimeType === false
            ? 'application/octet-stream'
            : $mimeType;
    }
}
