<?php

namespace App\Helpers;

use App\Models\Activity;
use App\Models\Contracts\Reviewable;
use App\Models\Place;
use InvalidArgumentException;

/**
 * Class RouteHelper.
 */
class RouteHelper
{
    /**
     * Get route to a reviewable model.
     *
     * @param Reviewable $reviewable
     *
     * @return string
     */
    public static function reviewable(Reviewable $reviewable): string
    {
        if ($reviewable instanceof Place) {
            $slugOrId = $reviewable->slug ?? $reviewable->id;

            return "/places/map/reviews/$slugOrId";
        }

        if ($reviewable instanceof Activity) {
            $slugOrId = $reviewable->slug ?? $reviewable->id;

            return "/{$reviewable->activityType->code}/read/$slugOrId";
        }

        throw new InvalidArgumentException('not a valid reviewable for routing');
    }

    /**
     * Get route path to many resources.
     *
     * @param string      $type
     * @param string|null $action
     *
     * @return string
     */
    public static function resourceMany(
        string $type,
        string | null $action = null,
    ): string {
        return self::resource($type, $action ?? 'view-many');
    }

    /**
     * Get route path to one resource.
     *
     * @param string      $type
     * @param string      $id
     * @param string|null $action
     *
     * @return string
     */
    public static function resourceOne(
        string $type,
        string $id,
        string | null $action = null,
    ): string {
        return self::resource($type, $action ?? 'view') . '/' . $id;
    }

    /**
     * Get route path to resource.
     *
     * @param string $type
     * @param string $action
     *
     * @return string
     */
    private static function resource(string $type, string $action): string
    {
        return "/resources/$type/$action";
    }
}
