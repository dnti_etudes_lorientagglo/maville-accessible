<?php

namespace App\Helpers;

use App\Models\Organization;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Throwable;

/**
 * Class AuthHelper.
 */
class AuthHelper
{
    /**
     * Alias of Auth facade for correct typing.
     *
     * @return User|null
     */
    public static function user(): User|null
    {
        return Auth::user();
    }

    /**
     * Check if the auth user has given permission.
     *
     * @param string $permission
     *
     * @return bool
     */
    public static function hasPermissionTo(string $permission): bool
    {
        $user = self::user();
        if (! $user) {
            return false;
        }

        try {
            if ($user->hasPermissionTo($permission)) {
                return true;
            }

            $actingForOrganization = $user->getActingForOrganization();
            if (! $actingForOrganization) {
                return false;
            }

            if ($actingForOrganization->role->hasPermissionTo($permission)) {
                return true;
            }
        } catch (Throwable) {
            return false;
        }

        return false;
    }

    /**
     * Check if user is acting for organization.
     *
     * @param Organization|string $organization
     *
     * @return bool
     */
    public static function isActingFor(Organization|string $organization): bool
    {
        $user = self::user();
        if (! $user) {
            return false;
        }

        $actingForOrganization = $user->getActingForOrganization();
        if (! $actingForOrganization) {
            return false;
        }

        $id = is_string($organization) ? $organization : $organization->id;

        return $actingForOrganization->organization_id === $id;
    }
}
