<?php

namespace App\Helpers;

use App\Models\Media;
use Illuminate\Support\Facades\Config;

/**
 * Class MediaHelper.
 */
class MediaHelper
{
    /**
     * Get the max file size in bytes.
     *
     * @return int
     */
    public static function maxFileSize(): int
    {
        return Config::get('media-library.max_file_size');
    }

    /**
     * Compute hash for a media path.
     *
     * @param string $path
     *
     * @return string
     */
    public static function computeHashForFile(string $path): string
    {
        return hash_file('md5', $path);
    }

    /**
     * Compute a normalized media path.
     *
     * @param Media $media
     *
     * @return string
     */
    public static function computePath(Media $media): string
    {
        return ltrim($media->getPathRelativeToRoot(), '/');
    }
}
