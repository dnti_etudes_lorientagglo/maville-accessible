<?php

namespace App\Helpers;

use Carbon\Carbon;
use Throwable;

/**
 * Class DateHelper.
 */
class DateHelper
{
    /**
     * Format a date in the given format.
     *
     * @param Carbon      $date
     * @param string|null $format
     * @param string|null $timezone
     * @param bool        $utcLabel
     *
     * @return string
     */
    public static function format(
        Carbon $date,
        string | null $format = null,
        string | null $timezone = null,
        bool $utcLabel = false,
    ): string {
        $format = $format ?? 'L';

        if ($timezone) {
            try {
                return $date->clone()->setTimezone($timezone)->isoFormat($format);
            } catch (Throwable) {
                // Ignore the exception and format as UTC.
            }
        }

        return $date->isoFormat($format) . ($utcLabel ? ' UTC' : '');
    }

    /**
     * Convert a classic weekday (sunday=0) to iso weekday (monday=1 and sunday=7).
     *
     * @param int $weekday
     *
     * @return int
     */
    public static function toIsoWeekday(int $weekday): int
    {
        return (($weekday + 6) % 7) + 1;
    }
}
