<?php

namespace App\Helpers;

use App\Models\Casts\TranslationObject;
use Illuminate\Support\Str;

/**
 * Class StrHelper.
 */
class StrHelper
{
    /**
     * Compute description.
     *
     * @param TranslationObject|null $description
     * @param TranslationObject|null $body
     * @param int                    $size
     *
     * @return TranslationObject|null
     */
    public static function computeDescription(
        TranslationObject | null $description,
        TranslationObject | null $body,
        int $size = 255,
    ): TranslationObject | null {
        if ($description !== null && $description->count()) {
            return $description;
        }

        if ($body !== null && $body->count()) {
            return new TranslationObject($body->collect()->map(
                static fn(string | null $value) => self::truncate(self::stripHtml($value), ['size' => $size]),
            )->toArray());
        }

        return null;
    }

    /**
     * Strip HTML from a string and replace whitespaces.
     *
     * @param string $html
     *
     * @return string
     */
    public static function stripHtml(string $html): string
    {
        return Str::of($html)
            ->replace('<br>', "\n")
            ->replaceMatches('/<\/(h[1-6]|p)>/', "</\$1>\n")
            ->stripTags()
            ->replaceMatches('/[\n\r]+/', ' ')
            ->replaceMatches('/ +/', ' ')
            ->trim();
    }

    /**
     * Truncate a text without breaking ending word.
     *
     * @param string $text
     * @param array  $options
     *
     * @return string
     */
    public static function truncate(string $text, array $options = []): string
    {
        $size = $options['size'] ?? 255;
        if (Str::length($text) <= $size) {
            return $text;
        }

        $end = $options['end'] ?? '...';
        $maxLength = $size - Str::length($end);
        $truncatedText = Str::substr($text, 0, $maxLength);
        $lastSpaceIndex = mb_strrpos($truncatedText, ' ');

        return $lastSpaceIndex !== false
            ? Str::substr($truncatedText, 0, min(Str::length($truncatedText), $lastSpaceIndex)) . $end
            : $truncatedText . $end;
    }
}
