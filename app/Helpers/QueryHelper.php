<?php

namespace App\Helpers;

use App\Models\Contracts\Sluggable;
use Closure;
use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Support\Str;

/**
 * Class QueryHelper.
 */
class QueryHelper
{
    /**
     * Add an explicit select clause when query does not have one.
     *
     * @param Builder|EloquentBuilder $query
     * @param array|mixed             $columns
     *
     * @return Builder|EloquentBuilder
     */
    public static function clarifySelects(
        Builder|EloquentBuilder $query,
        mixed $columns = ['*'],
    ): Builder|EloquentBuilder {
        $baseQuery = $query instanceof EloquentBuilder ? $query->getQuery() : $query;
        if (empty($baseQuery->columns)) {
            $baseQuery->select($columns);
        }

        return $query;
    }

    /**
     * Query a relation to have matching values over slugs or IDs.
     *
     * @param EloquentBuilder $query
     * @param string          $relation
     * @param array           $values
     * @param Closure|null    $filterCallback
     * @param Closure|null    $filterUsing
     *
     * @return EloquentBuilder
     */
    public static function whereRelationIn(
        EloquentBuilder $query,
        string $relation,
        array $values,
        Closure|null $filterCallback = null,
        Closure|null $filterUsing = null,
    ): EloquentBuilder {
        $values = collect($values);

        $noneIndex = $values->search('none');
        if ($noneIndex !== false) {
            return $query->whereDoesntHave($relation);
        }

        $anyIndex = $values->search('any');
        if ($anyIndex !== false) {
            $query->whereHas($relation);

            $values->forget($anyIndex);
            if ($values->isEmpty()) {
                return $query;
            }
        }

        return $query->whereHas($relation, static function (EloquentBuilder $query) use (
            $values,
            $filterCallback,
            $filterUsing,
        ) {
            [$ids, $slugs] = collect($values)->partition(
                static fn(string $value) => Str::isUuid($value),
            );

            if (! ($query->getModel() instanceof Sluggable)) {
                $slugs = collect();
            }

            $filterQueryUsing = $filterUsing ?? static fn(Builder $query) => $query->where(
                static fn(Builder $query) => $query
                    ->when($slugs->isNotEmpty(), static fn(Builder $q) => $q->whereIn('slug', $slugs))
                    ->when($ids->isNotEmpty(), static fn(Builder $q) => $q->orWhereIn('id', $ids)),
            );

            $query->where(
                static fn(Builder $query) => $filterQueryUsing($query)->when(
                    static fn() => $filterCallback,
                    static fn(EloquentBuilder $query, Closure $callback) => $callback($query, $filterQueryUsing),
                ),
            );
        });
    }
}
