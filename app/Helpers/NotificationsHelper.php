<?php

namespace App\Helpers;

use App\Models\Enums\RoleName;
use App\Models\User;
use App\Notifications\Auth\NewInvitation;
use App\Notifications\Reactions\NewReaction;
use App\Notifications\Reports\NewPendingReport;
use App\Notifications\Requests\NewPendingRequest;
use App\Notifications\Requests\NewRefusedRequest;
use App\Notifications\Requests\NewValidatedRequest;
use App\Notifications\Reviews\NewReview;
use App\Notifications\Reviews\NewReviewReply;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

/**
 * Class Notifications.
 */
class NotificationsHelper
{
    /**
     * Get the notifications which user may receive mapped with their default channels.
     *
     * @return array
     */
    public static function defaultChannels(): array
    {
        return [
            self::notificationType(NewReview::class)           => [
                'roles'    => [],
                'channels' => ['mail', 'database'],
            ],
            self::notificationType(NewReviewReply::class)      => [
                'roles'    => [],
                'channels' => ['mail', 'database'],
            ],
            self::notificationType(NewReaction::class)         => [
                'roles'    => [],
                'channels' => ['mail', 'database'],
            ],
            self::notificationType(NewInvitation::class)       => [
                'roles'    => [],
                'channels' => ['mail', 'database'],
            ],
            self::notificationType(NewPendingReport::class)    => [
                'roles'    => [RoleName::MODERATOR->value],
                'channels' => ['database'],
            ],
            self::notificationType(NewPendingRequest::class)   => [
                'roles'    => [RoleName::CONTENT_ADMIN->value, RoleName::ORGANIZATION_ADMIN->value],
                'channels' => ['mail', 'database'],
            ],
            self::notificationType(NewValidatedRequest::class) => [
                'roles'    => [],
                'channels' => ['mail', 'database'],
            ],
            self::notificationType(NewRefusedRequest::class)   => [
                'roles'    => [],
                'channels' => ['mail', 'database'],
            ],
        ];
    }

    /**
     * Notify users except currently authenticated user
     * (will also filter null or duplicates).
     *
     * @param Collection<mixed, User|null>|User $users
     * @param mixed                             $notification
     * @param bool                              $ignoreAuth
     *
     * @return void
     */
    public static function notify(
        Collection | User $users,
        mixed $notification,
        bool $ignoreAuth = false,
    ): void {
        $notifiables = self::getNotifiables($users, $ignoreAuth);
        if ($notifiables->isNotEmpty()) {
            Notification::send($notifiables, $notification);
        }
    }

    /**
     * Notify immediately users except currently authenticated user
     * (will also filter null or duplicates).
     *
     * @param Collection<mixed, User|null>|User $users
     * @param mixed                             $notification
     * @param bool                              $ignoreAuth
     *
     * @return void
     */
    public static function notifyNow(
        Collection | User $users,
        mixed $notification,
        bool $ignoreAuth = false,
    ): void {
        $notifiables = self::getNotifiables($users, $ignoreAuth);
        if ($notifiables->isNotEmpty()) {
            Notification::sendNow($notifiables, $notification);
        }
    }

    /**
     * Get notifiable users inside given list.
     *
     * @param Collection<mixed, User|null>|User $users
     * @param bool                              $ignoreAuth
     *
     * @return Collection<mixed, User>
     */
    private static function getNotifiables(Collection | User $users, bool $ignoreAuth): Collection
    {
        $authUser = AuthHelper::user();

        return Collection::wrap($users)
            ->filter(static fn(User | null $user,
            ) => $user && (! $ignoreAuth || ! $user->is($authUser)))
            ->unique('id');
    }

    /**
     * Get the channels to use for a notification.
     *
     * @param string $type
     * @param array  $preferences
     *
     * @return array
     */
    public static function channelsFor(string $type, array $preferences): array
    {
        $notificationType = self::notificationType($type);

        return $preferences[$notificationType] ?? self::defaultChannels()[$notificationType]['channels'];
    }

    /**
     * Get the notification type from the class name.
     *
     * @param string $type
     *
     * @return string
     */
    public static function notificationType(string $type): string
    {
        return Str::of($type)
            ->replaceFirst('App\\Notifications\\', '')
            ->replace('\\', '')
            ->camel()
            ->value();
    }

    /**
     * Get the notification type from the class name.
     *
     * @param string $type
     *
     * @return string
     */
    public static function notificationTranslations(string $type): string
    {
        return Str::of($type)
            ->replaceFirst('App\\Notifications\\', '')
            ->explode('\\')
            ->map(static fn(string $v) => Str::camel($v))
            ->join('.');
    }
}
