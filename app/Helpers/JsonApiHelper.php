<?php

namespace App\Helpers;

use Closure;
use LaravelJsonApi\Core\Resources\Relation;
use LaravelJsonApi\Laravel\Routing\ActionRegistrar;
use LaravelJsonApi\Laravel\Routing\Relationships;

/**
 * Class JsonApiHelper.
 */
class JsonApiHelper
{
    /**
     * Get a closure to disable links serialization on a relation.
     *
     * @return Closure
     */
    public static function noLinks(): Closure
    {
        return static fn(Relation $relation) => $relation->withoutLinks();
    }

    /**
     * Register pinnable routes.
     *
     * @param ActionRegistrar $actions
     *
     * @return void
     */
    public static function pinnableRoutes(ActionRegistrar $actions): void
    {
        $actions->withId()->post('pin');
        $actions->withId()->post('unpin');
    }

    /**
     * Register publishable routes.
     *
     * @param ActionRegistrar $actions
     *
     * @return void
     */
    public static function publishableRoutes(ActionRegistrar $actions): void
    {
        $actions->withId()->post('publish');
        $actions->withId()->post('un-publish');
    }

    /**
     * Register publishable routes.
     *
     * @param ActionRegistrar $actions
     *
     * @return void
     */
    public static function sourceableRoutes(ActionRegistrar $actions): void
    {
        $actions->withId()->post('merge-sources');
        $actions->withId()->post('sync-sources');
    }

    /**
     * Register reviewable relationships.
     *
     * @param Relationships $relationships
     *
     * @return void
     */
    public static function reviewableRelations(Relationships $relationships): void
    {
        $relationships->hasMany('reviews')->only('related');
    }
}
