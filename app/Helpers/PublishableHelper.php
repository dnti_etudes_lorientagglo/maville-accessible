<?php

namespace App\Helpers;

use App\Models\Contracts\Publishable;
use Illuminate\Contracts\Database\Eloquent\Builder;

/**
 * Class PublishableHelper.
 */
class PublishableHelper
{
    /**
     * Filter a query to match only published models.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public static function publishedQuery(Builder $query): Builder
    {
        return $query->whereNotNull('published_at')
            ->when($query->getModel() instanceof Publishable, fn(Builder $query) => $query->where(
                fn(Builder $query) => $query
                    ->whereDoesntHave('categories')
                    ->orWhereRelation('categories', 'published_at', '<>', null),
            ));
    }
}
