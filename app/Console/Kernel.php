<?php

namespace App\Console;

use App\Console\Commands\External\CrawlDispatch;
use App\Console\Commands\External\DataGouvDispatch;
use App\Console\Commands\Newsletter\NewsletterDispatch;
use App\Console\Commands\Users\UsersRunDeletions;
use App\Console\Commands\Users\UsersScheduleDeletions;
use App\Services\Newsletter\NewsletterPeriodicity;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel.
 */
class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->command(DataGouvDispatch::class)
            ->daily()
            ->at('23:00');

        $schedule->command(UsersScheduleDeletions::class)
            ->daily()
            ->at('00:00');

        $schedule->command(UsersRunDeletions::class)
            ->daily()
            ->at('01:00');

        $schedule->command(CrawlDispatch::class, ['places'])
            ->daily()
            ->at('02:00');

        $schedule->command(CrawlDispatch::class, ['articles', '--limit=500'])
            ->daily()
            ->at('03:00');

        $schedule->command(CrawlDispatch::class, ['events', '--limit=500'])
            ->daily()
            ->at('04:00');

        $schedule->command(CrawlDispatch::class, ['activities'])
            ->daily()
            ->at('05:00');

        $schedule->command(CrawlDispatch::class, ['organizations', '--no-create'])
            ->daily()
            ->at('06:00');

        $schedule->command(NewsletterDispatch::class, [NewsletterPeriodicity::monthly()->name])
            ->daily()
            ->at('08:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands(): void
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
