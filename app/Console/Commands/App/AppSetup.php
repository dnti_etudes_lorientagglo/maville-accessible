<?php

namespace App\Console\Commands\App;

use Illuminate\Console\Command;
use Illuminate\Contracts\Config\Repository as ConfigRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class AppSetup.
 */
class AppSetup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup an application (encryption key, database, etc.)';

    /**
     * Execute the console command.
     *
     * @param Application      $app
     * @param ConfigRepository $config
     *
     * @return int
     */
    public function handle(Application $app, ConfigRepository $config): int
    {
        if (! $config->get('app.key')) {
            $this->warn('Generating app key...');
            Artisan::call('key:generate', ['--force' => true]);
            $this->info('Generated app key.');
        }

        if (! file_exists(public_path('storage'))) {
            $this->warn('Linking storage...');
            Artisan::call('storage:link');
            $this->info('Linked storage.');
        }

        if ($app->environment('local')) {
            $defaultConnection = DB::connection();
            $temporaryConnectionName = 'temporarySetupConnection';
            $temporaryConnectionConfig = $config->get('database.connections.' . $defaultConnection->getName());
            $temporaryConnectionConfig['database'] = null;

            $config->set('database.connections.' . $temporaryConnectionName, $temporaryConnectionConfig);

            $this->warn('Preparing database...');
            try {
                DB::connection($temporaryConnectionName)
                    ->statement("CREATE DATABASE \"{$defaultConnection->getDatabaseName()}\"");
            } catch (Throwable) {
                // Database exists. Ignore error.
            }

            $config->set('database.connections.' . $temporaryConnectionName);

            Artisan::call('migrate', ['--force' => true, '--step' => true]);
            Artisan::call('db:seed', ['--force' => true]);
            $this->info('Prepared database.');
        }

        return Command::SUCCESS;
    }
}
