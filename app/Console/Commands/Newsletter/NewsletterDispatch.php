<?php

namespace App\Console\Commands\Newsletter;

use App\Console\Commands\Concerns\ValidatesCommandInputs;
use App\Services\Newsletter\NewsletterPeriodicity;
use App\Services\Newsletter\NewsletterService;
use Illuminate\Console\Command;
use Illuminate\Validation\Rule;

/**
 * Class NewsletterDispatch.
 */
class NewsletterDispatch extends Command
{
    use ValidatesCommandInputs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newsletter:dispatch {periodicity}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dispatch newsletter of given periodicity if current date matches.';

    /**
     * Execute the console command.
     *
     * @param NewsletterService $newsletterService
     *
     * @return int
     */
    public function handle(NewsletterService $newsletterService): int
    {
        $periodicityName = $this->argument('periodicity');

        $valid = $this->validateInput(['periodicity' => $periodicityName], [
            'periodicity' => [
                'required',
                Rule::in(NewsletterPeriodicity::all()->keys()),
            ],
        ]);
        if (! $valid) {
            return Command::FAILURE;
        }

        /** @var NewsletterPeriodicity $periodicity */
        $periodicity = NewsletterPeriodicity::all()->get($periodicityName);

        $this->warn('Checking current date to be start of period...');

        $now = now();
        if (! $periodicity->isValidStartOfPeriod($now)) {
            $this->info(
                "Stopping newsletter dispatch: current date ({$now->toDateString()}) is not a period start.",
            );

            return Command::SUCCESS;
        }

        $this->warn('Dispatching newsletter...');

        $campaign = $newsletterService->sendNewsletter($periodicity, $now);
        if ($campaign) {
            $this->info('Dispatched newsletter.');
        } else {
            $this->info(
                'Stopping newsletter dispatch: no content to display in newsletter.',
            );
        }

        return Command::SUCCESS;
    }
}
