<?php

namespace App\Console\Commands\Search;

use App\Helpers\ReflectionHelper;
use App\Models\Contracts\FulltextSearchable;
use App\Services\Search\FulltextSearch;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use ReflectionClass;

/**
 * Class SearchUpdateIndexes.
 */
class SearchUpdateIndexes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'search:update-indexes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all search indexes for all searchable models.';

    /**
     * Execute the console command.
     *
     * @param FulltextSearch $fulltextSearch
     *
     * @return int
     */
    public function handle(FulltextSearch $fulltextSearch): int
    {
        ReflectionHelper::classesIn('Models')
            ->filter(static fn(string $class) => (
                ! (new ReflectionClass($class))->isAbstract()
                && ReflectionHelper::classExtends($class, Model::class)
                && ReflectionHelper::classImplements($class, FulltextSearchable::class)
            ))
            ->map(static fn(string $class) => new $class())
            ->each(function (FulltextSearchable & Model $model) use ($fulltextSearch) {
                if ($model->isSearchable()) {
                    $count = $model->newQueryWithoutScopes()->count();

                    $this->warn("Updating $count \"{$model->getTable()}\"...");

                    $fulltextSearch->updateSearchColumnsForModel($model);

                    $this->info("Updated $count \"{$model->getTable()}\"...");
                }
            });

        return Command::SUCCESS;
    }
}
