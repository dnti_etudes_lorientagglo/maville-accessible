<?php

namespace App\Console\Commands\Concerns;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

/**
 * Trait ValidatesCommandInputs.
 *
 * @mixin Command
 */
trait ValidatesCommandInputs
{
    /**
     * Validate a command input and show errors.
     *
     * @param array $data
     * @param array $rules
     *
     * @return bool
     */
    private function validateInput(array $data, array $rules): bool
    {
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            $this->warn('Invalid command input, see details below:');

            collect($validator->errors()->all())
                ->each(fn(string $error) => $this->warn("- $error"));

            return false;
        }

        return true;
    }
}
