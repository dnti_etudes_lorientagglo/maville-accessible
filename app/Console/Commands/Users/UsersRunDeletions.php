<?php

namespace App\Console\Commands\Users;

use App\Services\Auth\UsersService;
use Illuminate\Console\Command;

/**
 * Class UsersScheduleDeletions.
 */
class UsersRunDeletions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:run-deletions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run scheduled users\' deletions';

    /**
     * Execute the console command.
     *
     * @param UsersService $usersService
     *
     * @return int
     */
    public function handle(UsersService $usersService): int
    {
        $this->warn("Deleting scheduled users...");

        $deleted = $usersService->deleteScheduled(now());

        $this->info("Deleted $deleted users.");

        return Command::SUCCESS;
    }
}
