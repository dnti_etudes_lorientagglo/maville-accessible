<?php

namespace App\Console\Commands\Users;

use App\Services\Auth\UsersService;
use Illuminate\Console\Command;

/**
 * Class UsersScheduleDeletions.
 */
class UsersScheduleDeletions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:schedule-deletions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Schedule not recently logger users\' deletions';

    /**
     * Execute the console command.
     *
     * @param UsersService $usersService
     *
     * @return int
     */
    public function handle(UsersService $usersService): int
    {
        $this->warn("Scheduling users deletion...");

        $scheduled = $usersService->scheduleDeletions(now());

        $this->info("Scheduled $scheduled users deletion.");

        return Command::SUCCESS;
    }
}
