<?php

namespace App\Console\Commands\Users;

use App\Console\Commands\Concerns\ValidatesCommandInputs;
use App\Models\Enums\RoleScopeName;
use App\Models\Role;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;

/**
 * Class UsersCreate.
 */
class UsersCreate extends Command
{
    use ValidatesCommandInputs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a user with all roles.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $firstName = $this->ask('Please give a firstname?');
        $lastName = $this->ask('Please give a lastname?');
        $email = $this->ask('Please give an email?');
        $password = $this->secret('Please give a password?');

        $attrs = [
            'first_name' => $firstName,
            'last_name'  => $lastName,
            'email'      => $email,
            'password'   => $password,
        ];

        $valid = $this->validateInput($attrs, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name'  => ['required', 'string', 'max:255'],
            'email'      => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'password'   => ['required', Password::defaults()],
        ]);
        if (! $valid) {
            return Command::FAILURE;
        }

        $this->warn('Creating user...');

        $roles = Role::query()
            ->where('scope_name', RoleScopeName::GLOBAL->value)
            ->get();

        $user = new User();
        $user->first_name = $attrs['first_name'];
        $user->last_name = $attrs['last_name'];
        $user->email = $attrs['email'];
        $user->email_verified_at = now();
        $user->password = Hash::make($attrs['password']);
        $user->save();

        $user->assignRole(...$roles);

        $this->info('Created user.');

        return Command::SUCCESS;
    }
}
