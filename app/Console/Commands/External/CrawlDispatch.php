<?php

namespace App\Console\Commands\External;

use App\Jobs\External\CrawlRunJob;
use App\Services\External\ExternalServicesManager;
use Illuminate\Console\Command;
use Illuminate\Contracts\Bus\Dispatcher;

/**
 * Class CrawlDispatch.
 */
class CrawlDispatch extends Command
{
    use RunsCrawlCommand;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:dispatch ' . self::CRAWL_COMMAND_SIGNATURE;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dispatch crawling contents from other websites and APIs';

    /**
     * Execute the console command.
     *
     * @param Dispatcher              $dispatcher
     * @param ExternalServicesManager $manager
     *
     * @return int
     */
    public function handle(Dispatcher $dispatcher, ExternalServicesManager $manager): int
    {
        return $this->runCrawl($manager, function (array $params) use ($dispatcher) {
            $this->warn('Dispatching crawl job...');

            $dispatcher->dispatch(new CrawlRunJob($params));

            $this->info('Dispatched crawl job.');
        });
    }
}
