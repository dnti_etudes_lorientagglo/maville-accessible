<?php

namespace App\Console\Commands\External;

use App\Console\Commands\Concerns\ValidatesCommandInputs;
use App\Services\External\ExternalServicesManager;
use Closure;
use Illuminate\Console\Command;
use Illuminate\Validation\Rule;

/**
 * Trait ExecutesCrawlCommand.
 *
 * @mixin Command
 */
trait RunsCrawlCommand
{
    use ValidatesCommandInputs;

    /**
     * Signature of a crawl command.
     */
    private const CRAWL_COMMAND_SIGNATURE = '
    {type? : Type of content to crawl.}
    {--limit= : Number of contents to crawl before stopping.}
    {--no-create : Disable creation of new contents.}
    {--no-update : Disable update of existing contents.}
    {--option=* : Configuration options to pass to crawlers.}
    ';

    /**
     * Run a crawl command using runner closure.
     *
     * @param ExternalServicesManager $manager
     * @param Closure                 $runner
     *
     * @return int
     */
    private function runCrawl(
        ExternalServicesManager $manager,
        Closure $runner,
    ): int {
        $availableTypes = $manager->getAvailableContentTypes();

        $type = $this->argument('type')
            ?? $this->choice('Choose type of contents to crawl:', $availableTypes);
        $limit = $this->option('limit');
        $options = $this->option('option');

        $valid = $this->validateInput([
            'type'    => $type,
            'limit'   => $limit,
            'options' => $options,
        ], [
            'type'      => ['required', Rule::in($availableTypes)],
            'limit'     => ['nullable', 'integer', 'min:0'],
            'options'   => ['present', 'array'],
            'options.*' => ['required', 'string'],
        ]);
        if (! $valid) {
            return Command::FAILURE;
        }

        call_user_func($runner, [
            'type'      => $type,
            'limit'     => $limit,
            'options'   => $options,
            'no-create' => $this->option('no-create'),
            'no-update' => $this->option('no-update'),
        ]);

        return Command::SUCCESS;
    }
}
