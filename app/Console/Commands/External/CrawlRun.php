<?php

namespace App\Console\Commands\External;

use App\Services\External\Concerns\CrawlReport;
use App\Services\External\Concerns\CrawlTask;
use App\Services\External\Contracts\ExternalService;
use App\Services\External\ExternalServicesManager;
use Illuminate\Console\Command;
use Symfony\Component\Console\Helper\ProgressBar;

/**
 * Class CrawlRun.
 */
class CrawlRun extends Command
{
    use RunsCrawlCommand;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:run ' . self::CRAWL_COMMAND_SIGNATURE;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Immediately crawl contents from other websites and APIs';

    /**
     * Execute the console command.
     *
     * @param ExternalServicesManager $manager
     *
     * @return int
     */
    public function handle(ExternalServicesManager $manager): int
    {
        return $this->runCrawl($manager, function (array $params) use ($manager) {
            $task = new CrawlTask(
                startCallback: fn(string $type, ExternalService $service) => $this->warn(
                    "Crawling [$type]: {$service->serviceName()}...",
                ),
                skipCallback: fn(string $type, ExternalService $service) => $this->warn(
                    "Crawling [$type]: {$service->serviceName()} is not configured or disabled, skipping.",
                ),
                endCallback: function (string $type, ExternalService $service, CrawlReport $report) {
                    $this->line('');
                    $this->info("Crawled [$type]: {$service->serviceName()}.");
                    $this->info("   - Created:          {$report->createdCount()}");
                    $this->info("   - Updated:          {$report->updatedCount()}");
                    $this->info("   - Ignored:          {$report->ignoredCount()}");
                    $this->info("   - Errors:           {$report->errors->count()} (check logs for details)");
                    $this->info('   - Potential total:  ' . $report->totalCount() ?? 'unknown');
                },
                limit: $params['limit'],
                options: $params['options'],
                noCreate: $params['no-create'],
                noUpdate: $params['no-update'],
                progressBar: new ProgressBar($this->output),
            );

            $manager->crawl($task, [$params['type']]);
        });
    }
}
