<?php

namespace App\Console\Commands\External;

use App\Jobs\External\DataGouvSendJob;
use Illuminate\Console\Command;
use Illuminate\Contracts\Bus\Dispatcher;

class DataGouvDispatch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'datagouv:dispatch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dispatch a job to send up-to-date resources to data.gouv.fr datasets.';

    /**
     * Execute the console command.
     *
     * @param Dispatcher $dispatcher
     *
     * @return int
     */
    public function handle(Dispatcher $dispatcher): int
    {
        $this->warn('Dispatching data.gouv.fr extract...');

        $dispatcher->dispatch(new DataGouvSendJob());

        $this->info('Dispatched data.gouv.fr extract.');

        return Command::SUCCESS;
    }
}
