<?php

namespace App\Console\Commands\Media;

use App\Models\Media;
use App\Services\Media\MediaService;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class MediaUpdateHash.
 */
class MediaUpdateHash extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'media:update-hashes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update hashes of all updated media';

    /**
     * Execute the console command.
     *
     * @param MediaService $mediaService
     *
     * @return int
     */
    public function handle(MediaService $mediaService): int
    {
        $count = Media::query()->whereNotNull('size')->count();

        $this->warn("Changing $count media hashes...");

        $progress = $this->output->createProgressBar($count);
        $progress->start();

        Media::query()
            ->whereNotNull('size')
            ->orderBy('created_at')
            ->chunk(100, fn(Collection $files) => $files->each(
                function (Media $media) use ($mediaService, $progress) {
                    $pendingHash = hash_init('md5');
                    hash_update_stream(
                        $pendingHash,
                        $mediaService->strategyForMedia($media)->readAsStream($media),
                    );

                    $media->setCustomProperty('hash', hash_final($pendingHash));
                    $media->saveQuietly(['timestamps' => false]);

                    $progress->advance();
                },
            ));

        $progress->finish();

        $this->line('');
        $this->info('Changed media hashes.');

        return Command::SUCCESS;
    }
}
