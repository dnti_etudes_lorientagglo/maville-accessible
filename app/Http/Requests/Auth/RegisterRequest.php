<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

/**
 * Class RegisterRequest.
 */
class RegisterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'data'           => ['required', 'array:firstName,lastName,email,password'],
            'data.firstName' => ['required', 'string', 'max:255'],
            'data.lastName'  => ['required', 'string', 'max:255'],
            'data.email'     => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'data.password'  => ['required', Password::defaults()],
        ];
    }
}
