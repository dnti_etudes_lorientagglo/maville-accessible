<?php

namespace App\Http\Requests\Auth;

use App\Services\Auth\VerifiesEmail;
use Illuminate\Foundation\Auth\EmailVerificationRequest as BaseEmailVerificationRequest;

/**
 * Class EmailVerificationRequest.
 */
class EmailVerificationRequest extends BaseEmailVerificationRequest
{
    use VerifiesEmail;

    /**
     * {@inheritDoc}
     */
    public function fulfill()
    {
        $this->markEmailAsVerified($this->user());
    }
}
