<?php

namespace App\Http\Requests\Media;

use App\Helpers\AuthHelper;
use App\Helpers\RuleHelper;
use App\Models\Enums\FileVisibility;
use App\Models\Media;
use App\Policies\MediaPolicy;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class MediaPrepareClientSideUploadRequest.
 */
class MediaPrepareClientSideUploadRequest extends FormRequest
{
    public function authorize(): bool
    {
        $user = AuthHelper::user();

        return $user && $user->can(MediaPolicy::CREATE, Media::class);
    }

    public function rules(): array
    {
        return RuleHelper::arrayHash('data', [
            'files' => ['required', 'array', 'min:1', 'max:5'],
            ...RuleHelper::arrayHash('files.*', [
                'name'       => ['required', 'string', 'min:1', 'max:255'],
                'hash'       => ['required', 'string'],
                'size'       => ['required', 'integer', 'min:0'],
                'mimeType'   => ['required', 'string'],
                'visibility' => [
                    'required',
                    Rule::in(
                        collect(FileVisibility::cases())
                            ->map(static fn(FileVisibility $v) => $v->value),
                    ),
                ],
            ], ['required']),
        ], ['required']);
    }
}
