<?php

namespace App\Http\Requests\Media;

use App\Helpers\AuthHelper;
use App\Helpers\MediaHelper;
use App\Helpers\RuleHelper;
use App\Models\Enums\FileVisibility;
use App\Models\Media;
use App\Policies\MediaPolicy;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\File;

/**
 * Class MediaUploadRequest.
 */
class MediaUploadRequest extends FormRequest
{
    public function authorize(): bool
    {
        $user = AuthHelper::user();

        return $user && $user->can(MediaPolicy::CREATE, Media::class);
    }

    public function rules(): array
    {
        return RuleHelper::arrayHash('data', [
            'files' => ['required', 'array', 'min:1', 'max:5'],
            ...RuleHelper::arrayHash('files.*', [
                'file'       => [
                    'required',
                    File::default()->max(MediaHelper::maxFileSize() / 1024),
                ],
                'visibility' => [
                    'required',
                    Rule::in(
                        collect(FileVisibility::cases())
                            ->map(static fn(FileVisibility $v) => $v->value),
                    ),
                ],
            ], ['required']),
        ], ['required']);
    }
}
