<?php

namespace App\Http\Requests\Media;

use App\Helpers\AuthHelper;
use App\Helpers\RuleHelper;
use App\Models\Media;
use App\Policies\MediaPolicy;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class MediaConfirmClientSideUploadRequest.
 */
class MediaConfirmClientSideUploadRequest extends FormRequest
{
    public function authorize(): bool
    {
        $user = AuthHelper::user();

        return $user && $user->can(MediaPolicy::CREATE, Media::class);
    }

    public function rules(): array
    {
        return RuleHelper::arrayHash('data', [
            'media'   => ['required', 'array', 'min:1', 'max:5'],
            'media.*' => ['required', 'uuid'],
        ], ['required']);
    }
}
