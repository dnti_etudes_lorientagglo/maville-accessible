<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Response;

/**
 * Class HealthcheckController.
 */
class HealthcheckController
{
    /**
     * Healthcheck route used by docker for Healthcheck.
     *
     * @return Response
     */
    public function __invoke(): Response
    {
        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
