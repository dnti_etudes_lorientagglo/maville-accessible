<?php

namespace App\Http\Controllers\Web\Services;

use App\Helpers\AuthHelper;
use App\Http\Controllers\Controller;
use App\Models\SendingCampaign;
use App\Policies\SendingCampaignPolicy;
use App\Services\Sending\SendingService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class SendingCampaignController.
 */
class SendingCampaignController extends Controller
{
    /**
     * SendingCampaignController constructor.
     *
     * @param SendingService $sendingService
     */
    public function __construct(private readonly SendingService $sendingService)
    {
    }

    /**
     * Preview a sending campaign content for user in browser.
     *
     * @param SendingCampaign $campaign
     *
     * @return Response
     *
     * @throws AuthorizationException
     */
    public function preview(SendingCampaign $campaign): Response
    {
        $this->authorize(SendingCampaignPolicy::VIEW, $campaign);

        $content = $this->sendingService->previewCampaign($campaign, AuthHelper::user());

        if ($content === null) {
            throw new NotFoundHttpException();
        }

        return new Response($content);
    }

    /**
     * Show a sending campaign content for recipient in browser.
     *
     * @param Request         $request
     * @param SendingCampaign $campaign
     * @param string          $recipient
     *
     * @return Response
     *
     * @throws AuthorizationException
     */
    public function browser(Request $request, SendingCampaign $campaign, string $recipient): Response
    {
        if (! $request->hasValidSignature()) {
            throw new AuthorizationException();
        }

        $content = $this->sendingService->renderCampaign($campaign, base64_decode($recipient));

        if ($content === null) {
            throw new NotFoundHttpException();
        }

        return new Response($content);
    }
}
