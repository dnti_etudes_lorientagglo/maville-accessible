<?php

namespace App\Http\Controllers\Web\Services;

use App\Http\Controllers\Controller;
use App\Models\Media;
use App\Policies\MediaPolicy;
use App\Services\Media\MediaService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;

/**
 * Class MediaController.
 */
class MediaController extends Controller
{
    /**
     * MediaController constructor.
     *
     * @param MediaService $mediaService
     */
    public function __construct(private readonly MediaService $mediaService)
    {
    }

    /**
     * Redirect to a media URL.
     *
     * @param Media $media
     *
     * @return RedirectResponse
     *
     * @throws AuthorizationException
     */
    public function redirect(Media $media): RedirectResponse
    {
        $this->authorize(MediaPolicy::VIEW, $media);

        return new RedirectResponse($this->mediaService->generateServeURL($media));
    }

    /**
     * Resolve a media URL.
     *
     * @param Media $media
     *
     * @return Response
     *
     * @throws AuthorizationException
     */
    public function resolve(Media $media): Response
    {
        $this->authorize(MediaPolicy::VIEW, $media);

        return new Response($this->mediaService->generateServeURL($media));
    }
}
