<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Services\App\AppEnv;
use Illuminate\Contracts\Config\Repository as ConfigRepository;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

/**
 * Class EmbedController.
 */
class EmbedController extends Controller
{
    /**
     * EmbedController constructor.
     *
     * @param ConfigRepository $config
     * @param ViewFactory      $viewFactory
     */
    public function __construct(
        private readonly ConfigRepository $config,
        private readonly ViewFactory $viewFactory,
    ) {
    }

    /**
     * Show the embed view.
     *
     * @param AppEnv  $appEnv
     * @param Request $request
     *
     * @return View
     */
    public function __invoke(AppEnv $appEnv, Request $request): View
    {
        return $this->viewFactory->make('embed', [
            'title' => $this->config->get('app.name'),
            'env'   => $appEnv->publicEnv(),
        ]);
    }
}
