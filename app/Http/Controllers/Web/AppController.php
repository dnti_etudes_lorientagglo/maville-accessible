<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Services\App\AppEnv;
use Illuminate\Contracts\Config\Repository as ConfigRepository;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

/**
 * Class AppController.
 */
class AppController extends Controller
{
    /**
     * AppController constructor.
     *
     * @param ConfigRepository $config
     * @param ViewFactory      $viewFactory
     */
    public function __construct(
        private readonly ConfigRepository $config,
        private readonly ViewFactory $viewFactory,
    ) {
    }

    /**
     * Show the main app view.
     *
     * @param AppEnv  $appEnv
     * @param Request $request
     *
     * @return View
     */
    public function __invoke(AppEnv $appEnv, Request $request): View
    {
        return $this->viewFactory->make('app', [
            'title' => $this->config->get('app.name'),
            'env'   => $appEnv->publicEnv(),
        ]);
    }
}
