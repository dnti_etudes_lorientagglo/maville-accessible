<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Services\Auth\SocialAuthentication\SocialAuthenticationManager;
use App\Services\Auth\VerifiesEmail;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Laravel\Socialite\Two\ProviderInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

/**
 * Class SocialAuthenticatedSessionController.
 */
class SocialAuthenticatedSessionController extends Controller
{
    use VerifiesEmail;

    /**
     * SocialAuthenticatedSessionController constructor.
     *
     * @param SocialAuthenticationManager $socialAuthenticationManager
     */
    public function __construct(
        private readonly SocialAuthenticationManager $socialAuthenticationManager,
    ) {
    }

    /**
     * Redirect to the given network for OAuth authentication flow.
     *
     * @param string $network
     *
     * @return RedirectResponse
     *
     * @throws NotFoundHttpException
     */
    public function redirect(string $network): RedirectResponse
    {
        return $this->useDriver($network)->redirect();
    }

    /**
     * Handle a given network OAuth callback.
     *
     * @param string $network
     *
     * @return RedirectResponse
     *
     * @throws AuthorizationException
     * @throws NotFoundHttpException
     */
    public function callback(string $network): RedirectResponse
    {
        $user = $this->useConvertedUser($network);

        /** @var User|null $existingUser */
        $existingUser = User::query()
            ->where('email', Str::lower($user->email))
            ->first();
        if ($existingUser) {
            $this->markEmailAsVerified($existingUser);

            Auth::login($existingUser);
        } else {
            $this->markEmailAsVerified($user);

            $user->save();

            Auth::login($user);
        }

        return new RedirectResponse(RouteServiceProvider::AUTH_HOME);
    }

    /**
     * Use the given network's driver.
     *
     * @param string $network
     *
     * @return ProviderInterface
     *
     * @throws NotFoundHttpException
     */
    private function useDriver(string $network): ProviderInterface
    {
        try {
            return $this->socialAuthenticationManager->driverFor($network);
        } catch (Throwable) {
            throw new NotFoundHttpException();
        }
    }

    /**
     * Use the given network's user model instance when handling a callback.
     *
     * @param string $network
     *
     * @return User
     *
     * @throws AuthorizationException
     * @throws NotFoundHttpException
     */
    private function useConvertedUser(string $network): User
    {
        try {
            return $this->socialAuthenticationManager->userFor($network);
        } catch (Throwable) {
            throw new AuthorizationException();
        }
    }
}
