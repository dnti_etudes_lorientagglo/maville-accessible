<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class EmailVerificationNotificationController.
 */
class EmailVerificationNotificationController extends Controller
{
    /**
     * Send a new email verification notification.
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws AuthorizationException
     */
    public function store(Request $request): JsonResponse
    {
        if ($request->user()->hasVerifiedEmail()) {
            throw new AuthorizationException();
        }

        $request->user()->sendEmailVerificationNotification();

        return new JsonResponse([
            'data' => [
                'message' => trans('auth.verification.queued'),
            ],
        ], Response::HTTP_ACCEPTED);
    }
}
