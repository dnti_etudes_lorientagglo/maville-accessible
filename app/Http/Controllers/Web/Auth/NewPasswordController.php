<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class NewPasswordController.
 */
class NewPasswordController extends Controller
{
    /**
     * Handle an incoming new password request.
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $this->validate($request, [
            'data'          => ['required', 'array:token,email,password'],
            'data.token'    => ['required'],
            'data.email'    => ['required', 'email'],
            'data.password' => ['required', Rules\Password::defaults()],
        ]);

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $status = Password::reset(
            $request->input('data'),
            static function (User $user) use ($request) {
                $user->forceFill([
                    'password'       => Hash::make($request->input('data.password')),
                    'remember_token' => Str::random(60),
                ])->save();

                event(new PasswordReset($user));
            }
        );

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        if ($status === Password::PASSWORD_RESET) {
            return new JsonResponse([
                'data' => [
                    'id'      => auth()->id(),
                    'message' => trans($status),
                ],
            ], Response::HTTP_OK);
        }

        throw ValidationException::withMessages([
            'data.password' => trans($status),
        ]);
    }
}
