<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ConfirmablePasswordController.
 */
class ConfirmablePasswordController extends Controller
{
    /**
     * Confirm the user's password.
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $this->validate($request, [
            'data'          => ['required', 'array:password'],
            'data.password' => ['required', 'string'],
        ]);

        $data = [
            'email'    => $request->user()->email,
            'password' => $request->input('data.password'),
        ];

        if (! Auth::guard('web')->validate($data)) {
            throw ValidationException::withMessages([
                'data.password' => trans('auth.confirmPassword.failed'),
            ]);
        }

        $request->session()->put('auth.password_confirmed_at', time());

        return new JsonResponse([
            'data' => [
                'message' => trans('auth.confirmPassword.success'),
            ],
        ], Response::HTTP_OK);
    }
}
