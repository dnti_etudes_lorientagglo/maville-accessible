<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RegisteredUserController.
 */
class RegisteredUserController extends Controller
{
    /**
     * Handle an incoming registration request.
     *
     * @param RegisterRequest $request
     *
     * @return JsonResponse
     */
    public function store(RegisterRequest $request): JsonResponse
    {
        /** @var User $user */
        $user = User::query()->create([
            'first_name' => $request->input('data.firstName'),
            'last_name'  => $request->input('data.lastName'),
            'email'      => $request->input('data.email'),
            'password'   => Hash::make($request->input('data.password')),
        ]);

        event(new Registered($user));

        Auth::login($user);

        return new JsonResponse([
            'data' => [
                'message' => trans('auth.register.success'),
                'id'      => $user->id,
            ],
        ], Response::HTTP_CREATED);
    }
}
