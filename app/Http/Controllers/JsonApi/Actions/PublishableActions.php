<?php

namespace App\Http\Controllers\JsonApi\Actions;

use App\Http\Controllers\JsonApi\Concerns\ResolvesRouteModels;
use App\Models\Composables\IsPublishable;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use LaravelJsonApi\Contracts\Routing\Route;
use LaravelJsonApi\Contracts\Store\Store as StoreContract;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Trait PublishableActions.
 *
 * @mixin JsonApiController
 */
trait PublishableActions
{
    use ResolvesRouteModels;

    /**
     * Publish the given model.
     *
     * @param Route         $route
     * @param StoreContract $store
     *
     * @return JsonResponse
     *
     * @throws AuthorizationException
     */
    public function publish(Route $route, StoreContract $store): JsonResponse
    {
        /** @var IsPublishable $model */
        [$model] = $this->resolveOneModel($route, $store);

        $this->authorize('publish', $model);

        $model->published_at = now();
        $model->save();

        if (method_exists($this, 'onPublished')) {
            $this->onPublished($model);
        }

        return new JsonResponse('', Response::HTTP_NO_CONTENT);
    }

    /**
     * Un-publish the given model.
     *
     * @param Route         $route
     * @param StoreContract $store
     *
     * @return JsonResponse
     *
     * @throws AuthorizationException
     */
    public function unPublish(Route $route, StoreContract $store): JsonResponse
    {
        /** @var IsPublishable $model */
        [$model] = $this->resolveOneModel($route, $store);

        $this->authorize('unPublish', $model);

        $model->published_at = null;
        $model->save();

        if (method_exists($this, 'onUnPublished')) {
            $this->onUnPublished($model);
        }

        return new JsonResponse('', Response::HTTP_NO_CONTENT);
    }
}
