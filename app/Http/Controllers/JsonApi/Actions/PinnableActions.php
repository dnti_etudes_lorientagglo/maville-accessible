<?php

namespace App\Http\Controllers\JsonApi\Actions;

use App\Http\Controllers\JsonApi\Concerns\ResolvesRouteModels;
use App\Models\Composables\IsPinnable;
use App\Policies\ActivityPolicy;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use LaravelJsonApi\Contracts\Routing\Route;
use LaravelJsonApi\Contracts\Store\Store as StoreContract;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Trait PinnableActions.
 *
 * @mixin JsonApiController
 */
trait PinnableActions
{
    use ResolvesRouteModels;

    /**
     * Pin an activity.
     *
     * @param Route         $route
     * @param StoreContract $store
     *
     * @return JsonResponse
     *
     * @throws AuthorizationException
     */
    public function pin(Route $route, StoreContract $store): JsonResponse
    {
        /** @var IsPinnable $model */
        [$model] = $this->resolveOneModel($route, $store);

        $this->authorize(ActivityPolicy::PIN, $model);

        $model->pinned_at = now();
        $model->save();

        return new JsonResponse('', Response::HTTP_NO_CONTENT);
    }

    /**
     * Unpin an activity.
     *
     * @param Route         $route
     * @param StoreContract $store
     *
     * @return JsonResponse
     *
     * @throws AuthorizationException
     */
    public function unpin(Route $route, StoreContract $store): JsonResponse
    {
        /** @var IsPinnable $model */
        [$model] = $this->resolveOneModel($route, $store);

        $this->authorize(ActivityPolicy::UNPIN, $model);

        $model->pinned_at = null;
        $model->save();

        return new JsonResponse('', Response::HTTP_NO_CONTENT);
    }
}
