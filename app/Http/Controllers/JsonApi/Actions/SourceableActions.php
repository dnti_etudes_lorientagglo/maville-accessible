<?php

namespace App\Http\Controllers\JsonApi\Actions;

use App\Helpers\RuleHelper;
use App\Http\Controllers\JsonApi\Concerns\ResolvesRouteModels;
use App\Models\Composables\IsSourceable;
use App\Models\Contracts\Reviewable;
use App\Models\Contracts\Sourceable;
use App\Models\Review;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use LaravelJsonApi\Contracts\Routing\Route;
use LaravelJsonApi\Contracts\Store\Store as StoreContract;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Trait SourceableActions.
 *
 * @mixin JsonApiController
 */
trait SourceableActions
{
    use ResolvesRouteModels;

    /**
     * Sync the given model with its sources.
     *
     * @param Route         $route
     * @param StoreContract $store
     *
     * @return JsonResponse
     *
     * @throws AuthorizationException
     */
    public function syncSources(Route $route, StoreContract $store): JsonResponse
    {
        /** @var Sourceable&IsSourceable $model */
        [$model] = $this->resolveOneModel($route, $store);

        $this->authorize('syncSources', $model);

        $model->markResyncedWithSources();

        return new JsonResponse('', Response::HTTP_NO_CONTENT);
    }

    /**
     * Merge the given model's sources into another one.
     *
     * @param Route         $route
     * @param StoreContract $store
     * @param Request       $request
     *
     * @return JsonResponse
     *
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function mergeSources(Route $route, StoreContract $store, Request $request): JsonResponse
    {
        $resourceType = $route->resourceType();

        $this->validate($request, RuleHelper::arrayHash('data', [
            'target'  => ['required', 'uuid'],
            'destroy' => ['required', 'boolean'],
        ], ['required']));

        /** @var Sourceable&IsSourceable $model */
        $model = $store
            ->queryOne($resourceType, $route->modelOrResourceId())
            ->first();

        $this->authorize('mergeSources', $model);

        /** @var Sourceable&IsSourceable $target */
        $target = $store
            ->queryOne($resourceType, $request->input('data.target'))
            ->first();

        if (! $target) {
            throw new AuthorizationException();
        }

        $this->authorize('update', $target);

        $model->sources()->update([
            'sourceable_type' => $target->getMorphClass(),
            'sourceable_id'   => $target->getKey(),
        ]);

        $destroy = $request->input('data.destroy');

        if (method_exists($this, 'onMergedSources')) {
            $this->onMergedSources($model, $target, $destroy);
        }

        if ($destroy) {
            $model->delete();
        }

        return new JsonResponse('', Response::HTTP_NO_CONTENT);
    }

    /**
     * Merge review from source into target.
     *
     * @param Reviewable $source
     * @param Reviewable $target
     *
     * @return void
     */
    private function mergeReviews(Reviewable $source, Reviewable $target): void
    {
        $review = new Review();

        $source->reviews()
            ->whereNotExists(
                fn(Builder $query) => $query->select(DB::raw('1'))
                    ->from($review->getTable(), 'other_reviews')
                    ->where(
                        'other_reviews.' . $review->reviewable()->getForeignKeyName(),
                        $target->getKey(),
                    )
                    ->where(
                        'other_reviews.' . $review->reviewable()->getMorphType(),
                        $target->getMorphClass(),
                    )
                    ->whereColumn(
                        'other_reviews.' . $review->ownerUser()->getForeignKeyName(),
                        $review->getTable() . '.' . $review->ownerUser()->getForeignKeyName(),
                    )
                    ->limit(1)
            )
            ->update([
                $review->reviewable()->getForeignKeyName() => $target->getKey(),
                $review->reviewable()->getMorphType()      => $target->getMorphClass(),
            ]);
    }
}
