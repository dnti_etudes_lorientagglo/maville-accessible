<?php

namespace App\Http\Controllers\JsonApi\Concerns;

use LaravelJsonApi\Contracts\Routing\Route;
use LaravelJsonApi\Contracts\Store\Store as StoreContract;
use LaravelJsonApi\Laravel\Http\Requests\ResourceQuery;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Trait ResolvesRouteModels.
 */
trait ResolvesRouteModels
{
    /**
     * Resolve the model corresponding to an action route.
     *
     * @param Route         $route
     * @param StoreContract $store
     *
     * @return array
     */
    private function resolveOneModel(Route $route, StoreContract $store): array
    {
        $request = ResourceQuery::queryOne(
            $resourceType = $route->resourceType()
        );

        $model = $store
            ->queryOne($resourceType, $route->modelOrResourceId())
            ->withRequest($request)
            ->first();
        if (! $model) {
            throw new NotFoundHttpException();
        }

        return [$model, $request];
    }
}
