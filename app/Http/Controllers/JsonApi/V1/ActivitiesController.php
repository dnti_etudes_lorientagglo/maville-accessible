<?php

namespace App\Http\Controllers\JsonApi\V1;

use App\Http\Controllers\JsonApi\Actions\PinnableActions;
use App\Http\Controllers\JsonApi\Actions\PublishableActions;
use App\Http\Controllers\JsonApi\Actions\SourceableActions;
use App\Models\Activity;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;

/**
 * Class ActivitiesController.
 */
class ActivitiesController extends JsonApiController
{
    use PinnableActions;
    use PublishableActions;
    use SourceableActions;

    /**
     * Hook to run when sources were merged successfully.
     *
     * @param Activity $source
     * @param Activity $target
     * @param bool     $destroy
     *
     * @return void
     */
    public function onMergedSources(Activity $source, Activity $target, bool $destroy): void
    {
        if (! $destroy) {
            return;
        }

        $this->mergeReviews($source, $target);
    }
}
