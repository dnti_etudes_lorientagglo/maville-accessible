<?php

namespace App\Http\Controllers\JsonApi\V1\Services;

use App\Helpers\LocaleHelper;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Http\JsonResponse;

/**
 * Class TranslationsController.
 */
class TranslationsController extends Controller
{
    /**
     * TranslationsController constructor.
     *
     * @param Translator $translator
     */
    public function __construct(private readonly Translator $translator)
    {
    }

    /**
     * Get all translations of app.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $this->getTranslations();

        return new JsonResponse([
            'data' => $this->getTranslations(),
        ]);
    }

    /**
     * Get all translations (with easy read specific translations if defined).
     *
     * @return array
     */
    private function getTranslations(): array
    {
        if (LocaleHelper::preferEasyRead()) {
            $easyReadLocale = LocaleHelper::easyRead(LocaleHelper::serverLocale());
            $easyReadTranslations = $this->translator->get('*', [], $easyReadLocale);
            if (is_array($easyReadTranslations)) {
                return array_replace_recursive(
                    $this->translator->get('*'),
                    $this->translator->get('*', [], LocaleHelper::easyRead(LocaleHelper::serverLocale())),
                );
            }
        }

        return $this->translator->get('*');
    }
}
