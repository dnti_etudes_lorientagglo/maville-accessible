<?php

namespace App\Http\Controllers\JsonApi\V1\Services;

use App\Http\Controllers\Controller;
use App\Models\Enums\AccessibilityCriteria;
use Illuminate\Http\JsonResponse;

/**
 * Class AccessibilityController.
 */
class AccessibilityController extends Controller
{
    /**
     * Get all accessibility criteria.
     *
     * @return JsonResponse
     */
    public function criteria(): JsonResponse
    {
        return new JsonResponse([
            'data' => AccessibilityCriteria::all()->toArray(),
        ]);
    }
}
