<?php

namespace App\Http\Controllers\JsonApi\V1\Services;

use App\Helpers\AuthHelper;
use App\Http\Controllers\Controller;
use App\Models\NewsletterSubscription;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class NewslettersController.
 */
class NewslettersController extends Controller
{
    /**
     * Check if an email address is subscribed.
     *
     * @param string $email
     *
     * @return JsonResponse
     *
     * @throws ValidationException
     */
    public function read(string $email): JsonResponse
    {
        return new JsonResponse([
            'data' => [
                'subscribedAt' => $this->findSendingSubscription($email)?->created_at?->toAtomString(),
            ],
        ]);
    }

    /**
     * Subscribe an email address to the newsletter.
     *
     * @param string $email
     *
     * @return JsonResponse
     *
     * @throws ValidationException
     */
    public function subscribe(string $email): JsonResponse
    {
        if ($this->findSendingSubscription($email)) {
            throw ValidationException::withMessages([
                'email' => trans('validation.unique', [':email']),
            ])->status(JsonResponse::HTTP_CONFLICT);
        }

        $sendingSubscription = new NewsletterSubscription();
        $sendingSubscription->email = $email;
        $sendingSubscription->save();

        return new JsonResponse('', JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * Unsubscribe an email address from the newsletter.
     *
     * @param string $email
     *
     * @return JsonResponse
     *
     * @throws AuthenticationException
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function unsubscribe(string $email): JsonResponse
    {
        $user = AuthHelper::user();
        if (! $user) {
            throw new AuthenticationException();
        }

        if ($user->email !== $email) {
            throw new AuthorizationException();
        }

        $sendingSubscription = $this->findSendingSubscription($email);
        if (! $sendingSubscription) {
            throw new NotFoundHttpException();
        }

        $sendingSubscription->delete();

        return new JsonResponse('', JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * Check if an email is subscribed.
     *
     * @param string $email
     *
     * @return NewsletterSubscription | null
     *
     * @throws ValidationException
     */
    private function findSendingSubscription(string $email): NewsletterSubscription | null
    {
        Validator::validate(['email' => $email], [
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);

        return NewsletterSubscription::query()
            ->where('email', $email)
            ->first();
    }
}
