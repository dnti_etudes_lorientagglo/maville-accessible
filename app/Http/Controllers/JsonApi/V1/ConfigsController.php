<?php

namespace App\Http\Controllers\JsonApi\V1;

use App\Helpers\RuleHelper;
use App\Http\Controllers\JsonApi\Concerns\ResolvesRouteModels;
use App\Models\Config;
use App\Policies\ConfigPolicy;
use App\Services\App\AppConfig;
use App\Services\External\InfoLocaleService;
use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use LaravelJsonApi\Contracts\Routing\Route;
use LaravelJsonApi\Contracts\Store\Store as StoreContract;
use LaravelJsonApi\Core\Responses\DataResponse;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;
use Throwable;

/**
 * Class ConfigsController.
 */
class ConfigsController extends JsonApiController
{
    use ResolvesRouteModels;

    /**
     * ConfigsController constructor.
     *
     * @param AppConfig $appConfig
     */
    public function __construct(private readonly AppConfig $appConfig)
    {
    }

    /**
     * Configure Facebook Login service.
     *
     * @param Route         $route
     * @param StoreContract $store
     * @param Request       $request
     *
     * @return DataResponse
     *
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function activateServiceFacebookLogin(
        Route $route,
        StoreContract $store,
        Request $request,
    ): DataResponse {
        /** @var Config $model */
        [$config] = $this->resolveOneModel($route, $store);

        $this->authorize(ConfigPolicy::UPDATE, $config);

        $data = $this->validate($request, RuleHelper::arrayHash('data', [
            'clientId'     => ['required', 'string'],
            'clientSecret' => ['required', 'string'],
        ], ['required']))['data'];

        $this->appConfig->updateService($config, 'facebookLogin', $data);

        return DataResponse::make($config);
    }

    /**
     * Configure Google Login service.
     *
     * @param Route         $route
     * @param StoreContract $store
     * @param Request       $request
     *
     * @return DataResponse
     *
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function activateServiceGoogleLogin(
        Route $route,
        StoreContract $store,
        Request $request,
    ): DataResponse {
        /** @var Config $model */
        [$config] = $this->resolveOneModel($route, $store);

        $this->authorize(ConfigPolicy::UPDATE, $config);

        $data = $this->validate($request, RuleHelper::arrayHash('data', [
            'clientId'     => ['required', 'string'],
            'clientSecret' => ['required', 'string'],
        ], ['required']))['data'];

        $this->appConfig->updateService($config, 'googleLogin', $data);

        return DataResponse::make($config);
    }

    /**
     * Configure France Connect Login service.
     *
     * @param Route         $route
     * @param StoreContract $store
     * @param Request       $request
     *
     * @return DataResponse
     *
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function activateServiceFranceConnectLogin(
        Route $route,
        StoreContract $store,
        Request $request,
    ): DataResponse {
        /** @var Config $model */
        [$config] = $this->resolveOneModel($route, $store);

        $this->authorize(ConfigPolicy::UPDATE, $config);

        $data = $this->validate($request, RuleHelper::arrayHash('data', [
            'clientId'     => ['required', 'string'],
            'clientSecret' => ['required', 'string'],
        ], ['required']))['data'];

        $this->appConfig->updateService($config, 'franceConnectLogin', $data);

        return DataResponse::make($config);
    }

    /**
     * Configure France Connect Login service.
     *
     * @param Route         $route
     * @param StoreContract $store
     * @param Request       $request
     *
     * @return DataResponse
     *
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function activateServiceHitineraire(
        Route $route,
        StoreContract $store,
        Request $request,
    ): DataResponse {
        /** @var Config $model */
        [$config] = $this->resolveOneModel($route, $store);

        $this->authorize(ConfigPolicy::UPDATE, $config);

        $data = $this->validate($request, RuleHelper::arrayHash('data', [
            'username' => ['required', 'string'],
            'password' => ['required', 'string'],
        ], ['required']))['data'];

        $this->appConfig->updateService($config, 'hitineraire', $data);

        return DataResponse::make($config);
    }

    /**
     * Configure Acceslibre service.
     *
     * @param Route         $route
     * @param StoreContract $store
     * @param Request       $request
     *
     * @return DataResponse
     *
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function activateServiceAcceslibre(
        Route $route,
        StoreContract $store,
        Request $request,
    ): DataResponse {
        /** @var Config $model */
        [$config] = $this->resolveOneModel($route, $store);

        $this->authorize(ConfigPolicy::UPDATE, $config);

        $data = $this->validate($request, RuleHelper::arrayHash('data', [
            'apiKey' => ['required', 'string'],
        ], ['required']))['data'];

        $this->appConfig->updateService($config, 'acceslibre', $data);

        return DataResponse::make($config);
    }

    /**
     * Configure Acceslibre service.
     *
     * @param Route         $route
     * @param StoreContract $store
     * @param Request       $request
     *
     * @return DataResponse
     *
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function updateServiceAcceslibre(
        Route $route,
        StoreContract $store,
        Request $request,
    ): DataResponse {
        /** @var Config $model */
        [$config] = $this->resolveOneModel($route, $store);

        $this->authorize(ConfigPolicy::UPDATE, $config);

        if (! Arr::get($config->services, 'acceslibre.activatedAt')) {
            throw new AuthorizationException();
        }

        $data = $this->validate($request, RuleHelper::arrayHash('data', [
            'sendData'           => ['required', 'boolean'],
            'draftNewCategories' => ['required', 'boolean'],
        ], ['required']))['data'];

        $this->appConfig->updateService($config, 'acceslibre', $data, true);

        return DataResponse::make($config);
    }

    /**
     * Configure InfoLocale service.
     *
     * @param Route             $route
     * @param StoreContract     $store
     * @param Request           $request
     * @param InfoLocaleService $infoLocaleCrawler
     *
     * @return DataResponse
     *
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function activateServiceInfolocale(
        Route $route,
        StoreContract $store,
        Request $request,
        InfoLocaleService $infoLocaleCrawler,
    ): DataResponse {
        /** @var Config $model */
        [$config] = $this->resolveOneModel($route, $store);

        $this->authorize(ConfigPolicy::UPDATE, $config);

        $this->validate($request, RuleHelper::arrayHash('data', [
            'email'    => ['required', 'string'],
            'password' => ['required', 'string'],
        ], ['required']));

        try {
            $data = $infoLocaleCrawler->authenticateWithLogin(
                $request->input('data.email'),
                $request->input('data.password'),
            );

            $this->appConfig->updateService($config, 'infolocale', $data);

            return DataResponse::make($config);
        } catch (Throwable $exception) {
            if ($exception instanceof RequestException) {
                throw ValidationException::withMessages([
                    'data.email' => trans('resources.configs.services.items.infolocale.errors.auth'),
                ]);
            }

            throw ValidationException::withMessages([
                'data.email' => trans('resources.configs.services.items.infolocale.errors.unknown'),
            ]);
        }
    }

    /**
     * Configure InfoLocale service.
     *
     * @param Route             $route
     * @param StoreContract     $store
     * @param Request           $request
     * @param InfoLocaleService $infoLocaleCrawler
     *
     * @return DataResponse
     *
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function updateServiceInfolocale(
        Route $route,
        StoreContract $store,
        Request $request,
        InfoLocaleService $infoLocaleCrawler,
    ): DataResponse {
        /** @var Config $model */
        [$config] = $this->resolveOneModel($route, $store);

        $this->authorize(ConfigPolicy::UPDATE, $config);

        if (! Arr::get($config->services, 'infolocale.activatedAt')) {
            throw new AuthorizationException();
        }

        $makeFlowRules = static fn(Closure $validate, string $error) => [
            'nullable',
            'string',
            'uuid',
            static function (string $attr, string $value, Closure $fail) use ($validate, $error) {
                if (! $validate($value)) {
                    $fail($error);
                }
            },
        ];

        $data = $this->validate($request, RuleHelper::arrayHash('data', [
            'draftNewCategories' => ['required', 'boolean'],
            'organizationsId'    => $makeFlowRules(
                static fn(string $id) => $infoLocaleCrawler->isOrganizationsFlow($id),
                trans('resources.configs.services.items.infolocale.errors.invalidOrganizationsId'),
            ),
            'publicationsId'     => $makeFlowRules(
                static fn(string $id) => $infoLocaleCrawler->isPublicationsFlow($id),
                trans('resources.configs.services.items.infolocale.errors.invalidPublicationsId'),
            ),
            'announcementsId'    => $makeFlowRules(
                static fn(string $id) => $infoLocaleCrawler->isAnnouncementsFlow($id),
                trans('resources.configs.services.items.infolocale.errors.invalidAnnouncementsId'),
            ),
        ], ['required']))['data'];

        $this->appConfig->updateService($config, 'infolocale', $data, true);

        return DataResponse::make($config);
    }

    /**
     * Configure DataGouv service.
     *
     * @param Route         $route
     * @param StoreContract $store
     * @param Request       $request
     *
     * @return DataResponse
     *
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function activateServiceDataGouv(
        Route $route,
        StoreContract $store,
        Request $request,
    ): DataResponse {
        /** @var Config $model */
        [$config] = $this->resolveOneModel($route, $store);

        $this->authorize(ConfigPolicy::UPDATE, $config);

        $data = $this->validate($request, RuleHelper::arrayHash('data', [
            'apiKey'         => ['required', 'string'],
            'organizationId' => ['required', 'string'],
        ], ['required']))['data'];

        $this->appConfig->updateService($config, 'datagouv', $data);

        return DataResponse::make($config);
    }

    /**
     * Configure Data Gouv service.
     *
     * @param Route         $route
     * @param StoreContract $store
     * @param Request       $request
     *
     * @return DataResponse
     *
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function updateServiceDataGouv(
        Route $route,
        StoreContract $store,
        Request $request,
    ): DataResponse {
        /** @var Config $model */
        [$config] = $this->resolveOneModel($route, $store);

        $this->authorize(ConfigPolicy::UPDATE, $config);

        if (! Arr::get($config->services, 'datagouv.activatedAt')) {
            throw new AuthorizationException();
        }

        $data = $this->validate($request, RuleHelper::arrayHash('data', [
            'placesDatasetId'     => ['nullable', 'string'],
            'activitiesDatasetId' => ['nullable', 'string'],
        ], ['required']))['data'];

        $this->appConfig->updateService($config, 'datagouv', $data, true);

        return DataResponse::make($config);
    }

    /**
     * Configure Acceslibre service.
     *
     * @param Route         $route
     * @param StoreContract $store
     * @param Request       $request
     *
     * @return DataResponse
     *
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function activateServiceMatomo(
        Route $route,
        StoreContract $store,
        Request $request,
    ): DataResponse {
        /** @var Config $model */
        [$config] = $this->resolveOneModel($route, $store);

        $this->authorize(ConfigPolicy::UPDATE, $config);

        $data = $this->validate($request, RuleHelper::arrayHash('data', [
            'host'               => ['required', 'string'],
            'siteId'             => ['required', 'string'],
            'trackerFileName'    => ['nullable', 'string'],
            'trackerScriptUrl'   => ['nullable', 'string'],
            'trackerEndpointUrl' => ['nullable', 'string'],
        ], ['required']))['data'];

        $this->appConfig->updateService($config, 'matomo', $data);

        return DataResponse::make($config);
    }

    /**
     * Remove a service.
     *
     * @param Route         $route
     * @param StoreContract $store
     * @param Request       $request
     *
     * @return DataResponse
     *
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function removeService(
        Route $route,
        StoreContract $store,
        Request $request,
    ): DataResponse {
        /** @var Config $model */
        [$config] = $this->resolveOneModel($route, $store);

        $this->authorize(ConfigPolicy::UPDATE, $config);

        $this->validate($request, RuleHelper::arrayHash('data', [
            'service' => [
                'required',
                Rule::in([
                    'hitineraire',
                    'acceslibre',
                    'infolocale',
                    'datagouv',
                    'matomo',
                    'facebookLogin',
                    'googleLogin',
                    'franceConnectLogin',
                ]),
            ],
        ], ['required']));

        $this->appConfig->updateService($config, $request->input('data.service'), null);

        return DataResponse::make($config);
    }
}
