<?php

namespace App\Http\Controllers\JsonApi\V1;

use App\Helpers\NotificationsHelper;
use App\Models\Contracts\Ownable;
use App\Models\OrganizationMember;
use App\Models\Review;
use App\Notifications\Reviews\NewReview;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;

/**
 * Class ReviewsController.
 */
class ReviewsController extends JsonApiController
{
    /**
     * Hook on request creation to notify owners of a new review.
     *
     * @param Review $review
     *
     * @return void
     */
    public function created(Review $review): void
    {
        $review->loadMissing(['reviewable']);
        $reviewable = $review->reviewable;

        if ($reviewable instanceof Ownable) {
            $reviewable->loadMissing([
                'ownerOrganization.membersOfOrganization.user',
                'ownerUser',
            ]);

            $userOwner = $reviewable->ownerUser;
            $organizationOwners = ($reviewable->ownerOrganization?->membersOfOrganization ?? collect())
                ->map(static fn(OrganizationMember $member) => $member->user);
            $owners = $organizationOwners->push($userOwner);

            NotificationsHelper::notify($owners, new NewReview($review), ignoreAuth: true);
        }
    }
}
