<?php

namespace App\Http\Controllers\JsonApi\V1;

use App\Helpers\NotificationsHelper;
use App\Models\Reaction;
use App\Models\Review;
use App\Notifications\Reactions\NewReaction;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;

/**
 * Class ReactionsController.
 */
class ReactionsController extends JsonApiController
{
    /**
     * Hook on request creation to notify owners of a new reaction.
     *
     * @param Reaction $reaction
     *
     * @return void
     */
    public function created(Reaction $reaction): void
    {
        $reaction->loadMissing(['reactable']);
        $reactable = $reaction->reactable;

        if ($reactable instanceof Review) {
            $reactable->loadMissing(['ownerUser']);

            NotificationsHelper::notify($reactable->ownerUser, new NewReaction($reaction), ignoreAuth: true);
        }
    }
}
