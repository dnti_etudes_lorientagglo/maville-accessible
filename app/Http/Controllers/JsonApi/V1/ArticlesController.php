<?php

namespace App\Http\Controllers\JsonApi\V1;

use App\Http\Controllers\JsonApi\Actions\PinnableActions;
use App\Http\Controllers\JsonApi\Actions\PublishableActions;
use App\Http\Controllers\JsonApi\Actions\SourceableActions;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;

/**
 * Class ArticlesController.
 */
class ArticlesController extends JsonApiController
{
    use PinnableActions;
    use PublishableActions;
    use SourceableActions;
}
