<?php

namespace App\Http\Controllers\JsonApi\V1;

use App\Helpers\AuthHelper;
use App\Models\Notification;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\JsonResponse;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;

/**
 * Class NotificationsController.
 */
class NotificationsController extends JsonApiController
{
    /**
     * Mark all user's notifications as read.
     *
     * @return JsonResponse
     *
     * @throws AuthenticationException
     */
    public function readAll(): JsonResponse
    {
        $user = AuthHelper::user();
        if (! $user) {
            throw new AuthenticationException();
        }

        Notification::query()
            ->whereMorphedTo('notifiable', $user)
            ->whereNull('read_at')
            ->update(['read_at' => now()]);

        return new JsonResponse('', JsonResponse::HTTP_NO_CONTENT);
    }
}
