<?php

namespace App\Http\Controllers\JsonApi\V1;

use App\Helpers\AuthHelper;
use App\Helpers\NotificationsHelper;
use App\Http\Controllers\JsonApi\Concerns\ResolvesRouteModels;
use App\Models\OrganizationInvitation;
use App\Models\OrganizationMember;
use App\Models\User;
use App\Notifications\Auth\NewInvitation;
use App\Policies\OrganizationInvitationPolicy;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use LaravelJsonApi\Contracts\Routing\Route;
use LaravelJsonApi\Contracts\Store\Store as StoreContract;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class OrganizationInvitationsController.
 */
class OrganizationInvitationsController extends JsonApiController
{
    use ResolvesRouteModels;

    /**
     * Hook on invitation creation to notify email.
     *
     * @param OrganizationInvitation $invitation
     *
     * @return void
     */
    public function created(OrganizationInvitation $invitation): void
    {
        $user = User::query()->where('email', $invitation->email)->first();
        if ($user) {
            NotificationsHelper::notify($user, new NewInvitation($invitation));
        } else {
            Notification::route('mail', $invitation->email)
                ->notify(new NewInvitation($invitation));
        }
    }

    /**
     * Accept an invitation.
     *
     * @param Route         $route
     * @param StoreContract $store
     *
     * @return JsonResponse
     *
     * @throws AuthorizationException
     */
    public function accept(Route $route, StoreContract $store): JsonResponse
    {
        /** @var OrganizationInvitation $model */
        [$model] = $this->resolveOneModel($route, $store);

        $this->authorize(OrganizationInvitationPolicy::ACCEPT, $model);

        $user = AuthHelper::user();

        $model->loadMissing(['role', 'organization']);

        DB::transaction(function () use ($user, $model) {
            $model->organization->membersOfOrganization()
                ->where('user_id', $user->id)
                ->delete();

            $model->delete();

            $member = new OrganizationMember();
            $member->user_id = $user->id;
            $member->role_id = $model->role_id;
            $member->organization_id = $model->organization_id;
            $member->save();
        });

        return new JsonResponse('', Response::HTTP_NO_CONTENT);
    }

    /**
     * Refuse an invitation.
     *
     * @param Route         $route
     * @param StoreContract $store
     *
     * @return JsonResponse
     *
     * @throws AuthorizationException
     */
    public function refuse(Route $route, StoreContract $store): JsonResponse
    {
        /** @var OrganizationInvitation $model */
        [$model] = $this->resolveOneModel($route, $store);

        $this->authorize(OrganizationInvitationPolicy::REFUSE, $model);

        $model->delete();

        return new JsonResponse('', Response::HTTP_NO_CONTENT);
    }
}
