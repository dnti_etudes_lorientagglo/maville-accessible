<?php

namespace App\Http\Controllers\JsonApi\V1;

use App\Http\Controllers\JsonApi\Actions\PublishableActions;
use App\Http\Controllers\JsonApi\Actions\SourceableActions;
use App\Models\Category;
use App\Models\Pivots\CategorizableCategory;
use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;

/**
 * Class CategoriesController.
 */
class CategoriesController extends JsonApiController
{
    use PublishableActions;
    use SourceableActions;

    /**
     * Hook on category save to clean up categories.
     *
     * @param Category $category
     *
     * @return void
     */
    public function saved(Category $category): void
    {
        $category->loadMissing(['parents', 'children']);

        $category->parents->each(static fn(Category $c) => $c->parents()->detach());
        $category->children->each(static fn(Category $c) => $c->children()->detach());
    }

    /**
     * Hook to run when sources were merged successfully.
     *
     * @param Category $source
     * @param Category $target
     * @param bool     $destroy
     *
     * @return void
     */
    protected function onMergedSources(Category $source, Category $target, bool $destroy): void
    {
        if (! $destroy) {
            return;
        }

        $categorizable = new CategorizableCategory();

        $source->categorizables()
            ->whereNotExists(
                fn(Builder $query) => $query->select(DB::raw('1'))
                    ->from($categorizable->getTable(), 'other_categorizables')
                    ->where(
                        'other_categorizables.' . $categorizable->category()->getForeignKeyName(),
                        $target->getKey(),
                    )
                    ->whereColumn(
                        'other_categorizables.' . $categorizable->categorizable()->getForeignKeyName(),
                        $categorizable->getTable() . '.' . $categorizable->categorizable()->getForeignKeyName(),
                    )
                    ->whereColumn(
                        'other_categorizables.' . $categorizable->categorizable()->getMorphType(),
                        $categorizable->getTable() . '.' . $categorizable->categorizable()->getMorphType(),
                    )
                    ->limit(1),
            )
            ->update([
                $categorizable->category()->getForeignKeyName() => $target->getKey(),
            ]);
    }
}
