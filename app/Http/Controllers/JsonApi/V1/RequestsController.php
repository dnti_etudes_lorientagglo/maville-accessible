<?php

namespace App\Http\Controllers\JsonApi\V1;

use App\Helpers\LocaleHelper;
use App\Helpers\NotificationsHelper;
use App\Helpers\RuleHelper;
use App\Http\Controllers\JsonApi\Concerns\ResolvesRouteModels;
use App\Models\Activity;
use App\Models\Contracts\OrganizationOwnable;
use App\Models\Enums\OrganizationTypeCode;
use App\Models\Enums\RequestTypeCode;
use App\Models\Enums\RoleName;
use App\Models\Organization;
use App\Models\OrganizationMember;
use App\Models\OrganizationType;
use App\Models\Place;
use App\Models\Request;
use App\Models\Role;
use App\Models\User;
use App\Notifications\Requests\NewPendingRequest;
use App\Notifications\Requests\NewRefusedRequest;
use App\Notifications\Requests\NewValidatedRequest;
use App\Policies\RequestPolicy;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use InvalidArgumentException;
use LaravelJsonApi\Contracts\Routing\Route;
use LaravelJsonApi\Contracts\Store\Store as StoreContract;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;
use RuntimeException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RequestsController.
 */
class RequestsController extends JsonApiController
{
    use ResolvesRouteModels;

    /**
     * Accept and apply a request.
     *
     * @param Route         $route
     * @param StoreContract $store
     *
     * @return JsonResponse
     *
     * @throws AuthorizationException
     */
    public function accept(Route $route, StoreContract $store): JsonResponse
    {
        /** @var Request $model */
        [$model] = $this->resolveOneModel($route, $store);

        $model->loadMissing(['ownerUser']);

        $this->authorize(RequestPolicy::ACCEPT, $model);

        DB::transaction(function () use ($model) {
            $this->acceptRequest($model);

            $model->accepted_at = $model->deleted_at = now();
            $model->save();
        });
        $model->ownerUser->notify(new NewValidatedRequest($model));

        return new JsonResponse('', Response::HTTP_NO_CONTENT);
    }

    /**
     * Accept and apply a request.
     *
     * @param Route         $route
     * @param StoreContract $store
     * @param HttpRequest   $request
     *
     * @return JsonResponse
     *
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function refuse(Route $route, StoreContract $store, HttpRequest $request): JsonResponse
    {
        /** @var Request $model */
        [$model] = $this->resolveOneModel($route, $store);

        $model->loadMissing(['ownerUser']);

        $this->authorize(RequestPolicy::REFUSE, $model);

        $this->validate($request, RuleHelper::arrayHash('data', [
            'reason' => ['required', 'string', 'max:500'],
        ], ['required']));

        $model->deleted_at = now();
        $model->save();

        $model->ownerUser->notify(new NewRefusedRequest($request->input('data.reason')));

        return new JsonResponse('', Response::HTTP_NO_CONTENT);
    }

    /**
     * Hook on request creation to notify admins of a new request.
     *
     * @param Request $request
     *
     * @return void
     */
    public function created(Request $request): void
    {
        NotificationsHelper::notify(
            $this->newPendingRequestNotifiedUsers($request),
            new NewPendingRequest($request),
            ignoreAuth: true,
        );
    }

    /**
     * Get the users to notify on new pending request.
     *
     * @param Request $request
     *
     * @return Collection
     */
    private function newPendingRequestNotifiedUsers(Request $request): Collection
    {
        $request->loadMissing(['requestType']);

        if ($request->requestType->code === RequestTypeCode::CLAIM->value) {
            return User::query()->role([RoleName::CONTENT_ADMIN->value])->get();
        }

        if ($request->requestType->code === RequestTypeCode::JOIN->value) {
            $request->loadMissing(['requestable']);

            /** @var Organization|null $organization */
            $organization = $request->requestable;
            if ($organization) {
                $adminRole = Role::findByName(RoleName::ORGANIZATION_ADMIN->value);

                $members = $organization->membersOfOrganization()
                    ->with(['user'])
                    ->where('role_id', $adminRole->id)
                    ->get();
                if ($members->isNotEmpty()) {
                    return $members->map(fn(OrganizationMember $m) => $m->user);
                }
            }

            return User::query()->role([RoleName::CONTENT_ADMIN->value])->get();
        }

        throw new RuntimeException('invalid request type given');
    }

    /**
     * Accept a request depending on the request type.
     *
     * @param Request $request
     *
     * @return void
     */
    private function acceptRequest(Request $request): void
    {
        match ($request->requestType->code) {
            RequestTypeCode::CLAIM->value => $this->acceptClaimRequest($request),
            RequestTypeCode::JOIN->value => $this->acceptJoinRequest($request),
            default => throw new InvalidArgumentException('invalid request type given'),
        };
    }

    /**
     * Accept a "become organization admin" request.
     *
     * @param Request $request
     *
     * @return void
     */
    private function acceptClaimRequest(Request $request): void
    {
        $organization = $this->defineClaimRequestOrganization($request);

        $request->resultable()->associate($organization);
    }

    /**
     * Accept a "join or create organization" request.
     *
     * @param Request $request
     *
     * @return void
     */
    private function acceptJoinRequest(Request $request): void
    {
        $request->loadMissing(['requestable']);

        $organization = $request->requestable ?? Organization::query()->forceCreate([
            'name'                 => [LocaleHelper::defaultLocale() => $request->data['name']],
            'organization_type_id' => OrganizationType::findIdByCode($request->data['type']),
        ]);

        $this->defineOrganizationMember($organization, $request->ownerUser);

        $request->requestable()->associate($organization);
    }

    /**
     * Get claim request related organization or create it.
     *
     * @param Request $request
     *
     * @return Organization
     */
    private function defineClaimRequestOrganization(Request $request): Organization
    {
        $request->loadMissing(['requestable', 'resultable']);

        /** @var Place|Activity $requestable */
        $requestable = $request->requestable;

        $organization = $request->resultable;
        if ($organization) {
            $this->defineOwningOrganization($requestable, $organization);

            return $organization;
        }

        $requestable->loadMissing(['ownerOrganization']);

        $organization = $requestable->ownerOrganization;
        $user = $request->ownerUser;
        if ($organization) {
            $this->defineOrganizationMember($organization, $user);

            return $organization;
        }

        $organization = new Organization();
        $organization->name = $requestable->name;
        $organization->organizationType()->associate(
            OrganizationType::findByCode(OrganizationTypeCode::COMPANY),
        );
        $organization->published_at = now();
        $organization->save();

        $requestable->loadMissing(['categories']);

        $organization->categories()->sync($requestable->categories);

        $this->defineOrganizationMember($organization, $user);
        $this->defineOwningOrganization($requestable, $organization);

        return $organization;
    }

    /**
     * Define the owning organization.
     *
     * @param OrganizationOwnable $ownable
     * @param Organization        $organization
     *
     * @return void
     */
    private function defineOwningOrganization(OrganizationOwnable $ownable, Organization $organization): void
    {
        $ownable->ownerOrganization()->associate($organization);
        $ownable->save();
    }

    /**
     * Add user to an organization.
     * When already member of it, does nothing.
     * When organization does not have an admin, set user admin, otherwise
     * user will be set as a member.
     *
     * @param Organization $organization
     * @param User         $user
     *
     * @return void
     */
    private function defineOrganizationMember(Organization $organization, User $user): void
    {
        $organization->loadMissing(['membersOfOrganization']);

        $members = $organization->membersOfOrganization;

        if ($members->where('user_id', $user->id)->isNotEmpty()) {
            return;
        }

        $adminRole = Role::findByName(RoleName::ORGANIZATION_ADMIN->value);
        $newRole = $members->where('role_id', $adminRole->id)->isNotEmpty()
            ? Role::findByName(RoleName::ORGANIZATION_MEMBER->value)
            : $adminRole;

        $newMember = new OrganizationMember();
        $newMember->organization()->associate($organization);
        $newMember->user()->associate($user);
        $newMember->role()->associate($newRole);
        $newMember->save();
    }
}
