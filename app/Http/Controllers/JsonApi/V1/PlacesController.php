<?php

namespace App\Http\Controllers\JsonApi\V1;

use App\Http\Controllers\JsonApi\Actions\PublishableActions;
use App\Http\Controllers\JsonApi\Actions\SourceableActions;
use App\Models\Activity;
use App\Models\Article;
use App\Models\Place;
use App\Services\External\ExternalServicesManager;
use Illuminate\Support\Facades\DB;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;

/**
 * Class PlacesController.
 */
class PlacesController extends JsonApiController
{
    use PublishableActions;
    use SourceableActions;

    /**
     * PlacesController constructor.
     *
     * @param ExternalServicesManager $externalServicesManager
     */
    public function __construct(
        private readonly ExternalServicesManager $externalServicesManager,
    ) {
    }

    /**
     * Hook on place save to send updates to external services.
     *
     * @param Place $place
     *
     * @return void
     */
    protected function saved(Place $place): void
    {
        $this->externalServicesManager->dispatchSend($place);
    }

    /**
     * Hook on place publication to send updates to external services.
     *
     * @param Place $place
     *
     * @return void
     */
    protected function onPublished(Place $place): void
    {
        $this->externalServicesManager->dispatchSend($place);
    }

    /**
     * Hook to run when sources were merged successfully.
     *
     * @param Place $source
     * @param Place $target
     * @param bool  $destroy
     *
     * @return void
     */
    protected function onMergedSources(Place $source, Place $target, bool $destroy): void
    {
        if (! $destroy) {
            return;
        }

        $this->mergeReviews($source, $target);

        Activity::query()
            ->where('place_id', $source->id)
            ->update([
                'place_id' => $target->id,
            ]);

        $localizableRelation = (new Article())->places();
        DB::table($localizableRelation->getTable())
            ->where($localizableRelation->getRelatedPivotKeyName(), $source->id)
            ->update([
                $localizableRelation->getRelatedPivotKeyName() => $target->id,
            ]);
    }
}
