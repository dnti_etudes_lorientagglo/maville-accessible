<?php

namespace App\Http\Controllers\JsonApi\V1;

use App\Http\Controllers\JsonApi\Actions\PublishableActions;
use App\Http\Controllers\JsonApi\Actions\SourceableActions;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;

/**
 * Class OrganizationsController.
 */
class OrganizationsController extends JsonApiController
{
    use PublishableActions;
    use SourceableActions;
}
