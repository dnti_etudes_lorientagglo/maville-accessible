<?php

namespace App\Http\Controllers\JsonApi\V1;

use App\Helpers\NotificationsHelper;
use App\Models\ReviewReply;
use App\Notifications\Reviews\NewReviewReply;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;

/**
 * Class ReviewRepliesController.
 */
class ReviewRepliesController extends JsonApiController
{
    /**
     * Hook on request reply creation to notify author of review.
     *
     * @param ReviewReply $reviewReply
     *
     * @return void
     */
    public function created(ReviewReply $reviewReply): void
    {
        $reviewReply->loadMissing(['review.ownerUser']);

        if ($reviewReply->review->ownerUser) {
            NotificationsHelper::notify($reviewReply->review->ownerUser, new NewReviewReply($reviewReply));
        }
    }
}
