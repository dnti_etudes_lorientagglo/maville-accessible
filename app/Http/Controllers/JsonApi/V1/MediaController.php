<?php

namespace App\Http\Controllers\JsonApi\V1;

use App\Helpers\AuthHelper;
use App\Helpers\MediaHelper;
use App\Http\Requests\Media\MediaConfirmClientSideUploadRequest;
use App\Http\Requests\Media\MediaPrepareClientSideUploadRequest;
use App\Http\Requests\Media\MediaUploadRequest;
use App\Models\Enums\FileVisibility;
use App\Models\Media;
use App\Models\User;
use App\Policies\MediaPolicy;
use App\Services\Media\Concerns\UploadingFile;
use App\Services\Media\Contracts\MediaStrategy;
use App\Services\Media\MediaService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use LaravelJsonApi\Core\Responses\DataResponse;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class MediaController.
 */
class MediaController extends JsonApiController
{
    /**
     * @var MediaStrategy
     */
    private readonly MediaStrategy $mediaStrategy;

    /**
     * MediaController constructor.
     *
     * @param MediaService $mediaService
     */
    public function __construct(private readonly MediaService $mediaService)
    {
        $this->mediaStrategy = $this->mediaService->strategyForMediaLibrary();
    }

    /**
     * Hook on media delete to prevent deleting in use media.
     *
     * @param Media $media
     *
     * @return void
     */
    public function deleting(Media $media): void
    {
        if ($this->mediaService->isMediaUsed($media)) {
            throw new HttpException(Response::HTTP_CONFLICT);
        }
    }

    /**
     * Upload new media.
     *
     * @param MediaUploadRequest $request
     *
     * @return DataResponse
     */
    public function upload(MediaUploadRequest $request): DataResponse
    {
        if ($this->mediaStrategy->supportsClientUploads()) {
            throw new NotFoundHttpException();
        }

        $user = AuthHelper::user();

        $files = $this->extractFilesFromRequest($request);
        $mediaByHash = $this->fetchMediaFromFiles($user, $files);

        $media = new EloquentCollection($files->map(
            fn(UploadingFile $file) => $this->findMatchingMedia($mediaByHash, $file)
                ?? $this->mediaService->createMediaForServerSideUpload($user, User::MEDIA_PUBLIC_COLLECTION, $file),
        ));

        $media->loadMissing('model');

        return (new DataResponse($media))->withIncludePaths('owner');
    }

    /**
     * Pre-sign new media upload.
     *
     * @param MediaPrepareClientSideUploadRequest $request
     *
     * @return JsonResponse
     */
    public function prepareClientSideUpload(MediaPrepareClientSideUploadRequest $request): JsonResponse
    {
        if (! $this->mediaStrategy->supportsClientUploads()) {
            throw new NotFoundHttpException();
        }

        $user = AuthHelper::user();

        $files = $this->extractFilesFromRequest($request);

        $maxFileSize = MediaHelper::maxFileSize();
        if ($files->some(fn(UploadingFile $file) => $file->size > $maxFileSize)) {
            throw new HttpException(Response::HTTP_REQUEST_ENTITY_TOO_LARGE);
        }

        $mediaByHash = $this->fetchMediaFromFiles($user, $files);

        return new JsonResponse([
            'data' => $files->mapWithKeys(fn(UploadingFile $file) => [
                $file->hash => $this->prepareClientSideUploadData($mediaByHash, $user, $file),
            ]),
        ], Response::HTTP_CREATED);
    }

    /**
     * Confirm pre-uploading media operation success.
     *
     * @param MediaConfirmClientSideUploadRequest $request
     *
     * @return JsonResponse
     *
     * @throws AuthorizationException
     */
    public function confirmClientSideUpload(MediaConfirmClientSideUploadRequest $request): JsonResponse
    {
        if (! $this->mediaStrategy->supportsClientUploads()) {
            throw new NotFoundHttpException();
        }

        $user = AuthHelper::user();
        $mediaIds = new Collection($request->input('data.media'));
        $media = Media::query()
            ->whereNull('size')
            ->where('collection_name', User::MEDIA_PUBLIC_COLLECTION)
            ->whereIn('id', $mediaIds)
            ->get();
        if (
            $mediaIds->count() !== $media->count()
            || $media->some(fn(Media $media) => ! $user->can(MediaPolicy::UPDATE, $media))
        ) {
            throw new AuthorizationException();
        }

        $media->each(fn(Media $media) => $this->mediaService->confirmMediaClientSideUpload($media));

        return new JsonResponse('', Response::HTTP_NO_CONTENT);
    }

    /**
     * Extract files data from a media request.
     *
     * @param FormRequest $request
     *
     * @return Collection
     */
    private function extractFilesFromRequest(FormRequest $request): Collection
    {
        return collect(Arr::get($request->all(), 'data.files'))
            ->map(static function (array $file) {
                /** @var UploadedFile|null $uploadedFile */
                $uploadedFile = $file['file'] ?? null;
                $hash = $uploadedFile
                    ? MediaHelper::computeHashForFile($uploadedFile->getRealPath())
                    : $file['hash'];
                $name = $uploadedFile
                    ? $uploadedFile->getClientOriginalName()
                    : $file['name'];
                $mimeType = $uploadedFile
                    ? $uploadedFile->getClientMimeType()
                    : $file['mimeType'];
                $size = $uploadedFile
                    ? ($uploadedFile->getSize() || 0)
                    : intval($file['size']);

                return new UploadingFile(
                    $hash,
                    $name,
                    FileVisibility::from($file['visibility']),
                    $size,
                    $mimeType,
                    $uploadedFile,
                );
            })
            ->values();
    }

    /**
     * Fetch already existing files from media.
     *
     * @param User       $user
     * @param Collection $files
     *
     * @return EloquentCollection
     */
    private function fetchMediaFromFiles(User $user, Collection $files): Collection
    {
        return $user->media()
            ->whereNotNull('size')
            ->where('collection_name', User::MEDIA_PUBLIC_COLLECTION)
            ->whereIn('custom_properties->hash', $files->map(static fn(UploadingFile $f) => $f->hash))
            ->get()
            ->keyBy(static fn(Media $media) => $media->hash);
    }

    /**
     * Find the matching media (if existing) for a given uploading file.
     *
     * @param Collection    $media
     * @param UploadingFile $file
     *
     * @return Media|null
     */
    private function findMatchingMedia(Collection $media, UploadingFile $file): Media|null
    {
        /** @var Media|null $existingMedia */
        $existingMedia = $media->get($file->hash);
        if ($existingMedia && $existingMedia->visibility === $file->visibility) {
            return $existingMedia;
        }

        return null;
    }

    /**
     * Prepare a client side upload data for file.
     *
     * @param Collection    $media
     * @param User          $user
     * @param UploadingFile $file
     *
     * @return array
     */
    private function prepareClientSideUploadData(Collection $media, User $user, UploadingFile $file): array
    {
        $existingMedia = $this->findMatchingMedia($media, $file);
        if ($existingMedia) {
            return [
                'type' => 'media',
                'data' => ['media' => $existingMedia->id],
            ];
        }

        $media = $this->mediaService->createMediaForClientSideUpload(
            $user,
            User::MEDIA_PUBLIC_COLLECTION,
            $file,
        );

        return [
            'type' => 'action',
            'data' => [
                'media'  => $media->id,
                'action' => $this->mediaStrategy->prepareClientSideUploadAction($media),
            ],
        ];
    }
}
