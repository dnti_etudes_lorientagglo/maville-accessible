<?php

namespace App\Http\Controllers\JsonApi\V1;

use App\Http\Controllers\JsonApi\Concerns\ResolvesRouteModels;
use App\Models\User;
use App\Policies\UserPolicy;
use App\Services\Auth\UsersService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use LaravelJsonApi\Contracts\Routing\Route;
use LaravelJsonApi\Contracts\Store\Store as StoreContract;
use LaravelJsonApi\Core\Responses\DataResponse;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UsersController.
 */
class UsersController extends JsonApiController
{
    use ResolvesRouteModels;

    /**
     * UsersController constructor.
     *
     * @param UsersService $usersService
     */
    public function __construct(private readonly UsersService $usersService)
    {
    }

    /**
     * Verify a user email manually.
     *
     * @param Route         $route
     * @param StoreContract $store
     *
     * @return JsonResponse
     *
     * @throws AuthorizationException
     */
    public function verify(Route $route, StoreContract $store): JsonResponse
    {
        /** @var User $user */
        [$user] = $this->resolveOneModel($route, $store);

        $this->authorize(UserPolicy::VERIFY, $user);

        $user->markEmailAsVerified();

        return new JsonResponse('', Response::HTTP_NO_CONTENT);
    }

    /**
     * Block a user.
     *
     * @param Route         $route
     * @param StoreContract $store
     *
     * @return JsonResponse
     *
     * @throws AuthorizationException
     */
    public function block(Route $route, StoreContract $store): JsonResponse
    {
        /** @var User $user */
        [$user] = $this->resolveOneModel($route, $store);

        $this->authorize(UserPolicy::BLOCK, $user);

        $user->blocked_at = now();
        $user->save();

        return new JsonResponse('', Response::HTTP_NO_CONTENT);
    }

    /**
     * Unblock a user.
     *
     * @param Route         $route
     * @param StoreContract $store
     *
     * @return DataResponse
     *
     * @throws AuthorizationException
     */
    public function unblock(Route $route, StoreContract $store): DataResponse
    {
        /** @var User $user */
        [$user, $request] = $this->resolveOneModel($route, $store);

        $this->authorize(UserPolicy::UNBLOCK, $user);

        $user->blocked_at = null;
        $user->save();

        return DataResponse::make($user)->withQueryParameters($request);
    }

    /**
     * Schedule the user deletion to next month.
     *
     * @param Route         $route
     * @param StoreContract $store
     *
     * @return JsonResponse
     *
     * @throws AuthorizationException
     */
    public function scheduleDeletion(Route $route, StoreContract $store): JsonResponse
    {
        /** @var User $user */
        [$user] = $this->resolveOneModel($route, $store);

        $this->authorize(UserPolicy::SCHEDULE_DELETION, $user);

        $this->usersService->scheduleDeletionAt($user, now());

        return new JsonResponse('', JsonResponse::HTTP_NO_CONTENT);
    }
}
