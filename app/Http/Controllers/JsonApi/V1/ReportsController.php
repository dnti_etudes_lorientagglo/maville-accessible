<?php

namespace App\Http\Controllers\JsonApi\V1;

use App\Helpers\NotificationsHelper;
use App\Models\Enums\RoleName;
use App\Models\Report;
use App\Models\User;
use App\Notifications\Reports\NewPendingReport;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;

/**
 * Class ReportsController.
 */
class ReportsController extends JsonApiController
{
    /**
     * Hook on report creation to notify moderators of a new report.
     *
     * @param Report $report
     *
     * @return void
     */
    public function created(Report $report): void
    {
        $users = User::query()->role([RoleName::MODERATOR->value])->get();

        NotificationsHelper::notify($users, new NewPendingReport($report), ignoreAuth: true);
    }
}
