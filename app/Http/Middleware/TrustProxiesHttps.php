<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Config\Repository as ConfigRepository;
use Illuminate\Http\Request;

/**
 * Class TrustProxiesHttps.
 */
class TrustProxiesHttps
{
    /**
     * TrustProxiesHttps constructor.
     *
     * @param ConfigRepository $config
     */
    public function __construct(private readonly ConfigRepository $config)
    {
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        if ($this->config->get('app.trust_proxies_https') && $request->isFromTrustedProxy()) {
            $request->server->set('HTTPS', 'ON');
            $request->server->set('SERVER_PORT', '443');
        }

        return $next($request);
    }
}
