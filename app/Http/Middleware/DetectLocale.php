<?php

namespace App\Http\Middleware;

use App\Helpers\AuthHelper;
use App\Helpers\LocaleHelper;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

/**
 * Class DetectLocale.
 */
class DetectLocale
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        $locale = LocaleHelper::guessLocale($request->getLanguages());
        if ($locale) {
            App::setLocale($locale);

            $user = AuthHelper::user();
            if ($user && $user->locale !== $locale) {
                $user->locale = $locale;
                $user->save();
            }
        }

        return $next($request);
    }
}
