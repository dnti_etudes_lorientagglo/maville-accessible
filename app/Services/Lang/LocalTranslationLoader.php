<?php

namespace App\Services\Lang;

use App\Helpers\JsonHelper;
use Illuminate\Contracts\Cache\Repository as CacheRepository;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Filesystem\FilesystemManager;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Spatie\TranslationLoader\TranslationLoaders\TranslationLoader;

/**
 * Class LocalTranslationLoader.
 */
class LocalTranslationLoader implements TranslationLoader
{
    /**
     * @var bool
     */
    private readonly bool $shouldCache;

    /**
     * @var Filesystem
     */
    private readonly Filesystem $filesystem;

    /**
     * LocalTranslationLoader constructor.
     *
     * @param CacheRepository   $cache
     * @param Application       $application
     * @param FilesystemManager $manager
     */
    public function __construct(
        private readonly CacheRepository $cache,
        Application $application,
        FilesystemManager $manager,
    ) {
        $this->shouldCache = ! $application->environment('local') && ! $application->environment('testing');
        $this->filesystem = $manager->disk('lang');
    }

    /**
     * Get the cache key for the given locale.
     *
     * @param string $locale
     *
     * @return string
     */
    public static function cacheKey(string $locale): string
    {
        return "translations.$locale";
    }

    /**
     * {@inheritDoc}
     */
    public function loadTranslations(string $locale, string $group): array
    {
        $groups = $this->loadTranslationsByGroups($locale);
        if ($group === '*') {
            return $groups;
        }

        return $groups[$group] ?? [];
    }

    /**
     * Get (and cache if not in local environment) the translations lines
     * mapped by groups for a given locale.
     *
     * @param string $locale
     *
     * @return array
     */
    private function loadTranslationsByGroups(string $locale): array
    {
        if ($this->shouldCache) {
            return $this->cache->rememberForever(
                self::cacheKey($locale),
                fn() => $this->getTranslationsByGroups($locale),
            );
        }

        return $this->getTranslationsByGroups($locale);
    }

    /**
     * Get the translations lines mapped by groups for a given locale.
     *
     * @param string $locale
     *
     * @return array
     */
    private function getTranslationsByGroups(string $locale): array
    {
        return collect($this->filesystem->allFiles($locale))
            ->reduce(function (Collection $translations, string $file) use ($locale) {
                if (! Str::endsWith($file, '.json')) {
                    return $translations;
                }

                $nestedKeys = Str::of($file)
                    ->replaceFirst("$locale/", '')
                    ->replaceLast('.json', '')
                    ->explode('/');

                $group = $nestedKeys->shift();
                $groupTranslations = $translations->getOrPut($group, collect());

                return $translations->put($group, $groupTranslations->mergeRecursive(
                    $nestedKeys->reverse()->reduce(
                        static fn(array $nestedTranslations, string $key) => [$key => $nestedTranslations],
                        Arr::undot(JsonHelper::decode($this->filesystem->get($file))),
                    ),
                ));
            }, collect())
            ->toArray();
    }
}
