<?php

namespace App\Services\Sending;

use App\Jobs\Sending\SendingCampaignBatchRunJob;
use App\Models\Enums\SendingCampaignTypeCode;
use App\Models\SendingCampaign;
use App\Models\SendingCampaignBatch;
use App\Models\SendingCampaignType;
use App\Models\User;
use App\Services\Sending\Recipients\MailSendingRecipient;
use App\Services\Sending\Recipients\SendingRecipient;
use App\Services\Sending\Recipients\SendingRecipientStatus;
use Carbon\Carbon;
use Closure;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Log\LogManager;
use Illuminate\Support\Collection;
use RuntimeException;
use Throwable;

/**
 * Class SendingService.
 */
class SendingService
{
    /**
     * Recipients batch size when sending.
     */
    private const SENDING_CAMPAIGN_BATCH_SIZE = 500;

    /**
     * Sending batch new attempt delay in seconds.
     */
    private const SENDING_CAMPAIGN_BATCH_ATTEMPT_DELAY = 5 * 1000;

    /**
     * Sending batch max attempts before stopping.
     */
    private const SENDING_CAMPAIGN_BATCH_MAX_ATTEMPTS = 3;

    /**
     * SendingService constructor.
     *
     * @param Dispatcher $dispatcher
     * @param LogManager $log
     */
    public function __construct(
        private readonly Dispatcher $dispatcher,
        private readonly LogManager $log,
    ) {
    }

    /**
     * Dispatch a sending campaign.
     *
     * @param SendingCampaign $campaign
     *
     * @return void
     */
    public function dispatchCampaign(SendingCampaign $campaign): void
    {
        $campaign->dispatched_at = now();

        $batches = $campaign->campaignBatches()->get();
        if ($batches->isEmpty()) {
            $campaign->started_at = now();
            $campaign->finished_at = now();
            $campaign->save();

            return;
        }

        $campaign->save();

        $batches->each(fn(SendingCampaignBatch $batch) => $this->dispatchCampaignBatch(
            $batch,
            $batch->attempts + self::SENDING_CAMPAIGN_BATCH_MAX_ATTEMPTS,
        ));
    }

    /**
     * Render a campaign for recipient.
     *
     * @param SendingCampaign $campaign
     * @param string          $recipientID
     *
     * @return mixed
     */
    public function renderCampaign(SendingCampaign $campaign, string $recipientID): mixed
    {
        $recipient = match ($campaign->campaignType->code) {
            SendingCampaignTypeCode::MAIL->value => $this->findRecipient($campaign, ['email' => $recipientID]),
            default => throw new RuntimeException('Unsupported sending campaign type'),
        };

        return $recipient?->render($campaign);
    }

    /**
     * Preview a campaign for a user.
     *
     * @param SendingCampaign $campaign
     * @param User            $user
     *
     * @return mixed
     */
    public function previewCampaign(SendingCampaign $campaign, User $user): mixed
    {
        $recipient = match ($campaign->campaignType->code) {
            SendingCampaignTypeCode::MAIL->value => MailSendingRecipient::make(
                email: $user->email,
                name: $user->fullName,
                locale: $user->locale,
            ),
            default => throw new RuntimeException('Unsupported sending campaign type'),
        };

        return $recipient->render($campaign);
    }

    /**
     * Find a recipient in campaign from values.
     *
     * @param SendingCampaign $campaign
     * @param array           $values
     *
     * @return array|null
     */
    private function findRecipient(SendingCampaign $campaign, array $values): SendingRecipient | null
    {
        $recipients = collect(
            $campaign->campaignBatches()
                ->whereJsonContains('recipients', [$values])
                ->value('recipients'),
        );

        $attributes = $recipients->first(
            static fn(array $recipientData) => ! array_diff($values, $recipientData),
        );

        return $attributes ? $this->makeRecipient($campaign, $attributes) : null;
    }

    /**
     * Make a recipient instance from attributes.
     *
     * @param SendingCampaign $campaign
     * @param array           $attributes
     *
     * @return SendingRecipient
     */
    private function makeRecipient(SendingCampaign $campaign, array $attributes): SendingRecipient
    {
        return match ($campaign->campaignType->code) {
            SendingCampaignTypeCode::MAIL->value => MailSendingRecipient::makeFromAttributes($attributes),
            default => throw new RuntimeException('Unsupported sending campaign type'),
        };
    }

    /**
     * Dispatch a sending campaign batch.
     *
     * @param SendingCampaignBatch $batch
     * @param int                  $maxAttempts
     * @param Carbon|null          $delay
     *
     * @return void
     */
    public function dispatchCampaignBatch(
        SendingCampaignBatch $batch,
        int $maxAttempts,
        Carbon | null $delay = null,
    ): void {
        $job = new SendingCampaignBatchRunJob($batch, $maxAttempts);

        if ($delay) {
            $job->delay($delay);
        }

        $this->dispatcher->dispatch($job);
    }

    /**
     * Send a sending campaign batch.
     *
     * @param SendingCampaignBatch $batch
     * @param int                  $maxAttempts
     *
     * @return void
     */
    public function sendCampaignBatch(SendingCampaignBatch $batch, int $maxAttempts): void
    {
        $batch->refresh();
        if ($batch->running_at) {
            return;
        }

        $batch->attempts += 1;
        $batch->started_at = $batch->started_at ?? now();
        $batch->running_at = now();
        $batch->finished_at = null;
        $batch->failed_at = null;
        $batch->save();

        /** @var SendingCampaign $campaign */
        $campaign = $batch->campaign()->with('campaignType')->first();
        if (! $campaign->started_at) {
            $campaign->started_at = now();
            $campaign->save();
        }

        $recipients = collect($batch->recipients);
        $finished = true;
        try {
            $recipients = $recipients->map(
                fn(array $recipient) => $this->sendCampaignToRecipient($campaign, $recipient),
            );
            $failed = $recipients->some(
                static fn(SendingRecipient $recipient) => $recipient->status() === SendingRecipientStatus::FAILED,
            );
            $finished = ! $failed || $batch->attempts >= $maxAttempts;
        } catch (Throwable $exception) {
            $failed = true;
            $this->log->error((string) $exception);
        }

        $batch->recipients = $recipients->toArray();
        $batch->running_at = null;
        $batch->finished_at = $finished ? now() : null;
        $batch->failed_at = $failed ? now() : null;
        $batch->save();

        $campaignFinished = $campaign->campaignBatches()
            ->whereNull('finished_at')
            ->doesntExist();
        if ($campaignFinished) {
            $campaign->finished_at = now();
            $campaign->save();
        }

        if ($batch->failed_at && ! $batch->finished_at) {
            $this->dispatchCampaignBatch(
                $batch,
                $maxAttempts,
                now()->addSeconds(self::SENDING_CAMPAIGN_BATCH_ATTEMPT_DELAY),
            );
        }
    }

    /**
     * Send a campaign to recipient using attributes.
     *
     * @param SendingCampaign $campaign
     * @param array           $attributes
     *
     * @return SendingRecipient
     *
     * @throws Throwable
     */
    private function sendCampaignToRecipient(SendingCampaign $campaign, array $attributes): SendingRecipient
    {
        /** @var SendingRecipient|null $recipient */
        $recipient = null;
        try {
            $recipient = $this->makeRecipient($campaign, $attributes);

            if (
                $recipient->status() === SendingRecipientStatus::PENDING
                || $recipient->status() === SendingRecipientStatus::FAILED
            ) {
                $recipient->send($campaign);
            }

            $recipient->setStatus(SendingRecipientStatus::SUCCEEDED);
        } catch (Throwable $exception) {
            if ($recipient) {
                $recipient->setStatus(SendingRecipientStatus::FAILED, (string) $exception);
            } else {
                throw $exception;
            }
        }

        return $recipient;
    }

    /**
     * Create campaign from a query (query will be chunked to batches).
     *
     * @param SendingCampaignTypeCode $type
     * @param array                   $data
     * @param Builder                 $query
     * @param Closure                 $resolver
     * @param string|null             $id
     *
     * @return SendingCampaign|null
     */
    public function createCampaignFromQuery(
        SendingCampaignTypeCode $type,
        array $data,
        Builder $query,
        Closure $resolver,
        string | null $id = null,
    ): SendingCampaign | null {
        $campaign = $this->createCampaign($type, $data, $id);
        if ($campaign) {
            $query->chunk(
                self::SENDING_CAMPAIGN_BATCH_SIZE,
                fn(Collection $items) => $this->createCampaignBatchFromCollection($campaign, $items, $resolver),
            );
        }

        return $campaign;
    }

    /**
     * Make a campaign instance without saving or dispatching.
     *
     * @param SendingCampaignTypeCode $type
     * @param array                   $data
     * @param string|null             $id
     *
     * @return SendingCampaign
     */
    public function makeCampaign(
        SendingCampaignTypeCode $type,
        array $data,
        string | null $id = null,
    ): SendingCampaign {
        $campaign = new SendingCampaign();
        $campaign->campaignType()->associate(SendingCampaignType::findByCode($type));
        $campaign->data = array_merge([...$data], $id ? ['id' => $id] : []);

        return $campaign;
    }

    /**
     * Create a campaign.
     *
     * @param SendingCampaignTypeCode $type
     * @param array                   $data
     * @param string|null             $id
     *
     * @return SendingCampaign|null
     */
    private function createCampaign(
        SendingCampaignTypeCode $type,
        array $data,
        string | null $id,
    ): SendingCampaign | null {
        $campaignWithLockExists = $id && SendingCampaign::query()->where('data->id', $id)->exists();
        if ($campaignWithLockExists) {
            return null;
        }

        $campaign = $this->makeCampaign($type, $data, $id);
        $campaign->save();

        return $campaign;
    }

    /**
     * Create a campaign batch from a collection.
     *
     * @param SendingCampaign $campaign
     * @param Collection      $items
     * @param Closure         $resolver
     *
     * @return void
     */
    private function createCampaignBatchFromCollection(
        SendingCampaign $campaign,
        Collection $items,
        Closure $resolver,
    ): void {
        if ($items->isEmpty()) {
            return;
        }

        $batch = new SendingCampaignBatch();
        $batch->campaign()->associate($campaign);
        $batch->attempts = 0;
        $batch->recipients = $items
            ->map(static fn(mixed $item) => $resolver($item))
            ->filter()
            ->toArray();
        $batch->save();
    }
}
