<?php

namespace App\Services\Sending\Recipients;

/**
 * Enum SendingRecipientStatus.
 */
enum SendingRecipientStatus: string
{
    case PENDING = 'pending';
    case SUCCEEDED = 'succeeded';
    case FAILED = 'failed';
}
