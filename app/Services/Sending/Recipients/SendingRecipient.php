<?php

namespace App\Services\Sending\Recipients;

use App\Models\SendingCampaign;
use Illuminate\Contracts\Support\Arrayable;

/**
 * Class SendingRecipient.
 */
abstract class SendingRecipient implements Arrayable
{
    /**
     * Create recipient from attributes.
     *
     * @param array $attributes
     *
     * @return static
     */
    public static function makeFromAttributes(array $attributes): static
    {
        return new static([
            ...$attributes,
            'status' => $attributes['status'] ?? SendingRecipientStatus::PENDING->value,
        ]);
    }

    /**
     * SendingRecipient constructor.
     *
     * @param array $attributes
     */
    private function __construct(protected array $attributes)
    {
    }

    /**
     * Send the given campaign to recipient.
     *
     * @param SendingCampaign $campaign
     *
     * @return void
     */
    abstract public function send(SendingCampaign $campaign): void;

    /**
     * Render the given campaign for recipient.
     *
     * @param SendingCampaign $campaign
     *
     * @return mixed
     */
    abstract public function render(SendingCampaign $campaign): mixed;

    /**
     * Get recipient locale.
     *
     * @return string|null
     */
    public function locale(): string | null
    {
        return $this->attributes['locale'] ?? null;
    }

    /**
     * Get recipient status.
     *
     * @return SendingRecipientStatus
     */
    public function status(): SendingRecipientStatus
    {
        return SendingRecipientStatus::from($this->attributes['status']);
    }

    /**
     * Set recipient status.
     *
     * @param SendingRecipientStatus $status
     * @param string|null            $reason
     *
     * @return void
     */
    public function setStatus(SendingRecipientStatus $status, string | null $reason = null): void
    {
        $this->attributes['status'] = $status->value;
        if ($reason !== null) {
            $this->attributes['statusReason'] = $reason;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function toArray(): array
    {
        return $this->attributes;
    }
}
