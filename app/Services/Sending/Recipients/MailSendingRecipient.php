<?php

namespace App\Services\Sending\Recipients;

use App\Mail\SendingCampaignMail;
use App\Models\SendingCampaign;
use Illuminate\Support\Facades\Mail;

/**
 * Class MailSendingRecipient.
 */
class MailSendingRecipient extends SendingRecipient
{
    /**
     * Create mail recipient from given values.
     *
     * @param string      $email
     * @param string|null $name
     * @param string|null $locale
     * @param string|null $unsubscribe
     *
     * @return static
     */
    public static function make(
        string $email,
        string | null $name = null,
        string | null $locale = null,
        string | null $unsubscribe = null,
    ): static {
        return static::makeFromAttributes([
            'email'       => $email,
            'name'        => $name,
            'locale'      => $locale,
            'unsubscribe' => $unsubscribe,
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function send(SendingCampaign $campaign): void
    {
        $pendingMail = Mail::to($this->email(), $this->name());

        if ($this->locale()) {
            $pendingMail->locale($this->locale());
        }

        $pendingMail->send(new SendingCampaignMail($campaign, $this));
    }

    /**
     * {@inheritDoc}
     */
    public function render(SendingCampaign $campaign): mixed
    {
        return (new SendingCampaignMail($campaign, $this, browser: true))->render();
    }

    /**
     * Get recipient email.
     *
     * @return string
     */
    public function email(): string
    {
        return $this->attributes['email'];
    }

    /**
     * Get recipient name.
     *
     * @return string|null
     */
    public function name(): string | null
    {
        return $this->attributes['name'] ?? null;
    }

    /**
     * Get recipient unsubscribe link.
     *
     * @return string|null
     */
    public function unsubscribe(): string | null
    {
        return $this->attributes['unsubscribe'] ?? null;
    }
}
