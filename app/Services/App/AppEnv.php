<?php

namespace App\Services\App;

use App\Features\Features;
use App\Helpers\LocaleHelper;
use App\Models\Category;
use App\Models\Media;
use App\Services\Auth\SocialAuthentication\SocialAuthenticationManager;
use App\Services\Cache\CachedValue;
use App\Services\Cache\CachedValueFactory;
use App\Services\Media\MediaService;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Config\Repository as ConfigRepository;
use Illuminate\Contracts\Routing\UrlGenerator;
use stdClass;

/**
 * Class AppEnv.
 */
class AppEnv
{
    /**
     * @var CachedValue
     */
    private readonly CachedValue $cachedEnv;

    /**
     * AppEnv constructor.
     *
     * @param Guard                       $auth
     * @param ConfigRepository            $config
     * @param UrlGenerator                $urlGenerator
     * @param AppConfig                   $appConfig
     * @param MediaService                $mediaService
     * @param SocialAuthenticationManager $socialAuthenticationManager
     * @param CachedValueFactory          $cachedValueFactory
     */
    public function __construct(
        private readonly Guard $auth,
        private readonly ConfigRepository $config,
        private readonly UrlGenerator $urlGenerator,
        private readonly AppConfig $appConfig,
        private readonly MediaService $mediaService,
        private readonly SocialAuthenticationManager $socialAuthenticationManager,
        CachedValueFactory $cachedValueFactory,
    ) {
        $this->cachedEnv = $cachedValueFactory->make(
            'application-env',
            fn() => $this->makeCachedEnvValue(),
        );
    }

    /**
     * Get the public (frontend required) environment data (app name, etc.).
     *
     * @return array
     */
    public function publicEnv(): array
    {
        return array_merge_recursive($this->publicRealtimeEnv(), $this->cachedEnv->value());
    }

    /**
     * Forget the public env cache.
     *
     * @return void
     */
    public function forgetPublicEnvCache(): void
    {
        $this->appConfig->forgetConfigCache();
        $this->cachedEnv->forget();
    }

    /**
     * Get default map zoom level when not configured.
     *
     * @return int
     */
    public function defaultMapZoom(): int
    {
        return 12;
    }

    /**
     * Get default map center address array when not configured.
     *
     * @return array
     */
    public function defaultMapCenter(): array
    {
        return [
            'id'         => '56121',
            'type'       => 'municipality',
            'city'       => 'Lorient',
            'postalCode' => '56100',
            'inseeCode'  => '56121',
            'geometry'   => [
                'type'        => 'Point',
                'coordinates' => [-3.3702449, 47.7482524],
            ],
        ];
    }

    /**
     * Get the public env realtime keys.
     *
     * @return array
     */
    private function publicRealtimeEnv(): array
    {
        return [
            'auth'     => [
                'id' => $this->auth->id(),
            ],
            'services' => [
                'i18n' => [
                    'locale' => LocaleHelper::locale(),
                ],
            ],
        ];
    }

    /**
     * Get the public env cached keys.
     *
     * @return array
     */
    private function makeCachedEnvValue(): array
    {
        $config = $this->appConfig->value();
        $locales = LocaleHelper::locales();
        $mediaStrategy = $this->mediaService->strategyForMediaLibrary();
        $availableDirections = collect([
            'hitineraire' => $this->appConfig->service('hitineraire.username') ? [
                'endpoint' => $this->config->get('services.hitineraire.endpoint'),
                'username' => $this->appConfig->service('hitineraire.username'),
                'password' => $this->appConfig->service('hitineraire.password'),
            ] : null,
        ])->filter();

        return [
            'app'      => [
                'id'          => $config->id,
                'name'        => $this->config->get('app.name'),
                'territory'   => $this->config->get('app.territory'),
                'description' => $config->description,
                'url'         => $this->urlGenerator->to(''),
            ],
            'features' => Features::collectValues(),
            'services' => [
                'configurable' => $this->config->get('app.external_services'),
                'i18n'         => [
                    'defaultLocale'   => LocaleHelper::defaultLocale(),
                    'locales'         => $locales,
                    'easyReadLocales' => LocaleHelper::easyReadLocales(),
                    'countries'       => $this->config->get('app.countries'),
                ],
                'map'          => [
                    'zoom'                 => $config->map_zoom ?? $this->defaultMapZoom(),
                    'center'               => $config->map_center ?? $this->defaultMapCenter(),
                    'layers'               => $this->config->get('app.map.layers'),
                    'searchableCategories' => $config->mapSearchableCategories
                        ->map(fn(Category $category) => $this->categoryToArray($category)),
                    'defaultCategories'    => $config->mapDefaultCategories
                        ->map(fn(Category $category) => $this->categoryToArray($category)),
                    'directions'           => [
                        'implementation' => $availableDirections->keys()->first(),
                        'options'        => $availableDirections->first() ?? new stdClass(),
                    ],
                ],
                'files'        => [
                    'supportsClientUploads' => $mediaStrategy->supportsClientUploads(),
                    'supportsVisibility'    => $mediaStrategy->supportsVisibility(),
                ],
                'social'       => [
                    'providers' => array_keys(
                        $this->socialAuthenticationManager->availableStrategies(),
                    ),
                ],
                'trackers'     => [
                    'matomo' => [
                        'siteId'             => $this->appConfig->service('matomo.siteId'),
                        'host'               => $this->appConfig->service('matomo.host'),
                        'trackerFileName'    => $this->appConfig->service('matomo.trackerFileName'),
                        'trackerEndpointUrl' => $this->appConfig->service('matomo.trackerEndpointUrl'),
                        'trackerScriptUrl'   => $this->appConfig->service('matomo.trackerScriptUrl'),
                        'crossOrigin'        => null,
                        'debug'              => null,
                    ],
                ],
            ],
        ];
    }

    /**
     * Convert an env media to array.
     *
     * @param Media|null $media
     *
     * @return array|null
     */
    private function mediaToArray(Media | null $media): array | null
    {
        return $media ? [
            'mimeType'        => $media->mime_type,
            'originalURL'     => $media->original_url,
            'placeholderURL'  => $media->placeholder_url,
            'optimizedURL'    => $media->optimized_url,
            'optimizedSrcset' => $media->optimized_srcset,
        ] : null;
    }

    /**
     * Convert an env category to array.
     *
     * @param Category|null $category
     *
     * @return array|null
     */
    private function categoryToArray(Category | null $category): array | null
    {
        return $category ? [
            'id'          => $category->id,
            'name'        => $category->name,
            'description' => $category->description,
            'icon'        => $category->icon,
            'color'       => $category->color,
        ] : null;
    }
}
