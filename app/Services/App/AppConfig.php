<?php

namespace App\Services\App;

use App\Models\Config;
use App\Services\Cache\CachedValue;
use App\Services\Cache\CachedValueFactory;
use Illuminate\Contracts\Config\Repository as ConfigRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use RuntimeException;

/**
 * Class AppConfig.
 */
class AppConfig
{
    /**
     * @var CachedValue
     */
    private readonly CachedValue $config;

    /**
     * AppConfig constructor.
     *
     * @param ConfigRepository   $configRepository
     * @param CachedValueFactory $cachedValueFactory
     */
    public function __construct(
        private readonly ConfigRepository $configRepository,
        CachedValueFactory $cachedValueFactory,
    ) {
        $this->config = $cachedValueFactory->make(
            'application-config',
            static function () {
                $config = Config::query()
                    ->with([
                        'mapSearchableCategories',
                        'mapDefaultCategories',
                    ])
                    ->first();
                if (! $config) {
                    throw new RuntimeException(
                        'config DB instance not found, did you forget to run "php artisan db:seed"?',
                    );
                }

                return $config;
            },
        );
    }

    /**
     * Get the config cached object.
     *
     * @return Config
     */
    public function value(): Config
    {
        return $this->config->value();
    }

    /**
     * Check if a service is configurable.
     *
     * @param string $name
     *
     * @return bool
     */
    public function isServiceConfigurable(string $name): bool
    {
        $services = $this->configRepository->get('app.external_services');

        return empty($services) || in_array($name, $services);
    }

    /**
     * Get a service configuration value.
     *
     * @param string     $key
     * @param mixed|null $default
     *
     * @return mixed
     */
    public function service(string $key, mixed $default = null): mixed
    {
        return $this->isServiceConfigurable(Str::before($key, '.'))
            ? Arr::get($this->value()->services, $key, $default)
            : null;
    }

    /**
     * Update a service data on given configuration.
     *
     * @param Config     $config
     * @param string     $service
     * @param array|null $data
     * @param bool       $merge
     *
     * @return void
     */
    public function updateService(
        Config $config,
        string $service,
        array | null $data,
        bool $merge = false,
    ): void {
        $prevData = $merge
            ? (($config->services ?? [])[$service] ?? [])
            : [];

        $config->services = array_filter(
            [
                ...($config->services ?? []),
                $service => $data ? [
                    ...$prevData,
                    ...$data,
                    'activatedAt' => $data['activatedAt'] ?? now()->timestamp,
                ] : null,
            ],
            static fn($v) => $v !== null,
        );
        $config->save();
    }

    /**
     * Forget the public env cache.
     *
     * @return void
     */
    public function forgetConfigCache(): void
    {
        $this->config->forget();
    }
}
