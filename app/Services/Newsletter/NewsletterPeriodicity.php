<?php

namespace App\Services\Newsletter;

use App\Helpers\DateHelper;
use Carbon\Carbon;
use Carbon\CarbonInterface;
use Closure;
use Illuminate\Support\Collection;

/**
 * Class NewsletterPeriodicity.
 */
class NewsletterPeriodicity
{
    /**
     * Determine which day of the week is the start of the period.
     */
    private const PERIODICITY_START_WEEKDAY = CarbonInterface::MONDAY;

    private const PERIODICITY_WEEKLY = 'weekly';

    private const PERIODICITY_BIMONTHLY = 'bimonthly';

    private const PERIODICITY_MONTHLY = 'monthly';

    /**
     * Newsletter is sent every week.
     *
     * @return static
     */
    public static function weekly(): static
    {
        return new NewsletterPeriodicity(
            self::PERIODICITY_WEEKLY,
            7,
            static fn(Carbon $date) => [
                'date' => DateHelper::format($date, format: 'LL'),
            ],
            static fn(Carbon $date) => $date->weekday(CarbonInterface::MONDAY),
        );
    }

    /**
     * Newsletter is sent two times a month.
     *
     * @return static
     */
    public static function bimonthly(): static
    {
        return new NewsletterPeriodicity(
            self::PERIODICITY_BIMONTHLY,
            15,
            static fn(Carbon $date) => [
                'start' => DateHelper::format($date->clone()->subDays(15), format: 'LL'),
                'end'   => DateHelper::format($date, format: 'LL'),
            ],
            static function (Carbon $date) {
                $firstOfMonth = $date->clone()->firstOfMonth(CarbonInterface::MONDAY);
                $middleOfMonth = $firstOfMonth->clone()->addWeeks(2);

                return $date->diffInDays($firstOfMonth) < $date->diffInDays($middleOfMonth)
                    ? $firstOfMonth
                    : $middleOfMonth;
            },
        );
    }

    /**
     * Newsletter is sent every month.
     *
     * @return static
     */
    public static function monthly(): static
    {
        return new NewsletterPeriodicity(
            self::PERIODICITY_MONTHLY,
            30,
            static fn(Carbon $date) => [
                'date' => DateHelper::format($date, format: 'MMMM YYYY'),
            ],
            static fn(Carbon $date) => $date->firstOfMonth(CarbonInterface::MONDAY),
        );
    }

    /**
     * Get all available periodicity.
     *
     * @return Collection
     */
    public static function all(): Collection
    {
        return collect([
            self::weekly(),
            self::bimonthly(),
            self::monthly(),
        ])->keyBy('name');
    }

    /**
     * NewsletterPeriodicity constructor.
     *
     * @param string  $name
     * @param int     $duration
     * @param Closure $descriptionData
     * @param Closure $startOfPeriod
     */
    public function __construct(
        public readonly string $name,
        public readonly int $duration,
        private readonly Closure $descriptionData,
        private readonly Closure $startOfPeriod,
    ) {
    }

    /**
     * Compute the valid start of period for a given date.
     *
     * @param Carbon $date
     *
     * @return Carbon
     */
    public function validStartOfPeriod(Carbon $date): Carbon
    {
        return call_user_func_array($this->startOfPeriod, [$date->clone()]);
    }

    /**
     * Check if the given date is a valid start date for period.
     *
     * @param Carbon $date
     *
     * @return bool
     */
    public function isValidStartOfPeriod(Carbon $date): bool
    {
        return $date->isSameDay($this->validStartOfPeriod($date));
    }

    /**
     * Compute the newsletter description.
     *
     * @param Carbon $date
     *
     * @return string
     */
    public function computeDescription(Carbon $date): string
    {
        return trans('campaigns.newsletter.description.' . $this->name, [
            ...call_user_func_array($this->descriptionData, [$date]),
            'name' => config('app.name'),
        ]);
    }
}
