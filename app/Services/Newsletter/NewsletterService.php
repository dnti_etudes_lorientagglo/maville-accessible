<?php

namespace App\Services\Newsletter;

use App\Features\FeatureName;
use App\Features\Features;
use App\Helpers\LocaleHelper;
use App\Helpers\PlacesHelper;
use App\Helpers\PublishableHelper;
use App\Helpers\StrHelper;
use App\JsonApi\V1\Activities\QueriesActivitiesOpening;
use App\Models\Activity;
use App\Models\Article;
use App\Models\Enums\ActivityTypeCode;
use App\Models\Enums\SendingCampaignTypeCode;
use App\Models\NewsletterSubscription;
use App\Models\Place;
use App\Models\SendingCampaign;
use App\Services\Sending\Recipients\MailSendingRecipient;
use App\Services\Sending\SendingService;
use Carbon\Carbon;
use Closure;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

/**
 * Class NewsletterService.
 */
class NewsletterService
{
    use QueriesActivitiesOpening;

    /**
     * Prefix to use when creating newsletter campaign ID.
     * This is also used to compute the newsletter number for display.
     * This should not be changed.
     */
    private const NEWSLETTER_ID_PREFIX = 'newsletter:';

    /**
     * NewsletterService constructor.
     *
     * @param SendingService $sendingService
     */
    public function __construct(private readonly SendingService $sendingService)
    {
    }

    /**
     * Send a newsletter for date.
     *
     * @param NewsletterPeriodicity $periodicity
     * @param Carbon                $date
     *
     * @return SendingCampaign|null
     */
    public function sendNewsletter(NewsletterPeriodicity $periodicity, Carbon $date): SendingCampaign | null
    {
        $campaign = $this->createNewsletterSendingCampaign($periodicity, $date);
        if ($campaign) {
            $this->dispatchNewsletterSendingCampaign($campaign);
        }

        return $campaign;
    }

    /**
     * Create the newsletter sending campaign.
     *
     * @param NewsletterPeriodicity $periodicity
     * @param Carbon                $date
     *
     * @return SendingCampaign|null
     */
    private function createNewsletterSendingCampaign(
        NewsletterPeriodicity $periodicity,
        Carbon $date,
    ): SendingCampaign | null {
        [$empty, $data] = $this->makeNewsletterSendingCampaignData($periodicity, $date);
        if ($empty) {
            return null;
        }

        $query = NewsletterSubscription::query()->with('user');

        $resolver = static fn(NewsletterSubscription $subscription) => MailSendingRecipient::make(
            email: $subscription->email,
            name: $subscription->user?->fullName,
            locale: $subscription->user?->locale,
            unsubscribe: url('/account/newsletter/' . $subscription->email),
        );

        return $this->sendingService->createCampaignFromQuery(
            type: SendingCampaignTypeCode::MAIL,
            data: $data,
            query: $query,
            resolver: $resolver,
            id: self::NEWSLETTER_ID_PREFIX . $date->toDateString(),
        );
    }

    /**
     * Create a newsletter campaign data.
     *
     * @param NewsletterPeriodicity $periodicity
     * @param Carbon                $date
     *
     * @return array
     */
    private function makeNewsletterSendingCampaignData(NewsletterPeriodicity $periodicity, Carbon $date): array
    {
        $newsletterNumber = $this->fetchNewsletterNumber();

        $articles = $this->fetchNewsletterArticles($periodicity, $date);
        $events = $this->fetchNewsletterEvents($periodicity, $date);
        $activities = $this->fetchNewsletterActivities($periodicity, $date);
        $places = $this->fetchNewsletterPlaces($periodicity, $date);

        return [
            $articles->isEmpty() && $events->isEmpty() && $activities->isEmpty() && $places->isEmpty(),
            [
                'markdown' => 'campaigns.newsletter',
                'subject'  => LocaleHelper::mapWithLocales(static fn() => trans('campaigns.newsletter.title', [
                    'number' => $newsletterNumber,
                    'name'   => config('app.name'),
                ])),
                'data'     => [
                    'description' => LocaleHelper::mapWithLocales(
                        static fn() => $periodicity->computeDescription($date),
                    ),
                    'articles'    => $articles,
                    'events'      => $events,
                    'activities'  => $activities,
                    'places'      => $places,
                ],
            ],
        ];
    }

    /**
     * Fetch the next newsletter number.
     *
     * @return int
     */
    private function fetchNewsletterNumber(): int
    {
        $newslettersCount = SendingCampaign::query()
            ->where('data->id', 'like', self::NEWSLETTER_ID_PREFIX . '%')
            ->count();

        return $newslettersCount + 1;
    }

    /**
     * Fetch articles to display inside newsletter.
     *
     * @param NewsletterPeriodicity $periodicity
     * @param Carbon                $date
     *
     * @return Collection
     */
    private function fetchNewsletterArticles(NewsletterPeriodicity $periodicity, Carbon $date): Collection
    {
        if (! Features::check(FeatureName::ARTICLES)) {
            return new Collection();
        }

        return PublishableHelper::publishedQuery(Article::query())
            ->with('cover')
            ->whereNotNull('published_at')
            ->whereDate('published_at', '<=', $date)
            ->whereDate('published_at', '>=', $date->clone()->subDays($periodicity->duration))
            ->orderByDesc('published_at')
            ->limit(5)
            ->get()
            ->map(static fn(Article $article) => [
                'cover'       => $article->cover?->original_url,
                'name'        => $article->name,
                'description' => StrHelper::computeDescription($article->description, $article->body, 100),
                'url'         => url($article->getReadLink()),
            ]);
    }

    /**
     * Fetch events to display inside newsletter.
     *
     * @param NewsletterPeriodicity $periodicity
     * @param Carbon                $date
     *
     * @return Collection
     */
    private function fetchNewsletterEvents(NewsletterPeriodicity $periodicity, Carbon $date): Collection
    {
        return Features::check(FeatureName::EVENTS)
            ? $this->fetchNewsletterActivitiesOfType(
                $periodicity,
                $date,
                ActivityTypeCode::EVENTS,
                static fn(Builder $query) => $query->orderBy('activities_differences.next_opening_days_difference'),
            )
            : new Collection();
    }

    /**
     * Fetch activities to display inside newsletter.
     *
     * @param NewsletterPeriodicity $periodicity
     * @param Carbon                $date
     *
     * @return Collection
     */
    private function fetchNewsletterActivities(NewsletterPeriodicity $periodicity, Carbon $date): Collection
    {
        return Features::check(FeatureName::ACTIVITIES)
            ? $this->fetchNewsletterActivitiesOfType(
                $periodicity,
                $date,
                ActivityTypeCode::ACTIVITIES,
                static fn(Builder $query) => $query->orderByDesc('updated_at'),
            )
            : new Collection();
    }

    /**
     * Fetch activities of the matching code to display inside newsletter.
     *
     * @param NewsletterPeriodicity $periodicity
     * @param Carbon                $date
     * @param ActivityTypeCode      $code
     * @param Closure|null          $callback
     *
     * @return Collection
     */
    private function fetchNewsletterActivitiesOfType(
        NewsletterPeriodicity $periodicity,
        Carbon $date,
        ActivityTypeCode $code,
        Closure | null $callback = null,
    ): Collection {
        return $this->queryWithOpeningDaysDifference(PublishableHelper::publishedQuery(Activity::query()), $date)
            ->with(['activityType', 'cover'])
            ->whereRelation('activityType', 'code', $code->value)
            ->whereNotNull('published_at')
            ->where('activities_differences.next_opening_days_difference', '>=', 0)
            ->where('activities_differences.next_opening_days_difference', '<=', $periodicity->duration)
            ->when(static fn() => $callback, $callback)
            ->limit(5)
            ->get()
            ->map(static fn(Activity $activity) => [
                'cover'       => $activity->cover?->original_url,
                'name'        => $activity->name,
                'dates'       => LocaleHelper::mapWithLocales(static fn() => $activity->computeNextDatesTitle($date)),
                'description' => StrHelper::computeDescription($activity->description, $activity->body, 100),
                'url'         => url($activity->getReadLink()),
            ]);
    }

    /**
     * Fetch places to display inside newsletter.
     *
     * @param NewsletterPeriodicity $periodicity
     * @param Carbon                $date
     *
     * @return Collection
     */
    private function fetchNewsletterPlaces(NewsletterPeriodicity $periodicity, Carbon $date): Collection
    {
        if (! Features::check(FeatureName::PLACES)) {
            return new Collection();
        }

        return PublishableHelper::publishedQuery(Place::query())
            ->whereDate('created_at', '<=', $date)
            ->whereDate('created_at', '>=', $date->clone()->subDays($periodicity->duration))
            ->orderByDesc('created_at')
            ->limit(5)
            ->get()
            ->map(static fn(Place $place) => [
                'name'        => $place->name,
                'address'     => PlacesHelper::formatAddress($place->address),
                'description' => $place->description,
                'url'         => url($place->getReadLink()),
            ]);
    }

    /**
     * Dispatch the newsletter sending campaign.
     *
     * @param SendingCampaign $campaign
     *
     * @return void
     */
    private function dispatchNewsletterSendingCampaign(SendingCampaign $campaign): void
    {
        $this->sendingService->dispatchCampaign($campaign);
    }
}
