<?php

namespace App\Services\Media;

use App\Helpers\FilesHelper;
use App\Helpers\MediaHelper;
use App\Helpers\ReflectionHelper;
use App\Models\Config;
use App\Models\Contracts\Coverable;
use App\Models\Contracts\HasMedia;
use App\Models\Enums\FileVisibility;
use App\Models\Media;
use App\Services\Media\Concerns\UploadingFile;
use App\Services\Media\Contracts\MediaStrategy;
use App\Services\Media\Contracts\MediaStrategyFactory;
use Carbon\Carbon;
use Illuminate\Contracts\Config\Repository as ConfigRepository;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Filesystem\FilesystemManager;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Spatie\Image\Image;
use Spatie\MediaLibrary\Conversions\FileManipulator;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Spatie\MediaLibrary\MediaCollections\FileAdder;
use stdClass;
use Throwable;

/**
 * Class MediaService.
 */
class MediaService
{
    /**
     * @var array<string, MediaStrategyFactory> List of strategies factories mapped by disk driver.
     */
    private readonly array $strategyFactories;

    /**
     * MediaService constructor.
     *
     * @param ConfigRepository          $config
     * @param FilesystemManager         $filesystemManager
     * @param FileManipulator           $fileManipulator
     * @param LocalMediaStrategyFactory $localMediaStrategyFactory
     * @param S3MediaStrategyFactory    $s3MediaStrategyFactory
     */
    public function __construct(
        private readonly ConfigRepository $config,
        private readonly FilesystemManager $filesystemManager,
        private readonly FileManipulator $fileManipulator,
        LocalMediaStrategyFactory $localMediaStrategyFactory,
        S3MediaStrategyFactory $s3MediaStrategyFactory,
    ) {
        $this->strategyFactories = [
            'local' => $localMediaStrategyFactory,
            's3'    => $s3MediaStrategyFactory,
        ];
    }

    /**
     * Check if a media is used in any content.
     *
     * @param Media $media
     *
     * @return bool
     */
    public function isMediaUsed(Media $media): bool
    {
        $config = new Config();

        $tablesAndColumns = new Collection([
            ['imageables', 'media_id'],
            ['mediables', 'media_id'],
            ...ReflectionHelper::classesIn('Models')
                ->filter(static fn(string $class) => (
                    ReflectionHelper::classExtends($class, Model::class)
                    && ReflectionHelper::classImplements($class, Coverable::class)
                ))
                ->values()
                ->map(function (string $class) {
                    /** @var Coverable $model */
                    $model = new $class();
                    $relation = $model->cover();

                    return [$model->getTable(), $relation->getForeignKeyName()];
                }),
        ]);

        $createQuery = static fn(array $tableAndColumn) => DB::table($tableAndColumn[0])
            ->select($tableAndColumn[1] . ' as id');

        $query = $createQuery($tablesAndColumns->shift());
        $tablesAndColumns->each(
            static fn(array $tableAndColumn) => $query->unionAll($createQuery($tableAndColumn)),
        );

        return DB::table($query, 'all_media')
            ->where('all_media.id', $media->id)
            ->exists();
    }

    /**
     * Generate a URL to serve a media (temporary if expires at is given or file is private).
     *
     * @param Media       $media
     * @param Carbon|null $expiresAt
     *
     * @return string
     */
    public function generateServeURL(Media $media, Carbon | null $expiresAt = null): string
    {
        $mediaStrategy = $this->strategyForMedia($media);
        if (
            $mediaStrategy->supportsTemporaryURL()
            && ($expiresAt || $media->visibility !== FileVisibility::PUBLIC)
        ) {
            return $mediaStrategy->generateServeTemporaryURL(
                $media,
                $expiresAt ?? now()->addMinutes(15),
            );
        }

        return $mediaStrategy->generateServeURL($media);
    }

    /**
     * Create a media for a server side upload.
     *
     * @param Model&HasMedia $model
     * @param string         $collection
     * @param string         $path
     * @param string         $name
     * @param string         $hash
     * @param array          $properties
     *
     * @return Media
     *
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function createMediaForLocalPath(
        HasMedia&Model $model,
        string $collection,
        string $path,
        string $name,
        string $hash,
        array $properties = [],
    ): Media {
        $mimeType = FilesHelper::detectMimeType($path);
        $extension = FilesHelper::detectExtensionFromMimeType($mimeType);
        $filename = Str::of($name)
            ->ascii()
            ->kebab()
            ->when($extension)
            ->append('.' . $extension)
            ->value();

        return $this->prepareFileAdder($model->addMedia($path), FileVisibility::PUBLIC)
            ->setFileName($filename)
            ->setName($name)
            ->withCustomProperties([
                ...$properties,
                'hash'       => $hash,
                'visibility' => FileVisibility::PUBLIC->value,
                ...$this->customPropertiesFromPath($mimeType, $path),
            ])
            ->toMediaCollection($collection);
    }

    /**
     * Create a media for a server side upload.
     *
     * @param Model&HasMedia $model
     * @param string         $collection
     * @param UploadingFile  $file
     *
     * @return Media
     *
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function createMediaForServerSideUpload(
        HasMedia&Model $model,
        string $collection,
        UploadingFile $file,
    ): Media {
        return $this->prepareFileAdder($model->addMedia($file->file), $file->visibility)
            ->withCustomProperties([
                'hash'       => $file->hash,
                'visibility' => $file->visibility->value,
                ...$this->customPropertiesFromPath($file->mimeType, $file->file->getRealPath()),
            ])
            ->toMediaCollection($collection);
    }

    /**
     * Create a media for a preparing client side upload.
     *
     * @param HasMedia&Model $model
     * @param string         $collection
     * @param UploadingFile  $file
     *
     * @return Media
     */
    public function createMediaForClientSideUpload(
        HasMedia&Model $model,
        string $collection,
        UploadingFile $file,
    ): Media {
        $diskName = $this->diskForMediaLibrary();

        $media = new Media();
        $media->uuid = Str::uuid()->toString();
        $media->collection_name = $collection;
        $media->name = pathinfo($file->name, PATHINFO_FILENAME);
        $media->file_name = Str::of(FilesHelper::sanitizeFileName($file->name))
            ->kebab()
            ->replaceMatches('/-+/', '-')
            ->value();
        $media->mime_type = $file->mimeType;
        $media->disk = $diskName;
        $media->conversions_disk = $diskName;
        $media->size = null;
        $media->manipulations = [];
        $media->generated_conversions = new stdClass();
        $media->responsive_images = new stdClass();
        $media->custom_properties = [
            'hash'       => $file->hash,
            'visibility' => $file->visibility->value,
        ];
        $media->model()->associate($model);
        $media->save();

        return $media;
    }

    /**
     * Confirm a media client side upload.
     *
     * @param Media $media
     *
     * @return void
     */
    public function confirmMediaClientSideUpload(Media $media): void
    {
        $diskName = $this->diskForMediaLibrary();
        $disk = $this->filesystemManager->disk($diskName);
        $path = MediaHelper::computePath($media);
        if (! $disk->exists($path)) {
            throw new InvalidArgumentException('file does not exists');
        }

        $media->custom_properties = [
            ...($media->custom_properties ?? []),
            ...$this->customPropertiesFromMedia($media),
        ];
        $media->size = $disk->size($path);
        $media->save();

        $this->fileManipulator->createDerivedFiles($media, [], false, true);
    }

    /**
     * Get the strategy to use for media library files.
     *
     * @return MediaStrategy
     */
    public function strategyForMediaLibrary(): MediaStrategy
    {
        return $this->strategyForDisk($this->diskForMediaLibrary());
    }

    /**
     * Get the strategy to use for a given media.
     *
     * @param Media $media
     *
     * @return MediaStrategy
     */
    public function strategyForMedia(Media $media): MediaStrategy
    {
        return $this->strategyForDisk($this->diskForMedia($media));
    }

    /**
     * Prepare a file adder.
     *
     * @param FileAdder      $fileAdder
     * @param FileVisibility $visibility
     *
     * @return FileAdder
     */
    private function prepareFileAdder(FileAdder $fileAdder, FileVisibility $visibility): FileAdder
    {
        $mediaStrategy = $this->strategyForMediaLibrary();
        if ($mediaStrategy->supportsVisibility()) {
            $mediaStrategy->applyVisibilityToFileAdder($fileAdder, $visibility);
        }

        return $fileAdder;
    }

    /**
     * Get custom properties from mime type and path.
     *
     * @param string $mimeType
     * @param string $path
     *
     * @return array[]
     */
    private function customPropertiesFromPath(string $mimeType, string $path): array
    {
        if (FilesHelper::isImageMimeType($mimeType)) {
            try {
                $image = Image::load($path);

                return [
                    'width'  => $image->getWidth(),
                    'height' => $image->getHeight(),
                ];
            } catch (Throwable) {
                return [];
            }
        }

        return [];
    }

    /**
     * Get custom properties from media.
     *
     * @param Media $media
     *
     * @return array
     */
    private function customPropertiesFromMedia(Media $media): array
    {
        if (FilesHelper::isImageMimeType($media->mime_type)) {
            $diskName = $this->diskForMediaLibrary();
            $disk = $this->filesystemManager->disk($diskName);
            $path = MediaHelper::computePath($media);
            if (! $disk->exists($path)) {
                throw new InvalidArgumentException('media is not uploaded yet');
            }

            $content = $disk->get($path);
            $localPath = FilesHelper::temporaryFile($content);

            return $this->customPropertiesFromPath($media->mime_type, $localPath);
        }

        return [];
    }

    /**
     * Get disk to use for media library files.
     *
     * @return string
     */
    private function diskForMediaLibrary(): string
    {
        return $this->config->get('media-library.disk_name');
    }

    /**
     * Get disk to use for a given media.
     *
     * @param Media $media
     *
     * @return string
     */
    private function diskForMedia(Media $media): string
    {
        return $media->disk;
    }

    /**
     * Get the driver used by a given disk.
     *
     * @param string $disk
     *
     * @return string
     */
    private function driverForDisk(string $disk): string
    {
        $driver = $this->config->get("filesystems.disks.$disk.driver");
        if (! $driver) {
            throw new InvalidArgumentException("disk $disk does not seems to exist.");
        }

        return $driver;
    }

    /**
     * Get the strategy to use for a disk.
     *
     * @param string $disk
     *
     * @return MediaStrategy
     */
    private function strategyForDisk(string $disk): MediaStrategy
    {
        return $this->strategyForDriver(
            $this->driverForDisk($disk),
            $this->filesystemManager->disk($disk),
        );
    }

    /**
     * Get the strategy to use for a disk.
     *
     * @param string     $driver
     * @param Filesystem $disk
     *
     * @return MediaStrategy
     */
    private function strategyForDriver(string $driver, Filesystem $disk): MediaStrategy
    {
        if (isset($this->strategyFactories[$driver])) {
            return $this->strategyFactories[$driver]->makeStrategy($disk);
        }

        throw new InvalidArgumentException("driver $driver is not supported.");
    }
}
