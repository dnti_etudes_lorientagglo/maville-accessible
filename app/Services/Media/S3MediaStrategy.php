<?php

namespace App\Services\Media;

use App\Helpers\MediaHelper;
use App\Models\Enums\FileVisibility;
use App\Models\Media;
use App\Services\Media\Concerns\PreparedClientSideUploadAction;
use App\Services\Media\Contracts\MediaStrategy;
use Aws\S3\PostObjectV4;
use Aws\S3\S3Client;
use Carbon\Carbon;
use Illuminate\Filesystem\AwsS3V3Adapter;
use Spatie\MediaLibrary\MediaCollections\FileAdder;

/**
 * Class S3MediaStrategy.
 */
class S3MediaStrategy implements MediaStrategy
{
    /**
     * S3MediaStrategy constructor.
     *
     * @param AwsS3V3Adapter $disk
     */
    public function __construct(private readonly AwsS3V3Adapter $disk)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function supportsVisibility(): bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function supportsTemporaryURL(): bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function supportsClientUploads(): bool
    {
        return ! ($this->getConfig()['disable_client_upload'] ?? false);
    }

    /**
     * {@inheritDoc}
     */
    public function generateServeURL(Media $media): string
    {
        return $this->disk->url(MediaHelper::computePath($media));
    }

    /**
     * {@inheritDoc}
     */
    public function generateServeTemporaryURL(Media $media, Carbon $expiresAt): string
    {
        $bucket = $this->getConfig()['bucket'];
        $client = $this->getSigningS3Client();

        $command = $client->getCommand('GetObject', [
            'Bucket' => $bucket,
            'Key'    => MediaHelper::computePath($media),
        ]);

        return $client->createPresignedRequest($command, $expiresAt)->getUri();
    }

    /**
     * {@inheritDoc}
     */
    public function readAsStream(Media $media): mixed
    {
        return $this->disk->readStream(MediaHelper::computePath($media));
    }

    /**
     * {@inheritDoc}
     */
    public function applyVisibilityToFileAdder(FileAdder $fileAdder, FileVisibility $visibility): FileAdder
    {
        return $fileAdder->addCustomHeaders([
            'ACL' => $this->computeACL($visibility),
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function prepareClientSideUploadAction(Media $media): PreparedClientSideUploadAction
    {
        $bucket = $this->getConfig()['bucket'];
        $client = $this->getSigningS3Client();
        $acl = $this->computeACL($media->visibility);
        $path = MediaHelper::computePath($media);
        $expires = '+15 minutes';

        $cacheControlHeader = [];
        if ($media->visibility === FileVisibility::PUBLIC) {
            $cacheControlHeader['Cache-Control'] = 'public, max-age=31536000';
        }

        if ($this->getConfig()['use_client_upload_put'] ?? false) {
            $command = $client->getCommand('PutObject', [
                'Bucket'             => $bucket,
                'Key'                => MediaHelper::computePath($media),
                'ACL'                => $acl,
                'Content-Type'       => $media->mime_type,
                'ContentLengthRange' => 0 . '-' . MediaHelper::maxFileSize(),
            ]);

            return new PreparedClientSideUploadAction(
                method: 'PUT',
                endpoint: (string) $client->createPresignedRequest($command, $expires)->getUri(),
                field: null,
                inputs: [],
                headers: [...$cacheControlHeader, 'Content-Type' => $media->mime_type],
            );
        }

        $inputs = [
            'key'          => $path,
            'acl'          => $acl,
            'Content-Type' => $media->mime_type,
            ...$cacheControlHeader,
        ];

        $options = [
            ['acl' => $acl],
            ['bucket' => $bucket],
            ['eq', '$key', $path],
            ['eq', '$content-type', $media->mime_type],
            ['content-length-range', 0, MediaHelper::maxFileSize()],
            $cacheControlHeader,
        ];

        if ($media->visibility === FileVisibility::PUBLIC) {
            $inputs['Cache-Control'] = 'public, max-age=31536000';
            $options[] = ['Cache-Control' => 'public, max-age=31536000'];
        }

        $postObject = new PostObjectV4(
            $client,
            $bucket,
            $inputs,
            $options,
            $expires,
        );

        return new PreparedClientSideUploadAction(
            method: 'POST',
            endpoint: $postObject->getFormAttributes()['action'],
            field: 'file',
            inputs: $postObject->getFormInputs(),
            headers: [],
        );
    }

    /**
     * Get S3 ACL for given visibility.
     *
     * @param FileVisibility $visibility
     *
     * @return string
     */
    private function computeACL(FileVisibility $visibility): string
    {
        return match ($visibility) {
            FileVisibility::PUBLIC => 'public-read',
            FileVisibility::PRIVATE => 'private',
        };
    }

    /**
     * Get the disk configuration.
     *
     * @return array
     */
    private function getConfig(): array
    {
        return $this->disk->getConfig();
    }

    /**
     * Get the client used to generate pre-signed URLs and requests.
     *
     * @return S3Client
     */
    private function getSigningS3Client(): S3Client
    {
        $config = $this->getConfig();
        $clientEndpoint = $config['client_endpoint'] ?? null;
        if ($clientEndpoint) {
            return new S3Client([
                ...$config,
                'endpoint' => $clientEndpoint,
            ]);
        }

        return $this->disk->getClient();
    }
}
