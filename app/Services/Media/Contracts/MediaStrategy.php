<?php

namespace App\Services\Media\Contracts;

use App\Models\Enums\FileVisibility;
use App\Models\Media;
use App\Services\Media\Concerns\PreparedClientSideUploadAction;
use Carbon\Carbon;
use Spatie\MediaLibrary\MediaCollections\FileAdder;

/**
 * Interface MediaStrategy.
 */
interface MediaStrategy
{
    /**
     * Check if the strategy supports visibility option.
     * When not supporting, all files will be public.
     *
     * @return bool
     */
    public function supportsVisibility(): bool;

    /**
     * Check if the strategy supports temporary URLs generation.
     *
     * @return bool
     */
    public function supportsTemporaryURL(): bool;

    /**
     * Check if the strategy supports client side upload.
     *
     * @return bool
     */
    public function supportsClientUploads(): bool;

    /**
     * Apply the visibility contraint to a file adder.
     *
     * @param FileAdder      $fileAdder
     * @param FileVisibility $visibility
     *
     * @return FileAdder
     */
    public function applyVisibilityToFileAdder(FileAdder $fileAdder, FileVisibility $visibility): FileAdder;

    /**
     * Prepare a client side upload action for media.
     *
     * @param Media $media
     *
     * @return PreparedClientSideUploadAction
     */
    public function prepareClientSideUploadAction(Media $media): PreparedClientSideUploadAction;

    /**
     * Generate a URL to serve a media.
     *
     * @param Media $media
     *
     * @return string
     */
    public function generateServeURL(Media $media): string;

    /**
     * Generate a temporary URL to serve a media.
     *
     * @param Media  $media
     * @param Carbon $expiresAt
     *
     * @return string
     */
    public function generateServeTemporaryURL(Media $media, Carbon $expiresAt): string;

    /**
     * Read a media as a stream.
     *
     * @param Media $media
     *
     * @return resource
     */
    public function readAsStream(Media $media): mixed;
}
