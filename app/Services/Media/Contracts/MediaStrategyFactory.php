<?php

namespace App\Services\Media\Contracts;

use Illuminate\Contracts\Filesystem\Filesystem;

/**
 * Interface MediaStrategyFactory.
 */
interface MediaStrategyFactory
{
    /**
     * Make an instance of media strategy for disk.
     *
     * @param Filesystem $disk
     *
     * @return MediaStrategy
     */
    public function makeStrategy(Filesystem $disk): MediaStrategy;
}
