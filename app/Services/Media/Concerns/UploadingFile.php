<?php

namespace App\Services\Media\Concerns;

use App\Models\Enums\FileVisibility;
use Illuminate\Http\UploadedFile;

/**
 * Class UploadingFile.
 */
class UploadingFile
{
    /**
     * UploadingFile constructor.
     *
     * @param string            $hash
     * @param string            $name
     * @param FileVisibility    $visibility
     * @param int               $size
     * @param string            $mimeType
     * @param UploadedFile|null $file
     */
    public function __construct(
        public readonly string $hash,
        public readonly string $name,
        public readonly FileVisibility $visibility,
        public readonly int $size,
        public readonly string $mimeType,
        public readonly UploadedFile|null $file,
    ) {
    }
}
