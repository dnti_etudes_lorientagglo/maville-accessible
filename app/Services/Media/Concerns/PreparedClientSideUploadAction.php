<?php

namespace App\Services\Media\Concerns;

use Illuminate\Contracts\Support\Arrayable;

/**
 * Class PreparedClientSideUploadAction.
 */
class PreparedClientSideUploadAction implements Arrayable
{
    /**
     * PreparedClientSideUploadAction constructor.
     *
     * @param string      $method
     * @param string      $endpoint
     * @param string|null $field
     * @param array       $headers
     * @param array       $inputs
     */
    public function __construct(
        public readonly string $method,
        public readonly string $endpoint,
        public readonly string | null $field,
        public readonly array $inputs = [],
        public readonly array $headers = [],
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function toArray(): array
    {
        return [
            'method'   => $this->method,
            'endpoint' => $this->endpoint,
            'field'    => $this->field,
            'inputs'   => $this->inputs,
            'headers'  => $this->headers,
        ];
    }
}
