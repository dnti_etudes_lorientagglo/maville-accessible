<?php

namespace App\Services\Media;

use App\Services\Media\Contracts\MediaStrategy;
use App\Services\Media\Contracts\MediaStrategyFactory;
use Illuminate\Contracts\Filesystem\Filesystem;

/**
 * Class LocalMediaStrategyFactory.
 */
class LocalMediaStrategyFactory implements MediaStrategyFactory
{
    /**
     * {@inheritDoc}
     */
    public function makeStrategy(Filesystem $disk): MediaStrategy
    {
        return new LocalMediaStrategy($disk);
    }
}
