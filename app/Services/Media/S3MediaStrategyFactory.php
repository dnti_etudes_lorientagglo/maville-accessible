<?php

namespace App\Services\Media;

use App\Services\Media\Contracts\MediaStrategy;
use App\Services\Media\Contracts\MediaStrategyFactory;
use Illuminate\Contracts\Filesystem\Filesystem;

/**
 * Class S3MediaStrategyFactory.
 */
class S3MediaStrategyFactory implements MediaStrategyFactory
{
    /**
     * {@inheritDoc}
     */
    public function makeStrategy(Filesystem $disk): MediaStrategy
    {
        return new S3MediaStrategy($disk);
    }
}
