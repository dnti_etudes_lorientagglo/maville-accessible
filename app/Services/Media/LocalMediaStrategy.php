<?php

namespace App\Services\Media;

use App\Helpers\MediaHelper;
use App\Models\Enums\FileVisibility;
use App\Models\Media;
use App\Services\Media\Concerns\PreparedClientSideUploadAction;
use App\Services\Media\Contracts\MediaStrategy;
use Carbon\Carbon;
use Illuminate\Filesystem\FilesystemAdapter;
use RuntimeException;
use Spatie\MediaLibrary\MediaCollections\FileAdder;

/**
 * Class LocalMediaStrategy.
 */
class LocalMediaStrategy implements MediaStrategy
{
    /**
     * S3MediaStrategy constructor.
     *
     * @param FilesystemAdapter $disk
     */
    public function __construct(
        private readonly FilesystemAdapter $disk,
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function supportsVisibility(): bool
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function supportsTemporaryURL(): bool
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function supportsClientUploads(): bool
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function generateServeURL(Media $media): string
    {
        return $this->disk->url(MediaHelper::computePath($media));
    }

    /**
     * {@inheritDoc}
     */
    public function generateServeTemporaryURL(Media $media, Carbon $expiresAt): string
    {
        throw new RuntimeException('local media strategy does not support temporary URL');
    }

    /**
     * {@inheritDoc}
     */
    public function readAsStream(Media $media): mixed
    {
        return $this->disk->readStream(MediaHelper::computePath($media));
    }

    /**
     * {@inheritDoc}
     */
    public function applyVisibilityToFileAdder(FileAdder $fileAdder, FileVisibility $visibility): FileAdder
    {
        throw new RuntimeException('local media strategy does not support visibility constraints');
    }

    /**
     * {@inheritDoc}
     */
    public function prepareClientSideUploadAction(Media $media): PreparedClientSideUploadAction
    {
        throw new RuntimeException('local media strategy does not support signed uploads');
    }
}
