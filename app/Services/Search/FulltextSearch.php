<?php

namespace App\Services\Search;

use App\Helpers\LocaleHelper;
use App\Helpers\MigrationsHelper;
use App\Helpers\StrHelper;
use App\Models\Casts\TranslationObject;
use App\Models\Contracts\FulltextSearchable;
use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;

/**
 * Class FulltextSearch.
 */
class FulltextSearch
{
    /**
     * Mapping between locales and PostgreSQL fulltext search languages.
     */
    public const SEARCH_LANGUAGES = [
        'fr-FR' => 'french',
    ];

    /**
     * Highest search weight.
     */
    public const SEARCH_WEIGHT_HIGH = 'A';

    /**
     * Moderate search weight.
     */
    public const SEARCH_WEIGHT_MODERATE = 'B';

    /**
     * Low search weight.
     */
    public const SEARCH_WEIGHT_LOW = 'C';

    /**
     * The alias used in query for fulltext search score.
     */
    public const SEARCH_SCORE_ALIAS = 'search_score';

    /*
    |--------------------------------------------------------------------------
    | Search score computation, filtering and sorting.
    |--------------------------------------------------------------------------
    */

    /**
     * Filter the given query by the search term.
     *
     * @param EloquentBuilder $query
     * @param string          $search
     *
     * @return void
     */
    public function whereSearch(EloquentBuilder $query, string $search): void
    {
        $query
            ->from(function (Builder $baseFromQuery) use ($query, $search, &$minSearchScore) {
                $model = $query->getModel();

                $fromQuery = $model->registerGlobalScopes(
                    $model->newEloquentBuilder($baseFromQuery),
                )->setModel($model);

                $ids = $this->searchToIds($search);
                if ($ids) {
                    $fromQuery
                        ->select([])
                        ->selectRaw(implode(', ', [
                            $model->qualifyColumn('*'),
                            '1 AS ' . self::SEARCH_SCORE_ALIAS,
                        ]))
                        ->whereIn(
                            $model->getQualifiedKeyName(),
                            $ids,
                        );

                    return;
                }

                $language = $this->computeSearchTextLanguage(LocaleHelper::locale());
                $tsQuery = 'to_tsquery(\'' . $language . '\', ?)';
                $tsQueryValue = $this->searchToTsQuery($search);

                $fromQuery
                    ->select([])
                    ->selectRaw(implode(', ', [
                        $model->qualifyColumn('*'),
                        'ts_rank_cd(' . $model->qualifyColumn('search_text') . ', ' . $tsQuery . ', 32)'
                        . ' AS ' . self::SEARCH_SCORE_ALIAS,
                    ]), [$tsQueryValue])
                    ->whereRaw(
                        $model->qualifyColumn('search_text') . ' @@ ' . $tsQuery,
                        [$tsQueryValue],
                    );
            }, $query->getModel()->getTable());
    }

    /**
     * Order a filtered query by the search score.
     *
     * @param EloquentBuilder $query
     * @param string          $direction
     *
     * @return void
     */
    public function orderBySearch(EloquentBuilder $query, string $direction): void
    {
        $query->orderBy(self::SEARCH_SCORE_ALIAS, $direction === 'asc' ? 'desc' : 'asc');
    }

    /**
     * Create a IDs collection from search.
     *
     * @param string $search
     *
     * @return Collection|null
     */
    private function searchToIds(string $search): Collection | null
    {
        $uuidWords = collect(preg_split('/\s+/', $search));
        if ($uuidWords->isNotEmpty() && $uuidWords->every(static fn(string $word) => Str::isUuid($word))) {
            return $uuidWords;
        }

        return null;
    }

    /**
     * Convert a search string to valid TS query.
     *
     * @param string $search
     *
     * @return string
     */
    private function searchToTsQuery(string $search): string
    {
        $words = Str::of($search)
            ->replace(['\'', '&', '|', '-', ':', '*'], ' ')
            ->replaceMatches('/\\s+/', ' ')
            ->explode(' ');

        return $words
            ->filter(static fn(mixed $p) => $p !== null && $p !== '')
            ->map(static fn(string $word) => "'$word':*")
            ->join('&');
    }

    /*
    |--------------------------------------------------------------------------
    | Search columns and indexes update.
    |--------------------------------------------------------------------------
    */

    /**
     * Update search columns for a query builder.
     *
     * @param Builder $query
     *
     * @return void
     */
    public function updateSearchColumnsForBuilder(Builder $query): void
    {
        $query->chunk(100, function (Collection $models) {
            $models->each(
                fn(FulltextSearchable & Model $model) => $this->updateSearchColumnsForInstance($model),
            );
        });
    }

    /**
     * Update search columns for all instances of a model.
     *
     * @param Model&FulltextSearchable $model
     *
     * @return void
     */
    public function updateSearchColumnsForModel(FulltextSearchable & Model $model): void
    {
        $this->updateSearchColumnsForBuilder($model->newQuery()->withoutGlobalScopes());
    }

    /**
     * Update a model instance with search columns values.
     *
     * @param Model&FulltextSearchable $model
     *
     * @return void
     */
    public function updateSearchColumnsForInstance(FulltextSearchable & Model $model): void
    {
        if (! MigrationsHelper::isPostgreSQL()) {
            return;
        }

        if (! $model->isSearchable()) {
            return;
        }

        $searchValues = $this->computeSearchTextValues($model);

        if ($searchValues->isEmpty()) {
            $searchValues->push(['', self::SEARCH_WEIGHT_HIGH, 'simple']);
        }

        DB::statement(
            'UPDATE ' . $model->getTable()
            . ' SET search_text = ('
            . $searchValues->map(
                fn(array $value) => 'setweight(to_tsvector(\'' . $value[2] . '\', ?), \'' . $value[1] . '\')',
            )->join(' || ')
            . ')'
            . ' WHERE ' . $model->getQualifiedKeyName() . '= ?',
            [...$searchValues->map(static fn(array $value) => $value[0]), $model->getKey()],
        );
    }

    /**
     * Compute collection of search text values.
     *
     * @param Model&FulltextSearchable $model
     *
     * @return Collection
     */
    private function computeSearchTextValues(FulltextSearchable & Model $model): Collection
    {
        return collect($model->getSearchable())
            ->reduce(function (Collection $values, string $weight, string $key) use ($model) {
                if ($model->isRelation($key)) {
                    $model->loadMissing($key);

                    $modelValue = $model->{$key};
                    $newValues = Collection::wrap($modelValue)->reduce(
                        fn(Collection $relatedValues, FulltextSearchable&Model $related) => $relatedValues->merge(
                            $this->computeSearchTextValues($related)->filter(fn(array $value) => in_array(
                                $value[1],
                                [self::SEARCH_WEIGHT_HIGH, self::SEARCH_WEIGHT_MODERATE],
                            ))->map(fn(array $value) => [$value[0], $weight, $value[2]]),
                        ),
                        new Collection(),
                    );

                    return $values->push(...$newValues);
                }

                $modelValue = $model->{$key};
                $newValues = $modelValue instanceof TranslationObject
                    ? $modelValue->collect()
                        ->map(fn(string | null $v, string $locale) => [
                            $this->normalizeSearchText($v ?? ''),
                            $weight,
                            $this->computeSearchTextLanguage($locale),
                        ])
                        ->all()
                    : [[$this->normalizeSearchText((string) $modelValue), $weight, $this->computeSearchTextLanguage()]];

                return $values->push(...$newValues);
            }, new Collection())
            ->filter(fn(array $value) => $value[0] !== null && $value[0] !== '');
    }

    /**
     * Normalize a searchable value.
     *
     * @param string|null $value
     *
     * @return string
     */
    private function normalizeSearchText(string | null $value): string
    {
        return Str::of(StrHelper::stripHtml($value ?? ''))
            ->when(
                $value !== '' && $value === Str::slug($value ?? ''),
                fn(Stringable $s) => $s->append(
                    ' ' . Str::of($value ?? '')->slug()->replace('-', '')
                )
            )
            ->trim()
            ->value();
    }

    /**
     * Compute search text language with or without a locale.
     *
     * @param string|null $locale
     *
     * @return string
     */
    private function computeSearchTextLanguage(string | null $locale = null): string
    {
        $locale = $locale ? LocaleHelper::decompose($locale)[0] : null;

        return self::SEARCH_LANGUAGES[$locale] ?? 'simple';
    }

    /**
     * Compute a model instance's search columns values.
     *
     * @param Model&FulltextSearchable $model
     *
     * @return array
     */
    private function computeSearchValues(FulltextSearchable & Model $model): array
    {
        $values = [];

        collect($model->getSearchable())
            ->each(function (string $column, string $key) use ($model, &$values) {
                $values[$column] = $this->mergeSearchValues([
                    $values[$column] ?? null,
                    $this->computeSearchValue($model, $key),
                ]);
            });

        return $values;
    }

    /**
     * Compute a model instance's search columns values for a searchable attribute.
     *
     * @param Model&FulltextSearchable $model
     * @param string                   $key
     *
     * @return string|null
     */
    private function computeSearchValue(FulltextSearchable & Model $model, string $key): string | null
    {
        if ($model->isRelation($key)) {
            $model->loadMissing($key);

            $value = $model->{$key};

            return $this->mergeSearchValues(
                Collection::wrap($value)->map(function (FulltextSearchable&Model $related) use ($model) {
                    $values = $this->computeSearchValues($related);

                    return $this->mergeSearchValues([
                        $values[self::SEARCH_WEIGHT_HIGH] ?? null,
                        $values[self::SEARCH_WEIGHT_MODERATE] ?? null,
                    ]);
                })->all(),
            );
        }

        $value = $model->{$key};

        return $value instanceof TranslationObject
            ? $this->mergeSearchValues(
                $value->collect()
                    ->map(fn(string | null $v) => $this->normalizeSearchValues($v))
                    ->all(),
            )
            : $this->normalizeSearchValues($value);
    }

    /**
     * Normalize a searchable value.
     *
     * @param string|null $value
     *
     * @return string
     */
    private function normalizeSearchValues(string | null $value): string
    {
        return Str::of(StrHelper::stripHtml($value ?? ''))
            ->replace(['\'', '"'], '')
            ->replaceMatches('/[«».,;:!]+/', ' ')
            ->ascii()
            ->lower()
            ->trim()
            ->value();
    }

    /**
     * Merge multiple searchable values.
     *
     * @param array $values
     *
     * @return string
     */
    private function mergeSearchValues(array $values): string
    {
        return implode(' ', array_filter($values));
    }
}
