<?php

namespace App\Services\External\Contracts;

use App\Services\External\Concerns\CrawlLimitReachedException;
use App\Services\External\Concerns\CrawlReport;
use App\Services\External\Concerns\CrawlTask;

/**
 * Interface CrawlArticles.
 */
interface CrawlArticles
{
    /**
     * Crawl a list of articles and insert/update them in DB.
     *
     * @param CrawlTask $task
     *
     * @return CrawlReport
     *
     * @throws CrawlLimitReachedException
     */
    public function crawlArticles(CrawlTask $task): CrawlReport;
}
