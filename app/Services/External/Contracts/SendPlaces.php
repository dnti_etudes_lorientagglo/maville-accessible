<?php

namespace App\Services\External\Contracts;

use App\Models\Place;

/**
 * Interface SendPlaces.
 */
interface SendPlaces
{
    /**
     * Send a place to an external service.
     *
     * @param Place $place
     *
     * @return void
     */
    public function sendPlace(Place $place): void;
}
