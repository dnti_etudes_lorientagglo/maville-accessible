<?php

namespace App\Services\External\Contracts;

use App\Services\External\Concerns\CrawlLimitReachedException;
use App\Services\External\Concerns\CrawlReport;
use App\Services\External\Concerns\CrawlTask;

/**
 * Interface CrawlOrganizations.
 */
interface CrawlOrganizations
{
    /**
     * Crawl a list of organizations and insert/update them in DB.
     *
     * @param CrawlTask $task
     *
     * @return CrawlReport
     *
     * @throws CrawlLimitReachedException
     */
    public function crawlOrganizations(CrawlTask $task): CrawlReport;
}
