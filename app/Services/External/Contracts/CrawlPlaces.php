<?php

namespace App\Services\External\Contracts;

use App\Services\External\Concerns\CrawlLimitReachedException;
use App\Services\External\Concerns\CrawlReport;
use App\Services\External\Concerns\CrawlTask;

/**
 * Interface CrawlPlaces.
 */
interface CrawlPlaces
{
    /**
     * Crawl a list of places and insert/update them in DB.
     *
     * @param CrawlTask $task
     *
     * @return CrawlReport
     *
     * @throws CrawlLimitReachedException
     */
    public function crawlPlaces(CrawlTask $task): CrawlReport;
}
