<?php

namespace App\Services\External\Contracts;

use App\Services\External\Concerns\CrawlLimitReachedException;
use App\Services\External\Concerns\CrawlReport;
use App\Services\External\Concerns\CrawlTask;

/**
 * Interface CrawlCategories.
 */
interface CrawlCategories
{
    /**
     * Crawl a list of categories and insert/update them in DB.
     *
     * @param CrawlTask $task
     *
     * @return CrawlReport
     *
     * @throws CrawlLimitReachedException
     */
    public function crawlCategories(CrawlTask $task): CrawlReport;
}
