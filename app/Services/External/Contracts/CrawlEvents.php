<?php

namespace App\Services\External\Contracts;

use App\Services\External\Concerns\CrawlLimitReachedException;
use App\Services\External\Concerns\CrawlReport;
use App\Services\External\Concerns\CrawlTask;

/**
 * Interface CrawlEvents.
 */
interface CrawlEvents
{
    /**
     * Crawl a list of events and insert/update them in DB.
     *
     * @param CrawlTask $task
     *
     * @return CrawlReport
     *
     * @throws CrawlLimitReachedException
     */
    public function crawlEvents(CrawlTask $task): CrawlReport;
}
