<?php

namespace App\Services\External\Contracts;

/**
 * Interface ExternalService.
 */
interface ExternalService
{
    /**
     * Get the service name.
     *
     * @return string
     */
    public function serviceName(): string;

    /**
     * Check if the service is configured (env variables, etc.).
     *
     * @param string $interfaceNeeded
     *
     * @return bool
     */
    public function serviceConfigured(string $interfaceNeeded): bool;
}
