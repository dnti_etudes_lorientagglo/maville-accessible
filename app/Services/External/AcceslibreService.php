<?php

namespace App\Services\External;

use App\Helpers\LocaleHelper;
use App\Helpers\PlacesHelper;
use App\Models\Category;
use App\Models\Enums\AccessibilityCriteria;
use App\Models\Enums\AccessibilityCriteriaType;
use App\Models\Enums\SourceOrigin;
use App\Models\Place;
use App\Models\Source;
use App\Services\App\AppConfig;
use App\Services\App\AppEnv;
use App\Services\External\Concerns\CrawlData;
use App\Services\External\Concerns\CrawlDataItem;
use App\Services\External\Concerns\CrawlerHelper;
use App\Services\External\Concerns\CrawlParser;
use App\Services\External\Concerns\CrawlRelatedModels;
use App\Services\External\Concerns\CrawlReport;
use App\Services\External\Concerns\CrawlTask;
use App\Services\External\Contracts\CrawlCategories;
use App\Services\External\Contracts\CrawlPlaces;
use App\Services\External\Contracts\ExternalService;
use App\Services\External\Contracts\SendPlaces;
use Closure;
use Illuminate\Contracts\Config\Repository as ConfigRepository;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Throwable;

/**
 * Class AcceslibreService.
 *
 * @see SourceOrigin
 */
class AcceslibreService implements ExternalService, CrawlCategories, CrawlPlaces, SendPlaces
{
    /**
     * AcceslibreService constructor.
     *
     * @param ConfigRepository $config
     * @param AppConfig        $appConfig
     * @param AppEnv           $appEnv
     * @param CrawlParser      $crawlParser
     */
    public function __construct(
        private readonly ConfigRepository $config,
        private readonly AppConfig $appConfig,
        private readonly AppEnv $appEnv,
        private readonly CrawlParser $crawlParser,
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function serviceName(): string
    {
        return SourceOrigin::ACCESLIBRE->value;
    }

    /**
     * {@inheritDoc}
     */
    public function serviceConfigured(string $interfaceNeeded): bool
    {
        return ! empty($this->getApiKey())
            && ($interfaceNeeded !== SendPlaces::class || $this->shouldSendData());
    }

    /*
    |--------------------------------------------------------------------------
    | Categories
    |--------------------------------------------------------------------------
    */

    /**
     * {@inheritDoc}
     */
    public function crawlCategories(CrawlTask $task): CrawlReport
    {
        return $this->crawlCategoriesFrom($this->makeDataCrawler('activites'), $task);
    }

    /**
     * Crawl categories using given crawl function.
     *
     * @param Closure        $crawlData
     * @param CrawlTask|null $task
     * @param bool           $keepModels
     *
     * @return CrawlReport
     */
    private function crawlCategoriesFrom(
        Closure $crawlData,
        CrawlTask | null $task = null,
        bool $keepModels = false,
    ): CrawlReport {
        $handleData = function (CrawlData $data, CrawlReport $report) use ($task) {
            $categories = CrawlerHelper::sourceableQuery(Category::query(), $data)->get();

            $data->data->each(function (CrawlDataItem $item) use ($categories, $task, $report) {
                /** @var Category|null $category */
                [$category, $done] = CrawlerHelper::sourceableUse(
                    $task,
                    $report,
                    $categories,
                    new Category(),
                    $item,
                );
                if (! $category) {
                    return;
                }

                if ($this->shouldDraftNewCategories() && ! $category->exists) {
                    $category->published_at = null;
                }

                CrawlerHelper::sourceableFillAndSave($category, $item, [
                    'name' => $item->value('nom', $this->crawlParser->translationParser()),
                ]);

                $done();
            });
        };

        return CrawlerHelper::run($crawlData, $handleData, fn() => null, $task, $keepModels);
    }

    /**
     * Make a category item from place.
     *
     * @param CrawlDataItem $item
     *
     * @return CrawlDataItem
     */
    private function makeCategoryItemFromPlace(CrawlDataItem $item): CrawlDataItem
    {
        return new CrawlDataItem(
            $item->value('activite.nom'),
            [['slug' => $item->value('activite.slug')]],
            $item->value('activite'),
        );
    }

    /**
     * Crawl categories from a place crawled data.
     *
     * @param CrawlData $data
     *
     * @return Collection
     */
    private function crawlCategoriesFromPlaces(CrawlData $data): Collection
    {
        $report = $this->crawlCategoriesFrom(fn() => new CrawlData(
            source: SourceOrigin::ACCESLIBRE,
            data: $data->data,
            more: false,
            factory: fn(CrawlDataItem $item) => $this->makeCategoryItemFromPlace($item),
        ), keepModels: true);

        return $report->models();
    }

    /*
    |--------------------------------------------------------------------------
    | Places
    |--------------------------------------------------------------------------
    */

    /**
     * Compute the crawling acceslibre zone parameters.
     *
     * @return array
     */
    private function computeCrawlingZoneParams(): array
    {
        $zone = $this->config->get('services.acceslibre.zone');
        if ($zone) {
            return ['zone' => $zone];
        }

        $publicEnv = $this->appEnv->publicEnv();
        $mapCenter = $publicEnv['services']['map']['center']['geometry']['coordinates'];

        return [
            'around' => implode(',', [
                number_format($mapCenter[1], 2, '.', ''),
                number_format($mapCenter[0], 2, '.', ''),
            ]),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function crawlPlaces(CrawlTask $task): CrawlReport
    {
        $crawlData = $this->makeDataCrawler('erps', [
            'page_size' => 100,
            ...$this->computeCrawlingZoneParams(),
        ]);

        /** @var CrawlRelatedModels|null $categories */
        $categories = null;

        $clearData = function () use (&$categories) {
            $categories?->trashUnused();
        };

        $handleData = function (CrawlData $data, CrawlReport $report) use ($task, &$categories) {
            $places = CrawlerHelper::sourceableQuery(Place::query()->with('categories'), $data)->get();
            $categories = new CrawlRelatedModels($this->crawlCategoriesFromPlaces($data));

            $data->data->each(function (CrawlDataItem $item) use ($places, $categories, $data, $task, $report) {
                /** @var Place|null $place */
                [$place, $done] = CrawlerHelper::sourceableUse(
                    $task,
                    $report,
                    $places,
                    new Place(),
                    $item,
                    fn(Place $place) => (
                        PlacesHelper::distance($place->address['geometry'], $item->value('geom')) < 0.25
                    ),
                );
                if (! $place) {
                    return;
                }

                $accessibility = $place->accessibility?->toArray();
                $accessibilityCriteria = $this->parsePlaceAccessibilityCriteria($item);
                if ($accessibilityCriteria->isNotEmpty()) {
                    $accessibility = [
                        ...($accessibility ?? []),
                        'criteria' => $accessibilityCriteria->toArray(),
                    ];
                }

                CrawlerHelper::sourceableFillAndSave($place, $item, [
                    'name'          => $item->value('nom', $this->crawlParser->translationParser()),
                    'phone'         => $item->value('telephone', $this->crawlParser->phoneParser()),
                    'email'         => $item->value('contact_email', $this->crawlParser->emailParser()),
                    'accessibility' => empty($accessibility) ? null : $accessibility,
                    'address'       => [
                        'type'       => 'housenumber',
                        'address'    => trim(Str::replace(
                            $item->value('code_postal') . ' ' . $item->value('commune'),
                            '',
                            $item->value('adresse'),
                        )),
                        'city'       => $item->value('commune'),
                        'postalCode' => $item->value('code_postal'),
                        'inseeCode'  => $item->value('code_insee'),
                        'geometry'   => $item->value('geom'),
                    ],
                    'links'         => $item->value('site_internet', fn($u) => ['website' => $u]),
                ]);

                /** @var Category|null $category */
                $category = CrawlerHelper::sourceableFind(
                    $categories->models(),
                    $this->makeCategoryItemFromPlace($item)->withParent($data),
                );
                if ($category && $place->categories->isEmpty()) {
                    $place->categories()->sync($category);
                    $categories->markUsed($category);
                }

                $done();
            });
        };

        return CrawlerHelper::run($crawlData, $handleData, $clearData, $task);
    }

    /**
     * Parse accessibility criteria from a place crawl item.
     *
     * @param CrawlDataItem $item
     *
     * @return Collection
     */
    private function parsePlaceAccessibilityCriteria(CrawlDataItem $item): Collection
    {
        return $this->mapAccessibilityCriteria(
            static fn(AccessibilityCriteria $c, string $k) => $item->value('accessibilite.' . $k),
            static fn(AccessibilityCriteria $c) => $c->id,
            static fn(array $values, $i) => $values[$i] ?? null,
        )->filter(
            static fn(mixed $value) => $value !== null && (! is_array($value) || ! empty($value)),
        );
    }

    /*
    |--------------------------------------------------------------------------
    | Places Sending
    |--------------------------------------------------------------------------
    */

    /**
     * {@inheritDoc}
     */
    public function sendPlace(Place $place): void
    {
        $accessibility = $this->serializePlaceAccessibilityCriteria($place);
        $hasAccessibility = $accessibility->some(
            static fn(mixed $value) => $value !== null && (! is_array($value) || ! empty($value)),
        );
        if (! $hasAccessibility) {
            return;
        }

        $place->loadMissing([
            'categories.sources',
        ]);

        $source = $place->sources()->get()->first(static fn(Source $source) => (
            $source->name === SourceOrigin::ACCESLIBRE->value
            && ! ! ($source->data['slug'] ?? null)
        ));

        $addressRoad = $place->address['address'] ?? null;
        $addressNumber = Str::match('/^([0-9]+)(,|\s)+/', $addressRoad) ?: null;
        if ($addressNumber !== null) {
            $addressRoad = Str::replaceMatches('/^[0-9]+(,|\s)+/', '', $addressRoad);
        }

        $data = collect([
            'nom'           => LocaleHelper::translate($place->name),
            'numero'        => $addressNumber,
            'voie'          => $addressRoad,
            'commune'       => $place->address['city'],
            'code_postal'   => $place->address['postalCode'],
            'code_insee'    => $place->address['inseeCode'] ?? null,
            'longitude'     => $place->address['geometry']['coordinates'][0],
            'latitude'      => $place->address['geometry']['coordinates'][1],
            'site_internet' => $place->links['website'] ?? null,
            'telephone'     => $place->phone,
            'import_email'  => $place->email,
            'activite'      => $this->serializePlaceActivity($place),
            'accessibilite' => $accessibility,
            'source_id'     => $place->id,
            // Other data documented by acceslibre API which we are not sending.
            // 'source'          => 'acceslibre',
            // 'geoloc_provider' => 'string',
            // 'siret'           => "string",
            // 'contact_url'     => "string",
            // 'lieu_dit'        => "string",
        ])->filter(static fn(mixed $v) => $v !== null);

        $pendingRequest = $this->prepareRequest();
        try {
            $response = $source
                ? $pendingRequest->patch('erps/' . $source->data['slug'] . '/', $data->toArray())
                : $pendingRequest->post('erps/', $data->toArray());
        } catch (Throwable $exception) {
            if (! $source) {
                // Potential duplicate, we'll try to find the place in
                // acceslibre API using "source_id".
                // If found, we'll force the source creation and
                // rerun the sync manually.
                $distPlace = $this->findAcceslibrePlaceUsingLocalId($place->id);
                if ($distPlace) {
                    $this->createSourceForAcceslibrePlace($place, $distPlace);

                    $this->sendPlace($place);

                    return;
                }
            }

            throw $exception;
        }

        if (! $source) {
            $distPlace = $response->json('uuid')
                ? $response->json()
                : $this->findAcceslibrePlaceUsingLocalId($place->id);

            $this->createSourceForAcceslibrePlace($place, $distPlace);
        }
    }

    /**
     * Try to find an acceslibre place from its local ID.
     *
     * @param string $id
     *
     * @return array|null
     */
    private function findAcceslibrePlaceUsingLocalId(string $id): array | null
    {
        try {
            return $this->prepareRequest()
                ->get('erps/', [
                    'source_id' => $id,
                    'page_size' => 1,
                    'page'      => 1,
                ])
                ->json('results.0');
        } catch (Throwable) {
            return null;
        }
    }

    /**
     * Create a source over place for the given dist place.
     *
     * @param Place $place
     * @param array $distPlace
     *
     * @return void
     */
    private function createSourceForAcceslibrePlace(Place $place, array $distPlace): void
    {
        $source = new Source();
        $source->name = SourceOrigin::ACCESLIBRE->value;
        $source->data = [
            'uuid'    => $distPlace['uuid'],
            'slug'    => $distPlace['slug'],
            'website' => $distPlace['web_url'],
        ];
        $source->sourceable()->associate($place);
        $source->save();
    }

    /**
     * Serialize a place activity using categories.
     *
     * @param Place $place
     *
     * @return string
     */
    private function serializePlaceActivity(Place $place): string
    {
        $firstCategorySource = $place->categories
            ->map(static fn(Category $category) => $category->sources->first(static fn(Source $source) => (
                $source->name === SourceOrigin::ACCESLIBRE->value
                && ! ! ($source->data['slug'] ?? null)
            )))
            ->first(static fn(Source | null $source) => ! ! $source);

        $firstCategorySlug = $firstCategorySource?->data['slug'];
        try {
            return $this->prepareRequest()
                ->get("activites/$firstCategorySlug/")
                ->json('nom');
        } catch (Throwable) {
            // Ignore non existing categories.
        }

        return 'Autre';
    }

    /**
     * Serialize accessibility criteria from a place model.
     *
     * @param Place $place
     *
     * @return Collection
     */
    private function serializePlaceAccessibilityCriteria(Place $place): Collection
    {
        return $this->mapAccessibilityCriteria(
            static fn(AccessibilityCriteria $c) => $place->accessibility['criteria'][$c->id] ?? null,
            static fn(AccessibilityCriteria $_, string $k) => Str::afterLast($k, '.'),
            static fn(array $values, mixed $i) => array_search($i, $values),
        );
    }

    /*
    |--------------------------------------------------------------------------
    | Concerns
    |--------------------------------------------------------------------------
    */

    /**
     * Get the mapping between acceslibre and platform accessibility criteria and values.
     *
     * @return Collection
     */
    private function accessibilityCriteriaMapping(): Collection
    {
        return collect([
            'transport.transport_station_presence'                   => [
                'criteria' => AccessibilityCriteria::transportStation(),
            ],
            'transport.transport_information'                        => [
                'criteria' => AccessibilityCriteria::transportInformation(),
            ],
            'transport.stationnement_presence'                       => [
                'criteria' => AccessibilityCriteria::innerParking(),
            ],
            'transport.stationnement_pmr'                            => [
                'criteria' => AccessibilityCriteria::innerParkingPrm(),
            ],
            'transport.stationnement_ext_presence'                   => [
                'criteria' => AccessibilityCriteria::externalParking(),
            ],
            'transport.stationnement_ext_pmr'                        => [
                'criteria' => AccessibilityCriteria::externalParkingPrm(),
            ],
            'cheminement_ext.cheminement_ext_presence'               => [
                'criteria' => AccessibilityCriteria::externalPath(),
            ],
            'cheminement_ext.cheminement_ext_terrain_stable'         => [
                'criteria' => AccessibilityCriteria::externalPathStable(),
            ],
            'cheminement_ext.cheminement_ext_plain_pied'             => [
                'criteria' => AccessibilityCriteria::externalPathFlat(),
            ],
            'cheminement_ext.cheminement_ext_ascenseur'              => [
                'criteria' => AccessibilityCriteria::externalPathElevator(),
            ],
            'cheminement_ext.cheminement_ext_nombre_marches'         => [
                'criteria' => AccessibilityCriteria::externalPathStairsCount(),
            ],
            'cheminement_ext.cheminement_ext_reperage_marches'       => [
                'criteria' => AccessibilityCriteria::externalPathStairsVisibility(),
            ],
            'cheminement_ext.cheminement_ext_sens_marches'           => [
                'criteria' => AccessibilityCriteria::externalPathStairsDirection(),
                'values'   => [
                    'montant'    => 'ascending',
                    'descendant' => 'descending',
                ],
            ],
            'cheminement_ext.cheminement_ext_main_courante'          => [
                'criteria' => AccessibilityCriteria::externalPathStairsHandrail(),
            ],
            'cheminement_ext.cheminement_ext_rampe'                  => [
                'criteria' => AccessibilityCriteria::externalPathStairsRail(),
                'values'   => [
                    'aucune'   => 'none',
                    'fixe'     => 'fixed',
                    'amovible' => 'removable',
                ],
            ],
            'cheminement_ext.cheminement_ext_pente_presence'         => [
                'criteria' => AccessibilityCriteria::externalPathSlope(),
            ],
            'cheminement_ext.cheminement_ext_pente_degre_difficulte' => [
                'criteria' => AccessibilityCriteria::externalPathSlopeLevel(),
                'values'   => [
                    'légère'     => 'low',
                    'importante' => 'high',
                ],
            ],
            'cheminement_ext.cheminement_ext_pente_longueur'         => [
                'criteria' => AccessibilityCriteria::externalPathSlopeLength(),
                'values'   => [
                    'courte'  => 'short',
                    'moyenne' => 'medium',
                    'longue'  => 'long',
                ],
            ],
            'cheminement_ext.cheminement_ext_devers'                 => [
                'criteria' => AccessibilityCriteria::externalPathInclinationLevel(),
                'values'   => [
                    'aucun'     => 'none',
                    'léger'     => 'low',
                    'important' => 'high',
                ],
            ],
            'cheminement_ext.cheminement_ext_bande_guidage'          => [
                'criteria' => AccessibilityCriteria::externalPathGuideStrip(),
            ],
            'cheminement_ext.cheminement_ext_retrecissement'         => [
                'criteria' => AccessibilityCriteria::externalPathShrink(),
            ],
            'entree.entree_reperage'                                 => [
                'criteria' => AccessibilityCriteria::entranceVisible(),
            ],
            'entree.entree_vitree'                                   => [
                'criteria' => AccessibilityCriteria::entranceWindow(),
            ],
            'entree.entree_vitree_vitrophanie'                       => [
                'criteria' => AccessibilityCriteria::entranceWindowContrast(),
            ],
            'entree.entree_plain_pied'                               => [
                'criteria' => AccessibilityCriteria::entranceFlat(),
            ],
            'entree.entree_ascenseur'                                => [
                'criteria' => AccessibilityCriteria::entranceElevator(),
            ],
            'entree.entree_marches'                                  => [
                'criteria' => AccessibilityCriteria::entranceStairsCount(),
            ],
            'entree.entree_marches_reperage'                         => [
                'criteria' => AccessibilityCriteria::entranceStairsVisibility(),
            ],
            'entree.entree_marches_main_courante'                    => [
                'criteria' => AccessibilityCriteria::entranceStairsHandrail(),
            ],
            'entree.entree_marches_rampe'                            => [
                'criteria' => AccessibilityCriteria::entranceStairsRail(),
                'values'   => [
                    'aucune'   => 'none',
                    'fixe'     => 'fixed',
                    'amovible' => 'removable',
                ],
            ],
            'entree.entree_marches_sens'                             => [
                'criteria' => AccessibilityCriteria::entranceStairsDirection(),
                'values'   => [
                    'montant'    => 'ascending',
                    'descendant' => 'descending',
                ],
            ],
            'entree.entree_dispositif_appel'                         => [
                'criteria' => AccessibilityCriteria::entranceCallDevice(),
            ],
            'entree.entree_dispositif_appel_type'                    => [
                'criteria' => AccessibilityCriteria::entranceCallDeviceType(),
                'values'   => [
                    'bouton'     => 'button',
                    'interphone' => 'intercom',
                    'visiophone' => 'visiophone',
                ],
            ],
            'entree.entree_balise_sonore'                            => [
                'criteria' => AccessibilityCriteria::entranceSoundBeacon(),
            ],
            'entree.entree_aide_humaine'                             => [
                'criteria' => AccessibilityCriteria::entranceHumanHelp(),
            ],
            'entree.entree_largeur_mini'                             => [
                'criteria' => AccessibilityCriteria::entranceWidth(),
            ],
            'entree.entree_pmr'                                      => [
                'criteria' => AccessibilityCriteria::entrancePrm(),
            ],
            'entree.entree_pmr_informations'                         => [
                'criteria' => AccessibilityCriteria::entrancePrmInformation(),
            ],
            'entree.entree_porte_presence'                           => [
                'criteria' => AccessibilityCriteria::entranceDoor(),
            ],
            'entree.entree_porte_manoeuvre'                          => [
                'criteria' => AccessibilityCriteria::entranceDoorMechanism(),
                'values'   => [
                    'battante'    => 'swing',
                    'coulissante' => 'sliding',
                    'tourniquet'  => 'turnstile',
                    'tambour'     => 'revolving',
                ],
            ],
            'entree.entree_porte_type'                               => [
                'criteria' => AccessibilityCriteria::entranceDoorType(),
                'values'   => [
                    'manuelle'    => 'manual',
                    'automatique' => 'automatic',
                ],
            ],
            'accueil.accueil_visibilite'                             => [
                'criteria' => AccessibilityCriteria::receptionVisible(),
            ],
            'accueil.accueil_personnels'                             => [
                'criteria' => AccessibilityCriteria::receptionPersonnel(),
                'values'   => [
                    'aucun'      => 'none',
                    'formés'     => 'trained',
                    'non-formés' => 'nonTrained',
                ],
            ],
            'accueil.accueil_equipements_malentendants_presence'     => [
                'criteria' => AccessibilityCriteria::receptionHearingEquipments(),
            ],
            'accueil.accueil_equipements_malentendants'              => [
                'criteria' => AccessibilityCriteria::receptionHearingEquipmentsType(),
                'values'   => [
                    'bim'    => 'bim',
                    'bmp'    => 'bmp',
                    'lsf'    => 'lsf',
                    'lpc'    => 'lpc',
                    'sts'    => 'sts',
                    'autres' => 'other',
                ],
            ],
            'accueil.accueil_cheminement_plain_pied'                 => [
                'criteria' => AccessibilityCriteria::receptionPathFlat(),
            ],
            'accueil.accueil_cheminement_ascenseur'                  => [
                'criteria' => AccessibilityCriteria::receptionPathElevator(),
            ],
            'accueil.accueil_cheminement_nombre_marches'             => [
                'criteria' => AccessibilityCriteria::receptionPathStairsCount(),
            ],
            'accueil.accueil_cheminement_reperage_marches'           => [
                'criteria' => AccessibilityCriteria::receptionPathStairsVisibility(),
            ],
            'accueil.accueil_cheminement_main_courante'              => [
                'criteria' => AccessibilityCriteria::receptionPathStairsHandrail(),
            ],
            'accueil.accueil_cheminement_rampe'                      => [
                'criteria' => AccessibilityCriteria::receptionPathStairsRail(),
                'values'   => [
                    'aucune'   => 'none',
                    'fixe'     => 'fixed',
                    'amovible' => 'removable',
                ],
            ],
            'accueil.accueil_cheminement_sens_marches'               => [
                'criteria' => AccessibilityCriteria::receptionPathStairsDirection(),
                'values'   => [
                    'montant'    => 'ascending',
                    'descendant' => 'descending',
                ],
            ],
            'accueil.accueil_retrecissement'                         => [
                'criteria' => AccessibilityCriteria::receptionPathShrink(),
            ],
            'accueil.sanitaires_presence'                            => [
                'criteria' => AccessibilityCriteria::restrooms(),
            ],
            'accueil.sanitaires_adaptes'                             => [
                'criteria' => AccessibilityCriteria::restroomsAdapted(),
            ],
            'commentaire.labels'                                     => [
                'criteria' => AccessibilityCriteria::labels(),
                'values'   => [
                    'dpt'     => 'dpt',
                    'mobalib' => 'mobalib',
                    'th'      => 'th',
                    'autre'   => 'other',
                ],
            ],
            'commentaire.labels_autre'                               => [
                'criteria' => AccessibilityCriteria::labelsOther(),
            ],
            'commentaire.labels_familles_handicap'                   => [
                'criteria' => AccessibilityCriteria::labelsCategories(),
                'values'   => [
                    'moteur'  => 'physical',
                    'visuel'  => 'visual',
                    'auditif' => 'hearing',
                    'mental'  => 'cognitive',
                ],
            ],
            'commentaire.commentaire'                                => [
                'criteria' => AccessibilityCriteria::comment(),
            ],
            'registre.registre_url'                                  => [
                'criteria' => AccessibilityCriteria::registryURL(),
            ],
            'conformite.conformite'                                  => [
                'criteria' => AccessibilityCriteria::conformity(),
            ],
        ]);
    }

    /**
     * Map accessibility criteria using closures.
     *
     * @param Closure $extractValue
     * @param Closure $keyByValue
     * @param Closure $searchItem
     *
     * @return Collection
     */
    private function mapAccessibilityCriteria(
        Closure $extractValue,
        Closure $keyByValue,
        Closure $searchItem,
    ): Collection {
        return $this->accessibilityCriteriaMapping()->mapWithKeys(function (
            array $criteriaData,
            string $acceslibreKey,
        ) use (
            $extractValue,
            $keyByValue,
            $searchItem,
        ) {
            /** @var AccessibilityCriteria $criteria */
            $criteria = $criteriaData['criteria'];
            $valuesMap = $criteriaData['values'] ?? [];

            $keyValue = call_user_func_array($keyByValue, [$criteria, $acceslibreKey]);
            $itemValue = call_user_func_array($extractValue, [$criteria, $acceslibreKey]);
            $searchItemValue = static fn(mixed $i) => call_user_func_array($searchItem, [$valuesMap, $i]);

            if ($criteria->type === AccessibilityCriteriaType::ITEMS) {
                $items = Collection::wrap($itemValue)
                    ->map(static fn(mixed $i) => $searchItemValue($i))
                    ->filter();

                return [$keyValue => $items->all()];
            }

            if ($criteria->type === AccessibilityCriteriaType::ITEM) {
                $foundItem = $searchItemValue($itemValue);

                return [$keyValue => $foundItem ?: null];
            }

            return [$keyValue => $itemValue !== null && $itemValue !== '' ? $itemValue : null];
        });
    }

    /**
     * Get Acceslibre API key.
     *
     * @return string|null
     */
    private function getApiKey(): string | null
    {
        return $this->appConfig->service('acceslibre.apiKey');
    }

    /**
     * Check if platform data should be sent to Acceslibre.
     *
     * @return bool
     */
    private function shouldSendData(): bool
    {
        return $this->appConfig->service('acceslibre.sendData') ?? false;
    }

    /**
     * Check if new categories should be marked unpublished.
     *
     * @return bool
     */
    private function shouldDraftNewCategories(): bool
    {
        return $this->appConfig->service('acceslibre.draftNewCategories') ?? false;
    }

    /**
     * Prepare a new Acceslibre request.
     *
     * @return PendingRequest
     */
    private function prepareRequest(): PendingRequest
    {
        return Http::baseUrl($this->config->get('services.acceslibre.endpoint'))
            ->withHeaders(['Authorization' => 'Api-Key ' . $this->getApiKey()])
            ->acceptJson()
            ->throw();
    }

    /**
     * Make a new Acceslibre data crawler function.
     *
     * @param string $endpoint
     * @param array  $params
     *
     * @return Closure
     */
    private function makeDataCrawler(string $endpoint, array $params = []): Closure
    {
        $page = 1;
        $prev = 0;

        return function () use ($endpoint, $params, &$page, &$prev) {
            $data = $this->prepareRequest()
                ->retry(3, 1000)
                ->get($endpoint, [
                    'page' => $page,
                    ...$params,
                ])
                ->json();

            $prev += count($data['results']);
            $page += 1;

            return new CrawlData(
                source: SourceOrigin::ACCESLIBRE,
                data: $data['results'],
                more: $data['count'] > $prev,
                factory: fn(array $result) => new CrawlDataItem(
                    $result['nom'],
                    $this->extractSourcesFromResult($result),
                    $result,
                ),
                total: $data['count'],
            );
        };
    }

    /**
     * Extract sources data from a result.
     *
     * @param array $result
     *
     * @return array
     */
    private function extractSourcesFromResult(array $result): array
    {
        $source = [
            'uuid' => $result['uuid'] ?? null,
            'slug' => $result['slug'],
        ];
        if ($result['web_url'] ?? null) {
            $source['website'] = $result['web_url'];
        }

        return [$source];
    }
}
