<?php

namespace App\Services\External;

use App\Helpers\DateHelper;
use App\Helpers\FilesHelper;
use App\Helpers\JsonHelper;
use App\Helpers\LocaleHelper;
use App\Models\Activity;
use App\Models\Category;
use App\Models\Media;
use App\Models\Place;
use App\Services\App\AppConfig;
use Closure;
use Illuminate\Contracts\Config\Repository as ConfigRepository;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use RuntimeException;
use stdClass;

/**
 * Class DataGouvService.
 */
class DataGouvService
{
    /**
     * DataGouvService constructor.
     *
     * @param ConfigRepository $config
     * @param AppConfig        $appConfig
     */
    public function __construct(
        private readonly ConfigRepository $config,
        private readonly AppConfig $appConfig,
    ) {
    }

    /**
     * Send places with accessibility information to data.gouv.
     *
     * @return void
     */
    public function sendPlaces(): void
    {
        if (! $this->shouldSendPlaces()) {
            return;
        }

        $tmpPath = $this->writeJsonArray(function (Closure $writeItem) {
            Place::query()
                ->with([
                    'cover',
                    'images',
                    'categories',
                ])
                ->whereNotNull('published_at')
                ->whereNotNull('accessibility')
                ->whereHas('categories')
                ->orderBy('created_at')
                ->chunk(100, function (Collection $places) use ($writeItem) {
                    $places->each(function (Place $place) use ($writeItem) {
                        $hasAccessibility = $place->accessibility->collect()->some(
                            static fn(mixed $value) => $value !== null && (! is_array($value) || ! empty($value)),
                        );
                        if (! $hasAccessibility) {
                            return;
                        }

                        $writeItem($this->makePlaceArray($place));
                    });
                });
        });

        $this->writeFileToDataset(
            $tmpPath,
            $this->getPlacesDatasetId(),
            'Lieux accessibles',
            file_get_contents(resource_path('assets/docs/datagouv/PLACES.md')),
            'lieux.json',
        );
    }

    /**
     * Send activities with accessibility information to data.gouv.
     *
     * @return void
     */
    public function sendActivities(): void
    {
        if (! $this->shouldSendActivities()) {
            return;
        }

        $tmpPath = $this->writeJsonArray(function (Closure $writeItem) {
            Activity::query()
                ->with([
                    'activityType',
                    'cover',
                    'images',
                    'categories',
                    'place.cover',
                    'place.images',
                    'place.categories',
                ])
                ->whereNotNull('published_at')
                ->whereNotNull('accessibility')
                ->whereHas('categories')
                ->orderBy('created_at')
                ->chunk(100, function (Collection $activities) use ($writeItem) {
                    $activities->each(function (Activity $activity) use ($writeItem) {
                        $hasAccessibility = $activity->accessibility->collect()->some(
                            static fn(mixed $value) => $value !== null && (! is_array($value) || ! empty($value)),
                        );
                        if (! $hasAccessibility) {
                            return;
                        }

                        $writeItem($this->makeActivityArray($activity));
                    });
                });
        });

        $this->writeFileToDataset(
            $tmpPath,
            $this->getActivitiesDatasetId(),
            'Activités et événements accessibles',
            file_get_contents(resource_path('assets/docs/datagouv/ACTIVITIES.md')),
            'activites.json',
        );
    }

    /**
     * Write a file to a dataset.
     *
     * @param string $path
     * @param string $datasetId
     * @param string $datasetTitle
     * @param string $datasetDescription
     * @param string $fileName
     *
     * @return void
     */
    private function writeFileToDataset(
        string $path,
        string $datasetId,
        string $datasetTitle,
        string $datasetDescription,
        string $fileName,
    ): void {
        if ($this->config->get('services.datagouv.update_dataset')) {
            // Update dataset with description.
            $this->prepareRequest()
                ->put('/datasets/' . $datasetId, [
                    'title'       => $datasetTitle,
                    'description' => $datasetDescription
                        . "\n> Pour plus de détails sur la structure des données"
                        . "\n> consulter la description d'un fichier.",
                ]);
        }

        $fileResource = fopen($path, 'r');

        $latestResource = $this->prepareRequest()
            ->get('/datasets/' . $datasetId . '/')
            ->json('resources')[0] ?? null;
        if (! $latestResource) {
            // Create the new resource.
            $latestResource = $this->prepareRequest()
                ->attach(
                    name: 'file',
                    contents: $fileResource,
                    filename: $fileName,
                )
                ->post('/datasets/' . $datasetId . '/upload/')
                ->json();
        } else {
            // Update the resource.
            $this->prepareRequest()
                ->attach(
                    name: 'file',
                    contents: $fileResource,
                    filename: $fileName,
                )
                ->post('/datasets/' . $datasetId . '/resources/' . $latestResource['id'] . '/upload/')
                ->json();
        }

        fclose($fileResource);

        // Update metadata on resource.
        $this->prepareRequest()
            ->put('/datasets/' . $datasetId . '/resources/' . $latestResource['id'], [
                'title'       => '[' . DateHelper::format(now(), timezone: 'Europe/Paris') . '] ' . $datasetTitle,
                'description' => $datasetDescription
                    . "\n" . file_get_contents(resource_path('assets/docs/datagouv/DATA.md')),
            ]);
    }

    /**
     * Convert a place to an array object.
     *
     * @param Place $place
     *
     * @return array
     */
    private function makePlaceArray(Place $place): array
    {
        return [
            'id'            => $place->id,
            'slug'          => $place->slug,
            'url'           => url($place->getReadLink()),
            'name'          => LocaleHelper::translate($place->name),
            'description'   => LocaleHelper::translate($place->description),
            'address'       => $this->makeFilteredObject($place->address),
            'accessibility' => $this->makeFilteredObject($place->accessibility ?? []),
            'openingHours'  => $place->opening_hours ?? [],
            'phone'         => $place->phone,
            'email'         => $place->email,
            'links'         => $place->links,
            'createdAt'     => $place->created_at->toAtomString(),
            'updatedAt'     => $place->updated_at->toAtomString(),
            'publishedAt'   => $place->published_at->toAtomString(),
            'images'        => collect([$place->cover])
                ->push(...$place->images)
                ->filter()
                ->unique('id')
                ->map(fn(Media $media) => $this->makeImageArray($media))
                ->values(),
            'categories'    => $place->categories
                ->map(fn(Category $category) => $this->makeCategoryArray($category)),
        ];
    }

    /**
     * Convert a place to an array object.
     *
     * @param Activity $activity
     *
     * @return array
     */
    private function makeActivityArray(Activity $activity): array
    {
        return [
            'id'                   => $activity->id,
            'slug'                 => $activity->slug,
            'url'                  => url($activity->getReadLink()),
            'activityType'         => $activity->activityType->code,
            'name'                 => LocaleHelper::translate($activity->name),
            'description'          => LocaleHelper::translate($activity->description),
            'body'                 => LocaleHelper::translate($activity->body),
            'place'                => $activity->place ? $this->makePlaceArray($activity->place) : null,
            'address'              => $activity->place || ! $activity->address
                ? null
                : $this->makeFilteredObject($activity->address),
            'accessibility'        => $this->makeFilteredObject($activity->accessibility),
            'openingOnPeriods'     => $activity->opening_on_periods,
            'openingPeriods'       => $activity->opening_on_periods ? ($activity->opening_periods ?? []) : null,
            'openingWeekdaysHours' => $activity->opening_on_periods ? ($activity->opening_weekdays_hours ?? []) : null,
            'openingDatesHours'    => $activity->opening_on_periods ? null : ($activity->opening_dates_hours ?? []),
            'openingHours'         => $activity->opening_hours ?? [],
            'pricingCode'          => $activity->pricing_code,
            'pricingDescription'   => LocaleHelper::translate($activity->pricing_description),
            'publicCodes'          => $activity->public_codes ?? [],
            'phone'                => $activity->phone,
            'email'                => $activity->email,
            'links'                => $activity->links,
            'createdAt'            => $activity->created_at->toAtomString(),
            'updatedAt'            => $activity->updated_at->toAtomString(),
            'publishedAt'          => $activity->published_at->toAtomString(),
            'images'               => collect([$activity->cover])
                ->push(...$activity->images)
                ->filter()
                ->unique('id')
                ->map(fn(Media $media) => $this->makeImageArray($media))
                ->values(),
            'categories'           => $activity->categories
                ->map(fn(Category $category) => $this->makeCategoryArray($category)),
        ];
    }

    /**
     * Convert a category to an array object.
     *
     * @param Category $category
     *
     * @return array
     */
    private function makeCategoryArray(Category $category): array
    {
        return [
            'id'   => $category->id,
            'slug' => $category->slug,
            'name' => LocaleHelper::translate($category->name),
        ];
    }

    /**
     * Convert an image to an array object.
     *
     * @param Media $media
     *
     * @return array
     */
    private function makeImageArray(Media $media): array
    {
        return [
            'id'  => $media->id,
            'url' => $media->original_url,
        ];
    }

    /**
     * Filter null values from an array object.
     *
     * @param Collection|Arrayable|array $object
     *
     * @return stdClass|array
     */
    private function makeFilteredObject(Collection | Arrayable | array $object): stdClass | array
    {
        $values = collect($object)
            ->filter(static fn(mixed $value) => $value !== null)
            ->all();

        return $values === [] ? new stdClass() : $values;
    }

    /**
     * Check if platform places should be sent to data.gouv.
     *
     * @return bool
     */
    private function shouldSendPlaces(): bool
    {
        return ! ! $this->getApiKey()
            && ! ! $this->getOrganizationId()
            && ! ! $this->getPlacesDatasetId();
    }

    /**
     * Check if platform activities should be sent to data.gouv.
     *
     * @return bool
     */
    private function shouldSendActivities(): bool
    {
        return ! ! $this->getApiKey()
            && ! ! $this->getOrganizationId()
            && ! ! $this->getActivitiesDatasetId();
    }

    /**
     * Get data.gouv API key.
     *
     * @return string|null
     */
    private function getApiKey(): string | null
    {
        return $this->appConfig->service('datagouv.apiKey');
    }

    /**
     * Get data.gouv organization ID.
     *
     * @return string|null
     */
    private function getOrganizationId(): string | null
    {
        return $this->appConfig->service('datagouv.organizationId');
    }

    /**
     * Get data.gouv places dataset ID.
     *
     * @return string|null
     */
    private function getPlacesDatasetId(): string | null
    {
        return $this->appConfig->service('datagouv.placesDatasetId');
    }

    /**
     * Get data.gouv activities dataset ID.
     *
     * @return string|null
     */
    private function getActivitiesDatasetId(): string | null
    {
        return $this->appConfig->service('datagouv.activitiesDatasetId');
    }

    /**
     * Prepare a new data.gouv request.
     *
     * @return PendingRequest
     */
    private function prepareRequest(): PendingRequest
    {
        return Http::baseUrl($this->config->get('services.datagouv.endpoint'))
            ->withHeaders(['X-Api-Key' => $this->getApiKey()])
            ->acceptJson()
            ->throw();
    }

    /**
     * Helper to write a JSON array and return path to temporary file.
     *
     * @param Closure $callback
     *
     * @return string
     */
    private function writeJsonArray(Closure $callback): string
    {
        $tmpPath = FilesHelper::temporaryFile('');
        $tmpResource = fopen($tmpPath, 'wb');
        if (! $tmpResource) {
            throw new RuntimeException('Cannot open temporary file');
        }

        $count = 0;

        fwrite($tmpResource, '[');

        $writeItem = function (array $item) use ($tmpResource, &$count) {
            if ($count > 0) {
                fwrite($tmpResource, ',');
            }

            fwrite($tmpResource, JsonHelper::encode($item));

            $count += 1;
        };

        $callback($writeItem);

        fwrite($tmpResource, ']');
        fclose($tmpResource);

        return $tmpPath;
    }
}
