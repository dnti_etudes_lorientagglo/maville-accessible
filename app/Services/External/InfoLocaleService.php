<?php

namespace App\Services\External;

use App\Helpers\DateHelper;
use App\Helpers\PlacesHelper;
use App\Models\Activity;
use App\Models\ActivityType;
use App\Models\Article;
use App\Models\Category;
use App\Models\Enums\AccessibilityCategory;
use App\Models\Enums\ActivityPricingCode;
use App\Models\Enums\ActivityPublicCode;
use App\Models\Enums\ActivityTypeCode;
use App\Models\Enums\OrganizationTypeCode;
use App\Models\Enums\SourceOrigin;
use App\Models\Media;
use App\Models\Organization;
use App\Models\OrganizationType;
use App\Models\Place;
use App\Services\App\AppConfig;
use App\Services\External\Concerns\CrawlData;
use App\Services\External\Concerns\CrawlDataItem;
use App\Services\External\Concerns\CrawlerHelper;
use App\Services\External\Concerns\CrawlParser;
use App\Services\External\Concerns\CrawlRelatedModels;
use App\Services\External\Concerns\CrawlReport;
use App\Services\External\Concerns\CrawlTask;
use App\Services\External\Contracts\CrawlActivities;
use App\Services\External\Contracts\CrawlArticles;
use App\Services\External\Contracts\CrawlEvents;
use App\Services\External\Contracts\CrawlOrganizations;
use App\Services\External\Contracts\ExternalService;
use Carbon\Carbon;
use Closure;
use DateTimeImmutable;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Token\Parser;
use Lcobucci\JWT\Token\RegisteredClaims;
use RuntimeException;
use Throwable;

/**
 * Class InfoLocaleService.
 *
 * @see SourceOrigin
 */
class InfoLocaleService implements ExternalService, CrawlArticles, CrawlActivities, CrawlEvents, CrawlOrganizations
{
    /**
     * Option to only crawl announcements with accessibility information.
     */
    private const OPTION_ONLY_ACCESSIBLE = 'only-accessible';

    /**
     * Map of organization "subtype" with organization type codes.
     */
    private const ORGANIZATION_TYPE_CODES_MAP = [
        781  => OrganizationTypeCode::ASSOCIATION,
        7373 => OrganizationTypeCode::ASSOCIATION,
        780  => OrganizationTypeCode::CIVIL_SERVICE,
        786  => OrganizationTypeCode::CIVIL_SERVICE,
    ];

    /**
     * Map of classification ID inside "announcements" with activity types.
     */
    private const ACTIVITY_TYPES_MAP = [
        'EVT' => ActivityTypeCode::EVENTS,
        'ACT' => ActivityTypeCode::ACTIVITIES,
    ];

    /**
     * Map of accessibility ID inside "announcements" with accessibility categories.
     */
    private const ACCESSIBILITY_CATEGORIES_MAP = [
        0 => AccessibilityCategory::HEARING,
        1 => AccessibilityCategory::VISUAL,
        2 => AccessibilityCategory::COGNITIVE,
        3 => AccessibilityCategory::PHYSICAL,
    ];

    /**
     * Map of public age ID inside "announcements" with public codes.
     */
    private const PUBLIC_CODES_MAP = [
        1 => ActivityPublicCode::ALL,
        2 => ActivityPublicCode::BABY,
        3 => ActivityPublicCode::CHILDREN,
        4 => ActivityPublicCode::TEENAGER,
        5 => ActivityPublicCode::ADULT,
        6 => ActivityPublicCode::SENIOR,
    ];

    /**
     * InfoLocaleService constructor.
     *
     * @param AppConfig   $appConfig
     * @param CrawlParser $crawlParser
     */
    public function __construct(
        private readonly AppConfig $appConfig,
        private readonly CrawlParser $crawlParser,
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function serviceName(): string
    {
        return SourceOrigin::INFOLOCALE->value;
    }

    /**
     * {@inheritDoc}
     */
    public function serviceConfigured(string $interfaceNeeded): bool
    {
        return ! empty($this->getAccessToken())
            && ($interfaceNeeded !== CrawlOrganizations::class || $this->getOrganizationsFlowId())
            && ($interfaceNeeded !== CrawlArticles::class || $this->getPublicationsFlowId())
            && ($interfaceNeeded !== CrawlActivities::class || $this->getAnnouncementsFlowId())
            && ($interfaceNeeded !== CrawlEvents::class || $this->getAnnouncementsFlowId());
    }

    /*
    |--------------------------------------------------------------------------
    | Categories
    |--------------------------------------------------------------------------
    */

    /**
     * Crawl categories using given crawl function.
     *
     * @param Closure        $crawlData
     * @param CrawlTask|null $task
     * @param bool           $keepModels
     *
     * @return CrawlReport
     */
    private function crawlCategoriesFrom(
        Closure $crawlData,
        CrawlTask | null $task = null,
        bool $keepModels = false,
    ): CrawlReport {
        $handleData = function (CrawlData $data, CrawlReport $report) use ($task) {
            $categories = CrawlerHelper::sourceableQuery(Category::query(), $data)->get();

            $data->data->each(function (CrawlDataItem $item) use ($categories, $task, $report) {
                /** @var Category|null $category */
                [$category, $done] = CrawlerHelper::sourceableUse(
                    $task,
                    $report,
                    $categories,
                    new Category(),
                    $item,
                );
                if (! $category) {
                    return;
                }

                if ($this->shouldDraftNewCategories() && ! $category->exists) {
                    $category->published_at = null;
                }

                CrawlerHelper::sourceableFillAndSave($category, $item, [
                    'name' => $item->value('libelle', $this->crawlParser->translationParser()),
                ]);

                $done();
            });
        };

        return CrawlerHelper::run($crawlData, $handleData, fn() => null, $task, $keepModels);
    }

    /**
     * Make a category item from activity data.
     *
     * @param string     $type
     * @param array|null $data
     *
     * @return CrawlDataItem|null
     */
    private function makeCategoryItemFromActivity(string $type, array | null $data): CrawlDataItem | null
    {
        if (! ($data['libelle'] ?? null)) {
            return null;
        }

        return new CrawlDataItem(
            $data['libelle'],
            [['type' => $type, 'id' => (string) $data['id'], 'uuid' => (string) $data['uuid']]],
            $data,
        );
    }

    /**
     * Make a categories items from organizations.
     *
     * @param CrawlDataItem $item
     *
     * @return Collection<int, CrawlDataItem>
     */
    private function makeCategoriesItemsFromOrganization(CrawlDataItem $item): Collection
    {
        $categories = new Collection();
        $pushCategoryItem = fn(array $activity) => $categories->push(
            $this->makeCategoryItemFromActivity('activite', $activity),
        );
        $forEachActivity = static function (array $activities) use ($pushCategoryItem, &$forEachActivity) {
            collect($activities)->each(
                static function (array $activity) use ($pushCategoryItem, &$forEachActivity) {
                    $pushCategoryItem($activity);
                    $forEachActivity($activity['activites'] ?? []);
                },
            );
        };

        $forEachActivity($item->value('activites') ?? []);

        return $categories->unique(static fn(CrawlDataItem $item) => $item->sources[0]['id']);
    }

    /**
     * Crawl categories from an organization crawled data.
     *
     * @param CrawlData $data
     *
     * @return Collection
     */
    private function crawlCategoriesFromOrganizations(CrawlData $data): Collection
    {
        $report = $this->crawlCategoriesFrom(fn() => new CrawlData(
            source: SourceOrigin::INFOLOCALE,
            data: $data->data,
            more: false,
            factory: fn(CrawlDataItem $item) => $this->makeCategoriesItemsFromOrganization($item),
        ), keepModels: true);

        return $report->models();
    }

    /**
     * Crawl categories from an organization crawled data.
     *
     * @param CrawlData $data
     *
     * @return Collection
     */
    private function crawlCategoriesFromActivities(CrawlData $data): Collection
    {
        $report = $this->crawlCategoriesFrom(fn() => new CrawlData(
            source: SourceOrigin::INFOLOCALE,
            data: $data->data,
            more: false,
            factory: fn(CrawlDataItem $item) => array_filter([
                $this->makeCategoryItemFromActivity('categorie', $item->value('categorie')),
                $this->makeCategoryItemFromActivity('categorie', $item->value('style')),
            ]),
        ), keepModels: true);

        return $report->models();
    }

    /*
    |--------------------------------------------------------------------------
    | Places
    |--------------------------------------------------------------------------
    */

    /**
     * Crawl places using given crawl function.
     *
     * @param Closure        $crawlData
     * @param CrawlTask|null $task
     * @param bool           $keepModels
     *
     * @return CrawlReport
     */
    private function crawlPlacesFrom(
        Closure $crawlData,
        CrawlTask | null $task = null,
        bool $keepModels = false,
    ): CrawlReport {
        $handleData = function (CrawlData $data, CrawlReport $report) use ($task) {
            $places = CrawlerHelper::sourceableQuery(Place::query()->with('categories'), $data)->get();

            $data->data->each(function (CrawlDataItem $item) use ($places, $task, $report) {
                $address = $this->makeAddressFromPlace($item);
                if (! $address) {
                    return;
                }

                /** @var Place|null $place */
                [$place, $done] = CrawlerHelper::sourceableUse(
                    $task,
                    $report,
                    $places,
                    new Place(),
                    $item,
                    fn(Place $place) => (
                        PlacesHelper::distance($place->address['geometry'], $address['geometry']) < 0.25
                    ),
                );

                if (! $place) {
                    return;
                }

                CrawlerHelper::sourceableFillAndSave($place, $item, [
                    'name'    => $item->value('nom', $this->crawlParser->translationParser()),
                    'address' => $address,
                ]);

                $done();
            });
        };

        return CrawlerHelper::run($crawlData, $handleData, fn() => null, $task, $keepModels);
    }

    /**
     * Get an address from a place crawl item.
     *
     * @param CrawlDataItem $item
     *
     * @return array|null
     */
    private function makeAddressFromPlace(CrawlDataItem $item): array | null
    {
        $coordinates = [
            floatval($item->value('longitude')),
            floatval($item->value('latitude')),
        ];

        if ($coordinates[0] === 0.0 && $coordinates[1] === 0.0) {
            return null;
        }

        return [
            'type'       => 'housenumber',
            'address'    => trim($item->value('adresse') ?? $item->value('nom')),
            'city'       => $item->value('commune.nom'),
            'postalCode' => $item->value('commune.codePostal'),
            'inseeCode'  => $item->value('commune.insee'),
            'geometry'   => [
                'type'        => 'Point',
                'coordinates' => $coordinates,
            ],
        ];
    }

    /**
     * Make a place item from organization.
     *
     * @param CrawlDataItem $item
     *
     * @return CrawlDataItem
     */
    private function makePlaceItemFromOrganization(CrawlDataItem $item): CrawlDataItem
    {
        return new CrawlDataItem(
            $item->value('nom'),
            [['id' => (string) $item->value('id'), 'uuid' => (string) $item->value('uuid')]],
            [
                'nom'       => $item->value('nom'),
                'adresse'   => $item->value('lieux.0.adresse') ?? $item->value('lieux.0.nom'),
                'longitude' => $item->value('longitude'),
                'latitude'  => $item->value('latitude'),
                'commune'   => [
                    'nom'        => $item->value('commune'),
                    'codePostal' => $item->value('codePostal'),
                    'insee'      => $item->value('insee'),
                ],
            ],
        );
    }

    /**
     * Crawl places from an organization crawled data.
     *
     * @param CrawlData $data
     *
     * @return Collection
     */
    private function crawlPlacesFromOrganizations(CrawlData $data): Collection
    {
        $report = $this->crawlPlacesFrom(fn() => new CrawlData(
            source: SourceOrigin::INFOLOCALE,
            data: $data->data,
            more: false,
            factory: fn(CrawlDataItem $item) => $this->makePlaceItemFromOrganization($item),
        ), keepModels: true);

        return $report->models();
    }

    /**
     * Make a place item from organization.
     *
     * @param CrawlDataItem $item
     *
     * @return CrawlDataItem|null
     */
    private function makePlaceItemFromActivity(CrawlDataItem $item): CrawlDataItem | null
    {
        if (! $item->value('lieu.nom')) {
            return null;
        }

        return new CrawlDataItem(
            $item->value('lieu.nom'),
            [['id' => (string) $item->value('id'), 'uuid' => (string) $item->value('uuid')]],
            $item->value('lieu'),
        );
    }

    /**
     * Crawl places from an organization crawled data.
     *
     * @param CrawlData $data
     *
     * @return Collection
     */
    private function crawlPlacesFromActivities(CrawlData $data): Collection
    {
        $report = $this->crawlPlacesFrom(fn() => new CrawlData(
            source: SourceOrigin::INFOLOCALE,
            data: $data->data,
            more: false,
            factory: fn(CrawlDataItem $item) => $this->makePlaceItemFromActivity($item),
        ), keepModels: true);

        return $report->models();
    }

    /*
    |--------------------------------------------------------------------------
    | Organizations
    |--------------------------------------------------------------------------
    */

    /**
     * {@inheritDoc}
     */
    public function crawlOrganizations(CrawlTask $task): CrawlReport
    {
        $organizationsFlowId = $this->getOrganizationsFlowId();
        if (! $organizationsFlowId) {
            return CrawlReport::withErrors(
                new RuntimeException('[infolocale] no organizations flow configured, skipping organizations crawl'),
            );
        }

        $crawlData = $this->makeDataCrawler("flux/$organizationsFlowId/data", 'nom', [
            'limit' => 20,
            'order' => 'dateCreation desc',
        ]);

        return $this->crawlOrganizationsFrom($crawlData, $task);
    }

    /**
     * Crawl organizations using given crawl function.
     *
     * @param Closure        $crawlData
     * @param CrawlTask|null $task
     * @param bool           $keepModels
     *
     * @return CrawlReport
     */
    private function crawlOrganizationsFrom(
        Closure $crawlData,
        CrawlTask | null $task = null,
        bool $keepModels = false,
    ): CrawlReport {
        $organizationTypes = OrganizationType::fetchIdsByCodesMap();

        /** @var CrawlRelatedModels|null $categories */
        $categories = null;
        /** @var CrawlRelatedModels|null $places */
        $places = null;

        $clearData = function () use (&$categories, &$places) {
            $categories?->trashUnused();
            $places?->trashUnused();
        };

        $handleData = function (
            CrawlData $data,
            CrawlReport $report,
        ) use (
            $organizationTypes,
            $task,
            &$categories,
            &$places,
        ) {
            $organizations = CrawlerHelper::sourceableQuery(
                Organization::query()->with(['categories', 'media']),
                $data,
            )->get();
            $categories = new CrawlRelatedModels($this->crawlCategoriesFromOrganizations($data));
            $places = new CrawlRelatedModels($this->crawlPlacesFromOrganizations($data));

            $data->data->each(function (CrawlDataItem $item) use (
                $organizationTypes,
                $organizations,
                $categories,
                $places,
                $data,
                $task,
                $report,
            ) {
                /** @var Organization|null $organization */
                [$organization, $done] = CrawlerHelper::sourceableUse(
                    $task,
                    $report,
                    $organizations,
                    new Organization(),
                    $item,
                );
                if (! $organization) {
                    return;
                }

                $media = new Collection($organization->media);

                $links = collect([
                    'website'   => $item->value('contact.siteInternet'),
                    'facebook'  => $item->value('contact.facebook'),
                    'linkedin'  => $item->value('contact.linkedin'),
                    'instagram' => $item->value('contact.instagram'),
                ])->filter();

                /** @var Media|null $cover */
                $cover = $item->value('logo', fn(string $url) => $this->crawlMediaFromPhoto([
                    'path'   => $url,
                    'legend' => $item->name,
                ]));

                $organizationCode = self::ORGANIZATION_TYPE_CODES_MAP[$item->value('sousType')]
                    ?? OrganizationTypeCode::COMPANY;
                $organizationTypeId = $organizationTypes[$organizationCode->value];

                CrawlerHelper::sourceableFillAndSave($organization, $item, [
                    'name'                 => $item->value('nom', $this->crawlParser->translationParser()),
                    'body'                 => $item->value(
                        'presentation',
                        $this->crawlParser->htmlTranslationParser($media),
                    ),
                    'phone'                => $item->value('contact.telephone1', $this->crawlParser->phoneParser()),
                    'email'                => $item->value('contact.email', $this->crawlParser->emailParser()),
                    'links'                => $links->isEmpty() ? null : $links,
                    'cover_id'             => $cover?->id,
                    'organization_type_id' => $organizationTypeId,
                ]);

                /** @var Place|null $place */
                $place = CrawlerHelper::sourceableFind(
                    $places->models(),
                    $this->makePlaceItemFromOrganization($item)?->withParent($data),
                );

                $newCategories = $this->makeCategoriesItemsFromOrganization($item)
                    ->map(static fn(CrawlDataItem $item) => CrawlerHelper::sourceableFind(
                        $categories->models(),
                        $item->withParent($data),
                    ))
                    ->filter();
                if ($newCategories->isNotEmpty() && $organization->categories->isEmpty()) {
                    $categories->markUsed(...$newCategories);
                    $organization->categories()->sync($newCategories->map->id);
                    if ($place && $place->categories->isEmpty()) {
                        $place->categories()->sync($newCategories->map->id);
                    }
                }

                if ($place && ! $place->owner_organization_id) {
                    $places->markUsed($place);
                    $place->owner_organization_id = $organization->id;
                    $place->save(['timestamps' => false]);
                }

                if ($media->isNotEmpty()) {
                    $organization->media()->sync($media->unique->id->map->id);
                }

                $done();
            });
        };

        return CrawlerHelper::run($crawlData, $handleData, $clearData, $task, $keepModels);
    }

    /**
     * Make an organization item from article.
     *
     * @param CrawlDataItem $item
     *
     * @return CrawlDataItem|null
     */
    private function makeOrganizationItemFromArticle(CrawlDataItem $item): CrawlDataItem | null
    {
        if (! $item->value('organisme.nom')) {
            return null;
        }

        return new CrawlDataItem(
            $item->value('organisme.nom'),
            [['id' => $item->value('organisme.id'), 'uuid' => $item->value('organisme.uuid')]],
            $item->value('organisme'),
        );
    }

    /**
     * Crawl organizations from a article crawled data.
     *
     * @param CrawlData $data
     *
     * @return Collection
     */
    private function crawlOrganizationsFromArticles(CrawlData $data): Collection
    {
        $report = $this->crawlOrganizationsFrom(fn() => new CrawlData(
            source: SourceOrigin::INFOLOCALE,
            data: $data->data,
            more: false,
            factory: fn(CrawlDataItem $item) => $this->makeOrganizationItemFromArticle($item),
        ), keepModels: true);

        return $report->models();
    }

    /**
     * Make an organization item from activity.
     *
     * @param CrawlDataItem $item
     *
     * @return CrawlDataItem|null
     */
    private function makeOrganizationItemFromActivity(CrawlDataItem $item): CrawlDataItem | null
    {
        if (! $item->value('organismeNom')) {
            return null;
        }

        return new CrawlDataItem(
            $item->value('organismeNom'),
            [['id' => $item->value('organismeId'), 'uuid' => $item->value('organismeUuid')]],
            [
                'nom'      => $item->value('organismeNom'),
                'sousType' => $item->value('organismeSousType'),
            ],
        );
    }

    /**
     * Crawl organizations from a article crawled data.
     *
     * @param CrawlData $data
     *
     * @return Collection
     */
    private function crawlOrganizationsFromActivities(CrawlData $data): Collection
    {
        $report = $this->crawlOrganizationsFrom(fn() => new CrawlData(
            source: SourceOrigin::INFOLOCALE,
            data: $data->data,
            more: false,
            factory: fn(CrawlDataItem $item) => $this->makeOrganizationItemFromActivity($item),
        ), keepModels: true);

        return $report->models();
    }

    /*
    |--------------------------------------------------------------------------
    | Articles
    |--------------------------------------------------------------------------
    */

    /**
     * {@inheritDoc}
     */
    public function crawlArticles(CrawlTask $task): CrawlReport
    {
        $publicationsFlowId = $this->getPublicationsFlowId();
        if (! $publicationsFlowId) {
            return CrawlReport::withErrors(
                new RuntimeException('[infolocale] no publications flow configured, skipping articles crawl'),
            );
        }

        $crawlData = $this->makeDataCrawler("flux/$publicationsFlowId/data", 'titre', [
            'limit' => 20,
            'order' => 'dateCreation desc',
        ]);

        /** @var CrawlRelatedModels|null $organizations */
        $organizations = null;

        $clearData = function () use (&$organizations) {
            $organizations?->trashUnused();
        };

        $handleData = function (CrawlData $data, CrawlReport $report) use ($task, &$organizations) {
            $articles = CrawlerHelper::sourceableQuery(
                Article::query()->with(['categories', 'media']),
                $data,
            )->get();
            $organizations = new CrawlRelatedModels($this->crawlOrganizationsFromArticles($data));

            $data->data->each(function (CrawlDataItem $item) use ($articles, $organizations, $data, $task, $report) {
                /** @var Article|null $article */
                [$article, $done] = CrawlerHelper::sourceableUse(
                    $task,
                    $report,
                    $articles,
                    new Article(),
                    $item,
                );
                if (! $article) {
                    return;
                }

                $media = new Collection($article->media);

                /** @var Collection<int, Media> $images */
                $images = $item->value('photos', fn(array | null $photos) => Collection::wrap($photos)->map(
                    fn(array $photo) => $this->crawlMediaFromPhoto($photo),
                )->filter());
                /** @var Media|null $cover */
                $cover = $images->shift();

                $createdAt = Carbon::parse($item->value('dateCreation'))->utc();

                /** @var Organization|null $organization */
                $organization = CrawlerHelper::sourceableFind(
                    $organizations->models(),
                    $this->makeOrganizationItemFromArticle($item)?->withParent($data),
                );
                $organizationId = $article->owner_organization_id;
                if (! $organizationId && $organization) {
                    $organizationId = $organization->id;
                    $organizations->markUsed($organization);
                }

                CrawlerHelper::sourceableFillAndSave($article, $item, [
                    'name'                  => $item->value('titre', $this->crawlParser->translationParser()),
                    'body'                  => $item->value('texte', $this->crawlParser->htmlTranslationParser($media)),
                    'created_at'            => $createdAt,
                    'published_at'          => $createdAt,
                    'cover_id'              => $cover?->id,
                    'owner_organization_id' => $organizationId,
                ]);

                if ($images->isNotEmpty()) {
                    $article->images()->sync($images->map->id);
                }

                if ($article->categories->isEmpty() && $organization && $organization->categories->isNotEmpty()) {
                    $article->categories()->sync($organization->categories->map->id);
                }

                if ($media->isNotEmpty()) {
                    $article->media()->sync($media->unique->id->map->id);
                }

                $done();
            });
        };

        return CrawlerHelper::run($crawlData, $handleData, $clearData, $task);
    }

    /*
    |--------------------------------------------------------------------------
    | Activities and events
    |--------------------------------------------------------------------------
    */

    /**
     * {@inheritDoc}
     */
    public function crawlActivities(CrawlTask $task): CrawlReport
    {
        return $this->crawlAnnouncements($task, ActivityTypeCode::ACTIVITIES);
    }

    /**
     * {@inheritDoc}
     */
    public function crawlEvents(CrawlTask $task): CrawlReport
    {
        return $this->crawlAnnouncements($task, ActivityTypeCode::EVENTS);
    }

    /**
     * Crawl announcements to create/update activities and events.
     *
     * @param CrawlTask        $task
     * @param ActivityTypeCode $activityTypeCode
     *
     * @return CrawlReport
     */
    public function crawlAnnouncements(CrawlTask $task, ActivityTypeCode $activityTypeCode): CrawlReport
    {
        $announcementsFlowId = $this->getAnnouncementsFlowId();
        if (! $announcementsFlowId) {
            return CrawlReport::withErrors(
                new RuntimeException('[infolocale] no announcements flow configured, skipping activities crawl'),
            );
        }

        $activityTypeId = ActivityType::findIdByCode($activityTypeCode);

        $crawlData = $this->makeDataCrawler("flux/$announcementsFlowId/data", 'titre', [
            'classification' => array_search($activityTypeCode, self::ACTIVITY_TYPES_MAP),
            'limit'          => 20,
            'order'          => 'dateCreation desc',
        ]);

        /** @var CrawlRelatedModels|null $categories */
        $categories = null;
        /** @var CrawlRelatedModels|null $organizations */
        $organizations = null;
        /** @var CrawlRelatedModels|null $places */
        $places = null;

        $clearData = function () use (&$categories, &$organizations, &$places) {
            $categories?->trashUnused();
            $organizations?->trashUnused();
            $places?->trashUnused();
        };

        $handleData = function (
            CrawlData $data,
            CrawlReport $report,
        ) use (
            $activityTypeId,
            $task,
            &$categories,
            &$organizations,
            &$places,
        ) {
            $activities = CrawlerHelper::sourceableQuery(
                Activity::query()->with(['place', 'place.categories', 'categories', 'media']),
                $data,
            )->get();
            $categories = new CrawlRelatedModels($this->crawlCategoriesFromActivities($data));
            $organizations = new CrawlRelatedModels($this->crawlOrganizationsFromActivities($data));
            $places = new CrawlRelatedModels($this->crawlPlacesFromActivities($data));

            $data->data->each(function (CrawlDataItem $item) use (
                $activityTypeId,
                $activities,
                $categories,
                $organizations,
                $places,
                $data,
                $task,
                $report,
            ) {
                /** @var Activity|null $activity */
                [$activity, $done] = CrawlerHelper::sourceableUse(
                    $task,
                    $report,
                    $activities,
                    new Activity(),
                    $item,
                );
                if (! $activity) {
                    return;
                }

                $accessibility = $this->makeActivityAccessibility($item);
                if ($task->hasOption(self::OPTION_ONLY_ACCESSIBLE) && $accessibility === null) {
                    $report->onIgnored($activity);

                    return;
                }

                $media = new Collection($activity->media);

                /** @var Collection<int, Media> $images */
                $images = $item->value('photos', fn(array | null $photos) => Collection::wrap($photos)->map(
                    fn(array $photo) => $this->crawlMediaFromPhoto($photo),
                )->filter());
                /** @var Media|null $cover */
                $cover = $images->shift();

                $createdAt = Carbon::parse($item->value('dateCreation'))->utc();

                /** @var Organization|null $organization */
                $organization = CrawlerHelper::sourceableFind(
                    $organizations->models(),
                    $this->makeOrganizationItemFromActivity($item)?->withParent($data),
                );
                $organizationId = $activity->owner_organization_id;
                if (! $organizationId && $organization) {
                    $organizationId = $organization->id;
                    $organizations->markUsed($organization);
                }

                $address = null;
                /** @var Place|null $place */
                $place = $item->value('lieu.nom')
                    ? CrawlerHelper::sourceableFind(
                        $places->models(),
                        $this->makePlaceItemFromActivity($item)?->withParent($data),
                    )
                    : null;
                $placeId = $activity->place_id;
                if (! $placeId && $place) {
                    $placeId = $place->id;
                    $places->markUsed($place);
                }

                if (! $placeId && ! $place) {
                    $address = $this->makeAddressFromPlace(new CrawlDataItem('temp-place', [], [
                        ...$item->value('lieu'),
                    ]));
                }

                CrawlerHelper::sourceableFillAndSave($activity, $item, [
                    'name'                  => $item->value('titre', $this->crawlParser->translationParser()),
                    'body'                  => $item->value(
                        'descriptifWeb',
                        $this->crawlParser->htmlTranslationParser($media),
                    ),
                    'address'               => $address,
                    'place_id'              => $placeId,
                    'created_at'            => $createdAt,
                    'published_at'          => $createdAt,
                    'cover_id'              => $cover?->id,
                    'owner_organization_id' => $organizationId,
                    'activity_type_id'      => $activityTypeId,
                    'accessibility'         => $accessibility,
                    'public_codes'          => $this->makeActivityPublicCodes($item),
                    ...$this->makeActivityOpeningData($item),
                    ...$this->makeActivityPricing($item),
                ]);

                if ($images->isNotEmpty()) {
                    $activity->images()->sync($images->map->id);
                }

                /** @var Collection<int, Category> $itemCategories */
                $itemCategories = collect([
                    CrawlerHelper::sourceableFind(
                        $categories->models(),
                        $this->makeCategoryItemFromActivity('categorie', $item->value('categorie'))?->withParent($data),
                    ),
                    CrawlerHelper::sourceableFind(
                        $categories->models(),
                        $this->makeCategoryItemFromActivity('categorie', $item->value('style'))?->withParent($data),
                    ),
                ])->filter();
                $activityCategories = $activity->categories;
                if ($itemCategories->isNotEmpty() && $activityCategories->isEmpty()) {
                    $activity->categories()->sync($itemCategories->map->id);
                    $activityCategories = $itemCategories;
                    $categories->markUsed(...$itemCategories);
                }

                if ($place && $activityCategories->isNotEmpty() && $place->categories->isEmpty()) {
                    $place->categories()->sync($activityCategories->map->id);
                    $place->save(['timestamps' => false]);
                }

                if ($media->isNotEmpty()) {
                    $activity->media()->sync($media->unique->id->map->id);
                }

                $done();
            });
        };

        return CrawlerHelper::run($crawlData, $handleData, $clearData, $task);
    }

    /**
     * Make accessibility information for an activity item.
     *
     * @param CrawlDataItem $item
     *
     * @return array|null
     */
    private function makeActivityAccessibility(CrawlDataItem $item): array | null
    {
        /** @var Collection $accessibility */
        $accessibility = Collection::wrap($item->value('accessibilites'))
            ->reduce(static function (Collection $items, array $item) {
                $accessibilityCategory = self::ACCESSIBILITY_CATEGORIES_MAP[$item['hanId']] ?? null;
                if ($accessibilityCategory) {
                    $items->put($accessibilityCategory->value, ['rating' => 3, 'body' => null]);
                }

                return $items;
            }, new Collection());

        return $accessibility->isEmpty() ? null : $accessibility->all();
    }

    /**
     * Make pricing information for an activity item.
     *
     * @param CrawlDataItem $item
     *
     * @return array
     */
    private function makeActivityPricing(CrawlDataItem $item): array
    {
        $prices = collect($item->value('tarifs') ?? []);
        if ($prices->isEmpty()) {
            return [
                'pricing_code'        => null,
                'pricing_description' => null,
            ];
        }

        $isFreePrice = static fn(array $price) => $price['gratuit']
            || ($price['payant'] && $price['payantMontant'] == 0);
        $isFree = $prices->some(static fn(array $price) => $isFreePrice($price));
        $isPaid = $prices->some(static fn(array $price) => ! $isFreePrice($price));
        $isPaidDetailed = $prices->some(static fn(array $price) => $price['payant'] || $price['libre']);
        $pricingDescription = $isPaidDetailed
            ? $item->value('tarif', $this->crawlParser->translationParser())
            : null;

        if ($isFree && $isPaid) {
            return [
                'pricing_code'        => ActivityPricingCode::CONDITIONED,
                'pricing_description' => $pricingDescription,
            ];
        }

        if ($isPaid) {
            return [
                'pricing_code'        => ActivityPricingCode::PAID,
                'pricing_description' => $pricingDescription,
            ];
        }

        return [
            'pricing_code'        => ActivityPricingCode::FREE,
            'pricing_description' => null,
        ];
    }

    /**
     * Make publics information for an activity item.
     *
     * @param CrawlDataItem $item
     *
     * @return array
     */
    private function makeActivityPublicCodes(CrawlDataItem $item): array
    {
        $publics = collect($item->value('categoriesAge') ?? [])
            ->map(static fn(array $category) => self::PUBLIC_CODES_MAP[$category['ageId']] ?? null)
            ->filter()
            ->values();
        if (
            $publics->isEmpty()
            || $publics->some(static fn(ActivityPublicCode $code) => $code === ActivityPublicCode::ALL)
        ) {
            $publics = collect([ActivityPublicCode::ALL]);
        }

        return $publics->map(static fn(ActivityPublicCode $code) => $code->value)->all();
    }

    /**
     * Make opening data for an activity item.
     *
     * @param CrawlDataItem $item
     *
     * @return array
     */
    private function makeActivityOpeningData(CrawlDataItem $item): array
    {
        $parseTime = static fn(string $time) => Str::of($time)
            ->explode(':')
            ->take(2)
            ->implode(':');

        /** @var Collection $openingWeekdaysHours */
        $openingWeekdaysHours = Collection::wrap($item->value('horaires'))->reduce(
            static fn(Collection $hours, array $item) => tap(
                $hours,
                function () use ($parseTime, $hours, $item) {
                    $isoWeekday = DateHelper::toIsoWeekday($item['jour']);
                    Collection::wrap($item['plages'])->map(static fn(array $hour) => $hours->push([
                        'weekday' => $isoWeekday,
                        'start'   => $parseTime($hour['debut']),
                        'end'     => $parseTime($hour['fin']),
                    ]));
                },
            ),
            new Collection(),
        );

        if ($openingWeekdaysHours->isNotEmpty()) {
            return [
                'opening_on_periods'     => true,
                'opening_periods'        => Collection::wrap($item->value('dates'))
                    ->map(static fn(array $date) => [
                        'start' => Carbon::parse($date['debut'])->utc()->toDateString(),
                        'end'   => Carbon::parse($date['fin'])->utc()->toDateString(),
                    ]),
                'opening_weekdays_hours' => $openingWeekdaysHours,
            ];
        }

        /** @var Collection $openingDatesHours */
        $openingDatesHours = Collection::wrap($item->value('dateHoraires'))->reduce(
            static fn(Collection $hours, array $item) => tap(
                $hours,
                function () use ($parseTime, $hours, $item) {
                    $date = Carbon::parse($item['date'])->utc()->toDateString();
                    $dateHours = Collection::wrap($item['horaires'])->map(static fn(array $hour) => [
                        'date'  => $date,
                        'start' => $parseTime($hour['debut']),
                        'end'   => $parseTime($hour['fin']),
                    ]);
                    if ($dateHours->isEmpty()) {
                        $dateHours->push([
                            'date'  => $date,
                            'start' => '00:00',
                            'end'   => '00:00',
                        ]);
                    }

                    $hours->push(...$dateHours);
                },
            ),
            new Collection(),
        );

        return [
            'opening_on_periods'  => false,
            'opening_dates_hours' => $openingDatesHours,
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | Concerns
    |--------------------------------------------------------------------------
    */

    /**
     * Check if the given ID match an organizations flow.
     *
     * @param string $id
     *
     * @return bool
     */
    public function isOrganizationsFlow(string $id): bool
    {
        return $this->isFlowOfType($id, 'organismes');
    }

    /**
     * Check if the given ID match a publications flow.
     *
     * @param string $id
     *
     * @return bool
     */
    public function isPublicationsFlow(string $id): bool
    {
        return $this->isFlowOfType($id, 'publications');
    }

    /**
     * Check if the given ID match an announcements flow.
     *
     * @param string $id
     *
     * @return bool
     */
    public function isAnnouncementsFlow(string $id): bool
    {
        return $this->isFlowOfType($id, 'annonces');
    }

    /**
     * Check if the given ID match a flow of given type.
     *
     * @param string $id
     * @param string $type
     *
     * @return bool
     */
    private function isFlowOfType(string $id, string $type): bool
    {
        try {
            $data = $this->executeRequest(
                fn() => $this->prepareRequest()
                    ->get('flux/' . $id),
            )->json();
        } catch (Throwable) {
            return false;
        }

        return $data['type'] === $type;
    }

    /**
     * Make a new InfoLocale data crawler function.
     *
     * @param string $endpoint
     * @param string $nameKey
     * @param array  $params
     *
     * @return Closure
     */
    private function makeDataCrawler(
        string $endpoint,
        string $nameKey,
        array $params = [],
    ): Closure {
        $page = 1;
        $prev = 0;

        return function () use ($endpoint, $nameKey, $params, &$page, &$prev) {
            $data = $this->executeRequest(
                fn() => $this->prepareRequest()->post($endpoint, [
                    'page'       => $page,
                    'countTotal' => 'true',
                    ...$params,
                ]),
            )->json();

            $prev += count($data['result']);
            $page += 1;

            return new CrawlData(
                source: SourceOrigin::INFOLOCALE,
                data: $data['result'],
                more: $data['resultCount'] > $prev,
                factory: fn(array $result) => new CrawlDataItem(
                    $result[$nameKey],
                    $this->extractSourcesFromResult($result),
                    $result,
                ),
                total: $data['resultCount'],
            );
        };
    }

    /**
     * Extract sources data from a result.
     *
     * @param array $result
     *
     * @return array
     */
    private function extractSourcesFromResult(array $result): array
    {
        $source = [
            'id'   => (string) $result['id'],
            'uuid' => (string) $result['uuid'],
        ];
        if ($result['url']['il'] ?? null) {
            $source['website'] = $result['url']['il'];
        }

        return [$source];
    }

    /**
     * Crawl a media from a photo array.
     *
     * @param array $photo
     *
     * @return Media|null
     */
    private function crawlMediaFromPhoto(array $photo): Media | null
    {
        $url = $photo['path'];
        $lastHttps = mb_strrpos($url, 'https://') ?: mb_strrpos($url, 'http://');
        if ($lastHttps !== false && $lastHttps !== 0) {
            $url = Str::substr($url, $lastHttps);
        }

        $media = $this->crawlParser->parseMedia(
            $url,
            ($photo['legend'] ?? null) ?: 'image',
            [
                'source'  => $url,
                'legend'  => ($photo['legend'] ?? null) ?: null,
                'credits' => ($photo['credits'] ?? null) ?: null,
            ],
        );
        if ($media && $media->type === Media::TYPE_IMAGE) {
            return $media;
        }

        return null;
    }

    /**
     * Authenticate to InfoConnect using an email/password login.
     *
     * @param string $email
     * @param string $password
     *
     * @return array
     */
    public function authenticateWithLogin(string $email, string $password): array
    {
        return $this->authenticate('signin', [
            'login'    => $email,
            'password' => $password,
        ]);
    }

    /**
     * Authenticate to InfoConnect using a refresh token.
     *
     * @return array
     */
    private function authenticateWithRefreshToken(): array
    {
        return $this->authenticate('refresh-token', [
            'refresh_token' => $this->getRefreshToken(),
        ]);
    }

    /**
     * Authenticate to InfoConnect.
     *
     * @param string $path
     * @param array  $data
     *
     * @return array
     */
    private function authenticate(string $path, array $data): array
    {
        $data = $this->prepareAuthenticationRequest()
            ->post("https://api.infolocale.fr/v2/auth/$path", $data)
            ->json();

        return $this->parseAuthenticationData($data);
    }

    /**
     * Run a request using its request factory. Reauthenticate if needed.
     *
     * @param Closure $factory
     * @param bool    $retry
     *
     * @return Response
     */
    private function executeRequest(Closure $factory, bool $retry = true): Response
    {
        try {
            return $factory();
        } catch (Throwable $exception) {
            if ($this->isAuthenticationException($exception) && $retry) {
                $config = $this->appConfig->value();
                $data = $this->authenticateWithRefreshToken();

                $this->appConfig->updateService($config, 'infolocale', $data, true);

                return $this->executeRequest($factory, retry: false);
            }

            throw $exception;
        }
    }

    /**
     * Prepare an authenticated request.
     *
     * @return PendingRequest
     */
    private function prepareRequest(): PendingRequest
    {
        return $this->prepareBaseRequest()
            ->withToken($this->getAccessToken())
            ->retry(3, 1000, fn(Throwable $exception) => ! $this->isAuthenticationException($exception));
    }

    /**
     * Check if an exception is an authentication exception.
     *
     * @param Throwable $exception
     *
     * @return bool
     */
    private function isAuthenticationException(Throwable $exception): bool
    {
        return $exception instanceof RequestException && $exception->response->status() === 401;
    }

    /**
     * Prepare a request to authenticate against API.
     *
     * @return PendingRequest
     */
    private function prepareAuthenticationRequest(): PendingRequest
    {
        return $this->prepareBaseRequest()->asForm();
    }

    /**
     * Prepare a request to InfoLocale API.
     *
     * @return PendingRequest
     */
    private function prepareBaseRequest(): PendingRequest
    {
        return Http::baseUrl('https://api.infolocale.fr/v2')
            ->acceptJson()
            ->throw();
    }

    /**
     * Get configured access token.
     *
     * @return string|null
     */
    private function getAccessToken(): string | null
    {
        return $this->appConfig->service('infolocale.accessToken');
    }

    /**
     * Get configured refresh token.
     *
     * @return string|null
     */
    private function getRefreshToken(): string | null
    {
        return $this->appConfig->service('infolocale.refreshToken');
    }

    /**
     * Get configured organizations flow ID.
     *
     * @return string|null
     */
    private function getOrganizationsFlowId(): string | null
    {
        return $this->appConfig->service('infolocale.organizationsId');
    }

    /**
     * Get configured publications flow ID.
     *
     * @return string|null
     */
    private function getPublicationsFlowId(): string | null
    {
        return $this->appConfig->service('infolocale.publicationsId');
    }

    /**
     * Get configured announcements flow ID.
     *
     * @return string|null
     */
    private function getAnnouncementsFlowId(): string | null
    {
        return $this->appConfig->service('infolocale.announcementsId');
    }

    /**
     * Check if new categories should be marked unpublished.
     *
     * @return bool
     */
    private function shouldDraftNewCategories(): bool
    {
        return $this->appConfig->service('infolocale.draftNewCategories') ?? false;
    }

    /**
     * Parse the authentication data from an InfoLocale sign in/refresh response.
     *
     * @param array $data
     *
     * @return array
     */
    private function parseAuthenticationData(array $data): array
    {
        $parser = new Parser(new JoseEncoder());
        $token = $parser->parse($data['refresh_token']);
        /** @var DateTimeImmutable $exp */
        $exp = $token->claims()->get(RegisteredClaims::EXPIRATION_TIME);

        return [
            'accessToken'  => $data['access_token'],
            'refreshToken' => $data['refresh_token'],
            'username'     => $data['username'],
            'expireAt'     => $exp->getTimestamp(),
        ];
    }
}
