<?php

namespace App\Services\External;

use App\Features\FeatureName;
use App\Features\Features;
use App\Jobs\External\SendToExternalServiceJob;
use App\Models\Contracts\Publishable;
use App\Models\Contracts\Sourceable;
use App\Services\External\Concerns\CrawlTask;
use App\Services\External\Contracts\CrawlActivities;
use App\Services\External\Contracts\CrawlArticles;
use App\Services\External\Contracts\CrawlCategories;
use App\Services\External\Contracts\CrawlEvents;
use App\Services\External\Contracts\CrawlOrganizations;
use App\Services\External\Contracts\CrawlPlaces;
use App\Services\External\Contracts\ExternalService;
use App\Services\External\Contracts\SendPlaces;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use InvalidArgumentException;

/**
 * Class ExternalServicesManager.
 */
class ExternalServicesManager
{
    /**
     * @var array<string, array> $crawlingMethods Map of contents which can be crawled with interfaces and methods.
     */
    private readonly array $crawlingMethods;

    /**
     * @var array<string, array> $sendingMethods Map of contents which can be sent with interfaces and methods.
     */
    private readonly array $sendingMethods;

    /**
     * @var array<int, ExternalService> $services Array of available external services.
     */
    private readonly array $services;

    /**
     * ExternalServicesManager constructor.
     *
     * @param Dispatcher        $dispatcher
     * @param AcceslibreService $acceslibreCrawler
     * @param InfoLocaleService $infoLocaleCrawler
     */
    public function __construct(
        private readonly Dispatcher $dispatcher,
        AcceslibreService $acceslibreCrawler,
        InfoLocaleService $infoLocaleCrawler,
    ) {
        $this->crawlingMethods = [
            'categories'    => [CrawlCategories::class, 'crawlCategories'],
            'places'        => [CrawlPlaces::class, 'crawlPlaces'],
            'articles'      => [CrawlArticles::class, 'crawlArticles'],
            'activities'    => [CrawlActivities::class, 'crawlActivities'],
            'events'        => [CrawlEvents::class, 'crawlEvents'],
            'organizations' => [CrawlOrganizations::class, 'crawlOrganizations'],
        ];

        $this->sendingMethods = [
            'places' => [SendPlaces::class, 'sendPlace'],
        ];

        // InfoLocale should be run first because its data are less qualified.
        $this->services = [
            $infoLocaleCrawler->serviceName() => $infoLocaleCrawler,
            $acceslibreCrawler->serviceName() => $acceslibreCrawler,
        ];
    }

    /**
     * Run a crawl for a given contents type.
     *
     * @param CrawlTask $task
     * @param array     $types
     *
     * @return void
     */
    public function crawl(CrawlTask $task, array $types): void
    {
        $this->detectInterfacesAndMethods(collect($types))->map(function (array $params) use ($task) {
            collect($this->services)->each(function (ExternalService $service) use ($task, $params) {
                [$type, $interface, $method] = $params;
                if ($service instanceof $interface) {
                    if ($task->startCallback) {
                        call_user_func_array($task->startCallback, [$type, $service]);
                    }

                    if (! $this->shouldRun($service, $type, $interface)) {
                        if ($task->skipCallback) {
                            call_user_func_array($task->skipCallback, [$type, $service]);
                        }

                        return;
                    }

                    $report = call_user_func_array([$service, $method], [$task]);

                    if ($task->endCallback) {
                        call_user_func_array($task->endCallback, [$type, $service, $report]);
                    }
                }
            });
        });
    }

    /**
     * Dispatch a sourceable update job if needed.
     *
     * @param Model&Sourceable $model
     *
     * @return void
     */
    public function dispatchSend(Sourceable&Model $model): void
    {
        if (
            $model->isSyncedWithSources()
            || ($model instanceof Publishable && ! $model->published())
        ) {
            return;
        }

        $type = Str::plural(Str::kebab(class_basename($model)));
        [$interface, $method] = $this->sendingMethods[$type] ?? [null, null];
        if ($interface === null) {
            return;
        }

        collect($this->services)->each(
            function (ExternalService $service) use ($type, $interface, $method, $model) {
                if ($service instanceof $interface && $this->shouldRun($service, $type, $interface)) {
                    $this->dispatcher->dispatch(
                        new SendToExternalServiceJob($model, $service->serviceName(), $method),
                    );
                }
            },
        );
    }

    /**
     * Send a sourceable to external services.
     *
     * @param Model&Sourceable $model
     * @param string           $serviceName
     * @param string           $methodName
     *
     * @return void
     */
    public function executeSend(Sourceable&Model $model, string $serviceName, string $methodName): void
    {
        $service = $this->services[$serviceName];

        call_user_func_array([$service, $methodName], [$model]);
    }

    /**
     * Get the available content types.
     *
     * @return array
     */
    public function getAvailableContentTypes(): array
    {
        return array_keys($this->crawlingMethods);
    }

    /**
     * Check if a crawler should be run.
     *
     * @param ExternalService $service
     * @param string          $type
     * @param string          $interface
     *
     * @return bool
     */
    private function shouldRun(ExternalService $service, string $type, string $interface): bool
    {
        if (! $service->serviceConfigured($interface)) {
            return false;
        }

        if ($type === 'organizations' || $type === 'categories') {
            return Features::check(FeatureName::PLACES)
                || Features::check(FeatureName::ARTICLES)
                || Features::check(FeatureName::ACTIVITIES)
                || Features::check(FeatureName::EVENTS);
        }

        return Features::check(FeatureName::from(Str::upper($type)));
    }

    /**
     * Detect interfaces and methods to use for types.
     *
     * @param Collection $types
     *
     * @return Collection
     */
    private function detectInterfacesAndMethods(Collection $types): Collection
    {
        return $types->map(function (string $type) {
            $interfaceAndMethod = $this->crawlingMethods[$type] ?? null;
            if (! $interfaceAndMethod) {
                throw new InvalidArgumentException(
                    'Invalid content type given, choose between: '
                    . implode(', ', $this->getAvailableContentTypes()),
                );
            }

            return [$type, ...$interfaceAndMethod];
        });
    }
}
