<?php

namespace App\Services\External\Concerns;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use RuntimeException;
use Throwable;

/**
 * Class CrawlReport.
 */
class CrawlReport
{
    /**
     * @var Collection<int, Model> Collection of created models.
     */
    private readonly Collection $created;

    /**
     * @var Collection<int, Model> Collection of updated models.
     */
    private readonly Collection $updated;

    /**
     * @var Collection<int, Model> Collection of ignored models.
     */
    private readonly Collection $ignored;

    /**
     * @var Collection<int, Throwable> Collection of occurred errors.
     */
    public readonly Collection $errors;

    /**
     * @var int|null Total count of potentially crawled data (if possible to define).
     */
    private int | null $totalCount = 0;

    /**
     * @var int Count of created models.
     */
    private int $createdCount = 0;

    /**
     * @var int Count of updated models.
     */
    private int $updatedCount = 0;

    /**
     * @var int Count of ignored models.
     */
    private int $ignoredCount = 0;

    /**
     * Create a crawler report with given errors.
     *
     * @param Throwable|Collection $errors
     *
     * @return static
     */
    public static function withErrors(Throwable | Collection $errors): static
    {
        $report = new static();
        $report->errors->push(...Collection::wrap($errors));

        return $report;
    }

    /**
     * CrawlReport constructor.
     *
     * @param CrawlTask|null $task
     * @param bool           $keepModels
     */
    public function __construct(
        private readonly CrawlTask | null $task = null,
        private readonly bool $keepModels = false,
    ) {
        $this->created = new Collection();
        $this->updated = new Collection();
        $this->ignored = new Collection();
        $this->errors = new Collection();
    }

    /**
     * Get all handled models.
     *
     * @return Collection
     */
    public function models(): Collection
    {
        if (! $this->keepModels) {
            throw new RuntimeException('trying to get models of not keeping crawl report');
        }

        return new Collection([
            ...$this->created,
            ...$this->updated,
            ...$this->ignored,
        ]);
    }

    /**
     * Define the total count if available.
     *
     * @param int|null $totalCount
     *
     * @return void
     */
    public function withTotalCount(int | null $totalCount): void
    {
        if ($totalCount !== null) {
            $this->totalCount = $totalCount;
        }
    }

    /**
     * Get the total count if available.
     *
     * @return int|null
     */
    public function totalCount(): int | null
    {
        return $this->totalCount;
    }

    /**
     * Count handled models.
     *
     * @return int
     */
    public function count(): int
    {
        return $this->createdCount() + $this->updatedCount() + $this->ignoredCount();
    }

    /**
     * Count created models.
     *
     * @return int
     */
    public function createdCount(): int
    {
        return $this->createdCount;
    }

    /**
     * Count updated models.
     *
     * @return int
     */
    public function updatedCount(): int
    {
        return $this->updatedCount;
    }

    /**
     * Count ignored models.
     *
     * @return int
     */
    public function ignoredCount(): int
    {
        return $this->ignoredCount;
    }

    /**
     * Handle a model creation.
     *
     * @param Model $model
     *
     * @return void
     *
     * @throws CrawlLimitReachedException
     */
    public function onCreated(Model $model): void
    {
        $this->onProgress();
        $this->createdCount++;
        if ($this->keepModels) {
            $this->created->push($model);
        }
        $this->assertLimitIsNotReached();
    }

    /**
     * Handle a model update.
     *
     * @param Model $model
     *
     * @return void
     *
     * @throws CrawlLimitReachedException
     */
    public function onUpdated(Model $model): void
    {
        $this->onProgress();
        $this->updatedCount++;
        if ($this->keepModels) {
            $this->updated->push($model);
        }
        $this->assertLimitIsNotReached();
    }

    /**
     * Handle an ignored model.
     *
     * @param Model $model
     *
     * @return void
     *
     * @throws CrawlLimitReachedException
     */
    public function onIgnored(Model $model): void
    {
        $this->onProgress();
        $this->ignoredCount++;
        if ($this->keepModels) {
            $this->ignored->push($model);
        }
        $this->assertLimitIsNotReached();
    }

    /**
     * Handle an error.
     *
     * @param Throwable $exception
     *
     * @return void
     */
    public function onError(Throwable $exception): void
    {
        $this->onProgress();
        $this->errors->push($exception);
    }

    /**
     * Handle progress of any kind.
     *
     * @return void
     */
    private function onProgress(): void
    {
        $this->task?->progressBar?->advance();
        $this->task?->progressIndicator?->advance();
    }

    /**
     * Assert that the crawling limit is not reached.
     *
     * @return void
     *
     * @throws CrawlLimitReachedException
     */
    private function assertLimitIsNotReached(): void
    {
        if ($this->task && $this->task->limit !== null) {
            $current = max($this->count(), $this->errors->count());
            if ($current >= $this->task->limit) {
                throw new CrawlLimitReachedException($this->task->limit);
            }
        }
    }
}
