<?php

namespace App\Services\External\Concerns;

use Closure;
use Illuminate\Support\Arr;

/**
 * Class CrawlDataItem.
 */
class CrawlDataItem
{
    /**
     * @var CrawlData|null Parent owning data collection.
     */
    public CrawlData|null $parent = null;

    /**
     * @var array Sources metadata array for item.
     */
    public array $sources;

    /**
     * CrawlDataItem constructor.
     *
     * @param string $name
     * @param array  $sources
     * @param mixed  $raw
     */
    public function __construct(
        public readonly string $name,
        array $sources,
        public readonly mixed $raw,
    ) {
        $this->sources = array_map(
            static fn(array $source) => array_filter($source, static fn($v) => $v !== null && $v !== ''),
            $sources,
        );
    }

    /**
     * Get a value by key.
     *
     * @param string       $key
     * @param Closure|null $parser
     *
     * @return mixed
     */
    public function value(string $key, Closure|null $parser = null): mixed
    {
        $value = Arr::get($this->raw, $key);
        $trimmedValue = is_string($value) ? trim($value) : $value;
        if ($trimmedValue === null || $trimmedValue === '') {
            return null;
        }

        $trimmedValue = is_string($value) ? trim($value) : $value;

        return $parser ? $parser($trimmedValue) : $trimmedValue;
    }

    /**
     * Register parent data.
     *
     * @param CrawlData $data
     *
     * @return $this
     */
    public function withParent(CrawlData $data): static
    {
        $this->parent = $data;

        return $this;
    }
}
