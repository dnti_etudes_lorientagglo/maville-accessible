<?php

namespace App\Services\External\Concerns;

use RuntimeException;

/**
 * Class CrawlLimitReachedException.
 */
class CrawlLimitReachedException extends RuntimeException
{
    /**
     * CrawlLimitReachedException constructor.
     *
     * @param int $limit
     */
    public function __construct(int $limit)
    {
        parent::__construct("crawling limit of $limit reached, stopping");
    }
}
