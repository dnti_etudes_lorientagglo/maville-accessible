<?php

namespace App\Services\External\Concerns;

use Closure;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\ProgressIndicator;

/**
 * Class CrawlTask.
 */
class CrawlTask
{
    /**
     * CrawlTask constructor.
     *
     * @param Closure|null           $startCallback
     * @param Closure|null           $skipCallback
     * @param Closure|null           $endCallback
     * @param int|null               $limit
     * @param array                  $options
     * @param bool                   $noCreate
     * @param bool                   $noUpdate
     * @param ProgressBar|null       $progressBar
     * @param ProgressIndicator|null $progressIndicator
     */
    public function __construct(
        public readonly Closure | null $startCallback = null,
        public readonly Closure | null $skipCallback = null,
        public readonly Closure | null $endCallback = null,
        public readonly int | null $limit = null,
        public readonly array $options = [],
        public readonly bool $noCreate = false,
        public readonly bool $noUpdate = false,
        public readonly ProgressBar | null $progressBar = null,
        public readonly ProgressIndicator | null $progressIndicator = null,
    ) {
    }

    /**
     * Check if the task has an option enabled.
     *
     * @param string $option
     *
     * @return bool
     */
    public function hasOption(string $option): bool
    {
        return in_array($option, $this->options, true);
    }
}
