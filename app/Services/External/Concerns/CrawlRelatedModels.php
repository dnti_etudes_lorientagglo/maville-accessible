<?php

namespace App\Services\External\Concerns;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class CrawlUsableModels.
 */
class CrawlRelatedModels
{
    /**
     * @var Collection Used models.
     */
    private readonly Collection $used;

    /**
     * CrawlUsableModels constructor.
     *
     * @param Collection $models
     */
    public function __construct(private readonly Collection $models)
    {
        $this->used = new Collection();
    }

    /**
     * Get all potentially related models.
     *
     * @return Collection
     */
    public function models(): Collection
    {
        return $this->models;
    }

    /**
     * Mark a related model as used.
     *
     * @param Model ...$models
     *
     * @return void
     */
    public function markUsed(...$models): void
    {
        $this->used->push(...$models);
    }

    /**
     * Trash all unused models (when those were recently created).
     *
     * @return void
     */
    public function trashUnused(): void
    {
        $this->models->each(function (Model $model) {
            if ($model->wasRecentlyCreated && ! $this->used->contains(static fn($m) => $model->is($m))) {
                $model->delete();
            }
        });
    }
}
