<?php

namespace App\Services\External\Concerns;

use App\Helpers\DOMHelper;
use App\Helpers\FilesHelper;
use App\Helpers\LocaleHelper;
use App\Helpers\MediaHelper;
use App\Models\Config;
use App\Models\Media;
use App\Services\App\AppConfig;
use App\Services\Media\MediaService;
use Closure;
use DOMDocument;
use DOMElement;
use DOMException;
use DOMNode;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\RFCValidation;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Propaganistas\LaravelPhone\PhoneNumber;
use Throwable;

/**
 * Class CrawlParser.
 */
class CrawlParser
{
    /**
     * CrawlParser constructor.
     *
     * @param AppConfig    $appConfig
     * @param MediaService $mediaService
     */
    public function __construct(
        private readonly AppConfig $appConfig,
        private readonly MediaService $mediaService,
    ) {
    }

    /**
     * Create a translation array from a string parser.
     *
     * @return Closure
     */
    public function translationParser(): Closure
    {
        return fn(string $value) => $this->parseTranslation($value);
    }

    /**
     * Create a translation array from a string.
     *
     * @param string $value
     *
     * @return array
     */
    public function parseTranslation(string $value): array
    {
        return [LocaleHelper::defaultLocale() => $value];
    }

    /**
     * Create an HTML translation array from a string parser.
     *
     * @param Collection $media
     *
     * @return Closure
     */
    public function htmlTranslationParser(Collection $media): Closure
    {
        return fn(string $value) => $this->parseHtmlTranslation($value, $media);
    }

    /**
     * Create an HTML translation array from a string.
     *
     * @param string     $value
     * @param Collection $media
     *
     * @return array|null
     */
    public function parseHtmlTranslation(
        string $value,
        Collection $media,
    ): array | null {
        try {
            $document = DOMHelper::makeDocument(
                Str::of($value)
                    ->replace('&thinsp;', ' ')
                    ->value(),
            );

            $this->fixHeadingLevels($document);
            $this->fixEmptyParagraphs($document);
            $this->fixMedia($document, $media);

            return $this->parseTranslation(
                DOMHelper::innerHTML($document->getElementsByTagName('body')[0]),
            );
        } catch (Throwable) {
            return null;
        }
    }

    /**
     * Create a media from a URL string.
     *
     * @param string $url
     * @param string $name
     * @param array  $properties
     *
     * @return Media|null
     */
    public function parseMedia(
        string $url,
        string $name = '',
        array $properties = [],
    ): Media | null {
        try {
            $content = Http::get($url)->body();

            $tmpPath = FilesHelper::temporaryFile($content);
            $hash = MediaHelper::computeHashForFile($tmpPath);

            $media = Media::query()
                ->where('collection_name', Config::MEDIA_PUBLIC_COLLECTION)
                ->where('custom_properties->hash', $hash)
                ->first();
            if ($media) {
                return $media;
            }

            return $this->mediaService->createMediaForLocalPath(
                $this->appConfig->value(),
                Config::MEDIA_PUBLIC_COLLECTION,
                $tmpPath,
                $name,
                $hash,
                $properties,
            );
        } catch (Throwable) {
            return null;
        }
    }

    /**
     * Create a phone string from a non formatted phone string.
     *
     * @return Closure
     */
    public function phoneParser(): Closure
    {
        return fn(string $phone) => $this->parsePhone($phone);
    }

    /**
     * Create a phone string from a non formatted phone string.
     *
     * @param string       $phone
     * @param string|array $country
     *
     * @return string|null
     */
    public function parsePhone(string $phone, string | array $country = 'FR'): string | null
    {
        $phoneObject = new PhoneNumber($phone, $country);
        if (! $phoneObject->isValid()) {
            if ($country === 'FR') {
                return $this->parsePhone($phone, []);
            }

            return null;
        }

        return $phoneObject->formatE164();
    }

    /**
     * Create a phone string from a non formatted phone string.
     *
     * @return Closure
     */
    public function emailParser(): Closure
    {
        return fn(string $email) => $this->parseEmail($email);
    }

    /**
     * Create a phone string from a non formatted phone string.
     *
     * @param string $email
     *
     * @return string|null
     */
    public function parseEmail(string $email): string | null
    {
        $parsedEmail = Str::of($email)->lower()->value();
        if (! (new EmailValidator())->isValid($parsedEmail, new RFCValidation())) {
            return null;
        }

        return $parsedEmail;
    }

    /*
    |--------------------------------------------------------------------------
    | HTML parsing
    |--------------------------------------------------------------------------
    */

    /**
     * Fix document empty paragraphs.
     *
     * @param DOMDocument $document
     *
     * @return void
     *
     * @throws DOMException
     */
    private function fixEmptyParagraphs(DOMDocument $document): void
    {
        DOMHelper::replaceTags($document, [
            'p' => static function (DOMElement $paragraphEl) {
                $isEmpty = $paragraphEl->childNodes->length === 0
                    || ! collect($paragraphEl->childNodes)->some(static fn(DOMNode $child) => (
                        $child->nodeType !== XML_TEXT_NODE
                        || preg_replace('/\s+/u', '', $child->textContent) !== ''
                    ));

                return $isEmpty ? null : false;
            },
        ]);
    }

    /**
     * Detect which heading levels are present in document.
     *
     * @param DOMDocument $document
     *
     * @return Collection
     */
    private function detectHeadingLevels(DOMDocument $document): Collection
    {
        return collect(range(1, 6))
            ->filter(static fn(int $level) => $document->getElementsByTagName("h$level")->count() > 0)
            ->values();
    }

    /**
     * Fix document heading hierarchy.
     *
     * @param DOMDocument $document
     *
     * @return void
     *
     * @throws DOMException
     */
    private function fixHeadingLevels(DOMDocument $document): void
    {
        $headingsReplacements = $this->detectHeadingLevels($document)
            ->mapWithKeys(static fn(int $level, int $index) => ["h$level" => 'h' . ($index + 1)]);
        if ($headingsReplacements->isNotEmpty()) {
            DOMHelper::replaceTags($document, $headingsReplacements->all());
        }
    }

    /**
     * Fix document heading hierarchy.
     *
     * @param DOMDocument $document
     * @param Collection  $media
     *
     * @return void
     *
     * @throws DOMException
     */
    private function fixMedia(DOMDocument $document, Collection $media): void
    {
        $makeMediaElement = function (string | null $url, string | null $alt) use ($document, $media) {
            if (! $url) {
                return null;
            }

            $alt = trim($alt ?: '');

            $newMedia = $this->parseMedia($url, $alt, ['source' => $url]);
            if (! $newMedia) {
                return null;
            }

            $media->push($newMedia);

            $mediaEl = $document->createElement('tt-media-image');
            $mediaEl->setAttribute('media', $newMedia->id);
            if ($alt) {
                $mediaEl->setAttribute('alt', $alt);
            }

            return $mediaEl;
        };

        DOMHelper::replaceTags($document, [
            'figure' => function (DOMElement $figureEl) use ($makeMediaElement) {
                /** @var DOMElement|null $imageEl */
                $imageEl = $figureEl->getElementsByTagName('img')[0];
                /** @var DOMElement|null $captionEl */
                $captionEl = $figureEl->getElementsByTagName('figcaption')[0];

                return $makeMediaElement(
                    $imageEl?->getAttribute('src'),
                    $imageEl?->getAttribute('alt') ?: $captionEl->textContent ?: '',
                );
            },
            'img'    => fn(DOMElement $imageEl) => $makeMediaElement(
                $imageEl->getAttribute('src'),
                $imageEl->getAttribute('alt'),
            ),
        ]);
    }
}
