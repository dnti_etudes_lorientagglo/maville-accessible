<?php

namespace App\Services\External\Concerns;

use App\Models\Enums\SourceOrigin;
use Closure;
use Illuminate\Support\Collection;

/**
 * Class CrawlData.
 */
class CrawlData
{
    /**
     * @var Collection<int, CrawlDataItem> The crawled data collection.
     */
    public readonly Collection $data;

    /**
     * CrawlData constructor.
     *
     * @param SourceOrigin $source
     * @param iterable     $data
     * @param bool         $more
     * @param Closure      $factory
     * @param int|null     $total
     */
    public function __construct(
        public readonly SourceOrigin $source,
        iterable $data,
        public readonly bool $more,
        Closure $factory,
        public readonly int|null $total = null,
    ) {
        $this->data = (new Collection($data))->reduce(
            fn(Collection $items, mixed $item) => $items->push(
                ...Collection::wrap(call_user_func_array($factory, [$item]))
                ->map(fn(CrawlDataItem $item) => $item->withParent($this))
            ),
            new Collection(),
        );
    }
}
