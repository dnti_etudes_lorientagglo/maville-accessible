<?php

namespace App\Services\External\Concerns;

use App\Helpers\LocaleHelper;
use App\Models\Concerns\BaseModel;
use App\Models\Contracts\FulltextSearchable;
use App\Models\Contracts\Publishable;
use App\Models\Contracts\Sourceable;
use App\Models\Enums\SourceOrigin;
use App\Models\Source;
use Closure;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Throwable;

/**
 * Class CrawlerHelper.
 */
class CrawlerHelper
{
    /**
     * Run the fetch callback until there is no more results.
     * Execute callback for each fetched crawler data.
     *
     * @param Closure        $fetch
     * @param Closure        $callback
     * @param Closure        $clear
     * @param CrawlTask|null $task
     * @param bool           $keepModels
     *
     * @return CrawlReport
     *
     */
    public static function run(
        Closure $fetch,
        Closure $callback,
        Closure $clear,
        CrawlTask | null $task = null,
        bool $keepModels = false,
    ): CrawlReport {
        $report = new CrawlReport($task, $keepModels);

        $task?->progressBar?->start();
        $task?->progressIndicator?->start('Processing...');

        $more = true;
        $stop = false;
        while ($more) {
            /** @var CrawlData $data */
            $data = call_user_func_array($fetch, []);

            $report->withTotalCount($data->total);

            try {
                DB::transaction(function () use ($data, $callback, $report, &$stop) {
                    try {
                        call_user_func_array($callback, [$data, $report]);
                    } catch (CrawlLimitReachedException) {
                        $stop = true;
                    }
                });
            } catch (Throwable $exception) {
                $report->onError($exception);

                Log::error($exception->getMessage() . "\n" . $exception->getTraceAsString());
            }

            try {
                call_user_func_array($clear, []);
            } catch (Throwable $exception) {
                Log::error($exception->getMessage() . "\n" . $exception->getTraceAsString());
            }

            $more = ! $stop && $data->more;
        }

        $task?->progressBar?->finish();
        $task?->progressIndicator?->finish('Done.');

        return $report;
    }

    /**
     * Filter a builder query to match crawler data existing records in DB.
     *
     * @param Builder   $query
     * @param CrawlData $data
     *
     * @return Builder
     */
    public static function sourceableQuery(Builder $query, CrawlData $data): Builder
    {
        return $query->with('sources')->where(static function (Builder $query) use ($data) {
            $sources = $data->data->reduce(static function (array $sources, CrawlDataItem $item) {
                foreach ($item->sources as $source) {
                    foreach ($source as $key => $value) {
                        $sources[$key] = [...($sources[$key] ?? []), $value];
                    }
                }

                return $sources;
            }, []);

            foreach ($sources as $key => $values) {
                if (! empty($values)) {
                    $query->orWhereHas(
                        'sources',
                        static fn(Builder $query) => $query
                            ->where('name', $data->source->value)
                            ->whereIn($query->qualifyColumn('data') . '->' . $key, $values),
                    );
                }
            }

            $data->data->each(
                static fn(CrawlDataItem $item) => LocaleHelper::allLocalesKeys()->each(
                    static fn(string $locale) => $query->orWhere(DB::raw(
                        'UNACCENT(LOWER(' . $query->qualifyColumn('name') . '->>' . Str::wrap($locale, '\'') . '))',
                    ), self::normalizeStr($item->name)),
                ),
            );

            return $query;
        });
    }

    /**
     * Find a matching model inside a given collection of models for crawler item.
     *
     * @param Collection         $models
     * @param CrawlDataItem|null $item
     * @param Closure|null       $predicate
     *
     * @return Sourceable|null
     */
    public static function sourceableFind(
        Collection $models,
        CrawlDataItem | null $item,
        Closure | null $predicate = null,
    ): Sourceable | null {
        if (! $item) {
            return null;
        }

        return self::findModelFromSource($models, $item)
            ?? self::findModelFromName($models, $item, $predicate);
    }

    /**
     * Use the sourceable if it should not be ignored.
     *
     * @param CrawlTask|null $task
     * @param CrawlReport    $report
     * @param Collection     $models
     * @param Sourceable     $new
     * @param CrawlDataItem  $item
     * @param Closure|null   $predicate
     *
     * @return array
     */
    public static function sourceableUse(
        CrawlTask | null $task,
        CrawlReport $report,
        Collection $models,
        Sourceable $new,
        CrawlDataItem $item,
        Closure | null $predicate = null,
    ): array {
        /** @var Sourceable&BaseModel $model */
        $model = CrawlerHelper::sourceableFind($models, $item, $predicate);
        if ($model) {
            if ($task?->noUpdate) {
                $report->onIgnored($model);

                return [null, null];
            }

            // Ignore delayed published model or differing source date
            // from update date (it means user touch model).
            if (! $model->sourced() || ! $model->isSyncedWithSources()) {
                $report->onIgnored($model);
                CrawlerHelper::updateSources($model, $item);

                return [null, null];
            }

            $done = static fn() => $report->onUpdated($model);
        } else {
            if ($task?->noCreate) {
                $report->onIgnored($new);

                return [null, null];
            }

            $model = $new;
            $model->created_at = now();
            if ($model instanceof Publishable) {
                $model->published_at = $model->created_at;
            }

            $models->push($model);

            $done = static fn() => $report->onCreated($model);
        }

        $model->sourced_at = now();
        $model->updated_at = $model->sourced_at;

        return [$model, $done];
    }

    /**
     * Fill and save the given sourceable.
     *
     * @param Sourceable    $model
     * @param CrawlDataItem $item
     * @param array         $values
     *
     * @return void
     */
    public static function sourceableFillAndSave(Sourceable $model, CrawlDataItem $item, array $values): void
    {
        $model->forceFill($values);

        if ($model instanceof FulltextSearchable) {
            $model->syncRelationsSearchIndexesOnSaved = false;
        }

        $model->save(['timestamps' => false]);

        if ($model instanceof FulltextSearchable) {
            $model->syncRelationsSearchIndexesOnSaved = true;
        }

        CrawlerHelper::updateSources($model, $item);
    }

    /**
     * Fill a sourceable model with crawler item source metadata.
     *
     * @param Sourceable    $model
     * @param CrawlDataItem $item
     *
     * @return void
     */
    public static function updateSources(Sourceable $model, CrawlDataItem $item): void
    {
        foreach ($item->sources as $itemSource) {
            $source = self::findMatchingSource($model, $item);
            if (! $source) {
                $newSource = new Source();
                $newSource->name = $item->parent->source->value;
                $newSource->data = $itemSource;
                $newSource->sourceable()->associate($model);
                $newSource->save();

                $model->sources->push($newSource);
            }
        }
    }

    /**
     * Find a matching source item on model.
     *
     * @param Sourceable    $model
     * @param CrawlDataItem $item
     *
     * @return Source|null
     */
    public static function findMatchingSource(
        Sourceable $model,
        CrawlDataItem $item,
    ): Source | null {
        return $model->sources->first(function (Source $source) use ($item) {
            if ($source->name !== $item->parent->source->value) {
                return false;
            }

            foreach ($item->sources as $itemSource) {
                $itemType = $itemSource['type'] ?? null;
                if (($source->data['type'] ?? null) !== $itemType) {
                    return false;
                }

                foreach ($itemSource as $key => $value) {
                    // Ignore special sources data keys.
                    if (in_array($key, ['website', 'type'])) {
                        continue;
                    }

                    if ($value === null || $value === '' || ($source->data[$key] ?? null) !== $value) {
                        return false;
                    }
                }
            }

            return true;
        });
    }

    /**
     * Find a matching model inside a given collection using source metadata.
     *
     * @param Collection    $models
     * @param CrawlDataItem $item
     * @param Closure|null  $predicate
     *
     * @return Sourceable|null
     */
    public static function findModelFromSource(
        Collection $models,
        CrawlDataItem $item,
        Closure | null $predicate = null,
    ): Sourceable | null {
        return $models->first(static function (Sourceable $model) use ($item, $predicate) {
            if (self::findMatchingSource($model, $item)) {
                return value($predicate ?? true, $model, $item);
            }

            return false;
        });
    }

    /**
     * Find a matching model inside a given collection using name.
     *
     * @param Collection    $models
     * @param CrawlDataItem $item
     * @param Closure|null  $predicate
     *
     * @return Sourceable|null
     */
    public static function findModelFromName(
        Collection $models,
        CrawlDataItem $item,
        Closure | null $predicate = null,
    ): Sourceable | null {
        $normalizedName = self::normalizeStr($item->name);

        return $models->first(static fn(Sourceable $model) => LocaleHelper::allLocalesKeys()->some(
            static fn(string $locale) => (
                self::normalizeStr($model->name[$locale] ?? '') === $normalizedName
                && value($predicate ?? true, $model, $item)
            ),
        ));
    }

    /**
     * Normalize a name before comparison.
     *
     * @param string $str
     *
     * @return string
     */
    private static function normalizeStr(string $str): string
    {
        return trim(Str::ascii(Str::lower($str)));
    }
}
