<?php

namespace App\Services\Cache;

use Carbon\Carbon;
use Closure;
use Illuminate\Contracts\Cache\Repository as CacheRepository;
use Illuminate\Contracts\Foundation\Application;

/**
 * Class CachedValue.
 */
class CachedValue
{
    /**
     * @var mixed|null The currently resolved value.
     */
    private mixed $value = null;

    /**
     * CachedValue constructor.
     *
     * @param Application     $app
     * @param CacheRepository $cache
     * @param string          $key
     * @param Closure         $factory
     */
    public function __construct(
        private readonly Application $app,
        private readonly CacheRepository $cache,
        private readonly string $key,
        private readonly Closure $factory,
    ) {
    }

    /**
     * Get value.
     *
     * @return mixed
     */
    public function value(): mixed
    {
        if ($this->value === null) {
            $this->value = $this->cache->remember($this->key, $this->duration(), $this->factory);
        }

        return $this->value;
    }

    /**
     * Forget cached value.
     *
     * @return void
     */
    public function forget(): void
    {
        $this->cache->forget($this->key);
        $this->value = null;
    }

    /**
     * Get cache expiration date.
     *
     * @return Carbon
     */
    private function duration(): Carbon
    {
        return $this->app->environment('local')
            ? now()->addSecond()
            : now()->addHour();
    }
}
