<?php

namespace App\Services\Cache;

use Closure;
use Illuminate\Contracts\Cache\Repository as CacheRepository;
use Illuminate\Contracts\Foundation\Application;

/**
 * Class CachedValueFactory.
 */
class CachedValueFactory
{
    /**
     * CachedValueFactory constructor.
     *
     * @param Application     $app
     * @param CacheRepository $cache
     */
    public function __construct(
        private readonly Application $app,
        private readonly CacheRepository $cache,
    ) {
    }

    /**
     * Make a cached value.
     *
     * @param string  $key
     * @param Closure $factory
     *
     * @return CachedValue
     */
    public function make(
        string $key,
        Closure $factory,
    ): CachedValue {
        return new CachedValue($this->app, $this->cache, $key, $factory);
    }
}
