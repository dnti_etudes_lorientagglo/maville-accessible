<?php

namespace App\Services\Validation;

use Illuminate\Validation\Validator as BaseValidator;

/**
 * Class Validator.
 *
 * Extend the validator so displayable attribute are the same as attributes.
 */
class Validator extends BaseValidator
{
    /**
     * {@inheritDoc}
     */
    public function getDisplayableAttribute($attribute): string
    {
        return ":$attribute";
    }
}
