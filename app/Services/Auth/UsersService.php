<?php

namespace App\Services\Auth;

use App\Helpers\NotificationsHelper;
use App\Models\Report;
use App\Models\Request;
use App\Models\Review;
use App\Models\User;
use App\Notifications\Auth\AccountDeleted;
use App\Notifications\Auth\ScheduledAccountDeletion;
use Carbon\Carbon;

/**
 * Class UsersService.
 */
class UsersService
{
    /**
     * Schedule all not recently logger users' deletion.
     *
     * @param Carbon $now
     *
     * @return int
     */
    public function scheduleDeletions(Carbon $now): int
    {
        return User::query()
            ->whereNull('scheduled_deletion_at')
            ->whereNotNull('last_login_at')
            ->whereDate('last_login_at', '<', $now->subMonths(23))
            ->get()
            ->each(fn(User $user) => $this->scheduleDeletionAt($user, $now))
            ->count();
    }

    /**
     * Run all scheduled users deletions.
     *
     * @param Carbon $now
     *
     * @return int
     */
    public function deleteScheduled(Carbon $now): int
    {
        return User::query()
            ->whereNotNull('scheduled_deletion_at')
            ->whereDate('scheduled_deletion_at', '<', $now)
            ->get()
            ->each(fn(User $user) => $this->delete($user))
            ->count();
    }

    /**
     * Schedule the user deletion.
     *
     * @param User   $user
     * @param Carbon $now
     *
     * @return void
     */
    public function scheduleDeletionAt(User $user, Carbon $now): void
    {
        $scheduledAt = $now->addDays(31);

        $user->scheduled_deletion_at = $scheduledAt;
        $user->save();

        NotificationsHelper::notify($user, new ScheduledAccountDeletion($scheduledAt));
    }

    /**
     * Run a user deletion.
     *
     * @param User $user
     *
     * @return void
     */
    private function delete(User $user): void
    {
        Report::query()
            ->whereBelongsTo($user, 'ownerUser')
            ->delete();
        Request::query()
            ->whereBelongsTo($user, 'ownerUser')
            ->delete();
        Review::query()
            ->whereBelongsTo($user, 'ownerUser')
            ->delete();

        $user->delete();

        NotificationsHelper::notifyNow($user, new AccountDeleted());
    }
}
