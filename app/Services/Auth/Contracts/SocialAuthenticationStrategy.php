<?php

namespace App\Services\Auth\Contracts;

use App\Models\User;
use Laravel\Socialite\AbstractUser;
use Laravel\Socialite\Two\ProviderInterface;

/**
 * Interface SocialAuthenticationStrategy.
 */
interface SocialAuthenticationStrategy
{
    /**
     * Check if the strategy can be used.
     *
     * @return bool
     */
    public function activated(): bool;

    /**
     * Create the driver before interacting.
     *
     * @return ProviderInterface
     */
    public function createDriver(): ProviderInterface;

    /**
     * Convert the given socialite user to an internal user model.
     *
     * @param AbstractUser $user
     *
     * @return User
     */
    public function convert(AbstractUser $user): User;
}
