<?php

namespace App\Services\Auth\SocialAuthentication;

use App\Models\User;
use App\Services\Auth\Contracts\SocialAuthenticationStrategy;
use Illuminate\Support\Str;
use Laravel\Socialite\Two\ProviderInterface;
use RuntimeException;
use Throwable;

/**
 * Class SocialAuthenticationManager.
 *
 * Manager for social user conversion to internal user model.
 */
class SocialAuthenticationManager
{
    /**
     * @var SocialAuthenticationStrategy[]
     */
    private readonly array $strategies;

    /**
     * SocialAuthenticationManager constructor.
     *
     * @param GoogleSocialAuthentication        $googleSocialAuthentication
     * @param FacebookSocialAuthentication      $facebookSocialAuthentication
     * @param FranceConnectSocialAuthentication $franceConnectSocialAuthentication
     */
    public function __construct(
        GoogleSocialAuthentication $googleSocialAuthentication,
        FacebookSocialAuthentication $facebookSocialAuthentication,
        FranceConnectSocialAuthentication $franceConnectSocialAuthentication,
    ) {
        $this->strategies = [
            'google'        => $googleSocialAuthentication,
            'facebook'      => $facebookSocialAuthentication,
            'franceConnect' => $franceConnectSocialAuthentication,
        ];
    }

    /**
     * Get the available (configured) drivers.
     *
     * @return SocialAuthenticationStrategy[]
     */
    public function availableStrategies(): array
    {
        return array_filter(
            $this->strategies,
            fn(SocialAuthenticationStrategy $strategy) => $strategy->activated(),
        );
    }

    /**
     * Get the given network's authentication strategy.
     *
     * @param string $network
     *
     * @return SocialAuthenticationStrategy
     */
    public function strategyFor(string $network): SocialAuthenticationStrategy
    {
        $strategy = $this->availableStrategies()[$network] ?? null;
        if (! $strategy) {
            throw new RuntimeException("no driver configured for network $network");
        }

        return $strategy;
    }

    /**
     * Get the given network's driver.
     *
     * @param string $network
     *
     * @return ProviderInterface
     */
    public function driverFor(string $network): ProviderInterface
    {
        return $this->strategyFor(Str::camel($network))->createDriver();
    }

    /**
     * Get the given network's authenticating user.
     *
     * @param string $network
     *
     * @return User
     */
    public function userFor(string $network): User
    {
        $driver = $this->driverFor($network);

        try {
            return $this
                ->strategyFor($network)
                ->convert($driver->user());
        } catch (Throwable) {
            throw new RuntimeException("no user authenticating for network $network");
        }
    }
}
