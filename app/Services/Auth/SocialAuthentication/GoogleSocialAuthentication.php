<?php

namespace App\Services\Auth\SocialAuthentication;

use App\Models\User;
use App\Services\App\AppConfig;
use App\Services\Auth\Contracts\SocialAuthenticationStrategy;
use Laravel\Socialite\AbstractUser;
use Laravel\Socialite\SocialiteManager;
use Laravel\Socialite\Two\GoogleProvider;
use Laravel\Socialite\Two\ProviderInterface;

/**
 * Class GoogleSocialAuthentication.
 */
class GoogleSocialAuthentication implements SocialAuthenticationStrategy
{
    use ConvertsSocialUsers;

    /**
     * FacebookSocialAuthentication constructor.
     *
     * @param SocialiteManager $socialiteManager
     * @param AppConfig        $appConfig
     */
    public function __construct(
        private readonly SocialiteManager $socialiteManager,
        private readonly AppConfig $appConfig,
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function activated(): bool
    {
        return ! empty($this->getConfiguration()['client_id']);
    }

    /**
     * {@inheritDoc}
     */
    public function createDriver(): ProviderInterface
    {
        return $this->socialiteManager->buildProvider(
            GoogleProvider::class,
            $this->getConfiguration(),
        );
    }

    /**
     * {@inheritDoc}
     */
    public function convert(AbstractUser $user): User
    {
        return new User([
            'email' => $user->getEmail(),
            ...$this->decomposeName(
                $user->getName(),
                $user->user['given_name'] ?? null,
                $user->user['family_name'] ?? null,
            ),
        ]);
    }

    /**
     * Get driver configuration.
     *
     * @return array
     */
    private function getConfiguration(): array
    {
        return [
            'client_id'     => $this->appConfig->service('googleLogin.clientId'),
            'client_secret' => $this->appConfig->service('googleLogin.clientSecret'),
            'redirect'      => url('/auth/social/google/callback'),
        ];
    }
}
