<?php

namespace App\Services\Auth\SocialAuthentication;

use Illuminate\Support\Collection;

/**
 * Trait ConvertsSocialUsers.
 */
trait ConvertsSocialUsers
{
    /**
     * Decompose the full name if first or last name are not defined.
     *
     * @param string      $fullName
     * @param string|null $firstName
     * @param string|null $lastName
     * @param bool        $firstNameFirst
     *
     * @return array
     */
    public function decomposeName(
        string $fullName,
        string|null $firstName,
        string|null $lastName,
        bool $firstNameFirst = true,
    ): array {
        if ($firstName === null || $lastName === null) {
            $reverseIfFirstNameFirst = static fn(Collection $c) => $c->when(
                $firstNameFirst,
                static fn(Collection $c) => $c->reverse(),
            );

            $decomposed = $reverseIfFirstNameFirst(collect(preg_split('/\s+/', $fullName)));

            return [
                'first_name' => $decomposed->pop(),
                'last_name'  => $reverseIfFirstNameFirst($decomposed)->join(' '),
            ];
        }

        return [
            'first_name' => $firstName,
            'last_name'  => $lastName,
        ];
    }
}
