<?php

namespace App\Services\Auth\SocialAuthentication;

use App\Models\User;
use App\Services\App\AppConfig;
use App\Services\Auth\Contracts\SocialAuthenticationStrategy;
use Laravel\Socialite\AbstractUser;
use Laravel\Socialite\SocialiteManager;
use Laravel\Socialite\Two\FacebookProvider;
use Laravel\Socialite\Two\ProviderInterface;

/**
 * Class FacebookSocialAuthentication.
 */
class FacebookSocialAuthentication implements SocialAuthenticationStrategy
{
    use ConvertsSocialUsers;

    /**
     * FacebookSocialAuthentication constructor.
     *
     * @param SocialiteManager $socialiteManager
     * @param AppConfig        $appConfig
     */
    public function __construct(
        private readonly SocialiteManager $socialiteManager,
        private readonly AppConfig $appConfig,
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function activated(): bool
    {
        return ! empty($this->getConfiguration()['client_id']);
    }

    /**
     * {@inheritDoc}
     */
    public function createDriver(): ProviderInterface
    {
        /** @var FacebookProvider $driver */
        $driver = $this->socialiteManager->buildProvider(
            FacebookProvider::class,
            $this->getConfiguration(),
        );

        $driver->scopes([
            'email',
            'public_profile',
        ]);

        $driver->fields([
            'name',
            'first_name',
            'last_name',
            'email',
        ]);

        return $driver;
    }

    /**
     * {@inheritDoc}
     */
    public function convert(AbstractUser $user): User
    {
        return new User([
            'email' => $user->getEmail(),
            ...$this->decomposeName(
                $user->getName(),
                $user->user['first_name'] ?? null,
                $user->user['last_name'] ?? null,
            ),
        ]);
    }

    /**
     * Get driver configuration.
     *
     * @return array
     */
    private function getConfiguration(): array
    {
        return [
            'client_id'     => $this->appConfig->service('facebookLogin.clientId'),
            'client_secret' => $this->appConfig->service('facebookLogin.clientSecret'),
            'redirect'      => url('/auth/social/facebook/callback'),
        ];
    }
}
