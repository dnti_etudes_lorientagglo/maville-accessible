<?php

namespace App\Services\Auth;

use App\Models\User;
use Illuminate\Auth\Events\Verified;

/**
 * Trait VerifiesEmail.
 */
trait VerifiesEmail
{
    /**
     * Mark the user email as verified if it is not already.
     *
     * @param User $user
     *
     * @return void
     */
    public function markEmailAsVerified(User $user): void
    {
        if (! $user->hasVerifiedEmail()) {
            $user->markEmailAsVerified();

            event(new Verified($user));
        }
    }
}
