<?php

namespace App\Features;

/**
 * Enum FeatureName.
 */
enum FeatureName: string
{
    /**
     * Add a map and places CRUD.
     */
    case PLACES = 'PLACES';

    /**
     * Add a list of articles and articles CRUD.
     */
    case ARTICLES = 'ARTICLES';

    /**
     * Add a list of activities and activities CRUD.
     */
    case ACTIVITIES = 'ACTIVITIES';

    /**
     * Add a list of events and events CRUD.
     */
    case EVENTS = 'EVENTS';

    /**
     * Send a newsletter to subscribed emails.
     */
    case NEWSLETTER = 'NEWSLETTER';

    /**
     * Create a "communication admin" role who can view sending campaigns.
     */
    case COMMUNICATION = 'COMMUNICATION';

    /**
     * Add accessibility information on places, activities and reviews.
     */
    case ACCESSIBILITY = 'ACCESSIBILITY';

    /**
     * Add ability to review places and activities.
     */
    case REVIEWS = 'REVIEWS';

    /**
     * Add ability to fill a comment body on reviews.
     */
    case REVIEWS_BODY = 'REVIEWS_BODY';
}
