<?php

namespace App\Features;

use Illuminate\Support\Collection;

/**
 * Class Features.
 */
class Features
{
    /**
     * Check a feature value.
     *
     * @param FeatureName $name
     *
     * @return bool
     */
    public static function check(FeatureName $name): bool
    {
        return ! ! config('app.features.' . $name->value);
    }

    /**
     * Collect values from app config.
     *
     * @return Collection
     */
    public static function collectValues(): Collection
    {
        return collect(config('app.features'));
    }

    /**
     * Collect values from env.
     *
     * @return Collection
     */
    public static function collectValuesFromEnv(): Collection
    {
        return self::collectNames()->mapWithKeys(static fn(FeatureName $name) => [
            $name->value => env('APP_FEAT_' . $name->value),
        ]);
    }

    /**
     * Get the list of features keys from defined constants using reflection.
     *
     * @return Collection
     */
    public static function collectNames(): Collection
    {
        return collect(FeatureName::cases());
    }
}
