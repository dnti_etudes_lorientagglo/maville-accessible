<?php

namespace App\Mail;

use App\Helpers\LocaleHelper;
use App\Models\SendingCampaign;
use App\Services\Sending\Recipients\MailSendingRecipient;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\URL;

/**
 * Class SendingCampaignMail.
 */
class SendingCampaignMail extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * SendingCampaignMail constructor.
     *
     * @param SendingCampaign      $campaign
     * @param MailSendingRecipient $recipient
     * @param bool                 $browser
     */
    public function __construct(
        private readonly SendingCampaign $campaign,
        private readonly MailSendingRecipient $recipient,
        private readonly bool $browser = false,
    ) {
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: LocaleHelper::translate($this->campaign->data['subject']),
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            view: $this->campaign->data['view'] ?? null,
            text: $this->campaign->data['text'] ?? null,
            markdown: $this->campaign->data['markdown'] ?? null,
            with: [
                'subject'   => $this->campaign->data['subject'],
                'recipient' => $this->recipient,
                'browser'   => ! $this->browser && $this->campaign->exists
                    ? URL::signedRoute('actions.sending-campaigns.browser', [
                        'campaign'  => $this->campaign->id,
                        'recipient' => base64_encode($this->recipient->email()),
                    ])
                    : null,
                ...($this->campaign->data['data'] ?? []),
            ],
        );
    }
}
