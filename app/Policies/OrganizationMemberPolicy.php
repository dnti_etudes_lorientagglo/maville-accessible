<?php

namespace App\Policies;

use App\Helpers\AuthHelper;
use App\Models\Enums\RoleName;
use App\Models\OrganizationMember;
use App\Models\User;
use App\Policies\Concerns\AuthorizeWithPermissions;
use App\Policies\Concerns\ModerablePolicy;

/**
 * Class OrganizationMemberPolicy.
 */
class OrganizationMemberPolicy
{
    use AuthorizeWithPermissions {
        show as baseShow;
        view as baseView;
        update as baseUpdate;
        delete as baseDelete;
    }
    use ModerablePolicy;

    /**
     * {@inheritDoc}
     */
    public static function permissions(): array
    {
        return [
            RoleName::CONTENT_ADMIN->value       => [
                static::adminPermission(),
                static::showAnyPermission(),
                static::viewAnyPermission(),
                static::showPermission(),
                static::viewPermission(),
                static::createPermission(),
                static::updatePermission(),
                static::deletePermission(),
            ],
            RoleName::ORGANIZATION_ADMIN->value  => [
                static::showAnyPermission(),
                static::viewAnyPermission(),
                static::showPermission(),
                static::viewPermission(),
                static::updatePermission(),
                static::deletePermission(),
            ],
            RoleName::ORGANIZATION_MEMBER->value => [
                static::showAnyPermission(),
                static::viewAnyPermission(),
                static::showPermission(),
                static::viewPermission(),
            ],
            RoleName::MODERATOR->value           => [
                static::showAnyPermission(),
                static::viewAnyPermission(),
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function show(User $user, OrganizationMember $model): bool
    {
        return $this->baseDelete($user, $model)
            && ($this->canModerate($user) || $this->canAdminOrOwn($model));
    }

    /**
     * {@inheritDoc}
     */
    public function view(User $user, OrganizationMember $model): bool
    {
        return $this->baseDelete($user, $model)
            && ($this->canModerate($user) || $this->canAdminOrOwn($model));
    }

    /**
     * {@inheritDoc}
     */
    public function update(User $user, OrganizationMember $model): bool
    {
        return $this->baseUpdate($user, $model)
            && $this->canAdminOrOwn($model, $user->id !== $model->user_id);
    }

    /**
     * {@inheritDoc}
     */
    public function delete(User $user, OrganizationMember $model): bool
    {
        return $this->baseDelete($user, $model)
            && $this->canAdminOrOwn($model, $user->id !== $model->user_id);
    }

    /**
     * Check if user can admin or acting for organization of member.
     *
     * @param OrganizationMember $organizationMember
     * @param bool               $ownerPrecondition
     *
     * @return bool
     */
    private function canAdminOrOwn(
        OrganizationMember $organizationMember,
        bool $ownerPrecondition = true,
    ): bool {
        return AuthHelper::hasPermissionTo(static::adminPermission())
            || ($ownerPrecondition && AuthHelper::isActingFor($organizationMember->organization_id));
    }
}
