<?php

namespace App\Policies;

use App\Models\Enums\ReportTypeCode;
use App\Models\Enums\RoleName;
use App\Models\ReportType;
use App\Models\User;
use App\Policies\Concerns\AuthorizeWithPermissions;

/**
 * Class ReportTypePolicy.
 */
class ReportTypePolicy
{
    use AuthorizeWithPermissions {
        delete as baseDelete;
    }

    /**
     * {@inheritDoc}
     */
    public static function permissions(): array
    {
        return [
            RoleName::APP_ADMIN->value => [
                static::showAnyPermission(),
                static::showPermission(),
                static::createPermission(),
                static::updatePermission(),
                static::deletePermission(),
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function viewAny(User|null $user): bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function view(User|null $user): bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function delete(User $user, ReportType $model): bool
    {
        return $this->baseDelete($user, $model)
            && $model->code !== ReportTypeCode::OTHER->value;
    }
}
