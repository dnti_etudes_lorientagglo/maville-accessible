<?php

namespace App\Policies;

use App\Helpers\AuthHelper;
use App\Models\Enums\RoleName;
use App\Models\OrganizationMember;
use App\Models\User;
use App\Policies\Concerns\AuthorizeWithPermissions;

/**
 * Class UserPolicy.
 */
class UserPolicy
{
    use AuthorizeWithPermissions {
        view as baseView;
        update as baseUpdate;
        shouldBlockAuthorization as baseShouldBlockAuthorization;
    }

    public const VIEW_DETAILS = 'viewDetails';

    public const VERIFY = 'verify';

    public const BLOCK = 'block';

    public const UNBLOCK = 'unblock';

    public const SCHEDULE_DELETION = 'scheduleDeletion';

    /**
     * Name for the view details permission.
     *
     * @return string
     */
    protected static function viewDetailsPermission(): string
    {
        return static::buildPermission(static::VIEW_DETAILS);
    }

    /**
     * Name for the block permission.
     *
     * @return string
     */
    protected static function verifyPermission(): string
    {
        return static::buildPermission(static::VERIFY);
    }

    /**
     * Name for the block permission.
     *
     * @return string
     */
    protected static function blockPermission(): string
    {
        return static::buildPermission(static::BLOCK);
    }

    /**
     * Name for the unblock permission.
     *
     * @return string
     */
    protected static function unblockPermission(): string
    {
        return static::buildPermission(static::UNBLOCK);
    }

    /**
     * {@inheritDoc}
     */
    public static function permissions(): array
    {
        return [
            RoleName::USER_ADMIN->value => [
                static::showAnyPermission(),
                static::viewAnyPermission(),
                static::showPermission(),
                static::viewPermission(),
                static::viewDetailsPermission(),
                static::updatePermission(),
                static::verifyPermission(),
                static::blockPermission(),
                static::unblockPermission(),
            ],
            RoleName::MODERATOR->value  => [
                static::showAnyPermission(),
                static::viewAnyPermission(),
                static::showPermission(),
                static::viewPermission(),
                static::viewDetailsPermission(),
                static::blockPermission(),
                static::unblockPermission(),
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function shouldBlockAuthorization(User | null $user, string $ability): bool
    {
        if ($this->baseShouldBlockAuthorization($user, $ability)) {
            return ! in_array($ability, [self::VIEW_DETAILS, self::VIEW, self::UPDATE]);
        }

        return false;
    }

    /**
     * Check if the user can view details of one.
     *
     * @param User $user
     * @param User $model
     *
     * @return bool
     */
    public function viewDetails(User $user, User $model): bool
    {
        return $user->id === $model->id
            || (
                ! $user->blocked()
                && AuthHelper::hasPermissionTo(static::viewDetailsPermission())
            )
            || (
                ! $user->blocked()
                && $user->getActingForOrganization()->organization->membersOfOrganization
                    ->some(fn(OrganizationMember $member) => $member->user_id === $user->id)
            );
    }

    /**
     * Check if the user can block one.
     *
     * @param User $user
     * @param User $model
     *
     * @return bool
     */
    public function verify(User $user, User $model): bool
    {
        return ! $model->hasVerifiedEmail()
            && AuthHelper::hasPermissionTo(static::verifyPermission());
    }

    /**
     * Check if the user can block one.
     *
     * @param User $user
     * @param User $model
     *
     * @return bool
     */
    public function block(User $user, User $model): bool
    {
        return ! $model->blocked()
            && AuthHelper::hasPermissionTo(static::blockPermission());
    }

    /**
     * Check if the user can unblock one.
     *
     * @param User $user
     * @param User $model
     *
     * @return bool
     */
    public function unblock(User $user, User $model): bool
    {
        return $model->blocked()
            && AuthHelper::hasPermissionTo(static::unblockPermission());
    }

    /**
     * Check if the user can schedule deletion.
     *
     * @param User $user
     * @param User $model
     *
     * @return bool
     */
    public function scheduleDeletion(User $user, User $model): bool
    {
        return $user->id === $model->id;
    }

    /**
     * {@inheritDoc}
     */
    public function view(User $user, User $model): bool
    {
        return $user->id === $model->id || $this->baseView($user, $model);
    }

    /**
     * {@inheritDoc}
     */
    public function update(User $user, User $model): bool
    {
        return $user->id === $model->id || $this->baseUpdate($user, $model);
    }
}
