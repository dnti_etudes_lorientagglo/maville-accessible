<?php

namespace App\Policies;

use App\Models\Enums\RoleName;
use App\Models\User;
use App\Policies\Concerns\AuthorizeWithPermissions;

/**
 * Class OrganizationTypePolicy.
 */
class OrganizationTypePolicy
{
    use AuthorizeWithPermissions;

    /**
     * {@inheritDoc}
     */
    public static function permissions(): array
    {
        return [
            RoleName::CONTENT_ADMIN->value => [
                static::viewAnyPermission(),
                static::viewPermission(),
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function viewAny(User|null $user): bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function view(User|null $user): bool
    {
        return true;
    }
}
