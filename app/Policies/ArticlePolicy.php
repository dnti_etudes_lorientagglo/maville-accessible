<?php

namespace App\Policies;

use App\Features\FeatureName;
use App\Features\Features;
use App\Models\Article;
use App\Models\Enums\RoleName;
use App\Models\User;
use App\Policies\Concerns\AuthorizeWithPermissions;
use App\Policies\Concerns\ModerablePolicy;
use App\Policies\Concerns\OwnablePolicy;
use App\Policies\Concerns\PinnablePolicy;
use App\Policies\Concerns\PublishablePolicy;
use App\Policies\Concerns\ReviewablePolicy;
use App\Policies\Concerns\SourceablePolicy;

/**
 * Class ArticlePolicy.
 */
class ArticlePolicy
{
    use AuthorizeWithPermissions {
        show as baseShow;
        view as baseView;
        update as baseUpdate;
        delete as baseDelete;
    }
    use ModerablePolicy;
    use PinnablePolicy;
    use PublishablePolicy;
    use OwnablePolicy;
    use ReviewablePolicy;
    use SourceablePolicy;

    /**
     * {@inheritDoc}
     */
    public static function permissions(): array
    {
        if (! Features::check(FeatureName::ARTICLES)) {
            return [];
        }

        return [
            RoleName::CONTENT_ADMIN->value       => [
                static::adminPermission(),
                static::showAnyPermission(),
                static::viewAnyPermission(),
                static::showPermission(),
                static::viewPermission(),
                static::createPermission(),
                static::updatePermission(),
                static::deletePermission(),
                static::publishPermission(),
                static::unPublishPermission(),
                static::mergeSourcesPermission(),
                static::pinPermission(),
                static::unpinPermission(),
            ],
            RoleName::ORGANIZATION_ADMIN->value  => [
                static::showAnyPermission(),
                static::viewAnyPermission(),
                static::showPermission(),
                static::viewPermission(),
                static::createPermission(),
                static::updatePermission(),
                static::deletePermission(),
                static::publishPermission(),
                static::unPublishPermission(),
            ],
            RoleName::ORGANIZATION_MEMBER->value => [
                static::showAnyPermission(),
                static::viewAnyPermission(),
                static::showPermission(),
                static::viewPermission(),
                static::createPermission(),
                static::updatePermission(),
                static::deletePermission(),
                static::publishPermission(),
                static::unPublishPermission(),
            ],
            RoleName::MODERATOR->value           => [
                static::showPermission(),
                static::viewPermission(),
                static::publishPermission(),
                static::unPublishPermission(),
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function viewAny(User | null $user): bool
    {
        return Features::check(FeatureName::ARTICLES);
    }

    /**
     * {@inheritDoc}
     */
    public function show(User $user, Article $model): bool
    {
        return $this->baseShow($user, $model)
            && ($this->canModerate($user) || $this->canAdminOrOwn($model));
    }

    /**
     * {@inheritDoc}
     */
    public function view(User | null $user, Article $model): bool
    {
        if ($this->publishableView($model)) {
            return true;
        }

        return $user
            && $this->baseView($user, $model)
            && ($this->canModerate($user) || $this->canAdminOrOwn($model));
    }

    /**
     * {@inheritDoc}
     */
    public function update(User $user, Article $model): bool
    {
        return $this->baseUpdate($user, $model) && $this->canAdminOrOwn($model);
    }

    /**
     * {@inheritDoc}
     */
    public function delete(User $user, Article $model): bool
    {
        return $this->baseDelete($user, $model) && $this->canAdminOrOwn($model);
    }
}
