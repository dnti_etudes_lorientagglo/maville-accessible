<?php

namespace App\Policies;

use App\Models\Enums\RoleName;
use App\Policies\Concerns\AuthorizeWithPermissions;

/**
 * Class ConfigPolicy.
 */
class ConfigPolicy
{
    use AuthorizeWithPermissions;

    /**
     * {@inheritDoc}
     */
    public static function permissions(): array
    {
        return [
            RoleName::APP_ADMIN->value => [
                static::showAnyPermission(),
                static::showPermission(),
                static::viewPermission(),
                static::updatePermission(),
            ],
        ];
    }
}
