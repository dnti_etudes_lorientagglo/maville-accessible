<?php

namespace App\Policies;

use App\Models\Enums\RoleName;
use App\Models\User;
use App\Policies\Concerns\AuthorizeWithPermissions;
use App\Policies\Concerns\PublishablePolicy;
use App\Policies\Concerns\SourceablePolicy;

/**
 * Class CategoryPolicy.
 */
class CategoryPolicy
{
    use AuthorizeWithPermissions;
    use PublishablePolicy;
    use SourceablePolicy;

    /**
     * {@inheritDoc}
     */
    public static function permissions(): array
    {
        return [
            RoleName::CONTENT_ADMIN->value => [
                static::showAnyPermission(),
                static::showPermission(),
                static::createPermission(),
                static::updatePermission(),
                static::deletePermission(),
                static::publishPermission(),
                static::unPublishPermission(),
                static::mergeSourcesPermission(),
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function viewAny(User|null $user): bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function view(User|null $user): bool
    {
        return true;
    }
}
