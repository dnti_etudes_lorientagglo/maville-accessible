<?php

namespace App\Policies;

use App\Models\User;
use App\Policies\Concerns\AuthorizeWithPermissions;

/**
 * Class RequestTypePolicy.
 */
class RequestTypePolicy
{
    use AuthorizeWithPermissions;

    /**
     * {@inheritDoc}
     */
    public function viewAny(User|null $user): bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function view(User|null $user): bool
    {
        return true;
    }
}
