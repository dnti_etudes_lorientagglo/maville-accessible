<?php

namespace App\Policies;

use App\Helpers\AuthHelper;
use App\Models\Enums\RoleName;
use App\Models\OrganizationInvitation;
use App\Models\User;
use App\Policies\Concerns\AuthorizeWithPermissions;

/**
 * Class OrganizationInvitationPolicy.
 */
class OrganizationInvitationPolicy
{
    use AuthorizeWithPermissions {
        delete as baseDelete;
    }

    public const ACCEPT = 'accept';

    public const REFUSE = 'refuse';

    /**
     * {@inheritDoc}
     */
    public static function permissions(): array
    {
        return [
            RoleName::CONTENT_ADMIN->value      => [
                static::adminPermission(),
                static::showAnyPermission(),
                static::viewAnyPermission(),
                static::createPermission(),
                static::updatePermission(),
                static::deletePermission(),
            ],
            RoleName::ORGANIZATION_ADMIN->value => [
                static::showAnyPermission(),
                static::viewAnyPermission(),
                static::createPermission(),
                static::updatePermission(),
                static::deletePermission(),
            ],
            RoleName::MODERATOR->value          => [
                static::showAnyPermission(),
                static::viewAnyPermission(),
            ],
        ];
    }

    /**
     * Check if user can accept an invitation.
     *
     * @param User                   $user
     * @param OrganizationInvitation $organizationInvitation
     *
     * @return bool
     */
    public function accept(User $user, OrganizationInvitation $organizationInvitation): bool
    {
        return $user->email === $organizationInvitation->email;
    }

    /**
     * Check if user can refuse an invitation.
     *
     * @param User                   $user
     * @param OrganizationInvitation $organizationInvitation
     *
     * @return bool
     */
    public function refuse(User $user, OrganizationInvitation $organizationInvitation): bool
    {
        return $user->email === $organizationInvitation->email;
    }

    /**
     * {@inheritDoc}
     */
    public function view(User $user, OrganizationInvitation $model): bool
    {
        return $user->email === $model->email;
    }

    /**
     * {@inheritDoc}
     */
    public function delete(User $user, OrganizationInvitation $model): bool
    {
        return $this->baseDelete($user, $model)
            && $this->canAdminOrOwn($model);
    }

    /**
     * Check if user can admin or acting for organization of invitation.
     *
     * @param OrganizationInvitation $invitation
     *
     * @return bool
     */
    private function canAdminOrOwn(
        OrganizationInvitation $invitation,
    ): bool {
        return AuthHelper::hasPermissionTo(static::adminPermission())
            || AuthHelper::isActingFor($invitation->organization_id);
    }
}
