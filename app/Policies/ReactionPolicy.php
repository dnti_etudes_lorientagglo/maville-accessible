<?php

namespace App\Policies;

use App\Models\Reaction;
use App\Models\User;
use App\Policies\Concerns\AuthorizeWithPermissions;

/**
 * Class ReactionPolicy.
 */
class ReactionPolicy
{
    use AuthorizeWithPermissions;

    /**
     * {@inheritDoc}
     */
    public function viewAny(User $user): bool
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function create(User $user): bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function view(User $user, Reaction $model): bool
    {
        return $model->owner_user_id === $user->id;
    }

    /**
     * {@inheritDoc}
     */
    public function update(User $user, Reaction $model): bool
    {
        return $model->owner_user_id === $user->id;
    }

    /**
     * {@inheritDoc}
     */
    public function delete(User $user, Reaction $model): bool
    {
        return $model->owner_user_id === $user->id;
    }
}
