<?php

namespace App\Policies;

use App\Models\Notification;
use App\Models\User;
use App\Policies\Concerns\AuthorizeWithPermissions;

/**
 * Class NotificationPolicy.
 */
class NotificationPolicy
{
    use AuthorizeWithPermissions;

    /**
     * {@inheritDoc}
     */
    protected function shouldBlockAuthorization(User|null $user, string $ability): bool
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function viewAny(User $user): bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function create(User $user): bool
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function view(User $user, Notification $model): bool
    {
        return $model->notifiable_id === $user->id;
    }

    /**
     * {@inheritDoc}
     */
    public function update(User $user, Notification $model): bool
    {
        return $model->notifiable_id === $user->id;
    }

    /**
     * {@inheritDoc}
     */
    public function delete(User $user, Notification $model): bool
    {
        return $model->notifiable_id === $user->id;
    }
}
