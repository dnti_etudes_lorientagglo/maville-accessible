<?php

namespace App\Policies;

use App\Models\Enums\RoleName;
use App\Policies\Concerns\AuthorizeWithPermissions;

/**
 * Class SendingCampaignBatchPolicy.
 */
class SendingCampaignBatchPolicy
{
    use AuthorizeWithPermissions;

    /**
     * {@inheritDoc}
     */
    public static function permissions(): array
    {
        return [
            RoleName::COMMUNICATION_ADMIN->value => [
                static::viewAnyPermission(),
                static::viewPermission(),
            ],
        ];
    }
}
