<?php

namespace App\Policies;

use App\Models\Enums\RoleName;
use App\Policies\Concerns\AuthorizeWithPermissions;

/**
 * Class SendingCampaignPolicy.
 */
class SendingCampaignPolicy
{
    use AuthorizeWithPermissions;

    /**
     * {@inheritDoc}
     */
    public static function permissions(): array
    {
        return [
            RoleName::COMMUNICATION_ADMIN->value => [
                static::showAnyPermission(),
                static::viewAnyPermission(),
                static::showPermission(),
                static::viewPermission(),
            ],
        ];
    }
}
