<?php

namespace App\Policies;

use App\Models\User;
use App\Policies\Concerns\AuthorizeWithPermissions;

/**
 * Class ActivityTypePolicy.
 */
class ActivityTypePolicy
{
    use AuthorizeWithPermissions;

    /**
     * {@inheritDoc}
     */
    public function viewAny(User|null $user): bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function view(User|null $user): bool
    {
        return true;
    }
}
