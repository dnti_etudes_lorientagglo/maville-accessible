<?php

namespace App\Policies;

use App\Helpers\AuthHelper;
use App\Models\Enums\RequestTypeCode;
use App\Models\Enums\RoleName;
use App\Models\Organization;
use App\Models\Request;
use App\Models\Role;
use App\Models\User;
use App\Policies\Concerns\AuthorizeWithPermissions;
use App\Policies\Concerns\OwnablePolicy;

/**
 * Class RequestPolicy.
 */
class RequestPolicy
{
    use AuthorizeWithPermissions {
        show as baseShow;
        view as baseView;
        update as baseUpdate;
    }
    use OwnablePolicy;

    public const ACCEPT = 'accept';

    public const REFUSE = 'refuse';

    /**
     * {@inheritDoc}
     */
    public static function permissions(): array
    {
        return [
            RoleName::CONTENT_ADMIN->value      => [
                static::adminPermission(),
                static::showAnyPermission(),
                static::viewAnyPermission(),
                static::showPermission(),
                static::viewPermission(),
                static::updatePermission(),
                static::acceptPermission(),
                static::refusePermission(),
            ],
            RoleName::ORGANIZATION_ADMIN->value => [
                static::showAnyPermission(),
                static::viewAnyPermission(),
                static::showPermission(),
                static::viewPermission(),
                static::updatePermission(),
                static::acceptPermission(),
                static::refusePermission(),
            ],
        ];
    }

    /**
     * Name for the accept permission.
     *
     * @return string
     */
    protected static function acceptPermission(): string
    {
        return static::buildPermission(static::ACCEPT);
    }

    /**
     * Name for the refuse permission.
     *
     * @return string
     */
    protected static function refusePermission(): string
    {
        return static::buildPermission(static::REFUSE);
    }

    /**
     * {@inheritDoc}
     */
    public function create(User $user): bool
    {
        return $user->hasVerifiedEmail();
    }

    /**
     * {@inheritDoc}
     */
    public function show(User $user, Request $model): bool
    {
        return $this->baseShow($user, $model) && $this->canManageRequest($user, $model);
    }

    /**
     * {@inheritDoc}
     */
    public function view(User $user, Request $model): bool
    {
        return $this->baseView($user, $model) && $this->canManageRequest($user, $model);
    }

    /**
     * {@inheritDoc}
     */
    public function update(User $user, Request $model): bool
    {
        return $this->baseUpdate($user, $model) && $this->canManageRequest($user, $model);
    }

    /**
     * Check if the user can accept one.
     *
     * @param User    $user
     * @param Request $model
     *
     * @return bool
     */
    public function accept(User $user, Request $model): bool
    {
        return AuthHelper::hasPermissionTo(static::acceptPermission())
            && ! $model->accepted_at
            && ! $model->deleted_at;
    }

    /**
     * Check if the user can refuse one.
     *
     * @param User    $user
     * @param Request $model
     *
     * @return bool
     */
    public function refuse(User $user, Request $model): bool
    {
        return AuthHelper::hasPermissionTo(static::refusePermission())
            && ! $model->accepted_at
            && ! $model->deleted_at;
    }

    /**
     * Check if user can manage a request.
     *
     * @param User    $user
     * @param Request $request
     *
     * @return bool
     */
    private function canManageRequest(User $user, Request $request): bool
    {
        if (AuthHelper::hasPermissionTo(static::adminPermission())) {
            return true;
        }

        $request->loadMissing(['requestType', 'requestable']);

        // Organization admins can manage join requests.
        if (
            RequestTypeCode::JOIN->value === $request->requestType->code
            && $request->requestable instanceof Organization
            && $request->requestable->membersOfOrganization()
                ->where('user_id', $user->id)
                ->where('role_id', Role::findByName(RoleName::ORGANIZATION_ADMIN->value)->id)
                ->exists()
        ) {
            return true;
        }

        return false;
    }
}
