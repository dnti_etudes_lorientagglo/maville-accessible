<?php

namespace App\Policies;

use App\Models\Enums\RoleName;
use App\Models\Review;
use App\Models\User;
use App\Policies\Concerns\AuthorizeWithPermissions;
use App\Policies\Concerns\OwnablePolicy;

/**
 * Class ReviewPolicy.
 */
class ReviewPolicy
{
    use AuthorizeWithPermissions {
        view as baseView;
        update as baseUpdate;
        delete as baseDelete;
    }
    use OwnablePolicy;

    /**
     * {@inheritDoc}
     */
    public static function permissions(): array
    {
        return [
            RoleName::MODERATOR->value => [
                static::viewAnyPermission(),
                static::viewPermission(),
                static::updatePermission(),
                static::deletePermission(),
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function create(User $user): bool
    {
        return $user->hasVerifiedEmail();
    }

    /**
     * {@inheritDoc}
     */
    public function view(User $user, Review $model): bool
    {
        return $this->baseView($user, $model) || $this->canAdminOrOwn($model);
    }

    /**
     * {@inheritDoc}
     */
    public function update(User $user, Review $model): bool
    {
        return $this->baseUpdate($user, $model) || $this->canAdminOrOwn($model);
    }

    /**
     * {@inheritDoc}
     */
    public function delete(User $user, Review $model): bool
    {
        return $this->baseDelete($user, $model) || $this->canAdminOrOwn($model);
    }
}
