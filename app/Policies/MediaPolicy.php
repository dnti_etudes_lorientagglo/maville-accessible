<?php

namespace App\Policies;

use App\Models\Config;
use App\Models\Enums\RoleName;
use App\Models\Media;
use App\Models\User;
use App\Policies\Concerns\AuthorizeWithPermissions;

/**
 * Class MediaPolicy.
 */
class MediaPolicy
{
    use AuthorizeWithPermissions;

    /**
     * {@inheritDoc}
     */
    public static function permissions(): array
    {
        return [
            RoleName::CONTENT_ADMIN->value       => [
                static::showAnyPermission(),
            ],
            RoleName::ORGANIZATION_ADMIN->value  => [
                static::showAnyPermission(),
            ],
            RoleName::ORGANIZATION_MEMBER->value => [
                static::showAnyPermission(),
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function viewAny(User $user): bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function create(User $user): bool
    {
        return $user->hasVerifiedEmail() && ! $user->blocked();
    }

    /**
     * {@inheritDoc}
     */
    public function view(User $user, Media $model): bool
    {
        return ($model->model_type === 'users' && $model->model_id === $user->id)
            || ($model->model_type === 'configs' && $model->collection_name === Config::MEDIA_PUBLIC_COLLECTION);
    }

    /**
     * {@inheritDoc}
     */
    public function update(User $user, Media $model): bool
    {
        return $model->model_type === 'users' && $model->model_id === $user->id;
    }

    /**
     * {@inheritDoc}
     */
    public function delete(User $user, Media $model): bool
    {
        return $model->model_type === 'users' && $model->model_id === $user->id;
    }
}
