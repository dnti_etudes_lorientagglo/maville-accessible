<?php

namespace App\Policies;

use App\Helpers\AuthHelper;
use App\Models\Enums\RoleName;
use App\Models\User;
use App\Policies\Concerns\AuthorizeWithPermissions;

/**
 * Class RolePolicy.
 */
class RolePolicy
{
    use AuthorizeWithPermissions;

    public const ASSIGN_ANY = 'assignAny';

    /**
     * {@inheritDoc}
     */
    public static function permissions(): array
    {
        return [
            RoleName::USER_ADMIN->value => [
                static::viewAnyPermission(),
                static::viewPermission(),
                static::assignAnyPermission(),
            ],
        ];
    }

    /**
     * Permission to attribute roles to other users.
     *
     * @return string
     */
    protected static function assignAnyPermission(): string
    {
        return static::buildPermission(static::ASSIGN_ANY);
    }

    /**
     * {@inheritDoc}
     */
    public function viewAny(User|null $user): bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function view(User|null $user): bool
    {
        return true;
    }

    /**
     * Check if the user can assign any role.
     *
     * @param User $user
     *
     * @return bool
     */
    public function assignAny(User $user): bool
    {
        return AuthHelper::hasPermissionTo(static::assignAnyPermission());
    }
}
