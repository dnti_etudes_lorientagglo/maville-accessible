<?php

namespace App\Policies;

use App\Helpers\AuthHelper;
use App\Models\Enums\RoleName;
use App\Models\Organization;
use App\Models\User;
use App\Policies\Concerns\AuthorizeWithPermissions;
use App\Policies\Concerns\ModerablePolicy;
use App\Policies\Concerns\PublishablePolicy;
use App\Policies\Concerns\SourceablePolicy;

/**
 * Class OrganizationPolicy.
 */
class OrganizationPolicy
{
    use AuthorizeWithPermissions {
        show as baseShow;
        view as baseView;
        update as baseUpdate;
        delete as baseDelete;
    }
    use ModerablePolicy;
    use PublishablePolicy;
    use SourceablePolicy;

    /**
     * {@inheritDoc}
     */
    public static function permissions(): array
    {
        return [
            RoleName::CONTENT_ADMIN->value         => [
                static::adminPermission(),
                static::showAnyPermission(),
                static::viewAnyPermission(),
                static::showPermission(),
                static::viewPermission(),
                static::createPermission(),
                static::updatePermission(),
                static::deletePermission(),
                static::publishPermission(),
                static::unPublishPermission(),
                static::mergeSourcesPermission(),
            ],
            RoleName::ORGANIZATION_ADMIN->value  => [
                static::showPermission(),
                static::viewPermission(),
                static::updatePermission(),
                static::publishPermission(),
                static::unPublishPermission(),
            ],
            RoleName::ORGANIZATION_MEMBER->value => [
                static::showPermission(),
                static::viewPermission(),
            ],
            RoleName::MODERATOR->value           => [
                static::showPermission(),
                static::viewPermission(),
                static::publishPermission(),
                static::unPublishPermission(),
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function viewAny(User|null $user): bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function show(User $user, Organization $model): bool
    {
        return $this->baseShow($user, $model)
            && ($this->canModerate($user) || $this->canAdminOrOwn($model));
    }

    /**
     * {@inheritDoc}
     */
    public function view(User|null $user, Organization $model): bool
    {
        if ($this->publishableView($model)) {
            return true;
        }

        return $user
            && $this->baseView($user, $model)
            && ($this->canModerate($user) || $this->canAdminOrOwn($model));
    }

    /**
     * {@inheritDoc}
     */
    public function update(User $user, Organization $model): bool
    {
        return $this->baseUpdate($user, $model) && $this->canAdminOrOwn($model);
    }

    /**
     * {@inheritDoc}
     */
    public function delete(User $user, Organization $model): bool
    {
        return $this->baseDelete($user, $model) && $this->canAdminOrOwn($model);
    }

    /**
     * Check if user can admin or acting for organization.
     *
     * @param Organization $organization
     *
     * @return bool
     */
    private function canAdminOrOwn(Organization $organization): bool
    {
        return AuthHelper::hasPermissionTo(static::adminPermission())
            || AuthHelper::isActingFor($organization);
    }
}
