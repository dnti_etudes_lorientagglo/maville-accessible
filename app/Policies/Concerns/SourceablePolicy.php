<?php

namespace App\Policies\Concerns;

use App\Helpers\AuthHelper;
use App\Models\Contracts\Sourceable;
use App\Models\User;

/**
 * Trait SourceablePolicy.
 *
 * @template TModel as object
 *
 * @mixin AuthorizeWithPermissions<TModel>
 */
trait SourceablePolicy
{
    public const MERGE_SOURCES = 'mergeSources';

    public const SYNC_SOURCES = 'syncSources';

    /**
     * Name for the merge sources permission.
     *
     * @return string
     */
    protected static function mergeSourcesPermission(): string
    {
        return static::buildPermission(static::MERGE_SOURCES);
    }

    /**
     * Name for the sync sources permission.
     *
     * @return string
     */
    protected static function syncSourcesPermission(): string
    {
        return static::buildPermission(static::SYNC_SOURCES);
    }

    /**
     * Check if the user can merge sources.
     *
     * @param User   $user
     * @param TModel $model
     *
     * @return bool
     */
    public function mergeSources(User $user, Sourceable $model): bool
    {
        return AuthHelper::hasPermissionTo(static::mergeSourcesPermission());
    }

    /**
     * Check if the user can sync sources.
     *
     * @param User   $user
     * @param TModel $model
     *
     * @return bool
     */
    public function syncSources(User $user, Sourceable $model): bool
    {
        return AuthHelper::hasPermissionTo(static::syncSourcesPermission());
    }
}
