<?php

namespace App\Policies\Concerns;

use App\Models\Contracts\Reviewable;
use App\Models\User;

/**
 * Trait ReviewablePolicy.
 *
 * @template TModel as object
 *
 * @mixin AuthorizeWithPermissions<TModel>
 */
trait ReviewablePolicy
{
    /**
     * Check if the user can publish one.
     *
     * @param User|null  $user
     * @param Reviewable $model
     *
     * @return bool
     */
    public function viewReviews(User|null $user, Reviewable $model): bool
    {
        return $this->view($user, $model);
    }
}
