<?php

namespace App\Policies\Concerns;

use App\Models\Enums\RoleName;
use App\Models\User;

/**
 * Trait ModerablePolicy.
 *
 * @template TModel as object
 *
 * @mixin AuthorizeWithPermissions<TModel>
 */
trait ModerablePolicy
{
    /**
     * Check if user can moderate.
     *
     * @param User $user
     *
     * @return bool
     */
    private function canModerate(User $user): bool
    {
        return $user->hasRole(RoleName::MODERATOR->value);
    }
}
