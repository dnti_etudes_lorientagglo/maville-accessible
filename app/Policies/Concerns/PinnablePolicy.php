<?php

namespace App\Policies\Concerns;

use App\Helpers\AuthHelper;
use App\Models\Contracts\Pinnable;
use App\Models\User;

/**
 * Trait PinnablePolicy.
 *
 * @template TModel as object
 *
 * @mixin AuthorizeWithPermissions<TModel>
 */
trait PinnablePolicy
{
    public const PIN = 'pin';

    public const UNPIN = 'unpin';

    /**
     * Name for the pin permission.
     *
     * @return string
     */
    protected static function pinPermission(): string
    {
        return static::buildPermission(static::PIN);
    }

    /**
     * Name for the pin permission.
     *
     * @return string
     */
    protected static function unpinPermission(): string
    {
        return static::buildPermission(static::UNPIN);
    }

    /**
     * Check if the user can pin an activity.
     *
     * @param User     $user
     * @param Pinnable $model
     *
     * @return bool
     */
    public function pin(User $user, Pinnable $model): bool
    {
        return ! $model->pinned() && AuthHelper::hasPermissionTo(static::pinPermission());
    }

    /**
     * Check if the user can pin an activity.
     *
     * @param User     $user
     * @param Pinnable $model
     *
     * @return bool
     */
    public function unpin(User $user, Pinnable $model): bool
    {
        return $model->pinned() && AuthHelper::hasPermissionTo(static::unpinPermission());
    }
}
