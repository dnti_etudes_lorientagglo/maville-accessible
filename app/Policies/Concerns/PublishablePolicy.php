<?php

namespace App\Policies\Concerns;

use App\Helpers\AuthHelper;
use App\Models\Contracts\Publishable;
use App\Models\User;

/**
 * Trait PublishablePolicy.
 *
 * @template TModel as object
 *
 * @mixin AuthorizeWithPermissions<TModel>
 */
trait PublishablePolicy
{
    public const PUBLISH = 'publish';

    public const UN_PUBLISH = 'unPublish';

    /**
     * Name for the publish permission.
     *
     * @return string
     */
    protected static function publishPermission(): string
    {
        return static::buildPermission(static::PUBLISH);
    }

    /**
     * Name for the un-publish permission.
     *
     * @return string
     */
    protected static function unPublishPermission(): string
    {
        return static::buildPermission(static::UN_PUBLISH);
    }

    /**
     * Check if the user can publish one.
     *
     * @param User   $user
     * @param TModel $model
     *
     * @return bool
     */
    public function publishableView(Publishable $model): bool
    {
        return $model->published();
    }

    /**
     * Check if the user can publish one.
     *
     * @param User   $user
     * @param TModel $model
     *
     * @return bool
     */
    public function publish(User $user, Publishable $model): bool
    {
        return ! $model->published()
            && AuthHelper::hasPermissionTo(static::publishPermission())
            && $this->update($user, $model);
    }

    /**
     * Check if the user can un-publish one.
     *
     * @param User   $user
     * @param TModel $model
     *
     * @return bool
     */
    public function unPublish(User $user, Publishable $model): bool
    {
        return $model->published()
            && AuthHelper::hasPermissionTo(static::unPublishPermission())
            && $this->update($user, $model);
    }
}
