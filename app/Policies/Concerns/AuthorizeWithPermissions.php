<?php

namespace App\Policies\Concerns;

use App\Helpers\AuthHelper;
use App\Models\User;
use Illuminate\Support\Str;
use ReflectionClass;
use ReflectionParameter;
use Throwable;

/**
 * Class AuthorizeWithPermissions.
 *
 * @template TModel as object
 */
trait AuthorizeWithPermissions
{
    public const ADMIN = 'admin';

    public const SHOW_ANY = 'showAny';

    public const VIEW_ANY = 'viewAny';

    public const CREATE = 'create';

    public const SHOW = 'show';

    public const VIEW = 'view';

    public const UPDATE = 'update';

    public const DELETE = 'delete';

    /**
     * Return the available permissions mapped by allowed role.
     *
     * @return array
     */
    public static function permissions(): array
    {
        return [];
    }

    /**
     * Name for the admin permission.
     *
     * @return string
     */
    public static function adminPermission(): string
    {
        return static::buildPermission(static::ADMIN);
    }

    /**
     * Name for the show any permission.
     *
     * @return string
     */
    public static function showAnyPermission(): string
    {
        return static::buildPermission(static::SHOW_ANY);
    }

    /**
     * Name for the view any permission.
     *
     * @return string
     */
    public static function viewAnyPermission(): string
    {
        return static::buildPermission(static::VIEW_ANY);
    }

    /**
     * Name for the create permission.
     *
     * @return string
     */
    public static function createPermission(): string
    {
        return static::buildPermission(static::CREATE);
    }

    /**
     * Name for the view permission.
     *
     * @return string
     */
    public static function showPermission(): string
    {
        return static::buildPermission(static::SHOW);
    }

    /**
     * Name for the view permission.
     *
     * @return string
     */
    public static function viewPermission(): string
    {
        return static::buildPermission(static::VIEW);
    }

    /**
     * Name for the update permission.
     *
     * @return string
     */
    public static function updatePermission(): string
    {
        return static::buildPermission(static::UPDATE);
    }

    /**
     * Name for the delete permission.
     *
     * @return string
     */
    public static function deletePermission(): string
    {
        return static::buildPermission(static::DELETE);
    }

    /**
     * Build the policy permission prefix (inferred from class name).
     *
     * @return string
     */
    protected static function buildPermissionPrefix(): string
    {
        return Str::of(class_basename(static::class))
            ->replaceLast('Policy', '')
            ->plural()
            ->kebab()
            ->value();
    }

    /**
     * Prefix the given permission with policy prefix.
     *
     * @param string $method
     *
     * @return string
     */
    protected static function buildPermission(string $method): string
    {
        return static::buildPermissionPrefix() . '.' . Str::kebab($method);
    }

    /**
     * Perform pre-authorization checks.
     *
     * @param User|null $user
     * @param string    $ability
     *
     * @return bool|null
     */
    public function before(User|null $user, string $ability): bool|null
    {
        if ($this->shouldBlockAuthorization($user, $ability)) {
            return false;
        }

        if ($this->shouldBypassAuthorization($user, $ability)) {
            return true;
        }

        return null;
    }

    /**
     * Check if authorization should be bypassed (even if permission would not be authorized).
     *
     * @param User|null $user
     * @param string    $ability
     *
     * @return bool
     */
    protected function shouldBypassAuthorization(User|null $user, string $ability): bool
    {
        return false;
    }

    /**
     * Check if authorization should be blocked (even if permission would be authorized).
     *
     * @param User|null $user
     * @param string    $ability
     *
     * @return bool
     */
    protected function shouldBlockAuthorization(User|null $user, string $ability): bool
    {
        if (! $user) {
            return false;
        }

        if ($user->blocked()) {
            return ! $this->methodAllowsGuest(static::class, $ability);
        }

        return false;
    }

    /**
     * Check if the user can admin.
     *
     * @param User $user
     *
     * @return bool
     */
    public function admin(User $user): bool
    {
        return AuthHelper::hasPermissionTo(static::adminPermission());
    }

    /**
     * Check if the user can show any.
     *
     * @param User $user
     *
     * @return bool
     */
    public function showAny(User $user): bool
    {
        return AuthHelper::hasPermissionTo(static::showAnyPermission());
    }

    /**
     * Check if the user can view any.
     *
     * @param User $user
     *
     * @return bool
     */
    public function viewAny(User $user): bool
    {
        return AuthHelper::hasPermissionTo(static::viewAnyPermission());
    }

    /**
     * Check if the user can create one.
     *
     * @param User $user
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        return AuthHelper::hasPermissionTo(static::createPermission());
    }

    /**
     * Check if the user can show one.
     *
     * @param User   $user
     * @param TModel $model
     *
     * @return bool
     */
    public function show(User $user, object $model): bool
    {
        return AuthHelper::hasPermissionTo(static::showPermission());
    }

    /**
     * Check if the user can view one.
     *
     * @param User   $user
     * @param TModel $model
     *
     * @return bool
     */
    public function view(User $user, object $model): bool
    {
        return AuthHelper::hasPermissionTo(static::viewPermission());
    }

    /**
     * Check if the user can update one.
     *
     * @param User   $user
     * @param TModel $model
     *
     * @return bool
     */
    public function update(User $user, object $model): bool
    {
        return AuthHelper::hasPermissionTo(static::updatePermission());
    }

    /**
     * Check if the user can delete one.
     *
     * @param User   $user
     * @param TModel $model
     *
     * @return bool
     */
    public function delete(User $user, object $model): bool
    {
        return AuthHelper::hasPermissionTo(static::deletePermission());
    }

    /**
     * Determine if the given class method allows guests.
     *
     * @param string $class
     * @param string $method
     *
     * @return bool
     */
    private function methodAllowsGuest(string $class, string $method): bool
    {
        try {
            $reflection = new ReflectionClass($class);
            $reflectionMethod = $reflection->getMethod($method);
        } catch (Throwable) {
            return false;
        }

        $parameters = $reflectionMethod->getParameters();

        return isset($parameters[0]) && $this->parameterAllowsGuests($parameters[0]);
    }

    /**
     * Determine if the given parameter allows guests.
     *
     * @param ReflectionParameter $parameter
     *
     * @return bool
     */
    protected function parameterAllowsGuests(ReflectionParameter $parameter): bool
    {
        return ($parameter->hasType() && $parameter->allowsNull()) ||
            ($parameter->isDefaultValueAvailable() && is_null($parameter->getDefaultValue()));
    }
}
