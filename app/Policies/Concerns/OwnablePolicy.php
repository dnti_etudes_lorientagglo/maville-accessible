<?php

namespace App\Policies\Concerns;

use App\Helpers\AuthHelper;
use App\Models\Contracts\OrganizationOwnable;
use App\Models\Contracts\UserOwnable;

/**
 * Trait OwnablePolicy.
 *
 * @template TModel as object
 *
 * @mixin AuthorizeWithPermissions<TModel>
 */
trait OwnablePolicy
{
    /**
     * Check if user can admin model or is owner (through organization or not).
     *
     * @param UserOwnable|OrganizationOwnable $ownable
     *
     * @return bool
     */
    private function canAdminOrOwn(UserOwnable|OrganizationOwnable $ownable): bool
    {
        if (AuthHelper::hasPermissionTo(static::adminPermission())) {
            return true;
        }

        if ($ownable instanceof OrganizationOwnable && $ownable->owner_organization_id) {
            return AuthHelper::isActingFor($ownable->owner_organization_id);
        }

        $user = AuthHelper::user();

        return $ownable instanceof UserOwnable
            && $user
            && $user->id === $ownable->owner_user_id;
    }
}
