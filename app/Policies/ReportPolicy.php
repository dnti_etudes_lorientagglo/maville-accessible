<?php

namespace App\Policies;

use App\Models\Enums\RoleName;
use App\Models\User;
use App\Policies\Concerns\AuthorizeWithPermissions;
use App\Policies\Concerns\OwnablePolicy;

/**
 * Class ReportPolicy.
 */
class ReportPolicy
{
    use AuthorizeWithPermissions;
    use OwnablePolicy;

    /**
     * {@inheritDoc}
     */
    public static function permissions(): array
    {
        return [
            RoleName::MODERATOR->value => [
                static::showAnyPermission(),
                static::viewAnyPermission(),
                static::showPermission(),
                static::viewPermission(),
                static::updatePermission(),
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function create(User $user): bool
    {
        return $user->hasVerifiedEmail();
    }
}
