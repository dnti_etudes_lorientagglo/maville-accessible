<?php

namespace App\Policies;

use App\Models\Enums\RoleName;
use App\Policies\Concerns\AuthorizeWithPermissions;

/**
 * Class PermissionPolicy.
 */
class PermissionPolicy
{
    use AuthorizeWithPermissions;

    /**
     * {@inheritDoc}
     */
    public static function permissions(): array
    {
        return [
            RoleName::USER_ADMIN->value => [
                static::viewAnyPermission(),
                static::viewPermission(),
            ],
        ];
    }
}
