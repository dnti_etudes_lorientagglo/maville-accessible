<?php

namespace App\Jobs\External;

use App\Models\Contracts\Sourceable;
use App\Services\External\ExternalServicesManager;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class SendToExternalServiceJob.
 */
class SendToExternalServiceJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * SendToExternalServiceJob constructor.
     *
     * @param Model&Sourceable $model
     * @param string           $serviceName
     * @param string           $methodName
     */
    public function __construct(
        private readonly Sourceable&Model $model,
        private readonly string $serviceName,
        private readonly string $methodName,
    ) {
    }

    /**
     * Execute the job.
     *
     * @param ExternalServicesManager $manager
     */
    public function handle(ExternalServicesManager $manager): void
    {
        $manager->executeSend($this->model, $this->serviceName, $this->methodName);
    }
}
