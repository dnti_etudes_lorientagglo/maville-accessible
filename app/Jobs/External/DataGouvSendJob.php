<?php

namespace App\Jobs\External;

use App\Services\External\Concerns\CrawlTask;
use App\Services\External\DataGouvService;
use App\Services\External\ExternalServicesManager;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class DataGouvSendJob.
 */
class DataGouvSendJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * Execute the job.
     *
     * @param DataGouvService $dataGouv
     */
    public function handle(DataGouvService $dataGouv): void
    {
        $dataGouv->sendPlaces();
        $dataGouv->sendActivities();
    }
}
