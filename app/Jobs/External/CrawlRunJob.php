<?php

namespace App\Jobs\External;

use App\Services\External\Concerns\CrawlTask;
use App\Services\External\ExternalServicesManager;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class CrawlRunJob.
 */
class CrawlRunJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * CrawlRunJob constructor.
     *
     * @param array $params
     */
    public function __construct(private readonly array $params)
    {
    }

    /**
     * Execute the job.
     *
     * @param ExternalServicesManager $manager
     */
    public function handle(ExternalServicesManager $manager): void
    {
        $task = new CrawlTask(
            limit: $this->params['limit'],
            options: $this->params['options'],
            noCreate: $this->params['no-create'],
            noUpdate: $this->params['no-update'],
        );

        $manager->crawl($task, [$this->params['type']]);
    }
}
