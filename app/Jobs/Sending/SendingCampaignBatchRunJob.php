<?php

namespace App\Jobs\Sending;

use App\Models\SendingCampaignBatch;
use App\Services\Sending\SendingService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Illuminate\Queue\SerializesModels;

/**
 * Class SendingCampaignBatchRunJob.
 */
class SendingCampaignBatchRunJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * SendingCampaignBatchRunJob constructor.
     *
     * @param SendingCampaignBatch $batch
     * @param int                  $maxAttempts
     */
    public function __construct(
        private readonly SendingCampaignBatch $batch,
        private readonly int $maxAttempts,
    ) {
    }

    /**
     * Execute the job.
     *
     * @param SendingService $sendingService
     */
    public function handle(SendingService $sendingService): void
    {
        $sendingService->sendCampaignBatch($this->batch, $this->maxAttempts);
    }

    /**
     * Get the middleware the job should pass through.
     *
     * @return array<int, object>
     */
    public function middleware(): array
    {
        return [new WithoutOverlapping('sending-campaign-batch:' . $this->batch->id)];
    }
}
