<?php

namespace App\Jobs\Search;

use App\Services\Search\FulltextSearch;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class UpdateRelationSearchIndexesJob.
 */
class UpdateRelationSearchIndexesJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * UpdateRelationSearchIndexesJob constructor.
     *
     * @param Model  $model
     * @param string $relation
     * @param array  $with
     */
    public function __construct(
        private readonly Model $model,
        private readonly string $relation,
        private readonly array $with = [],
    ) {
    }

    /**
     * Execute the job.
     *
     * @param FulltextSearch $fulltextSearch
     */
    public function handle(FulltextSearch $fulltextSearch): void
    {
        $fulltextSearch->updateSearchColumnsForBuilder(
            $this->model->{$this->relation}()->with($this->with),
        );
    }
}
