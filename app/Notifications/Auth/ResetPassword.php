<?php

namespace App\Notifications\Auth;

use Illuminate\Auth\Notifications\ResetPassword as BaseResetPassword;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPassword extends BaseResetPassword implements ShouldQueue
{
    use Queueable;

    /**
     * {@inheritDoc}
     */
    protected function buildMailMessage($url): MailMessage
    {
        return (new MailMessage())
            ->subject(trans('notifications.auth.resetPassword.mail.subject'))
            ->line(trans('notifications.auth.resetPassword.mail.body'))
            ->action(trans('notifications.auth.resetPassword.mail.action'), $url)
            ->line(trans('notifications.auth.resetPassword.mail.expirationNote', [
                'count' => config('auth.passwords.' . config('auth.defaults.passwords') . '.expire'),
            ]))
            ->line(trans('notifications.auth.resetPassword.mail.note'));
    }

    /**
     * {@inheritDoc}
     */
    protected function resetUrl($notifiable): string
    {
        $email = urlencode($notifiable->getEmailForPasswordReset());

        return url("/auth/reset-password/$email/$this->token");
    }
}
