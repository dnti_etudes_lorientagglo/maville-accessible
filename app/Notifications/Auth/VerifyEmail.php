<?php

namespace App\Notifications\Auth;

use Illuminate\Auth\Notifications\VerifyEmail as BaseVerifyEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class VerifyEmail extends BaseVerifyEmail implements ShouldQueue
{
    use Queueable;

    /**
     * {@inheritDoc}
     */
    protected function buildMailMessage($url): MailMessage
    {
        return (new MailMessage())
            ->subject(trans('notifications.auth.verifyEmail.mail.subject'))
            ->line(trans('notifications.auth.verifyEmail.mail.body'))
            ->action(trans('notifications.auth.verifyEmail.mail.action'), $url)
            ->line(trans('notifications.auth.verifyEmail.mail.note'));
    }
}
