<?php

namespace App\Notifications\Auth;

use App\Notifications\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class AccountDeleted.
 */
class AccountDeleted extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * {@inheritDoc}
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }
}
