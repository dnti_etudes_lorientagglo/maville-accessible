<?php

namespace App\Notifications\Auth;

use App\Helpers\LocaleHelper;
use App\Models\OrganizationInvitation;
use App\Notifications\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class NewInvitation.
 */
class NewInvitation extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * NewInvitation constructor.
     *
     * @param OrganizationInvitation $invitation
     */
    public function __construct(private readonly OrganizationInvitation $invitation)
    {
    }

    /**
     * {@inheritDoc}
     */
    protected function buildAction(object $notifiable): string | null
    {
        return url('/auth/invitations/' . $this->invitation->id);
    }

    /**
     * {@inheritDoc}
     */
    protected function buildData(object $notifiable): array
    {
        return [
            'organization' => LocaleHelper::translate($this->invitation->organization->name),
        ];
    }
}
