<?php

namespace App\Notifications\Auth;

use App\Helpers\DateHelper;
use App\Notifications\Notification;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class ScheduledAccountDeletion.
 */
class ScheduledAccountDeletion extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * ScheduledAccountDeletion constructor.
     *
     * @param Carbon $scheduledAt
     */
    public function __construct(private readonly Carbon $scheduledAt)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * {@inheritDoc}
     */
    protected function buildAction(object $notifiable): string|null
    {
        return url('/auth/sign-in');
    }

    /**
     * {@inheritDoc}
     */
    protected function buildMailMessage(object $notifiable, MailMessage $message): MailMessage
    {
        return parent::buildMailMessage($notifiable, $message)
            ->line($this->getMailTranslation('note'));
    }

    /**
     * {@inheritDoc}
     */
    protected function buildData(object $notifiable): array
    {
        return [
            'scheduledAt' => DateHelper::format($this->scheduledAt),
        ];
    }
}
