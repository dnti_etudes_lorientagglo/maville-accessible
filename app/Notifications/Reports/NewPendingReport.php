<?php

namespace App\Notifications\Reports;

use App\Helpers\LocaleHelper;
use App\Helpers\RouteHelper;
use App\Models\Report;
use App\Notifications\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class NewPendingReport.
 */
class NewPendingReport extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * NewPendingReport constructor.
     *
     * @param Report $report
     */
    public function __construct(private readonly Report $report)
    {
    }

    /**
     * {@inheritDoc}
     */
    protected function buildAction(object $notifiable): string|null
    {
        return url(RouteHelper::resourceOne('reports', $this->report->id));
    }

    /**
     * {@inheritDoc}
     */
    protected function buildData(object $notifiable): array
    {
        return [
            'reportType' => LocaleHelper::translate($this->report->reportType->name),
            'user'       => $this->report->ownerUser->fullName,
        ];
    }
}
