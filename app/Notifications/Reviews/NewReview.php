<?php

namespace App\Notifications\Reviews;

use App\Helpers\LocaleHelper;
use App\Helpers\RouteHelper;
use App\Models\Review;
use App\Notifications\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class NewReview.
 */
class NewReview extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * NewReview constructor.
     *
     * @param Review $review
     */
    public function __construct(private readonly Review $review)
    {
    }

    /**
     * {@inheritDoc}
     */
    protected function buildAction(object $notifiable): string | null
    {
        return url(RouteHelper::reviewable($this->review->reviewable));
    }

    /**
     * {@inheritDoc}
     */
    protected function buildData(object $notifiable): array
    {
        return [
            'name' => LocaleHelper::translate($this->review->reviewable->name),
        ];
    }
}
