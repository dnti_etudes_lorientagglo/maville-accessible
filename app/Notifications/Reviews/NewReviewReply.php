<?php

namespace App\Notifications\Reviews;

use App\Helpers\LocaleHelper;
use App\Helpers\RouteHelper;
use App\Models\ReviewReply;
use App\Notifications\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class NewReviewReply.
 */
class NewReviewReply extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * NewReview constructor.
     *
     * @param ReviewReply $reviewReply
     */
    public function __construct(private readonly ReviewReply $reviewReply)
    {
    }

    /**
     * {@inheritDoc}
     */
    protected function buildAction(object $notifiable): string | null
    {
        return url(RouteHelper::reviewable($this->reviewReply->review->reviewable));
    }

    /**
     * {@inheritDoc}
     */
    protected function buildData(object $notifiable): array
    {
        return [
            'name' => LocaleHelper::translate($this->reviewReply->review->reviewable->name),
        ];
    }
}
