<?php

namespace App\Notifications;

use App\Helpers\NotificationsHelper;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification as BaseNotification;

/**
 * Class Notification.
 *
 * Basic notification class for simple notification with preferences support.
 */
abstract class Notification extends BaseNotification
{
    use Queueable;

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        if ($notifiable instanceof User) {
            return NotificationsHelper::channelsFor(
                static::class,
                $notifiable->filteredNotificationsPreferences,
            );
        }

        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param object $notifiable
     *
     * @return MailMessage
     */
    public function toMail(object $notifiable): MailMessage
    {
        return $this->buildMailMessage($notifiable, new MailMessage());
    }

    /**
     * Get the database representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toDatabase(object $notifiable): array
    {
        return $this->buildDatabaseMessage($notifiable);
    }

    /**
     * Build the mail message.
     *
     * @param object      $notifiable
     * @param MailMessage $message
     *
     * @return MailMessage
     */
    protected function buildMailMessage(object $notifiable, MailMessage $message): MailMessage
    {
        $data = $this->buildMailData($notifiable);
        $action = $this->buildAction($notifiable);

        return $message
            ->subject($this->getMailTranslation('subject', $data))
            ->line($this->getMailTranslation('body', $data))
            ->when($action, fn(MailMessage $m) => $m->action($this->getMailTranslation('action', $data), $action));
    }

    /**
     * Build the message data.
     *
     * @param object $notifiable
     *
     * @return array
     */
    protected function buildMailData(object $notifiable): array
    {
        return $this->buildData($notifiable);
    }

    /**
     * Build the database data.
     *
     * @param object $notifiable
     *
     * @return array
     */
    protected function buildDatabaseMessage(object $notifiable): array
    {
        $data = $this->buildDatabaseData($notifiable);
        $action = $this->buildAction($notifiable);

        return [
            'title'  => $this->getDatabaseTranslation('title', $data),
            'action' => $action ? [
                'href'  => $action,
                'label' => $this->getDatabaseTranslation('action', $data),
            ] : null,
        ];
    }

    /**
     * Build the message data.
     *
     * @param object $notifiable
     *
     * @return array
     */
    protected function buildDatabaseData(object $notifiable): array
    {
        return $this->buildData($notifiable);
    }

    /**
     * Build the message data.
     *
     * @param object $notifiable
     *
     * @return array
     */
    protected function buildData(object $notifiable): array
    {
        return [];
    }

    /**
     * Build the message action.
     *
     * @param object $notifiable
     *
     * @return string|null
     */
    protected function buildAction(object $notifiable): string|null
    {
        return null;
    }

    /**
     * Translate using key for the mail notification scope.
     *
     * @param string $key
     * @param array  $data
     *
     * @return string
     */
    protected function getMailTranslation(string $key, array $data = []): string
    {
        return $this->getTranslation('mail', $key, $data);
    }

    /**
     * Translate using key for the mail notification scope.
     *
     * @param string $key
     * @param array  $data
     *
     * @return string
     */
    protected function getDatabaseTranslation(string $key, array $data = []): string
    {
        return $this->getTranslation('database', $key, $data);
    }

    /**
     * Get the translation key for scope, key and data.
     *
     * @param string $scope
     * @param string $key
     *
     * @return string
     */
    protected function getTranslationKey(string $scope, string $key): string
    {
        return "notifications.$scope.$key";
    }

    /**
     * Translate using key for the given notification scope.
     *
     * @param string $notificationScope
     * @param string $key
     * @param array  $data
     *
     * @return string
     */
    private function getTranslation(string $notificationScope, string $key, array $data = []): string
    {
        return trans(
            $this->getTranslationKey("{$this->getClassScope()}.$notificationScope", $key),
            $data,
        );
    }

    /**
     * Get the notification class scope.
     *
     * @return string
     */
    private function getClassScope(): string
    {
        return NotificationsHelper::notificationTranslations(static::class);
    }
}
