<?php

namespace App\Notifications\Requests;

use App\Notifications\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Str;

/**
 * Class NewRefusedRequest.
 */
class NewRefusedRequest extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * NewRefusedRequest constructor.
     *
     * @param string $reason
     */
    public function __construct(private readonly string $reason)
    {
    }

    /**
     * {@inheritDoc}
     */
    protected function buildData(object $notifiable): array
    {
        return [
            'reason' => Str::replace("\n", ' ', $this->reason),
        ];
    }
}
