<?php

namespace App\Notifications\Requests;

use App\Helpers\LocaleHelper;
use App\Models\Enums\RequestTypeCode;
use App\Models\Organization;
use App\Models\Place;
use App\Models\Request;
use App\Notifications\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use InvalidArgumentException;

/**
 * Class NewValidatedRequest.
 */
class NewValidatedRequest extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * NewValidatedRequest constructor.
     *
     * @param Request $request
     */
    public function __construct(private readonly Request $request)
    {
    }

    /**
     * {@inheritDoc}
     */
    protected function buildMailMessage(object $notifiable, MailMessage $message): MailMessage
    {
        $this->request->loadMissing(['requestType', 'requestable', 'resultable']);

        $data = $this->buildMailData($notifiable);

        if ($this->request->requestType->code === RequestTypeCode::CLAIM->value) {
            return $message
                ->subject($this->getMailTranslation('claim.subject', $data))
                ->line($this->getMailTranslation('claim.body', $data));
        }

        if ($this->request->requestType->code === RequestTypeCode::JOIN->value) {
            return $message
                ->subject($this->getMailTranslation('join.subject', $data))
                ->line($this->getMailTranslation('join.body', $data));
        }

        throw new InvalidArgumentException('invalid request type given');
    }

    /**
     * {@inheritDoc}
     */
    protected function buildDatabaseMessage(object $notifiable): array
    {
        $data = $this->buildDatabaseData($notifiable);

        if ($this->request->requestType->code === RequestTypeCode::CLAIM->value) {
            return [
                'title'  => $this->getDatabaseTranslation('claim.title', $data),
                'action' => null,
            ];
        }

        if ($this->request->requestType->code === RequestTypeCode::JOIN->value) {
            return [
                'title'  => $this->getDatabaseTranslation('join.title', $data),
                'action' => null,
            ];
        }

        throw new InvalidArgumentException('invalid request type given');
    }

    /**
     * {@inheritDoc}
     */
    protected function buildData(object $notifiable): array
    {
        if ($this->request->requestType->code === RequestTypeCode::CLAIM->value) {
            /** @var Place $requestable */
            $requestable = $this->request->requestable;
            /** @var Organization $organization */
            $organization = $this->request->resultable;

            return [
                'requestable'  => LocaleHelper::translate($requestable->name),
                'organization' => LocaleHelper::translate($organization->name),
            ];
        }

        if ($this->request->requestType->code === RequestTypeCode::JOIN->value) {
            /** @var Organization $requestable */
            $requestable = $this->request->requestable;

            return [
                'requestable' => LocaleHelper::translate($requestable->name),
            ];
        }

        throw new InvalidArgumentException('invalid request type given');
    }
}
