<?php

namespace App\Notifications\Requests;

use App\Helpers\LocaleHelper;
use App\Helpers\RouteHelper;
use App\Models\Request;
use App\Notifications\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class NewPendingRequest.
 */
class NewPendingRequest extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * NewPendingRequest constructor.
     *
     * @param Request $request
     */
    public function __construct(private readonly Request $request)
    {
    }

    /**
     * {@inheritDoc}
     */
    protected function buildAction(object $notifiable): string|null
    {
        return url(RouteHelper::resourceOne('requests', $this->request->id));
    }

    /**
     * {@inheritDoc}
     */
    protected function buildData(object $notifiable): array
    {
        return [
            'requestType' => LocaleHelper::translate($this->request->requestType->name),
            'user'        => $this->request->ownerUser->fullName,
        ];
    }
}
