<?php

namespace App\Notifications\Reactions;

use App\Helpers\RouteHelper;
use App\Models\Reaction;
use App\Models\Review;
use App\Notifications\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use InvalidArgumentException;

/**
 * Class NewReaction.
 */
class NewReaction extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * NewReaction constructor.
     *
     * @param Reaction $reaction
     */
    public function __construct(private readonly Reaction $reaction)
    {
    }

    /**
     * {@inheritDoc}
     */
    protected function buildAction(object $notifiable): string|null
    {
        if ($this->reaction->reactable instanceof Review) {
            return url(RouteHelper::reviewable($this->reaction->reactable->reviewable));
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    protected function getTranslationKey(string $scope, string $key): string
    {
        if ($this->reaction->reactable instanceof Review) {
            return "notifications.$scope.review.$key";
        }

        throw new InvalidArgumentException('reaction cannot bound to non review contents');
    }
}
