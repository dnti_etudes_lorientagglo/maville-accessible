<?php

namespace App\Providers;

use App\Listeners\Auth\UpdateLastLoginTimestamp;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * Class EventServiceProvider.
 */
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        Login::class      => [
            UpdateLastLoginTimestamp::class,
        ],
    ];

    /**
     * {@inheritDoc}
     */
    public function boot(): void
    {
    }

    /**
     * {@inheritDoc}
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
