<?php

namespace App\Providers;

use App\Models\Activity;
use App\Models\ActivityType;
use App\Models\Article;
use App\Models\Category;
use App\Models\Config;
use App\Models\Media;
use App\Models\Organization;
use App\Models\OrganizationInvitation;
use App\Models\OrganizationMember;
use App\Models\Place;
use App\Models\Report;
use App\Models\ReportType;
use App\Models\Request;
use App\Models\Review;
use App\Models\Role;
use App\Models\SendingCampaign;
use App\Models\User;
use App\Services\Validation\Validator;
use App\Services\Validation\ValidatorFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Config as LaravelConfig;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

/**
 * Class AppServiceProvider.
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        if ($this->app->environment('local')) {
            Model::shouldBeStrict();
        }

        if (Str::startsWith(LaravelConfig::get('app.url'), 'https://')) {
            URL::forceScheme('https');
        }

        Relation::enforceMorphMap([
            'configs'                  => Config::class,
            'users'                    => User::class,
            'roles'                    => Role::class,
            'media'                    => Media::class,
            'report-types'             => ReportType::class,
            'reports'                  => Report::class,
            'requests'                 => Request::class,
            'sending-campaigns'        => SendingCampaign::class,
            'categories'               => Category::class,
            'organization-members'     => OrganizationMember::class,
            'organization-invitations' => OrganizationInvitation::class,
            'organizations'            => Organization::class,
            'reviews'                  => Review::class,
            'places'                   => Place::class,
            'articles'                 => Article::class,
            'activity-types'           => ActivityType::class,
            'activities'               => Activity::class,
        ]);

        $this->app->extend('validator', function () {
            /** @var Validator $validator */
            $validator = $this->app->get(ValidatorFactory::class);

            $validator->setPresenceVerifier($this->app->get('validation.presence'));

            return $validator;
        });
    }
}
