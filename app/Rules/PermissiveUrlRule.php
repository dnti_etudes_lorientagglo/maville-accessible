<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Validation\Concerns\ValidatesAttributes;

/**
 * Class PermissiveUrlRule.
 */
class PermissiveUrlRule implements ValidationRule
{
    use ValidatesAttributes;

    /**
     * Create a new rule instance.
     *
     * @return static
     */
    public static function make(): static
    {
        return new static();
    }

    /**
     * {@inheritDoc}
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (
            ! is_string($value) || (
                ! $this->validateUrl($attribute, $value) && ! $this->validateUrl($attribute, 'https://' . $value)
            )
        ) {
            $fail(__('validation.url'));
        }
    }
}
