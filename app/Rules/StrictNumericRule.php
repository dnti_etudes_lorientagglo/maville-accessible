<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

/**
 * Class StrictNumericRule.
 */
class StrictNumericRule implements ValidationRule
{
    /**
     * Create a new rule instance.
     *
     * @return static
     */
    public static function make(): static
    {
        return new static();
    }

    /**
     * {@inheritDoc}
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (! is_int($value) && ! is_float($value)) {
            $fail(__('validation.numeric'));
        }
    }
}
