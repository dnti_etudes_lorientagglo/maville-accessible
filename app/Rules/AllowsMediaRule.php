<?php

namespace App\Rules;

use App\Helpers\AuthHelper;
use App\Models\Config;
use App\Models\Enums\FileVisibility;
use App\Models\Media;
use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Support\Collection;

/**
 * Class AllowsMediaRule.
 */
class AllowsMediaRule
{
    /**
     * Create a new rule instance.
     *
     * @param FileVisibility $visibility
     *
     * @return RelatedRule
     */
    public static function make(FileVisibility $visibility = FileVisibility::PUBLIC): RelatedRule
    {
        return new RelatedRule(
            static fn(Collection $data) => Media::query()
                ->whereIn('id', $data->map(static fn(array $item) => $item['id']))
                ->where(
                    static fn(Builder $query) => $query
                        // Owned by user.
                        ->whereMorphedTo('model', AuthHelper::user())
                        // Created by app.
                        ->orWhere(
                            static fn(Builder $query) => $query
                                ->where('model_type', 'configs')
                                ->where('collection_name', Config::MEDIA_PUBLIC_COLLECTION),
                        ),
                )
                ->where(
                    static fn(Builder $query) => $query
                        ->where('custom_properties->visibility', $visibility->value)
                        ->when($visibility === FileVisibility::PUBLIC)
                        ->orWhereNull('custom_properties->visibility'),
                )
                ->count(),
            static fn(int $count, Collection $data) => $count === $data->count(),
        );
    }
}
