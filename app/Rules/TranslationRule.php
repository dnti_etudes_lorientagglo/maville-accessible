<?php

namespace App\Rules;

use App\Helpers\LocaleHelper;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

/**
 * Class TranslationRule.
 */
class TranslationRule implements ValidationRule
{
    /**
     * Create a new rule instance.
     *
     * @param array $rules
     *
     * @return static
     */
    public static function make(array $rules = []): static
    {
        return new static($rules);
    }

    /**
     * TranslationRule constructor.
     *
     * @param array $rules
     */
    public function __construct(private readonly array $rules)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if ($value !== null && ! is_array($value)) {
            $fail(__('validation.custom.translation.format'));

            return;
        }

        $allowedLocales = LocaleHelper::allLocalesKeys();
        $translations = collect($value ?? []);
        $locales = $translations->keys();
        if (! $locales->every(static fn(string $l) => $allowedLocales->contains($l))) {
            $fail(__('validation.custom.translation.format'));

            return;
        }

        [$translationsRules, $translationRules] = collect($this->rules)->partition(
            static fn($rule) => in_array($rule, ['required', 'nullable']),
        );

        $isRequired = $translationsRules->contains('required');

        if ($isRequired && $translations->every(fn($l) => empty($l) && $l !== '0')) {
            $fail(__('validation.custom.translation.content', [
                'message' => Str::lcfirst(
                    Validator::make([], [$attribute => 'required'])->messages()->first(),
                ),
            ]));

            return;
        }

        $validator = Validator::make($value ?? [], [
            '*' => ['nullable', 'string', ...$translationRules],
        ]);

        $firstError = $validator->messages()->first();
        if ($firstError) {
            $fail(__('validation.custom.translation.content', [
                'message' => Str::lcfirst(
                    Str::replace($locales, $attribute, $firstError),
                ),
            ]));
        }
    }
}
