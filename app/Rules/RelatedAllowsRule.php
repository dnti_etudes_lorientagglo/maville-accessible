<?php

namespace App\Rules;

use App\Helpers\AuthHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class RelatedAllowsRule.
 */
class RelatedAllowsRule
{
    /**
     * Create a rule instance.
     *
     * @param Model  $model
     * @param string $ability
     *
     * @return RelatedRule
     */
    public static function make(Model $model, string $ability): RelatedRule
    {
        return new RelatedRule(
            static fn(Collection $data) => $model->newQuery()
                ->whereIn('id', $data->map(static fn(array $item) => $item['id']))
                ->get(),
            static function (Collection $models, Collection $data) use ($ability) {
                $user = AuthHelper::user();
                if (
                    $user
                    && $models->count() === $data->count()
                    && $models->every(static fn(Model $model) => $user->can($ability, $model))
                ) {
                    return true;
                }

                return false;
            },
        );
    }
}
