<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

/**
 * Class ColorRule.
 */
class ColorRule implements ValidationRule
{
    /**
     * Create a rule instance.
     *
     * @return static
     */
    public static function make(): static
    {
        return new static();
    }

    /**
     * {@inheritDoc}
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (! is_string($value) || preg_match('/^#([a-f0-9]{6}|[a-f0-9]{3})$/i', $value) !== 1) {
            $fail(__('validation.custom.color.invalid'));
        }
    }
}
