<?php

namespace App\Rules;

use Illuminate\Support\Facades\Gate;

/**
 * Class AllowsRule.
 */
class AllowsRule
{
    /**
     * Create a rule instance.
     *
     * @param string $ability
     * @param mixed  $arguments
     *
     * @return static
     */
    public static function make(string $ability, mixed $arguments = []): static
    {
        return new static($ability, $arguments);
    }

    /**
     * AllowsRule constructor.
     *
     * @param string $ability
     * @param mixed  $arguments
     */
    public function __construct(
        private readonly string $ability,
        private readonly mixed $arguments = [],
    ) {
    }

    /**
     * Convert the rule to a validation string.
     *
     * @return string
     */
    public function __toString()
    {
        return Gate::allows($this->ability, $this->arguments) ? '' : 'missing';
    }
}
