<?php

namespace App\Rules;

use App\Helpers\RuleHelper;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

/**
 * Class RelatedRule.
 */
class RelatedRule implements ValidationRule
{
    /**
     * Create a new rule instance.
     *
     * @param Closure    $query
     * @param Closure    $validate
     * @param mixed|null $prev
     *
     * @return static
     */
    public static function make(Closure $query, Closure $validate, mixed $prev = null): static
    {
        return new static($query, $validate, $prev);
    }

    /**
     * RelatedRule constructor.
     *
     * @param Closure    $query
     * @param Closure    $validate
     * @param mixed|null $prev
     */
    public function __construct(
        private readonly Closure $query,
        private readonly Closure $validate,
        private mixed $prev = null,
    ) {
    }

    /**
     * Use the previous value to validate the data.
     *
     * @param mixed $prev
     *
     * @return $this
     */
    public function withPrevious(mixed $prev): static
    {
        $this->prev = $prev;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $nextValue = RuleHelper::normalizeRelated($value);
        if ($nextValue->isEmpty()) {
            return;
        }

        $prevValue = RuleHelper::normalizeRelated($this->prev);
        if ($nextValue->every(static fn(mixed $v, mixed $k) => $v == $prevValue->get($k))) {
            return;
        }

        $result = call_user_func_array($this->query, [$nextValue]);
        if (! call_user_func_array($this->validate, [$result, $nextValue])) {
            $fail(__('validation.in'));
        }
    }
}
