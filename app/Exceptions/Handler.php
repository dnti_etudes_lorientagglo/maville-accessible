<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Exceptions\InvalidSignatureException;
use LaravelJsonApi\Core\Exceptions\JsonApiException;
use LaravelJsonApi\Exceptions\ExceptionParser;
use Psr\Log\LogLevel;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    private const HTTP_403_INVALID_SIGNATURE_ROUTE = '/errors/403-invalid-signature';

    private const HTTP_403_FORBIDDEN_ROUTE = '/errors/403-forbidden';

    private const HTTP_404_NOT_FOUND_ROUTE = '/errors/404-not-found';

    private const HTTP_429_TOO_MANY_ATTEMPTS_ROUTE = '/errors/429-too-many-attempts';

    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<Throwable>, LogLevel::*>
     */
    protected $levels = [];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        JsonApiException::class,
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * A list of errors to redirect on user-friendly error pages.
     *
     * @var string[]
     */
    protected $redirectTo = [
        InvalidSignatureException::class => self::HTTP_403_INVALID_SIGNATURE_ROUTE,
        AuthorizationException::class    => self::HTTP_403_FORBIDDEN_ROUTE,
        AccessDeniedHttpException::class => self::HTTP_403_FORBIDDEN_ROUTE,
        NotFoundHttpException::class     => self::HTTP_404_NOT_FOUND_ROUTE,
        ThrottleRequestsException::class => self::HTTP_429_TOO_MANY_ATTEMPTS_ROUTE,
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register(): void
    {
        $this->renderable(function (Throwable $exception, Request $request) {
            if (! $request->wantsJson()) {
                $redirectTo = $this->redirectTo[$exception::class] ?? null;
                if ($redirectTo) {
                    return new RedirectResponse($redirectTo);
                }
            }
        });

        $this->renderable(ExceptionParser::make()->renderable());
    }
}
