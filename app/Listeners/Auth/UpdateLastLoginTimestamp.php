<?php

namespace App\Listeners\Auth;

use App\Models\User;
use Illuminate\Auth\Events\Login;

/**
 * Class UpdateLastLoginTimestamp.
 */
class UpdateLastLoginTimestamp
{
    /**
     * Handle the event.
     *
     * @param Login $event
     */
    public function handle(Login $event): void
    {
        /** @var User $user */
        $user = $event->user;

        $user->last_login_at = now();
        $user->scheduled_deletion_at = null;

        $user->save();
    }
}
