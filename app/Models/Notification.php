<?php

namespace App\Models;

use App\Models\Casts\AsArrayObject;
use App\Models\Concerns\UuidKeyModel;
use Carbon\Carbon;
use Database\Factories\NotificationFactory;
use Illuminate\Database\Eloquent\Casts\ArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Class Notification.
 *
 * @property string           $type
 * @property string           $notifiable_type
 * @property string           $notifiable_id
 * @property User             $notifiable
 * @property ArrayObject|null $data
 * @property Carbon|null      $read_at
 *
 * @method static NotificationFactory factory()
 */
class Notification extends UuidKeyModel
{
    use HasFactory;

    protected $fillable = [
        'read_at',
    ];

    protected $casts = [
        'data'    => AsArrayObject::class,
        'read_at' => 'date',
    ];

    /**
     * @return MorphTo
     */
    public function notifiable(): MorphTo
    {
        return $this->morphTo();
    }
}
