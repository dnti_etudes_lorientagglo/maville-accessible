<?php

namespace App\Models\Enums;

/**
 * Class SourceOrigin.
 */
enum SourceOrigin: string
{
    /**
     * French government AccesLibre API.
     *
     * @link https://acceslibre.beta.gouv.fr/
     */
    case ACCESLIBRE = 'acceslibre';

    /**
     * French news and events service InfoLocale API.
     *
     * @link https://www.infolocale.fr/
     */
    case INFOLOCALE = 'infolocale';
}
