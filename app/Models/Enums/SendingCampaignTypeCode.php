<?php

namespace App\Models\Enums;

/**
 * Enum SendingCampaignTypeCode.
 */
enum SendingCampaignTypeCode: string
{
    /**
     * Send a mail to contacts.
     */
    case MAIL = 'mail';
}
