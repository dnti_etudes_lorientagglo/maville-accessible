<?php

namespace App\Models\Enums;

/**
 * Enum RoleScopeName.
 */
enum RoleScopeName: string
{
    /**
     * The role is applicable with no additional context.
     */
    case GLOBAL = 'global';

    /**
     * The role is applicable in an organization-member relationship context.
     */
    case ORGANIZATION = 'organization';
}
