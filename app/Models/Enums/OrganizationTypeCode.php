<?php

namespace App\Models\Enums;

/**
 * Enum OrganizationTypeCode.
 */
enum OrganizationTypeCode: string
{
    case CIVIL_SERVICE = 'civil-service';

    case ASSOCIATION = 'association';

    case COMPANY = 'company';
}
