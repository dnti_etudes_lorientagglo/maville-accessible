<?php

namespace App\Models\Enums;

/**
 * Enum AccessibilityCriteriaType.
 */
enum AccessibilityCriteriaType: string
{
    case BOOLEAN = 'boolean';
    case STRING = 'string';
    case TEXT = 'text';
    case INTEGER = 'integer';
    case ITEM = 'item';
    case ITEMS = 'items';
}
