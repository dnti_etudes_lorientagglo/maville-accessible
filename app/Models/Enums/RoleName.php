<?php

namespace App\Models\Enums;

/**
 * Enum RoleName.
 */
enum RoleName: string
{
    /**
     * Can manage users and roles.
     */
    case USER_ADMIN = 'user-admin';

    /**
     * Can manage application.
     */
    case APP_ADMIN = 'app-admin';

    /**
     * Can manage every content in application (organizations, places, etc.).
     */
    case CONTENT_ADMIN = 'content-admin';

    /**
     * Can manage communication for application (sending campaign, etc.).
     */
    case COMMUNICATION_ADMIN = 'communication-admin';

    /**
     * Can manage its organization information and contents.
     */
    case ORGANIZATION_ADMIN = 'organization-admin';

    /**
     * Can view its organization information and contents.
     */
    case ORGANIZATION_MEMBER = 'organization-member';

    /**
     * Can moderate other users' contents.
     */
    case MODERATOR = 'moderator';
}
