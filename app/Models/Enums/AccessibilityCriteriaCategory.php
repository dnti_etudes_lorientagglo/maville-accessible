<?php

namespace App\Models\Enums;

/**
 * Enum AccessibilityCriteriaCategory.
 */
enum AccessibilityCriteriaCategory: string
{
    case TRANSPORT = 'transport';
    case EXTERNAL = 'external';
    case ENTRANCE = 'entrance';
    case RECEPTION = 'reception';
    case COMMENTS = 'comments';
    case CONFORMITY = 'conformity';
}
