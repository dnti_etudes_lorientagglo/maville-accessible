<?php

namespace App\Models\Enums;

/**
 * Enum ReportTypeCode.
 */
enum ReportTypeCode: string
{
    case BUG = 'bug';
    case FEAT = 'feat';
    case INSULTING = 'insulting';
    case HARASSMENT = 'harassment';
    case SPAM = 'spam';
    case PERSONAL_DATA = 'personal-data';
    case ILLEGAL = 'illegal';
    case SCAM = 'scam';
    case FAKE_NEWS = 'fake-news';
    case COPYRIGHT_INFRINGEMENT = 'copyright-infringement';
    case OTHER = 'other';
}
