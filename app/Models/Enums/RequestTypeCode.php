<?php

namespace App\Models\Enums;

/**
 * Enum RequestTypeCode.
 */
enum RequestTypeCode: string
{
    /**
     * Request to become organization owner of a content.
     */
    case CLAIM = 'claim';

    /**
     * Request to join or create an organization.
     */
    case JOIN = 'join';
}
