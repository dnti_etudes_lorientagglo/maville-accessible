<?php

namespace App\Models\Enums;

/**
 * Enum ActivityPricingCode.
 */
enum ActivityPricingCode: string
{
    /**
     * Free event.
     */
    case FREE = 'free';

    /**
     * Free under conditions event.
     */
    case CONDITIONED = 'conditioned';

    /**
     * Paid event.
     */
    case PAID = 'paid';
}
