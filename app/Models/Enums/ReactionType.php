<?php

namespace App\Models\Enums;

/**
 * Enum ReactionType.
 */
enum ReactionType: string
{
    case LIKE = 'like';
}
