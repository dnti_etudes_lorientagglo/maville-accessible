<?php

namespace App\Models\Enums;

/**
 * Enum ActivityTypeCode.
 */
enum ActivityTypeCode: string
{
    /**
     * Activity represent a non-recurring event.
     */
    case EVENTS = 'events';

    /**
     * Activity represent a recurring activity.
     */
    case ACTIVITIES = 'activities';
}
