<?php

namespace App\Models\Enums;

/**
 * Enum ActivityPublicCode.
 */
enum ActivityPublicCode: string
{
    case ALL = 'all';
    case BABY = 'baby';
    case CHILDREN = 'children';
    case TEENAGER = 'teenager';
    case ADULT = 'adult';
    case SENIOR = 'senior';
}
