<?php

namespace App\Models\Enums;

/**
 * Enum FileVisibility.
 */
enum FileVisibility: string
{
    case PUBLIC = 'public';
    case PRIVATE = 'private';
}
