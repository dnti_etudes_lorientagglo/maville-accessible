<?php

namespace App\Models\Enums;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use ReflectionClass;
use ReflectionMethod;

/**
 * Class AccessibilityCriteria.
 */
class AccessibilityCriteria implements Arrayable
{
    public static function transportStation(): static
    {
        return new static(true, 'transportStation', AccessibilityCriteriaCategory::TRANSPORT, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true);
    }

    public static function transportInformation(): static
    {
        return new static(true, 'transportInformation', AccessibilityCriteriaCategory::TRANSPORT, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::TEXT, null, [
            'maxLength' => 1000,
            'dependsOn' => [
                static::transportStation()->id => true,
            ],
        ]);
    }

    public static function innerParking(): static
    {
        return new static(true, 'innerParking', AccessibilityCriteriaCategory::TRANSPORT, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true);
    }

    public static function innerParkingPrm(): static
    {
        return new static(true, 'innerParkingPrm', AccessibilityCriteriaCategory::TRANSPORT, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true, [
            'dependsOn' => [
                static::innerParking()->id => true,
            ],
        ]);
    }

    public static function externalParking(): static
    {
        return new static(true, 'externalParking', AccessibilityCriteriaCategory::TRANSPORT, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true);
    }

    public static function externalParkingPrm(): static
    {
        return new static(true, 'externalParkingPrm', AccessibilityCriteriaCategory::TRANSPORT, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true, [
            'dependsOn' => [
                static::externalParking()->id => true,
            ],
        ]);
    }

    public static function externalPath(): static
    {
        return new static(true, 'externalPath', AccessibilityCriteriaCategory::EXTERNAL, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, null);
    }

    public static function externalPathStable(): static
    {
        return new static(true, 'externalPathStable', AccessibilityCriteriaCategory::EXTERNAL, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true, [
            'dependsOn' => [
                static::externalPath()->id => true,
            ],
        ]);
    }

    public static function externalPathFlat(): static
    {
        return new static(true, 'externalPathFlat', AccessibilityCriteriaCategory::EXTERNAL, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true, [
            'dependsOn' => [
                static::externalPath()->id => true,
            ],
        ]);
    }

    public static function externalPathElevator(): static
    {
        return new static(false, 'externalPathElevator', AccessibilityCriteriaCategory::EXTERNAL, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true, [
            'dependsOn' => [
                static::externalPathFlat()->id => false,
            ],
        ]);
    }

    public static function externalPathStairsCount(): static
    {
        return new static(false, 'externalPathStairsCount', AccessibilityCriteriaCategory::EXTERNAL, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::INTEGER, null, [
            'dependsOn' => [
                static::externalPathFlat()->id => false,
            ],
        ]);
    }

    public static function externalPathStairsVisibility(): static
    {
        return new static(true, 'externalPathStairsVisibility', AccessibilityCriteriaCategory::EXTERNAL, [
            AccessibilityCategory::VISUAL,
        ], AccessibilityCriteriaType::BOOLEAN, true, [
            'dependsOn' => [
                static::externalPathFlat()->id => false,
            ],
        ]);
    }

    public static function externalPathStairsDirection(): static
    {
        return new static(false, 'externalPathStairsDirection', AccessibilityCriteriaCategory::EXTERNAL, [
            AccessibilityCategory::VISUAL,
        ], AccessibilityCriteriaType::ITEM, null, [
            'values'    => ['ascending', 'descending'],
            'dependsOn' => [
                static::externalPathFlat()->id => false,
            ],
        ]);
    }

    public static function externalPathStairsHandrail(): static
    {
        return new static(true, 'externalPathStairsHandrail', AccessibilityCriteriaCategory::EXTERNAL, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true, [
            'dependsOn' => [
                static::externalPathFlat()->id => false,
            ],
        ]);
    }

    public static function externalPathStairsRail(): static
    {
        return new static(true, 'externalPathStairsRail', AccessibilityCriteriaCategory::EXTERNAL, [
            AccessibilityCategory::PHYSICAL,
        ], AccessibilityCriteriaType::ITEM, ['fixed', 'removable'], [
            'values'    => ['none', 'fixed', 'removable'],
            'dependsOn' => [
                static::externalPathFlat()->id => false,
            ],
        ]);
    }

    public static function externalPathSlope(): static
    {
        return new static(false, 'externalPathSlope', AccessibilityCriteriaCategory::EXTERNAL, [
            AccessibilityCategory::PHYSICAL,
        ], AccessibilityCriteriaType::BOOLEAN, false, [
            'dependsOn' => [
                static::externalPath()->id => true,
            ],
        ]);
    }

    public static function externalPathSlopeLevel(): static
    {
        return new static(false, 'externalPathSlopeLevel', AccessibilityCriteriaCategory::EXTERNAL, [
            AccessibilityCategory::PHYSICAL,
        ], AccessibilityCriteriaType::ITEM, null, [
            'values'    => ['low', 'high'],
            'dependsOn' => [
                static::externalPathSlope()->id => true,
            ],
        ]);
    }

    public static function externalPathSlopeLength(): static
    {
        return new static(false, 'externalPathSlopeLength', AccessibilityCriteriaCategory::EXTERNAL, [
            AccessibilityCategory::PHYSICAL,
        ], AccessibilityCriteriaType::ITEM, null, [
            'values'    => ['short', 'medium', 'long'],
            'dependsOn' => [
                static::externalPathSlope()->id => true,
            ],
        ]);
    }

    public static function externalPathInclinationLevel(): static
    {
        return new static(false, 'externalPathInclinationLevel', AccessibilityCriteriaCategory::EXTERNAL, [
            AccessibilityCategory::PHYSICAL,
        ], AccessibilityCriteriaType::ITEM, ['none'], [
            'values'    => ['none', 'low', 'high'],
            'dependsOn' => [
                static::externalPath()->id => true,
            ],
        ]);
    }

    public static function externalPathGuideStrip(): static
    {
        return new static(true, 'externalPathGuideStrip', AccessibilityCriteriaCategory::EXTERNAL, [
            AccessibilityCategory::VISUAL,
        ], AccessibilityCriteriaType::BOOLEAN, true, [
            'dependsOn' => [
                static::externalPath()->id => true,
            ],
        ]);
    }

    public static function externalPathShrink(): static
    {
        return new static(false, 'externalPathShrink', AccessibilityCriteriaCategory::EXTERNAL, [
            AccessibilityCategory::PHYSICAL,
        ], AccessibilityCriteriaType::BOOLEAN, false, [
            'dependsOn' => [
                static::externalPath()->id => true,
            ],
        ]);
    }

    public static function entranceVisible(): static
    {
        return new static(true, 'entranceVisible', AccessibilityCriteriaCategory::ENTRANCE, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true);
    }

    public static function entranceDoor(): static
    {
        return new static(true, 'entranceDoor', AccessibilityCriteriaCategory::ENTRANCE, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, null);
    }

    public static function entranceDoorMechanism(): static
    {
        return new static(false, 'entranceDoorMechanism', AccessibilityCriteriaCategory::ENTRANCE, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::ITEM, null, [
            'values'    => ['swing', 'sliding', 'turnstile', 'revolving'],
            'dependsOn' => [
                static::entranceDoor()->id => true,
            ],
        ]);
    }

    public static function entranceDoorType(): static
    {
        return new static(true, 'entranceDoorType', AccessibilityCriteriaCategory::ENTRANCE, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::ITEM, ['automatic'], [
            'values'    => ['manual', 'automatic'],
            'dependsOn' => [
                static::entranceDoor()->id => true,
            ],
        ]);
    }

    public static function entranceWindow(): static
    {
        return new static(false, 'entranceWindow', AccessibilityCriteriaCategory::ENTRANCE, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true, [
            'dependsOn' => [
                static::entranceDoor()->id => true,
            ],
        ]);
    }

    public static function entranceWindowContrast(): static
    {
        return new static(true, 'entranceWindowContrast', AccessibilityCriteriaCategory::ENTRANCE, [
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true, [
            'dependsOn' => [
                static::entranceWindow()->id => true,
            ],
        ]);
    }

    public static function entranceCallDevice(): static
    {
        return new static(true, 'entranceCallDevice', AccessibilityCriteriaCategory::ENTRANCE, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true);
    }

    public static function entranceCallDeviceType(): static
    {
        return new static(false, 'entranceCallDeviceType', AccessibilityCriteriaCategory::ENTRANCE, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::ITEMS, true, [
            'values'    => ['button', 'intercom', 'visiophone'],
            'dependsOn' => [
                static::entranceCallDevice()->id => true,
            ],
        ]);
    }

    public static function entranceFlat(): static
    {
        return new static(true, 'entranceFlat', AccessibilityCriteriaCategory::ENTRANCE, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true);
    }

    public static function entranceElevator(): static
    {
        return new static(true, 'entranceElevator', AccessibilityCriteriaCategory::ENTRANCE, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true, [
            'dependsOn' => [
                static::entranceFlat()->id => false,
            ],
        ]);
    }

    public static function entranceStairsCount(): static
    {
        return new static(false, 'entranceStairsCount', AccessibilityCriteriaCategory::ENTRANCE, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::INTEGER, null, [
            'dependsOn' => [
                static::entranceFlat()->id => false,
            ],
        ]);
    }

    public static function entranceStairsVisibility(): static
    {
        return new static(true, 'entranceStairsVisibility', AccessibilityCriteriaCategory::ENTRANCE, [
            AccessibilityCategory::VISUAL,
        ], AccessibilityCriteriaType::BOOLEAN, true, [
            'dependsOn' => [
                static::entranceFlat()->id => false,
            ],
        ]);
    }

    public static function entranceStairsDirection(): static
    {
        return new static(false, 'entranceStairsDirection', AccessibilityCriteriaCategory::ENTRANCE, [
            AccessibilityCategory::VISUAL,
        ], AccessibilityCriteriaType::ITEM, null, [
            'values'    => ['ascending', 'descending'],
            'dependsOn' => [
                static::entranceFlat()->id => false,
            ],
        ]);
    }

    public static function entranceStairsHandrail(): static
    {
        return new static(false, 'entranceStairsHandrail', AccessibilityCriteriaCategory::ENTRANCE, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true, [
            'dependsOn' => [
                static::entranceFlat()->id => false,
            ],
        ]);
    }

    public static function entranceStairsRail(): static
    {
        return new static(true, 'entranceStairsRail', AccessibilityCriteriaCategory::ENTRANCE, [
            AccessibilityCategory::PHYSICAL,
        ], AccessibilityCriteriaType::ITEM, ['fixed', 'removable'], [
            'values'    => ['none', 'fixed', 'removable'],
            'dependsOn' => [
                static::entranceFlat()->id => false,
            ],
        ]);
    }

    public static function entranceSoundBeacon(): static
    {
        return new static(true, 'entranceSoundBeacon', AccessibilityCriteriaCategory::ENTRANCE, [
            AccessibilityCategory::VISUAL,
        ], AccessibilityCriteriaType::BOOLEAN, true);
    }

    public static function entranceHumanHelp(): static
    {
        return new static(true, 'entranceHumanHelp', AccessibilityCriteriaCategory::ENTRANCE, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true);
    }

    public static function entranceWidth(): static
    {
        return new static(false, 'entranceWidth', AccessibilityCriteriaCategory::ENTRANCE, [
            AccessibilityCategory::PHYSICAL,
        ], AccessibilityCriteriaType::INTEGER);
    }

    public static function entrancePrm(): static
    {
        return new static(true, 'entrancePrm', AccessibilityCriteriaCategory::ENTRANCE, [
            AccessibilityCategory::PHYSICAL,
        ], AccessibilityCriteriaType::BOOLEAN, true);
    }

    public static function entrancePrmInformation(): static
    {
        return new static(true, 'entrancePrmInformation', AccessibilityCriteriaCategory::ENTRANCE, [
            AccessibilityCategory::PHYSICAL,
        ], AccessibilityCriteriaType::TEXT, null, [
            'maxLength' => 500,
            'dependsOn' => [
                static::entrancePrm()->id => true,
            ],
        ]);
    }

    public static function receptionVisible(): static
    {
        return new static(false, 'receptionVisible', AccessibilityCriteriaCategory::RECEPTION, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true);
    }

    public static function receptionPersonnel(): static
    {
        return new static(true, 'receptionPersonnel', AccessibilityCriteriaCategory::RECEPTION, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::ITEM, ['trained'], [
            'values' => ['none', 'trained', 'nonTrained'],
        ]);
    }

    public static function receptionHearingEquipments(): static
    {
        return new static(true, 'receptionHearingEquipments', AccessibilityCriteriaCategory::RECEPTION, [
            AccessibilityCategory::HEARING,
        ], AccessibilityCriteriaType::BOOLEAN, true);
    }

    public static function receptionHearingEquipmentsType(): static
    {
        return new static(false, 'receptionHearingEquipmentsType', AccessibilityCriteriaCategory::RECEPTION, [
            AccessibilityCategory::HEARING,
        ], AccessibilityCriteriaType::ITEMS, true, [
            'values'    => ['bim', 'bmp', 'lsf', 'lpc', 'sts', 'other'],
            'dependsOn' => [
                static::receptionHearingEquipments()->id => true,
            ],
        ]);
    }

    public static function receptionPathFlat(): static
    {
        return new static(true, 'receptionPathFlat', AccessibilityCriteriaCategory::RECEPTION, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true);
    }

    public static function receptionPathElevator(): static
    {
        return new static(true, 'receptionPathElevator', AccessibilityCriteriaCategory::RECEPTION, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true, [
            'dependsOn' => [
                static::receptionPathFlat()->id => false,
            ],
        ]);
    }

    public static function receptionPathStairsCount(): static
    {
        return new static(false, 'receptionPathStairsCount', AccessibilityCriteriaCategory::RECEPTION, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::INTEGER, null, [
            'dependsOn' => [
                static::receptionPathFlat()->id => false,
            ],
        ]);
    }

    public static function receptionPathStairsVisibility(): static
    {
        return new static(true, 'receptionPathStairsVisibility', AccessibilityCriteriaCategory::RECEPTION, [
            AccessibilityCategory::VISUAL,
        ], AccessibilityCriteriaType::BOOLEAN, true, [
            'dependsOn' => [
                static::receptionPathFlat()->id => false,
            ],
        ]);
    }

    public static function receptionPathStairsDirection(): static
    {
        return new static(false, 'receptionPathStairsDirection', AccessibilityCriteriaCategory::RECEPTION, [
            AccessibilityCategory::VISUAL,
        ], AccessibilityCriteriaType::ITEM, null, [
            'values'    => ['ascending', 'descending'],
            'dependsOn' => [
                static::receptionPathFlat()->id => false,
            ],
        ]);
    }

    public static function receptionPathStairsHandrail(): static
    {
        return new static(false, 'receptionPathStairsHandrail', AccessibilityCriteriaCategory::RECEPTION, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true, [
            'dependsOn' => [
                static::receptionPathFlat()->id => false,
            ],
        ]);
    }

    public static function receptionPathStairsRail(): static
    {
        return new static(true, 'receptionPathStairsRail', AccessibilityCriteriaCategory::RECEPTION, [
            AccessibilityCategory::PHYSICAL,
        ], AccessibilityCriteriaType::ITEM, ['fixed', 'removable'], [
            'values'    => ['none', 'fixed', 'removable'],
            'dependsOn' => [
                static::receptionPathFlat()->id => false,
            ],
        ]);
    }

    public static function receptionPathShrink(): static
    {
        return new static(false, 'receptionPathShrink', AccessibilityCriteriaCategory::RECEPTION, [
            AccessibilityCategory::PHYSICAL,
        ], AccessibilityCriteriaType::BOOLEAN, false);
    }

    public static function restrooms(): static
    {
        return new static(true, 'restrooms', AccessibilityCriteriaCategory::RECEPTION, [
            AccessibilityCategory::RESTROOMS,
        ], AccessibilityCriteriaType::BOOLEAN, null);
    }

    public static function restroomsAdapted(): static
    {
        return new static(true, 'restroomsAdapted', AccessibilityCriteriaCategory::RECEPTION, [
            AccessibilityCategory::RESTROOMS,
        ], AccessibilityCriteriaType::BOOLEAN, true, [
            'dependsOn' => [
                static::restrooms()->id => true,
            ],
        ]);
    }

    public static function labels(): static
    {
        return new static(true, 'labels', AccessibilityCriteriaCategory::COMMENTS, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::ITEMS, true, [
            'values' => ['dpt', 'mobalib', 'th', 'other'],
        ]);
    }

    public static function labelsOther(): static
    {
        return new static(true, 'labelsOther', AccessibilityCriteriaCategory::COMMENTS, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::STRING, null, [
            'dependsOn' => [
                static::labels()->id => 'other',
            ],
        ]);
    }

    public static function labelsCategories(): static
    {
        return new static(true, 'labelsCategories', AccessibilityCriteriaCategory::COMMENTS, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::ITEMS, true, [
            'values'    => ['physical', 'visual', 'hearing', 'cognitive'],
            'dependsOn' => [
                static::labels()->id => 'any',
            ],
        ]);
    }

    public static function comment(): static
    {
        return new static(false, 'comment', AccessibilityCriteriaCategory::COMMENTS, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::TEXT, null, [
            'maxLength' => 1000,
        ]);
    }

    public static function registryURL(): static
    {
        return new static(false, 'registryURL', AccessibilityCriteriaCategory::CONFORMITY, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::STRING, true);
    }

    public static function conformity(): static
    {
        return new static(false, 'conformity', AccessibilityCriteriaCategory::CONFORMITY, [
            AccessibilityCategory::PHYSICAL,
            AccessibilityCategory::VISUAL,
            AccessibilityCategory::HEARING,
            AccessibilityCategory::COGNITIVE,
        ], AccessibilityCriteriaType::BOOLEAN, true);
    }

    /**
     * Get all accessibility criteria.
     *
     * @return Collection
     */
    public static function all(): Collection
    {
        $methods = collect((new ReflectionClass(static::class))->getMethods(ReflectionMethod::IS_STATIC));

        return $methods
            ->filter(static fn(ReflectionMethod $method) => $method->name !== 'all')
            ->map(static fn(ReflectionMethod $method) => $method->invoke(null));
    }

    /**
     * AccessibilityCriteria constructor.
     *
     * @param bool                          $editable
     * @param string                        $id
     * @param AccessibilityCriteriaCategory $criteriaCategory
     * @param array                         $accessibilityCategories
     * @param AccessibilityCriteriaType     $type
     * @param bool|null                     $positive
     * @param array                         $metadata
     */
    public function __construct(
        public readonly bool $editable,
        public readonly string $id,
        public readonly AccessibilityCriteriaCategory $criteriaCategory,
        public readonly array $accessibilityCategories,
        public readonly AccessibilityCriteriaType $type = AccessibilityCriteriaType::BOOLEAN,
        public readonly bool | array | null $positive = null,
        public readonly array $metadata = [],
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function toArray(): array
    {
        return [
            'id'                      => $this->id,
            'type'                    => $this->type->value,
            'positive'                => $this->positive,
            'editable'                => $this->editable,
            'criteriaCategory'        => $this->criteriaCategory->value,
            'accessibilityCategories' => array_map(
                static fn(AccessibilityCategory $c) => $c->value,
                $this->accessibilityCategories,
            ),
            'metadata'                => $this->metadata,
        ];
    }
}
