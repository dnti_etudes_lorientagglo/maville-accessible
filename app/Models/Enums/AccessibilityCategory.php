<?php

namespace App\Models\Enums;

/**
 * Enum AccessibilityCategory.
 */
enum AccessibilityCategory: string
{
    case PHYSICAL = 'physical';
    case VISUAL = 'visual';
    case HEARING = 'hearing';
    case COGNITIVE = 'cognitive';
    case RESTROOMS = 'restrooms';
}
