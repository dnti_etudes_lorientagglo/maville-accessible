<?php

namespace App\Models;

use App\Models\Casts\AsArrayObject;
use App\Models\Composables\IsSoftDeletable;
use App\Models\Composables\IsUserOwnable;
use App\Models\Concerns\UuidKeyModel;
use App\Models\Contracts\Requestable;
use App\Policies\RequestPolicy;
use App\Services\Search\FulltextSearch;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\ArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Class Request.
 *
 * @property string            $url
 * @property string|null       $body
 * @property ArrayObject|null  $data
 * @property string            $request_type_id
 * @property RequestType       $requestType
 * @property string|null       $requestable_type
 * @property string|null       $requestable_id
 * @property Requestable|null  $requestable
 * @property string|null       $resultable_type
 * @property string|null       $resultable_id
 * @property Organization|null $resultable
 * @property Carbon|null       $accepted_at
 */
class Request extends UuidKeyModel
{
    use HasFactory;
    use IsSoftDeletable;
    use IsUserOwnable;

    protected $fillable = [
        'url',
        'body',
        'data',
    ];

    protected $casts = [
        'data'        => AsArrayObject::class,
        'accepted_at' => 'date',
    ];

    protected array $searchable = [
        'ownerUser'   => FulltextSearch::SEARCH_WEIGHT_HIGH,
        'requestType' => FulltextSearch::SEARCH_WEIGHT_MODERATE,
        'body'        => FulltextSearch::SEARCH_WEIGHT_LOW,
    ];

    /**
     * {@inheritDoc}
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->instancePermissions[] = RequestPolicy::ACCEPT;
        $this->instancePermissions[] = RequestPolicy::REFUSE;
    }

    /**
     * @return BelongsTo
     */
    public function requestType(): BelongsTo
    {
        return $this->belongsTo(RequestType::class);
    }

    /**
     * @return MorphTo
     */
    public function requestable(): MorphTo
    {
        return $this->morphTo()->withTrashed();
    }

    /**
     * @return MorphTo
     */
    public function resultable(): MorphTo
    {
        return $this->morphTo()->withTrashed();
    }
}
