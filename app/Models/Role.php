<?php

namespace App\Models;

use App\Models\Casts\AsTranslationObject;
use App\Models\Casts\TranslationObject;
use App\Models\Composables\IsFulltextSearchable;
use App\Models\Composables\IsPermissionable;
use App\Models\Concerns\HasUuidKey;
use App\Models\Contracts\FulltextSearchable;
use App\Models\Contracts\Permissionable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Spatie\Permission\Models\Role as BaseRole;

/**
 * Class Role.
 *
 * @property string            $id
 * @property string            $name
 * @property string            $guard_name
 * @property string            $scope_name
 * @property TranslationObject $translated_name
 * @property Carbon            $created_at
 * @property Carbon            $updated_at
 *
 * @method static Builder|static query()
 */
class Role extends BaseRole implements FulltextSearchable, Permissionable
{
    use IsFulltextSearchable;
    use IsPermissionable;
    use HasUuidKey;

    protected $keyType = 'string';

    public $incrementing = false;

    protected $casts = [
        'translated_name' => AsTranslationObject::class,
    ];
}
