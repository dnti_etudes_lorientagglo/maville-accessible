<?php

namespace App\Models;

use App\Models\Casts\AsTranslationObject;
use App\Models\Casts\TranslationObject;
use App\Models\Composables\IsCodable;
use App\Models\Concerns\UuidKeyModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class OrganizationType.
 *
 * @property TranslationObject $name
 */
class OrganizationType extends UuidKeyModel
{
    use HasFactory;
    use IsCodable;

    protected $fillable = [
        'name',
    ];

    protected $casts = [
        'name' => AsTranslationObject::class,
    ];
}
