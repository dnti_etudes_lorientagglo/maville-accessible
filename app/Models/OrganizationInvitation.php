<?php

namespace App\Models;

use App\Models\Concerns\UuidKeyModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class OrganizationInvitation.
 *
 * @property string       $email
 * @property string       $organization_id
 * @property Organization $organization
 * @property string       $role_id
 * @property Role         $role
 */
class OrganizationInvitation extends UuidKeyModel
{
    use HasFactory;

    protected $fillable = [
        'email',
    ];

    /**
     * @return BelongsTo
     */
    public function organization(): BelongsTo
    {
        return $this->belongsTo(Organization::class);
    }

    /**
     * @return BelongsTo
     */
    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class);
    }
}
