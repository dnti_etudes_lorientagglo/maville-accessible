<?php

namespace App\Models\Pivots;

use App\Models\Category;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphPivot;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Class CategorizableCategory.
 */
class CategorizableCategory extends MorphPivot
{
    protected $table = 'categorizables';

    public $timestamps = false;

    /**
     * @return MorphTo
     */
    public function categorizable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
}
