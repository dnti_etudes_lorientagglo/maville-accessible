<?php

namespace App\Models;

use App\Helpers\DateHelper;
use App\Models\Casts\AsArrayObject;
use App\Models\Casts\AsTranslationObject;
use App\Models\Casts\TranslationObject;
use App\Models\Composables\IsAccessible;
use App\Models\Composables\IsCategorizable;
use App\Models\Composables\IsContactable;
use App\Models\Composables\IsCoverable;
use App\Models\Composables\IsImageable;
use App\Models\Composables\IsLinkable;
use App\Models\Composables\IsMediable;
use App\Models\Composables\IsOwnable;
use App\Models\Composables\IsPinnable;
use App\Models\Composables\IsPublishable;
use App\Models\Composables\IsReviewable;
use App\Models\Composables\IsSluggable;
use App\Models\Composables\IsSourceable;
use App\Models\Concerns\UuidKeyModel;
use App\Models\Contracts\Categorizable;
use App\Models\Contracts\Coverable;
use App\Models\Contracts\HasReadLink;
use App\Models\Contracts\Ownable;
use App\Models\Contracts\Pinnable;
use App\Models\Contracts\Publishable;
use App\Models\Contracts\Reviewable;
use App\Models\Contracts\Sluggable;
use App\Models\Contracts\Sourceable;
use App\Services\Search\FulltextSearch;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\ArrayObject;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Activity.
 *
 * @property bool                   $pinned
 * @property TranslationObject      $name
 * @property TranslationObject|null $description
 * @property TranslationObject|null $body
 * @property string|null            $pricing_code
 * @property string|null            $pricing_description
 * @property array|null             $public_codes
 * @property-read int|null          $prev_opening_days_difference
 * @property-read int|null          $next_opening_days_difference
 * @property bool                   $opening_on_periods
 * @property array|null             $opening_periods
 * @property array|null             $opening_dates_hours
 * @property array|null             $opening_weekdays_hours
 * @property ArrayObject|null       $address
 * @property-read  string|null      $addressCity
 * @property string|null            $place_id
 * @property Place|null             $place
 * @property string                 $activity_type_id
 * @property ActivityType           $activityType
 */
class Activity extends UuidKeyModel implements
    Categorizable,
    Coverable,
    HasReadLink,
    Ownable,
    Pinnable,
    Publishable,
    Reviewable,
    Sluggable,
    Sourceable
{
    use IsAccessible;
    use IsCategorizable;
    use IsContactable;
    use IsCoverable;
    use IsImageable;
    use IsLinkable;
    use IsMediable;
    use IsPinnable;
    use IsPublishable;
    use IsOwnable;
    use IsReviewable;
    use IsSluggable;
    use IsSourceable;

    protected $fillable = [
        'name',
        'description',
        'body',
        'pricing_code',
        'pricing_description',
        'public_codes',
        'opening_on_periods',
        'opening_periods',
        'opening_dates_hours',
        'opening_weekdays_hours',
        'address',
    ];

    protected $casts = [
        'name'                   => AsTranslationObject::class,
        'description'            => AsTranslationObject::class,
        'body'                   => AsTranslationObject::class,
        'pricing_description'    => AsTranslationObject::class,
        'public_codes'           => 'array',
        'opening_on_periods'     => 'boolean',
        'opening_periods'        => 'array',
        'opening_dates_hours'    => 'array',
        'opening_weekdays_hours' => 'array',
        'address'                => AsArrayObject::class,
    ];

    protected array $searchable = [
        'name'        => FulltextSearch::SEARCH_WEIGHT_HIGH,
        'description' => FulltextSearch::SEARCH_WEIGHT_LOW,
        'body'        => FulltextSearch::SEARCH_WEIGHT_LOW,
        'addressCity' => FulltextSearch::SEARCH_WEIGHT_LOW,
        'place'       => FulltextSearch::SEARCH_WEIGHT_LOW,
    ];

    /**
     * {@inheritDoc}
     */
    public function getSlugSource(): string
    {
        return implode(' ', array_filter([
            $this->guessSlugFromTranslation($this->name),
            $this->place->address['city'] ?? $this->address['city'] ?? null,
        ]));
    }

    /**
     * {@inheritDoc}
     */
    public function getReadLink(): string
    {
        return "/{$this->activityType->code}/read/$this->slug";
    }

    /**
     * Compute a title containing next dates relative to $date for activity.
     *
     * @param Carbon $date
     *
     * @return string
     */
    public function computeNextDatesTitle(Carbon $date): string
    {
        $activityPeriods = $this->computeActivityPeriods($date);
        $activityDates = $this->computeActivityDates($date, true);

        if ($this->opening_on_periods && ($activityPeriods[1]->isNotEmpty() || $activityPeriods[0]->isEmpty())) {
            if ($activityPeriods[1]->isNotEmpty()) {
                return trans('common.dates.period', [
                    'start' => DateHelper::format(Carbon::parse($activityPeriods[1][0]['start']), 'LL'),
                    'end'   => DateHelper::format(Carbon::parse($activityPeriods[1][0]['end']), 'LL'),
                ]);
            }

            return trans('resources.activities.labels.openingPeriods.none');
        }

        if (! $this->opening_on_periods && $activityDates[1]->isNotEmpty()) {
            $dates = $activityDates[1]->map(static fn(array $d) => Carbon::parse($d['date']));
            $lastDate = $dates->pop();

            return $dates->isEmpty()
                ? trans('common.dates.one', ['date' => DateHelper::format($lastDate, 'LL')])
                : trans('common.dates.many', [
                    'dates' => $dates->map(static fn(Carbon $d) => DateHelper::format($d, 'LL'))->join(', '),
                    'last'  => DateHelper::format($lastDate, 'LL'),
                ]);
        }

        return trans('resources.activities.states.noDateSoon');
    }

    /**
     * Compute activity periods before and after $date.
     *
     * @param Carbon $now
     *
     * @return array
     */
    public function computeActivityPeriods(Carbon $now): array
    {
        [$before, $after] = collect($this->opening_periods ?? [])
            ->unique(static fn(array $p) => $p['start'] . $p['end'])
            ->partition(
                static fn(array $p) => Carbon::parse($p['end'])
                    ->startOfDay()
                    ->isBefore($now->clone()->startOfDay()),
            );

        return [
            $before->sortByDesc(['start', 'end']),
            $after->sortBy(['start', 'end']),
        ];
    }

    /**
     * Compute activity dates before and after $date.
     *
     * @param Carbon $now
     * @param bool   $uniqByDay
     *
     * @return array
     */
    public function computeActivityDates(Carbon $now, bool $uniqByDay): array
    {
        [$before, $after] = collect($this->opening_dates_hours ?? [])
            ->unique(static fn(array $p) => ($uniqByDay ? $p['date'] : $p['date'] . $p['start'] . $p['end']))
            ->partition(
                static fn(array $p) => Carbon::parse($p['date'])
                    ->startOfDay()
                    ->isBefore($now->clone()->startOfDay()),
            );

        return [
            $before->sortByDesc('date'),
            $after->sortBy('date'),
        ];
    }

    /**
     * Get address city name for fulltext search.
     *
     * @return string|null
     */
    public function getAddressCityAttribute(): string | null
    {
        return $this->address['city'] ?? null;
    }

    /**
     * Get previous opening days difference.
     *
     * @return Attribute
     */
    protected function prevOpeningDaysDifference(): Attribute
    {
        return Attribute::make(
            get: static fn(string | null $value) => ($value !== null ? intval($value) : null),
        );
    }

    /**
     * Get next opening days difference.
     *
     * @return Attribute
     */
    protected function nextOpeningDaysDifference(): Attribute
    {
        return Attribute::make(
            get: static fn(string | null $value) => ($value !== null && intval($value) >= 0 ? intval($value) : null),
        );
    }

    /**
     * @return BelongsTo
     */
    public function activityType(): BelongsTo
    {
        return $this->belongsTo(ActivityType::class);
    }

    /**
     * @return BelongsTo
     */
    public function place(): BelongsTo
    {
        return $this->belongsTo(Place::class);
    }
}
