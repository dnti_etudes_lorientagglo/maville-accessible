<?php

namespace App\Models;

use App\Models\Composables\IsFulltextSearchable;
use App\Models\Composables\IsPermissionable;
use App\Models\Concerns\HasUuidKey;
use App\Models\Contracts\FulltextSearchable;
use App\Models\Contracts\Permissionable;
use App\Models\Enums\FileVisibility;
use App\Services\Search\FulltextSearch;
use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\URL;
use Spatie\MediaLibrary\Conversions\ImageGenerators\Image;
use Spatie\MediaLibrary\MediaCollections\Models\Media as BaseMedia;

/**
 * Class Media.
 *
 * @property-read string         $type
 * @property-read string         $original_url
 * @property-read string         $redirect_url
 * @property-read string         $resolve_url
 * @property-read string|null    $placeholder_url
 * @property-read string|null    $optimized_url
 * @property-read string|null    $optimized_srcset
 * @property-read int|null       $width
 * @property-read int|null       $height
 * @property-read FileVisibility $visibility
 * @property string              $id
 * @property string              $uuid
 * @property string              $collection_name
 * @property string              $name
 * @property string              $file_name
 * @property string              $disk
 * @property string              $conversions_disk
 * @property string              $mime_type
 * @property int|null            $size
 * @property string              $hash
 * @property string              $model_type
 * @property string              $model_id
 * @property array               $manipulations
 * @property array               $generated_conversions
 * @property array               $responsive_images
 * @property array               $custom_properties
 *
 * @method static Builder|static query()
 */
class Media extends BaseMedia implements FulltextSearchable, Permissionable
{
    use IsFulltextSearchable;
    use IsPermissionable;
    use HasUuidKey;

    public const TYPE_IMAGE = 'image';

    protected $keyType = 'string';

    public $incrementing = false;

    protected $fillable = [
        'name',
    ];

    /**
     * {@inheritDoc}
     */
    public function getSearchable(): array
    {
        return [
            'name' => FulltextSearch::SEARCH_WEIGHT_HIGH,
        ];
    }

    /**
     * @return string|null
     */
    protected function getRedirectURLAttribute(): string | null
    {
        return URL::route('actions.media.redirect', [
            'media' => $this->id,
        ]);
    }

    /**
     * @return string|null
     */
    protected function getResolveURLAttribute(): string | null
    {
        return URL::route('actions.media.resolve', [
            'media' => $this->id,
        ]);
    }

    /**
     * @return string|null
     */
    protected function getOriginalURLAttribute(): string | null
    {
        return $this->visibility === FileVisibility::PUBLIC
            ? $this->getUrl()
            : null;
    }

    /**
     * @return string|null
     */
    protected function getPlaceholderURLAttribute(): string | null
    {
        return $this->getWhenSupportsOptimized(
            fn() => $this->responsiveImages('optimized')->getPlaceholderSvg(),
        );
    }

    /**
     * @return string|null
     */
    protected function getOptimizedURLAttribute(): string | null
    {
        return $this->getWhenSupportsOptimized(fn() => $this->getUrl('optimized'));
    }

    /**
     * @return string|null
     */
    protected function getOptimizedSrcsetAttribute(): string | null
    {
        return $this->getWhenSupportsOptimized(fn() => $this->getSrcset('optimized'));
    }

    /**
     * @return int|null
     */
    protected function getWidthAttribute(): int | null
    {
        return $this->getCustomProperty('width');
    }

    /**
     * @return int|null
     */
    protected function getHeightAttribute(): int | null
    {
        return $this->getCustomProperty('height');
    }

    /**
     * @return string
     */
    protected function getHashAttribute(): string
    {
        return $this->getCustomProperty('hash');
    }

    /**
     * @return FileVisibility
     */
    protected function getVisibilityAttribute(): FileVisibility
    {
        return FileVisibility::from(
            $this->getCustomProperty('visibility') ?? FileVisibility::PUBLIC->value,
        );
    }

    /**
     * Check if the media supports optimized URLs.
     *
     * @return bool
     */
    private function supportsOptimized(): bool
    {
        return (new Image())->canHandleMime($this->mime_type)
            && $this->visibility === FileVisibility::PUBLIC;
    }

    /**
     * Get a value only if optimized conversion is supported.
     *
     * @param Closure $getter
     *
     * @return mixed
     */
    private function getWhenSupportsOptimized(Closure $getter): mixed
    {
        if ($this->supportsOptimized()) {
            $value = $getter();
            if ($value !== null && $value !== '') {
                return $value;
            }
        }

        return null;
    }
}
