<?php

namespace App\Models;

use App\Models\Casts\AsTranslationObject;
use App\Models\Casts\TranslationObject;
use App\Models\Composables\IsCodable;
use App\Models\Concerns\UuidKeyModel;

/**
 * Class RequestType.
 *
 * @property TranslationObject $name
 */
class RequestType extends UuidKeyModel
{
    use IsCodable;

    protected $fillable = [
        'name',
    ];

    protected $casts = [
        'name' => AsTranslationObject::class,
    ];
}
