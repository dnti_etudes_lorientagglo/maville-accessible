<?php

namespace App\Models\Contracts;

/**
 * Interface Permissionable.
 *
 * @property string[] $allowsPermissions
 */
interface Permissionable
{
}
