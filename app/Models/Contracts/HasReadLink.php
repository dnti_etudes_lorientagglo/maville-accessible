<?php

namespace App\Models\Contracts;

/**
 * Interface HasReadLink.
 */
interface HasReadLink
{
    /**
     * Get the frontend route path.
     *
     * @return string
     */
    public function getReadLink(): string;
}
