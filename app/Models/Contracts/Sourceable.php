<?php

namespace App\Models\Contracts;

use App\Models\Source;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * Interface Sourceable.
 *
 * @property Carbon|null             $sourced_at
 * @property Collection<int, Source> $sources
 */
interface Sourceable
{
    /**
     * Check if a sourceable is sourced.
     *
     * @return bool
     */
    public function sourced(): bool;

    /**
     * Check if a sourceable is synced with its sources.
     *
     * @return bool
     */
    public function isSyncedWithSources(): bool;

    /**
     * Update the sourceable to mark it as resynced with sources.
     *
     * @return void
     */
    public function markResyncedWithSources(): void;

    /**
     * @return MorphMany
     */
    public function sources(): MorphMany;
}
