<?php

namespace App\Models\Contracts;

/**
 * Interface Ownable.
 */
interface Ownable extends UserOwnable, OrganizationOwnable
{
}
