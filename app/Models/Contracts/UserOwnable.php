<?php

namespace App\Models\Contracts;

use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Interface UserOwnable.
 *
 * @property string|null $owner_user_id
 * @property User|null   $ownerUser
 */
interface UserOwnable
{
    /**
     * @return BelongsTo
     */
    public function ownerUser(): BelongsTo;
}
