<?php

namespace App\Models\Contracts;

use App\Models\Review;
use Illuminate\Database\Eloquent\Casts\ArrayObject;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * Interface Reviewable.
 *
 * @property-read  ArrayObject       $reviews_summary
 * @property Collection<int, Review> $reviews
 */
interface Reviewable
{
    /**
     * @return MorphMany
     */
    public function reviews(): MorphMany;
}
