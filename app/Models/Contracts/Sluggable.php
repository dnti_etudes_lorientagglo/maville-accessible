<?php

namespace App\Models\Contracts;

/**
 * Interface Sluggable.
 *
 * @property string $slug
 */
interface Sluggable
{
    /**
     * Get the source string for slug generation.
     *
     * @return string
     */
    public function getSlugSource(): string;
}
