<?php

namespace App\Models\Contracts;

use App\Models\Organization;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Interface OrganizationOwnable.
 *
 * @property string|null       $owner_organization_id
 * @property Organization|null $ownerOrganization
 */
interface OrganizationOwnable
{
    /**
     * @return BelongsTo
     */
    public function ownerOrganization(): BelongsTo;
}
