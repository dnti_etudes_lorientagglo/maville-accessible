<?php

namespace App\Models\Contracts;

use Carbon\Carbon;

/**
 * Interface Publishable.
 *
 * @property Carbon|null $published_at
 */
interface Publishable
{
    /**
     * Check if a publishable is published.
     *
     * @return bool
     */
    public function published(): bool;
}
