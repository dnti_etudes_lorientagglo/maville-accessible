<?php

namespace App\Models\Contracts;

use Spatie\MediaLibrary\HasMedia as BaseHasMedia;

/**
 * Interface HasMedia.
 */
interface HasMedia extends BaseHasMedia
{
    /**
     * Register the collection of media for user.
     */
    public function registerMediaCollections(): void;
}
