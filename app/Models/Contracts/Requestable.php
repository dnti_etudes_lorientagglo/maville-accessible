<?php

namespace App\Models\Contracts;

use App\Models\Report;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * Interface Requestable.
 *
 * @property Collection<int, Report> $requests
 */
interface Requestable
{
    /**
     * @return MorphMany
     */
    public function requests(): MorphMany;
}
