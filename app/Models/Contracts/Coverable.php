<?php

namespace App\Models\Contracts;

use App\Models\Media;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Interface Coverable.
 *
 * @property string|null $cover_id
 * @property Media|null  $cover
 */
interface Coverable
{
    /**
     * @return BelongsTo
     */
    public function cover(): BelongsTo;
}
