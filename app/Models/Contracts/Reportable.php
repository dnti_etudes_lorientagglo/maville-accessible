<?php

namespace App\Models\Contracts;

use App\Models\Report;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * Interface Reportable.
 *
 * @property Collection<int, Report> $reports
 */
interface Reportable
{
    /**
     * @return MorphMany
     */
    public function reports(): MorphMany;
}
