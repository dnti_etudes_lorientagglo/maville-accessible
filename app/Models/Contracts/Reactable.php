<?php

namespace App\Models\Contracts;

use App\Models\Reaction;
use Illuminate\Database\Eloquent\Casts\ArrayObject;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

/**
 * Interface Reportable.
 *
 * @property-read ArrayObject          $reactions_summary
 * @property-read Reaction|null        $current_reaction
 * @property Collection<int, Reaction> $reactions
 */
interface Reactable
{
    /**
     * @return MorphOne
     */
    public function currentReaction(): MorphOne;

    /**
     * @return MorphMany
     */
    public function reactions(): MorphMany;
}
