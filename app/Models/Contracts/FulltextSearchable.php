<?php

namespace App\Models\Contracts;

use App\Services\Search\FulltextSearch;

/**
 * Interface FulltextSearchable.
 *
 * @property-read int $search_score
 * @property mixed    $search_text
 */
interface FulltextSearchable
{
    /**
     * Check if model is searchable.
     *
     * @return bool
     */
    public function isSearchable(): bool;

    /**
     * Get the fulltext searchable columns of model mapped with search column.
     *
     * @return array<string, string>
     */
    public function getSearchable(): array;

    /**
     * Get fulltext search service instance.
     *
     * @return FulltextSearch
     */
    public function getFulltextSearchService(): FulltextSearch;
}
