<?php

namespace App\Models\Contracts;

use Illuminate\Database\Eloquent\Relations\MorphToMany;

/**
 * Interface Categorizable.
 */
interface Categorizable
{
    /**
     * @return MorphToMany
     */
    public function categories(): MorphToMany;
}
