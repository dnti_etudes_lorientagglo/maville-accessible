<?php

namespace App\Models\Contracts;

use Carbon\Carbon;

/**
 * Interface Pinnable.
 *
 * @property Carbon|null $pinned_at
 */
interface Pinnable
{
    /**
     * Check if pinnable is pinned.
     *
     * @return bool
     */
    public function pinned(): bool;
}
