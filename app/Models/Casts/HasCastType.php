<?php

namespace App\Models\Casts;

use Illuminate\Support\Str;

/**
 * Trait HasCastType.
 */
trait HasCastType
{
    /**
     * Get the cast type from the static class name.
     *
     * @return string
     */
    public static function castType(): string
    {
        return Str::lower(static::class);
    }
}
