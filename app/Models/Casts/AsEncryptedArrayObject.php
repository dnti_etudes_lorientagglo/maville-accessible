<?php

namespace App\Models\Casts;

use Illuminate\Support\Facades\Crypt;

/**
 * Class AsEncryptedArrayObject.
 */
class AsEncryptedArrayObject extends AsArrayObject
{
    /**
     * {@inheritDoc}
     */
    protected function decode(string|null $value): mixed
    {
        return parent::decode($value !== null ? Crypt::decryptString($value) : null);
    }

    /**
     * {@inheritDoc}
     */
    protected function encode(mixed $value): string|null
    {
        return Crypt::encryptString(parent::encode($value));
    }
}
