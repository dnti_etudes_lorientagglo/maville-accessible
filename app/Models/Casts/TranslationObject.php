<?php

namespace App\Models\Casts;

use Illuminate\Database\Eloquent\Casts\ArrayObject;

/**
 * Class TranslationObject.
 *
 * Alias class of ArrayObject class to better identify translatable attributes.
 */
class TranslationObject extends ArrayObject
{
}
