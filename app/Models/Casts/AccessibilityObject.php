<?php

namespace App\Models\Casts;

use Illuminate\Database\Eloquent\Casts\ArrayObject;

/**
 * Class AccessibilityObject.
 *
 * Alias class of ArrayObject class to better identify translatable attributes.
 */
class AccessibilityObject extends ArrayObject
{
}
