<?php

namespace App\Models\Casts;

use App\Helpers\JsonHelper;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Database\Eloquent\Casts\ArrayObject;

/**
 * Class AsArrayObject.
 */
class AsArrayObject implements CastsAttributes
{
    use HasCastType;

    /**
     * {@inheritDoc}
     */
    public function get($model, string $key, $value, array $attributes): object|null
    {
        if (! isset($attributes[$key])) {
            return null;
        }

        $data = $this->decode($attributes[$key]);

        return is_array($data) ? $this->makeInstance($data) : null;
    }

    /**
     * {@inheritDoc}
     */
    public function set($model, string $key, $value, array $attributes): array
    {
        return [
            $key => $this->encode($value),
        ];
    }

    /**
     * Decode the database value.
     *
     * @param string|null $value
     *
     * @return mixed
     */
    protected function decode(string|null $value): mixed
    {
        if ($value === null || $value === '') {
            return null;
        }

        return JsonHelper::decode($value);
    }

    /**
     * Decode the database value.
     *
     * @param mixed $value
     *
     * @return string|null
     */
    protected function encode(mixed $value): string|null
    {
        if ($value === null) {
            return null;
        }

        return $value === [] ? '{}' : JsonHelper::encode($value);
    }

    /**
     * Create the object instance for data.
     *
     * @param array $data
     *
     * @return object
     */
    protected function makeInstance(array $data): object
    {
        return new ArrayObject($data);
    }
}
