<?php

namespace App\Models\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Support\Str;

/**
 * Class AsLowerCase.
 */
class AsLowerCase implements CastsAttributes
{
    /**
     * {@inheritDoc}
     */
    public function get($model, string $key, $value, array $attributes): string|null
    {
        if (! is_string($value)) {
            return null;
        }

        return Str::lower($value);
    }

    /**
     * {@inheritDoc}
     */
    public function set($model, string $key, $value, array $attributes): array
    {
        return [
            $key => is_string($value) ? Str::lower($value) : null,
        ];
    }
}
