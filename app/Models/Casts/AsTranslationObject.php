<?php

namespace App\Models\Casts;

/**
 * Class AsTranslationObject.
 *
 * Alias class of AsArrayObject cast to better identify translatable attributes.
 */
class AsTranslationObject extends AsArrayObject
{
    /**
     * {@inheritDoc}
     */
    protected function makeInstance(array $data): object
    {
        return new TranslationObject($data);
    }
}
