<?php

namespace App\Models\Casts;

use App\Models\Enums\AccessibilityCategory;

/**
 * Class AsAccessibilityObject.
 */
class AsAccessibilityObject extends AsArrayObject
{
    /**
     * {@inheritDoc}
     */
    public function get($model, string $key, $value, array $attributes): object|null
    {
        $data = parent::get($model, $key, $value, $attributes);
        if (! $data) {
            return $data;
        }

        collect(AccessibilityCategory::cases())->each(static function (AccessibilityCategory $category) use ($data) {
            if (isset($data[$category->value])) {
                if (! is_int($data[$category->value]['rating'])) {
                    $data[$category->value] = null;

                    return;
                }

                if (empty($data[$category->value]['body'])) {
                    $data[$category->value]['body'] = null;
                }
            }
        });

        return $data;
    }

    /**
     * {@inheritDoc}
     */
    protected function makeInstance(array $data): object
    {
        return new AccessibilityObject($data);
    }
}
