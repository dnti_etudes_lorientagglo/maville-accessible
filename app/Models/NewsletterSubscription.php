<?php

namespace App\Models;

use App\Models\Concerns\UuidKeyModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class NewsletterSubscription.
 *
 * @property-read User|null $user
 * @property string         $email
 */
class NewsletterSubscription extends UuidKeyModel
{
    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'email', 'email');
    }
}
