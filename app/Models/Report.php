<?php

namespace App\Models;

use App\Models\Composables\IsSoftDeletable;
use App\Models\Composables\IsUserOwnable;
use App\Models\Concerns\UuidKeyModel;
use App\Models\Contracts\Reportable;
use App\Services\Search\FulltextSearch;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Class Report.
 *
 * @property string          $url
 * @property string|null     $body
 * @property string          $report_type_id
 * @property ReportType      $reportType
 * @property string|null     $reportable_type
 * @property string|null     $reportable_id
 * @property Reportable|null $reportable
 */
class Report extends UuidKeyModel
{
    use HasFactory;
    use IsSoftDeletable;
    use IsUserOwnable;

    protected $fillable = [
        'url',
        'body',
    ];

    protected array $searchable = [
        'ownerUser'  => FulltextSearch::SEARCH_WEIGHT_HIGH,
        'reportType' => FulltextSearch::SEARCH_WEIGHT_MODERATE,
        'body'       => FulltextSearch::SEARCH_WEIGHT_LOW,
    ];

    /**
     * @return BelongsTo
     */
    public function reportType(): BelongsTo
    {
        return $this->belongsTo(ReportType::class);
    }

    /**
     * @return MorphTo
     */
    public function reportable(): MorphTo
    {
        return $this->morphTo()->withTrashed();
    }
}
