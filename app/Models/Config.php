<?php

namespace App\Models;

use App\Models\Casts\AsArrayObject;
use App\Models\Casts\AsEncryptedArrayObject;
use App\Models\Casts\AsTranslationObject;
use App\Models\Casts\TranslationObject;
use App\Models\Concerns\RegistersMedia;
use App\Models\Concerns\UuidKeyModel;
use App\Models\Contracts\HasMedia;
use App\Services\App\AppEnv;
use Illuminate\Database\Eloquent\Casts\ArrayObject;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\App;

/**
 * Class Config.
 *
 * @property TranslationObject|null    $description
 * @property ArrayObject|null          $services
 * @property ArrayObject|null          $map_center
 * @property int|null                  $map_zoom
 * @property Collection<int, Category> $mapSearchableCategories
 * @property Collection<int, Category> $mapDefaultCategories
 */
class Config extends UuidKeyModel implements HasMedia
{
    use RegistersMedia;

    public const MEDIA_PUBLIC_COLLECTION = 'app-public';

    protected $fillable = [
        'description',
        'services',
        'map_center',
        'map_zoom',
    ];

    protected $casts = [
        'description' => AsTranslationObject::class,
        'services'    => AsEncryptedArrayObject::class,
        'map_center'  => AsArrayObject::class,
        'map_zoom'    => 'integer',
    ];

    /**
     * {@inheritDoc}
     */
    protected static function boot(): void
    {
        parent::boot();

        self::saved(static function () {
            App::get(AppEnv::class)->forgetPublicEnvCache();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(Config::MEDIA_PUBLIC_COLLECTION);
    }

    /**
     * @return BelongsToMany
     */
    public function mapSearchableCategories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'config_map_searchable_categories', 'config_id', 'category_id');
    }

    /**
     * @return BelongsToMany
     */
    public function mapDefaultCategories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'config_map_default_categories', 'config_id', 'category_id');
    }
}
