<?php

namespace App\Models;

use App\Models\Concerns\UuidKeyModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class SendingCampaignBatch.
 *
 * @property int         $attempts
 * @property array       $recipients
 * @property Carbon|null $started_at Datetime of the first attempt's start.
 * @property Carbon|null $running_at Datetime of the current attempt start (null if not running).
 * @property Carbon|null $finished_at Datetime of the end (or last attempt failure).
 * @property Carbon|null $failed_at Datetime of the last failure.
 * @property-read array  $recipientsCounts
 */
class SendingCampaignBatch extends UuidKeyModel
{
    protected $casts = [
        'attempts'    => 'integer',
        'recipients'  => 'array',
        'started_at'  => 'datetime',
        'running_at'  => 'datetime',
        'finished_at' => 'datetime',
        'failed_at'   => 'datetime',
    ];

    /**
     * @return BelongsTo
     */
    public function campaign(): BelongsTo
    {
        return $this->belongsTo(SendingCampaign::class);
    }

    /**
     * Get the recipients count by current statuses.
     *
     * @return array
     */
    public function getRecipientsCountsAttribute(): array
    {
        return collect($this->recipients)->countBy('status')->all();
    }
}
