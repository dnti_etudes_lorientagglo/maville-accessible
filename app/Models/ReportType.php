<?php

namespace App\Models;

use App\Models\Casts\AsTranslationObject;
use App\Models\Casts\TranslationObject;
use App\Models\Composables\IsCodable;
use App\Models\Concerns\UuidKeyModel;
use App\Models\Enums\ReportTypeCode;
use App\Services\Search\FulltextSearch;
use Illuminate\Database\Eloquent\Casts\Attribute;

/**
 * Class ReportType.
 *
 * @property TranslationObject $name
 * @property bool              $global
 */
class ReportType extends UuidKeyModel
{
    use IsCodable;

    protected $fillable = [
        'name',
        'global',
    ];

    protected $casts = [
        'name'   => AsTranslationObject::class,
        'global' => 'boolean',
    ];

    protected array $searchable = [
        'name' => FulltextSearch::SEARCH_WEIGHT_HIGH,
    ];

    /**
     * Get the position of the report type, allowing to order in a selection input.
     *
     * @return Attribute
     */
    public function position(): Attribute
    {
        return Attribute::make(
            get: static fn($_, array $attrs) => match (true) {
                $attrs['global'] => 0,
                $attrs['code'] === ReportTypeCode::OTHER->value => 200,
                default => 100,
            },
        );
    }
}
