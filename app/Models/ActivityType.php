<?php

namespace App\Models;

use App\Models\Casts\AsTranslationObject;
use App\Models\Casts\TranslationObject;
use App\Models\Composables\IsCodable;
use App\Models\Concerns\UuidKeyModel;

/**
 * Class ActivityType.
 *
 * @property TranslationObject $name
 */
class ActivityType extends UuidKeyModel
{
    use IsCodable;

    protected $fillable = [
        'name',
    ];

    protected $casts = [
        'name' => AsTranslationObject::class,
    ];
}
