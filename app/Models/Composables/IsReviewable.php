<?php

namespace App\Models\Composables;

use App\Models\Casts\AsArrayObject;
use App\Models\Review;
use Illuminate\Database\Eloquent\Casts\ArrayObject;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * Trait IsReviewable.
 *
 * @property-read ArrayObject        $reviews_summary
 * @property Collection<int, Review> $reviews
 *
 * @mixin Model
 */
trait IsReviewable
{
    /**
     * Boot a reviewable model.
     */
    protected static function bootIsReviewable(): void
    {
        self::deleted(static function (self $model) {
            $model->reviews()->delete();
        });
    }

    /**
     * Initialize a reviewable model.
     */
    protected function initializeIsReviewable(): void
    {
        $this->casts['reviews_summary'] = AsArrayObject::class;
    }

    /**
     * @return MorphMany
     */
    public function reviews(): MorphMany
    {
        return $this->morphMany(Review::class, 'reviewable');
    }
}
