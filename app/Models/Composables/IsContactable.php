<?php

namespace App\Models\Composables;

use Illuminate\Database\Eloquent\Model;

/**
 * Trait IsContactable.
 *
 * @property string|null $email
 * @property string|null $phone
 *
 * @mixin Model
 */
trait IsContactable
{
    /**
     * Initialize a contactable model.
     */
    protected function initializeIsContactable(): void
    {
        $this->fillable[] = 'email';
        $this->fillable[] = 'phone';
    }
}
