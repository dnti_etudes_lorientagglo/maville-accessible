<?php

namespace App\Models\Composables;

use App\Models\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * Trait IsRequestable.
 *
 * @property Collection<int, Request> $requests
 *
 * @mixin Model
 */
trait IsRequestable
{
    /**
     * Boot a requestable model.
     */
    protected static function bootIsRequestable(): void
    {
        self::deleted(static function (self $model) {
            $model->requests()->delete();
        });
    }

    /**
     * @return MorphMany
     */
    public function requests(): MorphMany
    {
        return $this->morphMany(Request::class, 'requestable');
    }
}
