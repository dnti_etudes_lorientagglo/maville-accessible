<?php

namespace App\Models\Composables;

use App\Models\Place;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

/**
 * Trait IsLocalizable.
 *
 * @property Collection<int, Place> $places
 *
 * @mixin Model
 */
trait IsLocalizable
{
    /**
     * Boot a localizable model.
     */
    protected static function bootIsLocalizable(): void
    {
        self::deleted(static function (self $model) {
            $model->places()->detach();
        });
    }

    /**
     * Initialize a localizable model.
     */
    protected function initializeIsLocalizable(): void
    {
    }

    /**
     * @return MorphToMany
     */
    public function places(): MorphToMany
    {
        return $this->morphToMany(Place::class, 'localizable');
    }
}
