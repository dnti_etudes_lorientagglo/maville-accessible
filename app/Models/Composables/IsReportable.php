<?php

namespace App\Models\Composables;

use App\Models\Report;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * Trait IsReportable.
 *
 * @property Collection<int, Report> $reports
 *
 * @mixin Model
 */
trait IsReportable
{
    /**
     * Boot a reportable model.
     */
    protected static function bootIsReportable(): void
    {
        self::deleted(static function (self $model) {
            $model->reports()->delete();
        });
    }

    /**
     * @return MorphMany
     */
    public function reports(): MorphMany
    {
        return $this->morphMany(Report::class, 'reportable');
    }
}
