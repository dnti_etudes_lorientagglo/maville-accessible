<?php

namespace App\Models\Composables;

use App\Models\Concerns\BaseModel;
use Carbon\Carbon;

/**
 * Trait IsPinnable.
 *
 * @property Carbon|null $pinned_at
 *
 * @mixin BaseModel
 */
trait IsPinnable
{
    /**
     * Initialize a publishable model.
     */
    protected function initializeIsPinnable(): void
    {
        $this->casts['pinned_at'] = 'datetime';

        $this->instancePermissions[] = 'pin';
        $this->instancePermissions[] = 'unpin';
    }

    /**
     * Check if pinnable is pinned.
     *
     * @return bool
     */
    public function pinned(): bool
    {
        return $this->pinned_at !== null;
    }
}
