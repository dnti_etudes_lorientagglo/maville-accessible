<?php

namespace App\Models\Composables;

use App\Helpers\AuthHelper;
use App\Models\Casts\AsArrayObject;
use App\Models\Reaction;
use Illuminate\Database\Eloquent\Casts\ArrayObject;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

/**
 * Trait IsReactable.
 *
 * @property-read string|null          $current_reaction
 * @property-read ArrayObject          $reactions_summary
 * @property Collection<int, Reaction> $reactions
 *
 * @mixin Model
 */
trait IsReactable
{
    /**
     * Boot a reactable model.
     */
    protected static function bootIsReactable(): void
    {
        self::deleted(static function (self $model) {
            $model->reactions()->delete();
        });
    }

    /**
     * Initialize a reactable model.
     */
    protected function initializeIsReactable(): void
    {
        $this->casts['reactions_summary'] = AsArrayObject::class;
    }

    /**
     * @return MorphOne
     */
    public function currentReaction(): MorphOne
    {
        $user = AuthHelper::user();

        return $this->reactions()
            ->whereNotNull('owner_user_id')
            ->where('owner_user_id', $user?->id)
            ->one();
    }

    /**
     * @return MorphMany
     */
    public function reactions(): MorphMany
    {
        return $this->morphMany(Reaction::class, 'reactable');
    }
}
