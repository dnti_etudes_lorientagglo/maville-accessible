<?php

namespace App\Models\Composables;

use App\Helpers\LocaleHelper;
use App\Models\Concerns\HasUniqueKey;
use Closure;
use Illuminate\Database\Eloquent\Casts\ArrayObject;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Trait IsSluggable.
 *
 * @property string $slug
 *
 * @mixin Model
 */
trait IsSluggable
{
    use HasUniqueKey;

    /**
     * Boot a sluggable model.
     */
    protected static function bootIsSluggable(): void
    {
        // We are using creating and updating events instead of saving because
        // slug generator should be called after saving events or ID generation.
        self::registerKeyGenerationHook(static::getSlugGenerator(), 'slug');
        self::registerKeyGenerationHook(static::getSlugGenerator(), 'slug', 'updating', true);
    }

    /**
     * Get the slug generator closure.
     *
     * @return Closure
     */
    public static function getSlugGenerator(): Closure
    {
        return static function (self $model, int $tries) {
            $slugSource = $model->getSlugSource();

            return Str::slug($tries > 0 ? "$slugSource-$tries" : $slugSource);
        };
    }

    /**
     * Guess a slug from a translation value.
     *
     * @param ArrayObject|null $sluggable
     *
     * @return string
     */
    private function guessSlugFromTranslation(ArrayObject|null $sluggable): string
    {
        return $this->guessSlugFromString(LocaleHelper::translate($sluggable));
    }

    /**
     * Guess a slug from a string value.
     *
     * @param string|null $sluggable
     *
     * @return string
     */
    private function guessSlugFromString(string|null $sluggable): string
    {
        return Str::limit($sluggable ?? $this->getKey(), 100, '');
    }

    /**
     * Get the source string for slug generation.
     *
     * @return string
     */
    abstract public function getSlugSource(): string;
}
