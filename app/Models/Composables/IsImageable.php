<?php

namespace App\Models\Composables;

use App\Models\Media;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

/**
 * Trait IsImageable.
 *
 * @property Collection<int, Media> $images
 *
 * @mixin Model
 */
trait IsImageable
{
    /**
     * Boot a imageable model.
     */
    protected static function bootIsImageable(): void
    {
        self::deleted(static function (self $model) {
            $model->images()->detach();
        });
    }

    /**
     * @return MorphToMany
     */
    public function images(): MorphToMany
    {
        return $this->morphToMany(Media::class, 'imageable');
    }
}
