<?php

namespace App\Models\Composables;

use App\Models\Casts\AccessibilityObject;
use App\Models\Casts\AsAccessibilityObject;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait IsAccessible.
 *
 * @property AccessibilityObject|null $accessibility
 *
 * @mixin Model
 */
trait IsAccessible
{
    /**
     * Initialize an accessible model.
     */
    protected function initializeIsAccessible(): void
    {
        $this->fillable[] = 'accessibility';

        $this->casts['accessibility'] = AsAccessibilityObject::class;
    }
}
