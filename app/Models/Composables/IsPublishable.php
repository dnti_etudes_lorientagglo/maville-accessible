<?php

namespace App\Models\Composables;

use App\Models\Concerns\BaseModel;
use App\Models\Contracts\Publishable;
use Carbon\Carbon;

/**
 * Trait IsPublishable.
 *
 * @property Carbon|null $published_at
 *
 * @mixin BaseModel
 */
trait IsPublishable
{
    /**
     * Initialize a publishable model.
     */
    protected function initializeIsPublishable(): void
    {
        $this->casts['published_at'] = 'datetime';

        $this->instancePermissions[] = 'publish';
        $this->instancePermissions[] = 'unPublish';
    }

    /**
     * @see Publishable::published()
     */
    public function published(): bool
    {
        return ! ! $this->published_at;
    }
}
