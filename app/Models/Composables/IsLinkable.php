<?php

namespace App\Models\Composables;

use App\Models\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Casts\ArrayObject;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait IsLinkable.
 *
 * @property ArrayObject|null $links
 *
 * @mixin Model
 */
trait IsLinkable
{
    /**
     * Initialize a linkable model.
     */
    protected function initializeIsLinkable(): void
    {
        $this->fillable[] = 'links';
        $this->casts['links'] = AsArrayObject::class;
    }
}
