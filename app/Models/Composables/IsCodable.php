<?php

namespace App\Models\Composables;

use BackedEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Trait IsCodable.
 *
 * @property string $code
 *
 * @mixin Model
 */
trait IsCodable
{
    /**
     * Find the model by its code.
     *
     * @param BackedEnum|string $code
     *
     * @return static
     */
    public static function findByCode(BackedEnum | string $code): static
    {
        return static::query()->where('code', self::resolveDBCode($code))->firstOrFail();
    }

    /**
     * Find the model's ID by its code.
     *
     * @param BackedEnum|string $code
     *
     * @return string
     */
    public static function findIdByCode(BackedEnum | string $code): string
    {
        return static::query()->where('code', self::resolveDBCode($code))->valueOrFail('id');
    }

    /**
     * Resolve backed enum code.
     *
     * @param BackedEnum|string $code
     *
     * @return string
     */
    private static function resolveDBCode(BackedEnum | string $code): string
    {
        return $code instanceof BackedEnum ? $code->value : $code;
    }

    /**
     * Get a map of code for each ID.
     *
     * @return Collection
     */
    public static function fetchIdsByCodesMap(): Collection
    {
        return static::query()->pluck('id', 'code');
    }
}
