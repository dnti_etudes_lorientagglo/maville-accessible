<?php

namespace App\Models\Composables;

use App\Models\Media;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

/**
 * Trait IsMediable.
 *
 * @property Collection<int, Media> $media
 *
 * @mixin Model
 */
trait IsMediable
{
    /**
     * Boot a mediable model.
     */
    protected static function bootIsMediable(): void
    {
        self::deleted(static function (self $model) {
            $model->media()->detach();
        });
    }

    /**
     * @return MorphToMany
     */
    public function media(): MorphToMany
    {
        return $this->morphToMany(Media::class, 'mediable');
    }
}
