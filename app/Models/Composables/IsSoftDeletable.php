<?php

namespace App\Models\Composables;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Trait IsSoftDeletable.
 *
 * @property Carbon|null $deleted_at
 *
 * @mixin Model
 */
trait IsSoftDeletable
{
    use SoftDeletes;
}
