<?php

namespace App\Models\Composables;

use App\Models\Media;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Trait IsCoverable.
 *
 * @property string|null $cover_id
 * @property Media|null  $cover
 *
 * @mixin Model
 */
trait IsCoverable
{
    /**
     * @return BelongsTo
     */
    public function cover(): BelongsTo
    {
        return $this->belongsTo(Media::class);
    }
}
