<?php

namespace App\Models\Composables;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Facades\Gate;
use Throwable;

/**
 * Trait IsPermissionable.
 */
trait IsPermissionable
{
    /**
     * @var string[] Model scoped permissions which should be listed.
     */
    protected array $modelPermissions = [];

    /**
     * @var string[] Instances related permissions which should be listed.
     */
    protected array $instancePermissions = [
        'show',
        'update',
        'delete',
    ];

    /**
     * Get the authorized permissions list for instance.
     *
     * @return Attribute
     */
    public function allowsPermissions(): Attribute
    {
        return Attribute::make(
            get: fn() => [
                ...collect($this->getModelPermissions())->filter(
                    fn(array $p) => $this->checkModelPermission($p[0], $p[1]),
                )->map(
                    fn(array $p) => (new $p[0]())->getMorphClass() . '.' . $p[1],
                ),
                ...collect($this->getInstancePermissions())->filter(
                    fn(string $p) => $this->checkInstancePermission($p),
                ),
            ],
        );
    }

    /**
     * Check a model scoped permission.
     *
     * @param string $model
     * @param string $permission
     *
     * @return bool
     */
    protected function checkModelPermission(string $model, string $permission): bool
    {
        try {
            return Gate::allows($permission, $model);
        } catch (Throwable) {
            return false;
        }
    }

    /**
     * Check a global permission.
     *
     * @param string $permission
     *
     * @return bool
     */
    protected function checkInstancePermission(string $permission): bool
    {
        try {
            return Gate::allows($permission, $this);
        } catch (Throwable) {
            return false;
        }
    }

    /**
     * Get the model scoped permissions which should be listed.
     *
     * @return string[]
     */
    protected function getModelPermissions(): array
    {
        return $this->modelPermissions;
    }

    /**
     * Get the instances related permissions which should be listed.
     *
     * @return string[]
     */
    protected function getInstancePermissions(): array
    {
        return $this->instancePermissions;
    }

    /**
     * Build a scoped permissions array.
     *
     * @param string $model
     * @param array  $permissions
     *
     * @return array
     */
    protected function buildModelPermissions(string $model, array $permissions): array
    {
        return collect($permissions)->map(fn(string $p) => [$model, $p])->all();
    }
}
