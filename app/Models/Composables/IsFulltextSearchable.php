<?php

namespace App\Models\Composables;

use App\Jobs\Search\UpdateRelationSearchIndexesJob;
use App\Services\Search\FulltextSearch;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait IsFulltextSearchable.
 *
 * @property-read int   $search_score
 * @property mixed|null $search_text
 *
 * @mixin Model
 */
trait IsFulltextSearchable
{
    /**
     * @var bool Toggle syncing search indexes of instance on saving event.
     */
    public bool $syncSearchIndexesOnSaving = true;

    /**
     * @var bool Toggle syncing search indexes of relationships on saved event.
     */
    public bool $syncRelationsSearchIndexesOnSaved = true;

    /**
     * The fulltext searchable columns of model mapped with search column.
     *
     * @var array<string, string>
     */
    protected array $searchable = [];

    /**
     * The list of relationships where search indexes depend on an instance
     * of this model (with the relations to load to update indexes).
     *
     * @var array<string, string[]>
     */
    protected array $dependingSearchableRelations = [];

    /**
     * Boot a fulltext searchable model.
     */
    protected static function bootIsFulltextSearchable(): void
    {
        self::saved(static function (self $model) {
            if ($model->syncSearchIndexesOnSaving) {
                $model->getFulltextSearchService()->updateSearchColumnsForInstance($model);
            }

            if ($model->syncRelationsSearchIndexesOnSaved) {
                foreach ($model->getDependingSearchableRelations() as $relation => $with) {
                    UpdateRelationSearchIndexesJob::dispatch($model, $relation, $with);
                }
            }
        });
    }

    /**
     * Filter the given query by search using fulltext search service.
     *
     * @param Builder $query
     * @param string  $search
     *
     * @return void
     */
    public function scopeWhereSearch(Builder $query, string $search): void
    {
        $this->getFulltextSearchService()->whereSearch($query, $search);
    }

    /**
     * Order the given query by search using fulltext search service.
     * This scope cannot be called without having called whereSearch scope.
     *
     * @param Builder $query
     * @param string  $direction
     *
     * @return void
     */
    public function scopeOrderBySearch(Builder $query, string $direction = 'asc'): void
    {
        $this->getFulltextSearchService()->orderBySearch($query, $direction);
    }

    /**
     * Check if model is searchable.
     *
     * @return bool
     */
    public function isSearchable(): bool
    {
        return ! empty($this->getSearchable());
    }

    /**
     * Get the fulltext searchable columns of model mapped with search column.
     *
     * @return array<string, string>
     */
    public function getSearchable(): array
    {
        return $this->searchable;
    }

    /**
     * Get the fulltext search score attribute (only when retrieved through a search).
     *
     * @return Attribute
     */
    public function searchScore(): Attribute
    {
        return Attribute::make(
            get: static function ($_, array $attrs) {
                return isset($attrs[FulltextSearch::SEARCH_SCORE_ALIAS])
                    ? round(floatval($attrs[FulltextSearch::SEARCH_SCORE_ALIAS]), 4)
                    : null;
            },
        );
    }

    /**
     * Get fulltext search service instance.
     *
     * @return FulltextSearch
     */
    public function getFulltextSearchService(): FulltextSearch
    {
        return app(FulltextSearch::class);
    }

    /**
     * Get the list of relationships where search indexes depend on an instance
     * of this model (with the relations to load to update indexes).
     *
     * @return array
     */
    protected function getDependingSearchableRelations(): array
    {
        return $this->dependingSearchableRelations;
    }
}
