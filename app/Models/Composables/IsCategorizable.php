<?php

namespace App\Models\Composables;

use App\Models\Category;
use App\Services\Search\FulltextSearch;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

/**
 * Trait IsCategorizable.
 *
 * @property Collection<int, Category> $categories
 *
 * @mixin Model
 */
trait IsCategorizable
{
    /**
     * Boot a categorizable model.
     */
    protected static function bootIsCategorizable(): void
    {
        self::deleted(static function (self $model) {
            $model->categories()->detach();
        });
    }

    /**
     * Initialize a categorizable model.
     */
    protected function initializeIsCategorizable(): void
    {
        $this->searchable['categories'] = FulltextSearch::SEARCH_WEIGHT_MODERATE;
    }

    /**
     * @return MorphToMany
     */
    public function categories(): MorphToMany
    {
        return $this->morphToMany(Category::class, 'categorizable');
    }
}
