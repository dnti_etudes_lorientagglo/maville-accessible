<?php

namespace App\Models\Composables;

use App\Models\Organization;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Trait IsOrganizationOwnable.
 *
 * @property string|null       $owner_organization_id
 * @property Organization|null $ownerOrganization
 *
 * @mixin Model
 */
trait IsOrganizationOwnable
{
    /**
     * @return BelongsTo
     */
    public function ownerOrganization(): BelongsTo
    {
        return $this->belongsTo(Organization::class);
    }
}
