<?php

namespace App\Models\Composables;

/**
 * Trait IsOwnable.
 */
trait IsOwnable
{
    use IsOrganizationOwnable;
    use IsUserOwnable;
}
