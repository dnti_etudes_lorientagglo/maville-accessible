<?php

namespace App\Models\Composables;

use App\Helpers\AuthHelper;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Trait IsUserOwnable.
 *
 * @property string|null $owner_user_id
 * @property User|null   $ownerUser
 *
 * @mixin Model
 */
trait IsUserOwnable
{
    /**
     * Boot a user ownable model.
     */
    protected static function bootIsUserOwnable(): void
    {
        self::creating(static function (self $model) {
            if (! $model->owner_user_id) {
                $model->ownerUser()->associate(AuthHelper::user());
            }
        });
    }

    /**
     * @return BelongsTo
     */
    public function ownerUser(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
