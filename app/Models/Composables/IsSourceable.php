<?php

namespace App\Models\Composables;

use App\Models\Contracts\Publishable;
use App\Models\Source;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * Trait IsSourceable.
 *
 * @property Carbon|null             $sourced_at
 * @property Collection<int, Source> $sources
 *
 * @mixin Model
 */
trait IsSourceable
{
    /**
     * Boot a sourceable model.
     */
    protected static function bootIsSourceable(): void
    {
        self::deleted(static function (self $model) {
            $model->sources()->delete();
        });
    }

    /**
     * Initialize a sourceable model.
     */
    protected function initializeIsSourceable(): void
    {
        $this->casts['sourced_at'] = 'datetime';

        $this->instancePermissions[] = 'mergeSources';
        $this->instancePermissions[] = 'syncSources';
    }

    /**
     * Check if a sourceable is sourced.
     *
     * @return bool
     */
    public function sourced(): bool
    {
        return ! ! $this->sourced_at;
    }

    /**
     * Check if a sourceable is synced with its source.
     *
     * @return bool
     */
    public function isSyncedWithSources(): bool
    {
        return $this->sourced_at
            && $this->sourced_at->equalTo($this->updated_at)
            && (
                ! ($this instanceof Publishable)
                || ($this->published_at && $this->published_at->equalTo($this->created_at))
            );
    }

    /**
     * Update the sourceable to mark it as resynced with sources.
     *
     * @return void
     */
    public function markResyncedWithSources(): void
    {
        $this->sourced_at = now();
        $this->updated_at = $this->sourced_at;

        if (
            $this instanceof Publishable
            && (! $this->published_at || ! $this->published_at->equalTo($this->created_at))
        ) {
            $this->published_at = $this->created_at;
        }

        $this->save(['timestamps' => false]);
    }

    /**
     * @return MorphMany
     */
    public function sources(): MorphMany
    {
        return $this->morphMany(Source::class, 'sourceable');
    }
}
