<?php

namespace App\Models\Joins;

use App\Helpers\RandomHelper;
use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Str;

/**
 * Class AliasedJoin.
 */
abstract class AliasedJoin
{
    /**
     * Join the given relation on query and return the aliased table.
     *
     * @param Builder       $query
     * @param string        $table
     * @param BelongsToMany $relation
     * @param string        $joinType
     *
     * @return string
     */
    abstract public function join(mixed $query, string $table, Relation $relation, string $joinType): string;

    /**
     * Generate a string alias for a table.
     *
     * @param string $table
     *
     * @return string
     */
    protected function generateAlias(string $table): string
    {
        return Str::lower($table . '_' . RandomHelper::randomString());
    }
}
