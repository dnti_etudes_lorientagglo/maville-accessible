<?php

namespace App\Models\Joins;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\JoinClause;

/**
 * Class BelongsToJoin.
 */
class BelongsToJoin extends AliasedJoin
{
    /**
     * {@inheritDoc}
     */
    public function join(mixed $query, string $table, Relation $relation, string $joinType): string
    {
        $relatedAlias = $this->generateAlias($relation->getRelated()->getTable());

        $query
            ->{$joinType}(
                $relation->getRelated()->getTable() . ' as ' . $relatedAlias,
                fn(JoinClause $join) => $this->configureRelatedJoin($join, $relation, $table, $relatedAlias),
            );

        return $relatedAlias;
    }

    /**
     * Configure join for related table.
     *
     * @param JoinClause $join
     * @param BelongsTo  $relation
     * @param string     $parentTable
     * @param string     $relatedTable
     *
     * @return void
     */
    protected function configureRelatedJoin(
        JoinClause $join,
        Relation $relation,
        string $parentTable,
        string $relatedTable,
    ): void {
        $join->on(
            $parentTable . '.' . $relation->getForeignKeyName(),
            $relatedTable . '.' . $relation->getOwnerKeyName(),
        );
    }
}
