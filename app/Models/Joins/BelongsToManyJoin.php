<?php

namespace App\Models\Joins;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\JoinClause;

/**
 * Class BelongsToManyJoin.
 */
class BelongsToManyJoin extends AliasedJoin
{
    /**
     * {@inheritDoc}
     */
    public function join(mixed $query, string $table, Relation $relation, string $joinType): string
    {
        $pivotAlias = $this->generateAlias($relation->getTable());
        $relatedAlias = $this->generateAlias($relation->getRelated()->getTable());

        $query
            ->{$joinType}(
                $relation->getTable() . ' as ' . $pivotAlias,
                fn(JoinClause $join) => $this->configurePivotJoin($join, $relation, $table, $pivotAlias),
            )
            ->{$joinType}(
                $relation->getRelated()->getTable() . ' as ' . $relatedAlias,
                fn(JoinClause $join) => $this->configureRelatedJoin($join, $relation, $pivotAlias, $relatedAlias),
            );

        return $relatedAlias;
    }

    /**
     * Configure join for pivot table.
     *
     * @param JoinClause    $join
     * @param BelongsToMany $relation
     * @param string        $parentTable
     * @param string        $pivotTable
     *
     * @return void
     */
    protected function configurePivotJoin(
        JoinClause $join,
        Relation $relation,
        string $parentTable,
        string $pivotTable,
    ): void {
        $join->on(
            $pivotTable . '.' . $relation->getForeignPivotKeyName(),
            $parentTable . '.' . $relation->getParentKeyName(),
        );
    }

    /**
     * Configure join for related table.
     *
     * @param JoinClause    $join
     * @param BelongsToMany $relation
     * @param string        $pivotTable
     * @param string        $relatedTable
     *
     * @return void
     */
    protected function configureRelatedJoin(
        JoinClause $join,
        Relation $relation,
        string $pivotTable,
        string $relatedTable,
    ): void {
        $join->on(
            $relatedTable . '.' . $relation->getRelatedKeyName(),
            $pivotTable . '.' . $relation->getRelatedPivotKeyName(),
        );
    }
}
