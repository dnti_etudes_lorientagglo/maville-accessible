<?php

namespace App\Models\Joins;

use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\JoinClause;

/**
 * Class MorphToManyJoin.
 */
class MorphToManyJoin extends BelongsToManyJoin
{
    /**
     * Configure join for pivot table (add morph type clause).
     *
     * @param JoinClause  $join
     * @param MorphToMany $relation
     * @param string      $parentTable
     * @param string      $pivotTable
     *
     * @return void
     */
    protected function configurePivotJoin(
        JoinClause $join,
        Relation $relation,
        string $parentTable,
        string $pivotTable,
    ): void {
        parent::configurePivotJoin($join, $relation, $parentTable, $pivotTable);

        $join->where(
            $pivotTable . '.' . $relation->getMorphType(),
            $relation->getMorphClass(),
        );
    }
}
