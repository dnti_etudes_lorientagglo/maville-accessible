<?php

namespace App\Models;

use App\Models\Casts\AsTranslationObject;
use App\Models\Casts\TranslationObject;
use App\Models\Composables\IsAccessible;
use App\Models\Composables\IsReactable;
use App\Models\Composables\IsSoftDeletable;
use App\Models\Composables\IsUserOwnable;
use App\Models\Concerns\UuidKeyModel;
use App\Models\Contracts\Reactable;
use App\Models\Contracts\Reviewable;
use App\Models\Contracts\UserOwnable;
use Database\Factories\ReviewFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Class Review.
 *
 * @property int                    $rating
 * @property TranslationObject|null $body
 * @property string                 $reviewable_type
 * @property string                 $reviewable_id
 * @property Reviewable             $reviewable
 *
 * @method static ReviewFactory factory()
 */
class Review extends UuidKeyModel implements Reactable, UserOwnable
{
    use HasFactory;
    use IsAccessible;
    use IsReactable;
    use IsSoftDeletable;
    use IsUserOwnable;

    protected $fillable = [
        'rating',
        'body',
    ];

    protected $casts = [
        'rating' => 'integer',
        'body'   => AsTranslationObject::class,
    ];

    /**
     * @return MorphTo
     */
    public function reviewable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return HasMany
     */
    public function replies(): HasMany
    {
        return $this->hasMany(ReviewReply::class);
    }
}
