<?php

namespace App\Models;

use App\Models\Casts\AsArrayObject;
use App\Models\Concerns\UuidKeyModel;
use App\Models\Contracts\Sourceable;
use Illuminate\Database\Eloquent\Casts\ArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Class Source.
 *
 * @property string      $name
 * @property ArrayObject $data
 * @property string      $sourceable_type
 * @property string      $sourceable_id
 * @property Sourceable  $sourceable
 */
class Source extends UuidKeyModel
{
    use HasFactory;

    protected $casts = [
        'data' => AsArrayObject::class,
    ];

    /**
     * @return MorphTo
     */
    public function sourceable(): MorphTo
    {
        return $this->morphTo();
    }
}
