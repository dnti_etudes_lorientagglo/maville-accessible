<?php

namespace App\Models\Concerns;

use App\Helpers\JsonHelper;
use App\Models\Composables\IsFulltextSearchable;
use App\Models\Composables\IsPermissionable;
use App\Models\Contracts\FulltextSearchable;
use App\Models\Contracts\Permissionable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UnescapedJsonModel.
 *
 * An extended model with an unescaped JSON serialization and fulltext search capabilities.
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @method static Builder|static query()
 */
abstract class BaseModel extends Model implements FulltextSearchable, Permissionable
{
    use IsFulltextSearchable;
    use IsPermissionable;

    /**
     * {@inheritDoc}
     */
    protected function asJson($value): string
    {
        return JsonHelper::encode($value);
    }
}
