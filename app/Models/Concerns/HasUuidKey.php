<?php

namespace App\Models\Concerns;

use Closure;
use Illuminate\Support\Str;

/**
 * Trait HasUuidKey.
 */
trait HasUuidKey
{
    use HasUniqueKey;

    /**
     * Boot a UUID keyed model.
     */
    protected static function bootHasUuidKey(): void
    {
        self::registerKeyGenerationHook(static::getUuidGenerator());
    }

    /**
     * Get the UUID generator closure.
     *
     * @return Closure
     */
    public static function getUuidGenerator(): Closure
    {
        return static fn() => Str::uuid()->toString();
    }
}
