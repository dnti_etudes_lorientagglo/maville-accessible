<?php

namespace App\Models\Concerns;

use Closure;
use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Trait HasUniqueKey.
 *
 * @mixin Model
 */
trait HasUniqueKey
{
    /**
     * Register the key generation on creating.
     *
     * @param Closure     $keyGenerator
     * @param string|null $keyName
     * @param string      $useHook
     * @param bool        $force
     *
     * @return void
     */
    protected static function registerKeyGenerationHook(
        Closure $keyGenerator,
        string|null $keyName = null,
        string $useHook = 'creating',
        bool $force = false,
    ): void {
        self::{$useHook}(static function (self $model) use ($keyGenerator, $keyName, $force): void {
            $model->runKeyGeneration($keyGenerator, $keyName, $force);
        });
    }

    /**
     * Run a key generation.
     *
     * @param Closure     $keyGenerator
     * @param string|null $keyName
     * @param bool        $force
     *
     * @return void
     */
    public function runKeyGeneration(
        Closure $keyGenerator,
        string|null $keyName = null,
        bool $force = false,
    ): void {
        $keyNameOrDefault = $keyName ?? $this->getKeyName();
        $prevKeyValue = $this->{$keyNameOrDefault};
        if (empty($prevKeyValue) || $force) {
            $tries = 0;
            do {
                $keyValue = $keyGenerator($this, $tries);
                if ($keyValue === $prevKeyValue) {
                    // If the value does not change since previous, do not run any query.
                    return;
                }

                $tries++;
            } while (
                DB::table($this->getTable())
                    ->where($keyNameOrDefault, $keyValue)
                    ->when(
                        $this->{$this->getKeyName()},
                        fn(Builder $q, $id) => $q->where($this->getKeyName(), '<>', $id)
                    )
                    ->exists()
            );

            $this->{$keyNameOrDefault} = $keyValue;
        }
    }
}
