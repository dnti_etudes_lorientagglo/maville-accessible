<?php

namespace App\Models\Concerns;

/**
 * Class UuidKeyModel.
 */
abstract class UuidKeyModel extends StringKeyModel
{
    use HasUuidKey;
}
