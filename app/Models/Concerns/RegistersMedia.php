<?php

namespace App\Models\Concerns;

use App\Models\Enums\FileVisibility;
use App\Models\Media;
use Illuminate\Support\Facades\Config;
use Spatie\Image\Exceptions\InvalidManipulation;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media as BaseMedia;

/**
 * Trait RegistersMedia.
 */
trait RegistersMedia
{
    use InteractsWithMedia;

    /**
     * Should deletion preserve attached media.
     *
     * @return bool
     */
    public function shouldDeletePreservingMedia(): bool
    {
        return true;
    }

    /**
     * Register the conversions for user's media.
     *
     * @param Media|null $media
     *
     * @throws InvalidManipulation
     */
    public function registerMediaConversions(BaseMedia $media = null): void
    {
        if ($media && $media->visibility !== FileVisibility::PUBLIC) {
            return;
        }

        // Create webp with responsive sizes.
        $optimizedConversion = $this->addMediaConversion('optimized')->withResponsiveImages();
        if (Config::get('media-library.webp_support')) {
            $optimizedConversion->format('webp');
        } else {
            $optimizedConversion->keepOriginalImageFormat();
        }
    }
}
