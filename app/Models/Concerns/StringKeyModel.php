<?php

namespace App\Models\Concerns;

/**
 * Class StringKeyModel.
 *
 * @property string $id
 */
abstract class StringKeyModel extends BaseModel
{
    /**
     * @var string The "type" of the primary key ID.
     */
    protected $keyType = 'string';

    /**
     * @var bool Indicates if the IDs are auto-incrementing.
     */
    public $incrementing = false;
}
