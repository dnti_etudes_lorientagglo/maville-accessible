<?php

namespace App\Models;

use App\Models\Casts\AsArrayObject;
use App\Models\Casts\AsTranslationObject;
use App\Models\Casts\TranslationObject;
use App\Models\Composables\IsAccessible;
use App\Models\Composables\IsCategorizable;
use App\Models\Composables\IsContactable;
use App\Models\Composables\IsCoverable;
use App\Models\Composables\IsImageable;
use App\Models\Composables\IsLinkable;
use App\Models\Composables\IsOwnable;
use App\Models\Composables\IsPublishable;
use App\Models\Composables\IsReviewable;
use App\Models\Composables\IsSluggable;
use App\Models\Composables\IsSourceable;
use App\Models\Concerns\UuidKeyModel;
use App\Models\Contracts\Categorizable;
use App\Models\Contracts\Coverable;
use App\Models\Contracts\HasReadLink;
use App\Models\Contracts\Ownable;
use App\Models\Contracts\Publishable;
use App\Models\Contracts\Reviewable;
use App\Models\Contracts\Sluggable;
use App\Models\Contracts\Sourceable;
use App\Services\Search\FulltextSearch;
use Database\Factories\PlaceFactory;
use Illuminate\Database\Eloquent\Casts\ArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Place.
 *
 * @property TranslationObject      $name
 * @property TranslationObject|null $description
 * @property array|null             $opening_hours
 * @property ArrayObject            $address
 * @property-read  string|null      $addressCity
 *
 * @method static PlaceFactory factory()
 */
class Place extends UuidKeyModel implements
    Categorizable,
    Coverable,
    HasReadLink,
    Ownable,
    Publishable,
    Reviewable,
    Sluggable,
    Sourceable
{
    use HasFactory;
    use IsAccessible;
    use IsCategorizable;
    use IsContactable;
    use IsCoverable;
    use IsImageable;
    use IsLinkable;
    use IsPublishable;
    use IsOwnable;
    use IsReviewable;
    use IsSluggable;
    use IsSourceable;

    protected $fillable = [
        'name',
        'description',
        'address',
        'opening_hours',
    ];

    protected $casts = [
        'name'          => AsTranslationObject::class,
        'description'   => AsTranslationObject::class,
        'address'       => AsArrayObject::class,
        'opening_hours' => 'array',
    ];

    protected array $searchable = [
        'name'        => FulltextSearch::SEARCH_WEIGHT_HIGH,
        'addressCity' => FulltextSearch::SEARCH_WEIGHT_HIGH,
        'slug'        => FulltextSearch::SEARCH_WEIGHT_HIGH,
        'description' => FulltextSearch::SEARCH_WEIGHT_LOW,
    ];

    /**
     * {@inheritDoc}
     */
    protected static function boot(): void
    {
        parent::boot();

        self::deleting(static function (Place $place) {
            Activity::query()
                ->whereNotNull('published_at')
                ->whereNotNull('place_id')
                ->where('place_id', $place->id)
                ->update([
                    'place_id'     => null,
                    'published_at' => null,
                ]);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function getSlugSource(): string
    {
        return implode(' ', array_filter([
            $this->guessSlugFromTranslation($this->name),
            $this->address['city'],
        ]));
    }

    /**
     * {@inheritDoc}
     */
    public function getReadLink(): string
    {
        return "/places/map/details/$this->slug";
    }

    /**
     * Get address city name for fulltext search.
     *
     * @return string|null
     */
    public function getAddressCityAttribute(): string | null
    {
        return $this->address['city'] ?? null;
    }
}
