<?php

namespace App\Models;

use App\Helpers\LocaleHelper;
use App\Helpers\NotificationsHelper;
use App\Models\Casts\AsArrayObject;
use App\Models\Casts\AsLowerCase;
use App\Models\Concerns\RegistersMedia;
use App\Models\Concerns\UuidKeyModel;
use App\Models\Contracts\HasMedia;
use App\Notifications\Auth\ResetPassword;
use App\Notifications\Auth\VerifyEmail;
use App\Policies\ActivityPolicy;
use App\Policies\ArticlePolicy;
use App\Policies\CategoryPolicy;
use App\Policies\ConfigPolicy;
use App\Policies\MediaPolicy;
use App\Policies\OrganizationInvitationPolicy;
use App\Policies\OrganizationMemberPolicy;
use App\Policies\OrganizationPolicy;
use App\Policies\PlacePolicy;
use App\Policies\ReportPolicy;
use App\Policies\ReportTypePolicy;
use App\Policies\RequestPolicy;
use App\Policies\RolePolicy;
use App\Policies\SendingCampaignPolicy;
use App\Policies\UserPolicy;
use App\Services\Search\FulltextSearch;
use Carbon\Carbon;
use Database\Factories\UserFactory;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;
use Illuminate\Contracts\Translation\HasLocalePreference;
use Illuminate\Database\Eloquent\Casts\ArrayObject;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\App;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User.
 *
 * @property-read  string                        $fullName
 * @property-read  int                           $reviewsCount
 * @property-read  int                           $unreadNotificationsCount
 * @property-read  array                         $notificationsDefaults
 * @property-read  array                         $filteredNotificationsPreferences
 * @property string                              $first_name
 * @property string                              $last_name
 * @property bool                                $display_initials
 * @property string                              $email
 * @property string                              $password
 * @property string|null                         $remember_token
 * @property string|null                         $locale
 * @property ArrayObject|null                    $notifications_preferences
 * @property Carbon|null                         $email_verified_at
 * @property Carbon|null                         $blocked_at
 * @property Carbon|null                         $last_login_at
 * @property Carbon|null                         $scheduled_deletion_at
 * @property string|null                         $acting_for_organization_id
 * @property OrganizationMember|null             $actingForOrganization
 * @property Collection<int, OrganizationMember> $memberOfOrganizations
 * @property Collection<int, Review>             $reviews
 * @property Collection<int, Role>               $roles
 *
 * @method static UserFactory factory()
 */
class User extends UuidKeyModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract,
    MustVerifyEmailContract,
    HasLocalePreference,
    HasMedia
{
    use Authenticatable;
    use Authorizable;
    use CanResetPassword;
    use HasApiTokens;
    use HasFactory;
    use HasRoles;
    use MustVerifyEmail;
    use Notifiable;
    use RegistersMedia;

    public const MEDIA_PUBLIC_COLLECTION = 'user-public';

    protected $withCount = [
        'reviews',
    ];

    protected $fillable = [
        'first_name',
        'last_name',
        'display_initials',
        'email',
        'password',
        'locale',
        'notifications_preferences',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'display_initials'          => 'boolean',
        'email'                     => AsLowerCase::class,
        'email_verified_at'         => 'datetime',
        'blocked_at'                => 'datetime',
        'last_login_at'             => 'datetime',
        'scheduled_deletion_at'     => 'datetime',
        'notifications_preferences' => AsArrayObject::class,
    ];

    protected array $searchable = [
        'first_name' => FulltextSearch::SEARCH_WEIGHT_HIGH,
        'last_name'  => FulltextSearch::SEARCH_WEIGHT_HIGH,
        'email'      => FulltextSearch::SEARCH_WEIGHT_HIGH,
    ];

    /**
     * {@inheritDoc}
     */
    protected static function boot(): void
    {
        parent::boot();

        self::deleted(static function (self $model) {
            $model->tokens()->delete();
            $model->notifications()->delete();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->instancePermissions[] = UserPolicy::VERIFY;
        $this->instancePermissions[] = UserPolicy::BLOCK;
        $this->instancePermissions[] = UserPolicy::UNBLOCK;
        $this->instancePermissions[] = UserPolicy::SCHEDULE_DELETION;


        $this->modelPermissions = [
            ...$this->modelPermissions,
            ...$this->buildModelPermissions(Config::class, [
                ConfigPolicy::SHOW_ANY,
            ]),
            ...$this->buildModelPermissions(Media::class, [
                MediaPolicy::SHOW_ANY,
            ]),
            ...$this->buildModelPermissions(User::class, [
                UserPolicy::SHOW_ANY,
                UserPolicy::CREATE,
            ]),
            ...$this->buildModelPermissions(Role::class, [
                RolePolicy::SHOW_ANY,
                RolePolicy::ASSIGN_ANY,
            ]),
            ...$this->buildModelPermissions(ReportType::class, [
                ReportTypePolicy::SHOW_ANY,
                ReportTypePolicy::CREATE,
            ]),
            ...$this->buildModelPermissions(Report::class, [
                ReportPolicy::SHOW_ANY,
            ]),
            ...$this->buildModelPermissions(Request::class, [
                RequestPolicy::SHOW_ANY,
            ]),
            ...$this->buildModelPermissions(SendingCampaign::class, [
                SendingCampaignPolicy::SHOW_ANY,
            ]),
            ...$this->buildModelPermissions(Category::class, [
                CategoryPolicy::SHOW_ANY,
                CategoryPolicy::CREATE,
            ]),
            ...$this->buildModelPermissions(OrganizationMember::class, [
                OrganizationMemberPolicy::SHOW_ANY,
                OrganizationMemberPolicy::CREATE,
            ]),
            ...$this->buildModelPermissions(OrganizationInvitation::class, [
                OrganizationInvitationPolicy::SHOW_ANY,
                OrganizationInvitationPolicy::CREATE,
            ]),
            ...$this->buildModelPermissions(Organization::class, [
                OrganizationPolicy::ADMIN,
                OrganizationPolicy::SHOW_ANY,
                OrganizationPolicy::CREATE,
            ]),
            ...$this->buildModelPermissions(Place::class, [
                PlacePolicy::ADMIN,
                PlacePolicy::SHOW_ANY,
                PlacePolicy::CREATE,
            ]),
            ...$this->buildModelPermissions(Article::class, [
                ArticlePolicy::ADMIN,
                ArticlePolicy::SHOW_ANY,
                ArticlePolicy::CREATE,
            ]),
            ...$this->buildModelPermissions(Activity::class, [
                ActivityPolicy::ADMIN,
                ActivityPolicy::SHOW_ANY,
                ActivityPolicy::CREATE,
            ]),
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function checkModelPermission(string $model, string $permission): bool
    {
        return ! $this->blocked() && parent::checkModelPermission($model, $permission);
    }

    /**
     * Get full name attribute definition.
     *
     * @return Attribute
     */
    public function fullName(): Attribute
    {
        return Attribute::make(
            get: static fn($_, array $attrs) => "{$attrs['first_name']} {$attrs['last_name']}",
        );
    }

    /**
     * Get the published reviews count.
     *
     * @return Attribute
     */
    public function reviewsCount(): Attribute
    {
        return Attribute::make(
            get: static fn($_, array $attrs) => isset($attrs['reviews_count'])
                ? intval($attrs['reviews_count'])
                : null,
        );
    }

    /**
     * Get the unread notifications count attribute (only when loaded through a query param).
     *
     * @return Attribute
     */
    public function unreadNotificationsCount(): Attribute
    {
        return Attribute::make(
            get: static fn($_, array $attrs) => isset($attrs['unread_notifications_count'])
                ? intval($attrs['unread_notifications_count'])
                : null,
        );
    }

    /**
     * Get the allowed notifications with their defaults.
     *
     * @return Attribute
     */
    public function notificationsDefaults(): Attribute
    {
        return Attribute::make(
            get: function () {
                $this->loadMissing('roles');

                $actingForOrganization = $this->getActingForOrganization();
                $roles = array_filter([
                    ...$this->roles->map(static fn(Role $r) => $r->name)->all(),
                    $actingForOrganization?->role?->name,
                ]);

                return collect(NotificationsHelper::defaultChannels())
                    ->filter(
                        static fn(array $defaults) => empty($defaults['roles'])
                            || ! empty(array_intersect($roles, $defaults['roles'])),
                    )
                    ->map(static fn(array $defaults) => $defaults['channels'])
                    ->all();
            },
        );
    }

    /**
     * Get the notifications preferences for user based on its role with defaults values.
     *
     * @return Attribute
     */
    public function filteredNotificationsPreferences(): Attribute
    {
        return Attribute::make(
            get: fn() => collect($this->notificationsDefaults)->map(
                fn(array $defaults, string $type) => $this->notifications_preferences[$type] ?? $defaults,
            )->all(),
        );
    }

    /**
     * {@inheritDoc}
     */
    public function sendEmailVerificationNotification(): void
    {
        $this->notify(new VerifyEmail());
    }

    /**
     * {@inheritDoc}
     */
    public function sendPasswordResetNotification($token): void
    {
        $this->notify(new ResetPassword($token));
    }

    /**
     * {@inheritDoc}
     */
    public function preferredLocale(): string
    {
        return $this->locale ? LocaleHelper::server($this->locale) : App::getLocale();
    }

    /**
     * {@inheritDoc}
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(User::MEDIA_PUBLIC_COLLECTION);
    }

    /**
     * Check if user is blocked.
     *
     * @return bool
     */
    public function blocked(): bool
    {
        return ! ! $this->blocked_at;
    }

    /**
     * Get the currently acting for organization.
     *
     * @return OrganizationMember|null
     */
    public function getActingForOrganization(): OrganizationMember | null
    {
        $this->loadMissing([
            'actingForOrganization',
            'actingForOrganization.role',
            'actingForOrganization.organization.membersOfOrganization',
        ]);

        return $this->actingForOrganization;
    }

    /**
     * @return BelongsTo
     */
    public function actingForOrganization(): BelongsTo
    {
        return $this->belongsTo(OrganizationMember::class);
    }

    /**
     * @return HasMany
     */
    public function memberOfOrganizations(): HasMany
    {
        return $this->hasMany(OrganizationMember::class);
    }

    /**
     * @return HasMany
     */
    public function reviews(): HasMany
    {
        return $this->hasMany(Review::class, 'owner_user_id');
    }
}
