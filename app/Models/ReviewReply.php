<?php

namespace App\Models;

use App\Models\Composables\IsSoftDeletable;
use App\Models\Composables\IsUserOwnable;
use App\Models\Concerns\UuidKeyModel;
use App\Models\Contracts\UserOwnable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class ReviewReply.
 *
 * @property string $body
 * @property string $review_id
 * @property Review $review
 */
class ReviewReply extends UuidKeyModel implements UserOwnable
{
    use IsSoftDeletable;
    use IsUserOwnable;

    protected $fillable = [
        'body',
    ];

    /**
     * @return BelongsTo
     */
    public function review(): BelongsTo
    {
        return $this->belongsTo(Review::class);
    }
}
