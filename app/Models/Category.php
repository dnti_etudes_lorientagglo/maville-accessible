<?php

namespace App\Models;

use App\Models\Casts\AsTranslationObject;
use App\Models\Casts\TranslationObject;
use App\Models\Composables\IsPublishable;
use App\Models\Composables\IsSluggable;
use App\Models\Composables\IsSourceable;
use App\Models\Concerns\UuidKeyModel;
use App\Models\Contracts\Publishable;
use App\Models\Contracts\Sluggable;
use App\Models\Contracts\Sourceable;
use App\Models\Pivots\CategorizableCategory;
use App\Services\Search\FulltextSearch;
use Database\Factories\CategoryFactory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

/**
 * Class Category.
 *
 * @property TranslationObject         $name
 * @property TranslationObject|null    $description
 * @property string|null               $icon
 * @property string|null               $color
 * @property Collection<int, Category> $parents
 * @property Collection<int, Category> $children
 *
 * @method static CategoryFactory factory()
 */
class Category extends UuidKeyModel implements Publishable, Sluggable, Sourceable
{
    use HasFactory;
    use IsPublishable;
    use IsSluggable;
    use IsSourceable;

    protected $fillable = [
        'name',
        'description',
        'icon',
        'color',
    ];

    protected $casts = [
        'name'        => AsTranslationObject::class,
        'description' => AsTranslationObject::class,
    ];

    protected array $searchable = [
        'name'        => FulltextSearch::SEARCH_WEIGHT_HIGH,
        'parents'     => FulltextSearch::SEARCH_WEIGHT_MODERATE,
        'description' => FulltextSearch::SEARCH_WEIGHT_MODERATE,
    ];

    protected array $dependingSearchableRelations = [
        'organizations' => ['categories.parents'],
        'articles'      => ['categories.parents'],
        'places'        => ['categories.parents'],
        'activities'    => ['categories.parents'],
    ];

    /**
     * {@inheritDoc}
     */
    public function getSlugSource(): string
    {
        return $this->guessSlugFromTranslation($this->name);
    }

    /**
     * @return BelongsToMany
     */
    public function parents(): BelongsToMany
    {
        return $this->belongsToMany(static::class, 'categories_parents', 'category_id', 'parent_id');
    }

    /**
     * @return BelongsToMany
     */
    public function children(): BelongsToMany
    {
        return $this->belongsToMany(static::class, 'categories_parents', 'parent_id', 'category_id');
    }

    /**
     * @return HasMany
     */
    public function categorizables(): HasMany
    {
        return $this->hasMany(CategorizableCategory::class);
    }

    /**
     * @return MorphToMany
     */
    public function organizations(): MorphToMany
    {
        return $this->morphedByMany(Organization::class, 'categorizable');
    }

    /**
     * @return MorphToMany
     */
    public function articles(): MorphToMany
    {
        return $this->morphedByMany(Article::class, 'categorizable');
    }

    /**
     * @return MorphToMany
     */
    public function places(): MorphToMany
    {
        return $this->morphedByMany(Place::class, 'categorizable');
    }

    /**
     * @return MorphToMany
     */
    public function activities(): MorphToMany
    {
        return $this->morphedByMany(Activity::class, 'categorizable');
    }
}
