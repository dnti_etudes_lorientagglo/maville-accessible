<?php

namespace App\Models;

use App\Models\Composables\IsUserOwnable;
use App\Models\Concerns\UuidKeyModel;
use App\Models\Contracts\Reactable;
use App\Models\Contracts\UserOwnable;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Class Reaction.
 *
 * @property string    $reaction
 * @property string    $reactable_type
 * @property string    $reactable_id
 * @property Reactable $reactable
 */
class Reaction extends UuidKeyModel implements UserOwnable
{
    use IsUserOwnable;

    protected $fillable = [
        'reaction',
    ];

    /**
     * @return MorphTo
     */
    public function reactable(): MorphTo
    {
        return $this->morphTo();
    }
}
