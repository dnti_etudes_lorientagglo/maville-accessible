<?php

namespace App\Models;

use App\Models\Concerns\UuidKeyModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class OrganizationMember.
 *
 * @property string       $organization_id
 * @property Organization $organization
 * @property string       $user_id
 * @property User         $user
 * @property string       $role_id
 * @property Role         $role
 */
class OrganizationMember extends UuidKeyModel
{
    use HasFactory;

    /**
     * {@inheritDoc}
     */
    protected static function boot(): void
    {
        parent::boot();

        self::created(static function (OrganizationMember $member) {
            $member->loadMissing('user');
            if (! $member->user->acting_for_organization_id) {
                $member->user->acting_for_organization_id = $member->id;
                $member->user->save();
            }

            OrganizationInvitation::query()
                ->where('organization_id', $member->organization_id)
                ->where('email', $member->user->email)
                ->delete();
        });

        self::deleted(static function (OrganizationMember $member) {
            $member->loadMissing('user');
            if ($member->id === $member->user->acting_for_organization_id) {
                $member->user->acting_for_organization_id = $member->user
                    ->memberOfOrganizations()
                    ->value('id');
                $member->user->save();
            }
        });
    }

    /**
     * @return BelongsTo
     */
    public function organization(): BelongsTo
    {
        return $this->belongsTo(Organization::class);
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class);
    }
}
