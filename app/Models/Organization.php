<?php

namespace App\Models;

use App\Models\Casts\AsArrayObject;
use App\Models\Casts\AsTranslationObject;
use App\Models\Casts\TranslationObject;
use App\Models\Composables\IsCategorizable;
use App\Models\Composables\IsContactable;
use App\Models\Composables\IsCoverable;
use App\Models\Composables\IsLinkable;
use App\Models\Composables\IsMediable;
use App\Models\Composables\IsPublishable;
use App\Models\Composables\IsSluggable;
use App\Models\Composables\IsSourceable;
use App\Models\Concerns\UuidKeyModel;
use App\Models\Contracts\Categorizable;
use App\Models\Contracts\Coverable;
use App\Models\Contracts\Publishable;
use App\Models\Contracts\Sluggable;
use App\Models\Contracts\Sourceable;
use App\Services\Search\FulltextSearch;
use Database\Factories\OrganizationFactory;
use Illuminate\Database\Eloquent\Casts\ArrayObject;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Organization.
 *
 * @property TranslationObject                   $name
 * @property TranslationObject|null              $description
 * @property TranslationObject|null              $body
 * @property ArrayObject|null                    $administrative_data
 * @property string                              $organization_type_id
 * @property OrganizationType                    $organizationType
 * @property Collection<int, OrganizationMember> $membersOfOrganization
 *
 * @method static OrganizationFactory factory()
 */
class Organization extends UuidKeyModel implements Categorizable, Coverable, Publishable, Sluggable, Sourceable
{
    use HasFactory;
    use IsCategorizable;
    use IsContactable;
    use IsCoverable;
    use IsLinkable;
    use IsMediable;
    use IsPublishable;
    use IsSluggable;
    use IsSourceable;

    protected $fillable = [
        'name',
        'description',
        'body',
        'administrative_data',
    ];

    protected $casts = [
        'name'                => AsTranslationObject::class,
        'description'         => AsTranslationObject::class,
        'body'                => AsTranslationObject::class,
        'administrative_data' => AsArrayObject::class,
    ];

    protected array $searchable = [
        'name'        => FulltextSearch::SEARCH_WEIGHT_HIGH,
        'description' => FulltextSearch::SEARCH_WEIGHT_LOW,
        'body'        => FulltextSearch::SEARCH_WEIGHT_LOW,
    ];

    /**
     * {@inheritDoc}
     */
    public function getSlugSource(): string
    {
        return $this->guessSlugFromTranslation($this->name);
    }

    /**
     * @return BelongsTo
     */
    public function organizationType(): BelongsTo
    {
        return $this->belongsTo(OrganizationType::class);
    }

    /**
     * @return HasMany
     */
    public function membersOfOrganization(): HasMany
    {
        return $this->hasMany(OrganizationMember::class);
    }

    /**
     * @return HasMany
     */
    public function invitations(): HasMany
    {
        return $this->hasMany(OrganizationInvitation::class);
    }
}
