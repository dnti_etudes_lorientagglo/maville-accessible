<?php

namespace App\Models;

use App\Models\Casts\AsArrayObject;
use App\Models\Casts\TranslationObject;
use App\Models\Concerns\UuidKeyModel;
use App\Models\Enums\SendingCampaignTypeCode;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\ArrayObject;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use RuntimeException;

/**
 * Class SendingCampaign.
 *
 * @property-read TranslationObject                $name
 * @property ArrayObject                           $data
 * @property Carbon|null                           $dispatched_at
 * @property Carbon|null                           $started_at
 * @property Carbon|null                           $finished_at
 * @property string                                $campaign_type_id
 * @property SendingCampaignType                   $campaignType
 * @property Collection<int, SendingCampaignBatch> $campaignBatches
 */
class SendingCampaign extends UuidKeyModel
{
    protected $casts = [
        'data'          => AsArrayObject::class,
        'dispatched_at' => 'datetime',
        'started_at'    => 'datetime',
        'finished_at'   => 'datetime',
    ];

    /**
     * Get the name of the campaign.
     *
     * @return TranslationObject
     */
    public function getNameAttribute(): TranslationObject
    {
        return match ($this->campaignType->code) {
            SendingCampaignTypeCode::MAIL->value => new TranslationObject($this->data['subject']),
            default => throw new RuntimeException('Unsupported sending campaign type'),
        };
    }

    /**
     * @return BelongsTo
     */
    public function campaignType(): BelongsTo
    {
        return $this->belongsTo(SendingCampaignType::class);
    }

    /**
     * @return HasMany
     */
    public function campaignBatches(): HasMany
    {
        return $this->hasMany(SendingCampaignBatch::class, 'campaign_id');
    }
}
