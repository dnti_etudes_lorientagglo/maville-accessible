<?php

namespace App\Models;

use App\Models\Casts\AsTranslationObject;
use App\Models\Casts\TranslationObject;
use App\Models\Composables\IsCategorizable;
use App\Models\Composables\IsCoverable;
use App\Models\Composables\IsImageable;
use App\Models\Composables\IsLocalizable;
use App\Models\Composables\IsMediable;
use App\Models\Composables\IsOwnable;
use App\Models\Composables\IsPinnable;
use App\Models\Composables\IsPublishable;
use App\Models\Composables\IsSluggable;
use App\Models\Composables\IsSourceable;
use App\Models\Concerns\UuidKeyModel;
use App\Models\Contracts\Categorizable;
use App\Models\Contracts\Coverable;
use App\Models\Contracts\HasReadLink;
use App\Models\Contracts\Ownable;
use App\Models\Contracts\Pinnable;
use App\Models\Contracts\Publishable;
use App\Models\Contracts\Sluggable;
use App\Models\Contracts\Sourceable;
use App\Services\Search\FulltextSearch;
use Database\Factories\ArticleFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Article.
 *
 * @property TranslationObject      $name
 * @property TranslationObject|null $description
 * @property TranslationObject|null $body
 *
 * @method static ArticleFactory factory()
 */
class Article extends UuidKeyModel implements
    Categorizable,
    Coverable,
    HasReadLink,
    Ownable,
    Pinnable,
    Publishable,
    Sluggable,
    Sourceable
{
    use HasFactory;
    use IsCategorizable;
    use IsCoverable;
    use IsImageable;
    use IsLocalizable;
    use IsMediable;
    use IsPinnable;
    use IsPublishable;
    use IsOwnable;
    use IsSluggable;
    use IsSourceable;

    protected $fillable = [
        'name',
        'description',
        'body',
    ];

    protected $casts = [
        'name'        => AsTranslationObject::class,
        'description' => AsTranslationObject::class,
        'body'        => AsTranslationObject::class,
    ];

    protected array $searchable = [
        'name'        => FulltextSearch::SEARCH_WEIGHT_HIGH,
        'description' => FulltextSearch::SEARCH_WEIGHT_LOW,
        'body'        => FulltextSearch::SEARCH_WEIGHT_LOW,
    ];

    /**
     * {@inheritDoc}
     */
    public function getSlugSource(): string
    {
        return $this->guessSlugFromTranslation($this->name);
    }

    /**
     * {@inheritDoc}
     */
    public function getReadLink(): string
    {
        return "/articles/read/$this->slug";
    }
}
