<?php

use App\Features\Features;
use App\Helpers\JsonHelper;
use App\Helpers\LocaleHelper;
use Illuminate\Support\Facades\Facade;

$locales = JsonHelper::decode(env('APP_LOCALES', '{"fr_FR":"Français"}'));

return [

    /*
    |--------------------------------------------------------------------------
    | Features
    |--------------------------------------------------------------------------
    |
    | Map of features with their values.
    |
    */

    'features' => Features::collectValuesFromEnv()->all(),

    /*
    |--------------------------------------------------------------------------
    | Services
    |--------------------------------------------------------------------------
    |
    | List of configurable external services (if empty, all external services
    | are configurable).
    |
    */

    'external_services' => array_filter(explode(',', env('APP_EXTERNAL_SERVICES', ''))),

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'name' => env('APP_NAME', 'Laravel'),

    /*
    |--------------------------------------------------------------------------
    | Application Territory
    |--------------------------------------------------------------------------
    |
    | The territory the application is used for (will be used on logo baseline).
    |
    */

    'territory' => env('APP_TERRITORY', 'Lorient Agglomération'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services the application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => (bool) env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    'asset_url' => env('ASSET_URL'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'UTC',

    /*
    |--------------------------------------------------------------------------
    | Application Available Countries
    |--------------------------------------------------------------------------
    |
    | The application countries determines which countries are available for the
    | application. It may be used for phone numbers inputs, locations, etc.
    |
    */

    'countries' => explode(',', env('APP_COUNTRIES', 'FR')),

    /*
    |--------------------------------------------------------------------------
    | Application Available Locales
    |--------------------------------------------------------------------------
    |
    | The application locales determines which locales are available for the
    | application. It should be ordered by priority when using as fallback.
    | Next locale configuration option must be compatible with those one.
    |
    */

    'locales' => $locales,

    /*
    |--------------------------------------------------------------------------
    | Application Easy-Read compatible locales
    |--------------------------------------------------------------------------
    |
    | The application locales codes which have "easy-read" mode enabled.
    |
    */

    'easy_read_locales' => explode(',', env(
        'APP_EASY_READ_LOCALES',
        implode(',', array_keys($locales)),
    )),

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => env('APP_LOCALE', 'fr_FR'),

    /*
    |--------------------------------------------------------------------------
    | Application Theme Configuration
    |--------------------------------------------------------------------------
    |
    | The application theme determines the primary colors that will be used
    | by the theme service (notifications, etc.).
    |
    */

    'theme' => [
        'light' => [
            'primary'      => env('THEME_LIGHT_PRIMARY') ?: '#172370',
            'primary_text' => env('THEME_LIGHT_PRIMARY_TEXT'),
            'on_primary'   => env('THEME_LIGHT_ON_PRIMARY'),
        ],
        'dark'  => [
            'primary'      => env('THEME_DARK_PRIMARY'),
            'primary_text' => env('THEME_DARK_PRIMARY_TEXT'),
            'on_primary'   => env('THEME_DARK_ON_PRIMARY'),
        ],
    ],

    'map' => [

        /*
        |--------------------------------------------------------------------------
        | Map layers configuration
        |--------------------------------------------------------------------------
        |
        | Define the available layers to use on map (first of list will be
        | used as the default). Available layers:
        | - osmDefault (OSM default layer by OSM.org)
        | - osmFr (OSM layer enhanced for France usage by OSM France)
        | - osmBzh (OSM layer enhanced for Bretagne usage by OSM France)
        |
        */

        'layers' => explode(',', env('MAP_LAYERS', 'osmFr')),
    ],

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => LocaleHelper::iso6391(env('APP_LOCALE', 'fr_FR')),

    /*
    |--------------------------------------------------------------------------
    | Faker Locale
    |--------------------------------------------------------------------------
    |
    | This locale will be used by the Faker PHP library when generating fake
    | data for your database seeds. For example, this will be used to get
    | localized telephone numbers, street address information and more.
    |
    */

    'faker_locale' => env('APP_LOCALE', 'fr_FR'),

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Trust proxies HTTPS
    |--------------------------------------------------------------------------
    |
    | When app is behind an HTTP proxy which does not forward protocol, this
    | configuration option will allow rewriting request protocol to HTTPS,
    | even if incoming request is using HTTP.
    |
    */

    'trust_proxies_https' => env('APP_TRUST_PROXIES_HTTPS', false),

    /*
    |--------------------------------------------------------------------------
    | Maintenance Mode Driver
    |--------------------------------------------------------------------------
    |
    | These configuration options determine the driver used to determine and
    | manage Laravel's "maintenance mode" status. The "cache" driver will
    | allow maintenance mode to be controlled across multiple machines.
    |
    | Supported drivers: "file", "cache"
    |
    */

    'maintenance' => [
        'driver' => 'file',
        // 'store'  => 'redis',
    ],

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        /*
         * Package Service Providers...
         */

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => Facade::defaultAliases()->merge([
        // 'ExampleClass' => App\Example\ExampleClass::class,
    ])->toArray(),

];
