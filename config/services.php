<?php

$socialCallbackDefaultURL = static function (string $provider) {
    return env('APP_URL') . '/auth/social/' . $provider . '/callback';
};

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain'   => env('MAILGUN_DOMAIN'),
        'secret'   => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
        'scheme'   => 'https',
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key'    => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'acceslibre' => [
        'endpoint' => env('ACCESLIBRE_ENDPOINT', 'https://acceslibre.beta.gouv.fr/api'),
        'zone'     => env('ACCESLIBRE_ZONE'),
    ],

    'datagouv' => [
        'endpoint'       => env('DATAGOUV_ENDPOINT', 'https://www.data.gouv.fr/api/1'),
        'update_dataset' => env('DATAGOUV_UPDATE_DATASET', false),
    ],

    'hitineraire' => [
        'endpoint' => env('HITINERAIRE_ENDPOINT', 'https://routing.lorient-agglo.handimap.fr'),
    ],

];
